<?php

Route::post('/update/weekly/totals', array('as' => 'update_weekly_totals', 'uses' => 'AnalysisController@UpdateTotalWeeklyTable'));


Route::get('/student/enrolment/totals/for/any/level', array('as' => 'student_enrolment_total', 'uses' => 'AnalysisController@calculateStudentEnrolmentTotalsForAllLevels'));
Route::get('/student/attendance/totals/for/any/level', array('as' => 'student_attendance_total', 'uses' => 'AnalysisController@calculateStudentAttendanceTotalsForAllLevels'));

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//Just a test route. Fot testing logic
//if logic works, i implment it
Route::get('/testRoute', function()
{
	

 
   $url = Request::url();

   return $url;




});



Route::get('/another/test/route', array('as' => 'another_test_route', 'uses' => 'SchoolReportCardController@RetrieveWeeksTermsYearsThatDataHasBeenSubmittedOn'));




//Route::when('*', 'csrf', array('POST', 'PUT', 'PATCH', 'DELETE'));
//for testing sending notifications
Route::get('/sendNotification', function()
{
	$devices = 'APA91bFRyul0gWg7J1QnSk_jHFBwgba94YJCPnBQ9js1gm5D6M2KRURkkZ7smBU6ivtGl8WlsfYHnnHBRbKWYrf3yeMeXIlSDEOyoEdpyzG51UHduaytezc9eRkgTK5aaii3ipr9Q4dz7ARVDMl_WzVLVju5sRC4YA';
	$purpose = 'info,';
	$information = ' Just a message';

	$message = NotificationsController::messageToBeSentThroughNotification($purpose, $information);

	return NotificationsController::packagesSendNotification($devices,$message);
});


Route::post('/receiveSMS', function()
{
	return CommunityInvolvementController::smsReceived();
});

//testing pusher
Route::get('/sendPusher', function()
{

	$school_id = 1;
	$data_type_submitted = 'schoolreview';
	return NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($school_id,$data_type_submitted);

});








Route::get('/', function()
{
	
	$url = Request::url();

	if ($url == 'https://cs.msrcghana.org' || $url == 'https://fieldcs.msrcghana.org' || $url == 'http://cs.msrcghana.org' || $url == 'http://fieldcs.msrcghana.org') {

		  return View::make('dashboard');
	}

	return View::make('index');

});

//Kaygee put this here
Route::get('/mobileview', function()
{
	
	return View::make('mobile_responsive');

});

Route::get('/download_school_report', function()
{

	return View::make('download_school_report');

});

Route::get('/home', function()
{
	return View::make('angular_front_index');

});

Route::get('/about', function()
{
	return View::make('about');

});

Route::get('/contact', function()
{
	return View::make('contact');

});


Route::get('api/regions', function(){
    //return Response::json($regions);
    //return json_decode($regions);
    //return json_encode(array_values($regions));
    return Region::all();
});

Route::get('api/districts', function(){
    return District::all();
});



if (Auth::check()) {

	Route::get('/admin', function () {

		return View::make('dashboard');

	});

} else {

	Route::get('/admin', function () {
		return View::make('dashboard');
//		return Redirect::to('/dashboard/login');

	});

}



Route::get('/dashboard/login', array('as' => 'admin_login', 'uses' => 'DashboardController@adminLoginIntoDashboard'));



Route::get('/password', array('as' => 'password_reminder', 'uses' => 'RemindersController@getRemind'));
Route::post('/remind/email', array('as' => 'password_email', 'uses' => 'RemindersController@postRemind'));
Route::get('/password/reset/{token}', array('as' => 'password_reset_view', 'uses' => 'RemindersController@getReset'));
Route::post('/reset/save', array('as' => 'password_reset_save', 'uses' => 'RemindersController@postReset'));


//this get the current ratings of a school
Route::get('school/{id}/rating', array('as' => 'school_rating', 'uses' => 'SchoolController@allSchools'));


//Route::resource('password', 'RemindersController');



Route::group(array('prefix' => 'admin'), function () {


	    Route::resource('users', 'UsersController');
		
		Route::get('dashboard#/', array('as' => 'main_dashboard', 'uses' => '@index'));

		Route::resource('dashboard', 'DashboardController');

	    Route::get('/schools/circuits/districts/regions/schools/circuits/districts/regions', array('as' => 'retrieve_levels', 'uses' => 'SchoolReportCardController@retrieveSchoolsCircuitsDistrictsRegionsDynamically'));

	//this takes the posted details of to retrieve itineraries base on that
		Route::get('/itinerary', array('as' => 'retrieve_itineraries', 'uses' => 'ItinararyController@adminRequestForIntineraries'));

		//this takes the posted details of to retrieve itineraries base on that
		Route::get('/users', array('as' => 'retrieve_all_users', 'uses' => 'UsersController@returnAllRequestedUsersByAdmin'));

		//this routes will receive any query parameter matching fields in the recent_data_submitted
		//table to return recent data submitted
		Route::get('recent/data/submission', array('as' => 'recent_data_submitted', 'uses' => 'DashboardController@recentDataSubmittedByDataCollectorsOnASchool'));

		//this post details of the new region to be created
		Route::post('region/create', array('as' => 'createRegion', 'uses' => 'DashboardController@createRegion'));
		//this returns the details of a region
		Route::get('region/{id}/profile', array('as' => 'regionProfile', 'uses' => 'DashboardController@showRegion'));
		//this get request show the form and details of the new user to be created
		Route::get('region/{id}/edit', array('as' => 'editRegion', 'uses' => 'DashboardController@editRegion'));
		//this post details of the updated user so that it can be saved
		Route::post('region/{id}/update', array('as' => 'updateRegion', 'uses' => 'DashboardController@updateRegion'));
		//this post the id of the user/admin to be deleted
		Route::post('region/{id}/delete', array('as' => 'deleteRegion', 'uses' => 'DashboardController@deleteRegion'));


		//this post details of the new district to be created
		Route::post('district/create', array('as' => 'createDistrict', 'uses' => 'DashboardController@createDistrict'));
			//this return the details of the login user
		Route::get('district/{id}/profile', array('as' => 'districtProfile', 'uses' => 'DashboardController@showDistrict'));
		//this get request show the form and details of the new user to be created
		Route::get('district/{id}/edit', array('as' => 'editDistrict', 'uses' => 'DashboardController@editDistrict'));
		//this post details of the updated user so that it can be saved
		Route::post('district/{id}/update', array('as' => 'updateDistrict', 'uses' => 'DashboardController@updateDistrict'));
		//this post the id of the user/admin to be deleted
		Route::post('district/{id}/delete', array('as' => 'deleteDistrict', 'uses' => 'DashboardController@deleteDistrict'));


		//this post details of the new circuit to be created
		Route::post('circuit/create', array('as' => 'createCircuit', 'uses' => 'DashboardController@createCircuit'));
		//this return the details of the login user
		Route::get('circuit/{id}/profile', array('as' => 'circuitProfile', 'uses' => 'DashboardController@showCircuit'));
		//this get request show the form and details of the new user to be created
		Route::get('circuit/{id}/edit', array('as' => 'editCircuit', 'uses' => 'DashboardController@editCircuit'));
		//this post details of the updated user so that it can be saved
		Route::post('circuit/{id}/update', array('as' => 'updateCircuit', 'uses' => 'DashboardController@updateCircuit'));
		//this post the id of the user/admin to be deleted
		Route::post('circuit/{id}/delete', array('as' => 'deleteCircuit', 'uses' => 'DashboardController@deleteCircuit'));


		Route::post('/create_school', array('as' => 'create_school', 'uses' => 'SchoolController@store'));
		//this return the details of the school
		Route::get('school/{id}/profile', array('as' => 'schoolProfile', 'uses' => 'SchoolController@showSchool'));
		//this get request show the form and details of the new user to be created
		Route::get('school/{id}/edit', array('as' => 'editSchool', 'uses' => 'SchoolController@editSchool'));
		//this post details of the updated user so that it can be saved
		Route::post('school/{id}/update', array('as' => 'updateSchool', 'uses' => 'SchoolController@updateSchool'));
		//this post the id of the user/admin to be deleted
		Route::post('school/{id}/delete', array('as' => 'deleteSchool', 'uses' => 'SchoolController@deleteSchool'));

		//routes for retrieving data collectors  i.e circuit supervisors and headteachers
		//this return data on a selected region
		Route::get('ghana/data_collectors', array('as' => 'all_data_collectors', 'uses' => 'DataCollectorsController@all'));
		Route::get('ghana/data_collectors/{type}', array('as' => 'national_supervisors_headteachers', 'uses' => 'DataCollectorsController@nationalSupervisorsOrHeadteachers'));
		Route::get('ghana/data_collectors/{type}/{id}', array('as' => 'national_supervisors_headteachers', 'uses' => 'DataCollectorsController@oneSupervisorsOrHeadteachers'));
		Route::get('ghana/data_collectors/regions/{region}', array('as' => 'regional_data_collectors', 'uses' => 'DataCollectorsController@allRegionalDataCollectors'));
		Route::get('ghana/data_collectors/regions/{region}/{type}', array('as' => 'regional_supervisors_headteachers', 'uses' => 'DataCollectorsController@regionalSupervisorsOrHeadteachers'));
		Route::get('ghana/data_collectors/districts/{district}', array('as' => 'district_data_collectors', 'uses' => 'DataCollectorsController@allDistrictDataCollectors'));
		Route::get('ghana/data_collectors/districts/{district}/{type}', array('as' => 'district_supervisors_headteachers', 'uses' => 'DataCollectorsController@districtSupervisorsOrHeadteachers'));
		Route::get('ghana/data_collectors/circuits/{circuit}', array('as' => 'circuit_data_collectors', 'uses' => 'DataCollectorsController@allCircuitDataCollectors'));
		Route::get('ghana/data_collectors/circuits/{circuit}/{type}', array('as' => 'circuit_supervisors_headteachers', 'uses' => 'DataCollectorsController@circuitSupervisorsOrHeadteachers'));
		Route::get('ghana/data_collectors/schools/{school}', array('as' => 'school_data_collectors', 'uses' => 'DataCollectorsController@allSchoolDataCollectors'));
		Route::get('ghana/data_collectors/schools/{school}/{type}', array('as' => 'school_supervisor_headteacher', 'uses' => 'DataCollectorsController@schoolSupervisorOrHeadteacher'));
		Route::post('resend/data_collectors/login/details/{firstName}/{identityCode}/{number}', array('as' => 'resend_data_collectors_loing', 'uses' => 'DataCollectorsController@ToResendDataCollectorsLoginDetails'));

		//this post details of the new user to be created
		Route::post('user/create', array('as' => 'createUser', 'uses' => 'UsersController@store'));
		//this return the details of the login user
		Route::get('user/profile', array('as' => 'userProfile', 'uses' => 'UsersController@showProfile'));
		//this return the details of the login user
		Route::get('user/other_users/{id}/profile', array('as' => 'userOthersProfile', 'uses' => 'UsersController@showOtherUserProfile'));
		//this get request show the form and details of the new user to be created
		Route::get('user/{id}/edit', array('as' => 'editUser', 'uses' => 'UsersController@editUser'));
		//this post details of the updated user so that it can be saved
		Route::post('user/update', array('as' => 'updateUser', 'uses' => 'UsersController@updateUser'));

		Route::post('user/password/change', array('as' => 'update_user_password', 'uses' => 'UsersController@editUserPassword'));
		//this post the id of the user/admin to be deleted
		Route::post('user/{id}/delete', array('as' => 'deleteUser', 'uses' => 'UsersController@deleteUser'));


		//this route return all data i.e regions, districts and circuits
		Route::get('ghana', array('as' => 'nationalData', 'uses' => 'DashboardController@allData'));
		//this returns data on all regions
		Route::get('ghana/regions', array('as' => 'allRegions', 'uses' => 'DashboardController@allRegions'));
		//this return data on all districts
		Route::get('ghana/districts', array('as' => 'allDistricts', 'uses' => 'DashboardController@allDistricts'));
		//this return data on all circuits
		Route::get('ghana/circuits', array('as' => 'allCircuits', 'uses' => 'DashboardController@allCircuits'));
		//this return data on all schools
		Route::get('ghana/schools', array('as' => 'allSchools', 'uses' => 'DashboardController@allSchools'));

		//this return data on a selected region
		Route::get('ghana/regions/{id}', array('as' => 'region', 'uses' => 'DashboardController@regionInfo'));
		//this return data on a selected district
		Route::get('ghana/districts/{id}', array('as' => 'district', 'uses' => 'DashboardController@districtInfo'));
		//this return data on a selected circuit
		Route::get('ghana/circuits/{id}', array('as' => 'circuit', 'uses' => 'DashboardController@circuitInfo'));
		//this return data on a selected school
		Route::get('ghana/schools/{id}', array('as' => 'school', 'uses' => 'DashboardController@schoolInfo'));

		//This is the resource for Data Collectors
		Route::resource('data_collectors','DataCollectorsController',array('only' => array('store', 'show','edit','update','destroy')));
		//this return the details of the selected data collector
		Route::get('data_collector/{id}/profile', array('as' => 'datacollectorProfile', 'uses' => 'DataCollectorsController@showDataCollector'));
		//this get request show the form and details of the data collector to be edited
		Route::get('data_collector/{id}/edit', array('as' => 'editDataCollector', 'uses' => 'DataCollectorsController@editDataCollector'));
		//this post details of the updated data collector so that it can be saved
		Route::post('data_collector/{id}/update', array('as' => 'updateDataCollector', 'uses' => 'DataCollectorsController@updateDataCollector'));
		//this post the id of the data collector to be deleted 
		Route::post('data_collector/{id}/delete', array('as' => 'deleteDataCollector', 'uses' => 'DataCollectorsController@deleteDataCollector'));
		Route::get('data_collector/{id}/detail_and_schools', array('as' => 'data_collector_and_schools', 'uses' => 'DataCollectorsController@dataCollectorDetailsAndSchool'));

		//this post post details of new itinerary to be create for district
		Route::post('district/itinerary', array('as' => 'createItinerary', 'uses' => 'ItinararyController@createItinerary'));
		//this returns all itinerary associated with a district
		//Route::get('district/all/itinerary', array('as' => 'listDistrictItinerary', 'uses' => 'ItinararyController@allDistrictItinerary'));
		//show an itinerary
		Route::get('district/itinerary/{id}/details', array('as' => 'itineraryDetails', 'uses' => 'ItinararyController@showItinerary'));
		//this get request show the form and details of the data collector to be edited
		Route::get('district/itinerary/{id}/edit', array('as' => 'editItinerary', 'uses' => 'ItinararyController@editItinerary'));
		//this post details of the updated data collector so that it can be saved
		Route::post('district/itinerary/{id}/update', array('as' => 'updateItinerary', 'uses' => 'ItinararyController@updateItinerary'));
		//this post the id of the data collector to be deleted 
		Route::post('district/itinerary/{id}/delete', array('as' => 'deleteItinerary', 'uses' => 'ItinararyController@deleteItinerary'));


		//this post details of the updated data collector so that it can be saved
		Route::get('national/school_report/grant', array('as' => 'nationa_grants', 'uses' => 'SchoolReportCardController@allSchoolsReportCardData'));




		//BEGINING OF ROUTES ON RETRIEVING SCHOOL VISITS DATA

		//this returns all data (answers on all questions under school visits)
		Route::get('schools/school_visits', array('as' => 'national_school_visits', 'uses' => 'SchoolVisitController@retrieveSchoolVisitsData'));

		//END OF ROUTES ON RETRIEVING SCHOOL VISITS DATA




		//BEGINING OF ROUTES IN RETRIEVING SCHOOL REVIEW DATA FOR ANALYSIS
		//this returns the school school school review data for all schools depending on the geography i.e national,regional,district circuit and school
		//make a post request with the keys matching the fields in the database you want to retrieve information
		Route::get('school_review/inclusiveness', array('as' => 'school_inclusiveness', 'uses' => 'SchoolReviewAnalysisController@schoolsInclusiveness'));
		Route::get('school_review/effective_teaching', array('as' => 'effective_teaching_learning', 'uses' => 'SchoolReviewAnalysisController@EffectiveTeachingAndLearning'));
		Route::get('school_review/healthy_level', array('as' => 'healthy_level', 'uses' => 'SchoolReviewAnalysisController@HealthyLevelOfSchools'));
		Route::get('school_review/friendly_schools', array('as' => 'friendly_schools', 'uses' => 'SchoolReviewAnalysisController@FriendlySchools'));
		Route::get('school_review/community_involvement', array('as' => 'community_involvement', 'uses' => 'SchoolReviewAnalysisController@CommunityInvolvementInSchool'));
		Route::get('school_review/safe_protective', array('as' => 'safe_protective', 'uses' => 'SchoolReviewAnalysisController@SafeAndProtectiveAndSchools'));
		//END OF ROUTES IN RETRIEVING SCHOOL REVIEW DATA FOR ANALYSIS

		//BEGINNIG OF ROUTES ON COMMUNITY SMS
		Route::get('general_public/sms', array('as' => 'general_public_sms', 'uses' => 'CommunityInvolvementController@QueryToReturnSmsSentByPublic'));
		Route::get('general/school/information', array('as' => 'general_school_info', 'uses' => 'SchoolController@RetrieveGeneralSchoolInformationToData'));

		////END OF ROUTES ON COMMUNITY SMS
		
	    //BEGINING OF ROUTES ON RETRIEVING SCHOOL REPORT CARD
	    Route::get('school_report_card/submissions/timelines', 'SchoolReportCardController@RetrieveWeeksTermsYearsThatDataHasBeenSubmittedOn'); //this returns the weeks, terms and years data has been submitted on for a school
		//this returns the school class enrolment for all schools depending on the geography i.e national,regional,district circuit and school
		//make a post request with the keys matching the fields in the database you want to retrieve information
		Route::get('school_report_card/enrollment', array('as' => 'school_enrolment_data', 'uses' => 'SchoolReportCardController@schoolsReportCardEnrolmentData'));
		Route::get('school_report_card/attendance', array('as' => 'school_reports_student_attendance', 'uses' => 'SchoolReportCardController@schoolsReportCardStudentsAttendanceData'));
		Route::get('school_report_card/special_enrollment', array('as' => 'school_special_enrolment_data', 'uses' => 'SchoolReportCardController@schoolsReportCardSpecialEnrolmentData'));
		Route::get('school_report_card/special_students_attendance', array('as' => 'school_special_student_attendance', 'uses' => 'SchoolReportCardController@schoolsReportCardSpecialStudentsAttendanceData'));
		Route::get('school_report_card/records_performance', array('as' => 'school_records_performance', 'uses' => 'SchoolProblemsController@stateOfSchoolRecordsAndPerformance'));

		Route::get('school_report_card/sanitation', array('as' => 'school_sanitition', 'uses' => 'SchoolProblemsController@stateOfSchoolSanitation'));
		Route::get('school_report_card/security', array('as' => 'school_decurity', 'uses' => 'SchoolProblemsController@stateOfSchoolSecurity'));
		Route::get('school_report_card/recreation_equipment', array('as' => 'school_recreation_equipment', 'uses' => 'SchoolProblemsController@stateOfSchoolRecreationalEquipment'));
		Route::get('school_report_card/structure', array('as' => 'school_structure', 'uses' => 'SchoolProblemsController@stateOfSchoolStructure'));
		Route::get('school_report_card/furniture', array('as' => 'school_furniture', 'uses' => 'SchoolProblemsController@stateOfSchoolFurniture'));
		Route::get('school_report_card/community_relations', array('as' => 'community_relations', 'uses' => 'SchoolProblemsController@stateOfSchoolAndCommunityRelations'));
		
		Route::get('school_report_card/school/teachers', array('as' => 'school_teachers', 'uses' => 'SchoolReportCardController@schoolsReportCardSchoolTeachers'));
		//Create tachers via forms
		Route::post('school_report_card/form/create/teacher', array('as' => 'form_create_teacher', 'uses' => 'TeacherController@createTeacherViaForm'));
		//create teachers via CSV
		Route::post('school_report_card/csv/upload/teachers', array('as' => 'csv_create_teachers', 'uses' => 'TeacherController@createTeacherViaCSVUpload'));

		Route::post('school_report_card/teacher/delete', array('as' => 'csv_create_teachers', 'uses' => 'TeacherController@deleteTeacherFromDashboard'));
		
		Route::post('school_report_card/teacher/edit', array('as' => 'edit_teachers', 'uses' => 'TeacherController@editTeacherFromDashboard'));

		Route::post('school_report_card/teacher/transfer', array('as' => 'transfer_teacher_to_a_new_school', 'uses' => 'TeacherController@transferTeacherToANewSchool'));


		Route::post('school_report_card/average/teacher/attendance', array('as' => 'average_teacher_attendance', 'uses' => 'TeacherController@getTheAverageTeacherAttendanceForASchool'));


		

		Route::get('school_report_card/pupils/performance', array('as' => 'pupils_performance', 'uses' => 'SchoolReportCardController@schoolsReportCardPupilsPerformance'));
		Route::get('school_report_card/school/support_types', array('as' => 'school_support_type', 'uses' => 'SchoolReportCardController@schoolsReportCardSchoolSupportType'));
		Route::get('school_report_card/grant_capitation_payments', array('as' => 'grant_capitation_payments', 'uses' => 'SchoolReportCardController@schoolsReportCardSchoolCapitationGrantsPayments'));
		Route::get('school_report_card/school/textbooks', array('as' => 'school_textbooks', 'uses' => 'SchoolReportCardController@schoolsReportCardSchoolClassTextBooks'));
		
		Route::get('school_report_card/school/punctuality_lesson_plan', array('as' => 'school_punctuality', 'uses' => 'SchoolReportCardController@stateOfSchoolPunctuality'));
		Route::get('school_report_card/school/teacher_class_management', array('as' => 'school_management', 'uses' => 'SchoolReportCardController@stateOfSchoolManagement'));
		Route::get('school_report_card/school/teacher/{id}/punctuality_lesson_plan', array('as' => 'teacher_punctuality_lesson_plan', 'uses' => 'SchoolReportCardController@stateOfSchoolTeacherPunctualityAndLessonPlan'));
		Route::get('school_report_card/school/teacher/{id}/class_management', array('as' => 'teacher_class_management', 'uses' => 'SchoolReportCardController@stateOfSchoolTeacherClassManagement'));
		Route::get('school_report_card/school/units/covered', array('as' => 'units_covered', 'uses' => 'SchoolReportCardController@stateOfSchoolUnitsCoveredByTeachers'));
		Route::get('school_report_card/school/meetings', array('as' => 'school_meetings', 'uses' => 'SchoolReportCardController@retrieveStateOfSchoolsMeetings'));

		Route::get('school_report_card/school/general_situation', array('as' => 'school_general_situation', 'uses' => 'SchoolReportCardController@retrieveGeneralSchoolSituationData'));

		Route::get('school/sms/codes', array('as' => 'school_sms_codes', 'uses' => 'DashboardController@retrieveSchoolSMSCodes'));
		Route::get('schools/images', array('as' => 'schools_images', 'uses' => 'ImageProcessingController@ReturnImagesBaseOnAQuery'));



		//END OF ROUTES ON SCHOOL SCHOOL REPORT CARD BY HEADMASTERS

		Route::post('upload/email/attachment', array('as' => 'upload_email_attachment', 'uses' => 'EmailController@uploadEmailAttachment'));
		Route::post('send/Email', array('as' => 'send_email', 'uses' => 'EmailController@sendEmailWithAttachment'));
		Route::post('remove/file/uploaded', array('as' => 'remove_file_uploaded', 'uses' => 'EmailController@RemoveFileUploaded'));
		Route::post('/sendMessageAsNotification', array('as' => 'send_message_as_notification', 'uses' => 'NotificationsController@sendMessageViaNotificationSystem'));
		Route::get('/retrieveMessagesSentByDataCollectors', array('as' => 'retrieve_messages', 'uses' => 'NotificationsController@showAllMessagesSentBackByDataCollecttorsAsAResponseToAMessageSentViaNotification'));


		Route::get('/totals/weekly/data', array('as' => 'totals_weekly_data', 'uses' => 'SchoolReportCardController@RetrieveTotalsOnWeeklySubmittedData'));
		Route::get('/totals/termly/data', array('as' => 'totals_termly_data', 'uses' => 'SchoolReportCardController@RetrieveTotalsOnTermlySubmittedData'));


		Route::get('/retrieve/weekly/submissions/period/details', array('as' => 'weekly_submissions_periods', 'uses' => 'DashboardController@gettingWeeklyPeriodsDataIsSubmittedUnder'));

		Route::get('/retrieve/weekly/submissions/under/school', array('as' => 'weekly_submissions_periods_for_school', 'uses' => 'DashboardController@gettingWeeklyPeriodsDataIsSubmittedUnderForEachSchool'));

		Route::get('/retrieve/termly/submissions/period/details', array('as' => 'termly_submissions_periods', 'uses' => 'DashboardController@gettingTermlyPeriodsDataIsSubmittedUnder'));



		//Beginning of routes for data analysis
	    Route::get('/student/enrolment/totals/for/any/level', array('as' => 'student_enrolment_total', 'uses' => 'AnalysisController@calculateStudentEnrolmentTotalsForAllLevels'));
	    Route::get('/student/attendance/totals/for/any/level', array('as' => 'student_attendance_total', 'uses' => 'AnalysisController@calculateStudentAttendanceTotalsForAllLevels'));

	    Route::get('/average/teacher/attendance/percentage', array('as' => 'average_teacher_attendance', 'uses' => 'AnalysisController@percentageAnalysisOfAverageTeacherAttendance'));
	    Route::get('/teachers/whos/attendance/submitted', array('as' => 'teachers_attendance_submitted', 'uses' => 'AnalysisController@findTeachersWhosAttendanceHasNotBeenSubmmitted'));
        Route::post('/update/teacher/status', array('as' => 'update_teacher_status', 'uses' => 'TeacherController@updateTeacherStatus'));



        Route::post('/update/weekly/totals', array('as' => 'update_weekly_totals', 'uses' => 'AnalysisController@UpdateTotalWeeklyTable'));


		//Errors Reporting and Retrieving on the server side
		Route::post('dashboard/errors', array('as' => 'dashboard_erros', 'uses' => 'AppErrorsReportingController@dashboardReportedErrors'));
		Route::get('retrieve/errors', array('as' => 'retrieve_erros', 'uses' => 'AppErrorsReportingController@retrieveErrors'));


		//Begining of Deleting Data submitted by data collectors
		Route::post('delete/enrolment/attendance', array('as' => 'delete_enrolment_attendance_data', 'uses' => 'SchoolController@deleteAnEnrolmentOrAttendanceData'));
		Route::post('delete/teacher/data/submitted', array('as' => 'delete_teacher_attendance_classmanagement', 'uses' => 'TeacherController@deleteTeacherAttendanceAndClassManagementForAPeriod'));
		

		Route::post('delete/textbooks/data', array('as' => 'delete_textbooks_data', 'uses' => 'TermlyDataProcessing@deleteTextBooksSubmittedData'));
		Route::post('delete/pupils/performance/data', array('as' => 'delete_pupils_performance_data', 'uses' => 'TermlyDataProcessing@deletePupilsPerformanceSubmittedData'));

		Route::post('delete/record/books', array('as' => 'delete_record_books', 'uses' => 'TermlyDataProcessing@deleteRecordBooksSubmittedData'));
		Route::post('delete/support/type/data', array('as' => 'delete_support_type_data', 'uses' => 'TermlyDataProcessing@deleteSupportTypeSubmittedData'));
		Route::post('delete/grant/data', array('as' => 'delete_grant_data', 'uses' => 'TermlyDataProcessing@deleteGrantSubmittedData'));


		Route::post('delete/sanitation/data', array('as' => 'delete_sanitation_data', 'uses' => 'TermlyDataProcessing@deleteSanitationSubmittedData'));
		Route::post('delete/security/data', array('as' => 'delete_security_data', 'uses' => 'TermlyDataProcessing@deleteSecuritySubmittedData'));
		Route::post('delete/sports/recreational/data', array('as' => 'delete_sports_recreational', 'uses' => 'TermlyDataProcessing@deleteSportsAndRecreationalSubmittedData'));
		Route::post('delete/school/structure/data', array('as' => 'delete_school_structure_data', 'uses' => 'TermlyDataProcessing@deleteSchoolStructureSubmittedData'));
		Route::post('delete/furniture/data', array('as' => 'delete_furniture_data', 'uses' => 'TermlyDataProcessing@deleteFurnitureSubmittedData'));


		Route::post('delete/community/relation/data', array('as' => 'delete_community_relation_data', 'uses' => 'TermlyDataProcessing@deleteCommunityRelationsSubmittedData'));
		Route::post('delete/meetings/held/data', array('as' => 'delete_meetings_held_data', 'uses' => 'TermlyDataProcessing@deleteMeetingsHeldSubmittedData'));
		Route::post('delete/general/issues/data', array('as' => 'delete_general_issues_data', 'uses' => 'TermlyDataProcessing@deleteGeneralIssuesSubmittedData'));
		//End of Deleting Data submitted by data collectors


		//Getting the various crud actions carried out by admin
		Route::get('created/actions', array('as' => 'retrieve_created_actions', 'uses' => 'DashboardController@getAllCreatedActionsByAdmins'));
		Route::get('edited/actions', array('as' => 'retrieve_edited_actions', 'uses' => 'DashboardController@getAllEditedActionsByAdmins'));
		Route::get('deleted/actions', array('as' => 'retrieve_deleted_actions', 'uses' => 'DashboardController@getAllDeletedActionsByAdmins'));
		

		Route::post('/send/sms/message/from/dashboard', array('as' => 'send_sms/_message_from_dashboard', 'uses' => 'DashboardController@sendSMSMessageFromDashboard'));

		Route::get('/retrieve/sms/messages', array('as' => 'retrieve_cs_sent_sms_messages', 'uses' => 'CSCommentsController@retrieveCSSentSMSMessages'));

	    Route::get('/retrieve/parametric/aggregate/data', array('as' => 'retrieve_aggregate_data', 'uses' => 'SchoolReportCardController@RetrievingAggregateDataForEnrolmentAndAttendance'));

	    //Update the weekly_totals table with this
	    Route::post('/update/weekly/table/enrollment', array('as' => 'update_enrollment_weekly_total', 'uses' => 'AnalysisController@updateTotalEnrollmentInWeeklyTable'));


         //Update the weekly_totals table with this
	    Route::post('/update/weekly/table/attendance', array('as' => 'update_attendance_weekly_total', 'uses' => 'AnalysisController@updateTotalAttendanceInWeeklyTable'));

	    Route::post('/update/weekly/table/for/teachers/attendance', array('as' => 'update_teachers_attendance_weekly_total', 'uses' => 'AnalysisController@updateTotalTeacherAttendanceInWeeklyTable'));


		Route::post('/school_report_card/school/class_textbooks/update/totals', array('as' => 'update_school_class_textbooks_totals', 'uses' => 'AnalysisController@UpdateTotalsOfTextBooksForASchoolInATerm'));

		Route::post('/school_report_card/school/pupil_performance/update/totals', array('as' => 'update_school_pupil_performance_totals', 'uses' => 'AnalysisController@UpdateTotalsOfPupilsPerformanceForASchoolInATerm'));

		Route::get('/school_report_crd/school/required/images', array('as' => 'school_required_images', 'uses' => 'ImageProcessingController@ReturnRequiredImagesBaseOnAQuery'));


		//This is the route group for the STARS PROJECT
        Route::group(array('prefix' => 'targeted/instructions'), function () {
            Route::post('/assign/school/to/group', array('as' => 'assign_school_to_group', 'uses' => 'TargetedInstructionsController@AssignASchoolToATargetInstructionGroup'));
            Route::get('/attendance', array('as' => 'targeted_attendance', 'uses' => 'TargetedInstructionsController@retrieveTargetedAttendanceWithDynamicQueries'));

            Route::post('/upload/elibrary/file', array('as' => 'upload_elibrary_file', 'uses' => 'TargetedInstructionsController@uploadELibraryFile'));
            Route::get('/elibrary/files', array('as' => 'elibrary_files', 'uses' => 'TargetedInstructionsController@retrieveElibraryFilesWithDynamicQueries'));





        });


});




//this route takes the posted data to login a user
//Route::post('/login', 'UsersController@login');
Route::post('/login', array('as' => 'login', 'uses' => 'UsersController@login'));

Route::get('/login', function()
{
    return View::make('login');
});

//this route logs out  a user
//Route::get('/logout', 'UsersController@logout');
Route::get('/logout', array('as' => 'logout', 'uses' => 'UsersController@logout'));


Route::post('/cs/dashboard/authentication', array('as' => 'cs_dashboard_authentication', 'uses' => 'CSCommentsController@csDashboardAuthentication'));


Route::group(array('prefix' => 'api'), function () {
		
		Route::post('/data_collectors/headteacher/details/update', array('as' => 'update_headteacher_details', 'uses' => 'DataCollectorsController@updateHeadTeacherDetailsWhoIsAlsoADataCollector'));

		Route::post('/schools/info/update', array('as' => 'update_school_details', 'uses' => 'SchoolController@updateSchoolInfoViaAndroidAppByADataCollector'));
		
		//getting curent school rating
		Route::post('/school/current/status', array('as' => 'school_current_status', 'uses' => 'SchoolController@androidRequestToGetSchoolCurrentPerformanceStatus'));

		//this takes the posted details of the data collector for authentication and subsequent login
		Route::post('/data_collectors/login', array('as' => 'data_collectors_login', 'uses' => 'DataCollectorsController@apiLogin'));
		//this takes the posted details of the data collector for authentication and subsequent login
		Route::post('/data_collectors/schools', array('as' => 'data_collectors_schools', 'uses' => 'DataCollectorsController@DataCollectorsSchoolsToReportOn'));
		
		
		//this gets the data on a particular school/review visit
		Route::post('/school_visits/data', 'SchoolVisitController@ReceivesSchoolVisitsDataSubmittedByAndroidApp');
		Route::post('/school_review/data', 'SchoolReviewController@ReceivesSchoolReviewDataSubmittedByAndroidApp');


		//this post the data on a particular school report card records
		Route::post('/school_report_card/school/teacher_registration', array('as' => 'school_report_card_teacher_registration', 'uses' => 'TeacherController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnTeacherRegistration'));
		Route::post('/school_report_card/school/teacher_registration/internet/detected', array('as' => 'school_report_card_teacher_registration_on_internet_detection', 'uses' => 'TeacherController@RegisterTeachersByAndroidOnInternetDetection'));


		Route::post('data_collector/created/teachers', array('as' => 'listDataCollectorTeachers', 'uses' => 'TeacherController@apiRequestForDataCollectorTeachersForASchool'));
		//this request for all teachers of a school
		Route::post('school_report_card/teachers/edit', array('as' => 'school_teachers_edit', 'uses' => 'TeacherController@editTeacherInformationThroughAndroidAPP'));
		Route::post('school_report_card/teachers/delete', array('as' => 'school_teachers_delete', 'uses' => 'TeacherController@deleteTeacherThroughAndroidAPP'));
		

		//this post the data on a particular school report card records
		Route::post('/school_report_card/data/enrollment', array('as' => 'school_report_card_data_enrolment', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEnrolment'));
		Route::post('/school_report_card/data/special/enrollment', array('as' => 'school_report_card_data_special_enrolment', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsEnrolment'));

		//this post the data on a particular school report card records
		Route::post('/school_report_card/data/class_attendance', array('as' => 'school_report_card_class_attendance', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassAttendance'));
		Route::post('/school_report_card/data/special/class_attendance', array('as' => 'school_report_card_special_class_attendance', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsClassAttendance'));
		Route::post('/school_report_card/data/sanitation', array('as' => 'school_report_card_sanitation', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSanitation'));
		Route::post('/school_report_card/data/security', array('as' => 'school_report_card_security', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSecurity'));
		Route::post('/school_report_card/data/recreational_equipment', array('as' => 'school_report_card_recreational_equipment', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEquipmentAndRecreation'));
		Route::post('/school_report_card/data/structure', array('as' => 'school_report_card_structure', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolStructure'));
		Route::post('/school_report_card/data/furniture', array('as' => 'school_report_card_furniture', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnFurniture'));
		Route::post('/school_report_card/data/records_performance', array('as' => 'school_report_card_records_performance', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnRecordAndPerformance'));
		Route::post('/school_report_card/data/community_relations', array('as' => 'school_report_card_community_relations', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnCommunityRelations'));
		Route::post('/school_report_card/data/punctuality_lesson_plan', array('as' => 'school_report_card_punctuality_lesson_plan', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPunctualityAndLessons'));
		Route::post('/school_report_card/data/class_management', array('as' => 'school_report_card_class_management', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassManagement'));
		Route::post('/school_report_card/teacher/units_covered', array('as' => 'teacher_units_covered', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberUnitsCoveredByTeacher'));

	
		Route::post('/school_report_card/pupils/performance', array('as' => 'pupils_performance', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPupilsPerformance'));
		Route::post('/school_report_card/school/support_types', array('as' => 'support_types', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolSupportTypes'));
		Route::post('/school_report_card/school/grants_capitation_payments', array('as' => 'school_grants_capitation_payments', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolGrantCapitationPayments'));
		Route::post('/school_report_card/school/class_textbooks', array('as' => 'school_class_textbooks', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberOfTextBooks'));


		Route::post('/school_report_card/data/general_school_situation', array('as' => 'general_school_situation', 'uses' => 'SchoolReportCardController@ReceiveDataOnGeneralSchoolSituation'));

		Route::post('/school_report_card/data/meetings', array('as' => 'school_meetings', 'uses' => 'SchoolReportCardController@ReceivesSchoolReportCardDataSubmittedByAndroidAppOnMeetings'));
		//this takes sms details by the community (public) forwarded by infobip
		Route::post('/community/sms', array('as' => 'community_sms', 'uses' => 'CommunityInvolvementController@smsReceived'));
		//this takes sms details by the community (public) forwarded by infobip
		Route::get('/community/sms', array('as' => 'community_sms', 'uses' => 'CommunityInvolvementController@smsReceived'));

		//this returns all itinerary associated with a district for the android api
		Route::post('district/all/itinerary', array('as' => 'listDistrictItinerary', 'uses' => 'ItinararyController@apiRequestForAllDistrictItinerary'));
		//Route::resource('inclusiveness', 'InclusivenessController',array('only' => array('index','store', 'show','edit','update','destroy')));
		Route::post('info/response', array('as' => 'receive_response_to_info', 'uses' => 'NotificationsController@ReceiveResponseOnInformationSentViaNotification'));
        //Route::get('/logout', 'UsersController@logout');
        //Storing DeviceIDs for Notifications
		Route::post('notification/device/register', array('as' => 'registerDeviceID', 'uses' => 'NotificationsController@registerDeviceIDFromAndroidAndSaveDetails'));
		
		Route::post('school/data/image', array('as' => 'school_image', 'uses' => 'ImageProcessingController@receiveImagePostedByAndroidApp'));




		// This route is for the aggregate submission of the weekly data under school report card
		Route::post('/school_reportcard/weekly/aggregate/submission', array('as' => 'weekly_aggregate_submission', 'uses' => 'SchoolReportCardController@ReceivesWeeklyAggregateSubmision'));

		// This route is for the aggregate submission of the termly data under school report card
		Route::post('/school_reportcard/termly/aggregate/submission', array('as' => 'termly_aggregate_submission', 'uses' => 'SchoolReportCardController@ReceivesTermlyAggregateSubmision'));


		//All routes in this group are for circuit supervisor dashboard
		
		// This route is for saving the comments made by the circuit supervisor on data submitted
		Route::post('/cs/comments/on/headteacher/submission', array('as' => 'cs_comments_on_headteacher_submission', 'uses' => 'CSCommentsController@saveACSCommentOnSubmittedData'));


		Route::post('/all/headteachers/in/a/circuit', array('as' => 'all_headteachers_in_a_circuit', 'uses' => 'TeacherController@allHeadTeachersInACircuit'));

		// This route is for saving the comments made by the circuit supervisor on data submitted
		Route::post('/all/teachers/in/a/circuit', array('as' => 'all_teachers_in_a_circuit', 'uses' => 'TeacherController@allTeachersInACircuit'));

		Route::post('/weekly/data/reported/totals', array('as' => 'weekly_data_reported_totals', 'uses' => 'SchoolReportCardController@RetrieveTotalsOnWeeklySubmittedDataForCSDashboard'));

		Route::post('/termly/data/reported/totals', array('as' => 'termly_data_reported_totals', 'uses' => 'SchoolReportCardController@RetrieveTotalsOnTermlySubmittedDataForCSDashboard'));


	    Route::post('/retrieve/data/for/cs/dashboard', array('as' => 'retrieve_data_for_cs_dashboard', 'uses' => 'CSCommentsController@retrieveDataFromDifferentSectionForCSDashboard'));

	    Route::post('/retrieve/recent/submissions/for/cs/dashboard', array('as' => 'retrieve_recent_submissions_for_cs_dashboard', 'uses' => 'CSCommentsController@retrieveRescentSubmissionsForCSDashboard'));

      	Route::post('/save/cs/sent/sms/messages', array('as' => 'save_cs_sent_sms_messages', 'uses' => 'CSCommentsController@saveCSSentSMSMessages'));

      	Route::get('/retrieve/cs/sent/sms/messages', array('as' => 'retrieve_cs_sent_sms_messages', 'uses' => 'CSCommentsController@retrieveCSSentSMSMessages'));



        // This route is for submitting required images on school
        Route::post('/school_reportcard/required/images/submission', array('as' => 'required_images', 'uses' => 'ImageProcessingController@saveRequiredImageSubmittedViaAndroidApp'));



//        This is the route group for the STARS PROJECT
        Route::group(array('prefix' => 'targeted/instructions'), function () {
            Route::post('/save/attendance', array('as' => 'save_targeted_attendance', 'uses' => 'TargetedInstructionsController@saveTargetedAttendance'));
            Route::get('/school/elibrary/files', array('as' => 'school_elibrary_files', 'uses' => 'TargetedInstructionsController@retrieveElibraryFilesForASchool'));



            Route::group(array('prefix' => 'help/desk'), function () {
                Route::post('/make/request', array('as' => 'make_help_desk_request', 'uses' => 'HelpDeskController@createAHelpRequest'));
            });

        });


    /**
     * =============================================================================================================
     *
     * Created by Makedu-Consult : Mantey 19/01/2019
     *
     * Targeted Instructions Updates for STARS PROJECT
     *
     * =================================================================================================
     */

    Route::group(array('prefix' => 'ti'), function () {

        Route::post('/save/attendance', array('uses' => 'TargetedInstructionsController@saveAttendanceData'));
        Route::post('/save/pupil/aggregated', array('uses' => 'TargetedInstructionsController@savePupilAggregatedData'));
        Route::post('/save/class/observation', array('uses' => 'TargetedInstructionsController@saveClassObservationData'));
        Route::post('/save/answer', array('uses' => 'TargetedInstructionsController@saveAnswerData'));

    });


});



/**
 * =============================================================================================================
 *
 * Created by Makedu-Consult : Mantey 19/01/2019
 *
 * Targeted Instructions Frontend
 *
 * =================================================================================================
 */

Route::group(['middleware' => ['web']], function () {

    Route::resource('anayltics', 'MkAnalyticsController');

    Route::resource('classobservation', 'MkClassObservationController');
});

Route::post('/save/pupilaggregateddata', array('as' => 'save_pupilaggregateddata', 'uses' => 'SubmitFormController@submitForm'));
Route::post('/save/targetedinstruction/analytics/poll', array('as' => 'save_poll', 'uses' => 'SubmitAnalyticsPollFormController@submitForm'));
Route::post('/save/targetedinstruction/analytics/pollgraph',array('as' => 'save_pollgraph','uses'=> 'SubmitAnalyticsPollGraphFormController@filterPoll'));
Route::get('/get/targetedinstruction/analytics/poll/questions',array('as' => 'get_pollquestions','uses'=> 'SubmitAnalyticsPollGraphFormController@getPollQuestions'));
Route::post('/save/attendance', array('as' => 'save_attendance', 'uses' => 'SubmitAttendanceFormController@submitForm'));
Route::post('/save/PupilAggregatedDataCountryGraphForm', array('as' => 'save_PupilAggregatedDataCountryGraphForm', 'uses' => 'SubmitPupilAggregatedDataCountryGraphFormController@submitForm'));
Route::post('/save/PupilAggregatedDataRegionGraphForm', array('as' =>'save_PupilAggregatedDataRegionGraphForm', 'uses' => 'SubmitPupilAggregatedDataRegionGraphFormController@submitForm'));
Route::post('/save/PupilAggregatedDataSchoolGraphForm',array('as'=>'save_PupilAggregatedDataSchoolGraphForm', 'uses' => 'SubmitPupilAggregatedDataSchoolGraphFormController@submitForm'));
Route::post('/save/PupilAggregatedDataCircuitGraphForm',array('as'=>'save_PupilAggregatedDataCircuitGraphForm', 'uses' => 'SubmitPupilAggregatedDataCircuitGraphFormController@submitForm'));
Route::post('/save/PupilAggregatedDataDistrictGraphForm',array('as' =>'save_PupilAggregatedDataDistrictGraphForm', 'uses' =>'SubmitPupilAggregatedDataDistrictGraphFormController@submitForm'));
Route::post('/save/AttendanceCountryGraphForm', array('as' => 'save_AttendanceCountryGraphForm', 'uses' => 'SubmitAttendanceCountryGraphFormController@submitForm'));
Route::post('/save/AttendanceRegionGraphForm', array('as' => 'save_AttendanceRegionGraphForm', 'uses' => 'SubmitAttendanceRegionGraphFormController@submitForm'));
Route::post('/save/AttendanceSchoolGraphForm',array('as'=>'save_AttendanceSchoolGraphForm', 'uses' => 'SubmitAttendanceSchoolGraphFormController@submitForm'));
Route::post('/save/AttendanceCircuitGraphForm',array('as'=>'save_AttendanceCircuitGraphForm', 'uses' => 'SubmitAttendanceCircuitGraphFormController@submitForm'));
Route::post('/save/AttendanceDistrictGraphForm',array('as' =>'save_AttendanceDistrictGraphForm', 'uses' =>'SubmitAttendanceDistrictGraphFormController@submitForm'));
Route::post('/save/analytics', array('as' => 'save_analytics', 'uses' => 'SubmitForm3Controller@submitForm'));
Route::post('/save/class_observation', array('as' => 'save_classobservation', 'uses' => 'SubmitClassObservationFormController@submitForm'));
Route::get('/get/class_observation/teachers', array('as' => 'get_class_observation', 'uses' => 'SubmitClassObservationFormController@getTeachers'));
Route::get('/get/targetedinstruction/analytics/polllist/view', array('as' => 'get_poll','uses' => 'SubmitAnalyticsPollFormController@getPolls'));

Route::get('/export/targetedinstruction/polls', array('as' => 'export_polls', 'uses' => 'SubmitAnalyticsPollGraphFormController@exportPolls'));
Route::delete('/targetedinstruction/poll/destroy', 'SubmitAnalyticsPollFormController@destroy');

Route::post('/targetedinstruction/poll/update','SubmitAnalyticsPollFormController@update');


Route::get('/ti/dashboard', array('uses' => 'TargetedInstructionsController@tiDashboard'));
Route::post('/filter/attendance', array('as' => 'filter_attendance', 'uses' => 'TargetedInstructionsController@filterAttendance'));



Route::get('/total/lesson/observations', array('as' => 'lesson_observations', 'uses' => 'SubmitClassObservationFormController@totalLessons'));
Route::get('/ti/only/schools', array('as' => 'ti_schools', 'uses' => 'TargetedInstructionsController@allTISchools'));
Route::get('/ti/only/circuits', array('as' => 'ti_circuits', 'uses' => 'TargetedInstructionsController@allTICircuits'));
Route::get('/ti/only/districts', array('as' => 'ti_districts', 'uses' => 'TargetedInstructionsController@allTIDistricts'));

Route::get('/total/lesson/attendance',array('as' => 'lesson_attendance', 'uses' =>'SubmitClassObservationFormController@attendance'));
Route::get('/total/lesson/pupilaggregateddata',array('as' => 'lesson_pupilaggregateddata', 'uses' =>'SubmitClassObservationFormController@pupilaggregateddata'));








Route::group(array('prefix' => 'public/data'), function () {


	Route::get('/normal/enrolment', array('as' => 'public_normal_enrolment', 'uses' => 'PublicDataViewController@normalEnrolment'));

	Route::get('/normal/attendance', array('as' => 'public_normal_attendance', 'uses' => 'PublicDataViewController@normalAttendance'));

	Route::get('/special/enrolment', array('as' => 'public_special_enrolment', 'uses' => 'PublicDataViewController@specialEnrolment'));

	Route::get('/special/attendance', array('as' => 'public_special_attendance', 'uses' => 'PublicDataViewController@specialAttendance'));

	Route::get('/teachers/attendance', array('as' => 'public_teacher_attendance', 'uses' => 'PublicDataViewController@teacherAttendance'));

	Route::get('/school/textbooks', array('as' => 'public_school_textbooks', 'uses' => 'PublicDataViewController@textbooks'));

	Route::get('/school/pupils/performance', array('as' => 'public_public_performance', 'uses' => 'PublicDataViewController@pupilsPerformance'));

	Route::get('/teachers/class/management', array('as' => 'public_class_room_management', 'uses' => 'PublicDataViewController@classRoomManagement'));

	Route::get('/teachers/units/covered', array('as' => 'public_units_covered', 'uses' => 'PublicDataViewController@teacherUnitsCovered'));

	Route::get('/school/record/books', array('as' => 'public_school_record_books', 'uses' => 'PublicDataViewController@recordBooks'));

	Route::get('/school/support', array('as' => 'public_school_record_books', 'uses' => 'PublicDataViewController@support'));

	Route::get('/school/grants', array('as' => 'school_grants', 'uses' => 'PublicDataViewController@grant'));

	Route::get('/school/sanitation', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@sanitation'));

	Route::get('/school/security', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@security'));

	Route::get('/school/recreational', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@recreational'));

	Route::get('/school/school_structure', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@school_structure'));

	Route::get('/school/furniture', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@furniture'));

	Route::get('/school/community/relations', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@communityRelations'));

	Route::get('/school/meetings/held', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@meetingsHeld'));

	Route::get('/school/general/situation', array('as' => 'school_sanitition', 'uses' => 'PublicDataViewController@generalSchoolSituation'));
	    
});


	
Route::get('school/review/questions', array('as' => 'school_review', 'uses' => 'SchoolReviewController@schoolReviewQuestionsList'));



Route::post('/queues', function () {
	   return Queue::marshal();
   }
);

//this route takes all request for unavailable pages
App::missing(function($exception)
{
   return View::make('404_error');
});


/*Begining of Routes on MSRC APPLICATION ERRORS REPORTING */

Route::post('api/android/errors', array('as' => 'android_app_erros', 'uses' => 'AppErrorsReportingController@androidPostedErrors'));



/*Begining of Routes on MSRC APPLICATION ERRORS REPORTING */































// Hadis hack for pegasus routes, this is for converting xls to xml and uploading odk aggreagate
// 
// 
 $GLOBALS['message']  = 'wecome to the Pegasus xls to xml converter
                         This API wrapper converts your xls into odk aggregate conpartible 
                         xml.';
                         
Route::get('/pegasus/api',function(){

	return $GLOBALS['message'];

});


Route::get('/api/sendXLSForms', array('uses' => 'PegasusController@downloadXLSFileFromGdrive'));
