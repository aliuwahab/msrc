<?php

//use Illuminate\Http\Request;

class SubmitAttendanceFormController extends BaseController
{

    public function submitForm()
    {

        $data = json_decode(@file_get_contents('php://input'), true);

//        try{

        $user = Auth::user();
        $id = $user->id;
        $level_id = $user->level_id;

        $circuit = Circuit::where('id', $level_id)->first();

//        }
//        catch(Exception $e) {
//
//            echo 'Error Message 1: ' .$e->getMessage();
//
//        }


        $MkAttendance = new MkDataAttendance();
        $MkAttendance->data_collector_id = $id;
        $MkAttendance->attendance_academic_term = $data['attendance_academic_term'];
        $MkAttendance->attendance_academic_year = $data['attendance_academic_year'];
        $MkAttendance->attendance_academic_level = $data['attendance_academic_level'];
        $MkAttendance->attendance_academic_week = $data['attendance_academic_week'];
        $MkAttendance->attendance_no_pupil_level = $data['attendance_no_pupil_level'];
        $MkAttendance->attendance_no_pupil_avg = $data['attendance_no_pupil_avg'];
        $MkAttendance->attendance_no_teacher_avg = $data['attendance_no_teacher_avg'];
        $MkAttendance->attendance_academic_subject = $data['attendance_academic_subject'];

        $MkAttendance->circuit_id = $circuit->id;

            $MkAttendance->school_id = null;

            $MkAttendance->district_id = $circuit->district_id;

            $MkAttendance->country_id = $circuit->country_id;

            $MkAttendance->region_id = $circuit->region_id;

        if ($MkAttendance->save()) {
            return 1;
        }
        return 0;
    }
}