<?php
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

//use Illuminate\Http\Request;

class SubmitAnalyticsPollFormController extends BaseController {

    public function submitForm()
    {
        $request = json_decode(@file_get_contents('php://input'), true);

        $poll_title = $request['poll_title'];
        $poll_question = $request['poll_question'];

        $answer_one = array_key_exists("la_lesson_summary1", $request) ? $request['la_lesson_summary1'] : null;
        $answer_two = array_key_exists("la_lesson_summary2", $request) ? $request['la_lesson_summary2'] : null;
        $answer_three = array_key_exists("la_lesson_summary3", $request) ? $request['la_lesson_summary3'] : null ;

        $answers = array();
        if($answer_one!=null || !empty($answer_one)) {
            array_push($answers, $answer_one);
        }

        if($answer_two!=null || !empty($answer_two)) {
            array_push($answers, $answer_two);
        }

        if($answer_three!=null || !empty($answer_three)) {
            array_push($answers, $answer_three);
        }

        //Save polls questions in table
        $poll_id = DB::table('mk_questions')
            ->insertGetId(
                array(
                    'poll_title' => $poll_title,
                    'poll_question' => $poll_question,
                    'answers' =>serialize($answers),
                    'created_at' => date("Y-m-d H:i"),
                    'updated_at' => date("Y-m-d H:i")
                ));

        //API KEY for Sending GCM Notification
        define('API_ACCESS_KEY', 'AIzaSyB_I1EzjJtNy38rEkRDF9bAc0NNlC2hB-4');

        $result = array(
            'success' => 1,
        );

        $page = 1;
        $row_count = 500;

        do {
            $offset = ($page - 1) * $row_count;
            $gcm_ids = DB::table('gcm_ids')->select('id', 'gcm_id')->take($row_count)->skip($offset)
                ->orderBy('id', 'DESC')->get();

            $registration_ids = array();

            foreach ($gcm_ids as $key => $gcm_id) {
                if($gcm_id->gcm_id!=null && !empty($gcm_id->gcm_id)) {
                    array_push($registration_ids, $gcm_id->gcm_id);
                }
            }

            if (!empty($registration_ids) && count($registration_ids) > 0) {

                $data = array(
                    'poll_id' => $poll_id,
                    'poll_title' => $poll_title,
                    'question_type' => "multiple_choice", //multiple_choice, straight_answers
                    'question' => $poll_question,
                    'answers' => $answers,
                    'timestamp' => date('Y-m-d H:i:s')
                );

                $fields = array(
                    'registration_ids' => $registration_ids,
                    'data' => ["notification_data" => $data]
                );

                $headers = array
                (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                curl_setopt($ch, CURLOPT_POST, true);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_exec($ch);

            }

            $page++;

        } while(count($gcm_ids) != 0);

        curl_close($ch);

        return $result;













        //Get All the notification registration id from gcm_ids table as array
//        $registration_ids = array();
//        $gcm_ids = DB::table('gcm_ids')
//                    ->select('gcm_id')
////                    ->where('phone_number', '=', '+233501476081')
//                    ->get();
//
//        foreach($gcm_ids as $gcm_id) {
//            if($gcm_id->gcm_id!=null && !empty($gcm_id->gcm_id)) {
//                array_push($registration_ids, $gcm_id->gcm_id);
//            }
//        }
//
//        define('API_ACCESS_KEY', 'AIzaSyB_I1EzjJtNy38rEkRDF9bAc0NNlC2hB-4');
//
//        $data = array(
//            'poll_id' => $poll_id,
//            'poll_title' => $poll_title,
//            'question_type' => "multiple_choice", //multiple_choice, straight_answers
//            'question' => $poll_question,
//            'answers' => $answers,
//            'timestamp' => date('Y-m-d H:i:s')
//        );
//
//        $fields = array(
//            'registration_ids' => $registration_ids,
//            'data' => ["notification_data" => $data]
//        );
//
//        $headers = array
//        (
//            'Authorization: key=' . API_ACCESS_KEY,
//            'Content-Type: application/json'
//        );
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
//
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//
//        curl_setopt($ch, CURLOPT_POST, true);
//
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//
//        $result = curl_exec($ch);
//
//        var_dump($result);
//
//        curl_close($ch);
//
//        return $result;

    }

    public function update() {


        $request = json_decode(@file_get_contents('php://input'), true);

//        try{
//
//        }
//        catch(Exception $e) {
//            echo 'Error Message: ' .$e->getMessage();
//        }
//            $user = Auth::user();
//            $id = $user->id;
//            $level_id = $user->level_id;
//
//            $circuit = Circuit::where('id', $level_id)->first();


        $poll_title = $request['poll_title'];
        $poll_question = $request['poll_question'];

        $answer_one = array_key_exists("la_lesson_summary1", $request) ? $request['la_lesson_summary1'] : null;
        $answer_two = array_key_exists("la_lesson_summary2", $request) ? $request['la_lesson_summary2'] : null;
        $answer_three = array_key_exists("la_lesson_summary3", $request) ? $request['la_lesson_summary3'] : null;


        $answers = array();
        if($answer_one!=null || !empty($answer_one)) {
            array_push($answers, $answer_one);
        }

        if($answer_two!=null || !empty($answer_two)) {
            array_push($answers, $answer_two);
        }

        if($answer_three!=null || !empty($answer_three)) {
            array_push($answers, $answer_three);
        }

        $myvar=$request['urlid'];
        $MkQuestion =MkQuestion::find($myvar);
        $MkQuestion->poll_title=$poll_title;
        $MkQuestion->poll_question=$poll_question;
        $MkQuestion->answers=serialize($answers);


        if($MkQuestion->save()) {
            return 1;
        }

        return "Data Updated ," . $MkQuestion->id;


    }



    public function getPolls(){
        DB::enableQueryLog();

        $get_polls = DB::table('mk_questions')
            ->select('id',DB::raw('poll_title as title,poll_question as question,answers as answer'))
            ->get();



        \Illuminate\Database\Eloquent\Collection::make($get_polls)->map(function ($row){
            $row->answer = implode(",", unserialize($row->answer));
            return $row;
        });

        return $get_polls;
    }


    public function destroy() {


        try {

            DB::enableQueryLog();

            $data = json_decode(@file_get_contents('php://input'), true);



            $result= DB::table('mk_questions')
                ->where('id','=',$data)
                ->delete();



            return "successfully deleted";

        }
        catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }

    }


}
