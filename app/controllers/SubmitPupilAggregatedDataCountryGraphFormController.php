<?php

use App\MkPupilAggregatedData;
use Illuminate\Support\Collection;

class SubmitPupilAggregatedDataCountryGraphFormController extends \BaseController
{


    public function submitForm()
    {

        $data = json_decode(@file_get_contents('php://input'), true);

        if ($data['subject'] == 'English') {

            DB::enableQueryLog();

            $result = DB::table('mk_attendance')
                ->select(DB::raw('attendance_academic_level as Level,attendance_academic_week as Week, sum(attendance_no_pupil_level) as pupil_level'))
                ->where('attendance_academic_year', '=', $data['attendance_academic_year'])
                ->where('attendance_academic_term', '=', $data['attendance_academic_term'])
                ->where('attendance_academic_subject', '=', $data['subject'])
                ->groupBy('attendance_academic_level')
                ->groupBy('attendance_academic_week')
                ->orderBy('attendance_academic_level')
                ->get();


            $results = Collection::make($result)->groupBy('Level')->toJson();

            $response = [];

            $colors = ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"];

            $i = 0;
            foreach (json_decode($results, true) as $level => $data) {

                $data = array_column($data, 'pupil_level', 'Week');

                $response[$level]['label'] = 'Level ' . $level;
                $response[$level]['backgroundColor'] = $colors[$i];

                foreach ($weeks = range(1, 8) as $week) {
                    $response[$level]['data'][] = empty($data[$week]) ? 0 : (int)$data[$week];
                }

                $i++;
            }

            return array_values($response);
        }


        if ($data['subject'] == 'Math') {

            DB::enableQueryLog();

            $result = DB::table('mk_attendance')
                ->select(DB::raw('attendance_academic_level as Level,attendance_academic_week as Week  , sum(attendance_no_pupil_level) as pupil_level'))
                ->where('attendance_academic_year', '=', $data['attendance_academic_year'])
                ->where('attendance_academic_term', '=', $data['attendance_academic_term'])
                ->where('attendance_academic_subject', '=', $data['subject'])
                ->groupBy('attendance_academic_level')
                ->groupBy('attendance_academic_week')
                ->orderBy('attendance_academic_level')
                ->get();

//            $results = Collection::make($result)->groupBy('Level')->toJson();
//
//            $response = [];
//
//            $colors = ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"];
//
//            foreach (json_decode($results, true) as $level => $data) {
//
//                $data = array_column($data, 'pupil_level', 'Week');
//
//                $response[$level]['label'] = 'Level ' . $level;
//                $response[$level]['backgroundColor'] = $colors[$level -1 ];
//
//                foreach ($weeks = range(1, 8) as $week) {
//                    $response[$level]['data'][] = empty($data[$week]) ? 0 : (int)$data[$week];
//                }
//            }
//
//            return array_values($response);


            $results = Collection::make($result)->groupBy('Level')->toJson();

            $response = [];

            $colors = ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"];

            $i = 0;
            foreach (json_decode($results, true) as $level => $data) {

                $data = array_column($data, 'pupil_level', 'Week');

                $response[$level]['label'] = 'Level ' . $level;
                $response[$level]['backgroundColor'] = $colors[$i];

                foreach ($weeks = range(1, 8) as $week) {
                    $response[$level]['data'][] = empty($data[$week]) ? 0 : (int)$data[$week];
                }

                $i++;
            }

            return array_values($response);
        }






        //select attendance_academic_level,
        // attendance_academic_week,
        //attendance_no_pupil_avg,
        // attendance_no_pupil_level
        // from `mk_attendance`
        // where attendance_academic_term = 'Term 2' and attendance_academic_year = '2015' and attendance_academic_subject = 'English'
        //  group by attendance_academic_level, attendance_academic_week

        // select sum(attendance_academic_term) as Term,sum(attendance_academic_level) as Level ,sum(attendance_academic_week) as Week ,sum(attendance_academic_year) as Year from `mk_attendance` where attendance_academic_year = '2015' and  attendance_academic_term='Term 2' and attendance_academic_subject='English'

        // select sum(attendance_academic_term) as Term,sum(attendance_academic_level) as Level ,sum(attendance_academic_week) as Week ,sum(attendance_academic_year) as Year from `mk_attendance` where attendance_academic_year = '2016' and  attendance_academic_term='Term 1'
//dd($data);

        //dd($result);


//        if ($data['subject'] == 'Maths') {
//
//            DB::enableQueryLog();
//            $result = DB::table('mk_pupil_aggregated')
//                ->select(DB::raw('grade as Grade,sum(math_beginner_text) as Beginner , sum(math_digit_one_text) as One , sum(math_digit_two_text) as Two, sum(math_addition_text) as Addition , sum(math_subtraction_text) as Subtraction'))
//                ->where('academic_year', '=', $data['academic_year'])
//                ->where('grade', '=', 'P4')
//                ->groupBy('grade')
//                ->get();
//
//            return $result;
//
//        }


//        if ($data['subject'] == 'Maths') {
//
//            $result = DB::table('mk_attendance')
//                ->select(DB::raw('grade as Grade,sum(academic_attendance_week),sum(academic_attendance_week)'))
//                ->where('academic_year', '=', $data['academic_year'])
//                ->where('academic_attendance_week', '=', $data['academic_attendance_week'])
//                ->groupBy('grade')
//                ->get();
//
//
//            return $result;
//        }

    }


}
