<?php

class SchoolReportCardController extends BaseController {

	


	//this function recieves the data submitted by the android app on the aggregate weekly data on school report card
	public function ReceivesWeeklyAggregateSubmision()

	{
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Aggregate weekly submission in the first function', array('context' => $data));

		//this calls a function in the school_report_card.php to process the data posted by the android app
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesWeeklyAggregateSubmision($data);


	}





	//this function recieves the data submitted by the android app on the aggregate termly data on school report card
	public function ReceivesTermlyAggregateSubmision()

	{
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Aggregate termly submission in the first function', array('context' => $data));

		//this calls a function in the school_report_card.php to process the data posted by the android app
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesTermlyAggregateSubmision($data);

		
	}




	//this function recieves the data submitted by the android app on school report card
	//on school class enrolment
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEnrolment()

	{
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school report card data on normal enrolment', array('context' => $data));

		//this calls a function in the school_report_card.php to process the data posted by the android app
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEnrolment($data);
	}

	//this function recieves the data submitted by the android app on school report card
	//on school class enrolment
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsEnrolment()
    {
		$data = json_decode(@file_get_contents('php://input'));	

		Log::info('Just to see school report card data on special enrolment', array('context' => $data));
		//exit();
		//this calls a function in the school_report_card.php to process the data posted by the android app
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsEnrolment($data);
	}


	//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSanitation()
	{
		$data = json_decode(@file_get_contents('php://input'));	
		Log::info('Just to see school report card data on sanitition', array('context' => $data));

		//$data =array('data' => $data);
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSanitation($data);
	}


	//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSecurity()
	{
		//return 'You are submitting school report card data';
		//return "You are about to submit school visits data";
		//$data = Input::all();
		$data = json_decode(@file_get_contents('php://input'));
		Log::info('Just to see school report card data on security', array('context' => $data));
		
		// return Response::json(array("status"=>200,
		// 							"message"=>"Data Submitted"));
		//$data =array('data' => $data);
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSecurity($data);
	}


	//this function recieves the data submitted by the android app on school report card
	//for equipment and recreation
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEquipmentAndRecreation()
	{

		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school report card data on recreation', array('context' => $data));
	
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEquipmentAndRecreation($data);
	}


	//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolStructure()
	{

		$data = json_decode(@file_get_contents('php://input'));
		Log::info('Just to see school report card data schol structure', array('context' => $data));
		
		// return Response::json(array("status"=>200,
		// 							"message"=>"Data Submitted"));
		//$data =array('data' => $data);
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolStructure($data);
	}


	//this function recieves the data submitted by the android app on school report card for furniture situation
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnFurniture()
	{
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school report card data on school structure', array('context' => $data));

		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnFurniture($data);
	}

	//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnRecordAndPerformance()
	{

		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school report card data on performance', array('context' => $data));
		
		// return Response::json(array("status"=>200,
		// 							"message"=>"Data Submitted"));

		//$data =array('data' => $data);
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnRecordAndPerformance($data);
	}

	//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnCommunityRelations()
	{

		$data = json_decode(@file_get_contents('php://input'));
		Log::info('Just to see school report card data community relations', array('context' => $data));

		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnCommunityRelations($data);
	}

		//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPunctualityAndLessons()
	{
		$data = json_decode(@file_get_contents('php://input'));
		Log::info('Just to see school report card data on punctuality and lessons', array('context' => $data));

		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPunctualityAndLessons($data);
	}

	//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassManagement()
	{
		$data = json_decode(@file_get_contents('php://input'));
		Log::info('Just to see school report card data on class management', array('context' => $data));

		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassManagement($data);
	}


	//this function recieves the data submitted by the android app on school report card
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberUnitsCoveredByTeacher()
	{

		$data = json_decode(@file_get_contents('php://input'));
		Log::info('Just to see school report card data on number units covered by teacher', array('context' => $data));
	
		//$data =array('data' => $data);
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberUnitsCoveredByTeacher($data);
	}

	//'Receive data submitted on school report card data on general school situation';
	public function ReceiveDataOnGeneralSchoolSituation()
	{
		//return "school situation";
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('All the general school situation', array('context' => $data));
		//exit();
		return SchoolReportCardsDataReceivedAnalysyis::ReceiveDataOnGeneralSchoolSituation($data);

	}


	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnMeetings()
	{
		
		//return "Meetings";

		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school report card data on meetings', array('context' => $data));

		
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnMeetings($data);
	}


	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPupilsPerformance()
	{
		
		//return "Meetings";

		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school report card data on Pupils Performance', array('context' => $data));

		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPupilsPerformance($data);
	}


	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberOfTextBooks()
	{
		
		//return "Meetings";

		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Text books availability', array('context' => $data));

		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberOfTextBooks($data);
	}





	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolSupportTypes()
	{
		
		//return "Meetings";

		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school support types data', array('context' => $data));

		
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolSupportTypes($data);
	}

	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolGrantCapitationPayments()
	{
		
		//return "Meetings";

		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see school school grant payments data', array('context' => $data));

		
		return SchoolReportCardsDataReceivedAnalysyis::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolGrantCapitationPayments($data);
	}


	//querying the school enrolment table
	public function schoolsReportCardEnrolmentData(){
		
		$question_category_code = 'SCE';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardEnrolmentData($question_category_code, $queryString, $values);
	}



	//querying the school enrolment table
	public function schoolsReportCardSpecialEnrolmentData()
	{
		
		$question_category_code = 'SSSE';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardSpecialEnrolmentData($question_category_code, $queryString, $values);
	}



	//querying the student attendance
	public function schoolsReportCardStudentsAttendanceData()
	{
		
		$question_category_code = 'SCA';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardStudentsAttendanceData($question_category_code, $queryString, $values);
	}


	
	//querying the student attendance
	public function schoolsReportCardSpecialStudentsAttendanceData()
	{
		
		$question_category_code = 'SSCA';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardSpecialStudentsAttendanceData($question_category_code, $queryString, $values);
	}



	//querying the school teachers table
	public function schoolsReportCardSchoolTeachers()
	{
	
		$question_category_code = 'STI';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardSchoolTeachers($question_category_code, $queryString, $values);
	}


	public function stateOfSchoolPunctuality()
	{
	
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::stateOfSchoolPunctuality($queryString, $values);
	}

	public function stateOfSchoolManagement()
	{
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::stateOfSchoolManagement($queryString, $values);
	}

	public function stateOfSchoolUnitsCoveredByTeachers()
	{
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::stateOfSchoolUnitsCoveredByTeachers($queryString, $values);
	}

	public function stateOfSchoolTeacherPunctualityAndLessonPlan($id)
	{

		return SchoolReportCardsDataReceivedAnalysyis::stateOfSchoolTeacherPunctualityAndLessonPlan($id);

	}


	public function stateOfSchoolTeacherClassManagement($id)
	{

		return SchoolReportCardsDataReceivedAnalysyis::stateOfSchoolTeacherClassManagement($id);

	}


//querying the pupils performance table
	public function schoolsReportCardPupilsPerformance()
	{
	
		$question_category_code = 'PEPA';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

//return $queryString;
		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardPupilsPerformance($question_category_code, $queryString, $values);
	}

	//querying the school support type table
	public function schoolsReportCardSchoolSupportType()
	{
	
		$question_category_code = 'ST';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardSchoolSupportType($question_category_code, $queryString, $values);
	}

//querying the school support type table
	public function schoolsReportCardSchoolCapitationGrantsPayments()
	{
	
		$question_category_code = 'SG';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardSchoolCapitationGrantsPayments($question_category_code, $queryString, $values);
	}

	//querying the school support type table
	public function schoolsReportCardSchoolClassTextBooks()
	{
	
		$question_category_code = 'CTB';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardSchoolClassTextBooks($question_category_code, $queryString, $values);
	}

	//querying the school support type table
	public function retrieveStateOfSchoolsMeetings()
	{

		$question_category_code = 'SM';

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return SchoolReportCardsDataReceivedAnalysyis::retrieveStateOfSchoolsMeetings($question_category_code, $queryString, $values);



	}



	public function retrieveGeneralSchoolSituationData()
	{
			//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
		return SchoolReportCardsDataReceivedAnalysyis::retrieveGeneralSchoolSituationData($queryString, $values);	
	}


public function RetrieveTotalsOnWeeklySubmittedData()
{
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
		return SchoolReportCardsDataReceivedAnalysyis::RetrieveTotalsOnWeeklySubmittedData($queryString, $values);
}	

public function RetrieveTotalsOnTermlySubmittedData()
{
			//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
		return SchoolReportCardsDataReceivedAnalysyis::RetrieveTotalsOnTermlySubmittedData($queryString, $values);
}





public function RetrieveWeeksTermsYearsThatDataHasBeenSubmittedOn()
{

	//getting the posted query parameters
	 $inputs = Input::except('category');

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

	$submission_category_to_check = Input::get('category');


	//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
	return SchoolReportCardsDataReceivedAnalysyis::getWeeksTermsYearsThatDataHasBeenSubmittedOn($queryString, $values, $submission_category_to_check);

	

}	




public function RetrieveTotalsOnWeeklySubmittedDataForCSDashboard()
{

	//getting the posted query parameters
	 $inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);


	// $submission_category_to_check = Input::get('category');


	$weekly_totals = DB::table('weekly_totals')->whereRaw($queryString, $values)->get();


	return Response::json(array("status"=>200,
								"message"=>"OK",
								"weekly_totals"=> $weekly_totals));

	

}





public function RetrieveTotalsOnTermlySubmittedDataForCSDashboard()
{

	//getting the posted query parameters
	 $inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

	// $submission_category_to_check = Input::get('category');


	
	$termly_totals = DB::table('termly_totals')->whereRaw($queryString, $values)->get();


	return Response::json(array("status"=>200,
								"message"=>"OK",
								"termly_totals"=> $termly_totals));



}




	public function RetrievingAggregateDataForEnrolmentAndAttendance()
	{
		
		$data_type = Input::get('data_type');

		//getting the posted query parameters
		$inputs = Input::except('data_type');

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);


		$enrolement  = array();

		$attendance  = array();

		$teacher_attendance_and_class_management  = array();

		switch ($data_type) {

			case 'student_enrolment':

				 $normal_enrolment_total_boys = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('normal_enrolment_total_boys');

				 $enrolement = array_add($enrolement, 'normal_enrolment_total_boys', $normal_enrolment_total_boys);

				 $normal_enrolment_total_girls = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('normal_enrolment_total_girls');

				 $enrolement = array_add($enrolement, 'normal_enrolment_total_girls', $normal_enrolment_total_girls);

				 $special_enrolment_total_boys = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('special_enrolment_total_boys');

				 $enrolement = array_add($enrolement, 'special_enrolment_total_boys', $special_enrolment_total_boys);

				 $special_enrolment_total_girls = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('special_enrolment_total_girls');

				 $enrolement = array_add($enrolement, 'special_enrolment_total_girls', $special_enrolment_total_girls);


				$response = self::formattedResponseObject($code = 200, $status = "OK", $message = "Aggregate enrolement data on your selected parameters", $data = array('aggregate_enrolment_data' => $enrolement));

				return  $response;

				break;

			case 'student_attendance':
				
				 $normal_attendance_total_boys = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('normal_attendance_total_boys');

				 $attendance = array_add($attendance, 'normal_attendance_total_boys', $normal_attendance_total_boys);

				 $normal_attendance_total_girls = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('normal_attendance_total_girls');

				 $attendance = array_add($attendance, 'normal_attendance_total_girls', $normal_attendance_total_girls);

				 $special_attendance_total_boys = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('special_attendance_total_boys');

				 $attendance = array_add($attendance, 'special_attendance_total_boys', $special_attendance_total_boys);

				 $special_attendance_total_girls = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('special_attendance_total_girls');

				 $attendance = array_add($attendance, 'special_attendance_total_girls', $special_attendance_total_girls);


				 $response = self::formattedResponseObject($code = 200, $status = "OK", $message = "Aggregate attendances data on your selected parameters", $data = array('aggregate_attendance_data' => $attendance));

			     return  $response;

				break;

			case 'teacher_attendance_and_class_management':
				
				 $average_teacher_attendance = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('average_teacher_attendance');

				 $teacher_attendance_and_class_management = array_add($teacher_attendance_and_class_management, 'average_teacher_attendance', $average_teacher_attendance);

				 $teacher_classmanagement = DB::table('weekly_totals')->whereRaw($queryString, $values)->sum('average_teacher_class_management');

				 $teacher_attendance_and_class_management = array_add($teacher_attendance_and_class_management, 'teacher_classmanagement', $teacher_classmanagement);

				 $response = self::formattedResponseObject($code = 200, $status = "OK", $message = "Aggregate teacher attendance and class management data on your selected parameters", $data = array('teacher_attendance_and_class_management' => $teacher_attendance_and_class_management));

			     return  $response;

				# 
				break;
			
			default:

				$response = self::formattedResponseObject($code = 200, $status = "OK", $message = "Error in parameters posted check and try again");

				 return  $response;
				 
				break;
		}


	}







public static function formattedResponseObject($code = 200, $status = "OK", $message = "Data returned succesfully", $data = null)
{
		
		
		$response = Response::json(array("code"=>$code,"status"=>$status, "message"=>$message, "data"=> $data));


		return $response;

}



	//querying the school enrolment table
	public function retrieveSchoolsCircuitsDistrictsRegionsDynamically()
	{
		$query_level = Input::get('query_level');

		//getting the posted query parameters
		$inputs = Input::except('query_level');

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


		return SchoolReportCardsDataReceivedAnalysyis::schoolsReportCardEnrolmentData($queryString, $values, $query_level);
	}









}
