<?php

class TeacherController extends BaseController {

	
	public function testing()
	{
		$school_id = 1;
		
		return TeacherRegistrationAndEditingAndDeleting::currentNumberOfTeachersInASchool($school_id);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnTeacherRegistration()
	{
		//return "just to see teacher";
		//return 'You are submitting school report card data';
		//return "You are about to submit school visits data";
		//$data = Input::all();
		$data = json_decode(@file_get_contents('php://input'));

		//$data = Input::all();

		Log::info('Just to see school report card data on teacher registration', array('context' => $data));
		
		// return Response::json(array("status"=>200,
		// 							"message"=>"Teacher Info Submitted"));

		return TeacherRegistrationAndEditingAndDeleting::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnTeacherRegistration($data);
	}




	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function RegisterTeachersByAndroidOnInternetDetection()
	{
		$data = json_decode(@file_get_contents('php://input'));

		//$data = Input::all();

		Log::info('Just to see school report card data on teacher registration when internet is detected', array('context' => $data));

		return TeacherRegistrationAndEditingAndDeleting::RegisterTeachersByAndroidOnInternetDetection($data);
	}



	public function apiRequestForDataCollectorTeachersForASchool()
	{
		$school_id = Input::get('school_id');

		return TeacherRegistrationAndEditingAndDeleting::apiRequestForDataCollectorTeachersForASchool($school_id);
		

	}


	public static function editTeacherInformationThroughAndroidAPP()
	{
		$data = json_decode(@file_get_contents('php://input'));
		//$data = Input::all();

		Log::info('Just to see details of teacher to edit', array('context' => $data));

		return TeacherRegistrationAndEditingAndDeleting::editTeacherInformationThroughAndroidAPP($data);
	}


	public static function deleteTeacherThroughAndroidAPP()
	{
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see details of teacher to delete', array('context' => $data));

		return TeacherRegistrationAndEditingAndDeleting::deleteTeacherThroughAndroidAPP($data);
	}



	public function createTeacherViaForm()
	{
			
			//return Input::all();

			$fields = array('school_reference_code' => Input::get('school_code'),
							'questions_category_reference_code' => Input::get('questions_category_reference_code','STI'),
							'first_name' => Input::get('first_name'),
							'last_name' => Input::get('last_name'),
							'gender' => Input::get('gender'),
							'level_taught' => Input::get('level_taught'),
							'class_subject_taught' => Input::get('class_subject_taught','No subjects provided'),
							'class_taught' => Input::get('class_taught','Not provided'),
							'category' => Input::get('category','Not provided'),
							'staff_number' => Input::get('staff_number'),
							'rank' => Input::get('rank','Not provided'),
							'highest_academic_qualification' => Input::get('highest_academic_qualification','Not provided'),
							'highest_professional_qualification' => Input::get('highest_professional_qualification','Not provided'),
							'years_in_the_school' => Input::get('years_in_the_school'),
							'term' => Input::get('term','Not provided'),
							'week_number' => Input::get('week_number','Not provided'),
							'year' => Input::get('year','Not provided'),
							'country_id' => Input::get('country_id'),
							'region_id' => Input::get('region_id'),
							'district_id' => Input::get('district_id'),
							'circuit_id' => Input::get('circuit_id'),
							'school_id' => Input::get('school_id'),
							'data_collector_id' => Input::get('data_collector_id'),
							'data_collector_type' => Input::get('data_collector_type'),
							'lat' => Input::get('lat','Not provided'),
							'long' => Input::get('long','Not provided'),
							);

		
		return TeacherRegistrationAndEditingAndDeleting::createTeacherViaForm($fields);


		
	}




	public function createTeacherViaCSVUpload()
	{
		$school_reference_code = Input::get('school_code');
		$school_id = Input::get('id');
		$country_id = Input::get('country_id',1);
		$region_id = Input::get('region_id');
		$district_id = Input::get('district_id');
		$circuit_id = Input::get('circuit_id');

		// echo "The school reference code is: ".$school_reference_code;
		// exit();

		$csv_file = Input::file('file');

		if (!empty($school_id) & is_file($csv_file)) {


			$csv_file_location = $csv_file->move(public_path() . '/csv/', time());

			$arrayOfCSVTeachers = Helper::csvToArray($csv_file_location);

			foreach ($arrayOfCSVTeachers as $csvTeacherInfo) {

					$rowCount = 1;

					$fields = array('school_reference_code' => $school_reference_code,
							'questions_category_reference_code' => 'STI',
							'first_name' => $csvTeacherInfo['first_name'],
							'last_name' => $csvTeacherInfo['last_name'],
							'gender' => $csvTeacherInfo['gender'],
							'level_taught' => $csvTeacherInfo['level_taught'],
							'class_subject_taught' => $csvTeacherInfo['class_subject_taught'],
							'class_taught' => $csvTeacherInfo['class_taught'],
							'category' => $csvTeacherInfo['category'],
							'staff_number' => $csvTeacherInfo['staff_number'],
							'rank' => $csvTeacherInfo['rank'],
							'highest_academic_qualification' => $csvTeacherInfo['highest_academic_qualification'],
							'highest_professional_qualification' => $csvTeacherInfo['highest_professional_qualification'],
							'years_in_the_school' => $csvTeacherInfo['years_in_the_school'],
							'year' => $year = date('Y'),
							'country_id' => $country_id,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id
							);

				try {

					//validating the input
					$validation = Teachers::create($fields);

					
				} catch (Exception $e) {

					echo "Error";

				}
				


			}


				return 'success';

		}


		return 'failed';

	
	}



	public static function deleteTeacherFromDashboard()
	{
		$school_id = Input::get('school_id');
		$teacher_id = Input::get('teacher_id');


		$delete_teacher = DB::table('teachers')->where('school_id', $school_id)->where('id', $teacher_id)->delete();

		
		if ($delete_teacher) {
			
			return Response::json(array(
										"status"=>200,
										"message"=> "Teacher deleted"));

		}else{

			return Response::json(array(
										"status"=>404,
										"message"=> "Teacher not deleted"));
		}

	}


	//Editing teacher
public static function editTeacherFromDashboard()
{

	$teacher_id = Input::get('teacher_id');

	$teacher_details_to_edit = Input::except('teacher_id');

	//getting Details of data collector who is edited
	$teacherToEdit = DB::table('teachers')->where('id', $teacher_id)->get();

	if (count($teacherToEdit) > 0) {

		$update = DB::table('teachers')->where('id', $teacher_id)->update($teacher_details_to_edit);


		if ($update) {

			return Response::json(array("status"=>200, "message"=> "The teacher edited successfully"));
		}


		return Response::json(array("status"=>401, "message"=> "Unable to edit teacher"));


	}else{

		return Response::json(array("status"=>401, "message"=> "The teacher you're trying to edit does not exist"));
	}

	
}


    public function updateTeacherStatus()
    {
        $teacher_id = Input::get('teacher_id');

        $new_teacher_status = Input::get('new_teacher_status');

        $find_teacher = Teachers::find($teacher_id);

        if (count($find_teacher) > 0) {

            $find_teacher->teacher_status = $new_teacher_status;
            $update_teacher_status = $find_teacher->save();

            if ($update_teacher_status) {

                return Response::json(array(
                    "status"=>200,
                    "message"=> "Teacher status changed successfully"));
            }else{

                return Response::json(array(
                    "status"=>401,
                    "message"=> "Unable to change teacher status, please try again"));
            }


        }


    }


public function transferTeacherToANewSchool()
{
	
	$teacher_id = Input::get('teacher_id');

	$teacher_old_school_id = Input::get('teacher_old_school_id');

	$old_school_reference_code = Input::get('old_school_reference_code');

	$new_school_teacher_is_transfer_to_id = Input::get('teacher_new_school_id');

	$new_school_reference_code = Input::get('new_school_reference_code');

	$look_for_the_teacher_with_reference_to_old_school = Teachers::find($teacher_id);

	// $look_for_the_teacher_with_reference_to_old_school = DB::table('teachers')->where('id', $teacher_id)->where('school_reference_code', $old_school_reference_code)->where('school_id', $teacher_old_school_id)->first();


	if (count($look_for_the_teacher_with_reference_to_old_school) > 0) {


		$find_new_school_teacher_is_transfered_to = DB::table('schools')->where('id', $new_school_teacher_is_transfer_to_id)->where('school_code', $new_school_reference_code)->first();

		if (count($find_new_school_teacher_is_transfered_to) > 0) {

			$new_teacher_transfer_school_reference_code = $find_new_school_teacher_is_transfered_to->school_code;

			$new_teacher_transfer_region = $find_new_school_teacher_is_transfered_to->region_id;

			$new_teacher_transfer_district = $find_new_school_teacher_is_transfered_to->district_id;

			$new_teacher_transfer_circuit_id = $find_new_school_teacher_is_transfered_to->circuit_id;

			$new_teacher_transfer_school_id = $find_new_school_teacher_is_transfered_to->id;

			$new_teacher_transfer_headteacher_id = $find_new_school_teacher_is_transfered_to->headteacher_id;

			$new_teacher_transfer_supervisor_id = $find_new_school_teacher_is_transfered_to->supervisor_id;


			$look_for_the_teacher_with_reference_to_old_school->school_reference_code = $new_teacher_transfer_school_reference_code;
			
			$look_for_the_teacher_with_reference_to_old_school->region_id = $new_teacher_transfer_region;

			$look_for_the_teacher_with_reference_to_old_school->district_id = $new_teacher_transfer_district;

			$look_for_the_teacher_with_reference_to_old_school->circuit_id = $new_teacher_transfer_circuit_id;

			$look_for_the_teacher_with_reference_to_old_school->school_id = $new_teacher_transfer_school_id;

			$look_for_the_teacher_with_reference_to_old_school->data_collector_id = $new_teacher_transfer_headteacher_id;

			$transfer = $look_for_the_teacher_with_reference_to_old_school->save();

			// $transfer_teacher = DB::table('users')
			// 						            ->where('id', 1)
			// 						            ->update(array('votes' => 1));

			if ($transfer) {

				
				return Response::json(array(
									"status"=>200,
									"message"=> "Teacher transfered to a new school successfully"));
			}else{

				return Response::json(array(
									"status"=>401,
									"message"=> "Unable to transfer teacher, please try again"));
			}
			
		}else{


			return Response::json(array(
									"status"=>401,
									"message"=> "The new school you are trying to transfer the teacher does not exist"));

		}
	}else{

		return Response::json(array(
									"status"=>401,
									"message"=> "The teacher you are trying to transfer under the school to a new school does not exist"));
	}

	
}





public function getTheAverageTeacherAttendanceForASchool()
{

	//getting the posted query parameters
	 $school_id = Input::get('school_id');

	 //getting the posted query parameters
	 $term= Input::get('term', null);

	 //getting the posted query parameters
	$year = Input::get('year');

	//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
	return TeacherRegistrationAndEditingAndDeleting::getAverageTeacherAttendanceBaseOnAPeriod($school_id, $term, $year);

	

}





public function deleteTeacherAttendanceAndClassManagementForAPeriod()
{

    $submission_id = Input::get('id');

    $teacher_assessment_type = Input::get('data_type');

    $inputs = Input::except('data_type');
    //return array_keys($inputs) of the posted data;
    $values = array_values($inputs);

    //declaring a query string to query with in the whereRaw method
    $queryString = '';

    //puting the key into the query parameters and appending the word 'and';
    foreach ($inputs as $key => $value) {
        $queryString .= $key.' = ? and ';
    }
    //removing the last and which will be appended
    $queryString = substr($queryString, 0, -4);


    if ($teacher_assessment_type == 'teacher_attendance') {

//     DB::table('punctuality_and_lessons')->where('id', $submission_id)->delete();
       $delete =  DB::table('punctuality_and_lessons')->whereRaw($queryString, $values)->delete();

        if ($delete) {
               DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['average_teacher_attendance' => 0]);
               DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['average_teacher_class_management' => 0]);
               DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['current_number_of_teachers' => 0]);
               DB::table('teachers_class_management')->whereRaw($queryString, $values)->delete();
                     }

        return "success";


    }elseif ($teacher_assessment_type == 'class_management') {

//        DB::table('teachers_class_management')->where('id', $submission_id)->delete();
        DB::table('teachers_class_management')->whereRaw($queryString, $values)->delete();

        return "success";


    }else{

        return "No Section specified";
    }


}




public function allHeadTeachersInACircuit()
{


    $circuit_id = Input::get('circuit_id');

    $district_id = Input::get('district_id');


    $all_headteachers_in_a_circuit = DB::table('data_collectors')->where('type', 'head_teacher')->where('district_id',$district_id)->where('circuit_id',$circuit_id)->get();

    return Response::json(array("status"=>200,
                                "message"=>"OK",
                                "all_headteachers_in_a_circuit"=> $all_headteachers_in_a_circuit));


}




public function allTeachersInACircuit()
{


    $circuit_id = Input::get('circuit_id');

    $district_id = Input::get('district_id');

    $region_id = Input::get('region_id');


    $all_teachers_in_a_circuit = DB::table('teachers')->where('region_id',$region_id)->where('district_id',$district_id)->where('circuit_id',$circuit_id)->get();

    return Response::json(array("status"=>200,
                                "message"=>"OK",
                                "all_teachers_in_a_circuit"=> $all_teachers_in_a_circuit));


}




}
