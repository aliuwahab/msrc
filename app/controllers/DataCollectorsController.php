<?php

class DataCollectorsController extends BaseController {

		/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//getting the posted details of the newly created data collector i.e headmaster or circuit supervisor
		$fields = array(
			'first_name' => Input::get('first_name'),
			'last_name' => Input::get('last_name'),
			'gender' => Input::get('gender'),
			'email' => Input::get('email','no email'),
			'identity_code' => Helper::generateDataCollectorsUniqueCode($table  = 'data_collectors', $column = 'identity_code'),
			'universal_code' => 'QG2354GHEQ45',
			'phone_number' => Input::get('phone_number'),
			'type' => Input::get('type'),
			'country_id' => Input::get('country', 1),
			'region_id' => Input::get('region'),
			'district_id' => Input::get('district'),
			'circuit_id' => Input::get('circuit')
		);


		//validating the input
		$validation = DataCollector::validate($fields);





		if ($validation !== true) {

			return $validation->messages();
			
		}else{
			//Save into the newly created data collector
			$new_data_collector = DataCollector::create($fields);

			if ($new_data_collector) {

					WhoCreatedWhat::create(array('user_id' => Auth::user()->id,
											 	'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
											 	'what_was_created' => "Data Collector with name: ".$new_data_collector->first_name.' '.$new_data_collector->last_name."was created by: ".Auth::user()->firstname.' '.Auth::user()->lastname));


					//the user credentials to be sent to the newly created data collector
					$message = "Hello ".$fields['first_name'].", these are your mSRC login credentials:\n Username: ".$fields['phone_number']."\n Password: ".$fields['identity_code'];

					$number = $fields['phone_number'];

					//calling the function that will send data collector login details
					self::sendingToSendSMSForDataCollectorsLoginDetails($message,$number);

					return "success";

			}else{
				return 'The PHONE NUMBER already exist';
			}
		}

	}



	public function updateHeadTeacherDetailsWhoIsAlsoADataCollector()
	{
		
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Just to see details of the headteacher', array('context' => $data));

		$headteacher_id = $data->data_collector_id;
		$school_code = $data->school_code;
		$school_id = $data->school_id;
		$region_id = $data->region_id;
		$region_id = $data->region_id;
		$district_id = $data->district_id;
		$circuit_id = $data->circuit_id;
		$data_collector_type = $data->data_collector_type;
		$long = $data->long;
		$lat = $data->lat;
		$first_name = $data->first_name;
		$last_name = $data->last_name;
		$years_of_teaching = $data->teacher_years;
		$years_as_headteacher = $data->head_teacher_years;
		$gender = $data->gender;




		$find_headteacher = DB::table('data_collectors')
								->where('id', $school_id)
            					->where('region_id', $region_id)
            					->where('district_id', $district_id)
            					->where('circuit_id', $circuit_id)->first();
       
        if (empty($find_headteacher)) {
				return Response::json(array("status"=>401,
											"message"=>"Head Teacher does not exist"));
        }


		//saving the edited info of the data collector
			$update = DB::table('data_collectors')
            					->where('id', $headteacher_id)
            					->where('region_id', $region_id)
            					->where('district_id', $district_id)
            					->where('circuit_id', $circuit_id)
            						->update(array(
            							'first_name' => $first_name,
            							'last_name' => $last_name,
            							'gender' => $gender,
            							'employed_date_as_teacher' => $years_of_teaching,
            							'employed_date_as_headteacher' => $years_as_headteacher,
            							'region_id' => $region_id,
            							'district_id' => $district_id,
            							'circuit_id' => $circuit_id));


        if ($update) {
        		$find_data_collector = DB::table('data_collectors')
								->where('id', $headteacher_id)
            					->where('region_id', $region_id)
            					->where('district_id', $district_id)
            					->where('circuit_id', $circuit_id)->first();
        	//Log::info('Data Already submitted by: ', array('context' => $data_collector_id));
				return Response::json(array("status"=>200,
											"message"=>"Head teacher information updated",
											'head_teacher' => $find_data_collector));
        }else{
        	//Log::info('Data Already submitted by: ', array('context' => $data_collector_id));
			return Response::json(array("status"=>200,
										"message"=>"Unable to update head teacher details"));
        }


	}





	public function ToResendDataCollectorsLoginDetails($firstName,$identityCode,$number)
	{
		//return $firstName;

		$message = "Hello ".$firstName.", these are your mSRC login credentials: \n Username: +".$number." \n Password: ".$identityCode;

		//calling the function that will send data collector login details
		self::sendingToSendSMSForDataCollectorsLoginDetails($message,$number);

		return "success";

	}

	public static function sendingToSendSMSForDataCollectorsLoginDetails($message,$number)
	{
		
		//the data been passed into the queue to be use by the function it calls
		$smsData = array("message"=>$message, 
						 "number"=>$number);

		SMS::sendSMS($smsData['message'],$smsData['number']);

		// Queue::push(function($job) use($smsData){
		// 					SMS::sendSMS($smsData['message'],$smsData['number']);
		// 					$job->delete();
		// });
					
	}



	public static function dataCollectorDetailsAndSchool($id)
	{
		
		$data_collector = DB::table('data_collectors')->where("id", $id)->first();
		if ($data_collector) {
			switch ($data_collector->type) {
				case 'circuit_supervisor':
					$circuitSupervisorSchools = DB::table('schools')->where("district_id", $data_collector->district_id)->get();
					return Response::json(array(
												"data_collector"=>$data_collector,
												'schools' => $circuitSupervisorSchools));
					break;
				
				default:
					$headteacherSchool = $allDistrictSchools = DB::table('schools')->where("headteacher_id", $data_collector->id)->first();
					return Response::json(array(
												"data_collector"=>$data_collector,
												'schools' => $headteacherSchool));
					break;
			}
		}else{
			$message = 'No Circuit Supervisor With Such Details';
			return Response::json(array(
				"message"=>$message
				));

		}
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showDataCollector($id)
	{
        return $schoolInfo = DataCollector::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editDataCollector($id)
	{
         return $schoolInfo = DataCollector::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateDataCollector($id)
	{
		//getting Details of data collector who is edited
		$datCollectorToEdit = DB::table('data_collectors')->where('id', $id)->first();
		$datCollectorToEditCurrentNumber = $datCollectorToEdit->phone_number;
		//getting the posted details of the newly created data collector i.e headmaster or circuit supervisor
		$fields = array(
			'first_name' => Input::get('first_name'),
			'last_name' => Input::get('last_name'),
			'gender' => Input::get('gender'),
			'employed_date_as_teacher' => Input::get('employed_date_as_teacher',''),
			'employed_date_as_headteacher' => Input::get('employed_date_as_headteacher',''),
			'employed_date_as_circuit_supervisor' => Input::get('employed_date_as_circuit_supervisor',''),
			'period' => Input::get('period'),
			'email' => Input::get('email', 'no email'),
			'identity_code' => Helper::generateDataCollectorsUniqueCode($table  = 'data_collectors', $column = 'identity_code'),
			'universal_code' => 'QG2354GHEQ45',
			'phone_number' => Input::get('phone_number','no number'),
			'type' => Input::get('type'),
			'country_id' => Input::get('country', 1),
			'region_id' => Input::get('region'),
			'district_id' => Input::get('district'),
			'circuit_id' => Input::get('circuit')
		);


		if ($datCollectorToEditCurrentNumber != $fields['phone_number']) {
			//validating the input
			$validation = DataCollector::validate($fields);

			if ($validation !== true) {

				return $validation;

			}else{

				//saving the edited info of the data collector
				$update = DB::table('data_collectors')
	            					->where('id', $id)
	            						->update(array(
	            							'first_name' => $fields['first_name'],
	            							'last_name' => $fields['last_name'],
	            							'gender' => $fields['gender'],
	            							'employed_date_as_teacher' => $fields['employed_date_as_teacher'],
	            							'employed_date_as_headteacher' => $fields['employed_date_as_headteacher'],
	            							'employed_date_as_circuit_supervisor' => $fields['employed_date_as_circuit_supervisor'],
	            							'period' => $fields['period'],
	            							'email' => $fields['email'],
											'identity_code' => $fields['identity_code'],
	            							'phone_number' => $fields['phone_number'],
	            							'type' => $fields['type'],
	            							'country_id' => $fields['country_id'],
	            							'region_id' => $fields['region_id'],
	            							'district_id' => $fields['district_id'],
	            							'circuit_id' => $fields['circuit_id']));


				if ($update) {
					//return "success";
					//return $firstName;
					//
					WhoEditedWhat::create(array('user_id' => Auth::user()->id, 
										 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
										 'what_was_edited' => "Data Collector with name: ".$fields['first_name']." ".$fields['last_name']." was edited by: ".Auth::user()->firstname.' '.Auth::user()->lastname));



					$message = "Hello ".$fields['first_name'].", these are your mSRC login credentials: \n Username: ".$fields['phone_number']." \n Password: ".$fields['identity_code'];
//					$message = "Hello ".$fields['first_name'].", your mSRC profile has been edited, logout and login again to get your latest profile";
					$number = $fields['phone_number'];
					//calling the function that will send data collector login details
					self::sendingToSendSMSForDataCollectorsLoginDetails($message,$number);

					return Response::json(array(
						"status"=> 200,
						'message' => 'success'));

				}else{

					return Response::json(array(
						"status"=> 401,
						'message' => 'failed'));

				}
			}
		}else{
			//saving the edited info of the data collector
			$update = DB::table('data_collectors')
	            					->where('id', $id)
	            						->update(array(
	            							'first_name' => $fields['first_name'],
	            							'last_name' => $fields['last_name'],
	            							'gender' => $fields['gender'],
	            							'employed_date_as_teacher' => $fields['employed_date_as_teacher'],
	            							'employed_date_as_headteacher' => $fields['employed_date_as_headteacher'],
	            							'employed_date_as_circuit_supervisor' => $fields['employed_date_as_circuit_supervisor'],
	            							'period' => $fields['period'],
	            							'email' => $fields['email'],
	            							'type' => $fields['type'],
	            							'country_id' => $fields['country_id'],
	            							'region_id' => $fields['region_id'],
	            							'district_id' => $fields['district_id'],
	            							'circuit_id' => $fields['circuit_id']));

	           if ($update) {

	           		WhoEditedWhat::create(array('user_id' => Auth::user()->id, 
										 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
										 'what_was_edited' => "Data Collector with name: ".$fields['first_name']." ".$fields['last_name']."was edited by: ".Auth::user()->firstname.' '.Auth::user()->lastname));



	           		return Response::json(array(
												"status"=> 200,
												'message' => 'success'));
	           }else{
	           	
	           		return Response::json(array(
												"status"=> 401,
												'message' => 'failed'));
	           }

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteDataCollector($id)
	{
       
       $data_collector = DataCollector::find($id);

       $data_collector_type = $data_collector->type;

       $data_collector_circuit = $data_collector->circuit_id;

       $what_was_deleted = $data_collector->first_name.' '.$data_collector->last_name;

       if ($data_collector_type == 'circuit_supervisor') {

       	 $deleteDataCollector = DataCollector::find($id)->delete();
       	 $deleteDataCollectorSchool = DB::table('schools')->where('supervisor_id', $id)->delete();
         //$deleteDataCollectorUnderTheDeletedDatacollector = DB::table('data_collectors')->where('circuit_id', $data_collector_circuit)->delete();
         $deleteDataCollectorGCMID = DB::table('gcm_ids')
         							->where('data_collector_id', $id)
         							->where('type_of_data_collector', $data_collector_type)->delete();

       }else{

       	 	 $deleteDataCollector = DataCollector::find($id)->delete();
       	 	 $deleteDataCollectorSchool = DB::table('schools')->where('headteacher_id', $id)->delete();
       	 	 $deleteDataCollectorGCMID = DB::table('gcm_ids')
         							->where('data_collector_id', $id)
         							->where('type_of_data_collector', $data_collector_type)->delete();


       }

        if ($deleteDataCollector) {

        	$fields = array(
		 			'user_id' => Auth::user()->id,
		 			'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
		 			'what_was_deleted' =>$what_was_deleted.' a Data Collector was deleted and everything under it by: '.Auth::user()->firstname.' '.Auth::user()->lastname);


		 $saveWhoDeletedASchool = WhoDeletedWhat::create($fields);


        	return 'success';
        }else{
        	return 'failed';
        }
	}

	

	public function apiLogin()
	{
		//getting the data posted to the api
		$data = Input::all();

		if (!empty($data)) {
			$required_fields = array("phone_number"=>"",
									 "universal_code"=>"",
									 "unique_code"=>"");

			//accessing the various parameters received
			$res = Helper::malformedQueryCheck($required_fields,$data);

			$message = "Bad Request, check out your fields";
			$code = 400;
			if(!$res) return Helper::apiRequestError($message,$code);

			$phone_number = trim($data['phone_number']);
			$universal_code = trim($data['universal_code']);
			$unique_code = trim($data['unique_code']);
			return $userData = AndroidAPIRequest::apiDataCollectorsLogin($phone_number,$unique_code,$universal_code);
		}else{
			$message = 'You have not provided all the required parameters';
			$errorCode = 401;
			return Helper::apiRequestError($message,$errorCode);
		}
		
	}


	//retrieving a list of schools base on data collector
	public function DataCollectorsSchoolsToReportOn()
	{
		//getting details of the posted device for pushing notifications
		// $data = json_decode(@file_get_contents('php://input'));

		// Log::info('API Request to get Data Collector Schools', array('context' => $data));
	
		// $data = Input::all();
		// $data_collector_id = $data->data_collector_id;
		// $data_collector_type = $data->data_collector_type;

		$data_collector_id = Input::get('data_collector_id');

		$data_collector_type = Input::get('data_collector_type');

		if ($data_collector_type == 'circuit_supervisor') {
			
			$schools = DB::table('schools')->where('supervisor_id',$data_collector_id)->get();

			return Response::json(array("status"=>200,
										"message"=>"OK",
										"schools"=> $schools));
		}else{

			$schools = DB::table('schools')->where('headteacher_id',$data_collector_id)->first();
			
			return Response::json(array("status"=>200,
										"message"=>"OK",
										"schools"=> $schools));
		}
		
  

	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function all()
	{
        $country = 1;
		return $userData = DataCollectorsCustomClass::allDataCollectors($country);
	}

	public function oneSupervisorsOrHeadteachers($value='')
	{
		# code...
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function nationalSupervisorsOrHeadteachers($type)
	{	
		//$type = Input::get('type');
		$country = Input::get('country',1);
		return $DataCollectors = DataCollectorsCustomClass::allSupervisorsOrallHeadteachers($country,$type);
	}

	public function allRegionalDataCollectors($region)
	{
		$regionID = 2;
		return $DataCollectors = DataCollectorsCustomClass::allDataCollectorsForARegion($region);
	}

	public function regionalSupervisorsOrHeadteachers()
	{
		$region = 1;
		$type = 'circuit_supervisor';
		return $DataCollectors = DataCollectorsCustomClass::allRegionalSupervisorsOrHeadteachers($region,$type);
	}

	public function allDistrictDataCollectors()
	{
		$districtID = 1;
		return $DataCollectors = DataCollectorsCustomClass::allDataCollectorsForADistrict($districtID);
	}

	public function districtSupervisorsOrHeadteachers()
	{
		$district = 1;
		$type = 'circuit_supervisor';
		return $DataCollectors = DataCollectorsCustomClass::allDistrictSupervisorsOrHeadteachers($district,$type);
	}

	public function allCircuitDataCollectors()
	{
		$circuitID = 1;
		return $DataCollectors = DataCollectorsCustomClass::allDataCollectorsForACircuit($circuitID);
	}

	public function circuitSupervisorsOrHeadteachers()
	{
		$circuit = 1;
		$type = 'circuit_supervisor';
		return $DataCollectors = DataCollectorsCustomClass::allCircuitSupervisorsOrHeadteachers($circuit,$type);
	}

	public function allSchoolDataCollectors()
	{
		$schoolID = 1;
		return $DataCollectors = DataCollectorsCustomClass::allDataCollectorsForASchool($schoolID);
	}

	public function schoolSupervisorOrHeadteacher()
	{
		$school = 1;
		$type = 'circuit_supervisor';
		return $DataCollectors = DataCollectorsCustomClass::schoolSupervisorOrHeadteacher($school,$type);
	}



}
