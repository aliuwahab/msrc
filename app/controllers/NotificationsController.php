   <?php

// use DataCollectorsController;

class NotificationsController extends BaseController {




	/**
	 * register the ID of an android device to enable push notifications.
	 * In order to send notifications
	 * @return Response
	 */
	public function registerDeviceIDFromAndroidAndSaveDetails()
	{
     	//getting details of the posted device for pushing notifications
		$deviceDetails = json_decode(@file_get_contents('php://input'));
  
		Log::info('The GCM Ddetails of the device is: ', array('context' => $deviceDetails));

		$fields = array(
					'phone_number' =>$deviceDetails->phone_number,
		          	'gcm_id' =>$deviceDetails->gcm_id,
		          	'data_collector_id' =>$deviceDetails->data_collector_id,
		          	'type_of_data_collector' =>$deviceDetails->type_of_data_collector,
		          	'country_id' =>1,
		          	'region_id' =>$deviceDetails->region_id,
		          	'district_id' =>$deviceDetails->district_id,
		          	'circuit_id' =>$deviceDetails->circuit_id);

				$saveGCMIDforANewDataCollector = GCMIDS::create($fields);

				if ($saveGCMIDforANewDataCollector) {

					$iDofNewlyRegisteredGCMIDforDataCollector = $saveGCMIDforANewDataCollector->id;
					$gcmIDofNewlyRegisteredGCMIDforDataCollector = $saveGCMIDforANewDataCollector->gcm_id;
					$country_id = $saveGCMIDforANewDataCollector->country_id;
					$region_id = $saveGCMIDforANewDataCollector->region_id;
					$district_id = $saveGCMIDforANewDataCollector->district_id;

					$devices = $gcmIDofNewlyRegisteredGCMIDforDataCollector;

					$getAllItinerariesForADataCollector = DB::table('itenary')
													->where('country_id',$country_id)
													->where('region_id',$region_id)
													->where('district_id',$district_id)->get();

					if (!empty($getAllItinerariesForADataCollector)) {
						
					 		$purpose = 'itinerary';
							$information = 'New Intinerary available';
							$message = self::messageToBeSentThroughNotification($purpose, $information);
							return self::packagesSendNotification($devices,$message);
							//return self::normalSendingOfNotification($devices,$message);
					}else{
							$purpose = 'no_itinerary';
							$information = 'No itineray available';
							$message = self::messageToBeSentThroughNotification($purpose, $information);
							//return self::normalSendingOfNotification($devices,$message);
							
							return self::packagesSendNotification($devices,$message);
					}

					
			}

}



public function sendMessageViaNotificationSystem()
{


	$message = Input::get("message");

	$ids_of_recipients = Input::get("recipients");

//	return $ids_of_recipients;


	foreach ($ids_of_recipients as $recipient) {

//		DataCollectorsController::sendingToSendSMSForDataCollectorsLoginDetails($message,$recipient);
		$message  = "Hello ".$message;


		$phone_number = $recipient['phone_number'];

//		echo "".$recipient."<br>";

		DataCollectorsController::sendingToSendSMSForDataCollectorsLoginDetails($message,$phone_number);

//		Queue::push(function($job) use($message,$phone_number){
//												DataCollectorsController::sendingToSendSMSForDataCollectorsLoginDetails($message,$phone_number);
//											$job->delete();
//		});

	}



	return Response::json(array("status"=>200, "message"=>"Messages sent succssfully"));




}



public static function messageToBeSentThroughNotification($purpose, $information)
{
	

	//the message to send to the devices
	return $purpose.", ".$information;

}

//this function calls davibennun/laravel-push-notification package,
//the laravel parckage for sending notifications to send the notification message
//passing the device(s) and the message
public static function packagesSendNotification($devices,$message)
{
	//device to send notification to
 	//$deviceToken = 'APA91bFRyul0gWg7J1QnSk_jHFBwgba94YJCPnBQ9js1gm5D6M2KRURkkZ7smBU6ivtGl8WlsfYHnnHBRbKWYrf3yeMeXIlSDEOyoEdpyzG51UHduaytezc9eRkgTK5aaii3ipr9Q4dz7ARVDMl_WzVLVju5sRC4YA';

	$sendStatus = $sendingNotificationStatus = PushNotification::app('M4DAPP')
									                ->to($devices)
									                ->send($message);


}



public static function ReceiveResponseOnInformationSentViaNotification()
{
		//getting details of the posted device for pushing notifications
		$infoResponse = json_decode(@file_get_contents('php://input'));
  
		Log::info('Response to Info request via notification: ', array('context' => $infoResponse));

		//exit();

		$fields = array(
						'data_collector_type' => $infoResponse->data_collector_type,
		 				'data_collector_id' => $infoResponse->data_collector_id,
		 				'country_id' => 1,
		 				'region_id' => $infoResponse->region_id,
		 				'district_id' => $infoResponse->district_id,
		 				'circuit_id' => $infoResponse->circuit_id,
		 				'response_message' => $infoResponse->message
		 				);

		$saveInfo = NotificationInfo::create($fields);

		if ($saveInfo) {
			return Response::json(array("status"=>200,
										"message"=>"Message Sent"));
		}
		
}




public function showAllMessagesSentBackByDataCollecttorsAsAResponseToAMessageSentViaNotification()
{
	//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		//return Dashboard::recentDataSubmittedByDataCollectorsOnASchool($queryString, $values);

		try {
				//querying base on the parameters posted which is contain in the $querystring variable
				return $recentMessages = DB::table('notification_info')->whereRaw($queryString, $values)->orderBy('id',"desc")->get();
	      			
	   	} catch (Exception $e) {

	      			return "Check the fields in your posted data";
	      			
	   }

}


public static  function recieveAndSendServerSideDatabaseChangesToClientSide($school_id,$data_type_submitted)
{
	$channel = 'dataChannel';
	$event = 'dataSubmitted';
	// $school_id = 1;
	// $data_type_submitted = 'schoolreportcard';
	
	return $notificationStatus =  self::sendPusherAPINotificationsToClientSide($channel,$event,$school_id,$data_type_submitted);

	//echo $notificationStatus;
}


public static function sendPusherAPINotificationsToClientSide($channel,$event,$school_id,$data_type_submitted)
{
	 // Send notification to Pusher

	$app_id = '85454';
	$app_key = '396cf3d207f8bb21224a';
	$app_secret = 'f42cd8db57a865bab606';
	$pusher = new Pusher( $app_key, $app_secret, $app_id );

	$pusher->trigger($channel,$event, array( 'school_id' => $school_id, $type = $data_type_submitted ));

    //$message = "This is just an example message!";
    // Pusherer::trigger($channel, $event, array( 'message' => $message ));
    // $message = "This is just an example message!";
    // Pusherer::trigger('my-channel', 'my-event', array( 'message' => $message ));
}


public static function normalSendingOfNotification($devices,$message)
{
	return self::testingNotificationFunction($devices,$message);
}	


//this function sends notifification to android devices 
//without any third party wrapper using curl connections
public static function testingNotificationFunction($registrationIds,$message)
{
		// API access key from Google API's Console
		define( 'API_ACCESS_KEY', 'AIzaSyB_I1EzjJtNy38rEkRDF9bAc0NNlC2hB-4' );

		$fields = array
		(
			'registration_ids' 	=> $registrationIds,
			'data'				=> $message
		);
		 
		 $headers = array
		(
			'Authorization: key='. API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ));

		$result = curl_exec($ch );

		curl_close( $ch );

		echo $result;

	}

	

}
