<?php

class SchoolProblemsController extends BaseController {

	public function stateOfSchoolSanitation()
	{

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolProlemsSectionUnderSchoolReportCard::stateOfSchoolSanitation($queryString, $values);

	}


	public function stateOfSchoolSecurity()
	{
			//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolProlemsSectionUnderSchoolReportCard::stateOfSchoolSecurity($queryString, $values);

	}


	public function stateOfSchoolRecreationalEquipment()
	{
			//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolProlemsSectionUnderSchoolReportCard::stateOfSchoolRecreationalEquipment($queryString, $values);

	}

	
	public function stateOfSchoolStructure()
	{
			//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolProlemsSectionUnderSchoolReportCard::stateOfSchoolStructure($queryString, $values);

	}


	public function stateOfSchoolFurniture()
	{
			//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolProlemsSectionUnderSchoolReportCard::stateOfSchoolFurniture($queryString, $values);

	}

	public function stateOfSchoolRecordsAndPerformance()
	{
			//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolProlemsSectionUnderSchoolReportCard::stateOfSchoolRecordsAndPerformance($queryString, $values);

	}



	public function stateOfSchoolAndCommunityRelations()
	{
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolProlemsSectionUnderSchoolReportCard::stateOfSchoolAndCommunityRelations($queryString, $values);

	}




	



}
