<?php

class SchoolReviewController extends BaseController {

	

	/**
	 * Takes the posted data from the android app on school visits
	 *
	 * @return Response
	 */
	public function ReceivesSchoolReviewDataSubmittedByAndroidApp()
	{
		$data = json_decode(@file_get_contents('php://input'));	
		Log::info('Just to see school review data', array('context' => $data));


		return SchoolReview::ReceivesSchoolReviewDataSubmittedByAndroidApp($data);
	}


	public function schoolReviewQuestionsList()
	{
		return $shool_review_questions = DB::table('school_review_questions')->select('question_category_reference','question')->get();
	}

}
