<?php

class SchoolVisitController extends BaseController {

	
	/**
	 * Takes the posted data from the android app on school visits
	 *
	 * @return Response
	 */
	public function ReceivesSchoolVisitsDataSubmittedByAndroidApp()
	{
		//return "You are about to submit school visits data";
		//$schoolVisitsData = Input::all();

		$schoolVisitsData = json_decode(@file_get_contents('php://input'));	
		Log::info('Just to see school visits data', array('context' => $schoolVisitsData));
		//exit();
		
		return SchoolVisitsAnswersRetrieval::ReceivesSchoolVisitsDataSubmittedByAndroidApp($schoolVisitsData);

		//returning a succesful data submitted response code
		// return Response::json(array("status"=>200,
		// 							"message"=>"Data Submitted"));
	}




   public function retrieveSchoolVisitsData()
   {

		//getting the posted query parameters
		$inputs = Input::all();

		//$question_category_reference = 'TA';

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		return SchoolVisitsAnswersRetrieval::retrieveSchoolVisitsData($queryString, $values);

	}





}
