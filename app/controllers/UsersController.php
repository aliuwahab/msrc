<?php
use Illuminate\Support\Facades\Queue;

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index() {

		return $administrators_at_this_level = CustomUser::allAdmin(Auth::user()->level,Auth::user()->level_id);
		
	}





	public function login() {

		$credentials = Input::all();



        $username = Input::get('username');
        $password = Input::get('password');
        $remember_me = Input::get('remember_me');

         $credentials = array('username' => $username,
         					 'password' => $password);

        //return $credentials;
		//authenticating a user
		$authentication = User::authenticate($credentials,$remember_me);

		if ($authentication != true) {

			// return Redirect::to('/admin')->with('message', 'Login Failed: Wrong Username Or Password');


			$response = Response::json(array("code"=> "401", "status"=>"Failed", "message" => "Wrong Username Or Password", "credentials_entered" => $credentials));


			return $response;


			//redirect with errors
			// echo "Wrong credentials";
			// exit();
		} else {


			$user = Auth::user();
			$last_login = date('l jS \of F Y h:i:s A');
			$user->last_login = $last_login;
			$user->save();


			$user = $user->toJson();

			// $user = json_encode($user);

			// return Redirect::route('admin.dashboard.index');


			$response = Response::json(array("code"=> "200", "status"=>"OK", "message" => "User logged in successfully", "user" => $user));


			return $response;


			//return $user;
			//returning details of the national data
			//return $userData = CustomUser::loginUserData($user->level,$user->level_id,$user->id);
			// return Redirect::route('admin.dashboard.index');
			//return Redirect::route('admin.dashboard.index');
			//Redirect::intended('admin.dashboard.index')->with('flash_message','You have been logged in');
			//Redirect::route(route, parameters, status, headers)
		}


	}



	public function showProfile()
	{
		

		if (Auth::user()) {
			
			$user = Auth::user()->toJson();

			$response = Response::json(array("code"=> "200", "status"=>"OK", "message" => "The Authenticated user", "user" => $user));

			return $response;

	
		}elseif (Session::has('authenticated_cs')){

			$response = Response::json(array("code"=> "200", "status"=>"OK", "message" => "The Authenticated circuit supervisor", "user" => Session::get("authenticated_cs")));

			return $response;

		}else{

			$response = Response::json(array("code"=> "401", "status"=>"Failed", "message" => "No Authenticated User or Circuit Suppervisor"));

			return $response;

		}

	}

	public function logout() {
		
		Session::flush();
		// return Redirect::to('/');
		// 
		
		$response = Response::json(array("code"=> "200", "status"=>"OK", "message" => "User Logged Out Successfully"));


		return $response;


	}
	

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store() {
		//return "You are about to create a user";
		//storing the information of the newly user
			//getting the posted details of the newly created data collector i.e headmaster or circuit supervisor

						$username = Input::get('username');
            			$firstname = Input::get('first_name');
            			$lastname = Input::get('other_names');
            			$gender = Input::get('gender');
            			$phone_number = Input::get('user_phone_number');
            			$email = Input::get('email');
            			$password = Input::get('password');
            			$password_confirmation = Input::get('confirm_password');
            			$level = Input::get('user_type');
            			$role = Input::get('admin_role');

                        if ($level == 'national'){
                          $level_id = 1;
                        }else{
            			    $temp= $level . '_id';
            			    $level_id = Input::get($temp);
                        }


		$fields = array(
			'username' => $username,
			'firstname' => $firstname,
			'lastname' =>$lastname,
			'gender' => $gender,
			'phone_number' => $phone_number,
			'email' => $email,
			'password' => $password,
			'password_confirmation' => $password_confirmation,
			'level' =>$level,
			'role' => $role,
			'level_id' => $level_id
		);

			//validating the input
		$validation = User::validate($fields);

		if ($validation !== true) {
			return $validation;
		}else{
			//Save into the newly created data collector
			$fields['password'] = Hash::make(Input::get('password'));
			if (User::create($fields)) {

				$salutation = 'Mr/Mrs';

				$recipientEmails = $email;
        		$subject = "Your mSRC Ghana Login Credentials";
        		$from = Auth::user()->email;
        		$pathToFile = null;

				$message = "Your mSRC login details are:
								 <br>Username: ".$username.
								 "<br> Password: ".$password.
								 ".<br> Login at www.msrcghana.org/admin. You can view and analysed data
								 on any school which falls under your domain";

        		$intro = 'You have been created as an admin on the mSRC school management system for Ghana.';

        		$confidentialFootnote = 'Please note, the information contained
        	here is confidential, as such should only be used for the purpose with which you are receiving it.';

				$data = array(
					'salutation' => $salutation,
					'content'=> $message,
					'intro'	=> $intro,
					'warning' => $confidentialFootnote
				);

				$sms_message = "Hello ".$fields['firstname'].", visit your email address: ".$fields['email']." to get your mSRC dashboard login credentials. Remember to change the default password provided, to anyone of your choice.";

				$number = $fields['phone_number'];
				//calling the function that will send data collector login details
				DataCollectorsController::sendingToSendSMSForDataCollectorsLoginDetails($sms_message,$number);
				
				SendEmail::sendEmailMessge($data,$from,$salutation,$recipientEmails,$pathToFile,$subject);

//				Queue::push(function($job) use($data,$from,$salutation,$recipientEmails,$pathToFile,$subject){
//													SendEmail::sendEmailMessge($data,$from,$salutation,$recipientEmails,$pathToFile,$subject);
//													$job->delete();
//				});



				return "success";

			}else{
				return 'It could not save, usernname already exists';
			}
		}
	}

	/**
	 * Display the specified resource.
	 * GET /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showUser($id) {
		
		return $userInfo = User::find($id);
	}


	public static function returnAllRequestedUsersByAdmin()
	{
		
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		$allusers = DB::table('users')->whereRaw($queryString, $values)->get();

		return Response::json(array( "users"=> $allusers));

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /user/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editUser($id) {
		//user to be edited
		return $userInfo = User::find($id);
	}


	public function showOtherUserProfile($id)
	{
		return $userInfo = User::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateUser() {

		$idOfUserToEdit = Input::get('id');
		//getting Details of data collector who is edited
		$datCollectorToEdit = DB::table('users')->where('id', $idOfUserToEdit)->first();
		$datCollectorToEditEmail = $datCollectorToEdit->email;
		$datCollectorToEditUsername = $datCollectorToEdit->username;
			//return "You are about to create a user";
		//storing the information of the newly user
			//getting the posted details of the newly created data collector i.e headmaster or circuit supervisor
		
		$level = Input::get('user_type');
        $role = Input::get('admin_role');

        if ($level == 'national'){
          $level_id = 1;
        }else{
		    $temp= $level . '_id';
		    $level_id = Input::get($temp);
        }

        $username = Input::get('username');
//        $password = Hash::make(Input::get('password'));
        $email = Input::get('email');


		$fields = array(
//			'username' => Input::get('username'),
			'firstname' => Input::get('first_name'),
			'lastname' => Input::get('other_names'),
			'gender' => Input::get('gender'),
			'phone_number' => Input::get('user_phone_number'),
			'email' => Input::get('email'),
			'level' => Input::get('user_type'),
			'role' => Input::get('admin_role'),
			'level_id' => $level_id
		);

		//$fields =Input::all();
		if (($datCollectorToEditEmail != $fields['email'])) {
				//validating the input
			$editAdminvalidate = User::editAdminvalidate($fields);

			if ($editAdminvalidate !== true) {

				return $editAdminvalidate;
				
			}else{
//				//Save into the newly created data collector
//				$password = $fields['password'] = Hash::make(Input::get('password'));
				$update = DB::table('users')
	            					->where('id', $idOfUserToEdit)
	            						->update(array(
//	            							'username' => $fields['username'],
	            							'firstname' => $fields['firstname'],
	            							'lastname' => $fields['lastname'],
	            							'gender' => $fields['gender'],
	            							'phone_number' => $fields['phone_number'],
	            							'email' => $fields['email'],
	            							'level' => $fields['level'],
	            							'role' => $fields['role'],
	            							'level_id' => $level_id
	 										));

				if ($update) {

					$salutation = 'Mr/Mrs';

					$recipientEmails = $email;
        			$subject = "Your MSRC Ghana Login Credentials";
        			$from = Auth::user()->email;
        			$pathToFile = null;

				$message = "Your MSRC login details updated:
								 <br>Username: ".$username.
								 ".<br> Login at www.msrcghana.org/admin. You can view and analysed data base
								 on any school which falls under your domain";

        		$intro = 'You have been created as an admin at the MSRC school managment system for Ghana.';

        		$confidentialFootnote = 'Please note, the information contained 
        	here is confidential, as such should only be use for the purpose with which you are receiving it.';

				$data = array(
					'salutation' => $salutation,
					'content'=> $message,
					'intro'	=> $intro,
					'warning' => $confidentialFootnote
				);
				
				SendEmail::sendEmailMessge($data,$from,$salutation,$recipientEmails,$pathToFile,$subject);

//				Queue::push(function($job) use($data,$from,$salutation,$recipientEmails,$pathToFile,$subject){
////													SendEmail::sendEmailMessge($data,$from,$salutation,$recipientEmails,$pathToFile,$subject);
////													$job->delete();
////				});
/// 

				return "success";

				}else{
					return 'It could not save, username already exists';
				}
			}
		}else{
				$password = $fields['password'] = Hash::make(Input::get('password'));
				$update = DB::table('users')
	            					->where('id', $idOfUserToEdit)
	            						->update(array(
	            							'firstname' => $fields['firstname'],
	            							'lastname' => $fields['lastname'],
	            							'gender' => $fields['gender'],
	            							'phone_number' => $fields['phone_number'],
	            							'password' => $password,
	            							'level' => $fields['level'],
	            							'role' => $fields['role'],
	            							'level_id' => $level_id
	 										));
	           if ($update) {
					return "success";
				}else{
					return 'It could not save, username already exists';
				}
		}
	}




	public function editUserPassword() {

		$idOfUserToEdit = Input::get('id');

		$idOfEmailToEdit = Input::get('email');

		$password = Input::get('password');


		$fields = array(
        			'password' => Input::get('password')
        		);


		$editUserPassword = User::editPasswordValidate($fields);



		//getting Details of data collector who is edited
		$datCollectorToEdit = DB::table('users')->where('id', $idOfUserToEdit)->where('email', $idOfEmailToEdit)->first();


		if(count($datCollectorToEdit) > 0){


			if ($editUserPassword !== true) {

				return $editUserPassword;

			}else{

				//Save into the newly created data collector
				$password = $fields['password'] = Hash::make(Input::get('password'));

				$update = DB::table('users')->where('id', $idOfUserToEdit)->update(array('password' => $password));

				if ($update) {

					$salutation = 'Mr/Mrs';

					$recipientEmails = $idOfEmailToEdit;
					$subject = "Your mSRC Ghana Login Credentials";
					$from = Auth::user()->email;
					$pathToFile = null;

					$message = "Your mSRC login details are:
								 <br>Username: ".$idOfEmailToEdit.
						"<br> Password: ".$password.
						".<br> Login at www.msrcghana.org/admin. You can view and analyse data
								 on any school which falls under your domain";

					$intro = 'Your admin password on the mSRC school management system has been edited.';

					$confidentialFootnote = 'Please note, the information contained here is confidential, as such should only be use for the purpose with which you are receiving it.';

					$data = array(
						'salutation' => $salutation,
						'content'=> $message,
						'intro'	=> $intro,
						'warning' => $confidentialFootnote
					);

					SendEmail::sendEmailMessge($data,$from,$salutation,$recipientEmails,$pathToFile,$subject);


					return "success";


				}else{

					return 'Password could not be changed, please try again';

				}
			}

		}else{

			return 'The user does not exist';
		}


	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteUser($id) {

		$deleteUser = User::find($id)->delete();
		if ($deleteUser) {
				return "success";
			}else{
				return 'failed';
			}
	}

}