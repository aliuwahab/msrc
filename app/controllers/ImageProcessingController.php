<?php

use Illuminate\Support\Facades\Log;

class ImageProcessingController extends BaseController {



    public function saveRequiredImageSubmittedViaAndroidApp()
    {

        Log::info('Details of posted Info on : ', array('RequiredImageDetails' => Input::all()));

        $imageFile = Input::file('image');
        //$image =Input::get('image');
        //$image = Image::make(Input::file('image')->getRealPath());
        $school_reference_code = Input::get('school_reference_code');
        $image_title = Input::get('title');
        $image_description = Input::get('description');
        $year = date('Y');
        $country_id = 1;
        $region_id = Input::get('region_id');
        $district_id = Input::get('district_id');
        $circuit_id = Input::get('circuit_id');
        $school_id = Input::get('school_id');
        $lat = Input::get('lat','No lat given');
        $long = Input::get('long','No long given');
        $week_number = Input::get('week_number',1);
        $term = Input::get('term','first_term');
        $data_collector_type = Input::get('data_collector_type','head_teacher');
        $data_collector_id = Input::get('data_collector_id',1);
        $image_category_type = Input::get('image_category_type');



        $save_image_details =  self::savingFileToInAppPublicDirectory($imageFile,$school_reference_code,$image_title,$image_description,$year,$country_id,$region_id,$district_id,$circuit_id,$school_id);


        Log::info('Saved Image Details: ', array('RequiredImageDetails' => $save_image_details));

        if (!$save_image_details) {
            return Response::json(array("status"=>401,
                "content" => "Error In Saving Image",
                "message"=>"Error In Saving Image"));
        }else {

            $save_image_details = $save_image_details->getData();
            $only_image_details = $save_image_details->image_details;

            Log::info('Only Image Details from JSON RETURNED: ', array('RequiredImageDetails' => $only_image_details));

            $only_image_details = json_decode($only_image_details);

            $fields = array(
                'image_id' => $only_image_details->image_id,
                'image_category_type' => $image_category_type,
                'lat' => $lat,
                'long' => $long,
                'school_reference_code' => $school_reference_code,
                'year' => $year,
                'country_id' => $country_id,
                'region_id' => $region_id,
                'district_id' => $district_id,
                'circuit_id' => $circuit_id,
                'week_number' => $week_number,
                'term' => $term,
                'data_collector_type' => $data_collector_type,
                'data_collector_id' => $data_collector_id,
                'school_id' => $school_id);


            $image_validation = RequiredImage::validate($fields);

            if ($image_validation != true) {

                return false;

            } else {

                $saveImageDetails = RequiredImage::create($fields);

                if ($saveImageDetails) {

                    return Response::json(array("status"=>200,
                        "content" => "Image Saved Successfully",
                        "message"=>"Image Saved Successfully",
                        "image_details" => $saveImageDetails));
                }

                return false;


            }
        }


    }








	//"This receives image posted by android";
	public function receiveImagePostedByAndroidApp()
	{
			$imageFile = Input::file('image');
			//$image =Input::get('image');
			//$image = Image::make(Input::file('image')->getRealPath());
			$school_reference_code = Input::get('school_reference_code');
			$image_title = Input::get('title');
			$image_description = Input::get('description');
			$year = date('Y');
			$country_id = 1;
			$region_id = Input::get('region_id');
			$district_id = Input::get('district_id');
			$circuit_id = Input::get('circuit_id');
			$school_id = Input::get('school_id');

			Log::info('Details of school_id is: ', array('context' => $school_id));
			Log::info('Details of region_id is: ', array('context' => $region_id));
			Log::info('Details of image_title is: ', array('context' => $image_title));
			Log::info('Details of image_description is: ', array('context' => $image_description));
			Log::info('Details of school_reference_code is: ', array('context' => $school_reference_code));


		return self::savingFileToInAppPublicDirectory($imageFile,$school_reference_code,$image_title,$image_description,$year,$country_id,$region_id,$district_id,$circuit_id,$school_id);


	}




	//this should save the file to the public folder and return the path to the file
	public static function savingFileToInAppPublicDirectory($imageFile,$school_reference_code,$image_title,$image_description,$year,$country_id,$region_id,$district_id,$circuit_id,$school_id)
	{		

				//checking to see if there is a file posted
				if (file_exists($imageFile)){


						$image = Image::make($imageFile->getRealPath());

						//directory to save images into
						$directoryPath = public_path(). '/schools_data_images/'.$region_id.'/';
						//$directoryPath = public_path(). 'schools_data_images/'.$region_id.'/';

						//this check if directory exist, if not it creates one
						File::exists($directoryPath) or File::makeDirectory($directoryPath);
		  				//gettting original file name and time in order to save file in that name
						$nameToSaveImageWith = time() . '-' .$imageFile->getClientOriginalName();
						//giving the absolute path to the image including its name
						$imageFileOnLocalServer = public_path('/schools_data_images/'.$region_id.'/'.$nameToSaveImageWith);


						// rotate image 90 degrees clockwise
						$image->rotate(-90);

						//resizing image to a specific size
						//$image->resize(610, 370)->insert(public_path().'/watermark.png');
						$image->insert(public_path().'/watermark.png');


						$imageDetailsSaveByIntervention = $image->save($imageFileOnLocalServer);

						//checking if the image and the name of image exist
						if (file_exists($imageFileOnLocalServer) && !empty($nameToSaveImageWith)) {

							$region = DB::table('regions')->where('id', $region_id)->first();
							//calling the function to save file at S3
							$imageDetailsAtS3 = self::savingFilesTOAmazonS3($imageFileOnLocalServer,$nameToSaveImageWith,$region);

							if (!empty($imageDetailsAtS3)) {

								$image_s3_url = $imageDetailsAtS3['ObjectURL'];

								$deleteLocalCopyOfFile = self::deleteFileFromAGivenPath($imageFileOnLocalServer);

								return $savingImageDetailsIntoDatabase = self::savingFileDetailsIntoADatabaseTable($image_s3_url,$image_title,$image_description,$school_reference_code,$year,$country_id,$region_id,$district_id,$circuit_id,$school_id);
								
							}else{
								return "No file path from s3";
							}

						}else{
							return "File does not exist";
						}
		}else{
			return "No Image";
		}


	}



	//this function saves the file at S3 and returns the URL
	public static function savingFilesTOAmazonS3($imageFileOnLocalServer,$nameToSaveImageWith,$region)
	{
		//calling the function in the image_processing.php file to save to S3
		return imageProcessingHelper::savingFilesTOAmazonS3($imageFileOnLocalServer,$nameToSaveImageWith,$region);
	}

	public static function savingFileDetailsIntoADatabaseTable($image_s3_url,$image_title,$image_description,$school_reference_code,$year,$country_id,$region_id,$district_id,$circuit_id,$school_id)
	{
		return imageProcessingHelper::savingFileDetailsIntoADatabaseTable($image_s3_url,$image_title,$image_description,$school_reference_code,$year,$country_id,$region_id,$district_id,$circuit_id,$school_id);
	}


	//this function deletes the file whose path is given
	public static function deleteFileFromAGivenPath($imageFileOnLocalServer)
	{
		return imageProcessingHelper::deleteFileFromAGivenPath($imageFileOnLocalServer);	
	}


	//bucket name on amazon s3 is: m4dimages
	public function ReturnImagesBaseOnAQuery()
	{

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();
		
		return imageProcessingHelper::ReturnImagesBaseOnAQuery($queryString,$values);
	}





    public function ReturnRequiredImagesBaseOnAQuery()
    {

        //getting the posted query parameters
        $inputs = Input::all();

        //return array_keys($inputs) of the posted data;
        $values = array_values($inputs);

        //declaring a query string to query with in the whereRaw method
        $queryString = '';

        //puting the key into the query parameters and appending the word 'and';
        foreach ($inputs as $key => $value) {
            $queryString .= $key.' = ? and ';
        }
        //removing the last and which will be appended
        $queryString = substr($queryString, 0, -4);
        //return SchoolEnrolment::whereRaw($queryString, $values)->get();

        return imageProcessingHelper::ReturnRequiredImagesBaseOnAQuery($queryString,$values);


    }
	

	

}
