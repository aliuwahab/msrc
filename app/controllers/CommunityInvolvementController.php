<?php

class CommunityInvolvementController extends \BaseController {

		

		public static function smsReceived()
		{
				
				//receive the sms data forwarded to this app
				$data = Input::all();

				Log::info('SMS', array('context' => $data));


				$sender = $data['sender'];
				$when = $data['when'];
				$message = $data['message'];

				//Log::info('Just to see whether I get the receiver', array('context' => $receiver));

				//exploding the message base on space
				$getPurposeCode = explode(' ', $message,3);

				//retrieving the code which defines the purpose of message
				$purpose = $getPurposeCode[0];


				Queue::push(function($job) use($purpose,$sender,$when,$message){
													CommunitySMSReportsAndInfomationRequest::checkWhatSenderWantToDo($purpose,$sender,$when,$message);
													$job->delete();
				});
				

		}


		//retrieving the sms sent on schools for national, regional
		//district, circuit and schools base on the query parameter
		public function QueryToReturnSmsSentByPublic()
		{

		//getting the posted query parameters
		$inputs = Input::all();
		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

        return CommunitySMSReportsAndInfomationRequest::retrievingSMSBaseOnCoverage($queryString, $values);


		}



	
}