<?php
namespace App\Http\Controllers;
use App\MkPupilAggregatedData;
use Illuminate\Http\Request;
class MkPupilAggregatedDataController extends BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {

	    dd($request->all());

        $data = $request->json()->all();

        $MkPupilAggregatedData = new MkPupilAggregatedData();
        $MkPupilAggregatedData['academic_term'] = $data['academic_term'];
        $MkPupilAggregatedData['academic_year'] = $data['academic_year'];
        $MkPupilAggregatedData['grade'] = $data['grade'];
        $MkPupilAggregatedData['no_student_grade'] = $data['no_student_grade'];
        $MkPupilAggregatedData['no_student_assessed'] = $data['no_student_assessed'];
        $MkPupilAggregatedData->save();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}

}
