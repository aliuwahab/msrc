<?php

use Illuminate\Support\Facades\DB;

class AnalysisController extends BaseController {




	public function percentageAnalysisOfAverageTeacherAttendance()
	{

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


		return DataAnalysis::percentageAnalysisOfTeacherAttendance($queryString, $values);



	}







	public function findTeachersWhosAttendanceHasNotBeenSubmmitted()
	{


		return DataAnalysis::findTeachersWhosAttendanceHasNotBeenSubmitted(Input::all());


	}


	//update the weekly_totals for enrollment
	public function updateTotalEnrollmentInWeeklyTable()
	{

		return DataAnalysis::toReCalculateStudentEnrolmentTotalsForAWeek(Input::all());

	}




	//Updating books for a whole school in a term
	public function UpdateTotalsOfTextBooksForASchoolInATerm()
	{

		$posted_textbook_totals = Input::all();


		foreach ($posted_textbook_totals as $school_textbooks_for_a_term) {

		    $find_school_data_on_termly_totals  = DB::table('termly_totals')
                ->where('district_id',$school_textbooks_for_a_term['district_id'])
                ->where('school_id',$school_textbooks_for_a_term['school_id'])
                ->where('term',$school_textbooks_for_a_term['term'])
                ->where('year',$school_textbooks_for_a_term['year'])
                ->first();

            if (count($find_school_data_on_termly_totals) > 0) {

                $update = DB::table('termly_totals')
                    ->where('district_id',$school_textbooks_for_a_term['district_id'])
                    ->where('school_id',$school_textbooks_for_a_term['school_id'])
                    ->where('term',$school_textbooks_for_a_term['term'])
                    ->where('year',$school_textbooks_for_a_term['year'])
                    ->update(array(
                        'num_ghanaian_language_books' => $school_textbooks_for_a_term['num_ghanaian_language_books'],
                        'num_english_books' => $school_textbooks_for_a_term['num_english_books'],
                        'num_maths_books' => $school_textbooks_for_a_term['num_maths_books']));

            }else{

            }
		}


        return Response::json(array("status"=>200,
            "content" => "Textbook Totals Updated Successfully",
            "message"=>"Textbook Totals Updated Successfully"));

	}


    public function UpdateTotalsOfPupilsPerformanceForASchoolInATerm()
    {



        $posted_pupil_performance_totals = Input::all();


        foreach ($posted_pupil_performance_totals as $pupil_performance_total_for_a_term) {

            $find_school_data_on_termly_totals  = DB::table('termly_totals')
                ->where('district_id',$pupil_performance_total_for_a_term['district_id'])
                ->where('school_id',$pupil_performance_total_for_a_term['school_id'])
                ->where('term',$pupil_performance_total_for_a_term['term'])
                ->where('year',$pupil_performance_total_for_a_term['year'])
                ->first();

            if (count($find_school_data_on_termly_totals) > 0) {

                $update = DB::table('termly_totals')
                    ->where('district_id',$pupil_performance_total_for_a_term['district_id'])
                    ->where('school_id',$pupil_performance_total_for_a_term['school_id'])
                    ->where('term',$pupil_performance_total_for_a_term['term'])
                    ->where('year',$pupil_performance_total_for_a_term['year'])
                    ->update(array(
                        'average_ghanaian_language_score' => $pupil_performance_total_for_a_term['average_ghanaian_language_score'],
                        'ghanaian_language_above_average' => $pupil_performance_total_for_a_term['ghanaian_language_above_average'],
                        'average_english_score' => $pupil_performance_total_for_a_term['average_english_score'],
                        'english_above_average' => $pupil_performance_total_for_a_term['english_above_average'],
                        'average_maths_score' => $pupil_performance_total_for_a_term['average_maths_score'],
                        'maths_above_average' => $pupil_performance_total_for_a_term['maths_above_average']));

            }else{


            }
        }


        return Response::json(array("status"=>200,
            "content" => "Pupils Performance Totals Updated Successfully",
            "message"=>"Pupils Performance Totals Updated Successfully"));




	}



	//update the weekly_totals for enrollment
	public function updateTotalAttendanceInWeeklyTable()
	{

		return DataAnalysis::toReCalculateStudentAttendanceTotalsForAWeek(Input::all());

	}


	//update the weekly_totals for teacher attendance
	public function updateTotalTeacherAttendanceInWeeklyTable()
	{

		return DataAnalysis::toReCalculateTeacherAttendanceTotalsForAWeek(Input::all());

	}




	public static function updateDataScopes($changed_scope,$school_id,$school_ges_code,$region_id,$district_id,$circuit_id,$country_id)
	{

		switch ($changed_scope) {

			case "school":
				$update_school_normal_enrolment_details = DB::table('school_enrolments')->where('school_id', $school_id)->update(array(
					'school_reference_code' => $school_ges_code,
					'region_id' => $region_id,
					'district_id' => $district_id,
					'circuit_id' => $circuit_id,
					'country_id' => $country_id));
				break;

			case "circuit":
				$update_school_normal_enrolment_details = DB::table('school_enrolments')->where('school_id', $school_id)->update(array(
					'school_reference_code' => $school_ges_code,
					'region_id' => $region_id,
					'district_id' => $district_id,
					'circuit_id' => $circuit_id,
					'country_id' => $country_id));
				break;

			case "district":
				$update_school_normal_enrolment_details = DB::table('school_enrolments')->where('school_id', $school_id)->update(array(
					'school_reference_code' => $school_ges_code,
					'region_id' => $region_id,
					'district_id' => $district_id,
					'circuit_id' => $circuit_id,
					'country_id' => $country_id));
				break;

			case "region":
				$update_school_normal_enrolment_details = DB::table('school_enrolments')->where('school_id', $school_id)->update(array(
					'school_reference_code' => $school_ges_code,
					'region_id' => $region_id,
					'district_id' => $district_id,
					'circuit_id' => $circuit_id,
					'country_id' => $country_id));
				break;
		}

	}







    //update the weekly_totals by re-calculating
    public function UpdateTotalWeeklyTable()
    {

        $terms_array = array("first_term","second_term","third_term");
        $scope = Input::get('scope');
        $scope_level_id = Input::get('scope_level_id');
        $year = Input::get('year');

        Log::info('Year', array('context' => $year));
        Log::info('Scope', array('context' => $scope));
        Log::info('Scope ID', array('context' => $scope_level_id));


        switch ($scope) {
            case "national":

//                $schools_id = DB::table('schools')->lists('id');
                foreach ($terms_array as $term) {
                    School::chunk(200, function($schools) use($year,$term)
                    {
                        Log::info('SCHOOLS', array('context' => $schools));
                        foreach ($schools as $school)
                        {
                            DataAnalysis::UpdateDistrictWeeklyTotalsByDeletingExisting($year, $term, $school->id);
                        }
                    });

                }
                return Response::json(array('code' => 200, 'message' => 'Updates Completed in the country'));
                break;

            case "regional":

                foreach ($terms_array as $term) {
                    School::where('region_id',$scope_level_id)->chunk(200, function($schools) use($year,$term)
                    {
                        Log::info('SCHOOLS', array('context' => $schools));
                        foreach ($schools as $school)
                        {
                            DataAnalysis::UpdateDistrictWeeklyTotalsByDeletingExisting($year, $term, $school->id);
                        }
                    });
                }
                return Response::json(array('code' => 200, 'message' => 'Updates Completed in the region'));
                break;

            case "district":

                foreach ($terms_array as $term) {
                    DB::table('schools')->where('district_id',$scope_level_id)->chunk(200, function($schools) use($year,$term)
                    {
                        Log::info('SCHOOLS', array('context' => $schools));
                        foreach ($schools as $school)
                        {
                            DataAnalysis::UpdateDistrictWeeklyTotalsByDeletingExisting($year, $term, $school->id);
                        }
                    });

                }
                return Response::json(array('code' => 200, 'message' => 'Updates Completed in the district'));
                break;

            case "circuit":

                foreach ($terms_array as $term) {
                    School::where('circuit_id',$scope_level_id)->chunk(200, function($schools) use($year,$term)
                    {
                        Log::info('SCHOOLS', array('context' => $schools));
                        foreach ($schools as $school)
                        {
                            DataAnalysis::UpdateDistrictWeeklyTotalsByDeletingExisting($year, $term, $school->id);
                        }
                    });
                }
                return Response::json(array('code' => 200, 'message' => 'Updates Completed in the circuit'));
                break;

            case "school":
                $school_id = DB::table('schools')->where('id', $scope_level_id)->first();
                foreach ($terms_array as $term) {
                    DataAnalysis::UpdateDistrictWeeklyTotalsByDeletingExisting($year, $term, $school_id->id);
                }
                return Response::json(array('code' => 200, 'message' => 'Updates Completed in the school'));
                break;
            default:


        }

        return DataAnalysis::UpdateDistrictWeeklyTotalsByDeletingExisting(Input::all());

    }







}
