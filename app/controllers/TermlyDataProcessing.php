<?php

class TermlyDataProcessing extends BaseController {

	

	public function deleteTextBooksSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

        $inputs = Input::all();
        //return array_keys($inputs) of the posted data;
        $values = array_values($inputs);

        //declaring a query string to query with in the whereRaw method
        $queryString = '';

        //puting the key into the query parameters and appending the word 'and';
        foreach ($inputs as $key => $value) {
            $queryString .= $key.' = ? and ';
        }
        //removing the last and which will be appended
        $queryString = substr($queryString, 0, -4);


//		DB::table('class_textbooks')->where('id', $db_unique_id_of_data_to_delete)->delete();
        $delete = DB::table('class_textbooks')->whereRaw($queryString, $values)->delete();

        if ($delete) {

            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['num_ghanaian_language_books' => 0]);
            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['num_english_books' => 0]);
            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['num_maths_books' => 0]);

            return "success";
        }

        return "Failed";
		
		
	}



	public function deletePupilsPerformanceSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

        $inputs = Input::all();
        //return array_keys($inputs) of the posted data;
        $values = array_values($inputs);

        //declaring a query string to query with in the whereRaw method
        $queryString = '';

        //puting the key into the query parameters and appending the word 'and';
        foreach ($inputs as $key => $value) {
            $queryString .= $key.' = ? and ';
        }
        //removing the last and which will be appended
        $queryString = substr($queryString, 0, -4);


//		DB::table('pupils_exams_performance')->where('id', $db_unique_id_of_data_to_delete)->delete();
		$delete = DB::table('pupils_exams_performance')->whereRaw($queryString, $values)->delete();

        if ($delete) {

            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['average_ghanaian_language_score' => 0]);
            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['ghanaian_language_above_average' => 0]);

            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['average_english_score' => 0]);
            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['english_above_average' => 0]);

            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['average_maths_score' => 0]);
            DB::table('termly_totals')->whereRaw($queryString, $values)->update(['maths_above_average' => 0]);

            return "success";
        }


        return "Failed";
		
		
	}


	public function deleteRecordBooksSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('school_records_performance')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}


	public function deleteSupportTypeSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('support_type')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}



	public function deleteGrantSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('grant_type_tranches')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}





	public function deleteSanitationSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('sanitation')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}



	public function deleteSecuritySubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('security')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}


	public function deleteSportsAndRecreationalSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('equipment_recreational')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}



	public function deleteSchoolStructureSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('school_structure')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}


	public function deleteFurnitureSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('funiture')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}



	public function deleteCommunityRelationsSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('school_community_relationship')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}


	public function deleteMeetingsHeldSubmittedData()
	{

		$db_unique_id_of_data_to_delete = Input::get('id');

		DB::table('schools_meeting')->where('id', $db_unique_id_of_data_to_delete)->delete();

		return "success";
		
		
	}



	public function deleteGeneralIssuesSubmittedData()
	{



                $inputs = Input::all();
                //return array_keys($inputs) of the posted data;
                $values = array_values($inputs);

                //declaring a query string to query with in the whereRaw method
                $queryString = '';

                //puting the key into the query parameters and appending the word 'and';
                foreach ($inputs as $key => $value) {
                    $queryString .= $key.' = ? and ';
                }
                //removing the last and which will be appended
                $queryString = substr($queryString, 0, -4);


        		$delete = DB::table('general_school_situation')->whereRaw($queryString, $values)->delete();

                 if ($delete) {
                 return "success";
                 }


		
		
	}





	





}
