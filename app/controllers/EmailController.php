<?php
//sending attachements as email controller
//use Vendor\flowjs;
use Flow\Basic;
use Flow\Config;
use Flow\ConfigInterface;
use Flow\File;
use Flow\FustyRequest;
use Flow\Request;
use Flow\RequestInterface;
use Flow\Uploader;

class EmailController extends BaseController {



public function uploadEmailAttachment()
{
	//getting the details of the file that is posted by
	// the flow js library as email attachement
	$filename = Input::get('flowFilename');
	$exploadingToGetFileExtension = explode('.', $filename);
	$filextension = end($exploadingToGetFileExtension);
	$final_file_destination = './uploads/'.time().'.'.$filextension;

	if (\Flow\Basic::save($final_file_destination, './chunks_temp_folder')) {
	   	//file saved successfully and can be accessed at './final_file_destination'
	 	return $final_file_destination;

	} else {
	  // This is not a final chunk or request is invalid, continue to upload.
	  $response = Response::make('', 404);
	  return $response;
	}
}


	/**
	 * Send email with analytics attached.
	 *
	 * @return Response
	 */
	public function sendEmailWithAttachment()
	{
        	
        	$intro = 'This report is generated from the M4D platform, managed by UNICEF and GES';

        	$confidentialFootnote = 'Please note, the information contained 
        	here is confidential, as such should only be use for the purpose with which you are receiving it';
			
			$recipientEmails = Input::get('emails');
        	$subject = Input::get('email_subject');
        	$from = Input::get(Auth::user()->email, 'no-reply@msrc.com');
        	$message = Input::get('email_message');

			// $recipientEmails = 'aliu@techmerge.org';
			// $from = 'aliu@techmerge.org';
			// $subject = 'A report on GES';
			$salutation = 'Mr/Mrs';
			$data = array(
				'salutation' => $salutation,
				'content'=> $message,
				'intro'	=> $intro,
				'warning' => $confidentialFootnote
			);

			$pathToFile = Input::get('file_path');
			// echo $pathToFile;
			// exit();
			
			//$pathToFile = public_path().'/quran.jpg';



			if (!empty($recipientEmails) && !empty($pathToFile)) {
				
				SendEmail::sendEmailMessge($data,$from,$salutation,$recipientEmails,$pathToFile,$subject);

				unlink($pathToFile);

				return "success";

			}else{

				return "Check Your email(s) and the attachment";
			}
	}


	public function RemoveFileUploaded()
	{
		$pathToFile = Input::get('file_path');

//		return $pathToFile;

		unlink($pathToFile);

		return "success";
	}

	

}
