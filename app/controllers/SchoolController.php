<?php

class SchoolController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('schools.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('schools.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$districtName = Input::get('district_name', 'District Name');
		$fields = array(
			'ges_code' => Input::get('ges_code'),//code by GES
//			'school_code' => Helper::generateSchoolUniqueCode($table  = 'schools', $column = 'school_code'),
			'school_code' => Helper::generateRandomString(4),
			'sms_code' => Helper::generateRandomString(7),
			'region_id' => Input::get('region'),
			'district_id' => Input::get('district'),
			'circuit_id' => Input::get('circuit'),
			'country_id' => Input::get('country',1),
			'headteacher_id' => Input::get('head_teacher'),
			'supervisor_id' => Input::get('circuit_supervisor'),
			'email' => Input::get('school_email'),
			'phone_number' => Input::get('school_phone_number'),
			'name' => Input::get('school_name'),
			'ghanaian_language' => Input::get('ghanaian_language','no language'),
			'category' => Input::get('category','jhs'),
			'slug' => Helper::initials(Input::get('school_name').' School'),
			'address' => Input::get('school_address', ''),
			'shift' => Input::get('shift_status', 0),
			'description' => Input::get('description'),
			'lat_long' => Input::get('school_location','no lat long'),
			'status' => Input::get('status', 1)
		);

		//return $fields;

		//validating the input
		$validation = School::validate($fields);

		if ($validation !== true) {

			return $validation;

		}else{

			$createSchool = School::create($fields);
			//Save into the newly created data collector
			if ($createSchool) {

					Cache::forget('all_schools');

					WhoCreatedWhat::create(array('user_id' => Auth::user()->id, 
											 	'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
											 	'what_was_created' => "School with name: ".$createSchool->name. "was created by: ".Auth::user()->firstname.' '.Auth::user()->lastname));

				return "success";

			}else{
				return 'It could not save, something went wrong';
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showSchool($id)
	{
        //$schoolInfo = DB::table('schools')->where('id',$id)->first();

        return $schoolInfo = School::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editSchool($id)
	{
       return $schoolInfo = School::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateSchool($id)
	{
		//getting the posted details
		$fields = array(
			'ges_code' => Input::get('ges_code'),//code by GES
			'region_id' => Input::get('region'),
			'district_id' => Input::get('district'),
			'circuit_id' => Input::get('circuit'),
			'country_id' => Input::get('country',1),
			'headteacher_id' => Input::get('head_teacher'),
			'supervisor_id' => Input::get('circuit_supervisor'),
			'email' => Input::get('school_email'),
			'phone_number' => Input::get('school_phone_number'),
			'name' => Input::get('school_name'),
			'category' => Input::get('category'),
			'slug' => Helper::initials(Input::get('school_name').' School'),
			'description' => Input::get('description'),
			'lat_long' => Input::get('school_location'),
			'status' => Input::get('status'),
			'shift' => Input::get('shift_status'),

			'address' => Input::get('school_address'),
			'ghanaian_language' => Input::get('ghanaian_language')
		);

		//return $fields;

		//validating the input
		$validation = School::editValidate($fields);

		if ($validation !== true) {
			return $validation;
		}else{
			//Save edited school
			$update = DB::table('schools')
            					->where('id', $id)
            						->update(array(
            							'ges_code' => $fields['ges_code'],
            							'region_id' => $fields['region_id'],
            							'district_id' => $fields['district_id'],
            							'circuit_id' => $fields['circuit_id'],
            							'country_id' => $fields['country_id'],
            							'headteacher_id' => $fields['headteacher_id'],
            							'supervisor_id' => $fields['supervisor_id'],
            							'email' => $fields['email'],
            							'phone_number' => $fields['phone_number'],
            							'name' => $fields['name'],
            							'category' => $fields['category'],
            							'slug' => $fields['slug'],
            							'description' => $fields['description'],
            							'lat_long' => $fields['lat_long'],

            							'shift' => $fields['shift'],
            							'address' => $fields['address'],
            							'ghanaian_language' => $fields['ghanaian_language']
            							));

			if ($update) {

				Cache::forget('all_schools');



				WhoEditedWhat::create(array('user_id' => Auth::user()->id,
										 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
										 'what_was_edited' => "School with name: ".$fields['name']. "was edited by: ".Auth::user()->firstname.' '.Auth::user()->lastname));

				return "success";

			}else{

                return $update;

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteSchool($id)
	{
		$what_was_deleted = School::find($id)->name;
		$head_teacher_id = School::find($id)->headteacher_id;
		$deleteSchool = School::find($id)->delete();
		$deleteHeadTeacherDataCollector = DataCollector::where('id',$head_teacher_id)
									->where('type','head_teacher')->delete();

		 $fields = array(
		 			'user_id' => Auth::user()->id,
		 			'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
		 			'what_was_deleted' =>$what_was_deleted.' a school was deleted and everything under it by: '.Auth::user()->firstname.' '.Auth::user()->lastname);


		 $saveWhoDeletedASchool = WhoDeletedWhat::create($fields);


		if ($deleteSchool) {

			Cache::forget('all_schools');
			
			return 'success';
		}else{
			return 'failed';
		}
	}

	public function updateSchoolInfoViaAndroidAppByADataCollector()
	{
		
		$data = json_decode(@file_get_contents('php://input'));

		Log::info('Aliu and Hadi Debugging Logging', array('context' => $data));

		//exit();

		self::GeneralSchoolInformationToSave($data);

		Log::info('Just to see school info to update', array('context' => $data));
		
		


		$data_collector_id = $data->data_collector_id;
		$school_code = $data->school_code;
		$school_id = $data->school_id;
		$region_id = $data->region_id;
		$district_id = $data->district_id;
		$circuit_id = $data->circuit_id;
		$year = $data->year;
		$week = $data->week;
		$term = $data->term;
		$long = $data->long;
		$lat = $data->lat;
		$schoolName = $data->schoolName;
		$schoolCategory = $data->schoolCategory;
		$schoolAddress = $data->schoolAddress;
		$schoolEmail = $data->schoolEmail;
		$schoolPhone = $data->schoolPhone;
		$shift = $data->shift;
		$GES_Approved = $data->GES_Approved;

		$find_school = DB::table('schools')
								->where('id', $school_id)
								->where('school_code', $school_code)
            					->where('region_id', $region_id)
            					->where('district_id', $district_id)
            					->where('circuit_id', $circuit_id)->first();
       
        if (empty($find_school)) {
				return Response::json(array("status"=>401,
											"message" => "School information does not exist"));
        }
		//Save edited school
		$update = DB::table('schools')
            					->where('id', $school_id)
            					->where('school_code', $school_code)
            					->update(array(
            							'email' => $schoolEmail,
            							'phone_number' => $schoolPhone,
            							'name' => $schoolName,
            							'category' => $schoolCategory,
            							'address' => $schoolAddress,
            							'shift' => $shift,
            							'lat_long' => $lat.'-'.$long));


        if ($update) {
        	$newSchoolInfo = DB::table('schools')
								->where('id', $school_id)
            					->where('school_code', $school_code)
            					->where('region_id', $region_id)
            					->where('district_id', $district_id)
            					->where('circuit_id', $circuit_id)->first();
        	
        	//return details of the updated school information
				return Response::json(array("status"=>200,
											"message" => "School Information Updated",
											'school' => $newSchoolInfo));
        }else{
        	//return response to inidicate inability to update schoo, info
			return Response::json(array("status"=>200,
										"message"=>"Unable to update school details"));
        }

	}



	public static function GeneralSchoolInformationToSave($data)
	{
		Log::info('Just to see data inside GeneralSchoolInformationToSave function', array('context' => $data));

		$fields = array('school_reference_code' => $data->school_code,
						'total_number_of_days_in_a_term' => $data->schoolDays,
						'total_number_of_instructional_days_during_a_term' => $data->instructionalDays,
						'term' => $data->term,
						'week_number' => $data->week,
						'year' => $data->year,
						'country_id' => 1,
						'region_id' => $data->region_id,
						'district_id' => $data->district_id,
						'circuit_id' => $data->circuit_id,
						'school_id' => $data->school_id,
						'data_collector_id' => $data->data_collector_id,
						'data_collector_type' => $data->data_collector_type,
						'lat' => $data->lat,
						'long' => $data->long);

		GeneralSchoolInformation::create($fields);
	}

	//retrieve general information on schools
	public function RetrieveGeneralSchoolInformationToData()
	{

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();
		try {
	        //querying base on the parameters posted which is contain in the $querystring variable
			 return $generalSchool = DB::table('general_school_information')->whereRaw($queryString, $values)->get();

        } catch (Exception $e) {
        	
        	return "Check the fields in your posted data";
        }
	}



	public function androidRequestToGetSchoolCurrentPerformanceStatus()
	{
		
		$school_id = Input::get('school_id','empty');
		//return "The posted ID is: ".$school_id;
		$currentSchoolPerformanceData = DB::table('schools')->where('id', $school_id)->first();

		if (!empty($currentSchoolPerformanceData)) {

			return Response::json(array("status"=>200,
										"message" => "Current Performance data of school",
										'schools_current_rating' => $currentSchoolPerformanceData));
		
		}

		return Response::json(array("status"=>400,
									"message" => "This school does not exist"));

	}






	public function deleteAnEnrolmentOrAttendanceData()
	{

//		$db_unique_id_of_data_to_delete = Input::get('id');

		$section_to_delete_data = Input::get('data_type');

        $inputs = Input::except('data_type');
        //return array_keys($inputs) of the posted data;
        $values = array_values($inputs);

        //declaring a query string to query with in the whereRaw method
        $queryString = '';

        //putting the key into the query parameters and appending the word 'and';
        foreach ($inputs as $key => $value) {
            $queryString .= $key.' = ? and ';
        }

        //removing the last and which will be appended
        $queryString = substr($queryString, 0, -4);

		switch ($section_to_delete_data) {
			case 'normal_enrolment':
//				DB::table('school_enrolments')->where('id', $db_unique_id_of_data_to_delete)->delete();
                $delete = DB::table('school_enrolments')->whereRaw($queryString, $values)->delete();


                if ($delete) {
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['normal_enrolment_total_boys' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['normal_enrolment_total_girls' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['normal_enrolment_total_students' => 0]);
                }

                $deleteSpecial = DB::table('special_enrolment')->whereRaw($queryString, $values)->delete();
                if ($deleteSpecial) {
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_enrolment_total_boys' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_enrolment_total_girls' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_enrolment_total_students' => 0]);
                                }
				break;

			case 'normal_attendance':

//				DB::table('student_attendance')->where('id', $db_unique_id_of_data_to_delete)->delete();
                $delete = DB::table('student_attendance')->whereRaw($queryString, $values)->delete();
                if ($delete) {
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['normal_attendance_total_boys' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['normal_attendance_total_girls' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['normal_attendance_total_students' => 0]);
                }
                $deleteSpecial = DB::table('special_attendance')->whereRaw($queryString, $values)->delete();

                    if ($deleteSpecial) {
                        DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_attendance_total_boys' => 0]);
                        DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_attendance_total_girls' => 0]);
                        DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_attendance_total_students' => 0]);
                    }

				break;
			case 'special_enrolment':
//				DB::table('special_enrolment')->where('id', $db_unique_id_of_data_to_delete)->delete();
                $delete = DB::table('special_enrolment')->whereRaw($queryString, $values)->delete();

                if ($delete) {
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_enrolment_total_boys' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_enrolment_total_girls' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_enrolment_total_students' => 0]);
                }
				break;
			case 'special_attendance':
//				DB::table('special_attendance')->where('id', $db_unique_id_of_data_to_delete)->delete();
                $delete = DB::table('special_attendance')->whereRaw($queryString, $values)->delete();

                if ($delete) {
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_attendance_total_boys' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_attendance_total_girls' => 0]);
                    DB::table('weekly_totals')->whereRaw($queryString, $values)->update(['special_attendance_total_students' => 0]);
                }
				break;
			default:
				# code...
				break;
		}



		return "success";

		
	}




	

}
