<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TargetedInstructionsController extends BaseController {

    /**
     * assign a school to a target instruction
     *
     * @return Response
     */
    public function AssignASchoolToATargetInstructionGroup()
    {

        //getting the posted details for assigning a school to a targetted group.
        $fields = array(
            'school_id' => Input::get('school_id'),//code by GES
            'targeted_group' => Input::get('targeted_group_to_assign_to')
        );

        $validation = School::assignSchoolToTargetedGroupRulesValidate($fields);

        if ($validation !== true) {

            return $validation;

        }else{
            //Save edited school
            $update_school = DB::table('schools')
                ->where('id', $fields['school_id'])
                ->update(array('targeted_groups' => $fields['targeted_group']));

            if ($update_school) {
                Cache::forget('all_schools');

                WhoEditedWhat::create(array('user_id' => Auth::user()->id,
                    'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
                    'what_was_edited' => "School with name: ".$fields['name']. "targetted instruction group was changed by: ".Auth::user()->firstname.' '.Auth::user()->lastname));
                return Response::json(array("code" => 200, "status"=>"OK", "message"=>"School Assign to targetted group successfully", "school" => $update_school));

            }else{
                return Response::json(array("code" => 401, "status"=>"Failed", "message"=>"Unable to assign school to targetted group"));
            }
        }


    }



    /**
	 * save the resource.
	 *
	 * @return Response
	 */
	public function saveTargetedAttendance()
	{
        $data = json_decode(@file_get_contents('php://input'));
        Log::info('Tagetted Attendance Posted From Android', array('context' => $data));

        $fields = array(
            'school_reference_code' =>$data->school_code,
            'primary_four_total_attendance' => $data->primary_four_total_attendance,
            'primary_five_total_attendance' => $data->primary_five_total_attendance,
            'primary_six_total_attendance' => $data->primary_six_total_attendance,
            'year' => $data->year,
            'data_collector_type' => $data->data_collector_type,
            'data_collector_id' => $data->data_collector_id,
            'term' =>  $data->term,
            'week_number' => $data->week_number,
            'country_id' => 1,
            'region_id' => $data->region_id,
            'district_id' =>  $data->district_id,
            'circuit_id' => $data->circuit_id,
            'school_id' =>$data->school_id,
            'lat' => $data->lat,
            'long' => $data->long,
            'number_of_week_days' => $data->school_session_days);


        $validation = TargetedAttendance::validateTargetedAttendanceCreation($fields);
        if ($validation !== true) {
            return $validation;
        }else{
            $targetted_attendance_create = TargetedAttendance::create($fields);
            if ($targetted_attendance_create) {
                $schoolIDAndDataType = array('school_id' => $data->school_id, 'type' => 'targeted_attendance' );
                NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($schoolIDAndDataType['school_id'],$schoolIDAndDataType['type']);
                return Response::json(array("status"=> 200,
                    "status" => "OK",
                    "message"=>"Targeted Attendance Data Changed Successfully"));
            }else{
                return Response::json(array("status"=> 401,
                    "status" => "OK",
                    "message"=>"Unable to save targeted attendance"));
            }

        }
    }


	/**
	 * Retrieve targetted attendance with dynamic queries.
	 *
	 * @return Response
	 */
	public function retrieveTargetedAttendanceWithDynamicQueries()
	{

        //getting the posted query parameters
        $inputs = Input::all();

        //return array_keys($inputs) of the posted data;
        $values = array_values($inputs);

        //declaring a query string to query with in the whereRaw method
        $queryString = '';

        //puting the key into the query parameters and appending the word 'and';
        foreach ($inputs as $key => $value) {
            $queryString .= $key.' = ? and ';
        }
        //removing the last and which will be appended
        $queryString = substr($queryString, 0, -4);

        $targeted_attendance = DB::table('targeted_attendance')->whereRaw($queryString, $values)->get();

        return Response::json(array("status"=>200,
            "message"=>"OK",
            "targeted_attendance"=> $targeted_attendance));


    }



	/**
	 * save an e-library file.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function uploadELibraryFile()
	{

        $targeted_groups = Input::get('targeted_groups');
        $file_type = Input::get('file_type');
        $file_title = Input::get('file_title');
        $file_description = Input::get('file_description','No Description');
        $e_library_file = Input::file('e_library_file');

        $fields = array('targeted_groups' => $targeted_groups, 'file_type' => $file_type, 'file_title' => $file_title, 'file_description' => $file_description);

        $validation = ELibrary::validateElibraryFileUpload($fields);

        if ($validation !== true) {

            return $validation;

        }else{

            if (is_file($e_library_file)) {

                $file_local_path = $e_library_file->getRealPath();

                //directory to save files into
                $directoryPath = public_path(). '/targeted_instructions_elibrary/';

                //this check if directory exist, if not it creates one
                File::exists($directoryPath) or File::makeDirectory($directoryPath);

                //gettting original file name and time in order to save file in that name
                $nameToSaveElibraryFileWith = time() . '-' .$file_local_path->getClientOriginalName();

                //giving the absolute path to the image including its name
                $eLibraryFileOnLocalServer = public_path('/targeted_instructions_elibrary/'.$nameToSaveElibraryFileWith);

                //checking if the image and the name of image exist
                if (file_exists($eLibraryFileOnLocalServer) && !empty($nameToSaveElibraryFileWith)) {

                    $bucket = "m4dimages/targeted_instructions_elibrary_s3_bucket";

                    //calling the function to save file at S3
                    $fileDetailsAtS3 = Helper::savingFilesTOAmazonS3($bucket,$eLibraryFileOnLocalServer,$nameToSaveElibraryFileWith);

                    if (!empty($fileDetailsAtS3)) {

                        $file_s3_url = $fileDetailsAtS3['ObjectURL'];

                        $e_library_file_details = array_add($fields, 'file_s3_url', $file_s3_url);

                        $create_file_details = ELibrary::create($e_library_file_details);

                        if ($create_file_details) {
                            if (file_exists($eLibraryFileOnLocalServer)) {
                                unlink($eLibraryFileOnLocalServer);
                            }

                            return Response::json(array("status"=>200, "message"=>"E-Library file uploaded successfully"));

                        }else{

                            return Response::json(array("status"=>401, "message"=>"Unable to save E-Library file details, please try again "));
                        }

                    }else{

                        return Response::json(array("status"=>401, "message"=>"Unable to upload file on s3, please try again"));
                    }

                }else{

                    return Response::json(array("status"=>401, "message"=>"Unable to find file on local server or generate a name for the file"));

                }

            }else{

                return Response::json(array("status"=>401, "message"=>"No valid file detected"));

            }


        }

    }



    /**
     * Retrieve E-Library files with dynamic queries.
     *
     * @return Response
     */
    public function retrieveElibraryFilesWithDynamicQueries()
    {

        //getting the posted query parameters
        $inputs = Input::all();

        //return array_keys($inputs) of the posted data;
        $values = array_values($inputs);

        //declaring a query string to query with in the whereRaw method
        $queryString = '';

        //puting the key into the query parameters and appending the word 'and';
        foreach ($inputs as $key => $value) {
            $queryString .= $key.' = ? and ';
        }
        //removing the last and which will be appended
        $queryString = substr($queryString, 0, -4);

        $e_library = DB::table('e_library')->whereRaw($queryString, $values)->get();

        return Response::json(array("status"=>200,
            "message"=>"OK",
            "e_library_files"=> $e_library));


    }






    /**
     * Retrieve E-Library files that A School Has Access To.
     *
     * @return Response
     */
    public function retrieveElibraryFilesForASchool()
    {
        $school_id = Input::get('school_id');

        $school = DB::table('schools')->where('id',$school_id)->first();

        if (count($school) > 0) {

            $e_library_files = DB::table('e_library')->where('targeted_groups',$school->targeted_groups)
                ->orWhere('targeted_groups','t_one_and_t_two')
                ->orWhere('targeted_groups','no_restriction')
                ->get();

            return Response::json(array("status"=>200, "message"=>"OK", "e_library_files"=> $e_library_files));

        }

        return Response::json(array("status"=>401, "message"=>"Failed, No school exist with that ID.".$school_id));

    }


    /**
     * =============================================================================================================
     *
     * Created by Makedu-Consult : Mantey 19/01/2019
     *
     * Targeted Instructions Updates Updates for STARS PROJECT
     *
     * =================================================================================================
     */


    public function saveAttendanceData()
    {
        $data = json_decode(@file_get_contents('php://input'));

        $fields = array(
            'attendance_academic_term' =>$data->attendance_academic_term,
            'attendance_academic_year' => $data->attendance_academic_year,
            'attendance_academic_subject' => $data->attendance_academic_subject,
            'attendance_academic_level' => $data->attendance_academic_level,
            'attendance_academic_week' => $data->attendance_academic_week,
            'attendance_no_pupil_level' => $data->attendance_no_pupil_level,
            'attendance_no_pupil_avg' => $data->attendance_no_pupil_avg,
            'attendance_no_teacher_avg' => $data->attendance_no_teacher_avg,
            'data_collector_id' => $data->data_collector_id,
            'data_collector_type' =>  $data->data_collector_type,
            'saved_attendance_id' => $data->id,
            'longitude' => $data->longitude,
            'latitude' => $data->latitude,
            'school_id' => $data->school_id,
            'district_id' => $data->district_id,
            'circuit_id' => $data->circuit_id,
            'country_id' => $data->country_id,
            'region_id' => $data->region_id,
        );

        $validation = MkAttendance::validateAttendance($fields);
        if (!$validation) {

            $attendance = MkAttendance::create($fields);
            if ($attendance) {
                return Response::json(array("status"=> 200,
                    "content" => "accepted",
                    "message"=>"Data submitted"));
            }else{
                return Response::json(array("status"=> 401,
                    "content" => "rejected",
                    "message"=>"Data submission failed"));
            }

        }else{

            return Response::json(array("status"=> 401,
                "content" => "rejected",
                "message"=>"Data submission failed"));
        }


    }


    public function savePupilAggregatedData()
    {
        $data = json_decode(@file_get_contents('php://input'));

        $fields = array(
            'academic_term' =>$data->academic_term,
            'academic_year' => $data->academic_year,
            'grade' => $data->grade,
            'no_student_grade' => $data->no_student_grade,
            'no_student_assessed' => $data->no_student_assessed,
            'data_collector_id' => $data->data_collector_id,
            'data_collector_type' =>  $data->data_collector_type,
            'longitude' => $data->longitude,
            'latitude' => $data->latitude,
        );

        $save_data = array(
            'academic_term' =>$data->academic_term,
            'academic_year' => $data->academic_year,
            'grade' => $data->grade,
            'no_student_grade' => $data->no_student_grade,
            'no_student_assessed' => $data->no_student_assessed,
            'data_collector_id' => $data->data_collector_id,
            'data_collector_type' =>  $data->data_collector_type,
            'longitude' => $data->longitude,
            'latitude' => $data->latitude,
            'saved_pupil_aggregated_id' => $data->id,

            'english_beginner_text' => $data->mk_english_assessment->english_beginner_text,
            'english_letter_text' => $data->mk_english_assessment->english_letter_text,
            'english_paragraph_text' => $data->mk_english_assessment->english_paragraph_text,
            'english_word_text' => $data->mk_english_assessment->english_word_text,
            'english_sentence_text' => $data->mk_english_assessment->english_sentence_text,
            'english_level_one_text' => $data->mk_english_assessment->english_level_one_text,
            'english_level_two_text' => $data->mk_english_assessment->english_level_two_text,
            'english_level_three_text' => $data->mk_english_assessment->english_level_three_text,

            'math_beginner_text' => $data->mk_math_assessment->math_beginner_text,
            'math_digit_one_text' => $data->mk_math_assessment->math_digit_one_text,
            'math_digit_two_text' => $data->mk_math_assessment->math_digit_two_text,
            'math_addition_text' => $data->mk_math_assessment->math_addition_text,
            'math_subtraction_text' => $data->mk_math_assessment->math_subtraction_text,
            'math_level_one_text' => $data->mk_math_assessment->math_level_one_text,
            'math_level_two_text' => $data->mk_math_assessment->math_level_two_text,
            'math_level_three_text' => $data->mk_math_assessment->math_level_three_text,

            'school_id' => $data->school_id,
            'district_id' => $data->district_id,
            'circuit_id' => $data->circuit_id,
            'country_id' => $data->country_id,
            'region_id' => $data->region_id,
        );


        $validation = MkPupilAggregatedData::validate($fields);
        if (!$validation) {

            $pupil_aggregated = MkPupilAggregatedData::create($save_data);
            if ($pupil_aggregated) {
                return Response::json(array("status"=> 200,
                    "content" => "accepted",
                    "message"=>"Data submitted"));
            }else{
                return Response::json(array("status"=> 401,
                    "content" => "rejected",
                    "message"=>"Data submission failed"));
            }

        }else{
            return Response::json(array("status"=> 401,
                "content" => "rejected",
                "message"=>"Data submission failed"));
        }

    }



    public function saveClassObservationData()
    {

        $json_data = file_get_contents('php://input');

        $data = json_decode($json_data);


        $fields = array(
            'class_ob_teacher_name' =>$data->class_ob_teacher_name,
            'class_ob_level' => $data->class_ob_level,
            'class_ob_subject' => $data->class_ob_subject,
            'class_ob_enrolled_p_four' => $data->class_ob_enrolled_p_four,
            'class_ob_enrolled_p_five' => $data->class_ob_enrolled_p_five,
            'class_ob_enrolled_p_six' => $data->class_ob_enrolled_p_six,
            'class_ob_enrolled_p_total' => $data->class_ob_enrolled_p_total,
            'class_ob_absent_p_four' => $data->class_ob_absent_p_four,
            'class_ob_absent_p_five' => $data->class_ob_absent_p_five,
            'class_ob_absent_p_six' => $data->class_ob_absent_p_six,
            'class_ob_absent_p_total' => $data->class_ob_absent_p_total,
            'class_ob_duration' => $data->class_ob_duration,
            'data_collector_id' => $data->data_collector_id,
            'data_collector_type' =>  $data->data_collector_type,
            'longitude' => $data->longitude,
            'latitude' => $data->latitude,
        );

        $save_data = array(
            'class_ob_teacher_name' =>$data->class_ob_teacher_name,
            'class_ob_level' => $data->class_ob_level,
            'class_ob_subject' => $data->class_ob_subject,
            'class_ob_date_monitoring' => $data->class_ob_day." ".$data->class_ob_month." ".$data->class_ob_year,
            'class_ob_enrolled_p_four' => $data->class_ob_enrolled_p_four,
            'class_ob_enrolled_p_five' => $data->class_ob_enrolled_p_five,
            'class_ob_enrolled_p_six' => $data->class_ob_enrolled_p_six,
            'class_ob_enrolled_p_total' => $data->class_ob_enrolled_p_total,
            'class_ob_absent_p_four' => $data->class_ob_absent_p_four,
            'class_ob_absent_p_five' => $data->class_ob_absent_p_five,
            'class_ob_absent_p_six' => $data->class_ob_absent_p_six,
            'class_ob_absent_p_total' => $data->class_ob_absent_p_total,
            'class_ob_duration' => $data->class_ob_duration,
            'class_ob_attendance_summary' => $data->class_ob_attendance_summary,
            'areas_summary' => $data->areas_summary,
            'areas_improvement' => $data->areas_improvement,
            'data_collector_id' => $data->data_collector_id,
            'data_collector_type' =>  $data->data_collector_type,
            'longitude' => $data->longitude,
            'latitude' => $data->latitude,
            'saved_class_ob_id' => $data->id,

            'ti_question_one_answer' => $data->mk_teaching_levels->question_one_answer,
            'ti_question_two_answer' => $data->mk_teaching_levels->question_two_answer,
            'ti_question_three_answer' => $data->mk_teaching_levels->question_three_answer,
            'ti_question_one_comment' => $data->mk_teaching_levels->question_one_comment,
            'ti_question_two_comment' => $data->mk_teaching_levels->question_two_comment,
            'ti_question_three_comment' => $data->mk_teaching_levels->question_three_comment,
            'ti_lesson_summary' => $data->mk_teaching_levels->lesson_summary,

            'la_question_one_answer' => $data->mk_lesson_approach->question_one_answer,
            'la_question_two_answer' => $data->mk_lesson_approach->question_two_answer,
            'la_question_three_answer' => $data->mk_lesson_approach->question_three_answer,
            'la_question_one_comment' => $data->mk_lesson_approach->question_one_comment,
            'la_question_two_comment' => $data->mk_lesson_approach->question_two_comment,
            'la_question_three_comment' => $data->mk_lesson_approach->question_three_comment,
            'la_lesson_summary' => $data->mk_lesson_approach->lesson_summary,

            'cl_question_one_reading_answer' => $data->mk_classroom_learning->question_one_reading_answer,
            'cl_question_one_saying_answer' => $data->mk_classroom_learning->question_one_saying_answer,
            'cl_question_one_doing_answer' => $data->mk_classroom_learning->question_one_doing_answer,
            'cl_question_one_writing_answer' => $data->mk_classroom_learning->question_one_writing_answer,
            'cl_question_one_speaking_answer' => $data->mk_classroom_learning->question_one_speaking_answer,
            'cl_question_two_answer' => $data->mk_classroom_learning->question_two_answer,
            'cl_question_three_answer' => $data->mk_classroom_learning->question_three_answer,
            'cl_question_one_comment' => $data->mk_classroom_learning->question_one_comment,
            'cl_question_two_comment' => $data->mk_classroom_learning->question_two_comment,
            'cl_question_three_comment' => $data->mk_classroom_learning->question_three_comment,
            'cl_question_four_comment' => $data->mk_classroom_learning->question_four_comment,
            'cl_classroom_summary' => $data->mk_classroom_learning->classroom_summary,

            'school_id' => $data->school_id,
            'district_id' => $data->district_id,
            'circuit_id' => $data->circuit_id,
            'country_id' => $data->country_id,
            'region_id' => $data->region_id,
        );


        $validation = MkClassObservationData::validate($fields);
        if (!$validation) {

            $class_observation = MkClassObservationData::create($save_data);
            if ($class_observation) {
                return Response::json(array("status"=> 200,
                    "content" => "accepted",
                    "message"=>"Data submitted"));
            }else{
                return Response::json(array("status"=> 401,
                    "content" => "rejected",
                    "message"=>"Data submission failed"));
            }

        }else{
            return Response::json(array("status"=> 401,
                "content" => "rejected",
                "message"=>"Data submission failed"));
        }

    }


    public function saveAnswerData()
    {
        $data = json_decode(@file_get_contents('php://input'));

        $fields = array(
            'district_id' =>$data->district_id,
            'circuit_id' => $data->circuit_id,
            'school_name' => $data->school_name,
            'school_id' => $data->school_id,
            'school_code' => $data->school_code,
            'phone_number' => $data->phone_number,
            'head_teacher_name' => $data->head_teacher_name,
            'data_collector_id' => $data->data_collector_id,
            'poll_question_id' => $data->poll_question_id,
            'poll_answer' => $data->poll_answer,
        );

        $validation = MkPollsQuestion::validateAttendance($fields);
        if (!$validation) {
            $polls = MkPollsQuestion::create($fields);
            if ($polls) {
                return Response::json(array("status"=> 200,
                    "content" => "accepted",
                    "message"=>"Data submitted"));
            } else {
                return Response::json(array("status"=> 401,
                    "content" => "rejected",
                    "message"=>"Data submission failed"));
            }

        } else {
            return Response::json(array("status"=> 401,
                "content" => "rejected",
                "message"=>"Data submission failed"));
        }


    }



    public function allTISchools($country = 1)
    {
        if(Cache::has('all_mk_stars_schools')) {

            $data = Cache::get('all_mk_stars_schools');

            return $data;
        }

        $school_codes = array();
        $mk_stars_schools = DB::table('mk_stars_schools')->select('school_code')->get();

        foreach ($mk_stars_schools as $school) {
            array_push($school_codes, $school->school_code);
        }

        $schools = DB::table('schools')->where('country_id', $country)
            ->whereIn('ges_code', $school_codes)->get();

        $attendance_count = DB::table("mk_attendance")->count();
        $pupil_aggregated_count = DB::table("mk_pupil_aggregated")->count();
        $lesson_count = DB::table("mk_class_observation")->count();

        $data = [
            "schools" => $schools,
            "attendance_count" => $attendance_count,
            "pupil_aggregated_count" => $pupil_aggregated_count,
            "lessons_count" => $lesson_count,
        ];

        Cache::put( 'all_mk_stars_schools', $data, 15);

        return json_encode($data);
    }

    public function allTICircuits($country = 1)
    {
        if(Cache::has('all_ti_circuits')) {

            $data = Cache::get('all_ti_circuits');

            return $data;
        }

        $school_codes = array();
        $mk_stars_schools = DB::table('mk_stars_schools')->select('school_code')->get();
        foreach ($mk_stars_schools as $school) {
            array_push($school_codes, $school->school_code);
        }

        $circuits = DB::table('schools')->select('circuit_id')->where('country_id', $country)
            ->whereIn('ges_code', $school_codes)->distinct()->get();

        $circuit_ids = array();
        foreach ($circuits as $id) {
            array_push($circuit_ids, $id->circuit_id);
        }

        $circuits = DB::table('circuits')->where('country_id', $country)
            ->whereIn('id', $circuit_ids)->distinct()->get();

        $attendance_count = DB::table("mk_attendance")->count();
        $pupil_aggregated_count = DB::table("mk_pupil_aggregated")->count();
        $lesson_count = DB::table("mk_class_observation")->count();

        $data = [
            "circuits" => $circuits,
            "attendance_count" => $attendance_count,
            "pupil_aggregated_count" => $pupil_aggregated_count,
            "lessons_count" => $lesson_count,
        ];

        Cache::put('all_ti_circuits', $data, 15);

        return json_encode($data);
    }


    public function allTIDistricts($country = 1)
    {
        if(Cache::has('all_ti_districts')) {

            $data = Cache::get('all_ti_districts');

            return $data;
        }

        $school_codes = array();
        $mk_stars_schools = DB::table('mk_stars_schools')->select('school_code')->get();
        foreach ($mk_stars_schools as $school) {
            array_push($school_codes, $school->school_code);
        }

        $districts = DB::table('schools')->select('district_id')->where('country_id', $country)
            ->whereIn('ges_code', $school_codes)->distinct()->get();

        $district_ids = array();
        foreach ($districts as $id) {
            array_push($district_ids, $id->district_id);
        }

        $districts = DB::table('districts')->where('country_id', $country)
            ->whereIn('id', $district_ids)->distinct()->get();

        $attendance_count = DB::table("mk_attendance")->count();
        $pupil_aggregated_count = DB::table("mk_pupil_aggregated")->count();
        $lesson_count = DB::table("mk_class_observation")->count();

        $data = [
            "districts" => $districts,
            "attendance_count" => $attendance_count,
            "pupil_aggregated_count" => $pupil_aggregated_count,
            "lessons_count" => $lesson_count,
        ];

        Cache::put('all_ti_districts', $data, 15);

        return json_encode($data);
    }


    /**
     * =============================================================================================================
     *
     * Created by Makedu-Consult : Mantey 19/01/2019
     *
     * Targeted Instructions Frontend
     *
     * =================================================================================================
     */

    public function tiDashboard()
    {
        $attendance = MkAttendance::all();

        return View::make('mk_testing/app_dashboard')
            ->with('attendance', $attendance);

    }



    public function filterAttendance()
    {
        dd(Input::all());
    }












}
