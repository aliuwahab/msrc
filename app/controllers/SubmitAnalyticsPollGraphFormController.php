<?php

use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class SubmitAnalyticsPollGraphFormController extends BaseController
{

    public function getPollQuestions()
    {
        $get_questions = DB::table("mk_questions")->select('id', 'poll_question')->get();
        return $get_questions;
    }

    public function filterPoll()
    {
        $data = json_decode(@file_get_contents('php://input'), true);

        $answers = DB::table('mk_polls_questions')
            ->select(DB::raw('count(poll_answer) as answer_count, poll_answer'))
            ->where('poll_question_id', '=', $data['poll_title']['id'])
            ->groupBy('poll_answer')
            ->get();

        $answers_count = array();
        $poll_answers = array();
        foreach ($answers as $answer) {
            array_push($answers_count, $answer->answer_count);
            array_push($poll_answers, $answer->poll_answer);
        }

        return array([
            'answers_count' => $answers_count,
            'poll_answers' => $poll_answers
        ]);

    }


    public function exportPolls()
    {

        $id = Input::get('id');

        $answers = DB::table('mk_polls_questions')
            ->select(DB::raw('count(poll_answer) as answer_count, poll_answer'))
            ->where('poll_question_id', '=', $id)
            ->groupBy('poll_answer')
            ->get();

        if (count($answers) > 0) {

            $question = DB::table('mk_questions')->where('id', '=', $id)->first();

            $answers_count = array();
            $poll_answers = array();
            $total_headmaster = 0;

            foreach ($answers as $answer) {

                $total_headmaster = $total_headmaster + $answer->answer_count;

                array_push($answers_count, $answer->answer_count);
                array_push($poll_answers, $answer->poll_answer);
            }

            array_push($poll_answers, "Total Responders");
            array_push($answers_count, $total_headmaster);

            try {

                $path = public_path('downloadadbles/sample_polls_spreadsheet.xlsx');
//                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
                $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

                $sheet = $spreadsheet->getActiveSheet();

                $highest_row = $sheet->getHighestRow();
                $highest_column = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex(count($poll_answers) + 1);
                $highest_column++;

                $sheet->setCellValue("A2", "Date");
                $sheet->setCellValue("A3", date("d-m-Y"))
                    ->getColumnDimension('A')->setWidth(12);

                $sheet->setCellValue("B2", "Question");
                $sheet->setCellValue("B3", $question->poll_question)
                    ->getColumnDimension('B')->setWidth(20);

                $sheet->setCellValue("C1", "Answers Summary");
                $spreadsheet->getActiveSheet()->mergeCells("C1:" . $highest_column . "1");


                for ($col = 'C'; $col <= $highest_column; ++$col) {
                    $poll_index = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($col);

                    $sheet->setCellValue($col . "2", $poll_answers[($poll_index - 3)])
                        ->getColumnDimension($col)->setWidth(15);

                    $sheet->getStyle($col)->getAlignment()->setHorizontal('center');
                }


                for ($col = 'C'; $col <= $highest_column; ++$col) {
                    $poll_index = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($col);
                    $sheet->setCellValue($col . "3", $answers_count[($poll_index - 3)]);

                    $sheet->getStyle($col)->getAlignment()->setHorizontal('center');
                }

                $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

                $filename = "poll_answers_summary";

                ob_end_clean();

                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="' . $filename . '.xlsx"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');


            } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
                Log::error($e->getMessage() . ' @ ' . $e->getLine());
                dd($e->getMessage());
            }
        }


    }


}
