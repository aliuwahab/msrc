<?php

class DashboardController extends \BaseController {

	




	public function adminLoginIntoDashboard()
	{
		// return View::make('login');
		return View::make('dashboard');
		//
		
	}



	/**
	 * Display a listing of the resource.
	 * GET /dashboards
	 *
	 * @return Response
	 */
	public function index() {

		return View::make('dashboard');
	}
	//return all or data about the login user
	public function allData()
	{
		$user = Auth::user();
		return $userData = CustomUser::loginUserData($user->level,$user->level_id,$user->id);
	}
	public function createRegion()
	{
	//getting the posted details of the about to  create region
		$fields = array(
			'code' => Helper::generateRegionUniqueCode($table  = 'regions', $column = 'code'),
			'country_id' => Input::get('country', 1),
			'name' => Input::get('region_name'),
			'email' => Input::get('region_email'),
			'phone_number' => Input::get('region_phone_number'),
			'slug' => Helper::initials(Input::get('region_name').' Region'),
			'description' => Input::get('description'),
			'lat_long' => Input::get('lat_long','lat23gdhdhdhdh4,long34fhfhtthg')
		);

		//validating the input
		$validation = Region::validate($fields);

		if ($validation !== true) {

			return $validation;

		}else{
			//Saving the newly created region
			$created_region = Region::create($fields);

			if ($created_region) {

				Cache::forget('all_regions');

				WhoCreatedWhat::create(array('user_id' => Auth::user()->id, 
											 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
											 'what_was_created' => "Region with name: ".$created_region->name. "was created by: ".Auth::user()->firstname.' '.Auth::user()->lastname));

				return "success";

			}else{

				return 'It could not save, code already exists';

			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showRegion($id)
	{
        return $regionInfo = Region::find($id);
	}

		/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editRegion($id)
	{
        return $regionInfo = Region::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateRegion($id)
	{
		//getting the posted details of the about to  edit region
		$fields = array(
			'code' => Helper::generateRegionUniqueCode($table  = 'regions', $column = 'code'),
			'country_id' => Input::get('country', 1),
			'name' => Input::get('region_name'),
			'email' => Input::get('region_email'),
			'phone_number' => Input::get('region_phone_number'),
			'slug' => Helper::initials(Input::get('region_name').' Region'),
			'description' => Input::get('description'),
			'lat_long' => Input::get('lat_long','lat23gdhdhdhdh4,long34fhfhtthg')
		);

		//return $fields;

		//validating the input
		$validation = Region::validate($fields);

		if ($validation !== true) {
			return $validation->messages();
		}else{
			//Saving the newly created 
			$update = DB::table('regions')
            					->where('id', $id)
            						->update(array(
            							'country_id' => $fields['country_id'],
            							'name' => $fields['name'],
            							'email' => $fields['email'],
            							'phone_number' => $fields['phone_number'],
            							'slug' => $fields['slug'],
            							'description' => $fields['description'],
            							'lat_long' => $fields['lat_long']));
			if ($update) {

				WhoEditedWhat::create(array('user_id' => Auth::user()->id, 
											 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
											 'what_was_edited' => "Region with name: ".$fields['name']."was edited by: ".Auth::user()->firstname.' '.Auth::user()->lastname));

				Cache::forget('all_regions');
				Cache::forget('all_districts');
				Cache::forget('all_circuits');
				Cache::forget('all_schools');

				return "success";
			}else{
				return 'It could not save, code already exists';
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteRegion($id)
	{
		 $what_was_deleted = Region::find($id)->name;
		 //$what_was_deleted =  
		 $deleteRegion = Region::find($id)->delete();
		 $deleteAllDistrictsUnderTheRegion = District::where('region_id',$id)->delete();
		 $deleteAllCircuitsUnderTheRegion = Circuit::where('region_id',$id)->delete();
		 $deleteAllSchoolsUnderTheRegion = School::where('region_id',$id)->delete();
		 $deleteDataCollectors = DataCollector::where('region_id',$id)->delete();
		 
		 $fields = array(
		 			'user_id' => Auth::user()->id,
		 			'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
		 			'what_was_deleted' =>$what_was_deleted.' Region was deleted and everything under it by: '.Auth::user()->firstname.' '.Auth::user()->lastname);

		 $saveWhoDeletedARegion = WhoDeletedWhat::create($fields);

		 if ($deleteRegion) {

			Cache::forget('all_regions');
			Cache::forget('all_districts');
			Cache::forget('all_circuits');
			Cache::forget('all_schools');

		 	return 'success';
		 }else{
		 	return 'failed';
		 }
	}

	public function createDistrict()
	{	
		//return "About to create district";
		//getting the posted details of the about to  create district
		$fields = array(
			'code' => Helper::generateDistrictUniqueCode($table  = 'districts', $column = 'code'),
			'region_id' => Input::get('region'),
			'country_id' => Input::get('country', 1),
			'name' => Input::get('district_name'),
			'email' => Input::get('district_email','no email'),
			'phone_number' => Input::get('district_phone_number','no phone number'),
			'slug' => Helper::initials(Input::get('district_name').'District'),
			'description' => Input::get('description','No description provided'),
			'lat_long' => Input::get('lat_long','No Cordinates provided')
		);

			//validating the input
		$validation = District::validate($fields);

		if ($validation !== true) {
			return $validation;
		}else{
			//Saving the newly created district
			$created_district = District::create($fields);

			if ($created_district) {

				Cache::forget('all_districts');

				WhoCreatedWhat::create(array('user_id' => Auth::user()->id, 
											 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
											 'what_was_created' => "District with name: ".$created_district->name. "was created by: ".Auth::user()->firstname." ".Auth::user()->lastname));


				return "success";
			}else{
				return 'It could not save, code already exists';
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showDistrict($id)
	{
        return $districtInfo = District::find($id);
	}

		/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editDistrict($id)
	{
        return $districtInfo = District::find($id); 
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateDistrict($id)
	{
		//return "About to create district";
		//getting the posted details of the about to  create district
		$fields = array(
			'code' => Helper::generateDistrictUniqueCode($table  = 'districts', $column = 'code'),
			'region_id' => Input::get('region'),
			'country_id' => Input::get('country', 1),
			'name' => Input::get('district_name'),
			'email' => Input::get('district_email'),
			'phone_number' => Input::get('district_phone_number'),
			'slug' => Helper::initials(Input::get('district_name').'District'),
			'description' => Input::get('description'),
			'lat_long' => Input::get('lat_long','lat23gdhdhdhdh4,long34fhfhtthg')
		);

			//validating the input
		$validation = District::validate($fields);

		if ($validation !== true) {
			//returning Error Messages
			return $validation->messages();
		}else{
			//Saving the edited district
			$update = DB::table('districts')
            					->where('id', $id)
            						->update(array(
            							'region_id' => $fields['region_id'],
            							'country_id' => $fields['country_id'],
            							'name' => $fields['name'],
            							'email' => $fields['email'],
            							'phone_number' => $fields['phone_number'],
            							'slug' => $fields['slug'],
            							'description' => $fields['description'],
            							'lat_long' => $fields['lat_long']));
			if ($update) {

				Cache::forget('all_districts');
				Cache::forget('all_circuits');
				Cache::forget('all_schools');

				WhoEditedWhat::create(array('user_id' => Auth::user()->id, 
											 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
											 'what_was_edited' => "District with name: ".$fields['name']. "was edited by: ".Auth::user()->firstname.' '.Auth::user()->lastname));

				return "success";
			}else{
				return 'It could not save, code already exists';
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteDistrict($id)
	{
		 $what_was_deleted = District::find($id)->name;

		$district_id = $what_was_deleted->id;

		 $deleteDistrict = District::find($id)->delete();

		 $deleteAllCircuitsUnderTheDistrict = Circuit::where('district_id',$district_id)->delete();

		 $deleteAllSchoolsUnderTheDistrict = School::where('district_id',$district_id)->delete();

		 $deleteDataCollectors = DataCollector::where('district_id',$district_id)->delete();

		 $deleteDataItinerary = DataIntenary::where('district_id',$district_id)->delete();


		 $fields = array(
		 			'user_id' => Auth::user()->id,
		 			'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
		 			'what_was_deleted' => $what_was_deleted." District was deleted and everything under it by: ".Auth::user()->firstname." ".Auth::user()->lastname);

		 $saveWhoDeletedADistrict = WhoDeletedWhat::create($fields);
		 

		 if ($deleteDistrict) {

			Cache::forget('all_districts');
			 Cache::forget('all_circuits');
			 Cache::forget('all_schools');


			 return 'success';
		 }else{
		 	return 'failed';
		 }
	}


	public function createCircuit($value='')
	{
			//return "You are about to create a circuit";
			//getting the posted details of the about to  create circuit
		$fields = array(
			'code' => Helper::generateCircuitUniqueCode($table  = 'circuits', $column = 'code'),
			'region_id' => Input::get('region'),
			'district_id' => Input::get('district'),
			'country_id' => Input::get('country', 1),
			'name' => Input::get('circuit_name'),
			'email' => Input::get('circuit_email'),
			'phone_number' => Input::get('circuit_phone_number'),
			'slug' => Helper::initials(Input::get('circuit_name').' Circuit'),
			'description' => Input::get('description'),
			'lat_long' => Input::get('circuit_location','lat23gdhdhdhdh4,long34fhfhtthg')
		);

			//validating the input
		$validation = Circuit::validate($fields);

		if ($validation !== true) {
			return $validation;
		}else{
			//Saving the newly created circuit
			$created_circuit = Circuit::create($fields);

			if ($created_circuit) {

				Cache::forget('all_circuits');

				WhoCreatedWhat::create(array('user_id' => Auth::user()->id, 
											 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
											 'what_was_created' => "Circuit with name: ".$created_circuit->name. "was created by: ".Auth::user()->firstname.' '.Auth::user()->lastname));



				return "success";


			}else{
				return 'It could not save, code already exists';
			}
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showCircuit($id)
	{
        return $circuitInfo = Circuit::find($id); 
  
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editCircuit($id)
	{
        return $circuitInfo = Circuit::find($id); 
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateCircuit($id)
	{
			//return "You are about to create a circuit";
			//getting the posted details of the about to  create circuit
		$fields = array(
			'code' => Helper::generateCircuitUniqueCode($table  = 'circuits', $column = 'code'),
			'region_id' => Input::get('region'),
			'district_id' => Input::get('district'),
			'country_id' => Input::get('country', 1),
			'name' => Input::get('circuit_name'),
			'email' => Input::get('circuit_email'),
			'phone_number' => Input::get('circuit_phone_number'),
			'slug' => Helper::initials(Input::get('circuit_name').' Circuit'),
			'description' => Input::get('description'),
			'lat_long' => Input::get('circuit_location','lat23gdhdhdhdh4,long34fhfhtthg')
		);

			//validating the input
		$validation = Circuit::validate($fields);

		if ($validation !== true) {
			return $validation;
		}else{
			//Saving the newly created circuit
			$update = DB::table('circuits')
            					->where('id', $id)
            						->update(array(
            							'region_id' => $fields['region_id'],
            							'district_id' => $fields['district_id'],
            							'country_id' => $fields['country_id'],
            							'name' => $fields['name'],
            							'email' => $fields['email'],
            							'phone_number' => $fields['phone_number'],
            							'slug' => $fields['slug'],
            							'description' => $fields['description'],
            							'lat_long' => $fields['lat_long']));
			if ($update) {

				Cache::forget('all_circuits');
				Cache::forget('all_schools');


				WhoEditedWhat::create(array('user_id' => Auth::user()->id, 
										 'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
										 'what_was_edited' => "Circuit with name: ".$fields['name']. "was edited by: ".Auth::user()->firstname.' '.Auth::user()->lastname));


				return "success";
			}else{
				return 'It could not save, code already exists';
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteCircuit($id)
	{
		$what_was_deleted = Circuit::find($id)->name;
        $deleteCircuit = Circuit::find($id)->delete(); 
        $deleteAllSchoolsUnderTheCircuit = School::where('circuit_id',$id)->delete();
        $deleteDataCollectors = DataCollector::where('circuit_id',$id)->delete();

        $fields = array(
		 			'user_id' => Auth::user()->id,
		 			'name' => Auth::user()->firstname.' '.Auth::user()->lastname,
		 			'what_was_deleted' => $what_was_deleted.' Circuit was deleted and everything under it by: '.Auth::user()->firstname.' '.Auth::user()->lastname);


		 $saveWhoDeletedACircuit = WhoDeletedWhat::create($fields);


        if ($deleteCircuit) {

			Cache::forget('all_circuits');
			Cache::forget('all_schools');

        	return 'success';
        }else{
        	return 'failed';
        }
	}



	public function allRegions($country = 1)
	{
		return $regions = Dashboard::allRegions($country);
	}

	public function allDistricts($country = 1)
	{
		return $regions = Dashboard::allDistricts($country);
	}

	public function allCircuits($country = 1)
	{
		return $regions = Dashboard::allCircuits($country);
	}

	public function allSchools($country = 1)
	{
		//return json_encode($regions) = Dashboard::allSchools($country);
		return $schools = Dashboard::allSchools($country);

		return json_encode($schools);
		
	}


	public function regionInfo($id)
	{
		return $regionsInfo = Dashboard::regionInfo($id);
	}
	public function districtInfo($id)
	{
		return $regionsInfo = Dashboard::districtInfo($id);
	}

	public function circuitInfo($id)
	{
		return $regionsInfo = Dashboard::circuitInfo($id);
	}

	public function schoolInfo($id)
	{
		return $regionsInfo = Dashboard::schoolInfo($id);
	}


	public function recentDataSubmittedByDataCollectorsOnASchool()
	{
		
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

		return Dashboard::recentDataSubmittedByDataCollectorsOnASchool($queryString, $values);
	}




	public function retrieveSchoolSMSCodes()
	{
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();

	    return $schoolsSMSCodes = DB::table('schools')->whereRaw($queryString, $values)->select('name','sms_code')->get();

	}




	public function getAllCreatedActionsByAdmins()
	{
				//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
	    return $created_actions_by_admins = DB::table('who_created_what')->get();

	}



	public function getAllEditedActionsByAdmins()
	{
				//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
	    return $edited_actions_by_admins = DB::table('who_edited_what')->get();

	}



	public function getAllDeletedActionsByAdmins()
	{
				//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

	    return $deleted_actions_by_admins = DB::table('who_deleted_what')->get();

	}








	public function gettingWeeklyPeriodsDataIsSubmittedUnder()
	{
		
		//getting the posted query parameters
	 	$inputs = Input::except('category','data_submission_interval');

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		$submission_category_to_check = Input::get('category');

		$data_submission_interval = Input::get('data_submission_interval');

	//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
	return self::getWeeksTermsAndYearsForWhichDataHasBeenSubmittedOn($queryString, $values, $submission_category_to_check);

	
	}





		/**
		 * Get the week, term and year that data has been submitted on for the termly reports.
		 * With respect to school report card
		 * @return Response
		 */
		public static function getWeeksTermsAndYearsForWhichDataHasBeenSubmittedOn($queryString, $values, $submission_category_to_check)
		{
			switch ($submission_category_to_check) {

				case 'normal_students_enrolment':
						$response = DB::table('school_enrolments')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;
				case 'normal_students_attendance':
						$response = DB::table('student_attendance')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;

				case 'special_students_enrolment':
						$response = DB::table('special_enrolment')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;

				case 'special_students_attendance':
						$response = DB::table('special_attendance')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;

				case 'teacher_attendance':
						$response = DB::table('punctuality_and_lessons')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;
					
				default:
					$response = array('info' => "Please check the category you are posting");
					break;
			}


			return Response::json(array("status"=>200,
										"message" => "Data returned if there exist any in a list form",
										"data"=>$response));


	}







	public function gettingWeeklyPeriodsDataIsSubmittedUnderForEachSchool()
	{
		
		//getting the posted query parameters
	 	$inputs = Input::except('category','data_submission_interval');

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		$submission_category_to_check = Input::get('category');

		$data_submission_interval = Input::get('data_submission_interval');


		//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
		return self::getWeeksTermsAndYearsForWhichDataHasBeenSubmittedOnASchool($queryString, $values, $submission_category_to_check);

	
	}





		/**
		 * Get the week, term and year that data has been submitted on for the termly reports.
		 * With respect to school report card
		 * @return Response
		 */
		public static function getWeeksTermsAndYearsForWhichDataHasBeenSubmittedOnASchool($queryString, $values, $submission_category_to_check)
		{
			switch ($submission_category_to_check) {

				case 'weekly_total':
						$response = DB::table('weekly_totals')->whereRaw($queryString, $values)->distinct()->
						select('week_number','term','year','cs_students_attendance_comments','cs_students_enrolment_comments','cs_teacher_attendance_comments','cs_teacher_class_management_comments')->get();
					break;

					case 'normal_students_enrolment':
						$response = DB::table('school_enrolments')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();

					break;
				case 'normal_students_attendance':
						$response = DB::table('student_attendance')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;

				case 'special_students_enrolment':
						$response = DB::table('special_enrolment')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;

				case 'special_students_attendance':
						$response = DB::table('special_attendance')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;

				case 'teacher_attendance':
						$response = DB::table('punctuality_and_lessons')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
					break;
					
				default:
					$response = array('info' => "Please check the category you are posting");
					break;
			}


			return Response::json(array("status"=>200,
										"message" => "Data returned if there exist any in a list form",
										"data"=>$response));


	}








	public function gettingTermlyPeriodsDataIsSubmittedUnder()
	{
		
		//getting the posted query parameters
	 	$inputs = Input::except('category');

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);

		$submission_category_to_check = Input::get('category');


	//return SchoolEnrolment::whereRaw($queryString, $values)->get();	
	return self::getTermsAndYearsForWhichDataHasBeenSubmittedOn($queryString, $values, $submission_category_to_check);

	
	}





		/**
		 * Get the term and year that data has been submitted on for the termly reports.
		 * With respect to school report card
		 * @return Response
		 */
	public static function getTermsAndYearsForWhichDataHasBeenSubmittedOn($queryString, $values, $submission_category_to_check)
	{
			switch ($submission_category_to_check) {

				case 'sanitation':
						$response = DB::table('sanitation')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;
				case 'security':
						$response = DB::table('security')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'equipment_recreational':
						$response = DB::table('equipment_recreational')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'school_structure':
						$response = DB::table('school_structure')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'funiture':
						$response = DB::table('punctuality_and_lessons')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'school_records_performance':
						$response = DB::table('school_records_performance')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'school_community_relationship':
						$response = DB::table('school_community_relationship')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'pupils_exams_performance':
						$response = DB::table('pupils_exams_performance')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'support_type':
						$response = DB::table('support_type')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'grant_type_tranches':
						$response = DB::table('grant_type_tranches')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'class_textbooks':
						$response = DB::table('class_textbooks')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				case 'teacher_units_covered':
					$response = DB::table('teacher_units_covered')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
						break;

				case 'general_school_situation':
						$response = DB::table('general_school_situation')->whereRaw($queryString, $values)->distinct()->select('term','year')->get();
					break;

				default:
					$response = array('info' => "Please check the category you are posting");
					break;
			}


			return Response::json(array("status"=>200,
										"message" => "Data returned if there exist any in a list form",
										"data"=>$response));


		}





		public function sendSMSMessageFromDashboard()
		{

			date_default_timezone_set('Africa/Accra');

			$message =  Input::get('message');

			$recipient_number =  Input::get('recipient_number');

		
			$sendMessage = SMS::sendSMS($message,$recipient_number);

			

			$sms_message_details = array('from' => Input::get('from'),
										 'sender_db_id' => Input::get('sender_db_id'),
										 'recipient_type' => Input::get('recipient_type'),
										 'recipient_db_id' => Input::get('recipient_db_id'),
										 'recipient_number' => $recipient_number,
										 'message' => $message,
										 'country_id' => 1,
										  'region_id' => Input::get('region_id'),
										  'district_id' => Input::get('district_id'),
										  'circuit_id' => Input::get('circuit_id'),
										  'school_id' => Input::get('school_id'),
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s'));



			$save_cs_sent_sms_message = DB::table('sms_messages')->insert($sms_message_details);


			if ($save_cs_sent_sms_message) {

				return Response::json(array("status"=>200,
                										"message" => "SMS saved and sent",
                										"data"=>$save_cs_sent_sms_message));

				 return $response;

			}


			$response = self::formattedResponseObject($status = 401, $message = "Failed", $data = array('message_details_to_save' => $sms_message_details));

			return $response;




		}







}