<?php

class AppErrorsReportingController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function androidPostedErrors()
	{
        
        $data = json_decode(@file_get_contents('php://input'));

		Log::info('Errors posted from android app', array('context' => $data));

		$fields = array('error_title' => $data->errorDetails->title,
						'error_description' => $data->errorDetails->description,
						'app_part' => 'mobile',
						'reporter_id' => $data->data_collector_id );


		$reportError = AppErrorsReports::create($fields);

		if ($reportError) {

			return Response::json(array("status"=>200,
										 "message"=>"OK"));
			
		}


		return Response::json(array("status"=>401,
										 "message"=>"failed"));

		
	}



	/**
	 * receive details of errors info posted from server and save 
	 *
	 * @return Response
	 */
	public function dashboardReportedErrors()
	{
        
        $data = json_decode(@file_get_contents('php://input'));

		Log::info('Errors posted from android app', array('context' => $data));



		$fields = array('error_title' => Input::get('title'),
						'error_description' => Input::get('description'),
						'app_part' => 'server',
						'reporter_id' => Input::get('reporter_id'));


		$reportError = AppErrorsReports::create($fields);


		if ($reportError) {

			return 'success';
		}

		return 'failed';


	}


	public function retrieveErrors()
	{

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}

		$queryString = substr($queryString, 0, -4);

		try {

			return $returnErrorsReported = AppErrorsReports::with('reporter')->whereRaw($queryString, $values)->get();

	        } catch (Exception $e) {
	        	
	        	return "Check the fields in your posted data";
	        }
	        
	}



	

}
