<?php
use App\MkPupilAggregatedData;
use Illuminate\Support\Collection;

class SubmitPupilAggregatedDataRegionGraphFormController extends \BaseController {

    public function submitForm()
    {

        $data = json_decode(@file_get_contents('php://input'), true);

        if ($data['subject'] == 'English') {

            DB::enableQueryLog();

            $result=DB::table('mk_attendance')
                ->select(DB::raw('attendance_academic_level as Level,attendance_academic_week as Week , sum(attendance_no_pupil_level) as pupil_level'))
                ->where('attendance_academic_year','=',$data['attendance_academic_year'])
                ->where('attendance_academic_term','=',$data['attendance_academic_term'])
                ->where('attendance_academic_subject','=',$data['subject'])
                ->where('region_id','=',$data['urlid'])
                ->groupBy('attendance_academic_level')
                ->groupBy('attendance_academic_week')
                ->get();


            $results = Collection::make($result)->groupBy('Level')->toJson();

            $response = [];

            $colors = ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"];

            $i = 0;
            foreach (json_decode($results, true) as $level => $data) {

                $data = array_column($data, 'pupil_level', 'Week');

                $response[$level]['label'] = 'Level ' . $level;
                $response[$level]['backgroundColor'] = $colors[$i];

                foreach ($weeks = range(1, 8) as $week) {
                    $response[$level]['data'][] = empty($data[$week]) ? 0 : (int)$data[$week];
                }

                $i++;
            }

            return array_values($response);



        }

        if ($data['subject'] == 'Math') {

            DB::enableQueryLog();

            $result=DB::table('mk_attendance')
                ->select(DB::raw('attendance_academic_level as Level,attendance_academic_week as Week  , sum(attendance_no_pupil_level) as pupil_level'))
                ->where('attendance_academic_year','=',$data['attendance_academic_year'])
                ->where('attendance_academic_term','=',$data['attendance_academic_term'])
                ->where('attendance_academic_subject','=',$data['subject'])
                ->where('region_id','=',$data['urlid'])
                ->groupBy('attendance_academic_level')
                ->groupBy('attendance_academic_week')
                ->get();


            $results = Collection::make($result)->groupBy('Level')->toJson();

            $response = [];

            $colors = ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"];

            $i = 0;
            foreach (json_decode($results, true) as $level => $data) {

                $data = array_column($data, 'pupil_level', 'Week');

                $response[$level]['label'] = 'Level ' . $level;
                $response[$level]['backgroundColor'] = $colors[$i];

                foreach ($weeks = range(1, 8) as $week) {
                    $response[$level]['data'][] = empty($data[$week]) ? 0 : (int)$data[$week];
                }

                $i++;
            }

            return array_values($response);

        }


    }

}
