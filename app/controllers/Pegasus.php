<?php

class PegasusController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function downloadXLSFileFromGdrive()
	{

		$url = Input::get('url');

		$filename = Input::get('filename');

		$name_to_save_file_with = "aliu.xlsx";

		$directoryPath = public_path(). '/uploads/pegasus/';

		//directory to save images into
		$file_path = public_path(). '/uploads/pegasus/'.$name_to_save_file_with;

		//this check if directory exist, if not it creates one
	    File::exists($directoryPath) or File::makeDirectory($directoryPath);



		// $files = glob('./uploads/*'); 

	    $xlsFile = file_put_contents($file_path, file_get_contents($url));

		// $contents = File::getRemote($url);

		if ($xlsFile === false)
		{
		    die("Couldn't fetch the file.");
		}


		if (File::exists($file_path))
		{
	     
	     $link = $this->process_google_sheet($file_path);
		    
		}

		 die("No file exists");

         
	    $link = $this->process_google_sheet($file_path);


        $contains_error =  strpos($link,"Error:");

       if($contains_error){
           return   explode(" ", substr($link,$contains_error));
       }

       else{

          $this->submitToPegasusRisesODK($filename,$link);
       }

	    

		
	}

    public function process_google_sheet($file_path){
    	
    	$ch = curl_init();

		$target_url = 'http://23.21.114.69/xlsform/';
		$filename = "AliuFile";
        //This needs to be the full path to the file you want to send.
		// $file_name_with_full_path = realpath('./uploads/'.$filename.'.xlsx');
        /* curl will accept an array here too.
         * Many examples I found showed a url-encoded string instead.
         * Take note that the 'key' in the array will be the key that shows up in the
         * $_FILES array of the accept script. and the at sign '@' is required before the
         * file name.
         */
	// $post = array('filename' => $filename.'.xlsx','file'=>'@'.$file_name_with_full_path);

	// $post = array('filename' => $filename.'.xlsx','file'=>'@'.$file_path);

	$args['file'] = new CurlFile($filename.'.xlsx', $file_path);

 
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$target_url);
	curl_setopt($ch, CURLOPT_POST,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec ($ch);
	curl_close ($ch);
	// var_dump($result);
	// exit();
		//var_dump($data_response);
	//echo (string)$result;

	foreach(explode('\n', $result) as $line)
	{
		
		 $temp_file_path = '';

          $start_ip = strpos($result,"http://23.21.114.69/xlsform/tmp");
          $xml = strpos($result,"target=");
          $error = strpos($result,"Error:");
          $required = strpos($result, "This field is required");
		if ($start_ip && $xml) 
		{
       // We found a string inside string
			//echo $start_ip ." ". $xml;
            $url_link = explode(" ", substr($line,$start_ip,$xml));
            $url_link = explode('\"', substr($url_link[0],0,-1));
			$temp_file_path = $url_link[0] ;

		}

		elseif($error) {
			# code...
			$temp_file_path = $line;
		}

		elseif($required) {
			# code...
			$temp_file_path = $line;
		}

		else{
            continue ;
		}
	}
	return $temp_file_path;
    //http://enketo.org/webform/preview?form=http://23.21.114.69/xlsform/tmp/tmpLUgvf9/odk_kaygee.xml
    // $line = explode('\n', $result);
    // while($line !== false){

    //    echo $line."yes  ";
    // }
    }


    public function submitToPegasusRisesODK($filename,$temp_url){
       // $url = 'http://23.21.114.69/xlsform/tmp/tmpLUgvf9/odk_kaygee.xml';
        // $url = $temp_url;
   
	    $xmlFile = file_put_contents('./uploads/'.$filename.'.xml', file_get_contents($temp_url));
		// header("Content-Type: application/vnd.ms-excel");
		// header('Content-Disposition: attachment; filename="' . $xlsFile . '"');
		// readfile($xlsFile);

		// if($xmlFile)
		// {
		// 	$handle = fopen("odk_kaygee.xml", "r");
		// 	if ($handle) 
		// 	{
		// 	    while (($line = fgets($handle)) !== false) 
		// 	    {
		// 	        // process the line read.
		// 	        echo  $line;
		// 	    }

		// 	    fclose($handle);
		// 	} 

		// 	else 
		// 	{
		// 	    // error opening the file.
		// 	} 
		$ch = curl_init();

		$target_url = 'https://odk-for-pegasus.appspot.com/formUpload';
        //This needs to be the full path to the file you want to send.
		$file_name_with_full_path = realpath('./uploads/'.$filename.'.xml');
        /* curl will accept an array here too.
         * Many examples I found showed a url-encoded string instead.
         * Take note that the 'key' in the array will be the key that shows up in the
         * $_FILES array of the accept script. and the at sign '@' is required before the
         * file name.
         */
	$post = array('filename' => $filename,'form_def_file'=>'@'.$file_name_with_full_path);
 
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$target_url);
	curl_setopt($ch, CURLOPT_POST,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec ($ch);
	curl_close ($ch);
	var_dump($result);
	unlink('./uploads/'.$filename.'.xml');
	unlink('./uploads/'.$filename.'.xlsx');

	//echo $result;

		// }

		// else
		// {
		// 	echo "Not fetched";
		// }

    }
  
}
