<?php

class SchoolReviewAnalysisController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function schoolsInclusiveness()
	{
		
		$question_category_code = 'AIS';
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//putting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


        return SchoolReviewAnalysis::schoolsInclusiveness($question_category_code, $queryString, $values);
	}

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function EffectiveTeachingAndLearning()
	{
		
		$question_category_code = 'ETL';
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


        return SchoolReviewAnalysis::EffectiveTeachingAndLearning($question_category_code, $queryString, $values);
	}


		/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function HealthyLevelOfSchools()
	{
		
		$question_category_code = 'HLS';
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


        return SchoolReviewAnalysis::HealthyLevelOfSchools($question_category_code, $queryString, $values);
	}


		/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function FriendlySchools()
	{
		
		$question_category_code = 'FSBGB';
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


        return SchoolReviewAnalysis::FriendlySchools($question_category_code, $queryString, $values);
	}


		/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function CommunityInvolvementInSchool()
	{
		
		$question_category_code = 'CIS';
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


        return SchoolReviewAnalysis::CommunityInvolvementInSchool($question_category_code, $queryString, $values);
	}


		/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function SafeAndProtectiveAndSchools()
	{
		
		$question_category_code = 'SPS';
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		//return SchoolEnrolment::whereRaw($queryString, $values)->get();


        return SchoolReviewAnalysis::SafeAndProtectiveAndSchools($question_category_code, $queryString, $values);
	}





}
