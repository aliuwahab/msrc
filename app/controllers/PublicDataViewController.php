<?php

class PublicDataViewController extends BaseController {


	public function normalEnrolment()
	{

	 	$school_id = Input::get('school_id');

	 	$week = Input::get('week_number');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	$term = Input::get('term');

		$school_details = self::findASchool($school_id);

		$normal_enrolment = DB::table('school_enrolments')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('week_number', $week)->where('data_collector_type', $data_collector_type)->get();

		$message = "Data returned successfully";

		$school_details = self::findASchool($school_id);

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $normal_enrolment);

		return $response;

	}


	public function normalAttendance()
	{

	 	$school_id = Input::get('school_id');

	 	$week = Input::get('week_number');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	$term = Input::get('term');

	 	$school_details = self::findASchool($school_id);

		$normal_attendance = DB::table('student_attendance')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('week_number', $week)->where('data_collector_type', $data_collector_type)->get();

		$message = "Data returned successfully";

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $normal_attendance);

		return $response;
		

	}


	public function specialEnrolment()
	{
		
	 	$school_id = Input::get('school_id');

	 	$week = Input::get('week_number');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	$term = Input::get('term');

	 	$school_details = self::findASchool($school_id);

		$special_enrolment = DB::table('special_enrolment')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('week_number', $week)->where('data_collector_type', $data_collector_type)->get();

		$message = "Data returned successfully";

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $special_enrolment);

		return $response;

		
	}


	public function specialAttendance()
	{
	
	 	$school_id = Input::get('school_id');

	 	$week = Input::get('week_number');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	$term = Input::get('term');

	 	$school_details = self::findASchool($school_id);

		$special_attendance = DB::table('special_attendance')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('week_number', $week)->where('data_collector_type', $data_collector_type)->get();

		$message = "Data returned successfully";

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $special_attendance);

		return $response;
		
	}


	public function teacherAttendance()
	{
	
	 	$school_id = Input::get('school_id');

	 	$week = Input::get('week_number');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	$term = Input::get('term');

		$school_details = self::findASchool($school_id);

		//Querying to get units covered
		return $data = Teachers::with(array('puntuality_and_lesson_plans' => function($query) use($term,$year,$week,$data_collector_type)
		{	
			//querying base on the parameters posted which is contain in the $querystring variable
		    $query->where('year', $year)->where('week_number', $week)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		}))->where('school_id',$school_id)->get();


		$message = "Data returned successfully";

		// $response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $data);

		// return $response;
		
	}


	public function classRoomManagement()
	{
	
	 	$school_id = Input::get('school_id');

	 	$week = Input::get('week_number');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	$term = Input::get('term');

		$school_details = self::findASchool($school_id);

		//Querying to get units covered
		return $teacher_class_room_management = Teachers::with(array('classroom_management' => function($query) use($term,$year,$week,$data_collector_type)
		{	
			//querying base on the parameters posted which is contain in the $querystring variable
		    $query->where('year', $year)->where('week_number', $week)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		}))->where('school_id',$school_id)->get();


		// $message = "Data returned successfully";

		// $response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $teacher_class_room_management);

		// return $response;
		
	}




	public function textbooks()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	// return $all_inputs = Input::all();

		$school_details = self::findASchool($school_id);

		$textbooks = DB::table('class_textbooks')->where('year', $year)->where('data_collector_type', $data_collector_type)->where('term', $term)->where('school_id', $school_id)->get();

		$message = "Data returned successfully";

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $textbooks);

		return $response;
		
	}



	public function pupilsPerformance()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$pupils_performance = DB::table('pupils_exams_performance')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$message = "Data returned successfully";

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $pupils_performance);

		return $response;
		
	}





	public function teacherUnitsCovered()
	{
	
	 	$school_id = Input::get('school_id');

	 	$week = Input::get('week_number');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

	 	$term = Input::get('term');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		 //Querying to get units covered
		return $teacher_units_covered = Teachers::with(array('units_covered' => function($query) use($term,$year,$data_collector_type,$week)
		{	
			//querying base on the parameters posted which is contain in the $querystring variable
		    $query->where('year', $year)->where('term', $term)->where('week_number', $week)->where('data_collector_type', $data_collector_type)->get();

		}))->where('school_id',$school_id)->get();


		// $response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $teacher_units_covered);

		// return $response;
		
	}


	public function recordBooks()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$record_books_performance = DB::table('school_records_performance')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $record_books_performance);

		return $response;
		
	}



	public function support()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$school_supports = DB::table('support_type')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $school_supports);

		return $response;
		
	}


	public function grant()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$school_grants = DB::table('grant_type_tranches')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $school_grants);

		return $response;
		
	}



	public function sanitation()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$school_sanitation = DB::table('sanitation')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $school_sanitation);

		return $response;
		
	}



	public function security()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$school_security = DB::table('security')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $school_security);

		return $response;
		
	}


	public function recreational()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$equipment_recreational = DB::table('equipment_recreational')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $equipment_recreational);

		return $response;
		
	}


	public function school_structure()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$school_structure = DB::table('school_structure')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $school_structure);

		return $response;
		
	}


	public function furniture()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$school_furniture = DB::table('funiture')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $school_furniture);

		return $response;
		
	}



	public function communityRelations()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$school_community_relationship = DB::table('school_community_relationship')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $school_community_relationship);

		return $response;
		
	}


	public function meetingsHeld()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$schools_meetings = DB::table('schools_meeting')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $schools_meetings);

		return $response;
		
	}


	public function generalSchoolSituation()
	{
	
	 	$school_id = Input::get('school_id');

	 	$term = Input::get('term');

	 	$year = Input::get('year');

	 	$data_collector_type = Input::get('data_collector_type');

		$school_details = self::findASchool($school_id);

		$message = "Data returned successfully";

		$general_school_situation = DB::table('general_school_situation')->where('school_id', $school_id)->where('year', $year)->where('term', $term)->where('data_collector_type', $data_collector_type)->get();

		$response = self::generateRequestResponse($code = 200, $status = "OK", $message, $school_details, $general_school_situation);

		return $response;
		
	}



	public static function rawQueryStringGenerator($inputs)
	{
		
		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);


		return $queryString;


	}




	public static function findASchool($school_id)
	{
		
		$school_info = DB::table('schools')->where('id',$school_id)->first();

		return $school_info;

	}



	public static function generateRequestResponse($code = 200, $status = "OK", $message = "Data returned successfully", $school_details, $data = NULL)
	{
		
		return Response::json(array("code"=> $code,
									"status" => $status,
									"message"=>$message,
									'school' => $school_details,
									'data' => $data));
	}

}
