<?php
use App\MkAttendance;
use Illuminate\Http\Request;

class SubmitAttendanceCountryGraphFormController extends \BaseController {

    public function submitForm()
    {

        $data = json_decode(@file_get_contents('php://input'), true);

        if ($data['subject'] == 'English' && $data['grade'] == 'P4' ) {


            DB::enableQueryLog();

            $result2 = DB::table('mk_pupil_aggregated')
                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
                ->where('academic_year', '=', $data['academic_year'])
                ->where('grade', '=', 'P4')
                ->groupBy('grade')
                ->get();

//            $result3 = DB::table('mk_pupil_aggregated')
//                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
//                ->where('academic_year', '=', $data['academic_year'])
//                ->where('grade', '=', 'P6')
//                ->groupBy('grade')
//                ->get();

            //  $resultArray=[$result,$result2,$result3];


//            return array("result"=>$result,"result2"=>$result2,"result3"=>$result3);


            // dd($resultArray);
//dd('hello');
//            return $resultArray;

            return $result2;

            // grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph from `mk_pupil_aggregated`  where academic_year='2015' and grade = 'P4'

        }



        /////////
        ///
        ///
        ///

        if ($data['subject'] == 'English' && $data['grade'] == 'P5' ) {


            DB::enableQueryLog();
//            $result = DB::table('mk_pupil_aggregated')
//                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
//                ->where('academic_year', '=', $data['academic_year'])
//                ->groupBy('grade')
//                ->get();

            $result2 = DB::table('mk_pupil_aggregated')
                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
                ->where('academic_year', '=', $data['academic_year'])
                ->where('grade', '=', 'P5')
                ->groupBy('grade')
                ->get();

//            $result3 = DB::table('mk_pupil_aggregated')
//                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
//                ->where('academic_year', '=', $data['academic_year'])
//                ->where('grade', '=', 'P6')
//                ->groupBy('grade')
//                ->get();

            //  $resultArray=[$result,$result2,$result3];


//            return array("result"=>$result,"result2"=>$result2,"result3"=>$result3);


            // dd($resultArray);
//dd('hello');
//            return $resultArray;

            return $result2;

            // grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph from `mk_pupil_aggregated`  where academic_year='2015' and grade = 'P4'

        }




        if ($data['subject'] == 'English' && $data['grade'] == 'P6' ) {


            DB::enableQueryLog();
//            $result = DB::table('mk_pupil_aggregated')
//                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
//                ->where('academic_year', '=', $data['academic_year'])
//                ->groupBy('grade')
//                ->get();

//            $result2 = DB::table('mk_pupil_aggregated')
//                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
//                ->where('academic_year', '=', $data['academic_year'])
//                ->where('grade', '=', 'P4')
//                ->groupBy('grade')
//                ->get();

            $result3 = DB::table('mk_pupil_aggregated')
                ->select(DB::raw('grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph'))
                ->where('academic_year', '=', $data['academic_year'])
                ->where('grade', '=', 'P6')
                ->groupBy('grade')
                ->get();

            //  $resultArray=[$result,$result2,$result3];


//            return array("result"=>$result,"result2"=>$result2,"result3"=>$result3);


            // dd($resultArray);
//dd('hello');
//            return $resultArray;

            return $result3;

            // grade as Grade,sum(english_beginner_text) as Beginner, sum(english_letter_text) as Letter, sum(english_word_text) as Word, sum(english_sentence_text) as Sentence, sum(english_paragraph_text) as Paragraph from `mk_pupil_aggregated`  where academic_year='2015' and grade = 'P4'

        }



        if ($data['subject'] == 'Maths' && $data['grade'] == 'P4') {

            DB::enableQueryLog();
            $result = DB::table('mk_pupil_aggregated')
                ->select(DB::raw('grade as Grade,sum(math_beginner_text) as Beginner , sum(math_digit_one_text) as One , sum(math_digit_two_text) as Two, sum(math_addition_text) as Addition , sum(math_subtraction_text) as Subtraction'))
                ->where('academic_year', '=', $data['academic_year'])
                ->where('grade', '=', 'P4')
                ->groupBy('grade')
                ->get();



            return $result;


        }


        if ($data['subject'] == 'Maths' && $data['grade'] == 'P5') {

            DB::enableQueryLog();
            $result2 = DB::table('mk_pupil_aggregated')
                ->select(DB::raw('grade as Grade,sum(math_beginner_text) as Beginner , sum(math_digit_one_text) as One , sum(math_digit_two_text) as Two, sum(math_addition_text) as Addition , sum(math_subtraction_text) as Subtraction'))
                ->where('academic_year', '=', $data['academic_year'])
                ->where('grade', '=', 'P5')
                ->groupBy('grade')
                ->get();



            return $result2;


        }




        if ($data['subject'] == 'Maths' && $data['grade'] == 'P6') {

            DB::enableQueryLog();
            $result3 = DB::table('mk_pupil_aggregated')
                ->select(DB::raw('grade as Grade,sum(math_beginner_text) as Beginner , sum(math_digit_one_text) as One , sum(math_digit_two_text) as Two, sum(math_addition_text) as Addition , sum(math_subtraction_text) as Subtraction'))
                ->where('academic_year', '=', $data['academic_year'])
                ->where('grade', '=', 'P6')
                ->groupBy('grade')
                ->get();



            return $result3;


        }











    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */


    public function destroy($id)
    {
        //
    }


}
