<?php

use Illuminate\Http\Request;

//use Illuminate\Http\Request;

class SubmitClassObservationFormController extends BaseController
{


    /**
     * @return int
     */
    public function submitForm()
    {

        $data = json_decode(@file_get_contents('php://input'), true);

        $user = Auth::user();
        $id = $user->id;
        $level_id = $user->level_id;
        $circuit = Circuit::where('id', $level_id)->first();

//        $teacher_name = DB::table("teachers")
//            ->select(DB::raw('CONCAT(last_name, " ",first_name) AS full_name'))
//            ->get();
//


//        return $teacher_name;

//        if ($data['class_ob_teacher_name']['full_name'] == $teacher_name) {
    $MkClassObservation = new MkDataClassObservation();
    $MkClassObservation->data_collector_id = $id;
    $MkClassObservation->class_ob_level = $data['class_ob_level'];
    $MkClassObservation->saved_class_ob_id = 1;
    $MkClassObservation->class_ob_subject = $data['class_ob_subject'];


    $MkClassObservation->class_ob_teacher_name = $data['class_ob_teacher_name']['full_name'];
    $MkClassObservation->class_ob_enrolled_p_four = $data['class_ob_enrolled_p_four'];
    $MkClassObservation->class_ob_enrolled_p_five = $data['class_ob_enrolled_p_five'];
    $MkClassObservation->class_ob_enrolled_p_six = $data['class_ob_enrolled_p_six'];

    $MkClassObservation->class_ob_enrolled_p_total = $data['class_ob_enrolled_p_four'] + $data['class_ob_enrolled_p_five'] + $data['class_ob_enrolled_p_six'];

    $MkClassObservation->class_ob_absent_p_five = $data['class_ob_absent_p_five'];
    $MkClassObservation->class_ob_absent_p_six = $data['class_ob_absent_p_six'];

    $MkClassObservation->class_ob_absent_p_total = $data['class_ob_absent_p_five'] + $data['class_ob_absent_p_four'] + $data['class_ob_absent_p_six'];

    $MkClassObservation->class_ob_absent_p_four = $data['class_ob_absent_p_four'];
    $MkClassObservation->class_ob_date_monitoring = $data['class_ob_date_monitoring'];
    $MkClassObservation->class_ob_duration = $data['class_ob_duration'];
    $MkClassObservation->class_ob_attendance_summary = $data['class_ob_attendance_summary'];
    $MkClassObservation->ti_question_two_answer = $data['ti_question_two_answer'];
    $MkClassObservation->ti_question_one_answer = $data['ti_question_one_answer'];
    $MkClassObservation->ti_question_two_comment = $data['ti_question_two_comment'];
    $MkClassObservation->attendance_no_pupil_level = $data['attendance_no_pupil_level'];
    $MkClassObservation->ti_question_one_comment = $data['ti_question_one_comment'];
    $MkClassObservation->ti_question_three_answer = $data['ti_question_three_answer'];
    $MkClassObservation->ti_question_three_comment = $data['ti_question_three_comment'];
    $MkClassObservation->ti_lesson_summary = $data['ti_lesson_summary'];
    $MkClassObservation->la_question_one_answer = $data['la_question_one_answer'];
    $MkClassObservation->la_question_one_comment = $data['la_question_one_comment'];
    $MkClassObservation->la_question_two_answer = $data['la_question_two_answer'];
    $MkClassObservation->la_question_two_comment = $data['la_question_two_comment'];
    $MkClassObservation->la_question_three_answer = $data['la_question_three_answer'];
    $MkClassObservation->la_question_three_comment = $data['la_question_three_comment'];
    $MkClassObservation->la_lesson_summary = $data['la_lesson_summary'];
    $MkClassObservation->cl_question_one_reading_answer = $data['cl_question_one_reading_answer'];
    $MkClassObservation->cl_question_one_saying_answer = $data['cl_question_one_saying_answer'];
    $MkClassObservation->cl_question_one_doing_answer = $data['cl_question_one_doing_answer'];
    $MkClassObservation->cl_question_one_writing_answer = $data['cl_question_one_writing_answer'];
    $MkClassObservation->cl_question_one_speaking_answer = $data['cl_question_one_speaking_answer'];
    $MkClassObservation->cl_question_one_comment = $data['cl_question_one_comment'];
    $MkClassObservation->cl_question_two_answer = $data['cl_question_two_answer'];
    $MkClassObservation->cl_question_two_comment = $data['cl_question_two_comment'];
    $MkClassObservation->cl_question_three_answer = $data['cl_question_three_answer'];
    $MkClassObservation->cl_question_three_comment = $data['cl_question_three_comment'];
    $MkClassObservation->cl_question_four_comment = $data['cl_question_four_comment'];
    $MkClassObservation->cl_classroom_summary = $data['cl_classroom_summary'];

    $MkClassObservation->circuit_id = $circuit->id;
    $MkClassObservation->school_id = null;
    $MkClassObservation->district_id = $circuit->district_id;
    $MkClassObservation->country_id = $circuit->country_id;
    $MkClassObservation->region_id = $circuit->region_id;


    if ($MkClassObservation->save()) {

        return 1;
    }

    return 0;

//}
//else{
//    return 3;
//}
    }


    public function getTeachers()
    {
        $get_teachers = DB::table("teachers")
            ->select('id', DB::raw('CONCAT(last_name, " ",first_name) AS full_name'))->get();
        return $get_teachers;
    }


}
