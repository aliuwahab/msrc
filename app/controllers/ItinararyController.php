<?php

class ItinararyController extends BaseController {

	

	/**
	 * Store a newly created resource in storage.
	 *creates a new itinerary for a district
	 * @return Response
	 */
	public function createItinerary()
	{

		$district_id = Input::get('district_id');
		//getting the posted details of the newly created data collector i.e headmaster or circuit supervisor
		$fields = array(
			'name' => Input::get('name'),
			'description' => Input::get('description'),
			'start_date' => Input::get('start_date'),
			'end_date' => Input::get('end_date'),
			'country_id' => Input::get('national_id',1),
			'region_id' => Input::get('region_id'),
			'district_id' => Input::get('district_id')
		);
		//return $fields;
		//validating the input
		$validation = DataIntenary::validate($fields);

		if ($validation !== true) {
			return $validation;
		}else{
			//Save the newly created itinerary
			if (DataIntenary::create($fields)) {

				//sending a notification to indicate a new Itinerary has been created
				$scope = 'district';
				$id_of_scope = $district_id;
				$purpose = 'itinerary';
				$information = 'New Itinerary Available';
				
				return "success";

			}else{

				return 'The PHONE NUMBER already exist';
				
			}
		}
	}

	//list all itinerary created for a district
	// public function allDistrictItineraryForAdmin($district = 1);
	// {
		
	// 	//return $allItinerary  = DataIntenary::where('district_id', '>', $district)->get();
	// 	return $allItinerary = DB::table('itenary')->where('district_id', $district)->get();

	// }
	

	//list all itinerary created for a district
	public function apiRequestForAllDistrictItinerary()
	{
		//return 'Am Here';
		//getting the data posted to the api
		$data = Input::all();
		//$data['district_id'] = 1;
		//return $data;

		//calling AndroidAPIRequest class and apiRequestForAllItinariesForADistrict
		return $allItinerary =  AndroidAPIRequest::apiRequestForAllItinariesForADistrict($data);
	}


	public function adminRequestForIntineraries()
	{

		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {

			$queryString .= $key.' = ? and ';

		}


		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
	
		$allItinerary = DB::table('itenary')->whereRaw($queryString, $values)->get();
		
		return Response::json(array("itinerary"=> $allItinerary));

	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editItinerary()
	{
        return $itineraryInfo = DataIntenary::find($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateItinerary($id)
	{
		//getting the posted details of the newly created data collector i.e headmaster or circuit supervisor
		$fields = array(
			'name' => Input::get('name'),
			'description' => Input::get('description'),
			'slug'	 => Helper::initials(Input::get('name').'District Itinerary'),
			'start_date' => Input::get('start_date'),
			'end_date' => Input::get('end_date'),
			'country_id' => Input::get('country_id',1),
			'region_id' => Input::get('region_id',1),
			'district_id' => Input::get('district_id',1)
		);
		//validating the input
		$validation = DataIntenary::validate($fields);

		if ($validation !== true) {
			return $validation;
		}else{
			//update itinerary
			$update = DB::table('itenary')
            					->where('id', $id)
            						->update(array(
            							'name' => $fields['name'],
            							'description' => $fields['description'],
            							'slug' => $fields['slug'],
            							'start_date' => $fields['start_date'],
            							'end_date' => $fields['end_date'],
            							'country_id' => $fields['country_id'],
            							'region_id' => $fields['region_id'],
            							'district_id' => $fields['district_id']));
			if ($update) {
				return "success";
			}else{
				return 'failed';
			}
		}
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showItinerary($id)
	{
         return $itineraryInfo = DataIntenary::find($id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteItinerary($id)
	{
		DB::table('answers')->where('itenary_id', $id)->delete();
		DB::table('community_involvement')->where('itenary_id', $id)->delete();
		DB::table('effective_teaching_learning')->where('itenary_id', $id)->delete();
		DB::table('friendly_schools')->where('itenary_id', $id)->delete();
		DB::table('healthy_level')->where('itenary_id', $id)->delete();
		DB::table('school_inclusive_answers')->where('itenary_id', $id)->delete();
		DB::table('safe_protective')->where('itenary_id', $id)->delete();
		$deleteItinerary = DataIntenary::find($id)->delete();

		if ($deleteItinerary) {
			return 'success';
		}else{
			return 'failed';
		}
	}

}
