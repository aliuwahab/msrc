<?php

class ApiController extends \BaseController {

		protected $statusCode;

		public function getStatusCode($statusCode)
		{
			return $this->statusCode;
		}
		public function setStatusCode($statusCode)
		{
			$this->statusCode = $statusCode;
		}

		public function respondNotFound($message=  'Not Found')
		{
			return Response::json([

				'error' => [
				   'message' => $message,
				   'status_code' => $this->getStatusCode()
				   ]
				]);
		}

}