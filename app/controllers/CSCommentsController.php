<?php

class CSCommentsController extends BaseController {







	public function csDashboardAuthentication()
	{
		$cs_phone_number = Input::get('cs_phone_number');

		$cs_identity_code = Input::get('cs_identity_code');

		$validity_check = DB::table('data_collectors')->where('phone_number', $cs_phone_number)->where('identity_code', $cs_identity_code)->where('type', 'circuit_supervisor')->first();


		if (count($validity_check) > 0) {

			Session::put('authenticated_cs', $validity_check);


			return Response::json(array("code" => 200,
										"status"=>"OK",
										"message"=>"CS successfully authenticated",
										"authticated_cs" => $validity_check));
		}



		return Response::json(array("code" => 401,
									 "status"=>"Failed",
									 "message"=>"failed cs authentication"));



	}


	/**
	 * retrieve data from different categories for CS Dashboard
	 *
	 * @return Response
	 */
	public function retrieveDataFromDifferentSectionForCSDashboard(){

		$category_of_data = Input::get('category_of_data', 'no-category');

		//getting the posted query parameters
		$inputs = Input::except('category_of_data');

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);


		// return $queryString;
	

		switch ($category_of_data) {


			case 'student_enrolment':
				  
				  $normal_enrolment = DB::table('school_enrolments')->whereRaw($queryString, $values)->get();

				  $special_enrolment = DB::table('special_enrolment')->whereRaw($queryString, $values)->get();

				  $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('normal_students_enrolment' => $normal_enrolment, 'special_students_enrolment' => $special_enrolment));

				  return $response;

				break;

			case 'students_attendance':

				  $normal_attendance = DB::table('student_attendance')->whereRaw($queryString, $values)->get();

				  $special_attendance = DB::table('special_attendance')->whereRaw($queryString, $values)->get();

				  $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('normal_students_attendance' => $normal_attendance, 'special_students_attendance' => $special_attendance));

				  return $response;

				break;

			case 'teachers_asessement':

				  $teacher_attendance = DB::table('punctuality_and_lessons')->whereRaw($queryString, $values)->get();

				  $teachers_classmanagement = DB::table('teachers_class_management')->whereRaw($queryString, $values)->get();

				  // $teachers_excersises_given = DB::table('teachers_class_management')->whereRaw($queryString, $values)->get();

				  $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('teacher_attendance' => $teacher_attendance, 'teachers_classmanagement' => $teachers_classmanagement));

				  return $response;
				break;
			case 'sanitation':

				  $sanitation = DB::table('sanitation')->whereRaw($queryString, $values)->get();

				  $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('sanitation' => $sanitation));

				  return $response;

				break;
			case 'security':

				 $security = DB::table('security')->whereRaw($queryString, $values)->get();

				 $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('security' => $security));

				  return $response;

				break;
			case 'meeting':

				 $schools_meeting = DB::table('schools_meeting')->whereRaw($queryString, $values)->get();

				 $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('schools_meeting' => $schools_meeting));

				  return $response;

				break;
			case 'general_issues':
				 
				  $general_school_situation = DB::table('general_school_situation')->whereRaw($queryString, $values)->get();

				  $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('general_school_situation' => $general_school_situation));

				  return $response;

				break;
			case 'pupil_performance':
				  
				  $pupils_exams_performance = DB::table('pupils_exams_performance')->whereRaw($queryString, $values)->get();

				  $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('pupils_exams_performance' => $pupils_exams_performance));

				  return $response;

				break;
			case 'community_relationship':

					$school_community_relationship = DB::table('school_community_relationship')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('school_community_relationship' => $school_community_relationship));

					return $response;

				break;
			case 'teacher_units_covered':

					$teacher_units_covered = DB::table('teacher_units_covered')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('teacher_units_covered' => $teacher_units_covered));

					return $response;

				break;
			case 'class_textbooks':

					$class_textbooks = DB::table('class_textbooks')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('class_textbooks' => $class_textbooks));

					return $response;

				break;

			case 'recordbooks_in_school':

				 	$recordbooks_in_school = DB::table('school_records_performance')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('recordbooks_in_school' => $recordbooks_in_school));

					return $response;

				break;
			case 'support_to_school':

					$support_type = DB::table('support_type')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('support_type' => $support_type));

					return $response;

				break;
			case 'grants_to_school':
					
					$grant_type_tranches = DB::table('grant_type_tranches')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('grant_type_tranches' => $grant_type_tranches));

					return $response;

				break;
			case 'sports_recreational_facilities':
					
					$equipment_recreational = DB::table('equipment_recreational')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('equipment_recreational' => $equipment_recreational));

					return $response;

				break;
			case 'school_structure':
					
					$school_structure = DB::table('school_structure')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('school_structure' => $school_structure));

					return $response;

				break;
			case 'school_furniture':

					$school_furniture = DB::table('funiture')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('school_furniture' => $school_furniture));

					return $response;

				break;
			case 'itineraries':
					
					$itineraries = DB::table('itenary')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('itineraries' => $itineraries));

					return $response;

				break;
			case 'school_inclusiveness':
					
					$school_inclusive_answers = DB::table('school_inclusive_answers')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('school_inclusiveness' => $school_inclusive_answers));

					return $response;

				break;
			case 'effective_teaching':

					$effective_teaching = DB::table('effective_teaching_learning')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('effective_teaching' => $effective_teaching));

					return $response;

				break;
			case 'healthy_level':
	
					$healthy_level = DB::table('healthy_level')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('healthy_level' => $healthy_level));

					return $response;

				break;
			case 'safe_and_protective':
					
					$safe_and_protective_schools = DB::table('safe_protective')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('safe_and_protective_schools' => $safe_and_protective_schools));

					return $response;

				break;
			case 'school_friendliness':
					
					$school_friendliness = DB::table('friendly_schools')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('school_friendliness' => $school_friendliness));

					return $response;

				break;
			case 'community_involvement':
					
					$community_involvement = DB::table('community_involvement')->whereRaw($queryString, $values)->get();

					$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('community_involvement' => $community_involvement));

					return $response;

				break;
			default:

					$response = self::formattedResponseObject($status = 200, $message = "Check your request parameters and try again");

					return $response;

				break;



		}



	}













	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function saveACSCommentOnSubmittedData()
	{
		$comment = Input::get('cs_comment', 'No comment by Circuit Supervisor');

		$id_of_data_to_insert_comment_on = Input::get('submission_id');

		$submission_category = Input::get('submission_category');

		switch ($submission_category) {

			case 'student_enrolment':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'weekly_totals',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_students_enrolment_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'student_attendance':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'weekly_totals',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_students_attendance_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'weekly_teacher_attendance':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'weekly_totals',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_teacher_attendance_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'weekly_teachers_class_management':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'weekly_totals',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_teacher_class_management_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'sanitation':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'sanitation',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'security':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'security',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'equipment_recreational':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'equipment_recreational',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'school_structure':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'school_structure',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'furniture':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'funiture',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'school_records_performance':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'school_records_performance',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'school_community_relationship':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'school_community_relationship',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'punctuality_and_lessons':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'punctuality_and_lessons',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'teachers_class_management':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'teachers_class_management',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'general_school_situation':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'general_school_situation',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'schools_meeting':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'schools_meeting',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'support_type':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'support_type',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'grant_type_tranches':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'grant_type_tranches',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'class_textbooks':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'class_textbooks',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'teacher_units_covered':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'teacher_units_covered',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'school_inclusiveness':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'teacher_units_covered',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;

				break;
			case 'effective_teaching_learning':

				$save_cs_comment = self::saveAComment($table_to_insert_comment = 'teacher_units_covered',$id_of_data_to_insert_comment_on,$array_of_comment = array('cs_comments' => $comment));
				
				$save_cs_comment = self::formattedResponseObject($status = 200, $message = "OK", $data = array('save_cs_comment' => $save_cs_comment));

				return $save_cs_comment;
				
				break;
			case 'healthy_level':
				# code...
				break;
			case 'safe_protective':
				# code...
				break;
			case 'friendly_schools':
				# code...
				break;
			case 'community_involvement':
				# code...
				break;
			default:
				# code...
				break;


		}



	}









	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function retrieveRescentSubmissionsForCSDashboard()
	{
		
		//getting the posted query parameters
		$inputs = Input::except('query_limit');

		$query_limit = Input::get('query_limit', 10);

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		

		try {
				
				//querying base on the parameters posted which is contain in the $querystring variable
				$recentdataSubmitted = DB::table('recent_data_submitted')->whereRaw($queryString, $values)->orderBy('id',"desc")->take($query_limit)->get();


				$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('rescent_submissions' => $recentdataSubmitted));


				return $response;


	      			
	   	} catch (Exception $e) {

	      	
	      	$response = self::formattedResponseObject($status = 401, $message = "Failed");


			return $response;
	      			
	   }



	}






	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public static function saveAComment($table_to_insert_comment,$id_of_data_to_insert_comment_on,$array_of_comment)
	{
	
			$update_comment = DB::table($table_to_insert_comment)->where('id', $id_of_data_to_insert_comment_on)->update($array_of_comment);


			if ($update_comment) {
				
				 $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('comment_saved' => $update_comment));

				 return $response;
			}


			$response = self::formattedResponseObject($status = 401, $message = "Failed", $data = array('coment_to_save' => $array_of_comment));

			return $response;

	}





	/**
	 * save the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public static function saveCSSentSMSMessages()
	{
	

			$sms_message_details = array('from' => Input::get('from'),
										 'sender_db_id' => Input::get('headteacher_id'),
										 'recipient_type' => Input::get('recipient_type'),
										 'recipient_db_id' => Input::get('recipient_db_id'),
										 'recipient_number' => Input::get('recipient_number'),
										 'message' => Input::get('message'),
										 'country_id' => 1,
										  'region_id' => Input::get('region_id'),
										  'district_id' => Input::get('district_id'),
										  'circuit_id' => Input::get('circuit_id'),
										  'school_id' => Input::get('school_id') );

			$save_cs_sent_sms_message = DB::table('sms_messages')->insert($sms_message_details);


			if ($save_cs_sent_sms_message) {
				
				 $response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('sms_message_saved' => $save_cs_sent_sms_message));

				 return $response;
			}


			$response = self::formattedResponseObject($status = 401, $message = "Failed", $data = array('message_details_to_save' => $sms_message_details));

			return $response;

	}



	/**
	 * save the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public static function retrieveCSSentSMSMessages()
	{
	

		//getting the posted query parameters
		$inputs = Input::all();

		// $query_limit = Input::get('query_limit', 10);

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'OR'. Kaygee made an edit from 'and' to 'or' ';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? OR  ';
		}
		
		//removing the last ' and' which will be appended
		$queryString = substr($queryString, 0, -4);

			
		$sms_messages = DB::table('sms_messages')->whereRaw($queryString, $values)->get();

		$response = self::formattedResponseObject($status = 200, $message = "OK", $data = array('sms_messages' => $sms_messages));

		return $response;

	}




	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroyAComment($id)
	{
		//
	}





	public static function formattedResponseObject($status = 200, $message = "OK", $data = null)
	{
		
		
		$response = Response::json(array("status"=>200, "message"=>"OK", "data"=> $data));


		return $response;

	}


















}
