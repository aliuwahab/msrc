<?php
/**
 * Created by PhpStorm.
 * User: aliuwahab
 * Date: 27/10/2018
 * Time: 7:58 PM
 */
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;



class UpdateWeeklyTotalsCommand extends ScheduledCommand{


    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'msrc:calculate-weekly-totals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This send reminders to data collectors that have not submitted data on itineraries that have less than 3days to expire ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * When a command should run
     *
     * @param Scheduler $scheduler
     * @return \Indatus\Dispatcher\Scheduling\Schedulable
     */
    public function schedule(Schedulable $scheduler)
    {
        //return $scheduler;
        return $scheduler->everyMinutes(1);
        //scheduling a push notification on itnerary expiration
//        return $scheduler->daily()->hours(10)->minutes(0);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Log::info('This updates the weekly totals');

        echo "This updates the weekly totals";




    }


}