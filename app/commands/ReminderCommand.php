<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ReminderCommand extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'msrc:reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This is to send gcm notification as a reminder on monday and friday morning to remind data collectors about weekly data submission';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		//return $scheduler->daily()->hours(8)->minutes(0);
		//return $scheduler->everyMinutes(1);

	    return $scheduler->daysOfTheWeek(array(
                Scheduler::TUESDAY,
                Scheduler::THURSDAY
            ))->hours(8)->minutes(30);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//this is what is done when the command runs
        Log::info('The automatic task runner by indantus dispatcher set by Aliu to send week days reminders');

        $purpose = 'Week';

		$information = "If you haven't submitted your data for this week, please do";

		//retrieving gcm ids to send notification to
    	$gcmIDS = DB::table('gcm_ids')->get();
		$arrayOfGCMIDS = array();
		foreach ($gcmIDS as $gcm_id) {
			array_push($arrayOfGCMIDS, PushNotification::Device($gcm_id->gcm_id));
		}
		// print_r($arrayOfGCMIDS);
		// exit();
		$devices = PushNotification::DeviceCollection($arrayOfGCMIDS);
		$message = NotificationsController::messageToBeSentThroughNotification($purpose, $information);
		//return self::normalSendingOfNotification($devices,$message);
		
		return NotificationsController::packagesSendNotification($devices,$message);

	}



}
