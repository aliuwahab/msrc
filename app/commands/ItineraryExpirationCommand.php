<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ItineraryExpirationCommand extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'msrc:itinerary-reminder';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This send reminders to data collectors that have not submitted data on itineraries that have less than 3days to expire ';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		//return $scheduler;
		//return $scheduler->everyMinutes(1);
		//scheduling a push notification on itnerary expiration
		return $scheduler->daily()->hours(10)->minutes(0);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
        Log::info('The automatic task runner by Aliu to send itinerary expiry notification');
		
		$purpose = 'Itinerary_deadline';
		$information = "Your itinerary(s) have less than three days to expire, please submit data on them if you haven't";

		//logic follows here when I am finally ready to work it out
		$today = date('Y-m-d');
		//getting all itineraries that have their expiry days less than 3 days
		$allItinerary = DB::table('itenary')->where('end_date', '>=',$today)->get();
		foreach ($allItinerary as $itinerary) {
			$daysDifference = $itinerary->end_date-$today;
			$itineraryDistrict = $itinerary->district_id;
			if ($daysDifference <= 3) {
					//getting the gcm ids of data collectors who falls under a district
					//that has itinerary with end date less than 3days
					$gcmIDS = DB::table('gcm_ids')->where('district_id',$itineraryDistrict)->get();
					$arrayOfGCMIDS = array();
					foreach ($gcmIDS as $gcm_id) {
						array_push($arrayOfGCMIDS, PushNotification::Device($gcm_id->gcm_id));
					}
					$devices = PushNotification::DeviceCollection($arrayOfGCMIDS);
					$message = NotificationsController::messageToBeSentThroughNotification($purpose, $information);
					//return self::normalSendingOfNotification($devices,$message);
					return NotificationsController::packagesSendNotification($devices,$message);
			}
		}
	}

	// /**
	//  * Get the console command arguments.
	//  *
	//  * @return array
	//  */
	// protected function getArguments()
	// {
	// 	return array(
	// 		array('example', InputArgument::REQUIRED, 'An example argument.'),
	// 	);
	// }

	// /**
	//  * Get the console command options.
	//  *
	//  * @return array
	//  */
	// protected function getOptions()
	// {
	// 	return array(
	// 		array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
	// 	);
	// }

}
