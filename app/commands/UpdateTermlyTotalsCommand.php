<?php
/**
 * Created by PhpStorm.
 * User: aliuwahab
 * Date: 27/10/2018
 * Time: 7:58 PM
 */
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;



class UpdateTermlyTotalsCommand extends ScheduledCommand{


    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'msrc:totals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This reads the individual weekly submissions tables and update the respective weekly totals columns';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * When a command should run
     *
     * @param Scheduler $scheduler
     * @return \Indatus\Dispatcher\Scheduling\Schedulable
     */
    public function schedule(Schedulable $scheduler)
    {
        //return $scheduler;
        return $scheduler->everyMinutes(1);
        //scheduling a push notification on itnerary expiration
//        return $scheduler->daily()->hours(10)->minutes(0);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Log::info('This updates the termly totals');

        echo "This updates the termly totals";













//
//        foreach ($posted_textbook_totals as $school_textbooks_for_a_term) {
//
//            $find_school_data_on_termly_totals = DB::table('termly_totals')
//                ->where('district_id', $school_textbooks_for_a_term['district_id'])
//                ->where('school_id', $school_textbooks_for_a_term['school_id'])
//                ->where('term', $school_textbooks_for_a_term['term'])
//                ->where('year', $school_textbooks_for_a_term['year'])
//                ->first();
//
//            if (count($find_school_data_on_termly_totals) > 0) {
//
//                $update = DB::table('termly_totals')
//                    ->where('district_id', $school_textbooks_for_a_term['district_id'])
//                    ->where('school_id', $school_textbooks_for_a_term['school_id'])
//                    ->where('term', $school_textbooks_for_a_term['term'])
//                    ->where('year', $school_textbooks_for_a_term['year'])
//                    ->update(array(
//                        'num_ghanaian_language_books' => $school_textbooks_for_a_term['num_ghanaian_language_books'],
//                        'num_english_books' => $school_textbooks_for_a_term['num_english_books'],
//                        'num_maths_books' => $school_textbooks_for_a_term['num_maths_books']));
//
//            } else {

                //            $fields = array(
//                'num_ghanaian_language_books' => ($school_textbooks_for_a_term->num_ghanaian_language_books,
//                'num_english_books' => $school_textbooks_for_a_term->num_english_books,
//                'num_maths_books' => $school_textbooks_for_a_term->num_maths_books,
//                'data_collector_id' => $data_collector_id,
//                'year' => $school_textbooks_for_a_term->year,
//                'term' => $school_textbooks_for_a_term->term,
//                'country_id' => 1,
//                'region_id' => $region_id,
//                'district_id' => $school_textbooks_for_a_term->district_id,
//                'circuit_id' => $circuit_id,
//                'school_id' =>$school_id,
//                'lat' => $lat,
//                'long' =>$long);
//
//            SchoolTotalsOnTermlySubmittedData::create($fields);


//                echo "No termly totals found for that term and year: Textbook";
//            echo '<pre>';
//            var_dump($school_textbooks_for_a_term);
//            echo '</pre>';
//            }
//
//        }


        }






}