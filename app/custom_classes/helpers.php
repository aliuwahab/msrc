<?php

class Helper {

	public static function generateDataCollectorsUniqueCode($table  = 'data_collectors', $column = 'identity_code'){
		$uniqueCode = str_random(5);
		$exist = DB::table($table)->where($column,$uniqueCode)->first();
		if($exist){
			generateDataCollectorsUniqueCode();
		}
		return $uniqueCode;
	}

	public static function generateSchoolUniqueCode($table  = 'schools', $column = 'code'){
		$uniqueCode = str_random(16);
		$exist = DB::table($table)->where($column,$uniqueCode)->first();
		if($exist){
			generateSchoolUniqueCode();
		}
		return $uniqueCode;
	}


	public static function generateRandomString($length = 5) {

		return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);

	}

	public static function generateRegionUniqueCode($table  = 'regions', $column = 'code'){
		$uniqueCode = str_random(5);
		$exist = DB::table($table)->where($column,$uniqueCode)->first();
		if($exist){
			generateRegionUniqueCode();
		}
		return $uniqueCode;
	}
	public static function generateDistrictUniqueCode($table  = 'districts', $column = 'code'){
		$uniqueCode = str_random(9);
		$exist = DB::table($table)->where($column,$uniqueCode)->first();
		if($exist){
			generateDistrictUniqueCode();
		}
		return $uniqueCode;
	}

	public static function generateCircuitUniqueCode($table  = 'circuits', $column = 'code'){
		$uniqueCode = str_random(14);
		$exist = DB::table($table)->where($column,$uniqueCode)->first();
		if($exist){
			generateCircuitUniqueCode();
		}
		return $uniqueCode;
	}


	//creating slugs from text
	public static  function slugify($text)
	{ 
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	  // trim
	  $text = trim($text, '-');

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // lowercase
	  $text = strtolower($text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  if (empty($text))
	  {
	    return 'n-a';
	  }

	  return $text;
	}


	public static function fileUpload($name,$file)
	{
		if (!empty($file)) {
			//$file = Input::file('file');
			$fileLocation = $file->move(public_path() . '/emailAttachement/', time().'-'.$name);
			return $fileLocation;
		}

		return "No file Uploaded";
		
	}


	public static function initials($sentence){
		$words = explode(" ", $sentence);
		$initials = "";

		foreach ($words as $word) {
			$initials.=substr($word,0,1);
		}
		return strtoupper($initials);
	}



	public static function apiRequestError($message = 'Error',$errorCode = 401)
	{
		return $requestFailed = Response::json(array(
                    'status'      =>  $errorCode,
                    'message'   =>  $message
        ), $errorCode);
	}

	public static function malformedQueryCheck($required_fields,$submitted_fields){
		$result = array_intersect_key($required_fields,$submitted_fields);
		$result = array_diff_key($required_fields,$result);
		$result = empty($result);
		return ($result) ? true : false;
	}

	//this generates the unique code for school SMS
	public static function generateSchoolSMSCode($districtName,$table  = 'schools', $column = 'sms_code', $district = NULL)
	{
		$districtNameShortForm = self::initials($districtName);
		$smsCode = $districtNameShortForm.rand ( 0 , 999 );

		// $exist = DB::table($table)->where($column,$smsCode)->first();
		// if($exist){
		// 	generateSchoolSMSCode();
		// }
		// return $smsCode;

		do {
			$smsCode = $districtNameShortForm.rand ( 0 , 999 );
			$exist = DB::table($table)->where($column,$smsCode)->first();
		} while ($exist);

		return $smsCode;
	}



	//Passing the model with which the query will be done on
	public function queringAModelBaseOnDynamiicNumberOfFields($model)
	{
		//getting the posted query parameters
		$inputs = Input::all();

		//return array_keys($inputs) of the posted data;
		$values = array_values($inputs);

		//declaring a query string to query with in the whereRaw method
		$queryString = '';

		//puting the key into the query parameters and appending the word 'and';
		foreach ($inputs as $key => $value) {
			$queryString .= $key.' = ? and ';
		}
		//removing the last and which will be appended
		$queryString = substr($queryString, 0, -4);
		
		return $model::whereRaw($queryString, $values)->get();
	}




	public static function csvToArray($csv_file_location, $delimiter=',')
	{
			if(!file_exists($csv_file_location) || !is_readable($csv_file_location))
				return FALSE;
 
			$header = NULL;
 
			$data = array();
			
			if (($handle = fopen($csv_file_location, 'r')) !== FALSE)
			{
				while (($row = fgetcsv($handle, 250, $delimiter)) !== FALSE)
				{
					if(!$header){

						$header = $row;
						
					}
					else{

						$data[] = array_combine($header, $row);
					}
				}
				fclose($handle);
				unlink($csv_file_location);
			}

			return $data;
	}





    public static function savingFilesTOAmazonS3($bucket,$fileOnLocalServer,$nameToSavefileWith)
    {
        $s3 = AWS::get('s3');
        $file = $s3->putObject(array(
            'Bucket'     => $bucket,
            'Key'        => $nameToSavefileWith,
            'SourceFile' => $fileOnLocalServer,
            'ACL'   	 => 'public-read',
            //'SourceFile' => '/the/path/to/the/file/you/are/uploading.ext',
        ));

        return $file;

    }
		




}