<?php

use Illuminate\Support\Facades\Log;

/**
		* This class contains all the functions for image processing
		*/
		class imageProcessingHelper
		{


				



				public static function savingFilesTOAmazonS3($imageFileOnLocalServer,$nameToSaveImageWith,$region)
				{
						$s3 = AWS::get('s3');
						$file = $s3->putObject(array(
						    'Bucket'     => 'm4dimages/'.$region->name,
						    'Key'        => $nameToSaveImageWith,
						    'SourceFile' => $imageFileOnLocalServer,
						    'ACL'   	 => 'public-read',
						    //'SourceFile' => '/the/path/to/the/file/you/are/uploading.ext',
						));

						return $file;

				}

				public static function savingFileDetailsIntoADatabaseTable($image_s3_url,$image_title,$image_description,$school_reference_code,$year,$country_id,$region_id,$district_id,$circuit_id,$school_id)
				{
					$fields = array(
								'image_url' => $image_s3_url,
								'image_title' => $image_title,
								'image_description' => $image_description,
								'school_reference_code' => $school_reference_code,
								'year' => $year,
								'country_id'=> $country_id,
								'region_id' => $region_id,
								'district_id' => $district_id,
								'circuit_id' => $circuit_id,
								'school_id' => $school_id );

					$authentication = ImageModel::validate($fields);

					if ($authentication != true) {
						return false;
					} else {
						$saveImageDetails = ImageModel::create($fields);

                        if ($saveImageDetails) {
                            // re-retrieve the instance to get all of the fields in the table.
                            $newlySavedImageDetails = ImageModel::orderBy('image_id', 'desc')->first()->toJson();

                            Log::info('Newly created Image: ', array('RequiredImageDetails' => $newlySavedImageDetails));

                            return Response::json(array("status"=>200,
                                "content" => "Image Saved Successfully",
                                "message"=>"Image Saved Successfully",
                                "image_details" => $newlySavedImageDetails));


                        }

                        return false;


					}
				}



				//this function deletes the file whose path is given
				public static function deleteFileFromAGivenPath($imageFileOnLocalServer)
				{
					if (file_exists($imageFileOnLocalServer)) {
						unlink($imageFileOnLocalServer);
					}else{
						return "File Does not exist";
					}
				}


				//this return images base on the query given
				public static function ReturnImagesBaseOnAQuery($queryString,$values)
				{
					return $images = DB::table('images')->whereRaw($queryString,$values)->get();
				}


            //this return images base on the query given
            public static function ReturnRequiredImagesBaseOnAQuery($queryString,$values)
            {
                return $images = RequiredImage::with('images')->whereRaw($queryString,$values)->get();
            }

			
		}



?>