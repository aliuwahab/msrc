<?php
use Illuminate\Support\Facades\DB;

/**
	* This class works on retrieving all data
	* Saving and retrieving school report card data
	*/
	class SchoolReportCardsDataReceivedAnalysyis
	{
		
		
		//this receives the posted data on aggregate weekly submission
		public static function ReceivesWeeklyAggregateSubmision($data)
		{
			
			Log::info('Aggregate weekly submission printed in the second function', array('context' => $data));


			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEnrolment($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsEnrolment($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassAttendance($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsClassAttendance($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPunctualityAndLessons($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassManagement($data);


			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberUnitsCoveredByTeacher($data);
					

		}





		//this receives the posted data on aggregate termly submission
		public static function ReceivesTermlyAggregateSubmision($data)
		{
			
			Log::info('Aggregate termly submissions printed in the second function', array('context' => $data));


			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSanitation($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSecurity($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEquipmentAndRecreation($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolStructure($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnFurniture($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnRecordAndPerformance($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnCommunityRelations($data);


			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnMeetings($data);


			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPupilsPerformance($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolSupportTypes($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberOfTextBooks($data);

			self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolGrantCapitationPayments($data);

			self::ReceiveDataOnGeneralSchoolSituation($data);

			
					

		}










		//this receives the posted data on school enrolment by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEnrolment($data)
		{
			
				Log::info('Normal school enrolment', array('context' => $data));
					
					$data_collector_id = $data->data_collector_id;
					$data_collector_type = $data->data_collector_type;
					$questions_category_reference_code = 'SCE';
					//$questions_category_reference_code = $data->question_reference_code;
					$school_reference_code = $data->school_code;
					$school_id = $data->school_id;
					$region_id = $data->region_id;
					$district_id = $data->district_id;
					$circuit_id = $data->circuit_id;
					$week_number = $data->week_number;
					$term = $data->term;
					$year = $data->year;
					$lat = $data->lat;
					$long = $data->long;
					$number_of_days_school_in_session = $data->school_session_days;
					$type = 'enrolment';
					//$term = 'first_term';


				$enrolmentInfoSave = false;

			//this variable is just to pass this condition
			$just_dummy_variable = false;

			if ($just_dummy_variable) {

//				Log::info('Normal Enrolment Data Already submitted by: ', array('context' => $data_collector_id));

				return Response::json(array("status"=>200,
											"content" => "rejected",
											"message"=>"Normal Enrolment Data Already submitted"));
				
			}else{

				//looping through to save enrolment for each class
				$totalNormalStudentsEnrolmentForASchool = 0;
				$totalNormalBoysStudentsEnrolmentForASchool = 0;
				$totalNormalGirlStudentsEnrolmentForASchool = 0;

					foreach ($data->normal_enrolment  as $enrolmentData) {

							$checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass = DB::table('school_enrolments')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)->where('level',$enrolmentData->level)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();

								if (count($checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass) < 1) {

									$totalNormalBoysStudentsEnrolmentForASchool += $enrolmentData->boys;
									$totalNormalGirlStudentsEnrolmentForASchool += $enrolmentData->girls;

									$fields = array(
										'school_reference_code' =>$school_reference_code,
									    'questions_category_reference_code' =>$questions_category_reference_code,
										'level' => $enrolmentData->level,
										'num_males' => $enrolmentData->boys,
										'num_females' => $enrolmentData->girls,
										'num_of_streams' => $enrolmentData->streams,
										'total_enrolment' => $enrolmentData->boys + $enrolmentData->girls,
										'year' => $year,
										'data_collector_type' => $data_collector_type,
										'data_collector_id' => $data_collector_id,
										'term' => $term,
										'week_number' => $week_number,
										'country_id' => 1,
										'region_id' => $region_id,
										'district_id' => $district_id,
										'circuit_id' => $circuit_id,
										'school_id' =>$school_id,
										'lat' => $lat,
										'long' =>$long,
										'school_in_session' => $number_of_days_school_in_session);

										$enrolmentInfoSave = SchoolEnrolment::create($fields);
										$enrolmentInfoSave = true;

								}else{

									echo "Data already submitted for this class level for this week, term, year and by this data collector";

								}
						}

						$totalNormalStudentsEnrolmentForASchool = $totalNormalBoysStudentsEnrolmentForASchool + $totalNormalGirlStudentsEnrolmentForASchool;

						if ($enrolmentInfoSave == true) {

							self::savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

							$current_number_of_teachers = TeacherRegistrationAndEditingAndDeleting::currentNumberOfTeachersInASchool($school_id);

							$recordNotAvailable = self::checkingToSeeWhetherARecordHasBeenCreatedForTotalWeeklyDataForAWeek($data_collector_type,$week_number,$school_id,$school_reference_code,$data_collector_id,$term,$year);
							if ($recordNotAvailable == true) {
								
								$fields = array(
												'school_reference_code' => $school_reference_code, 
												'normal_enrolment_total_boys' => $totalNormalBoysStudentsEnrolmentForASchool,
												'normal_enrolment_total_girls' => $totalNormalGirlStudentsEnrolmentForASchool,
												'normal_enrolment_total_students' => $totalNormalStudentsEnrolmentForASchool,
												'data_collector_id' => $data_collector_id,
												'data_collector_type' => $data_collector_type,
												'number_of_days_school_was_in_session' => $number_of_days_school_in_session,
												'year' => $year,
												'term' => $term,
												'week_number' => $week_number,
												'country_id' => 1,
												'region_id' => $region_id,
												'district_id' => $district_id,
												'circuit_id' => $circuit_id,
												'school_id' =>$school_id,
												'lat' => $lat,
												'long' =>$long,
												'current_number_of_teachers' => $current_number_of_teachers);

								SchoolTotalsOnWeeeklySubmittedData::create($fields);

							}else{

								$update = DB::table('weekly_totals')
	            					->where('school_reference_code', $school_reference_code)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('circuit_id', $circuit_id)
	            					->where('school_id', $school_id)
	            					->where('week_number', $week_number)
	            						->update(array(
	            							'normal_enrolment_total_boys' => $totalNormalBoysStudentsEnrolmentForASchool,
											'normal_enrolment_total_girls' => $totalNormalGirlStudentsEnrolmentForASchool,
											'normal_enrolment_total_students' => $totalNormalStudentsEnrolmentForASchool,
											'current_number_of_teachers' => $current_number_of_teachers));
							}

							//using push api available at www.pusher.com to send new data changes info to client side
							$data_type_submitted = 'schoolreportcard';

							$schoolIDAndDataType = array('school_id' => $school_id, 'type' => $data_type_submitted );


							//Here am calling the  function that will save normal students enrolment
							self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassAttendance($data);


							NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($schoolIDAndDataType['school_id'],$schoolIDAndDataType['type']);

							
							return Response::json(array("status"=> 200,
														"content" => "accepted",
														"message"=>"Data Submitted"));

						}else{

							return Response::json(array("status"=> 401,
														"content" => "rejected",
														"message"=>"Data could not be saved becuase some fields are not provided"));

						}



				}

			

		}





		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsEnrolment($data)
		{
					Log::info('Special school enrolment', array('context' => $data));
					$data_collector_id = $data->data_collector_id;
					$data_collector_type = $data->data_collector_type;
					$questions_category_reference_code = 'SSSE';
					//$questions_category_reference_code = $data->question_reference_code;
					$school_reference_code = $data->school_code;
					$school_id = $data->school_id;
					$region_id = $data->region_id;
					$district_id = $data->district_id;
					$circuit_id = $data->circuit_id;
					$week_number = $data->week_number;
					$term = $data->term;
					$year = $data->year;
					$lat = $data->lat;
					$long = $data->long;
					$number_of_days_school_in_session = $data->school_session_days;
					$type = 'specialenrolment';
					//$term = 'first_term';

				$specialEnrolmentInfoSave = false;

			//this variable is just to pass this condition
			$just_dummy_variable = false;

			if ($just_dummy_variable) {

					Log::info('Special Students Enrolment Data Already submitted by: ', array('context' => $data_collector_id));
					
				
					return Response::json(array("status"=>200,
												"content" => "rejected",
												"message"=>"Special Students Enrolment Data Already submitted"));

					
			}else{

				

				$totalSpecialStudentsEnrolmentForASchool = 0;
					$totalSpecialBoysStudentsEnrolmentForASchool = 0;
					$totalSpecialGirlStudentsEnrolmentForASchool = 0;
						//looping through to save enrolment for each class
					foreach ($data->special_enrolment  as $enrolmentData) {


						$checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass = DB::table('special_enrolment')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)->where('level',$enrolmentData->level)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();


														if (count($checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass) < 1) {
															
															
																$totalSpecialBoysStudentsEnrolmentForASchool += $enrolmentData->boys;
																$totalSpecialGirlStudentsEnrolmentForASchool += $enrolmentData->girls;

																$fields = array(
																	'school_reference_code' =>$school_reference_code,
																    'questions_category_reference_code' =>$questions_category_reference_code,
																	'level' => $enrolmentData->level,
																	'num_males' => $enrolmentData->boys,
																	'num_females' => $enrolmentData->girls,
																	'num_of_streams' => $enrolmentData->streams,
																	'total_enrolment' => $enrolmentData->boys + $enrolmentData->girls,
																	'year' => $year,
																	'data_collector_type' => $data_collector_type,
																	'data_collector_id' => $data_collector_id,
																	'term' => $term,
																	'week_number' => $week_number,
																	'country_id' => 1,
																	'region_id' => $region_id,
																	'district_id' => $district_id,
																	'circuit_id' => $circuit_id,
																	'school_id' =>$school_id,
																	'lat' => $lat,
																	'long' =>$long,
																	'school_in_session' => $number_of_days_school_in_session);

																	$specialEnrolmentInfoSave = SpecialStudentsEnrolment::create($fields);
																	$specialEnrolmentInfoSave = true;

														}else{

															echo "Data already submitted for special enrolment for this class at the set period";
														}


						}

						$totalSpecialStudentsEnrolmentForASchool = $totalSpecialBoysStudentsEnrolmentForASchool+$totalSpecialGirlStudentsEnrolmentForASchool; 
						if ($specialEnrolmentInfoSave == true) {

							self::savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
							
							$current_number_of_teachers = TeacherRegistrationAndEditingAndDeleting::currentNumberOfTeachersInASchool($school_id);

							$recordNotAvailable = self::checkingToSeeWhetherARecordHasBeenCreatedForTotalWeeklyDataForAWeek($data_collector_type,$week_number,$school_id,$school_reference_code,$data_collector_id,$term,$year);
							if ($recordNotAvailable == true) {
								
								$fields = array(
												'school_reference_code' => $school_reference_code, 
												'special_enrolment_total_boys' => $totalSpecialBoysStudentsEnrolmentForASchool,
												'special_enrolment_total_girls' => $totalSpecialGirlStudentsEnrolmentForASchool,
												'special_enrolment_total_students' => $totalSpecialStudentsEnrolmentForASchool,
												'data_collector_id' => $data_collector_id,
												'data_collector_type' => $data_collector_type,
												'year' => $year,
												'term' => $term,
												'week_number' => $week_number,
												'number_of_days_school_was_in_session' => $number_of_days_school_in_session,
												'country_id' => 1,
												'region_id' => $region_id,
												'district_id' => $district_id,
												'circuit_id' => $circuit_id,
												'school_id' =>$school_id,
												'lat' => $lat,
												'long' =>$long,
												'current_number_of_teachers' => $current_number_of_teachers);

								SchoolTotalsOnWeeeklySubmittedData::create($fields);


							}else{

								$update = DB::table('weekly_totals')
	            					->where('school_reference_code', $school_reference_code)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('circuit_id', $circuit_id)
	            					->where('school_id', $school_id)
	            					->where('week_number', $week_number)
	            						->update(array(
	            							'special_enrolment_total_boys' => $totalSpecialBoysStudentsEnrolmentForASchool,
											'special_enrolment_total_girls' => $totalSpecialGirlStudentsEnrolmentForASchool,
											'special_enrolment_total_students' => $totalSpecialStudentsEnrolmentForASchool,
											'current_number_of_teachers' => $current_number_of_teachers));
							}




							//calling the function that will save special attendance data
							self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsClassAttendance($data);


							
							return Response::json(array("status"=>200,
														"content" => "accepted",
														"message"=>"Data Submitted"));


						}else{

							return Response::json(array("status"=>200,
														"content" => "rejected",
														"message"=>"Data rejected becuase some fields are not provided"));
						}

			}

		}


		//this recieves data posted by android app school students class attendance
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassAttendance($data)
		{
					Log::info('Normal Class Attendance', array('context' => $data));
					$data_collector_id = $data->data_collector_id;
					$data_collector_type = $data->data_collector_type;
					//$questions_category_reference_code = $data->question_reference_code;
					$questions_category_reference_code = 'SCA';
					$school_reference_code = $data->school_code;
					$school_id = $data->school_id;
					$region_id = $data->region_id;
					$district_id = $data->district_id;
					$circuit_id = $data->circuit_id;
					$week_number = $data->week_number;
					$term = $data->term;
					$year = $data->year;
					$type = 'attendance';
					$lat = $data->lat;
					$long = $data->long;
					$school_session_days = $data->school_session_days;
					//$term = 'first_term';


					$checkingToSeeWhetherEnrolmentDataExistForThisWeek = DB::table('school_enrolments')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();


					$saveData = false;

//			((count($checkingToSeeWhetherAttendanceDataExistForThisWeek) == 1) || (count($checkingToSeeWhetherEnrolmentDataExistForThisWeek) != 1))
			//looping through to save attendnace for each class
			if (count($checkingToSeeWhetherEnrolmentDataExistForThisWeek) < 1) {

				//Log::info('Data Already submitted by: ', array('context' => $data_collector_id));
				//
				Log::info('Normal students enrolment not submitted by this data collector for the week: ', array('context' => $data_collector_id));
			

				return Response::json(array("status"=>200,
											"content" => "rejected",
											"message"=>"Normal students enrolment not submitted by this data collector for the week"));
			}else{

					$totalNormalStudentsAttendanceForASchool = 0;
					$totalNormalBoysStudentsAttendanceForASchool = 0;
					$totalNormalGirlStudentsAttendanceForASchool = 0;

						foreach ($data->normal_attendance as $attendanceData) {

							$checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass = DB::table('student_attendance')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)->where('level',$attendanceData->level)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();

										if (count($checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass) < 1) {
											
										$totalNormalBoysStudentsAttendanceForASchool += $attendanceData->boys;
										$totalNormalGirlStudentsAttendanceForASchool += $attendanceData->girls;
										
										$fields = array(
											'school_reference_code' =>$school_reference_code,
										    'questions_category_reference_code' =>$questions_category_reference_code,
											'level' => $attendanceData->level,
											'num_males' => $attendanceData->boys,
											'num_females' => $attendanceData->girls,
											'total' =>$attendanceData->boys + $attendanceData->girls,
											'year' => $year,
											'data_collector_type' => $data_collector_type,
											'data_collector_id' => $data_collector_id,
											'term' => $term,
											'week_number' => $week_number,
											'number_of_week_days' => $school_session_days,
											'country_id' => 1,
											'region_id' => $region_id,
											'district_id' => $district_id,
											'circuit_id' => $circuit_id,
											'school_id' =>$school_id,
											'lat' => $lat,
											'long' =>$long);

											$saveData = StudentAttendance::create($fields);
											$saveData = true;

										}else{

											echo "Data already submitted for this class for the time period selected";
										}
							}

							$totalNormalStudentsAttendanceForASchool = $totalNormalBoysStudentsAttendanceForASchool+$totalNormalGirlStudentsAttendanceForASchool;

							$current_number_of_teachers = TeacherRegistrationAndEditingAndDeleting::currentNumberOfTeachersInASchool($school_id);

							$recordNotAvailable = self::checkingToSeeWhetherARecordHasBeenCreatedForTotalWeeklyDataForAWeek($data_collector_type,$week_number,$school_id,$school_reference_code,$data_collector_id,$term,$year);
							if ($recordNotAvailable == true) {
								
								$fields = array(
												'school_reference_code' => $school_reference_code, 
												'normal_attendance_total_boys' => $totalNormalBoysStudentsAttendanceForASchool,
												'normal_attendance_total_girls' => $totalNormalGirlStudentsAttendanceForASchool,
												'normal_attendance_total_students' => $totalNormalStudentsAttendanceForASchool,
												'data_collector_id' => $data_collector_id,
												'data_collector_type' => $data_collector_type,
												'year' => $year,
												'term' => $term,
												'week_number' => $week_number,
									            'number_of_days_school_was_in_session' => $school_session_days,
												'country_id' => 1,
												'region_id' => $region_id,
												'district_id' => $district_id,
												'circuit_id' => $circuit_id,
												'school_id' =>$school_id,
												'lat' => $lat,
												'long' =>$long,
												'current_number_of_teachers' => $current_number_of_teachers);

								SchoolTotalsOnWeeeklySubmittedData::create($fields);


							}else{

								$update = DB::table('weekly_totals')
	            					->where('school_reference_code', $school_reference_code)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('circuit_id', $circuit_id)
	            					->where('school_id', $school_id)
	            					->where('week_number', $week_number)
	            						->update(array(
	            							'normal_attendance_total_boys' => $totalNormalBoysStudentsAttendanceForASchool,
											'normal_attendance_total_girls' => $totalNormalGirlStudentsAttendanceForASchool,
											'normal_attendance_total_students' => $totalNormalStudentsAttendanceForASchool,
											'current_number_of_teachers' => $current_number_of_teachers));
							}
								
								if ($saveData == true) {

									$fields = array(
										'data_collector_id' => $data_collector_id, 
										'data_type_submitted' => 'schoolreportcard',
										'country_id' => 1,
										'region_id' => $region_id,
										'district_id' => $district_id,
										'circuit_id' => $circuit_id,
										'school_id' => $school_id);

								RecentDataSubmitted::create($fields);

								self::savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

								return Response::json(array("status"=>200,
															"content" => "accepted",
															"message"=>"Normal Enrolment Data Already submitted"));
								
							}else{


								return Response::json(array("status"=>401,
															"content" => "rejected",
															"message"=>"Data could not be saved"));


							}

			}
		}

		//saving the special enrolment data
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSpecialStudentsClassAttendance($data)
		{
					Log::info('Special Class Attendance', array('context' => $data));
					
					$data_collector_id = $data->data_collector_id;
					$data_collector_type = $data->data_collector_type;
					$questions_category_reference_code = 'SSCA';
					//$questions_category_reference_code = $data->question_reference_code;
					
					$school_reference_code = $data->school_code;
					$school_id = $data->school_id;
					$region_id = $data->region_id;
					$district_id = $data->district_id;
					$circuit_id = $data->circuit_id;
					$week_number = $data->week_number;
					$term = $data->term;
					$year = $data->year;
					$type = 'specialattendance';
					$lat = $data->lat;
					$long = $data->long;
					$school_session_days = $data->school_session_days;
					//$term = 'first_term';
					$checkingToSeeWhetherDataHasAlreadyBeenSubmitted = DB::table('schoolreportcard_weekyl_submission_status')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)->where('type',$type)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();

					$checkingToSeeWhetherSpecialAttendanceDataExistForThisWeek = DB::table('special_attendance')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();

					$checkingToSeeWhetherSpecialEnrolmentDataExistForThisWeek = DB::table('special_enrolment')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();

					$specialStudents = false;
			//looping through to save attendnace for each class
			if (count($checkingToSeeWhetherSpecialEnrolmentDataExistForThisWeek) < 1) {

				Log::info('No special enrolment data by this data collector: ', array('context' => $data_collector_id));

				return Response::json(array("status"=>200,
											"content" => "rejected",
											"message"=>"No special enrolment data submitted for the week"));


			}else{

						$totalSpecialStudentsAttendanceForASchool = 0;
						$totalSpecialBoysStudentsAttendanceForASchool = 0;
						$totalSpecialGirlStudentsAttendanceForASchool = 0;

						foreach ($data->special_attendance as $attendanceData) {


							$checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass = DB::table('special_attendance')
														->where('school_id',$school_id)
														->where('data_collector_id',$data_collector_id)->where('level',$attendanceData->level)
														->where('term',$term)->where('week_number',$week_number)
														->where('year',$year)->first();

														if (count($checkingToSeeWhetherDataHasAlreadyBeenSubmittedForAClass) < 1) {
															
																	$totalSpecialBoysStudentsAttendanceForASchool += $attendanceData->boys;
																	$totalSpecialGirlStudentsAttendanceForASchool += $attendanceData->girls;
																	
																	$fields = array(
																		'school_reference_code' =>$school_reference_code,
																	    'questions_category_reference_code' =>$questions_category_reference_code,
																		'level' => $attendanceData->level,
																		'num_males' => $attendanceData->boys,
																		'num_females' => $attendanceData->girls,
																		'total' =>$attendanceData->boys + $attendanceData->girls,
																		'year' => $year,
																		'data_collector_type' => $data_collector_type,
																		'data_collector_id' => $data_collector_id,
																		'term' => $term,
																		'week_number' => $week_number,
																		'number_of_week_days' => $school_session_days,
																		'country_id' => 1,
																		'region_id' => $region_id,
																		'district_id' => $district_id,
																		'circuit_id' => $circuit_id,
																		'school_id' =>$school_id,
																		'lat' => $lat,
																		'long' =>$long);

																		$specialStudents = SpecialStudentsAttendance::create($fields);

																		$specialStudents = true;

														}else{

															echo "Data has already been Submitted under this class for the selected periods";
															
														}
							}

							$totalSpecialStudentsAttendanceForASchool = $totalSpecialBoysStudentsAttendanceForASchool+$totalSpecialGirlStudentsAttendanceForASchool;

							$current_number_of_teachers = TeacherRegistrationAndEditingAndDeleting::currentNumberOfTeachersInASchool($school_id);
							
							$recordNotAvailable = self::checkingToSeeWhetherARecordHasBeenCreatedForTotalWeeklyDataForAWeek($data_collector_type,$week_number,$school_id,$school_reference_code,$data_collector_id,$term,$year);
							if ($recordNotAvailable == true) {
								
								$fields = array(
												'school_reference_code' => $school_reference_code, 
												'special_attendance_total_boys' => $totalSpecialBoysStudentsAttendanceForASchool,
												'special_attendance_total_girls' => $totalSpecialGirlStudentsAttendanceForASchool,
												'special_attendance_total_students' => $totalSpecialStudentsAttendanceForASchool,
												'data_collector_id' => $data_collector_id,
												'data_collector_type' => $data_collector_type,
												'year' => $year,
												'term' => $term,
												'week_number' => $week_number,
												'country_id' => 1,
												'region_id' => $region_id,
												'district_id' => $district_id,
												'number_of_days_school_was_in_session' => $school_session_days,
												'circuit_id' => $circuit_id,
												'school_id' =>$school_id,
												'lat' => $lat,
												'long' =>$long,
												'current_number_of_teachers' => $current_number_of_teachers);

								SchoolTotalsOnWeeeklySubmittedData::create($fields);

								
							}else{

								$update = DB::table('weekly_totals')
	            					->where('school_reference_code', $school_reference_code)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('circuit_id', $circuit_id)
	            					->where('school_id', $school_id)
	            					->where('week_number', $week_number)
	            						->update(array(
	            							'special_attendance_total_boys' => $totalSpecialBoysStudentsAttendanceForASchool,
											'special_attendance_total_girls' => $totalSpecialGirlStudentsAttendanceForASchool,
											'special_attendance_total_students' => $totalSpecialStudentsAttendanceForASchool,
											'current_number_of_teachers' => $current_number_of_teachers));
							}

								
								if ($specialStudents == true) {

									$fields = array(
										'data_collector_id' => $data_collector_id, 
										'data_type_submitted' => 'schoolreportcard',
										'country_id' => 1,
										'region_id' => $region_id,
										'district_id' => $district_id,
										'circuit_id' => $circuit_id,
										'school_id' => $school_id);

									RecentDataSubmitted::create($fields);

								self::savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

								return Response::json(array("status"=>200,
															"content" => "accepted",
															"message"=>"Data submitted"));
					

							}else{

								return Response::json(array("status"=>200,
															"content" => "rejected",
															"message"=>"Data not submitted"));

							}
			

				}

		}


		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSanitation($data)
		{
			Log::info('Sanitation data on school problems', array('context' => $data));
			
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'SP';
			
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->Comments;
			$type = 'sanitation';
			$lat = $data->lat;
			$long = $data->long;
			//$comment = "This is a test comment";
			$sanitationAnswers = $data->sanitation;

			Log::info('Just to see school class data', array('context' => $sanitationAnswers));

			
			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				
				$fields = array(
						'school_reference_code' => $school_code,
						'questions_category_reference_code' =>$questions_category_reference_code,
						'toilet' => $toiletStatus = $sanitationAnswers[0]->availability,
						'urinal' => $urinalStatus = $sanitationAnswers[1]->availability,
						'water' => $waterStatus = $sanitationAnswers[2]->availability,
						'dust_bins' =>$dustbinStatus = $sanitationAnswers[3]->availability,
						'veronica_buckets' => $veronicaBucketStatus = $sanitationAnswers[4]->availability,
						'comment' => $comment,
						'term' => $term,
						'year' => $year,
						'data_collector_type' => $data_collector_type,
						'data_collector_id' => $data_collector_id,
						'country_id' => 1,
						'region_id' => $region_id,
						'district_id' => $district_id,
						'circuit_id' => $circuit_id,
						'school_id' => $school_id,
						'lat' => $lat,
						'long' =>$long
						);

				if (Sanitation::create($fields)) {

					//saving into recent submissions
					self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
					//saving into school report card submission status
					self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

					//using push api available at www.pusher.com to send new data changes info to client side
					$data_type_submitted = 'schoolreportcard';
					$schoolIDAndDataType = array('school_id' => $school_id, 'type' => $data_type_submitted );
					
					NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($schoolIDAndDataType['school_id'],$schoolIDAndDataType['type']);

					//response returning success
					return Response::json(array("status"=>200,
											"content" => "accepted",
											"message"=>"Data submitted"));
				}else{
					
					return Response::json(array("status"=>200,
											"content" => "rejected",
											"message"=>"Data submission failed"));
				}
			}

		}

		//saving data on security submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSecurity($data)
		{
			Log::info('Security data on school problems', array('context' => $data));
			
			$type = 'security';
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'SP';
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comments;
			$securityAnswers = $data->security;
			$lat = $data->lat;
			$long = $data->long;

			Log::info('Just to see security data', array('context' => $securityAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);


			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {


					$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'walled' => $wallStatus = $securityAnswers[0]->availability,
							'gated' => $gateStatus = $securityAnswers[1]->availability,
							'lights' =>$lightsbinStatus = $securityAnswers[2]->availability,
							'security_man' => $securityManStatus = $securityAnswers[3]->availability,
							'comment' => $comment,
							'term' => $term,
							'year' => $year,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
							);

					if (Security::create($fields)) {

						//saving into recent submissions
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						//saving into school report card submission status
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

				
						return Response::json(array("status"=>200,
													"content" => "accepted",
													"message"=>"Data submitted"));
					}else{
						
						return Response::json(array("status"=>401,
													"content" => "rejected",
													"message"=>"Data couldn't be submitted"));
					}

			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already submitted"));
			}

		}



		//saving data on equipment and recreation submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnEquipmentAndRecreation($data)
		{
			
			Log::info('Equipment and Recreation data on school problems', array('context' => $data));

			$type = 'recreation';
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'SP';
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comments;
			$lat = $data->lat;
			$long = $data->long;

			$equipmentAndRecreationAnswers = $data->recreation;
			//Log::info('Just to see security data', array('context' => $equipmentAndRecreationAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'football' => $footballStatus = $equipmentAndRecreationAnswers[0]->availability,
							'volleyball' => $volleyballStatus = $equipmentAndRecreationAnswers[1]->availability,
							'netball' =>$netballStatus = $equipmentAndRecreationAnswers[2]->availability,
							'playing_field' => $playingFieldStatus = $equipmentAndRecreationAnswers[3]->availability,
							'sports_wear' => $sportsWearStatus = $equipmentAndRecreationAnswers[4]->availability,
							'seesaw' => $seesawStatus = $equipmentAndRecreationAnswers[5]->availability,
							'merry_go_round' => $merryGoRoundStatus = $equipmentAndRecreationAnswers[6]->availability,
							'first_aid_box' => $firstAidBoxStatus = $equipmentAndRecreationAnswers[8]->availability,
							'comment' => $comment,
							'term' => $term,
							'year' => $year,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
							);

					if (EquipmentRecreational::create($fields)) {
						//saving into recent submissions
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						//saving into school report card submission status
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
						
						return Response::json(array("status"=>200,
												"content" => "accepted",
												"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
												"content" => "rejected",
												"message"=>"Failed submission"));
					}
			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already submitted"));

			}

		}

		
		//saving data on equipment and recreation submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolStructure($data)
		{
			Log::info('School Structure data on school problems', array('context' => $data));
			
			$type = 'structure';
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'SP';
			
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comments;
			$lat = $data->lat;
			$long = $data->long;

			$schoolStructureAnswers = $data->school_structure;
			Log::info('Just to see school structure data', array('context' => $schoolStructureAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				# code...
					$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'walls' => $wallsStatus = $schoolStructureAnswers[0]->availability,
							'doors' => $doorStatus = $schoolStructureAnswers[1]->availability,
							'windows' =>$windowsStatus = $schoolStructureAnswers[2]->availability,
							'floors' => $floorsStatus = $schoolStructureAnswers[4]->availability,
							'blackboard' => $blackboardStatus = $schoolStructureAnswers[3]->availability,
							'comment' => $comment,
							'term' => $term,
							'year' => $year,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long,
							'illumination' =>$schoolStructureAnswers[5]->availability
							);

					if (SchoolStructure::create($fields)) {
						
						//saving into recent submissions
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						//saving into school report card submission status
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
						
						return Response::json(array("status"=>200,
												"content" => "accepted",
												"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
												"content" => "rejected",
												"message"=>"Failed submission"));
					}
			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already Submitted"));
			}

		}



		//saving data on class equipment and furniture submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnFurniture($data)
		{
			
			Log::info('Furniture data on school problems', array('context' => $data));
			
			$type = 'furniture';
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'SP';
			
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			//$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comments;
			$lat = $data->lat;
			$long = $data->long;

			$schoolFurnitureAnswers = $data->furniture;
			Log::info('Just to see school furniture data', array('context' => $schoolFurnitureAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
					$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'pupils_furniture' => $pupilsFunitureStatus = $schoolFurnitureAnswers[0]->availability,
							'teacher_tables' => $teacherTablesStatus = $schoolFurnitureAnswers[1]->availability,
							'teacher_chairs' =>$teacherChairStatus = $schoolFurnitureAnswers[2]->availability,
							'classrooms_cupboard' => $classCupbaordsStatus = $schoolFurnitureAnswers[3]->availability,
							'comment' => $comment,
							'term' => $term,
							'year' => $year,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
							);

					if (Furniture::create($fields)) {
						
						//saving into recent submissions
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						//saving into school report card submission status
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
						
						return Response::json(array("status"=>200,
												"content" => "accepted",
												"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
												"content" => "rejected",
												"message"=>"Failed submission"));
					}
			}else{
				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already Submitted"));
			}

		}
		

		//saving data on class records and performance submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnRecordAndPerformance($data)
		{
			Log::info('Records and Performance data on school problems', array('context' => $data));
			
			$type = 'performance';
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'SP';
			
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			//$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comments;
			$lat = $data->lat;
			$long = $data->long;

			//$comment = "This is a test comment";
			$schoolRecordAndPerformanceAnswers = $data->school_performance;
			Log::info('Just to see school school record and performance data', array('context' => $schoolRecordAndPerformanceAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			
			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				
				$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'admission_register' => $admissionRegisterStatus = $schoolRecordAndPerformanceAnswers[0]->availability,
							'class_registers' => $classRegisterStatus = $schoolRecordAndPerformanceAnswers[1]->availability,
							'teacher_attendance_register' =>$teacherAttendanceStatus = $schoolRecordAndPerformanceAnswers[2]->availability,
							'log' => $classLogStatus = $schoolRecordAndPerformanceAnswers[3]->availability,
							'visitors' => $visitorsLogStatus = $schoolRecordAndPerformanceAnswers[4]->availability,
							'sba' => $sbaStatus = $schoolRecordAndPerformanceAnswers[5]->availability,
							'movement' => $classMovementStatus = $schoolRecordAndPerformanceAnswers[6]->availability,
							'spip' => $spipStatus = $schoolRecordAndPerformanceAnswers[7]->availability,
							'inventory_books' => $inventoryStatus = $schoolRecordAndPerformanceAnswers[8]->availability,
							'cummulative_records_books' => $cummulativeStatus = $schoolRecordAndPerformanceAnswers[9]->availability,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'comment' => $comment,
							'term' => $term,
							'year' => $year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long,
							'continous_assessment' => $schoolRecordAndPerformanceAnswers[10]->availability
							);
			// 		$table->enum('inventory_books',array('yes','no'));
			// $table->enum('cummulative_records_books',array('yes','no'));

					if (SchoolRecordsAndPerformance::create($fields)) {

						//saving into recent submissions
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						//saving into school report card submission status
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
						
						
						return Response::json(array("status"=>200,
												"content" => "accepted",
												"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
												"content" => "rejected",
												"message"=>"Failed submission"));
					}
			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already Submitted"));
			}

		}


		//saving data on school and community relations submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnCommunityRelations($data)
		{
			Log::info('Community Relations data on school problems', array('context' => $data));
			
			$type = 'relations';
			
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'LCSR';
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			//$term = 'first_term';
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comments;
			$lat = $data->lat;
			$long = $data->long;

			$schoolCommunityRelationsAnswers = $data->Community_relation;
			Log::info('Just to see data on community relations', array('context' => $schoolCommunityRelationsAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				
				$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'pta_involvement' => $ptaStatus = $schoolCommunityRelationsAnswers[0]->parent_involvement,
							'parents_notified_of_student_progress' => $notificationStatus = $schoolCommunityRelationsAnswers[1]->parent_notification,
							'community_sensitization_for_school_attendance' =>$sensitisationStatus = $schoolCommunityRelationsAnswers[2]->sensitise,
							'school_management_committees' => $managementStatus = $schoolCommunityRelationsAnswers[3]->SMC,
							'comment' => $comment,
							'term' => $term,
							'year' => $year,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
							);

					if (SchoolCommunityRelationship::create($fields)) {
						
						//saving into recent submissions
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						//saving into school report card submission status
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
								
						return Response::json(array("status"=>200,
												"content" => "accepted",
												"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
												"content" => "rejected",
												"message"=>"Failed submission"));
					}
			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already Submitted"));
			}

		}
		

		//saving data on teacher punctuality and lesson submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPunctualityAndLessons($data)
		{
			Log::info('Punctuality and Lesson Plans data on school problems', array('context' => $data));
				
				$data_collector_id = $data->data_collector_id;
				$data_collector_type = $data->data_collector_type;
//				$questions_category_reference_code = $data->question_reference_code;
				$questions_category_reference_code = 'PLP';
				
				$savingPunctualityData = null;

				$school_code = $data->school_code;
				$school_id = $data->school_id;
				$region_id = $data->region_id;
				$district_id = $data->district_id;
				$circuit_id = $data->circuit_id;
				$week_number = $data->week;
				$number_days_school_in_session = $data->school_session_days;
				$term = $data->term;
				$year = $data->year;
				$lat = $data->lat;
				$long = $data->long;
				$type = 'punctuality';

				$comment = 'no comments';

			//$comment = "This is a test comment";

			$teacherPunctualityAnswers = $data->teacher_punctuality;
			Log::info('Just to see Punctuality and lesson plan', array('context' => $teacherPunctualityAnswers));

					$totalNumberOfSchoolDays = $number_days_school_in_session;

					foreach ($teacherPunctualityAnswers as $Teacher) {


								$teacher_id = $Teacher->teacher_id;

								//checking to see whether a submission has already been made for a teacher
								$checkingToSeeWhetherDataHasAlreadyBeenSubmitted = DB::table('punctuality_and_lessons')
														->where('teacher_id',$teacher_id)->where('week_number',$week_number)
														->where('data_collector_id',$data_collector_id)->where('data_collector_type',$data_collector_type)
														->where('term',$term)->where('year',$year)->first();
							

								if (count($checkingToSeeWhetherDataHasAlreadyBeenSubmitted) < 1) {


									$fields = array(
									'teacher_id' => $teacher_id,
									'school_reference_code' => $school_code,
									'questions_category_reference_code' =>$questions_category_reference_code,
									'days_in_session' =>$number_days_school_in_session,
									'days_present' => $Teacher->teacherPresentDays,
									'days_punctual' => $Teacher->teacherPuntualDays,
									'days_absent_with_permission' => $Teacher->teacherAbsent,
									'days_absent_without_permission' => $Teacher->teacherAbsentNoPermission,
									'lesson_plans' => $Teacher->less_plan,
									'comment' => $comment,
									'term' => $term,
									'week_number' => $week_number,
									'year' => $year,
									'data_collector_type' => $data_collector_type,
									'data_collector_id' => $data_collector_id,
									'country_id' => 1,
									'region_id' => $region_id,
									'district_id' => $district_id,
									'circuit_id' => $circuit_id,
									'school_id' => $school_id,
									'lat' => $lat,
									'long' =>$long,
									'num_of_exercises_given' => $Teacher->num_of_exercises_given,
									'num_of_exercises_marked' =>$Teacher->num_of_exercises_marked
									);

										if ($savingPunctualityData= PuntualityAndLessons::create($fields)) {
											//to help check whether it has entered
											$savingPunctualityData = "just a dummy value";
										}
								}



					}

					// self::ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberUnitsCoveredByTeacher($data);
					
					$school_reference_code = $school_code;

					if ($savingPunctualityData != null) {
						
						$averageTeachersAttendanceForASchool = self::calculatingThePercentageOfTeacherAttendanceInAWeek($year,$term, $week_number,$totalNumberOfSchoolDays,$school_id,$data_collector_type,$data_collector_id);

						$recordNotAvailable = self::checkingToSeeWhetherARecordHasBeenCreatedForTotalWeeklyDataForAWeek($data_collector_type,$week_number,$school_id,$school_reference_code,$data_collector_id,$term,$year);
							
							if ($recordNotAvailable == true) {

								$fields = array(
												'school_reference_code' => $school_reference_code, 
												'average_teacher_attendance' => $averageTeachersAttendanceForASchool,
												'data_collector_id' => $data_collector_id,
												'data_collector_type' => $data_collector_type,
												'year' => $year,
												'term' => $term,
												'week_number' => $week_number,
												'country_id' => 1,
												'number_of_days_school_was_in_session' => $number_days_school_in_session,
												'region_id' => $region_id,
												'district_id' => $district_id,
												'circuit_id' => $circuit_id,
												'school_id' =>$school_id,
												'lat' => $lat,
												'long' =>$long);

								SchoolTotalsOnWeeeklySubmittedData::create($fields);

							}else{

								$update = DB::table('weekly_totals')
	            					->where('school_reference_code', $school_reference_code)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('circuit_id', $circuit_id)
	            					->where('school_id', $school_id)
	            					->where('year', $year)
	            					->where('term', $term)
	            					->where('week_number', $week_number)
	            						->update(array(
	            							'average_teacher_attendance' => $averageTeachersAttendanceForASchool));


							}

						if ($data_collector_type == 'head_teacher') {

						 	$school = School::find($school_id);
//							$school->current_teacher_attendance_by_headteacher = self::calculatingThePercentageOfTeacherAttendanceInAWeek($year,$term, $week_number,$totalNumberOfSchoolDays,$school_id);
							$school->current_teacher_attendance_by_headteacher = $averageTeachersAttendanceForASchool;
							$school->save();

						 }else{
						 	//saving the current teachers attendance by Circuit Supervisor
						 	$school = School::find($school_id);
//							$school->current_teacher_attendance_by_circuit_supervisor = self::calculatingThePercentageOfTeacherAttendanceInAWeek($year,$term, $week_number,$totalNumberOfSchoolDays,$school_id);
							$school->current_teacher_attendance_by_circuit_supervisor = $averageTeachersAttendanceForASchool;
							$school->save();
						 } 

						self::savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
						
						return Response::json(array("status"=>200,
													"content" => "accepted",
													"message"=>"Data Submitted"));
					}



		}


		



		public static function calculatingThePercentageOfTeacherAttendanceInAWeek($year,$term, $week_number,$totalNumberOfSchoolDays,$school_id,$data_collector_type,$data_collector_id)
		{
			
			$totalNumberOfTeachersInTheSchool  = DB::table('teachers')->where('school_id',$school_id)->count();

			$total_number_of_days_present_by_all_teachers_in_the_week = DB::table('punctuality_and_lessons')
				->where('school_id',$school_id)
				->where('week_number',$week_number)
				->where('term',$term)
				->where('year',$year)
				->where('data_collector_type',$data_collector_type)
				->where('data_collector_id',$data_collector_id)
				->sum('days_present');

			$total_expected_attendance_by_all_teachers_in_the_week = $totalNumberOfSchoolDays * $totalNumberOfTeachersInTheSchool;

			$percentage_teacher_attendance = ($total_number_of_days_present_by_all_teachers_in_the_week / $total_expected_attendance_by_all_teachers_in_the_week) * 100;

			if ($percentage_teacher_attendance > 100) {

				return $percentage_teacher_attendance = 100 - 1.97;

			}else{

				return $percentage_teacher_attendance;

			}

		}



		//saving data on teacher class room management submitted by android app
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnClassManagement($data)
		{
				Log::info('Class Management data', array('context' => $data));
				
				$type = 'class_management';
				$data_collector_id = $data->data_collector_id;
				$data_collector_type = $data->data_collector_type;
				//$questions_category_reference_code = $data->question_reference_code;
				$questions_category_reference_code = 'TCM';
				
				$school_code = $data->school_code;
				$school_id = $data->school_id;
				$region_id = $data->region_id;
				$district_id = $data->district_id;
				$circuit_id = $data->circuit_id;
				$week_number = $data->week;
				$term = $data->term;
				$year = $data->year;
				$lat = $data->lat;
				$long = $data->long;
				$comment = 'No comment';

				$saveTeacherClassManagementStatus = null;

			$classRoomManagementAnswers = $data->Class_management;
			Log::info('Just to see teacher class management data', array('context' => $classRoomManagementAnswers));

				foreach ($classRoomManagementAnswers as $classManagement) {
					
								$teacher_id = $classManagement->teacher_id;
								//checking to see whether a submission has already been made for a teacher which acconuts
								//for why am not checking base on the weekly submissions
								$checkingToSeeWhetherDataHasAlreadyBeenSubmitted = DB::table('teachers_class_management')
														->where('teacher_id',$teacher_id)->where('week_number',$week_number)
														->where('data_collector_id',$data_collector_id)->where('data_collector_type',$data_collector_type)
														->where('term',$term)->where('year',$year)->first();

					if (count($checkingToSeeWhetherDataHasAlreadyBeenSubmitted) < 1) {
						$fields = array(
							'teacher_id' => $teacher_id,
							'school_reference_code' => $school_code,
							'questions_category_reference_code' => $questions_category_reference_code,
							'class_control' => $classManagement->class_control,
							'children_behaviour' =>$classManagement->child_behaviour,
							'children_participation' =>$classManagement->child_participation,
							'teacher_discipline' =>$classManagement->teacher_discipline,
							'comment' => $comment,
							'term' => $term,
							'week_number' => $week_number,
							'year' => $year,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
							);
						//saving teachers class management info
						$saveTeacherClassManagementStatus = TeachersClassManagement::create($fields);
					}
			}

				if ($saveTeacherClassManagementStatus != null) {

							self::savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
							
							return Response::json(array("status"=>200,
														"content" => "accepted",
														"message"=>"Data Submitted"));
				}

		}




		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberUnitsCoveredByTeacher($data)
		{
				Log::info('Number of units covered', array('context' => $data));
				$type = 'units_covered';
				$data_collector_id = $data->data_collector_id;
				$data_collector_type = $data->data_collector_type;
				//$questions_category_reference_code = $data->question_reference_code;
				$questions_category_reference_code = 'NUCP';
				$school_code = $data->school_code;
				$school_id = $data->school_id;
				$region_id = $data->region_id;
				$district_id = $data->district_id;
				$circuit_id = $data->circuit_id;
				$week_number = $data->week;
				$term = $data->term;
				$year = $data->year;
				$lat = $data->lat;
				$long = $data->long;
				$comment = 'No comment';

				$TeachersUnitsCovered = $data->units_covered;

				Log::info('Just to see only teacher units covered', array('context' => $TeachersUnitsCovered));

				$saveTeacherUnitsCovers = false;

				foreach ($TeachersUnitsCovered as $TeacherUnitsCovered) {
						$teacher_id = $TeacherUnitsCovered->teacher_id;


//					$teacher_id = $TeacherUnitsCovered->teacher_id;
					//checking to see whether a submission has already been made for a teacher which acconuts
					//for why am not checking base on the weekly submissions

					$checkingToSeeWhetherDataHasAlreadyBeenSubmitted = DB::table('teacher_units_covered')
						->where('teacher_id',$teacher_id)->where('week_number',$week_number)
						->where('data_collector_id',$data_collector_id)->where('data_collector_type',$data_collector_type)
						->where('term',$term)->where('year',$year)->first();

					if (count($checkingToSeeWhetherDataHasAlreadyBeenSubmitted) < 1) {

						$fields = array(
							'teacher_id' => $teacher_id,
							'school_reference_code' => $school_code,
							'questions_category_reference_code' => $questions_category_reference_code,
							'ghanaian_language_units_covered' => $TeacherUnitsCovered->ghanaian_language,
							'english_language_units_covered' =>$TeacherUnitsCovered->english,
							'mathematics_units_covered' =>$TeacherUnitsCovered->maths,
							'comment' => $comment,
							'term' => $term,
							'week_number' => $week_number,
							'year' => $year,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
						);

						//saving teachers class management info
						$saveTeacherUnitsCovers = TeacherUnitsCovered::create($fields);

					}

			}


			if ($saveTeacherUnitsCovers == false) {

				Log::info('Teacher units covered submission failed', array('context' => $TeachersUnitsCovered));


						return Response::json(array("status"=>200,
													"content" => "rejected",
													"message"=>"No teacher units covered info was posted fully, hence unsuccesful"));
			}else{

				Log::info('Teacher units covered submitted succesfully', array('context' => $TeachersUnitsCovered));


						return Response::json(array("status"=>200,
													"content" => "accepted",
													"message"=>"Data Submitted"));
			}


		}



		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnMeetings($data)
		{
			
			Log::info('Meeetings data on school problems', array('context' => $data));
			
			$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = "SM";
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comment;
			$lat = $data->lat;
			$long = $data->long;
			$type = 'meeting';
			//$comment = "This is a test comment";
			$number_circuit_supervisor_visits = $data->csVisit;
			$inset = $data->inset;
			$long = $data->long;

			$schoolMeetingAnswers = $data->management_meetings;
			Log::info('Just to see school meetings data', array('context' => $schoolMeetingAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				
					$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'number_of_circuit_supervisor_visit' => $number_circuit_supervisor_visits,
							'number_of_inset' => $inset,
							'number_of_staff_meeting' => $schoolMeetingAnswers[0]->staff_number_of_times,
							'num_males_present_at_staff_meeting' => $schoolMeetingAnswers[0]->staff_male,
							'num_females_present_at_staff_meeting' => $schoolMeetingAnswers[0]->staff_female,
							'number_of_spam_meeting' => $schoolMeetingAnswers[3]->spam_number_of_times,
							'num_males_present_at_spam_meeting' => $schoolMeetingAnswers[3]->spam_male,
							'num_females_present_at_spam_meeting' => $schoolMeetingAnswers[3]->spam_female,
							'number_of_pta_meeting' => $schoolMeetingAnswers[1]->PTA_number_of_times,
							'num_males_present_at_pta_meeting' =>$schoolMeetingAnswers[1]->PTA_male,
							'num_females_present_at_pta_meeting' =>$schoolMeetingAnswers[1]->PTA_female,
							'number_of_smc_meeting' => $schoolMeetingAnswers[2]->SMC_number_of_times,
							'num_males_present_at_smc_meeting' => $schoolMeetingAnswers[2]->SMC_male,
							'num_females_present_at_smc_meeting' => $schoolMeetingAnswers[2]->SMC_female,
							'comment' => $comment,
							'term' => $term,
							'week_number' =>$week_number,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'year' => $year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
							);
					//saving the meeting data
					if (SchoolsMeeting::create($fields)) {	
						
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

						return Response::json(array("status"=>200,
													"content" => "accepted",
													"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
													"content" => "rejected",
													"message"=>"Failed submission"));
					}
			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already Submitted"));
			}
		}


		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnPupilsPerformance($data)
		{
			Log::info('Pupils Performance data on school problems', array('context' => $data));
			
			$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = "PEPA";
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$lat = $data->lat;
			$long = $data->long;
			$type = 'pupils_performance';
			//$comment = "This is a test comment";

			$schoolPupilsPerformanceAnswers = $data->performance;
			Log::info('Pupils performance data only', array('context' => $schoolPupilsPerformanceAnswers));

			// $checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			// if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {

					$saving_pupils_performance = false;
				


					

					foreach ($schoolPupilsPerformanceAnswers as $AClassPerformance) {

						$checking_to_see_data_is_submitted = DB::table('pupils_exams_performance')
																	->where('level', $AClassPerformance->class)
																	->where('data_collector_type',$data_collector_type)
																	->where('term',$term)
																	->where('year',$year)
																	->where('school_id',$school_id)
																	->where('data_collector_id',$data_collector_id)->first();


						if (count($checking_to_see_data_is_submitted) < 1) {

								
								$fields = array(
								'school_reference_code' => $school_code,
								'questions_category_reference_code' =>$questions_category_reference_code,
								'level' => $AClassPerformance->class,
								'average_ghanaian_language_score' => $AClassPerformance->ghanaian_language_average,
								'num_pupil_who_score_above_average_in_ghanaian_language' => $AClassPerformance->ghanaian_language_above_average,
								'average_english_score' =>$AClassPerformance->english_average,
								'num_pupil_who_score_above_average_in_english' => $AClassPerformance->english_above_average,
								'average_maths_score' =>$AClassPerformance->maths_average,
								'num_pupil_who_score_above_average_in_maths' => $AClassPerformance->maths_above_average,
								'comment' => $AClassPerformance->comment,
								'term' => $term,
								'data_collector_type' => $data_collector_type,
								'data_collector_id' => $data_collector_id,
								'year' => $year,
								'country_id' => 1,
								'region_id' => $region_id,
								'district_id' => $district_id,
								'circuit_id' => $circuit_id,
								'school_id' => $school_id,
								'lat' => $lat,
								'long' =>$long,
								'average_science_score' => $AClassPerformance->average_science_score,
								'num_pupil_who_score_above_average_in_science' => $AClassPerformance->science_above_average,
								'num_pupil_who_can_read_and_write' => $AClassPerformance->num_literate_pupil
								);

							$save_pupils_performance_info = PupilsExamsPerformance::create($fields);


							if ($save_pupils_performance_info) {

								$saving_pupils_performance = true;

							}

							$saving_pupils_performance = true;

						}


				}


				//saving the pupils performance data data
				if ($saving_pupils_performance == true) {

					$school_reference_code = $school_code;

					self::calculatePupilAveragePerformanceOnTermlyExams($school_id, $school_reference_code,$data_collector_id,$data_collector_type,$term,$year,$region_id,$district_id,$circuit_id,$lat,$long);

					return Response::json(array("status"=>200,
						"content" => "Accepted",
						"message"=>"Saved successfully"));

				}else{

					return Response::json(array("status"=>401,
												"content" => "rejected",
												"message"=>"Failed Some class submission might not be complete"));
				}





		}







		public static function calculatePupilAveragePerformanceOnTermlyExams($school_id, $school_reference_code,$data_collector_id,$data_collector_type,$term,$year,$region_id,$district_id,$circuit_id,$lat,$long){


			$totalAverageGhanaianLanguageScore = 0;
			$totalPupilsWhoScoreAboveAverageInGhanaianLanguage = 0;
			$totalAverageEnglishLanguageScore = 0;
			$totalPupilsWhoScoreAboveAverageInEnglishLanguage = 0;
			$totalAverageMathsScore = 0;
			$totalPupilsWhoScoreAboveAverageInMaths = 0;

			$pupil_performance_reported_so_far = DB::table('pupils_exams_performance')
										->where('school_id',$school_id)
										->where('data_collector_id',$data_collector_id)
										->where('data_collector_type',$data_collector_type)
										->where('term',$term)
										->where('year',$year)
										->get();

			$totalClasses = count($pupil_performance_reported_so_far);


			if ($totalClasses > 0) {

				foreach ($pupil_performance_reported_so_far as $AClassPerformance) {

					$totalAverageGhanaianLanguageScore += $AClassPerformance->average_ghanaian_language_score;
					$totalPupilsWhoScoreAboveAverageInGhanaianLanguage += $AClassPerformance->num_pupil_who_score_above_average_in_ghanaian_language;
					$totalAverageEnglishLanguageScore += $AClassPerformance->average_english_score;
					$totalPupilsWhoScoreAboveAverageInEnglishLanguage += $AClassPerformance->num_pupil_who_score_above_average_in_english;
					$totalAverageMathsScore += $AClassPerformance->average_maths_score;
					$totalPupilsWhoScoreAboveAverageInMaths += $AClassPerformance->num_pupil_who_score_above_average_in_maths;

				}

				$recordNotAvailable = self::checkingToSeeWhetherARecordHasBeenCreatedForTotalTermlyDataForATerm($data_collector_type,$term,$year,$school_id,$school_reference_code,$data_collector_id);

				if ($recordNotAvailable) {

					$fields = array(
						'school_reference_code' => $school_reference_code,
						'average_ghanaian_language_score' => ($totalAverageGhanaianLanguageScore/$totalClasses) * 100,
						'ghanaian_language_above_average' => $totalPupilsWhoScoreAboveAverageInGhanaianLanguage/$totalClasses,
						'average_english_score' => ($totalAverageEnglishLanguageScore/$totalClasses) * 100,
						'english_above_average' => $totalPupilsWhoScoreAboveAverageInEnglishLanguage/$totalClasses,
						'average_maths_score' => ($totalAverageMathsScore/$totalClasses) * 100,
						'maths_above_average' => $totalPupilsWhoScoreAboveAverageInMaths/$totalClasses,
						'data_collector_id' => $data_collector_id,
						'data_collector_type' => $data_collector_type,
						'year' => $year,
						'term' => $term,
						'country_id' => 1,
						'region_id' => $region_id,
						'district_id' => $district_id,
						'circuit_id' => $circuit_id,
						'school_id' =>$school_id,
						'lat' => $lat,
						'long' =>$long);

					SchoolTotalsOnTermlySubmittedData::create($fields);


				}else{


					$findCurrentLevelOfPupilsPerformance = DB::table('termly_totals')
						->where('data_collector_type',$data_collector_type)
						->where('term',$term)
						->where('year',$year)
						->where('school_id',$school_id)
						->where('data_collector_id',$data_collector_id)->first();


					Log::info('Echoing current level of pupils performance object before update', array('context' => $findCurrentLevelOfPupilsPerformance));


					$update = DB::table('termly_totals')
						->where('school_reference_code', $school_reference_code)
						->where('data_collector_id', $data_collector_id)
						->where('data_collector_type', $data_collector_type)
						->where('circuit_id', $circuit_id)
						->where('school_id', $school_id)
						->where('year', $year)
						->where('term', $term)
						->update(array(
							'average_ghanaian_language_score' => ($totalAverageGhanaianLanguageScore/$totalClasses) * 100,
							'ghanaian_language_above_average' => $totalPupilsWhoScoreAboveAverageInGhanaianLanguage/$totalClasses,
							'average_english_score' => ($totalAverageEnglishLanguageScore/$totalClasses) * 100,
							'english_above_average' => $totalPupilsWhoScoreAboveAverageInEnglishLanguage/$totalClasses,
							'average_maths_score' => ($totalAverageMathsScore/$totalClasses) * 100,
							'maths_above_average' => $totalPupilsWhoScoreAboveAverageInMaths/$totalClasses));

				}

			}


		}





		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolSupportTypes($data)
		{
			Log::info('School Support Types data', array('context' => $data));
			
			$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = "ST";
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = $data->comment;
			$lat = $data->lat;
			$long = $data->long;
			$type = 'school_support_types';
			//$comment = "This is a test comment";

			$schoolSupportTypeAnswers = $data->types_of_support;

			$DonorSupport = $schoolSupportTypeAnswers[0];
			$CommunitySupport = $schoolSupportTypeAnswers[1];
			$DistrictSupport = $schoolSupportTypeAnswers[2];
			$PTASupport = $schoolSupportTypeAnswers[3];
			$OtherSupport = $schoolSupportTypeAnswers[4];

			Log::info('School Supprt Type data in an array', array('context' => $schoolSupportTypeAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				
					$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'donor_support_in_cash' => $DonorSupport->Cash,
							'donor_support_in_kind' => $DonorSupport->Kind,
							'community_support_in_cash' => $CommunitySupport->Cash,
							'community_support_in_kind' => $CommunitySupport->Kind,
							'district_support_in_cash' => $DistrictSupport->Cash,
							'district_support_in_kind' => $DistrictSupport->Kind,
							'pta_support_in_cash' => $PTASupport->Cash,
							'pta_support_in_kind' => $PTASupport->Kind,
							'other_support_in_cash' => $OtherSupport->Cash,
							'other_support_in_kind' => $OtherSupport->Kind,
							'comment' => $comment,
							'term' => $term,
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'year' => $year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long
							);
					//saving the support type data
					if (SupportType::create($fields)) {	
						
						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
						self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

						return Response::json(array("status"=>200,
													"content" => "accepted",
													"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
													"content" => "rejected",
													"message"=>"Failed submission"));
					}
			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already Submitted"));
			}
		}




		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnNumberOfTextBooks($data)
		{
			Log::info('Number of Textbooks', array('context' => $data));
			
			//$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = "CTB";
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			//$comment = $data->comment;
			$lat = $data->lat;
			$long = $data->long;
			$type = 'school_class_textbooks';
			//$comment = "This is a test comment";

			$schoolClassTexbooksAnswers = $data->number_of_textbooks;
			Log::info('School Number of Textbooks', array('context' => $schoolClassTexbooksAnswers));

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
				 $numberOfClasses = 0;
				 $numberOfGhanaianLanguageTextbooksInSchool = 0;
				 $numberOfEnglishTextbooksInSchool = 0;
				 $numberOfMathematicsTextbooksInASchool = 0;
					foreach ($schoolClassTexbooksAnswers  as $classAndtextBooks) {


						$checking_to_see_data_has_already_been_submitted_on_the_class = DB::table('class_textbooks')->where('school_id', $school_id)->where('level', $classAndtextBooks->level)->where('year', $year)->where('term', $term)->first();

								
								if (count($checking_to_see_data_has_already_been_submitted_on_the_class) < 1) {

									$numberOfClasses += 1;
					 				$numberOfGhanaianLanguageTextbooksInSchool += $classAndtextBooks->local;
					 				$numberOfEnglishTextbooksInSchool += $classAndtextBooks->english;
					 				$numberOfMathematicsTextbooksInASchool += $classAndtextBooks->maths;

									$fields = array(
										'school_reference_code' =>$school_code,
									    'questions_category_reference_code' =>$questions_category_reference_code,
										'level' => $classAndtextBooks->level,
										'number_of_ghanaian_language_textbooks' => $classAndtextBooks->local,
										'number_of_english_textbooks' => $classAndtextBooks->english,
										'number_of_maths_textbooks' => $classAndtextBooks->maths,
										'year' => $year,
										'data_collector_type' => $data_collector_type,
										'data_collector_id' => $data_collector_id,
										'term' => $term,
										'country_id' => 1,
										'region_id' => $region_id,
										'district_id' => $district_id,
										'circuit_id' => $circuit_id,
										'school_id' =>$school_id,
										'lat' => $lat,
										'long' =>$long);

										$textbookSave = Textbook::create($fields);
										//$textbookSave = true;
										//
									
								}else{

									Log::info('Textbooks data already submitted on this class for this term and year already', array('context' => $classAndtextBooks->level));

								}


						}

					$school_reference_code = $school_code;
					$recordNotAvailable = self::checkingToSeeWhetherARecordHasBeenCreatedForTotalTermlyDataForATerm($data_collector_type,$term,$year,$school_id,$school_reference_code,$data_collector_id);
																													

					if ($recordNotAvailable == false) {
								$fields = array(
												'school_reference_code' => $school_reference_code, 
												'num_ghanaian_language_books' => $numberOfGhanaianLanguageTextbooksInSchool,
												'num_english_books' => $numberOfEnglishTextbooksInSchool,
												'num_maths_books' => $numberOfMathematicsTextbooksInASchool,
												'data_collector_id' => $data_collector_id,
												'data_collector_type' => $data_collector_type,
												'year' => $year,
												'term' => $term,
												'country_id' => 1,
												'region_id' => $region_id,
												'district_id' => $district_id,
												'circuit_id' => $circuit_id,
												'school_id' =>$school_id,
												'lat' => $lat,
												'long' =>$long);

								SchoolTotalsOnTermlySubmittedData::create($fields);
							}else{

								$update = DB::table('termly_totals')
	            					->where('school_reference_code', $school_reference_code)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('circuit_id', $circuit_id)
	            					->where('school_id', $school_id)
	            					->where('term', $term)
	            						->update(array(
												'num_ghanaian_language_books' => $numberOfGhanaianLanguageTextbooksInSchool,
												'num_english_books' => $numberOfEnglishTextbooksInSchool,
												'num_maths_books' => $numberOfMathematicsTextbooksInASchool));
							}
					

					self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);
					self::saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id);

					return Response::json(array("status"=>200,
											"content" => "accepted",
											"message"=>"Data Submitted"));

			}else{

				return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Data Already Submitted"));
			}
		}






		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnSchoolGrantCapitationPayments($data)
		{
			Log::info('School grant payments data', array('context' => $data));
			
			$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = "SG";
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$comment = 'No Comment';
			$lat = $data->lat;
			$long = $data->long;
			$type = 'school_grants_payments';
			//$comment = "This is a test comment";

			$schoolGrantsPaymentsAnswers = $data->grants;
			Log::info('School Supprt Type data only', array('context' => $schoolGrantsPaymentsAnswers));

			//$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			$checkingToSeeWhetherDataIsSubmiteedAlready = true;

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {

					foreach ($schoolGrantsPaymentsAnswers as $grant) {

					Log::info('School grant type inside foreach', array('context' => $grant));

						$newSchoolInfo = DB::table('grant_type_tranches')
								->where('id', $school_id)
            					->where('school_reference_code', $school_code)
            					->where('school_grant_type', $grant->grantName)
            					->where('data_collector_type', $data_collector_type)
            					->where('data_collector_id', $data_collector_id)
            					->where('year', $year)->first();

            			if (count($newSchoolInfo) < 1) {

							Log::info('Inside if empty if condition', array('context' => 'infor'));

            				if ($grant->tranche == 'first_tranche') {
								
								Log::info('Inside another if empty if condition', array('context' => 'infor'));

		            				$fields = array(
									'school_reference_code' => $school_code,
									'questions_category_reference_code' =>$questions_category_reference_code,
									'school_grant_type' => $grant->grantName,
									'first_tranche_amount' => $grant->grantAmount,
									'second_tranche_amount' => '0',
									'third_tranche_amount' => '0',
									'first_tranche_date' => $grant->grantDate,
									'second_tranche_date' => 'Not submitted yet',
									'third_tranche_date' => 'Not submitted yet, hence no date',
									'comment' => $grant->comment,
									'term' => $term,
									'data_collector_type' => $data_collector_type,
									'data_collector_id' => $data_collector_id,
									'year' => $year,
									'country_id' => 1,
									'region_id' => $region_id,
									'district_id' => $district_id,
									'circuit_id' => $circuit_id,
									'school_id' => $school_id,
									'lat' => $lat,
									'long' =>$long
									);

		            				Log::info('Just before the create function', array('context' => 'information'));
									//Grant::create($fields);
									$save = Grant::create($fields);

									return Response::json(array("status"=>200,
																"content" => "accepted",
																"message"=>"Payments saved"));

									//$save = GeneralSchoolSituation::create($fields);
									

	            			}else{	
	            		
	            						Log::info('Just before the create function', array('context' => 'information'));
										//Grant::create($fields);
										//$save = Grant::create($fields);
										Log::info('Inside else condition of first tranche condition', array('context' => 'infor'));

	            					//Log::info('Just to see the plucked data', array('context' => $getCurrentCommentGrantComment));

	            				switch ($grant->tranche) {
	            						case 'second_tranche':
	            						//saving the the tranches
										$update = DB::table('grant_type_tranches')
		            					->where('school_reference_code', $school_code)
		            					->where('data_collector_type', $data_collector_type)
		            					->where('data_collector_id', $data_collector_id)
		            					->where('school_grant_type', $grant->grantName)
		            					->where('school_id', $school_id)
		            					->where('year', $year)
		            						->update(array(
		            							'second_tranche_amount' => $grant->grantAmount,
		            							'second_tranche_date' => $grant->grantDate,
		            							'term' => $term,
		            							'year' => $year));
	            							break;
	            						case 'third_tranche':
	            							//saving the the tranches
										 $update = DB::table('grant_type_tranches')
		            					->where('school_reference_code', $school_code)
		            					->where('data_collector_type', $data_collector_type)
		            					->where('data_collector_id', $data_collector_id)
		            					->where('school_grant_type', $grant->grantName)
		            					->where('school_id', $school_id)
		            					->where('year', $year)
		            						->update(array(
		            							'third_tranche_amount' => $grant->grantAmount,
		            							'third_tranche_date' => $grant->grantDate,
		            							'term' => $term,
		            							'year' => $year));
	            							break;
	            						
	            						default:
	            								return Response::json(array("status"=>401,
																"content" => "rejected",
																"message"=>"Please check your tranche"));
	            							break;
	            					}
	            			
	            			 }


            			}else{

	

            				switch ($grant->tranche) {

            						case 'first_tranche':
            						//saving the the tranches
									$update = DB::table('grant_type_tranches')
	            					->where('school_reference_code', $school_code)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('school_grant_type', $grant->grantName)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('school_id', $school_id)
	            					->where('year', $year)
	            						->update(array(
	            							'first_tranche_amount' => $grant->grantAmount,
	            							'first_tranche_date' => $grant->grantDate,
	            							'term' => $term,
	            							'year' => $year));

            							break;

            						case 'second_tranche':
            						//saving the the tranches
									$update = DB::table('grant_type_tranches')
	            					->where('school_reference_code', $school_code)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('school_grant_type', $grant->grantName)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('school_id', $school_id)
	            					->where('year', $year)
	            						->update(array(
	            							'second_tranche_amount' => $grant->grantAmount,
	            							'second_tranche_date' => $grant->grantDate,
	            							'term' => $term,
	            							'year' => $year));
            							break;

            						case 'third_tranche':
            							//saving the the tranches
									 $update = DB::table('grant_type_tranches')
	            					->where('school_reference_code', $school_code)
	            					->where('data_collector_type', $data_collector_type)
	            					->where('data_collector_id', $data_collector_id)
	            					->where('school_grant_type', $grant->grantName)
	            					->where('school_id', $school_id)
	            					->where('year', $year)
	            						->update(array(
	            							'third_tranche_amount' => $grant->grantAmount,
	            							'third_tranche_date' => $grant->grantDate,
	            							'term' => $term,
	            							'year' => $year));
            							break;
            						
            						default:
            								return Response::json(array("status"=>401,
															"content" => "rejected",
															"message"=>"Please check your tranche"));
            							break;
            					}


            			}

					}


			}else{

				return Response::json(array("status"=>401,
											"message"=>"Data Already Submitted"));
			}
		}



		public static function ReceiveDataOnGeneralSchoolSituation($data)
		{
			Log::info('General School data on school problems', array('context' => $data));
			
			$questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'SGS';
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$term = $data->term;
			$week_number = $data->week;
			$data_collector_id = $data->data_collector_id;
			$data_collector_type = $data->data_collector_type;
			$year = $data->year;
			$lat = $data->lat;
			$long = $data->long;
			$type = 'general_situation';
			$date = date('Y-m-d');
			//exit();
			$save = null;
			//accessing the various questions and their answers
			$generalSituation = $data->general_issues;

			$checkingToSeeWhetherDataIsSubmiteedAlready = self::checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year);

			Log::info('The general situation data', array('context' => $generalSituation));

			if ($checkingToSeeWhetherDataIsSubmiteedAlready == true) {
					

					foreach ($generalSituation as $oneSituation) {
							
							$fields = array(
							'data_collector_type' => $data_collector_type, 
							'data_collector_id' => $data_collector_id,
							'situation' => $oneSituation->question,
							'status' =>$oneSituation->answer,
							'comment' => $oneSituation->comment,
							'date' => $date,
							'week_number' => $week_number,
							'term' => $term,
							'year' =>$year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' =>$long);

							$save = GeneralSchoolSituation::create($fields);
					}

					if ($save != null) {

						self::saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id);

						self::savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id);
						
						//using push api available at www.pusher.com to send new data changes info to client side
						$data_type_submitted = 'schoolreportcard';
						$schoolIDAndDataType = array('school_id' => $school_id, 'type' => $data_type_submitted );

						NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($schoolIDAndDataType['school_id'],$schoolIDAndDataType['type']);

						// Queue::push(function($job) use($schoolIDAndDataType){
						// 	NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($schoolIDAndDataType['school_id'],$schoolIDAndDataType['type']);
						// 	$job->delete();
						// });

						return Response::json(array("status"=>200,
											"content" => "accepted",
											"message"=>"Data Submitted"));
					}else{
						return Response::json(array("status"=>401,
											"content" => "rejected",
											"message"=>"Failed submission"));
					}

					
			}else{
				return Response::json(array("status"=>401,
										"content" => "rejected",
										"message"=>"Data Already Submitted"));

			}

		}

		
			/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * Preturn Response
			 */
			public static function schoolsReportCardEnrolmentData($question_category_code,$queryString, $values)
			{
		      
		      		try {

				      			  //Querying The Questions Category and adding enrolment data
		    			return $enrolmentData = QuestionCategory::with(array('school_enrolment' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_code', $question_category_code)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}


			}


				/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardSpecialEnrolmentData($question_category_code,$queryString, $values)
			{
		      
		      		try {

				      			  //Querying The Questions Category and adding enrolment data
		    			return $enrolmentData = QuestionCategory::with(array('special_school_enrolment' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_code', $question_category_code)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}


			}


					/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardStudentsAttendanceData($question_category_code,$queryString, $values)
			{
		        
		   		try {

		   			     //Querying The Questions Category and adding school attendance data
		    			return $enrolmentData = QuestionCategory::with(array('school_attendance' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_code', $question_category_code)->get();
		   			
		   		} catch (Exception $e) {
		   			
		   			return "Check the fields in your posted data";
		   		}


			}



					/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardSpecialStudentsAttendanceData($question_category_code,$queryString, $values)
			{
		        
		   		try {

		   			     //Querying The Questions Category and adding school attendance data
		    			return $enrolmentData = QuestionCategory::with(array('special_students_attendance' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_code', $question_category_code)->get();
		   			
		   		} catch (Exception $e) {
		   			
		   			return "Check the fields in your posted data";
		   		}


			}



			public static function retrieveStateOfSchoolsMeetings($question_category_code, $queryString, $values)
			{
				  try {

			        	//return "Am here to see see school teachers info";
			        //Querying The Questions Category and adding school teachers info
	    			return $schoolMeetings = QuestionCategory::with(array('school_meetings' => function($query) use($queryString, $values)
					{
						//querying base on the parameters posted which is contain in the $querystring variable
					    $query->whereRaw($queryString, $values)->get();;

					}))->where('question_category_code', $question_category_code)->get();

		        } catch (Exception $e) {

		        	return "Check the fields in your posted data";
		        }


			}
			



				/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardSchoolTeachers($question_category_code, $queryString, $values)
			{
		        
		        try {

			        	//return "Am here to see see school teachers info";
			        //Querying The Questions Category and adding school teachers info
	    			return $enrolmentData = QuestionCategory::with(array('school_teachers' => function($query) use($queryString, $values)
					{	
						//querying base on the parameters posted which is contain in the $querystring variable
					    $query->whereRaw($queryString, $values)->get();

					}))->where('question_category_code', $question_category_code)->get();

		        } catch (Exception $e) {
		        	
		        	return "Check the fields in your posted data";
		        }
		        
			}


				/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
			public static function stateOfSchoolTeacherPunctualityAndLessonPlan($id)
			{
				try {

					return $data = Teachers::with('puntuality_and_lesson_plans')->where('id',$id)->first(); 
					
				} catch (Exception $e) {
					
					return "You must pass the teacher id in the route";
				}
			}


					/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
			public static function stateOfSchoolUnitsCoveredByTeachers($queryString, $values)
			{
				try {
				 		//return "Am here to see see school teachers info";
			        	//Querying The Questions Category and adding school teachers info
	    				return $UnitsCoveredBySchoolTeachers = TeacherUnitsCovered::with('teacher')->whereRaw($queryString, $values)->get();
				 	
				 	} catch (Exception $e) {

				 		return "Please pass at least a query parameter e.g country_id =1 ";
				 		
				 	}
			}


			/**
			 * Display a listing of info on school whole
			 * school managenent.
			 * With respect to school report card
			 * @return Response
			 */
			public static function stateOfSchoolTeacherClassManagement($id)
			{
				try {

					//return "You about to see a teaher class management skills";
					return $data = Teachers::with('classroom_management')->where('id',$id)->first();
				 	
				 } catch (Exception $e) {
				 	
				 	return "You must pass the teacher id in the route";
				 } 
			}


			public static function stateOfSchoolPunctuality($queryString, $values)
			{
				 	try {
				 		//return "Am here to see see school teachers info";
			        	//Querying The Questions Category and adding school teachers info
	    				return $PunctualityData = PuntualityAndLessons::with('teacher')->whereRaw($queryString, $values)->get();
				 	} catch (Exception $e) {

				 		return "Please pass at least a query parameter e.g country_id =1 ";
				 		
				 	}
			}



			public static function stateOfSchoolManagement($queryString, $values)
			{	
					try {

						//return "Am here to see see school teachers info";
			        //Querying The Questions Category and adding school teachers info
	    			return $enrolmentData = TeachersClassManagement::with('teacher')->whereRaw($queryString, $values)->get();
						
					} catch (Exception $e) {

				 		return "Please pass atleast a query parameter e.g country_id =1 ";
						
					}
				 	
			}


					/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardPupilsPerformance($question_category_code, $queryString, $values)
			{
		        try {

			        	//return "Am here to see see school teachers info";
			        //Querying The Questions Category and adding school teachers info
	    			return $PupilsPerformance = QuestionCategory::with(array('pupils_performance' => function($query) use($queryString, $values)
					{	
						//querying base on the parameters posted which is contain in the $querystring variable
					    $query->whereRaw($queryString, $values)->get();;

					}))->where('question_category_code', $question_category_code)->get();

		        } catch (Exception $e) {
		        	
		        	return "Check the fields in your posted data";
		        }
		        
			}


				/**
			 * Display a listing of info on school support.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardSchoolSupportType($question_category_code, $queryString, $values)
			{
		        
		        try {

			        	//return "Am here to see see school teachers info";
			        //Querying The Questions Category and adding school teachers info
	    			return $SupportType = QuestionCategory::with(array('school_support_type' => function($query) use($queryString, $values)
					{	
						//querying base on the parameters posted which is contain in the $querystring variable
					    $query->whereRaw($queryString, $values)->get();;

					}))->where('question_category_code', $question_category_code)->get();

		        } catch (Exception $e) {
		        	
		        	return "Check the fields in your posted data";
		        }
		        
			}

			/**
			 * Display a listing of info on school support.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardSchoolCapitationGrantsPayments($question_category_code, $queryString, $values)
			{
		        
		        try {

			        	//return "Am here to see see school teachers info";
			        //Querying The Questions Category
	    			return $GrantCapitationPayments = QuestionCategory::with(array('school_grant' => function($query) use($queryString, $values)
					{	
						//querying base on the parameters posted which is contain in the $querystring variable
					    $query->whereRaw($queryString, $values)->get();;

					}))->where('question_category_code', $question_category_code)->get();

		        } catch (Exception $e) {
		        	
		        	return "Check the fields in your posted data";
		        }
		        
			}


				/**
			 * Display a listing of info on school support.
			 * With respect to school report card
			 * @return Response
			 */
			public static function schoolsReportCardSchoolClassTextBooks($question_category_code, $queryString, $values)
			{
		        
		        try {

			        	//return "Am here to see see school teachers info";
			        //Querying The Questions Category
	    			return $schoolTextBooks = QuestionCategory::with(array('school_textbooks' => function($query) use($queryString, $values)
					{	
						//querying base on the parameters posted which is contain in the $querystring variable
					    $query->whereRaw($queryString, $values)->get();;

					}))->where('question_category_code', $question_category_code)->get();

		        } catch (Exception $e) {
		        	
		        	return "Check the fields in your posted data";
		        }
		        
			}



			public static function retrieveGeneralSchoolSituationData($queryString, $values)
			{
				try {
				 		//return "Am here to see see school teachers info";
			        	//Querying The Questions Category and adding school teachers info
	    				return $generalSituation = DB::table('general_school_situation')->whereRaw($queryString, $values)->get();

				 	} catch (Exception $e) {

				 		return "Please pass atleast a query parameter e.g country_id =1 ";
				 		
				 	}
			}


			public static function saveRecentSubmissionOnSchoolReportCard($data_collector_id,$year,$region_id,$district_id,$circuit_id,$school_id)
			{
				
				$fields = array(
									'data_collector_id' => $data_collector_id, 
									'data_type_submitted' => 'schoolreportcard',
									'year' => $year,
									'country_id' => 1,
									'region_id' => $region_id,
									'district_id' => $district_id,
									'circuit_id' => $circuit_id,
									'school_id' => $school_id
								);
						//saving recent submissions
						$savingRecentDataStatus = RecentDataSubmitted::create($fields);

			}


			public static function saveSchoolReportCardSubmissionStatus($data_collector_id,$data_collector_type,$type,$term,$year,$region_id,$district_id,$circuit_id,$school_id)
			{
				$fields = array(
											'data_collector_id' => $data_collector_id, 
											'data_collector_type' => $data_collector_type,
											'type' => $type,
											'term' => $term,
											'year' => $year,
											'country_id' => 1,
											'region_id' => $region_id,
											'district_id' => $district_id,
											'circuit_id' => $circuit_id,
											'school_id' => $school_id
										);

						$savingSchoolReportDataStatus = SchoolReportCardSubmissionStatus::create($fields);
			}


			public static function checkingToSeeDataHasBeenSubmittedOnTermlySubmission($data_collector_id,$data_collector_type,$type,$term,$year)
			{
				$checkingToSeeWhetherDataHasAlreadyBeenSubmitted = DB::table('school_reportcard_submission_status')
														->where('data_collector_id',$data_collector_id)
														->where('data_collector_type',$data_collector_type)
														->where('type',$type)
														->where('term',$term)->where('year',$year)->first();

				if (empty($checkingToSeeWhetherDataHasAlreadyBeenSubmitted)) {
					return true;
				}

				return false;
			}


			public static function savingWeeklySubmissionStatusOnSchoolReportCard($data_collector_type,$data_collector_id,$type,$week_number,$term,$year,$region_id,$district_id,$circuit_id,$school_id)
			{
				$fields = array(
								'data_collector_type' => $data_collector_type, 
								'data_collector_id' => $data_collector_id,
								'type' =>$type,
								'week_number' =>$week_number,
								'term'=> $term,
								'year' => $year,
								'country_id' => 1,
								'region_id' => $region_id,
								'district_id' => $district_id,
								'circuit_id' => $circuit_id,
								'school_id' =>$school_id
								);
				$savingWeekySubmissionStatusOnSchoolReportCard = SchoolReportCardWeeklySubmissionStatus::create($fields);

			}


			public static function checkingToSeeWhetherGrantDataHasBeenSubmitted($grantName,$tranche,$trancheDate,$data_collector_id,$school_id)
			{
				
				return $checkingToSee = DB::table('school_grant_type',$grantName)
										->where('tranche_type', $tranche)
										->where('grant_date', $trancheDate)
										->where('data_collector_id', $data_collector_id)
										->where('school_id', $school_id)
										->first();
			}



			public static function checkingToSeeWhetherARecordHasBeenCreatedForTotalWeeklyDataForAWeek($data_collector_type,$week_number,$school_id,$school_reference_code,$data_collector_id,$term,$year)
			{
				 $recordAvailability = DB::table('weekly_totals')
											->where('data_collector_type',$data_collector_type)
											->where('week_number',$week_number)
											->where('term',$term)
											->where('year',$year)
											->where('school_id',$school_id)
											->where('school_reference_code',$school_reference_code)
											->where('data_collector_id',$data_collector_id)->first();

					if (empty($recordAvailability)) {

						return true;

					}else{
						return false;
					}
			}

		public static function checkingToSeeWhetherARecordHasBeenCreatedForTotalTermlyDataForATerm($data_collector_type,$term,$year,$school_id,$school_reference_code,$data_collector_id)
		{
				 $recordAvailability = DB::table('termly_totals')
											->where('data_collector_type',$data_collector_type)
											->where('term',$term)
											->where('year',$year)
											->where('school_id',$school_id)
											->where('data_collector_id',$data_collector_id)->first();

					if (count($recordAvailability) != 1) {

						return true;

					}else{

						return false;

					}
		}


		public static function RetrieveTotalsOnWeeklySubmittedData($queryString, $values)
		{
//			try {
				 		//return "Am here to see see school teachers info";
			        	//Querying The Questions Category and adding school teachers info
	    				return $weekly_totals = DB::table('weekly_totals')->whereRaw($queryString, $values)->get();

//				 	} catch (Exception $e) {

//				 		return "Please pass atleast a query parameter e.g country_id =1 ";
				 		
//				 	}
		}


		public static function RetrieveTotalsOnTermlySubmittedData($queryString, $values)
		{
			try {
				 		//return "Am here to see see school teachers info";
			        	//Querying The Questions Category and adding school teachers info
	    				return $termly_totals = DB::table('termly_totals')->whereRaw($queryString, $values)->get();

				 	} catch (Exception $e) {

				 		return "Please pass atleast a query parameter e.g country_id =1 ";
				 		
				 	}
		}



			/**
			 * Display a listing of info on school enrolment.
			 * With respect to school report card
			 * @return Response
			 */
		public static function getWeeksTermsYearsThatDataHasBeenSubmittedOn($queryString, $values, $submission_category_to_check)
		{
				switch ($submission_category_to_check) {

					case 'normal_students_enrolment':
							$response = DB::table('school_enrolments')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
						break;
					case 'normal_students_attendance':
							$response = DB::table('student_attendance')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
						break;

					case 'special_students_enrolment':
							$response = DB::table('special_enrolment')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
						break;

					case 'special_students_attendance':
							$response = DB::table('special_attendance')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
						break;

					case 'teacher_attendance':
							$response = DB::table('punctuality_and_lessons')->whereRaw($queryString, $values)->distinct()->select('week_number','term','year')->get();
						break;
						
					default:
						$response = array('info' => "Please check the category you are posting");
						break;
				}


				return Response::json(array("status"=>200,
											"message" => "Data returned if there exist any in a list form",
											"data"=>$response));


			}








		public static function getSchoolsCircuitsDistrictsRegions($queryString, $values, $query_level)
		{
			switch ($query_level) {

				case 'schools':
					$response = DB::table('schools')->whereRaw($queryString, $values)->get();
					break;
				case 'circuits':
					$response = DB::table('circuits')->whereRaw($queryString, $values)->get();
					break;

				case 'districts':
					$response = DB::table('districts')->whereRaw($queryString, $values)->get();
					break;

				case 'regions':
					$response = DB::table('regions')->whereRaw($queryString, $values)->get();
					break;

				default:
					$response = array('info' => "Please check the category you are posting");
					break;
			}


			return Response::json(array("status"=>200,
				 "message" => "Data returned if there exist any in a list form base on your query paramerters",
				"data"=>$response));


		}



	}


?>