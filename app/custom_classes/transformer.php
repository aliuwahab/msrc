<?php namespace custom_classes;


		abstract class Transformer{


			//this take an array of a collection of data and pass
			//each to the transform function
			public function transformCollection(array $item)
			{
				# transforming the data
				return array_map([$this, 'transform'], $items); 
			}

			public abstract function transform($item);

		}