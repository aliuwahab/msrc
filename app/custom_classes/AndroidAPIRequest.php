<?php
//This class accepts Android API request to login and 
//for itineraries associated with a district
//Most request by android via the api are processed here
class AndroidAPIRequest {


	public static function apiDataCollectorsLogin($phone_number,$unique_code,$universal_code)
	{
		
		$data_collector = DB::table('data_collectors')->where("phone_number", $phone_number)->where('identity_code', $unique_code)->where('universal_code', $universal_code)->first();

		if ($data_collector) {
			switch ($data_collector->type) {
				case 'circuit_supervisor':
					$allDistrictSchools = DB::table('schools')->where("district_id", $data_collector->district_id)->get();
					return Response::json(array("status"=>200,
												"message"=>"OK",
												"type"=>"circuit_supervisor",
												"circuit_supervisor"=>$data_collector,
												'schools' => $allDistrictSchools));
					break;
				
				default:
					$headteacherSchool = $allDistrictSchools = DB::table('schools')->where("headteacher_id", $data_collector->id)->first();

					$starsSchool  = DB::table('mk_stars_schools')->where("school_code", $headteacherSchool->ges_code)->first();

                    $headteacherSchool->circuit_status = "";
					if (!empty($starsSchool) && $starsSchool != null) {
                        $headteacherSchool->circuit_status = $starsSchool->circuit_status;
                    }

					return Response::json(array("status"=>200,
												"message"=>"OK",
												"type"=>"head_teacher",
												"head_teacher"=>$data_collector,
												'schools' => $headteacherSchool));
					break;
			}
		}else{
			$message = 'Check your login details, or your app authenticity';
			$errorCode = 401;
			return Helper::apiRequestError($message,$errorCode);
		}
	}


	public static function apiRequestForAllItinariesForADistrict($data)
	{
		$checkingForUnexpiredItineraries = date('Y-m-d');
		//$checkingForUnexpiredItineraries = '2014-08-25';
		
		if (!empty($data)) {
			$required_fields = array("district_id"=>"");
			//accessing the various parameters received
			$res = Helper::malformedQueryCheck($required_fields,$data);

			$message = "Bad Request, check out your fields";
			$code = 400;
			
			if(!$res) return Helper::apiRequestError($message,$code);

			$district = trim($data['district_id']);

			$allItinerary = DB::table('itenary')->where("district_id", $district)
									->where('end_date', '>=',$checkingForUnexpiredItineraries)
									->get();

					return Response::json(array("status"=>200,
												"message"=>"OK",
												"itinerary"=>$allItinerary));

		}else{
			$message = 'You have not provided all the required parameters';
			$errorCode = 401;
			return Helper::apiRequestError($message,$errorCode);
		}
	}


}