<?php
use Illuminate\Support\Facades\DB;

/**
		* This class receives data submitted by android app
		* through the the SchoolReviewController
		*/
		class SchoolReview
		{
			

			public static function ReceivesSchoolReviewDataSubmittedByAndroidApp($data)
			{
				//getting the school property of the data posted and accessing it various fields
				$school = $data->school;
				$category = $data->category;

				Log::info('Under school review data the school', array('context' => $school));
				//this access the various properties of the school
				$school_id = $school->school_id;
				$phone_number = $school->phone_number;
				$school_code = $school->school_code;
				$school_name = $school->school_name;
				$region_id = $school->region_id;
				$district_id = $school->district_id;
				$circuit_id = $school->circuit_id;
				$headteacher_id = $school->headteacher_id;
				$week_number =  "5";
				$term =  "first_term";

				$year = date('Y');
				$date = date("m.d.y");

				//$itenary_id = 1;
				$itenary_id = $school->itenary_id;

				//accessing the data_collector property
				$data_collector = $data->data_collector;

				//Log::info('Under school review data the data_collector', array('context' => $data_collector));

				//accesing the various properties in the data collector
				$data_collector_id = $data_collector->id;
				$data_collector_first_name = $data_collector->first_name;
				$data_collector_last_name = $data_collector->last_name;
				$data_collector_identity_code = $data_collector->identity_code;
				//$data_collector_type = 'circuit_supervisor';
				$data_collector_type = $data_collector->type;

				$latitude = $data_collector->lat;

				$longitude = $data_collector->long;



				//accessing the various parts of the data posted By Android App

				switch ($category) {

					case "community_involvement":

						$communityInvolvement = $data->schoolReview->CommunityInvolvementAnswer;
						//Log::info('Under school review data communityInvolvement', array('context' => $communityInvolvement));
						if (!empty($communityInvolvement)) {

							$lastSave = self::saveCommunityInvolvementData($communityInvolvement,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$year,$date,$latitude,$longitude);

						}else{
							return Response::json(array("status"=>401,
								"message"=>"There exist no data on the selected part"));
						}
						break;
					case "effective_teaching_and_learning":
						$EffectiveTeachingAndLearningAnswer = $data->schoolReview->EffectiveTeachingAndLearningAnswer;
						//Log::info('Under school review data effective teaching', array('context' => $EffectiveTeachingAndLearningAnswer));
						if (!empty($EffectiveTeachingAndLearningAnswer)) {
							$lastSave = self::saveTeachingAndLearningData($EffectiveTeachingAndLearningAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$year,$date,$latitude,$longitude);

						}else{
							return Response::json(array("status"=>401,
								"message"=>"There exist no data on the selected part"));
						}
						break;
					case "friendly_boys_and_girls":
						$FriendlyBoysAndGirlsAnswer = $data->schoolReview->FriendlyBoysAndGirlsAnswer;
						//Log::info('Under school review data FriendlyBoysAndGirlsAnswer', array('context' => $FriendlyBoysAndGirlsAnswer));
						if (!empty($FriendlyBoysAndGirlsAnswer)) {
							$lastSave = self::saveFriendlyBoysAndGirlsData($FriendlyBoysAndGirlsAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$year,$date,$latitude,$longitude);
						}else{
							return Response::json(array("status"=>401,
								"message"=>"There exist no data on the selected part"));
						}
						break;
					case "healthy_school":
						$HealthySchoolAnswer = $data->schoolReview->HealthySchoolAnswer;
						//Log::info('Under school review data HealthySchoolAnswer', array('context' => $HealthySchoolAnswer));
						if (!empty($HealthySchoolAnswer)) {
							$lastSave = self::saveHealthySchoolAnswerData($HealthySchoolAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$year,$date,$latitude,$longitude);
						}else{
							return Response::json(array("status"=>401,
								"message"=>"There exist no data on the selected part"));
						}
						break;
					case "safety_and_protection":
						$SafetyAndProtectionAnswer = $data->schoolReview->SafetyAndProtectionAnswer;
						//Log::info('Under school review data safety_and_protection', array('context' => $SafetyAndProtectionAnswer));
						if (!empty($SafetyAndProtectionAnswer)) {
							$lastSave = self::saveSafetyAndProtectionData($SafetyAndProtectionAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$year,$date,$latitude,$longitude);

						}else{
							return Response::json(array("status"=>401,
								"message"=>"There exist no data on the selected part"));
						}
						break;
					case "school_inclusiveness":
						$SchoolInclusivenessAnswer = $data->schoolReview->SchoolInclusivenessAnswer;
						//Log::info('Under school review data school_inclusiveness', array('context' => $SchoolInclusivenessAnswer));
						if (!empty($SchoolInclusivenessAnswer)) {

							$lastSave = self::saveSchoolInclusivenessData($SchoolInclusivenessAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$year,$date,$latitude,$longitude);

						}else{
							return Response::json(array("status"=>401,
								"message"=>"There exist no data on the selected part"));
						}
						break;
					default:

						return Response::json(array("status"=>401,
							"message"=>"Please there is no an appropriate category indicated"));

				}



						//using push api available at www.pusher.com to send new data changes info to client side
						$data_type_submitted = 'schoolreview';
						$schoolIDAndDataType = array('school_id' => $school_id, 'type' => $data_type_submitted );
						

						NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($schoolIDAndDataType['school_id'],$schoolIDAndDataType['type']);

						if ($lastSave) {
								$fields = array(

											'data_collector_id' => $data_collector_id, 
											'data_type_submitted' => 'schoolreview',
											'year' => $year,
											'country_id' => 1,
											'region_id' => $region_id,
											'district_id' => $district_id,
											'circuit_id' => $circuit_id,
											'school_id' => $school_id
										);

										RecentDataSubmitted::create($fields);


									$fields = array(
										'data_collector_type' => $data_collector_type,
										'data_collector_id' => $data_collector_id, 
										'date' => $date,
										'week_number' => $week_number,
										'term' => $term,
										'year' => $year,
										'country_id' => 1,
										'region_id' => $region_id,
										'district_id' => $district_id,
										'circuit_id' => $circuit_id,
										'school_id' => $school_id);

									$saving = SchoolReviewSubmissionStatus::create($fields);


									if ($saving) {
										return Response::json(array("status"=>200,
																	"message"=>"Data Submitted"));
									}
									return Response::json(array("status"=>401,
																"message"=>"Failed Submission"));


						}




			}





			public static function saveCommunityInvolvementData($communityInvolvement,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$latitude,$longitude)
			{

				$current_community_involvement = 0;

				$school = School::find($school_id);


				$checkingToSeeSubmissionStatus = DB::table('community_involvement')
					->where('itenary_id',$itenary_id)
					->where('submitter',$data_collector_type)
					->where('school_code_reference',$school_code)
					->where('school_id',$school_id)
					->first();


				//checking to see whether data has already been submitted under that school
				if (count($checkingToSeeSubmissionStatus) > 0) {

					Log::info('School Review Data Already', array('context' => 'Already submitted'));

					return Response::json(array("status"=>401,
						"message"=>"Data Already Submitted"));
					exit();


				}



				foreach ($communityInvolvement as $community_involvement) {

							$current_community_involvement += (int) $community_involvement->answer;

							$fields = array(
							'itenary_id' => $itenary_id,
							'school_code_reference' => $school_code,
							'question_code_reference' => $community_involvement->question_code,
							'submitter' =>  $data_collector_type,
							'answer' => $community_involvement->answer, 
//							'comment' => $community_involvement->comment,
							'comment' => (!empty($community_involvement->comment) ? $community_involvement->comment : "No comment"),
							'year' => $year,
							'date' => $date, 
							'country_id' => 1, 
							'region_id' => $region_id, 
							'district_id' => $district_id, 
							'circuit_id' => $circuit_id, 
							'school_id' => $school_id,
							'lat' => $latitude,
							'long' => $longitude
						);


						CommunityInvolvement::create($fields);
				}


				Log::info('CommunityInvolvement Total: ', array('context' => $current_community_involvement));

				$current_community_involvement = ($current_community_involvement / 2);

				$school->current_state_of_community_involvement = $current_community_involvement;

				$school->save();



			}





			public static function saveTeachingAndLearningData($EffectiveTeachingAndLearningAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$latitude,$longitude)
			{
					
					$current_teaching_learning = 0;

					$school = School::find($school_id);


				$checkingToSeeSubmissionStatus = DB::table('effective_teaching_learning')
					->where('itenary_id',$itenary_id)
					->where('submitter',$data_collector_type)
					->where('school_code_reference',$school_code)
					->where('school_id',$school_id)
					->first();


				//checking to see whether data has already been submitted under that school
				if (count($checkingToSeeSubmissionStatus) > 0) {

					Log::info('School Review Data Already Submitted On this Itnerary for Effective Teaching', array('context' => 'Already submitted'));

					return Response::json(array("status"=>401,
						"message"=>"Data Already Submitted"));
					exit();


				}


					foreach ($EffectiveTeachingAndLearningAnswer as $teaching_learning) {

							$current_teaching_learning += (int) $teaching_learning->answer;

							$fields = array(
							'itenary_id' => $itenary_id,
							'school_code_reference' => $school_code,
							'question_code_reference' => $teaching_learning->question_code,
							'submitter' =>  $data_collector_type,
							'answer' => $teaching_learning->answer, 
//							'comment' => $teaching_learning->comment,
							'comment' => (!empty($teaching_learning->comment) ? $teaching_learning->comment : "No comment"),
							'year' => $year,
							'date' => $date, 
							'country_id' => 1, 
							'region_id' => $region_id, 
							'district_id' => $district_id, 
							'circuit_id' => $circuit_id, 
							'school_id' => $school_id,
							'lat' => $latitude,
							'long' => $longitude
						);

						//saving the effective teaching and learning data
						EffectiveTeachingLearning::create($fields);
				}

				Log::info('Teaching And learning Total: ', array('context' => $current_teaching_learning));

				$current_teaching_learning = ($current_teaching_learning / 2);

				$school->current_state_of_teaching_and_learning = $current_teaching_learning;

				$school->save();





			}





			public static function saveFriendlyBoysAndGirlsData($FriendlyBoysAndGirlsAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$latitude,$longitude)
			{

					$current_friendliness_to_boys_girls = 0;

					$school = School::find($school_id);



				$checkingToSeeSubmissionStatus = DB::table('friendly_schools')
					->where('itenary_id',$itenary_id)
					->where('submitter',$data_collector_type)
					->where('school_code_reference',$school_code)
					->where('school_id',$school_id)
					->first();


				//checking to see whether data has already been submitted under that school
				if (count($checkingToSeeSubmissionStatus) > 0) {

					Log::info('School Review Data Already Submitted On this Itnerary for Friendly Boys and Girls Section', array('context' => 'Already submitted'));

					return Response::json(array("status"=>401,
						"message"=>"Data Already Submitted"));
					exit();


				}
					
					foreach ($FriendlyBoysAndGirlsAnswer as $friendliness) {

							$current_friendliness_to_boys_girls += (int) $friendliness->answer;

							$fields = array(
							'itenary_id' => $itenary_id,
							'school_code_reference' => $school_code,
							'question_code_reference' => $friendliness->question_code,
							'submitter' =>  $data_collector_type,
							'answer' => $friendliness->answer , 
//							'comment' => $friendliness->comment,
							'comment' => (!empty($friendliness->comment) ? $friendliness->comment : "No comment"),
							'year' => $year,
							'date' => $date, 
							'country_id' => 1, 
							'region_id' => $region_id, 
							'district_id' => $district_id, 
							'circuit_id' => $circuit_id, 
							'school_id' => $school_id,
							'lat' => $latitude,
							'long' => $longitude
						);

						//saving friendly schools data
						FriendlySchool::create($fields);
				}



				Log::info('FriendlySchool total: ', array('context' => $current_friendliness_to_boys_girls));


				$current_friendliness_to_boys_girls = ($current_friendliness_to_boys_girls / 2);

				$school->current_state_of_friendliness_to_boys_girls = $current_friendliness_to_boys_girls;

				$school->save();

			}




			public static function saveHealthySchoolAnswerData($HealthySchoolAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$latitude,$longitude)
			{
					$current_healthy_school_level = 0;

					$school = School::find($school_id);


					$checkingToSeeSubmissionStatus = DB::table('healthy_level')
						->where('itenary_id',$itenary_id)
						->where('submitter',$data_collector_type)
						->where('school_code_reference',$school_code)
						->where('school_id',$school_id)
						->first();


					//checking to see whether data has already been submitted under that school
					if (count($checkingToSeeSubmissionStatus) > 0) {

						Log::info('School Review Data Already Submitted On this Itnerary for Healthy Level', array('context' => 'Already submitted'));

						return Response::json(array("status"=>401,
							"message"=>"Data Already Submitted"));
						exit();


					}

					foreach ($HealthySchoolAnswer as $healthy_school) {

							$current_healthy_school_level += (int) $healthy_school->answer;

							$fields = array(
							'itenary_id' => $itenary_id,
							'school_code_reference' => $school_code,
							'question_code_reference' => $healthy_school->question_code,
							'submitter' =>  $data_collector_type,
							'answer' => $healthy_school->answer , 
//							'comment' => $healthy_school->comment,
							'comment' => (!empty($healthy_school->comment) ? $healthy_school->comment : "No comment"),
							'year' => $year,
							'date' => $date, 
							'country_id' => 1, 
							'region_id' => $region_id, 
							'district_id' => $district_id, 
							'circuit_id' => $circuit_id, 
							'school_id' => $school_id,
							'lat' => $latitude,
							'long' => $longitude
						);

						//save healthy schools
						HealthyLevel::create($fields);
				}


				Log::info('HealthyLevel total: ', array('context' => $current_healthy_school_level));

				$current_healthy_school_level = ($current_healthy_school_level / 2);

				$school->current_state_of_healthy_level = $current_healthy_school_level;

				$school->save();



			}




			public static function saveSafetyAndProtectionData($SafetyAndProtectionAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$latitude,$longitude)
			{
					
					$current_safety_and_protection_level = 0;

					$school = School::find($school_id);


					$checkingToSeeSubmissionStatus = DB::table('safe_protective')
						->where('itenary_id',$itenary_id)
						->where('submitter',$data_collector_type)
						->where('school_code_reference',$school_code)
						->where('school_id',$school_id)
						->first();


					//checking to see whether data has already been submitted under that school
					if (count($checkingToSeeSubmissionStatus) > 0) {

						Log::info('School Review Data Already Submitted On this Itnerary for Safety and Protection', array('context' => 'Already submitted'));

						return Response::json(array("status"=>401,
							"message"=>"Data Already Submitted"));
						exit();


					}


					foreach ($SafetyAndProtectionAnswer as $safety_and_protection) {

							$current_safety_and_protection_level += (int) $safety_and_protection->answer;

							$fields = array(
							'itenary_id' => $itenary_id,
							'school_code_reference' => $school_code,
							'question_code_reference' => $safety_and_protection->question_code,
							'submitter' =>  $data_collector_type,
							'answer' => $safety_and_protection->answer , 
//							'comment' => $safety_and_protection->comment,
							'comment' => (!empty($safety_and_protection->comment) ? $safety_and_protection->comment : "No comment"),
							'year' => $year,
							'date' => $date, 
							'country_id' => 1, 
							'region_id' => $region_id, 
							'district_id' => $district_id, 
							'circuit_id' => $circuit_id, 
							'school_id' => $school_id,
							'lat' => $latitude,
							'long' => $longitude
						);

						SafeProtective::create($fields);
				}


				Log::info('SafetyAndProtection total: ', array('context' => $current_safety_and_protection_level));

				$current_safety_and_protection_level = ($current_safety_and_protection_level / 2);

				$school->current_state_of_safety_and_protection = $current_safety_and_protection_level;

				$school->save();


			}




			public static function saveSchoolInclusivenessData($SchoolInclusivenessAnswer,$itenary_id,$year,$date,$school_code,$region_id,$district_id,$circuit_id,$school_id,$data_collector_type,$latitude,$longitude)
			{
					
					$current_school_inclusiveness_level = 0;

					$school = School::find($school_id);


					$checkingToSeeSubmissionStatus = DB::table('school_inclusive_answers')
						->where('itenary_id',$itenary_id)
						->where('submitter',$data_collector_type)
						->where('school_code_reference',$school_code)
						->where('school_id',$school_id)
						->first();


					//checking to see whether data has already been submitted under that school
					if (count($checkingToSeeSubmissionStatus) > 0) {

						Log::info('School Review Data Already Submitted On this Itnerary for Safety and Protection', array('context' => 'Already submitted'));

						return Response::json(array("status"=>401,
							"message"=>"Data Already Submitted"));
						exit();


					}


						


					foreach ($SchoolInclusivenessAnswer as $school_inclusiveness) {
							
							$current_school_inclusiveness_level += (int) $school_inclusiveness->answer;

							$fields = array(
							'itenary_id' => $itenary_id,
							'school_code_reference' => $school_code,
							'question_code_reference' => $school_inclusiveness->question_code,
							'submitter' =>  $data_collector_type,
							'answer' => $school_inclusiveness->answer , 
//							'comment' => $school_inclusiveness->comment,
							'comment' => (!empty($school_inclusiveness->comment) ? $school_inclusiveness->comment : 'No Comment'),
							'year' => $year,
							'date' => $date, 
							'country_id' => 1, 
							'region_id' => $region_id, 
							'district_id' => $district_id, 
							'circuit_id' => $circuit_id, 
							'school_id' => $school_id,
							'lat' => $latitude,
							'long' => $longitude
						);

						SchoolInclusiveAnswer::create($fields);
				}


				Log::info('SchoolInclusive total: ', array('context' => $current_school_inclusiveness_level));


				$current_school_inclusiveness_level = ($current_school_inclusiveness_level / 2);
				
				$school->current_state_of_school_inclusiveness = $current_school_inclusiveness_level;
				
				$school->save();


			}


			

		}



?>