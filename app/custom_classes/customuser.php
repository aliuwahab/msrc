<?php

class CustomUser {

	public static function loginUserData($user_level,$user_level_id,$user_id)
	{
		switch ($user_level) {
			case 'national':

		        if( Cache::has('national_level') ) {
					$data = Cache::get('national_level');
					return $data;
				}
				//retrieving a whole national data if the user is a national admin
				$data = Country::with('regions.districts.circuits.schools')->where('id', $user_level_id)->first();

		        Cache::put( 'national_level', $data,15);

		        return $data;

				break;
			case 'regional':
				//retrieving a whole national data if the user is a national admin
				return $data = Region::with('districts.circuits.schools')->where('id', $user_level_id)->first();

				break;
			case 'district':
				//retrieving a whole national data if the user is a national admin
				return $data = District::with('circuits.schools')->where('id', $user_level_id)->first();
				break;
			case 'circuit':
				//retrieving a whole national data if the user is a national admin
				return $data = Circuit::with('schools')->where('id', $user_level_id)->first();
				break;
			case 'circuit':
				//retrieving a whole national data if the user is a national admin
				return $data = School::with('schools.answers')->where('id', $user_level_id)->first();
				break;
			default:
				# code...
				break;
		}
		
	}

	public static function allAdmin($level,$level_id)
	{
		

		switch ($level) {
			case 'national':

				if( Cache::has('all_admin') ) {

					$alladministrators = Cache::get('all_admin');

					return $alladministrators;
				}

				//retrieving a whole national data if the user is a national admin
				$alladministrators = User::where('id', "!=",Auth::user()->id)->get();

		        Cache::put( 'all_admin', $alladministrators,15);

		        return $alladministrators;
		        
				break;

			default:
				return $administrators_at_this_level = DB::table('users')->where("level", $level)
														 ->where('level_id', $level_id)
														 ->where('id', "!=",Auth::user()->id)->get();
				break;
		}
	}

}