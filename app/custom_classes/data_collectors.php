<?php

class DataCollectorsCustomClass {


	public static function allDataCollectors($country)
	{
		return $data_collectors = Country::with('data_collectors')->where('id', $country)->first();
	}




	public static function allSupervisorsOrallHeadteachers($country=1, $type)
	{
		return $data_collector_type = DB::table('data_collectors')->where("country_id", $country)->where('type', $type)->get();
	}

	public static function allDataCollectorsForARegion($region)
	{
		return $data_collectors = Region::with('data_collectors')->where('id', $region)->first();
	}

	public static function allRegionalSupervisorsOrHeadteachers($region,$type)
	{
		return $data_collector_type = DB::table('data_collectors')->where("region_id", $region)->where('type', $type)->get();
	}

	public static function allDataCollectorsForADistrict($district)
	{
		return $data_collectors = District::with('data_collectors')->where('id', $district)->first();
	}

	public static function allDistrictSupervisorsOrHeadteachers($district,$type)
	{
		return $data_collector_type = DB::table('data_collectors')->where("district_id", $district)->where('type', $type)->get();
	}

	public static function allDataCollectorsForACircuit($circuit)
	{
		return $data_collectors = Circuit::with('data_collectors')->where('id', $circuit)->first();
	}

	public static function allCircuitSupervisorsOrHeadteachers($circuit,$type)
	{
		return $data_collector_type = DB::table('data_collectors')->where("circuit_id", $circuit)->where('type', $type)->get();
	}

	public static function allDataCollectorsForASchool($school)
	{
		return $data_collector_type = DB::table('data_collectors')->where("school_id", $school)->get();
	}

	public static function schoolSupervisorOrHeadteacher($school,$type)
	{
		return $data_collector_type = DB::table('data_collectors')->where("school_id", $school)->where('type', $type)->get();
	}

}