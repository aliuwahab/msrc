<?php
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: gbeilaaliuwahab
 * Date: 6/8/16
 * Time: 3:38 PM
 */
class DataAnalysis
{


    public static function percentageAnalysisOfTeacherAttendance($queryString, $values)
    {


        $punctuality_and_lessons_query = $users = PuntualityAndLessons::whereRaw($queryString, $values)->get();

        $number_of_query_results = count($punctuality_and_lessons_query);


        if ($number_of_query_results > 0) {

            $first_object_of_query = $punctuality_and_lessons_query->first();

            $total_number_of_days_school_was_in_succession = $punctuality_and_lessons_query->sum("days_in_session");

            $number_of_days_school_was_in_session = $total_number_of_days_school_was_in_succession/$number_of_query_results;


            $week_number = $first_object_of_query->week_number;

            $total_days_present = $punctuality_and_lessons_query->sum("days_present");



            $percentage_teacher_attendance = number_format(($total_days_present/($number_of_days_school_was_in_session * $number_of_query_results)) * 100, 2);


            return Response::json(array("status"=>200,
                                         "message" => "Average teacher attendance percentage",
                                        "data"=> array('week_number' => $week_number, 'teacher_attendance_in_percentage' => $percentage_teacher_attendance)));


        }


        return Response::json(array("status"=>401, "message" => "No teachers attendance information submitted on the set period"));

    }






    public static function findTeachersWhosAttendanceHasNotBeenSubmitted($inputs = array ())
    {

        //return array_keys($inputs) of the posted data;
        $values = array_values($inputs);

        //declaring a query string to query with in the whereRaw method
        $queryString = '';

        //puting the key into the query parameters and appending the word 'and';
        foreach ($inputs as $key => $value) {
            $queryString .= $key.' = ? and ';
        }
        //removing the last and which will be appended
        $queryString = substr($queryString, 0, -4);


        $all_teachers_of_the_school = DB::table('teachers')->where('school_id', $inputs["school_id"])->get();


        $teachers_attendance_submitted_for_a_week = DB::table('punctuality_and_lessons')->whereRaw($queryString, $values)->get();


        return Response::json(array("status"=>200,
                                    "message" => "Teachers and those whose attendance has been reported for the specified week",
                                    "data"=> array('school_teachers' => $all_teachers_of_the_school, 'attendance_reported' => $teachers_attendance_submitted_for_a_week)));



    }






    public static function toReCalculateStudentEnrolmentTotalsForAWeek($enrolment_object = array())
    {


        foreach ($enrolment_object as $weekly_enrolment_totals_for_school) {

            $update_weekly_totals = DB::table('weekly_totals')->where('school_id', $weekly_enrolment_totals_for_school["school_id"])
                ->where('week_number', $weekly_enrolment_totals_for_school["week_number"])
                ->where('term', $weekly_enrolment_totals_for_school["term"])
                ->where('year', $weekly_enrolment_totals_for_school["year"])
                ->update($weekly_enrolment_totals_for_school);
        }


        return Response::json(array("status"=>200, "message" => "Re-calculated weekly totals for student enrolment for a school saved successfully"));



    }



    public static function calculateStudentEnrolmentTotalForAWeekGivenWeekNumber($week_number)
    {




    }





    public static function calculateStudentAttendanceTotalForAWeekGivenWeekNumber($week_number)
    {


    }




    public static function calculateTeacherAttendanceTotalForAWeekGivenWeekNumber($week_number)
    {
        

    }






    public static function toReCalculateStudentAttendanceTotalsForAWeek($attendance_object = array())
    {



        foreach ($attendance_object as $weekly_attendance_totals_for_a_school) {

            $update_weekly_totals = DB::table('weekly_totals')->where('school_id', $weekly_attendance_totals_for_a_school["school_id"])
                ->where('week_number', $weekly_attendance_totals_for_a_school["week_number"])
                ->where('term', $weekly_attendance_totals_for_a_school["term"])
                ->where('year', $weekly_attendance_totals_for_a_school["year"])
                ->update($weekly_attendance_totals_for_a_school);

        }



        return Response::json(array("status"=>200, "message" => "Re-calculated weekly totals for student attendance for a school saved successfully"));





    }






    public static function toReCalculateTeacherAttendanceTotalsForAWeek($teacher_attendance_object = array())
    {


    //This recalculate teacher attendance for each week and save it
        foreach ($teacher_attendance_object as $teacher_attendance_weekly_totals_for_a_school) {

            $update_weekly_totals = DB::table('weekly_totals')->where('school_id', $teacher_attendance_weekly_totals_for_a_school["school_id"])
                ->where('week_number', $teacher_attendance_weekly_totals_for_a_school["week_number"])
                ->where('term', $teacher_attendance_weekly_totals_for_a_school["term"])
                ->where('year', $teacher_attendance_weekly_totals_for_a_school["year"])
                ->update($teacher_attendance_weekly_totals_for_a_school);


            if ($update_weekly_totals) {


            }else{



            }


        }


        //This return a json object with the details attached
        return Response::json(array("status"=>200, "message" => "Re-calculated weekly totals for student attendance for a school saved successfully"));



    }








    public static function calculateStudentEnrolmentTotalForASchoolInWeekGivenWeekNumber($school_id,$week_number,$term,$year)
    {
        $total_normal_student_males = 0;

        $total_normal_student_girls = 0;

        $total_special_student_males = 0;

        $total_special_student_girls = 0;


        $find_all_normal_students_enrolment_submitted_for_the_week = DB::table('school_enrolments')->where('school_id',$school_id)
            ->where('week_number', $week_number) ->where('term', $term)->where('year', $year)->get();

        $find_all_special_students_enrolment_submitted_for_the_week = DB::table('special_enrolment')->where('school_id',$school_id)
            ->where('week_number', $week_number) ->where('term', $term)->where('year', $year)->get();


        if (count($find_all_normal_students_enrolment_submitted_for_the_week) > 0) {

            foreach ($find_all_normal_students_enrolment_submitted_for_the_week as $normal_students_class_enrolment) {

                $total_normal_student_males +=  $normal_students_class_enrolment->num_males;
                $total_normal_student_girls +=  $normal_students_class_enrolment->num_females;


            }


        }else{


        }



        if (count($find_all_special_students_enrolment_submitted_for_the_week) > 0) {

            foreach ($find_all_special_students_enrolment_submitted_for_the_week as $special_students_class_enrolment) {

                $total_special_student_males +=  $special_students_class_enrolment->num_males;
                $total_special_student_girls +=  $special_students_class_enrolment->num_females;

            }


        }else{


        }


        $selected_week_totals_if_it_exist  = DB::table('weekly_totals')->where('school_id',$school_id)
            ->where('week_number', $week_number)->where('term', $term)->where('year', $year)->first();


        if (count($selected_week_totals_if_it_exist) > 0) {

            $update_weekly_totals = DB::table('weekly_totals')->where('school_id', $school_id)
                ->where('week_number', $week_number)
                ->where('term', $term)
                ->where('year', $year)
                ->update(array('normal_enrolment_total_boys' => $total_normal_student_males,
                    'normal_enrolment_total_girls' => $total_normal_student_girls,
                    'normal_enrolment_total_students' => $total_normal_student_males + $total_normal_student_girls,
                    'special_enrolment_total_boys' => $total_special_student_males,
                    'special_enrolment_total_girls' => $total_special_student_girls,
                    'special_enrolment_total_students' => $total_special_student_males + $total_special_student_girls));


        }else{



        }



    }










    public static function calculateStudentAttendanceTotalForASchoolInWeekGivenWeekNumber($week_number)
    {

    }



    public static function calculateTeacherAttendanceTotalForASchoolWeekGivenWeekNumber($week_number)
    {



    }





    public static function createWeeklyTotals()
    {


    }










    public static function UpdateDistrictWeeklyTotalsByDeletingExisting($year, $term, $school_id)
    {

        $week_numbers = DB::table('school_enrolments')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->lists('week_number');

        foreach ($week_numbers as $week_number) {

            Log::info('WEEK NUMBER', array('context' => $week_number));

            $normal_enrolment_total_boys = DB::table('school_enrolments')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_males");
            $normal_enrolment_total_girls = DB::table('school_enrolments')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_females");
            $normal_attendance_total_boys = DB::table('student_attendance')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_males");
            $normal_attendance_total_girls = DB::table('student_attendance')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_females");

            $special_enrolment_total_boys = DB::table('school_enrolments')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_males");
            $special_enrolment_total_girls = DB::table('school_enrolments')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_females");
            $special_attendance_total_boys = DB::table('school_enrolments')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_males");
            $special_attendance_total_girls = DB::table('school_enrolments')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number',$week_number)->where('data_collector_type', 'head_teacher')->sum("num_females");

            $total_teachers_attendance = DB::table('punctuality_and_lessons')->where('school_id', $school_id)->where('term', $term)->where('year', $year)->where('week_number', $week_number)->where('data_collector_type', 'head_teacher')->sum('days_present');

            $current_number_of_teachers = DB::table('weekly_totals')->where('week_number', $week_number)->where('term', $term)->where('year', $year)->pluck('current_number_of_teachers');

            if ($current_number_of_teachers == 0.0) {
                $current_number_of_teachers = 1;
            }
            $average_teacher_attendance_in_the_week = $total_teachers_attendance / $current_number_of_teachers;
            $weekly_totals = array(
                'normal_enrolment_total_boys' => $normal_enrolment_total_boys,
                'normal_enrolment_total_girls' => $normal_enrolment_total_girls,
                'normal_enrolment_total_students' => $normal_enrolment_total_boys + $normal_enrolment_total_girls,
                'normal_attendance_total_boys' => $normal_attendance_total_boys,
                'normal_attendance_total_girls' => $normal_attendance_total_girls,
                'normal_attendance_total_students' => $normal_attendance_total_boys + $normal_attendance_total_girls,
                'special_enrolment_total_boys' => $special_enrolment_total_boys,
                'special_enrolment_total_girls' => $special_enrolment_total_girls,
                'special_enrolment_total_students' => $special_enrolment_total_boys + $special_enrolment_total_girls,
                'special_attendance_total_boys' => $special_attendance_total_boys,
                'special_attendance_total_girls' => $special_attendance_total_girls,
                'special_attendance_total_students' => $special_attendance_total_boys + $special_attendance_total_girls,
                'average_teacher_attendance' => $average_teacher_attendance_in_the_week,
                'number_of_days_school_was_in_session' => 1,
            );



            Log::info('UPDATED VALUES', array('context' => $weekly_totals));

            $update_weekly_totals = DB::table('weekly_totals')->where('school_id', $school_id)
                ->where('week_number', $week_number)
                ->where('term', $term)
                ->where('year', $year)
                ->update($weekly_totals);

        }

    }





    public static function UpdateDistrictTermlyTotalsByDeletingExisting($district_id, $term, $year)
    {


    }









}