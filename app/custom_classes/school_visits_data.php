<?php
use Illuminate\Support\Facades\DB;

/**
	* This is for retieving and analysing school visits data
	*/
	class SchoolVisitsAnswersRetrieval
	{
		
		
		public static function ReceivesSchoolVisitsDataSubmittedByAndroidApp($schoolVisitsData)
		{
			   $category = $schoolVisitsData->category;
				//accessing the school object parts of the data posted By Android App
				$school = $schoolVisitsData->school;
				//accesing the various parts of the school details posted
				$school_id = $school->school_id;
				$phone_number = $school->phone_number;
				$school_code = $school->school_code;
				//$school_name = $school->school_name;
				$region_id = $school->region_id;
				$district_id = $school->district_id;
				$circuit_id = $school->circuit_id;
				$date = date('d - m - Y');
				//$year = date('Y');
				$year = date('Y');
				$term = $school->term;
				//$term = 'third_term';
				//$headteacher_id = $school->headteacher_id;

				//$itenary_id = 1;
				$itenary_id = $school->itenary_id;

				//accessing the data_collector property
				$data_collector = $schoolVisitsData->data_collector;
				//Log::info('Under school review data the data_collector', array('context' => $data_collector));

				//accesing the various properties in the data collector
				$data_collector_id = $data_collector->id;
				$data_collector_type = $data_collector->type;
				//$data_collector_first_name = $data_collector->first_name;
				//$data_collector_last_name = $data_collector->last_name;
				$data_collector_identity_code = $data_collector->identity_code;

				$lat = $data_collector->lat;
				$long = $data_collector->long;
				$location_of_submission = $data_collector->long.'-'.$data_collector->lat; 
				

				//accessing the various parts of the data posted By Android App

			switch ($category) {
				case "teacher_attendance":
					$stateOfTeacherAttendance = $schoolVisitsData->schoolVisit->teacherAnswers;
					if (!empty($stateOfTeacherAttendance)) {
						$saveASectionOfSchoolVisitisData = self::saveStateOfTeacherAttendanceData($data_collector_type,$stateOfTeacherAttendance,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long);
					}else{
						return Response::json(array("status"=>401,
							"message"=>"There exist no data on the selected part"));
					}
					break;
				case "student_attendance":
					$stateOfStudentAttendance= $schoolVisitsData->schoolVisit->studentAttendanceAnswers;
					if (!empty($stateOfStudentAttendance)) {
						$saveASectionOfSchoolVisitisData = self::saveStateOfStudentsAttendanceData($data_collector_type,$stateOfStudentAttendance,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long);
					}else{
						return Response::json(array("status"=>401,
							"message"=>"There exist no data on the selected part"));
					}
					break;
				case "lesson_plans":
					$stateOfLessonPlans  = $schoolVisitsData->schoolVisit->lessonAnswers;
					if (!empty($stateOfLessonPlans)) {
						$saveASectionOfSchoolVisitisData = self::saveStateOfLessonsPlansData($data_collector_type,$stateOfLessonPlans,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long);
					}else{
						return Response::json(array("status"=>401,
							"message"=>"There exist no data on the selected part"));
					}
					break;
				case "records_and_performance":
					$stateOfSchoolRecordsAndPerformance = $schoolVisitsData->schoolVisit->schoolRecordAndPerformanceAnswers;
					if (!empty($stateOfSchoolRecordsAndPerformance)) {
						$saveASectionOfSchoolVisitisData = self::saveStateOfSchoolRecordsAndPerformance($data_collector_type,$stateOfSchoolRecordsAndPerformance,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long);
					}else{
						return Response::json(array("status"=>401,
							"message"=>"There exist no data on the selected part"));
					}
					break;
				case "facilities_and_safety":
					$stateOfFacilitiesAndSafety= $schoolVisitsData->schoolVisit->facilitiesAndSafetyAnswers;
					if (!empty($stateOfFacilitiesAndSafety)) {
						$saveASectionOfSchoolVisitisData = self::saveStateOfFacilitiesAndSafety($data_collector_type,$stateOfFacilitiesAndSafety,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long);
					}else{
						return Response::json(array("status"=>401,
							"message"=>"There exist no data on the selected part"));
					}
					break;
				case "teaching_and_learning_material":
					$stateOfTeachingAndLearningMaterials  = $schoolVisitsData->schoolVisit->teachingAndLeaningMaterialAnswers;
					if (!empty($stateOfTeachingAndLearningMaterials)) {
						$saveASectionOfSchoolVisitisData = self::saveStateOfTeachingAndLearningMaterials($data_collector_type,$stateOfTeachingAndLearningMaterials,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long);
					}else{
						return Response::json(array("status"=>401,
							"message"=>"There exist no data on the selected part"));
					}
					break;
				case "community_involvement":
					$stateOfCommunityInvlovement = $schoolVisitsData->schoolVisit->communityInvolvementAnswer;
					if (!empty($stateOfCommunityInvlovement)) {
						$saveASectionOfSchoolVisitisData = self::saveStateOfCommunityInvlovement($data_collector_type,$stateOfCommunityInvlovement,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long);
					}else{
						return Response::json(array("status"=>401,
							"message"=>"There exist no data on the selected part"));
					}
					break;

				default:

					return Response::json(array("status"=>401,
						"message"=>"Please there is no an appropriate category indicated"));

			}


					//using push api available at www.pusher.com to send new data changes info to client side
					$data_type_submitted = 'schoolvisits';
					$schoolIDAndDataType = array('school_id' => $school_id, 'type' => $data_type_submitted );
					NotificationsController::recieveAndSendServerSideDatabaseChangesToClientSide($schoolIDAndDataType['school_id'],$schoolIDAndDataType['type']);

					//After the last section is submitted
					if ($saveASectionOfSchoolVisitisData) {
						//data submitted table
						$fields = array(
							'data_collector_id' => $data_collector_id,
							'data_type_submitted' => 'schoolvisits',
							'year' => $year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id);

						RecentDataSubmitted::create($fields);

						$fields = array(
							'data_collector_type' => $data_collector_type,
							'data_collector_id' => $data_collector_id,
							'date' => $date,
							'itinerary_id' => $itenary_id,
							'term' => $term,
							'year' => $year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id);
						//saving submission status
						$saving = SchoolVisitisSubmissionStatus::create($fields);

						if ($saving) {

							return Response::json(array("status"=>200,
								"message"=>"Data Submitted"));

						}else{

							return Response::json(array("status"=>401,
								"message"=>"Failed Submission"));
						}


					}else{

						return Response::json(array("status"=>200,
							"message"=>"Data Submitted but not the last section"));
					}



		}





		public static function saveStateOfTeacherAttendanceData($data_collector_type,$stateTeacherAttendance,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long)
		{


			foreach ($stateTeacherAttendance as $attendance) {

				if (!is_null($attendance->answer) && !is_null($attendance->question_code)) {

					//checking to see whether data is already is submitted on this question under this itinerary for a school
					$checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary = DB::table('answers')
						->where('itenary_id',$itenary_id)->where('question_code_reference',$attendance->question_code)
						->where('data_collector_type',$data_collector_type)->where('school_id',$school_id)->first();

					if (count($checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary) < 1) {

						$fields = array(
							'itenary_id' => $itenary_id,
							'school_code_reference' => $school_code,
							'question_code_reference' =>  $attendance->question_code,
							'answer' => $attendance->answer,
							'location_of_submission' => $location_of_submission,
							'comment' => $attendance->comment,
							'question_type' => $attendance->question_type,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'lat' => $lat,
							'long' => $long,
							'data_collector_type' => $data_collector_type
						);

						$save = Answer::create($fields);

					}

				}


			}



		}






		public static function saveStateOfStudentsAttendanceData($data_collector_type,$stateStudentAttendance,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long)
		{
			foreach ($stateStudentAttendance as $attendance) {

					if (!is_null($attendance->answer) && !is_null($attendance->question_code)) {

						//checking to see whether data is already is submitted on this question under this itinerary for a school
						$checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary = DB::table('answers')
							->where('itenary_id',$itenary_id)->where('question_code_reference',$attendance->question_code)
							->where('data_collector_type',$data_collector_type)->where('school_id',$school_id)->first();

						if (count($checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary) < 1) {

							$fields = array(
								'itenary_id' => $itenary_id,
								'school_code_reference' => $school_code,
								'question_code_reference' => $attendance->question_code,
								'answer' => $attendance->answer,
								'location_of_submission' => $location_of_submission,
								'comment' => $attendance->comment,
								'question_type' => $attendance->question_type,
								'country_id' => 1,
								'region_id' => $region_id,
								'district_id' => $district_id,
								'circuit_id' => $circuit_id,
								'school_id' => $school_id,
								'lat' => $lat,
								'long' => $long,
								'data_collector_type' => $data_collector_type
							);

							$save = Answer::create($fields);

						}


					}
							
				}
		}





		public static function saveStateOfLessonsPlansData($data_collector_type,$stateOfLessonPlans,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long)
		{
			foreach ($stateOfLessonPlans as $lessonplans) {
								//checking to see if there is answer provided
							if (!is_null($lessonplans->answer) && !is_null($lessonplans->question_code)) {

								//checking to see whether data is already is submitted on this question under this itinerary for a school
								$checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary = DB::table('answers')
									->where('itenary_id',$itenary_id)->where('question_code_reference',$lessonplans->question_code)
									->where('data_collector_type',$data_collector_type)->where('school_id',$school_id)->first();

								if (count($checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary) < 1) {

									$fields = array(
										'itenary_id' => $itenary_id,
										'school_code_reference' => $school_code,
										'question_code_reference' => $lessonplans->question_code,
										'answer' => $lessonplans->answer,
										'location_of_submission' => $location_of_submission,
										'comment' => $lessonplans->comment,
										'question_type' => $lessonplans->question_type,
										'country_id' => 1,
										'region_id' => $region_id,
										'district_id' => $district_id,
										'circuit_id' => $circuit_id,
										'school_id' => $school_id,
										'lat' => $lat,
										'long' => $long,
										'data_collector_type' => $data_collector_type
									);

									$save = Answer::create($fields);
								}


							}
						
				}
		}




		public static function saveStateOfSchoolRecordsAndPerformance($data_collector_type,$stateOfSchoolRecordsAndPerformance,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long)
		{

			foreach ($stateOfSchoolRecordsAndPerformance as $school_records) {
						if (!is_null($school_records->answer) && !is_null($school_records->question_code)) {

							//checking to see whether data is already is submitted on this question under this itinerary for a school
							$checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary = DB::table('answers')
								->where('itenary_id',$itenary_id)->where('question_code_reference',$school_records->question_code)
								->where('data_collector_type',$data_collector_type)->where('school_id',$school_id)->first();


							if (count($checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary) < 1) {

								$fields = array(
									'itenary_id' => $itenary_id,
									'school_code_reference' => $school_code,
									'question_code_reference' => $school_records->question_code,
									'answer' => $school_records->answer,
									'location_of_submission' => $location_of_submission,
									'comment' => $school_records->comment,
									'question_type' => $school_records->question_type,
									'country_id' => 1,
									'region_id' => $region_id,
									'district_id' => $district_id,
									'circuit_id' => $circuit_id,
									'school_id' => $school_id,
									'lat' => $lat,
									'long' => $long,
									'data_collector_type' => $data_collector_type
								);

								$save = Answer::create($fields);

							}
					}
						
				}
		}





		public static function saveStateOfFacilitiesAndSafety($data_collector_type,$stateOfFacilitiesAndSafety,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long)
		{
				foreach ($stateOfFacilitiesAndSafety as $SafetyAndfacilities) {

						if (!is_null($SafetyAndfacilities->answer) && !is_null($SafetyAndfacilities->question_code)) {

							//checking to see whether data is already is submitted on this question under this itinerary for a school
							$checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary = DB::table('answers')
								->where('itenary_id',$itenary_id)->where('question_code_reference',$SafetyAndfacilities->question_code)
								->where('data_collector_type',$data_collector_type)->where('school_id',$school_id)->first();


							if (count($checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary) < 1) {


								$fields = array(
									'itenary_id' => $itenary_id,
									'school_code_reference' => $school_code,
									'question_code_reference' => $SafetyAndfacilities->question_code,
									'answer' => $SafetyAndfacilities->answer,
									'location_of_submission' => $location_of_submission,
									'comment' => $SafetyAndfacilities->comment,
									'question_type' => $SafetyAndfacilities->question_type,
									'country_id' => 1,
									'region_id' => $region_id,
									'district_id' => $district_id,
									'circuit_id' => $circuit_id,
									'school_id' => $school_id,
									'lat' => $lat,
									'long' => $long,
									'data_collector_type' => $data_collector_type
								);

								$save = Answer::create($fields);


							}


						}
							
				}

		}



		public static function saveStateOfTeachingAndLearningMaterials($data_collector_type,$stateOfTeachingAndLearningMaterials,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long)
		{



			foreach ($stateOfTeachingAndLearningMaterials as $teachingMaterials) {


							if (!is_null($teachingMaterials->answer) && !is_null($teachingMaterials->question_code)) {

								//checking to see whether data is already is submitted on this question under this itinerary for a school
								$checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary = DB::table('answers')
									->where('itenary_id',$itenary_id)->where('question_code_reference',$teachingMaterials->question_code)
									->where('data_collector_type',$data_collector_type)->where('school_id',$school_id)->first();


								if (count($checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary) < 1) {

									$fields = array(
										'itenary_id' => $itenary_id,
										'school_code_reference' => $school_code,
										'question_code_reference' =>  $teachingMaterials->question_code,
										'answer' => $teachingMaterials->answer,
										'location_of_submission' => $location_of_submission,
										'comment' => $teachingMaterials->comment,
										'question_type' => $teachingMaterials->question_type,
										'country_id' => 1,
										'region_id' => $region_id,
										'district_id' => $district_id,
										'circuit_id' => $circuit_id,
										'school_id' => $school_id,
										'lat' => $lat,
										'long' => $long,
										'data_collector_type' => $data_collector_type
									);

									$save = Answer::create($fields);



								}

							}
						
				}

		}



		public static function saveStateOfCommunityInvlovement($data_collector_type,$stateOfCommunityInvlovement,$itenary_id,$school_code,$location_of_submission,$region_id,$district_id,$circuit_id,$school_id,$lat,$long)
		{
				foreach ($stateOfCommunityInvlovement as $community_involvement) {

						if (!is_null($community_involvement->answer) && !is_null($community_involvement->question_code)) {
							# code...

							//checking to see whether data is already is submitted on this question under this itinerary for a school
							$checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary = DB::table('answers')
								->where('itenary_id',$itenary_id)->where('question_code_reference',$community_involvement->question_code)
								->where('data_collector_type',$data_collector_type)->where('school_id',$school_id)->first();

							if (count($checkingToSeeWhetherDataIsSubmittedOnASchoolItinerary) < 1) {

								$fields = array(
									'itenary_id' => $itenary_id,
									'school_code_reference' => $school_code,
									'question_code_reference' =>  $community_involvement->question_code,
									'answer' => $community_involvement->answer,
									'location_of_submission' => $location_of_submission,
									'comment' => $community_involvement->comment,
									'question_type' => $community_involvement->question_type,
									'country_id' => 1,
									'region_id' => $region_id,
									'district_id' => $district_id,
									'circuit_id' => $circuit_id,
									'school_id' => $school_id,
									'lat' => $lat,
									'long' => $long,
									'data_collector_type' => $data_collector_type
								);

								$save = Answer::create($fields);

							}


						}
							
				}
		}





		//this function retrieves data on school visits for dashboard (server request)
		public static function retrieveSchoolVisitsData($queryString, $values)
		{
				//$question_category_reference = 'schoolvisits';
				//try {
				       //Querying The Questions Category and adding enrolment data
		    			return $schoolInclusiveness = SchoolVisit::with(array('school_visit_answers' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();

						}))->get();
						//->where('question_category_reference', $question_category_reference)->get();
		      			

		}



		
	}


?>