<?php 

		/**
		* This class is for sending general public requested information on schools
		* And Also for receiving and saving reports made by general public on schools
		*/
		class CommunitySMSReportsAndInfomationRequest
		{
			
			
				public static function checkWhatSenderWantToDo($purpose,$sender,$when,$message)
				{
					// comparing the purpose of the sender to find out whether is for reporting
					// or the sender whats information on a particular school
					$similar_to_reporting = similar_text('report', strtolower($purpose), $report_percent);
					$similar_to_getting_info = similar_text('info', strtolower($purpose), $info_percent);
					round($report_percent);
					round($info_percent);

					if ($report_percent > 50) {

						//exploding the message base on space
						$getSchoolCodeAndMessage = explode(' ', $message,3);
			
						try {

						//retrieving the school and the message
						$schoolCode = $getSchoolCodeAndMessage[1];
						//retrieving the school and the message
						$message = $getSchoolCodeAndMessage[2];

						return self::recordIssueReportedByPublic($sender,$when, $schoolCode,$message);


						} catch (Exception $e) {

							$message = 'Message not understood, leave a space between "report" the school code, another space, then message';

							//the data been passed into the queue to be use by the function it calls
								$smsData = array("message"=>$message, 
												 "number"=>'+'.$sender);

								// Queue::push(function($job) use($smsData){
								// 					SMS::sendSMS($smsData['message'],$smsData['number']);
								// 					$job->delete();
								// });

								return SMS::sendSMS($smsData['message'],$smsData['number']);


						}
						
						


					}elseif($info_percent > 50){

						//exploding the message base on space
						$getSchoolCode = explode(' ', $message,2);
						try {

							//retrieving the school code user sender wants information on
							$schoolCode = $getSchoolCode[1];
							$requester = $sender;

							return self::informationOnASchool($requester,$schoolCode);
							
						} catch (Exception $e) {
							
								$message = 'School code not understood, leave a space between "info" and school code';

								$smsData = array(
												 "message"=>$message, 
												 "number"=>'+'.$sender);

									// Queue::push(function($job) use($smsData){
									// 					SMS::sendSMS($smsData['message'],$smsData['number']);
									// 					$job->delete();
									// });
								return SMS::sendSMS($smsData['message'],$smsData['number']);
						}

					}else{

						  $message = 'Purpose not known, prepend message with "report" or "info" if you are reporting or getting an info about a school respectively';

							$smsData = array("message"=>$message, 
												 "number"=>'+'.$sender);

								// Queue::push(function($job) use($smsData){
								// 					SMS::sendSMS($smsData['message'],$smsData['number']);
								// 					$job->delete();
								// });
							return SMS::sendSMS($smsData['message'],$smsData['number']);

					}


				}


				public static function recordIssueReportedByPublic($sender,$when, $schoolCode,$message)
				{
					$schoolDetails = DB::table('schools')->where('sms_code',$schoolCode)->first();

					// print_r($schoolDetails);
					// exit();
					
					if ($schoolDetails) {
					//putting the message details in order to save
					$fields = array(
							'sender' => $sender, 
							'message' => $message,
							'school_sms_code' => $schoolCode,
							'country_id' => $schoolDetails->country_id,
							'region_id' => $schoolDetails->region_id,
							'district_id' => $schoolDetails->district_id,
							'circuit_id' => $schoolDetails->circuit_id,
							'school_id' => $schoolDetails->id,
							);

					CommunitySms::create($fields);

					$message = "Thank you for reporting, we appreciate and we've taken note.";

							//the data been passed into the queue to be use by the function it calls
								$smsData = array("message"=>$message, 
												 "number"=>'+'.$sender);

								// Queue::push(function($job) use($smsData){
								// 					SMS::sendSMS($smsData['message'],$smsData['number']);
								// 					$job->delete();
								// });

						return SMS::sendSMS($smsData['message'],$smsData['number']);



					}else{

						$fields = array(
							'sender' => $sender, 
							'message' => $message,
							'school_sms_code' => 'empty',
							'country_id' => 0,
							'region_id' => 0,
							'district_id' => 0,
							'circuit_id' => 0,
							'school_id' =>0,
							);

						return CommunitySms::create($fields);

					}
				}


				public static function informationOnASchool($requester,$schoolCode)
				{
					
						
					$schoolCurrentPerformance = DB::table('schools')->where('sms_code',$schoolCode)->first();

					$schoolName = $schoolCurrentPerformance->name;
					$community_involvement_current_state = $schoolCurrentPerformance->current_state_of_community_involvement;
					$teaching_learning_current_state = $schoolCurrentPerformance->current_state_of_teaching_and_learning;
					$friendliness_current_state = $schoolCurrentPerformance->current_state_of_friendliness_to_boys_girls;
					$health_current_state = $schoolCurrentPerformance->current_state_of_healthy_level;
					$safety_protection_current_state = $schoolCurrentPerformance->current_state_of_safety_and_protection;
					$inclusiveness_current_state = $schoolCurrentPerformance->current_state_of_school_inclusiveness;

					$totalScore =  $community_involvement_current_state + $teaching_learning_current_state + $friendliness_current_state + $health_current_state + $safety_protection_current_state + $inclusiveness_current_state;
					$totalPercentage = ( $totalScore/60)*100;
					$totalPercentage = round($totalPercentage, 2);

					$message = "Out of 10, ".$schoolName." scores:".
												"\nHealth-".$health_current_state.
												"\nCommunity Involvement-".$community_involvement_current_state.
												"\nFriendliness to girls-".$friendliness_current_state.
												"\nSafeness-".$safety_protection_current_state.
												"\nInclusiveness-".$inclusiveness_current_state.
												"\nTeaching-".$teaching_learning_current_state.
												"\nThe overall score is: ".$totalPercentage."%";


					//sms data works
					$smsData = array("message"=> $message, 
									 "number"=>'+'.$requester);

					Queue::push(function($job) use($smsData){
										SMS::sendSMS($smsData['message'],$smsData['number']);
										$job->delete();
					});

					//return SMS::sendSMS($smsData['message'],$smsData['number']);
				}





				public static function retrievingSMSBaseOnCoverage($queryString, $values)
				{
					try {
							
						//querying base on the parameters posted which is contain in the $querystring variable
						 return DB::table('community_sms')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
				}
		}


?>