<?php 
		
		/**
		* This is for retrieving data on school problems under school report card
		*/
		class SchoolProlemsSectionUnderSchoolReportCard
		{
			public static function stateOfSchoolSanitation($queryString,$values)
			{
					try {
							//querying base on the parameters posted which is contain in the $querystring variable
						    return $schoolSanitationData = DB::table('sanitation')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
			}


			public static function stateOfSchoolSecurity($queryString,$values)
			{
					try {
							//querying base on the parameters posted which is contain in the $querystring variable
						    return $schoolSanitationData = DB::table('security')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
			}



		public static function stateOfSchoolRecreationalEquipment($queryString,$values)
			{
					try {
							//querying base on the parameters posted which is contain in the $querystring variable
						    return $schoolSanitationData = DB::table('equipment_recreational')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
			}


		public static function stateOfSchoolStructure($queryString,$values)
			{
					try {
							//querying base on the parameters posted which is contain in the $querystring variable
						    return $schoolSanitationData = DB::table('school_structure')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
			}


		public static function stateOfSchoolFurniture($queryString,$values)
			{
					try {
							//querying base on the parameters posted which is contain in the $querystring variable
						    return $schoolSanitationData = DB::table('funiture')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
			}



		public static function stateOfSchoolRecordsAndPerformance($queryString,$values)
			{
					try {
							//querying base on the parameters posted which is contain in the $querystring variable
						    return $schoolSanitationData = DB::table('school_records_performance')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
			}


		public static function stateOfSchoolAndCommunityRelations($queryString,$values)
			{
					try {
							//querying base on the parameters posted which is contain in the $querystring variable
						    return $schoolSanitationData = DB::table('school_community_relationship')->whereRaw($queryString, $values)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
			}
			
	}

?>