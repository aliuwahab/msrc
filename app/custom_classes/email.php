<?php 
			/**
			* This class is in chanrge of sending emails
			*/
			class SendEmail{


			
				public static function sendEmailMessge($data,$from,$salutation,$recipientEmails,$pathToFile,$subject)
				{
					

					$recipientEmails = explode(',', $recipientEmails);


					foreach ($recipientEmails as $recipient) {
						// use Mail::send function to send email passing the data and using the $user variable in the closure
						return Mail::send('emails.infosharing.data', $data, function($message) use ($from,$salutation,$recipient,$pathToFile,$subject)
						{
						  $message->from('admin@msrcghana.org', 'mSRC School Monitioring System');
						  $message->to($recipient, $salutation)->subject($subject);
						  
						  if (empty($pathToFile)) {

						  	return 'message sent';
						  }
						  //attaching an email to send
						  $message->attach($pathToFile);
						  return 'message sent';
						});
					}

					
				}
				
				
			}


?>