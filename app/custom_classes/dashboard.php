<?php

class Dashboard {


	public static function allRegions($country = 1)
	{
		
		if(Cache::has('all_regions')) {

			$data = Cache::get('all_regions');

			return $data;
		}
		
		//return $regions = Region::all();
		$data = DB::table('regions')->where('country_id', $country)->get();
		//return $model = Region::where('country_id', '=', $country)->get();

        Cache::put('all_regions', $data, 1440);

        return $data;

	}

	public static function allDistricts($country = 1)
	{

		if(Cache::has('all_districts')) {

			$data = Cache::get('all_districts');

			return $data;
		}
		
		$data = DB::table('districts')->where('country_id', $country)->get();
		
        Cache::put('all_districts', $data, 15);

        return $data;

	}

	public static function allCircuits($country = 1)
	{

		if(Cache::has('all_circuits')) {

			$data = Cache::get('all_circuits');

			return $data;
		}
		
		$data = DB::table('circuits')->where('country_id', $country)->get();
		
        Cache::put('all_circuits', $data, 15);

        return $data;


	}

	public static function allSchools($country = 1)
	{

		if(Cache::has('all_schools')) {

			$data = Cache::get('all_schools');

			return $data;
		}
		
		$data = DB::table('schools')->where('country_id', $country)->get();
		
        Cache::put('all_schools', $data, 15);

        return $data;

	}


	public static function regionInfo($region)
	{
		

		
		//retrieving a whole region data if the user is an admin
		$data = Region::with('districts.circuits.schools')->where('id', $region)->first();
		
        // Cache::put('region_'.$region, $data, 10);

        return $data;

	}



	public static function districtInfo($district)
	{

		
		//retrieving a whole district data if the user is an admin
		$data = District::with('circuits.schools')->where('id', $district)->first();

        // Cache::put('district_'.$district, $data, 3);

        return $data;

	}

	public static function circuitInfo($circuit)
	{


		//retrieving a whole circuit data if the user is an admin
		$data = Circuit::with('schools')->where('id', $circuit)->first();

        // Cache::put('circuit_'.$circuit, $data, 3);

        return $data;

	}

	public static function schoolInfo($school)
	{

		//retrieving a whole school data if the user is an admin
		$data = School::with('schools.answers')->where('id', $school)->first();

        // Cache::put('school_'.$school, $data, 3);

        return $data;

	}


	//this receives query parameters to return the recent data submitted
	//on a school base on the query parameters
	public static function recentDataSubmittedByDataCollectorsOnASchool($queryString, $values)
	{
		try {
				//querying base on the parameters posted which is contain in the $querystring variable
				return $recentdataSubmitted = DB::table('recent_data_submitted')->whereRaw($queryString, $values)->orderBy('id',"desc")->take(10)->get();
	      			
	   	} catch (Exception $e) {

	      			return "Check the fields in your posted data";
	      			
	   }

	   
	}


}