<?php 
	
	/**
	* This retrieves all data on school review for analysis
	*/
	class SchoolReviewAnalysis
	{
		//returns data related to school inclusiveness
		public static function schoolsInclusiveness($question_category_code, $queryString, $values)
		{
			
		      		try {

				       //Querying The Questions Category and adding enrolment data
		    			return $schoolInclusiveness = SchoolReviewQuestion::with(array('inclusiveness_answers' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_reference', $question_category_code)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
		}



		public static function EffectiveTeachingAndLearning($question_category_code, $queryString, $values)
		{
			
		      		try {

				       //Querying The Questions Category and adding enrolment data
		    			return $effectiveTeaching = SchoolReviewQuestion::with(array('effective_teaching_learning' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_reference', $question_category_code)->get();

						//return Response::json(array("effectiveTeaching"=>$effectiveTeaching));
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
		}
		


		public static function HealthyLevelOfSchools($question_category_code, $queryString, $values)
		{
			
		      		try {

				       //Querying The Questions Category and adding enrolment data
		    			return $healthySchools = SchoolReviewQuestion::with(array('healthy_level' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_reference', $question_category_code)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
		}


		public static function FriendlySchools($question_category_code, $queryString, $values)
		{
			
		      		try {

				       //Querying The Questions Category and adding enrolment data
		    			return $friendlySchools = SchoolReviewQuestion::with(array('friendly_schools' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_reference', $question_category_code)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
		}



		public static function CommunityInvolvementInSchool($question_category_code, $queryString, $values)
		{
			
		      		try {

				       //Querying The Questions Category and adding enrolment data
		    			return $communityInvlovement = SchoolReviewQuestion::with(array('community_involvement' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_reference', $question_category_code)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
		}


			
		public static function SafeAndProtectiveAndSchools($question_category_code, $queryString, $values)
		{
			
		      		try {

				       //Querying The Questions Category and adding enrolment data
		    			return $safeAndProtectiveSchools = SchoolReviewQuestion::with(array('safe_protective' => function($query) use($queryString, $values)
						{	
							//querying base on the parameters posted which is contain in the $querystring variable
						    $query->whereRaw($queryString, $values)->get();;

						}))->where('question_category_reference', $question_category_code)->get();
		      			
		      		} catch (Exception $e) {

		      			return "Check the fields in your posted data";
		      			
		      		}
		}
		
	}



?>