<?php
use Illuminate\Support\Facades\DB;

/**
	* This handles the creation of teaachers for school and editing and deleting of teachers
	*/
	class TeacherRegistrationAndEditingAndDeleting
	{
		
		//this registers a new teacher
		public static function ReceivesSchoolReportCardDataSubmittedByAndroidAppOnTeacherRegistration($data)
		{
			// $questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'STI';
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$week_number = $data->week;
			$term = $data->term;
			$year = $data->year;
			$data_collector_type = $data->data_collector_type;
			$data_collector_id = $data->data_collector_id;

			$saveTeacher = null;

			//getting the info of the teacher(s) to be created which is an array
			$teachersToRegister = $data->Teacher;

			foreach ($teachersToRegister as $teacher) {
							
							$fields = array(
							'school_reference_code' => $school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'first_name' => $teacher->first_name,
							'last_name' => $teacher->last_name,
							'gender' => $teacher->gender,
							'level_taught' => $teacher->level_taught,
							'class_subject_taught' => $teacher->classSubject,
							'class_taught' => $teacher->class_taught,
							'category' => $teacher->category,
							'staff_number' => $teacher->staff_number,
							'rank' => $teacher->rank,
							'highest_academic_qualification' => $teacher->qualification_academic,
							'highest_professional_qualification' => $teacher->qualification_professional,
							'years_in_the_school' => $teacher->years_in_school,
							'term' => $term,
							'week_number' => $week_number,
							'year' => $year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $school_id,
							'data_collector_id' => $data_collector_id
							);

			     	//validating the teacher to be created
					$validation = Teachers::validate($fields);

					if ($validation !== true) {

						Log::info('Error with creating teacher, may be teacher staff number already exist');

					}else{

						//saving a teacher
			     		$saveTeacher = Teachers::create($fields);

					}
				
		}

					
			$all_teachers = DB::table('teachers')->where('school_id',$school_id)->get();

			return Response::json(array(
										"status"=>200,
										"all_teachers"=>$all_teachers));
		}



		//this registers a new teacher
		public static function RegisterTeachersByAndroidOnInternetDetection($data)
		{
			// $questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'STI';
			//$school_code = $data->school_code;
			//$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$week_number = $data->week;
			$term = $data->term;
			$year = $data->year;
			$data_collector_type = $data->data_collector_type;
			$data_collector_id = $data->data_collector_id;

			$saveTeacher = null;

			//getting the info of the teacher(s) to be created which is an array
			$teachersToRegister = $data->Teacher;

			foreach ($teachersToRegister as $teacher) {
					
				$fields = array(
							'school_reference_code' => $teacher->school_code,
							'questions_category_reference_code' =>$questions_category_reference_code,
							'first_name' => $teacher->first_name,
							'last_name' => $teacher->last_name,
							'gender' => $teacher->gender,
							'level_taught' => $teacher->level_taught,
							'class_subject_taught' => $teacher->classSubject,
							'class_taught' => $teacher->class_taught,
							'category' => $teacher->category,
							'staff_number' => $teacher->staff_number,
							'rank' => $teacher->rank,
							'highest_academic_qualification' => $teacher->qualification_academic,
							'highest_professional_qualification' => $teacher->qualification_professional,
							'years_in_the_school' => $teacher->years_in_school,
							'term' => $term,
							'week_number' => $week_number,
							'year' => $year,
							'country_id' => 1,
							'region_id' => $region_id,
							'district_id' => $district_id,
							'circuit_id' => $circuit_id,
							'school_id' => $teacher->school_id,
							'data_collector_id' => $data_collector_id
							);

						//validating the newly teacher to create
						$validation = Teachers::validate($fields);

						if ($validation !== true) {

							Log::info('Error with creating teacher, may be teacher staff number already exist');

						}else{

							try {

								//saving a teacher
				     			$saveTeacher = Teachers::create($fields);
								
							} catch (Exception $e) {
								

							}

						}
				
				}

					
				$all_teachers = DB::table('teachers')->where('circuit_id',$circuit_id)->get();

				return Response::json(array("status"=>200, "message" => "accepted", "all_teachers"=>$all_teachers));


		}


			

		//this returns the list of teachers under a school for android api request
		public static function apiRequestForDataCollectorTeachersForASchool($school_id)
		{
			$teachersCreatedByADataCollector = DB::table('teachers')->where('school_id',$school_id)->get();

				return Response::json(array("status"=>200,
											"teachers"=> $teachersCreatedByADataCollector));
		}



		
		public static function editTeacherInformationThroughAndroidAPP($data)
		{
			// $questions_category_reference_code = $data->question_reference_code;
			$questions_category_reference_code = 'STI';
			$school_code = $data->school_code;
			$school_id = $data->school_id;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$week_number = $data->week;
			$term = $data->term;
			$year = $data->year;
			$data_collector_type = $data->data_collector_type;
			$data_collector_id = $data->data_collector_id;
			$lat = $data->lat;
			$long = $data->long;

			$saveTeacher = null;

			$teacherNewInfoAfterEdit = $data->Teacher;

			foreach ($teacherNewInfoAfterEdit as $teacherNewInfo) {

				$teacherID = $teacherNewInfo->teacher_id;

				$checkingToSeeWhetherTeacherExist = DB::table('teachers')
									->where('id',$teacherID)
									->where('school_reference_code',$school_code)
									->where('region_id',$region_id)
									->where('district_id',$district_id)
									->where('circuit_id',$circuit_id)
									->where('school_id',$school_id)->first();
					
					if (count($checkingToSeeWhetherTeacherExist) > 0) {
					//Save edited teacher details
					$update = DB::table('teachers')
            					->where('id', $teacherID)
            						->update(array(
									'first_name' => $teacherNewInfo->first_name,
									'last_name' => $teacherNewInfo->last_name,
									'gender' => $teacherNewInfo->gender,
									'class_subject_taught' => $teacherNewInfo->classSubject,
									'category' => $teacherNewInfo->category,
									'staff_number' => $teacherNewInfo->staff_number,
									'rank' => $teacherNewInfo->rank,
									'highest_academic_qualification' => $teacherNewInfo->qualification_academic,
									'highest_professional_qualification' => $teacherNewInfo->qualification_professional,
									'years_in_the_school' => $teacherNewInfo->years_in_school,
									'term' => $term,
									'week_number' => $week_number,
									'year' => $year,
									'country_id' => 1,
									'region_id' => $region_id,
									'district_id' => $district_id,
									'circuit_id' => $circuit_id,
									'school_id' => $school_id,
									'data_collector_id' => $data_collector_id,
									'data_collector_type' =>$data_collector_type,
									'lat' => $lat,
									'long' => $long
									));


						if ($update) {

							$all_teachers = DB::table('teachers')->where('school_id',$school_id)->get();
							return Response::json(array(
													"status"=>200,
													"all_teachers"=>$all_teachers));
						}else{

							$all_teachers = DB::table('teachers')->where('school_id',$school_id)->get();

							return Response::json(array(
													"status"=>200,
													"all_teachers"=>$all_teachers));
							
						}
					}else{

						return Response::json(array(
													"status"=>  401,
													"message"=> 'Teacher not found'));
					}

			}



		}




		public static function deleteTeacherThroughAndroidAPP($data)
		{
			$school_code = $data->school_code;
			$region_id = $data->region_id;
			$district_id = $data->district_id;
			$circuit_id = $data->circuit_id;
			$school_id = $data->school_id;

			$teacherID = $data->deleted_teacher_id;

			$checkingToSeeWhetherTeacherExistAndWasDeleted = DB::table('teachers')
									->where('id',$teacherID)
									->where('school_reference_code',$school_code)
									->where('region_id',$region_id)
									->where('district_id',$district_id)
									->where('circuit_id',$circuit_id)
									->where('school_id',$school_id)->delete();

			$all_teachers = DB::table('teachers')->where('school_id',$school_id)->get();

			if ($checkingToSeeWhetherTeacherExistAndWasDeleted) {
				//return all teachers
				return Response::json(array(
											"status"=>200,
											"all_teachers"=>$all_teachers));
			}else{

				return Response::json(array(
										   "status"=>200,
										   "all_teachers"=>$all_teachers));
			}
		}




		public static function createTeacherViaForm($fields)
		{
			//validating the input
			$validation = Teachers::validate($fields);

			if ($validation !== true) {

				return $validation;

			}else{

				$createTeacher = Teachers::create($fields);

				if ($createTeacher) {

					return 'success';

				}

				return 'failed';
		    }

		}




		public static function currentNumberOfTeachersInASchool($school_id)
		{
			
			$current_number_of_teachers_in_a_school = DB::table('teachers')->where('school_id', $school_id)->count();

			return  $current_number_of_teachers_in_a_school;
		}






		public static function getAverageTeacherAttendanceBaseOnAPeriod($school_id, $term, $year)
		{
			
			if (is_null($term)) {

					$number_of_weeks_data_submissions_has_been_made_for = SchoolTotalsOnWeeeklySubmittedData::where('school_id', $school_id)->where('year',$year)->count();

					$sum_of_all_weekly_attendance_for_teacher_attendance_for_the_period_chosen = SchoolTotalsOnWeeeklySubmittedData::where('school_id', $school_id)->where('year',$year)->sum('average_teacher_attendance');

				
			}else{

					$number_of_weeks_data_submissions_has_been_made_for = SchoolTotalsOnWeeeklySubmittedData::where('school_id', $school_id)->where('term',$term)->where('year',$year)->count();

					$sum_of_all_weekly_attendance_for_teacher_attendance_for_the_period_chosen = SchoolTotalsOnWeeeklySubmittedData::where('school_id', $school_id)->where('term',$term)->where('year',$year)->sum('average_teacher_attendance');


			}

			$average_teacher_attendance = $sum_of_all_weekly_attendance_for_teacher_attendance_for_the_period_chosen / $number_of_weeks_data_submissions_has_been_made_for;

			return $average_teacher_attendance;

		}




	
	}


?>