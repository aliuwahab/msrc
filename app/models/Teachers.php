<?php

class Teachers extends Eloquent {

	protected $table = 'teachers';

	protected $guarded = array('id');


	//this are rules for the creation of a region
	public static $rules = array(
		'school_reference_code' => 'required',
		'first_name' => 'required',
		'gender' => 'required',
		'level_taught' => 'required',
		'staff_number' => 'required|unique:teachers,staff_number'
	);


	public static function validate($input) {

		$validation = Validator::make($input, static ::$rules);

		if ($validation->fails()) {

			return $validation->messages();

		}else{
			
			return true;
		}
	}




	public function puntuality_and_lesson_plans() {

		return $this->hasMany('PuntualityAndLessons','teacher_id','id');
		
	}

	public function classroom_management() {

		return $this->hasMany('TeachersClassManagement','teacher_id','id');
		
	}

	public function units_covered() {

		return $this->hasMany('TeacherUnitsCovered','teacher_id','id');

	}

	public function question_category() {
		//this soecifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('QuestionCategory','question_category_code', 'questions_category_reference_code');
	}
	
	public function school() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('School','school_reference_code', 'school_code');
	}

	public function teacher_school() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('School');
	}

	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuits() {
		return $this->belongsTo('Circuit');
	}

	public function data_collector() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('DataCollector','data_collector_id', 'id');
	}

}
