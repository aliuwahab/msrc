<?php

class Security extends Eloquent {
	
	protected $table = 'security';
	protected $guarded = array('id');

	public static $rules = array();


		public function question_category() {
		//this soecifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('QuestionCategory','question_category_code', 'questions_category_reference_code');
	}

	// this relates the school_mangement table to the schools table
	public function school() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('School','school_reference_code', 'school_code');
	}


	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuits() {
		return $this->belongsTo('Circuit');
	}

	public function school_belongs_to() {
		return $this->belongsTo('School');
	}
}
