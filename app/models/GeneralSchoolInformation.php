<?php

class GeneralSchoolInformation extends Eloquent {

	protected $table = 'general_school_information';
	protected $guarded = array('id');

	public static $rules = array();

	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuit() {
		return $this->belongsTo('Circuit');
	}
	public function school() {
		return $this->belongsTo('School');
	}
	public function data_collector() {
		return $this->belongsTo('DataCollector','data_collector_id','id');
	}
}
