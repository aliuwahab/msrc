<?php

class Answer extends Eloquent {
	protected $guarded = array('id');

	public static $rules = array();

	public function school_visit_question() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate on
		return $this->belongsTo('SchoolVisit','question_code', 'question_code_reference');
	}

	public function itinerary() {
		return $this->belongsTo('DataIntenary','itenary_id','id');
	}

	public function answer_country() {
		return $this->belongsTo('Country');
	}

	public function answer_region() {
		return $this->belongsTo('Region');
	}
	public function answer_district() {
		return $this->belongsTo('District');
	}
	public function answer_circuit() {
		return $this->belongsTo('Circuit');
	}
	public function answer_school() {
		return $this->belongsTo('School','school_code_reference','school_code');
	}
}
