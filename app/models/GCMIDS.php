<?php

class GCMIDS extends Eloquent {

	protected $table = 'gcm_ids';
	
	protected $guarded = array('id');

	public static $rules = array();

	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuit() {
		return $this->belongsTo('Circuit');
	}
	public function data_collector() {
		return $this->belongsTo('DataCollector', 'data_collector_id','id');
	}

	
}
