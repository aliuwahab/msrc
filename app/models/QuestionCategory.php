<?php

class QuestionCategory extends Eloquent {

	protected $table = 'questions_categories';

	protected $guarded = array('id');

	public static $rules = array();


	public function data_category() {
		//this soecifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('DataCategory','data_category_code', 'data_category_code_reference');
	}



	public function school_visit_questions() {
		return $this->hasMany('SchoolVisit','question_category_reference', 'question_category_code');
	}

	public function school_review_questions() {
		return $this->hasMany('SchoolReviewQuestion','question_category_reference', 'question_category_code');
	}


	

	public function school_enrolment() {
		return $this->hasMany('SchoolEnrolment','questions_category_reference_code', 'question_category_code');
	}

	public function special_school_enrolment() {
		return $this->hasMany('SpecialStudentsEnrolment','questions_category_reference_code', 'question_category_code');
	}

	public function school_textbooks() {
		return $this->hasMany('Textbook','questions_category_reference_code', 'question_category_code');
	}

	public function school_peformance() {
		return $this->hasMany('Performance','questions_category_reference_code', 'question_category_code');
	}

	public function school_attendance() {
		return $this->hasMany('StudentAttendance','questions_category_reference_code', 'question_category_code');
	}

	public function special_students_attendance() {
		return $this->hasMany('SpecialStudentsAttendance','questions_category_reference_code', 'question_category_code');
	}

	public function school_grant() {
		return $this->hasMany('Grant','questions_category_reference_code', 'question_category_code');
	}
	public function school_support_type() {
		return $this->hasMany('SupportType','questions_category_reference_code', 'question_category_code');
	}
	public function pupils_performance() {
		return $this->hasMany('PupilsExamsPerformance','questions_category_reference_code', 'question_category_code');
	}
	public function school_teachers() {
		return $this->hasMany('Teachers','questions_category_reference_code', 'question_category_code');
	}
	public function school_management() {
		return $this->hasMany('SchoolManagement','questions_category_reference_code', 'question_category_code');
	}
	public function school_meetings() {
		return $this->hasMany('SchoolsMeeting','questions_category_reference_code', 'question_category_code');
	}

	public function country() {
		return $this->hasMany('Country');
	}
}
