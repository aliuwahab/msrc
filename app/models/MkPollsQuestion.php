<?php

class MkPollsQuestion extends Eloquent {

    protected $table = 'mk_polls_questions';

	protected $guarded = array('id');

    public static $rules = array(
        'district_id' => 'required', 'circuit_id' => 'required',
        'school_name' => 'required', 'school_id' => 'required',
        'school_code' => 'required', 'phone_number' => 'required',
        'head_teacher_name' => 'required', 'data_collector_id' => 'required',
        'poll_question_id' => 'required', 'poll_answer' => 'required'
    );

    public static function validateAttendance($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return true;
        }else{
            return false;
        }
    }





}
