<?php

class ImageModel extends Eloquent {

	protected $table = 'images';
	protected $guarded = array('id');


	public function school_by_reference_code() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('School','school_reference_code', 'school_code');
	}

	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuit() {
		return $this->belongsTo('Circuit');
	}

	public function school() {
		return $this->belongsTo('School');
	}


		//this are rules for the creation of a circuit
	public static $rules = array(
		'image_url' => 'required',
		'image_title' => 'required',
		'image_description' => 'required',
		'school_reference_code' => 'required',
		'year' => 'required',
		'country_id' => 'required',
		'region_id' => 'required',
		'district_id' => 'required',
		'circuit_id' => 'required',
		'school_id' => 'required'
	);

	public static function validate($input) {
		$validation = Validator::make($input, static ::$rules);
		if ($validation->fails()) {
			return $validation->messages();
		}else{
			return true;
		}
	}


}
