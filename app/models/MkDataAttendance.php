<?php
use Illuminate\Database\Eloquent;


class MkDataAttendance extends Eloquent\Model {

    protected $table = 'mk_attendance';

    public $fillable = ['data_collector_id','attendance_academic_term', 'attendance_academic_level', 'attendance_academic_week','attendance_academic_year','attendance_no_pupil_level','attendance_no_pupil_avg','attendance_no_teacher_avg','attendance_academic_subject'];
}
