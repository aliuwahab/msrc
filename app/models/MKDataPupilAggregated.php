<?php
use Illuminate\Database\Eloquent;
class MkDataPupilAggregated extends Eloquent\Model {

    protected $table = 'mk_pupil_aggregated';
	public $fillable=['data_collector_id'
                    ,'academic_term'
                    ,'academic_year'
                    ,'no_student_grade'
                    ,'no_student_assessed'
                    ,'attendance_no_pupil_level'
                    ,'attendance_academic_week'
                    ,'attendance_no_pupil_level'
                    ,'attendance_pupil_avg'
                    ,'math_beginner_text'
                    ,'math_digit_one_text'
                    ,'math_digit_two_text'
                    ,'math_addition_text'
                    ,'math_subtraction_text'
                    ,'math_level_one_text'
                    ,'math_level_two_text'
                    ,'math_level_three_text'
                    ,'english_beginner_text'
                    ,'english_paragraph_text'
                    ,'english_word_text'
                    ,'english_sentence_text'
                    ,'english_level_one_text'
                    ,'english_level_two_text'
                    ,'english_level_three_text'
    ];

}



















