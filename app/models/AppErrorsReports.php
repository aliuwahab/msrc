<?php

class AppErrorsReports extends Eloquent {


	protected $table = 'app_errors_reported';

	protected $guarded = array('id');

	public static $rules = array();



	public function reporter()
	{
		return $this->belongTo('DataCollector','reporter_id','id');
	}
}
