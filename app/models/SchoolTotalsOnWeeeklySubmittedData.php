<?php

class SchoolTotalsOnWeeeklySubmittedData extends Eloquent {

	protected $table = 'weekly_totals';

	protected $guarded = array('id');

	public static $rules = array();


	public function question_category() {
		//this soecifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('QuestionCategory','question_category_code', 'questions_category_reference_code');
	}

	public function school() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('School','school_reference_code', 'school_code');
	}


	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuit() {
		return $this->belongsTo('Circuit');
	}

	public function school_class_attendance()
	{
		return $this->belongsTo('School');
	}


}
