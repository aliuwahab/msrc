<?php

class DataIntenary extends Eloquent {

	public $table = "itenary";

	protected $guarded = array('id');

	public function country() {
		return $this->belongsTo('Country');
	}
	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}
	public function circuit() {
		return $this->belongsTo('Circuit');
	}


	public function itinerary_answers() {
		return $this->hasMany('Answer','id','itinerary_id');
	}




	public static $rules = array(
		'name' => 'required',
		'description' => 'required',
		'start_date' => 'required',
		'end_date' => 'required',
		'country_id' => 'required',
		'region_id' => 'required',
		'district_id' => 'required'
		);


		public static function validate($input) {
		$validation = Validator::make($input, static ::$rules);
		if ($validation->fails()) {
			return $validation->messages();
		}else{
			return true;
		}
	}
}
