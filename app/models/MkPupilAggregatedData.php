<?php

class MkPupilAggregatedData extends Eloquent {

    protected $table = 'mk_pupil_aggregated';

	protected $guarded = array('id');

    public static $rules = array(
        'academic_term' => 'required', 'academic_year' => 'required',
        'grade' => 'required', 'no_student_grade' => 'required',
        'no_student_assessed' => 'required', 'data_collector_id' => 'required',
        'data_collector_type' => 'required', 'longitude' => 'required',
        'latitude' => 'required'
    );


    public static function validate($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return true;
        }else{
            return false;
        }
    }




}
