<?php

class Country extends \Eloquent {
	public $table       = "countries";
	protected $guarded = array('id');

	public function regions() {
		return $this->hasMany('Region');
	}

	public function districts() {
		return $this->hasMany('District');
	}

	public function itinararies() {
		return $this->hasMany('DataIntenary');
	}
	public function circuits() {
		return $this->hasMany('Circuit');
	}
	public function schools() {
		return $this->hasMany('School');
	}

	public function data_collectors() {
		return $this->hasMany('DataCollector');
	}

	public function grants() {
		return $this->hasMany('Grant');
	}

	public function performances() {
		return $this->hasMany('Peformance');
	}

	public function school_enrolments() {
		return $this->hasMany('SchoolEnrolment');
	}
	public function school_managemnets() {
		return $this->hasMany('SchoolManagement');
	}

	public function school_teachers() {
		return $this->hasMany('Teachers');
	}

	public function school_textbooks() {
		return $this->hasMany('Textbook');
	}

	public function school_support_types() {
		return $this->hasMany('SupportType');
	}

	public function school_submission_status() {
		return $this->hasMany('SubmissionStatus');
	}

	public function gcims_data_collectors() {
		return $this->hasMany('GCMIDS');
	}

	public function data_categories() {
		return $this->hasMany('DataCategory');
	}

	public function question_categories() {
		return $this->hasMany('QuestionCategory');
	}

	public function community_sms() {
		return $this->hasMany('CommunitySms');
	}

	public function schoolreportcard_termly_submission_status() {
		return $this->hasMany('SchoolReportCardSubmissionStatus');
	}
	public function schoolreportcard_weekly_submission_status() {
		return $this->hasMany('SchoolReportCardWeeklySubmissionStatus');
	}
	public function schoolreview_submission_status() {
		return $this->hasMany('SchoolReviewSubmissionStatus');
	}
	public function schoolvisits_submission_status() {
		return $this->hasMany('SchoolVisitisSubmissionStatus');
	}

	public function images()
	{
		return $this->hasMany('ImageModel');
	}


	//this are rules for the creation of a country
	public static $rules = array(
		'code' => 'required',
		'name' => 'required',
		'slug' => 'required',
		'description' => 'required',
		'lat_long' => 'required|min:8|max:14'
	);

	public static function validate($input) {
		$validation = Validator::make($input, static ::$rules);
		if ($validation->fails()) {
			return $validation;
		}else{
			return true;
		}
	}
}