<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $fillable = array('username','firstname','lastname','gender','phone_number','email','password','level','role','level_id');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 *hiiden field
	 */
	protected $hidden = array('password');


	//this are rules for the creation of a new user
	public static $rules = array(
		'username' => 'required | unique:users,username',
		'firstname' => 'required',
		'lastname' => 'required',
		'gender' => 'required',
		'phone_number' => 'required',
		'email' => 'required | unique:users,email',
		'password' => 'required | confirmed',
		'level' => 'required',
		'role' => 'required',
		'level_id' => 'required'
	);

	//this function validate the information entered to create a new user
	public static function validate($input) {
		$validation = Validator::make($input, static ::$rules);
		if ($validation->fails()) {
			return $validation->messages();
		} else {
			return true;
		}
	}




	//this are rules for the creation of a new user
	public static $editAdminrules = array(
		'firstname' => 'required',
		'lastname' => 'required',
		'gender' => 'required',
		'phone_number' => 'required',
		'email' => 'required',
		'level' => 'required',
		'role' => 'required',
		'level_id' => 'required'
	);

	//this function validate the information entered to create a new user
	public static function editAdminvalidate($input) {
		$validation = Validator::make($input, static ::$editAdminrules);
		if ($validation->fails()) {
			return $validation->messages();
		} else {
			return true;
		}
	}



	//this are rules for the creation of a new user
	public static $editPasswordRules = array(
		'password' => 'required'
	);

	//this function validate the information entered to create a new user
	public static function editPasswordValidate($input) {
		$validation = Validator::make($input, static ::$editPasswordRules);
		if ($validation->fails()) {
			return $validation->messages();
		} else {
			return true;
		}
	}

	//this function authencticate a user when login in
	public static function authenticate($credentials,$remember_me) {
		# logging in a user using laravel authentication system
        
        if ($remember_me != 'on') {

        	$loggingIn = Auth::attempt($credentials);

        }else{

        	$loggingIn = Auth::attempt($credentials,true);
        }
		
		if ($loggingIn) {

			return true;

		} else {

			return false;

		}

	}



	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 * Send Reminder Email
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
	//Remeber me setting upon sign in and sign up
	public function getRememberToken() {
		return $this->remember_token;
	}
	//setting up remember me token
	public function setRememberToken($value) {
		$this->remember_token = $value;
	}
	public function getRememberTokenName() {
		return 'remember_token';
	}

}