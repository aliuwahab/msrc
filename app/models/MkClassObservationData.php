<?php

class MkClassObservationData extends Eloquent {

    protected $table = 'mk_class_observation';

	protected $guarded = array('id');

    public static $rules = array(
        'class_ob_teacher_name' => 'required', 'class_ob_level' => 'required',
        'class_ob_subject' => 'required', 'class_ob_enrolled_p_six' => 'required',
        'class_ob_enrolled_p_four' => 'required', 'class_ob_enrolled_p_five' => 'required',
        'class_ob_enrolled_p_total' => 'required',
        'class_ob_absent_p_four' => 'required', 'class_ob_absent_p_five' => 'required',
        'class_ob_absent_p_six' => 'required', 'class_ob_absent_p_total' => 'required',
        'class_ob_duration' => 'required', 'data_collector_id' => 'required',
        'data_collector_type' => 'required', 'longitude' => 'required',
        'latitude' => 'required',
    );


    public static function validate($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return true;
        }else{
            return false;
        }
    }





}
