<?php

class RequiredImage extends Eloquent {

    protected $table = 'required_images';

    protected $guarded = array('id');

    //this are rules for the creation of a circuit
    public static $rules = array(
        'image_id' => 'required',
        'image_category_type' => 'required'
    );


    public static function validate($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return $validation->messages();
        }else{
            return true;
        }
    }

    public function images() {
        return $this->hasMany('ImageModel','image_id','image_id'); // First in RequiredImages table another in Images table
    }


	public function answer_country() {
		return $this->belongsTo('Country');
	}

	public function answer_region() {
		return $this->belongsTo('Region');
	}
	public function answer_district() {
		return $this->belongsTo('District');
	}
	public function answer_circuit() {
		return $this->belongsTo('Circuit');
	}
	public function answer_school() {
		return $this->belongsTo('School','school_code_reference','school_code');
	}
}
