<?php

class MkAttendance extends Eloquent {

    protected $table = 'mk_attendance';

	protected $guarded = array('id');

    public static $rules = array(
        'attendance_academic_term' => 'required', 'attendance_academic_year' => 'required',
        'attendance_academic_subject' => 'required', 'attendance_academic_level' => 'required',
        'attendance_no_pupil_level' => 'required', 'attendance_no_pupil_avg' => 'required',
        'attendance_no_teacher_avg' => 'required', 'data_collector_id' => 'required',
        'data_collector_type' => 'required', 'longitude' => 'required',
        'latitude' => 'required',
    );


    public static function validateAttendance($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return true;
        }else{
            return false;
        }
    }




}
