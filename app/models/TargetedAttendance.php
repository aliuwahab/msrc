<?php

class TargetedAttendance extends Eloquent {

    protected $table = 'targeted_attendance';

	protected $guarded = array('id');

    //this are rules for the creating targeted attendance of a school
    public static $rules = array(
        'school_reference_code' => 'required',
        'primary_four_total_attendance' => 'required',
        'primary_five_total_attendance' => 'required',
        'primary_six_total_attendance' => 'required',
        'number_of_week_days' => 'required',
        'data_collector_id' => 'required',
        'data_collector_type' => 'required',
        'region_id' => 'required',
        'district_id' => 'required',
        'circuit_id' => 'required',
        'year' => 'required',
        'term' => 'required',
        'week_number' => 'required'
    );


    public static function validateTargetedAttendanceCreation($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return $validation->messages();
        }else{
            return true;
        }
    }




}
