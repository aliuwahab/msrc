<?php

class HealthyLevel extends Eloquent {

	protected $table = 'healthy_level';

	protected $guarded = array('id');

	public static $rules = array();



	
	public function school_review_question() {
		//this specifies the field that the two tables relate on
		//first one representing this model table field and second one 
		//representing the parent table field the relate
		return $this->belongsTo('SchoolReviewQuestion','question_code','question_code_reference');
	}

	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuit() {
		return $this->belongsTo('Circuit');
	}

	public function school() {
		return $this->belongsTo('School');
	}
}
