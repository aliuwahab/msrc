<?php

class NotificationInfo extends Eloquent {
	
	protected $table = 'notification_info';

	protected $guarded = array('id');

	public static $rules = array();


	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuits() {
		return $this->belongsTo('Circuit');
	}

	public function data_collector_sent_info() {
		return $this->belongsTo('DataCollector', 'id','data_collector_id');
	}
}
