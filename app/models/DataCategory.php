<?php

class DataCategory extends Eloquent {

	protected $table = 'data_categories';

	protected $guarded = array('id');

	public static $rules = array();


	public function questions_category() {
		return $this->hasMany('QuestionCategory','data_category_code_reference', 'data_category_code');
	}

	public function country() {
		return $this->belongsTo('Country');
	}

}
