<?php

class HelpRequest extends Eloquent {

    protected $table = 'help_requests';

    protected $guarded = array('id');

    //this are rules for the uploading of elibrary files
    public static $rules = array(
        'request_title' => 'required'
    );


    public static function validateHelpRequestCreation($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return $validation->messages();
        }else{
            return true;
        }
    }








}
