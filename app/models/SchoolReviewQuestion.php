<?php

class SchoolReviewQuestion extends Eloquent {

	protected $table = 'school_review_questions';

	protected $guarded = array('id');

	public static $rules = array();



	public function question_category() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('QuestionCategory','question_category_code', 'question_category_reference');
	}


	public function inclusiveness_answers() {
		return $this->hasMany('SchoolInclusiveAnswer','question_code_reference','question_code');
	}

	public function effective_teaching_learning() {
		return $this->hasMany('EffectiveTeachingLearning','question_code_reference','question_code');
	}

	public function healthy_level() {
		return $this->hasMany('HealthyLevel','question_code_reference','question_code');
	}

	public function safe_protective() {
		return $this->hasMany('SafeProtective','question_code_reference','question_code');
	}

	public function friendly_schools() {
		return $this->hasMany('FriendlySchool','question_code_reference','question_code');
	}
	public function community_involvement() {
		return $this->hasMany('CommunityInvolvement','question_code_reference','question_code');
	}



	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuits() {
		return $this->belongsTo('Circuit');
	}
}
