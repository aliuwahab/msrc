<?php

class SchoolsMeeting extends Eloquent {

	protected $table = 'schools_meeting';

	protected $guarded = array('id');

	public static $rules = array();


	public function question_category() {
		//this soecifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('QuestionCategory','question_category_code', 'questions_category_reference_code');
	}

	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuit() {
		return $this->belongsTo('Circuit');
	}
	public function school() {
		return $this->belongsTo('School');
	}
	
	public function data_collector() {
		return $this->belongsTo('DataCollector', 'data_collector_id','id');
	}

	
}
