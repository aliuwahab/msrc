<?php

class ELibrary extends Eloquent {

    protected $table = 'e_library';

    protected $guarded = array('id');

    //this are rules for the uploading of elibrary files
    public static $rules = array(
        'targeted_groups' => 'required',
        'file_type' => 'required',
        'file_title' => 'required',
        'file_description' => 'required'
    );


    public static function validateElibraryFileUpload($input) {
        $validation = Validator::make($input, static ::$rules);
        if ($validation->fails()) {
            return $validation->messages();
        }else{
            return true;
        }
    }




}
