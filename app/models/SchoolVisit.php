<?php

class SchoolVisit extends Eloquent {

	protected $table = 'school_visits_questions';
	protected $guarded = array('id');

	public static $rules = array();


	public function question_category() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('DataCategory','question_category_code', 'question_category_reference');
	}


	public function school_visit_answers() {
		return $this->hasMany('Answer','question_code_reference','question_code');
	}


	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuits() {
		return $this->belongsTo('Circuit');
	}



}
