<?php

class WeeklyTotals extends \Eloquent {
	
	protected $table = 'weekly_totals';
	
	protected $guarded = array('id');

	public static $rules = array();


	// this relates the school_mangement table to the schools table
	public function school() {
		//this specifies the field that the two tables relate on
		//first one representing this model table and second one 
		//representing the parent table field the relate
		return $this->belongsTo('School','school_reference_code', 'school_code');
	}


	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuits() {
		return $this->belongsTo('Circuit');
	}

	public function school_belongs_to() {
		return $this->belongsTo('School');
	}



	 public static function sums($columns){

	    $instance = new static;
	    $columns = is_array($columns) ? $columns : func_get_args();

	    $selects = array_map(function($column){
	        return DB::raw('SUM('.$column.') AS '.$column);
	    }, $columns);

	    return $instance->select($selects)->first();

	}

	
}
