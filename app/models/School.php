<?php

class School extends \Eloquent {
	
	public $table       = "schools";
	
	protected $guarded = array('id');

	public function country() {
		return $this->belongsTo('Country');
	}
	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}
	public function circuit() {
		return $this->belongsTo('Circuit');
	}
	public function supervisor() {
		return $this->belongsTo('DataCollector','id');
	}
public function images()
	{
		return $this->hasMany('ImageModel');
	}
	public function enrolments() {
		return $this->hasMany('SchoolEnrolment','school_code','school_reference_code');
	}
	public function attendance() {
		return $this->hasMany('StudentAttendance','school_code','school_reference_code');
	}

	public function textbooks() {
		return $this->hasMany('Textbook','school_code','school_reference_code');
	}

	public function perfomances() {
		return $this->hasMany('Performance','school_code','school_reference_code');
	}

    public function school_visits_answers() {

		return $this->hasMany('Answer','school_code','school_code_reference');
		
	}

	public function grants() {

		return $this->hasMany('Grant','school_code','school_code_reference');
		
	}

	public function suport_types() {

		return $this->hasMany('SupportType','school_code','school_code_reference');
		
	}

	public function teachers() {

		return $this->hasMany('Teachers','school_code','school_code_reference');
		
	}
	public function management() {

		return $this->hasMany('SchoolManagement','school_code','school_code_reference');
		
	}

	public function community_sms() {
		return $this->hasMany('CommunitySms');
	}
	

	public function schoolreportcard_termly_submission_status() {
		return $this->hasMany('SchoolReportCardSubmissionStatus');
	}
	public function schoolreportcard_weekly_submission_status() {
		return $this->hasMany('SchoolReportCardWeeklySubmissionStatus');
	}
	public function schoolreview_submission_status() {
		return $this->hasMany('SchoolReviewSubmissionStatus');
	}
	public function schoolvisits_submission_status() {
		return $this->hasMany('SchoolVisitisSubmissionStatus');
	}

	//this are rules for the creation of a school
	public static $rules = array(
		'ges_code' => 'required | unique:schools,ges_code',
		'region_id' => 'required',
		'district_id' => 'required',
		'circuit_id' => 'required',
		'country_id' => 'required',
		'headteacher_id' => 'required',
		'supervisor_id' => 'required',
		'name' => 'required',
		'category' => 'required'
	);

	//this are rules for the creation of a school
	public static $editRules = array(
		'ges_code' => 'required',
		'region_id' => 'required',
		'district_id' => 'required',
		'circuit_id' => 'required',
		'country_id' => 'required',
		'headteacher_id' => 'required',
		'supervisor_id' => 'required',
		'name' => 'required',
		'category' => 'required'
	);

    //this are rules for the creation of a school
    public static $assignSchoolToTargetedGroupRules = array(
        'school_id' => 'required',
        'targeted_group' => 'required',
    );




	public static function validate($input) {
		$validation = Validator::make($input, static ::$rules);
		if ($validation->fails()) {
			return $validation->messages();
		}else{
			return true;
		}
	}



	public static function editValidate($input) {
		$validation = Validator::make($input, static ::$editRules);
		if ($validation->fails()) {
			return $validation->messages();
		}else{
			return true;
		}
	}


    public static function assignSchoolToTargetedGroupRulesValidate($input) {

        $validation = Validator::make($input, static ::$assignSchoolToTargetedGroupRules);
        if ($validation->fails()) {
            return $validation->messages();
        }else{
            return true;
        }

    }



}