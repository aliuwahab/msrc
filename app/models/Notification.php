<?php

class Notification extends Eloquent {
	protected $table = 'notifications';
	protected $guarded = array('id');

	public static $rules = array();

	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

	public function circuit() {
		return $this->belongsTo('Circuit');
	}
	public function school() {
		return $this->belongsTo('School');
	}
}
