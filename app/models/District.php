<?php

class District extends \Eloquent {
	public $table       = "districts";
	protected $fillable = array('code','region_id','country_id','name','email','phone_number','slug','description','lat_long');

	public function country() {
		return $this->belongsTo('Country');
	}
	public function circuits() {
		return $this->hasMany('Circuit');
	}
	public function schools() {
		return $this->hasMany('School');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
		public function data_collectors() {
		return $this->hasMany('DataCollector');
	}

	public function itinararies() {
		return $this->hasMany('DataIntenary');
	}

public function images()
	{
		return $this->hasMany('ImageModel');
	}

	public function grants() {
		return $this->hasMany('Grant');
	}

	public function performances() {
		return $this->hasMany('Peformance');
	}

	public function school_enrolments() {
		return $this->hasMany('SchoolEnrolment');
	}
	public function school_managemnets() {
		return $this->hasMany('SchoolManagement');
	}

	public function school_teachers() {
		return $this->hasMany('Teachers');
	}

	public function school_textbooks() {
		return $this->hasMany('Textbook');
	}

	public function school_support_types() {
		return $this->hasMany('SupportType');
	}

	public function school_submission_status() {
		return $this->hasMany('SubmissionStatus');
	}

	public function gcims_data_collectors() {
		return $this->hasMany('GCMIDS');
	}

	public function community_sms() {
		return $this->hasMany('CommunitySms');
	}

	public function schoolreportcard_termly_submission_status() {
		return $this->hasMany('SchoolReportCardSubmissionStatus');
	}
	public function schoolreportcard_weekly_submission_status() {
		return $this->hasMany('SchoolReportCardWeeklySubmissionStatus');
	}
	public function schoolreview_submission_status() {
		return $this->hasMany('SchoolReviewSubmissionStatus');
	}
	public function schoolvisits_submission_status() {
		return $this->hasMany('SchoolVisitisSubmissionStatus');
	}

	//this are rules for the creation of a district
	public static $rules = array(
		'code' => 'required | unique:districts,code',
		'region_id' => 'required',
		'country_id' => 'required',
		'name' => 'required'	
	);

	public static function validate($input) {
		$validation = Validator::make($input, static ::$rules);
		if ($validation->fails()) {
			return $validation->messages();
		}else{
			return true;
		}
	}
}