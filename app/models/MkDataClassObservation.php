<?php
//namespace App;
use Illuminate\Database\Eloquent;

class MkDataClassObservation extends Eloquent\Model {
    protected $table = 'mk_class_observation';

    public $fillable = ['cl_classroom_summary'
                        ,'attendance_no_pupil_level'
                        ,'cl_question_four_comment'
                        ,'cl_question_three_comment'
                        ,'cl_question_two_comment'
                        ,'cl_question_one_comment'
                        ,'cl_question_three_answer'
                        ,'cl_question_two_answer'
                        ,'cl_question_one_speaking_answer'
                        ,'cl_question_one_writing_answer'
                        ,'cl_question_one_doing_answer'
                        ,'cl_question_one_saying_answer'
                        ,'cl_question_one_reading_answer'
                        ,'la_lesson_summary'
                        ,'la_question_three_comment'
                        ,'la_question_two_comment'
                        ,'la_question_one_comment'
                        ,'la_question_three_answer'
                        ,'la_question_two_answer'
                        ,'la_question_one_answer'
                        ,'ti_lesson_summary'
                        ,'ti_question_three_comment'
                        ,'ti_question_two_comment'
                        ,'ti_question_one_comment'
                        ,'ti_question_three_answer'
                        ,'ti_question_two_answer'
                        ,'ti_question_one_answer'
                        ,'class_ob_attendance_summary'
                        ,'class_ob_duration'
                        ,'class_ob_date_monitoring'
                        ,'class_ob_absent_p_total'
                        ,'class_ob_absent_p_six'
                        ,'class_ob_absent_p_five'
                        ,'class_ob_absent_p_four'
                        ,'class_ob_enrolled_p_total'
                        ,'class_ob_enrolled_p_six'
                        ,'class_ob_enrolled_p_five'
                        ,'class_ob_enrolled_p_four'
                        ,'class_ob_level'
                        ,'class_ob_subject'
                        ,'class_ob_teacher_name'
                        ,'data_collector_id'];
}



  