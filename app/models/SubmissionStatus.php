<?php

class SubmissionStatus extends Eloquent {

	protected $table = 'submission_status';

	protected $guarded = array('id');

	public static $rules = array();


	public function country() {
		return $this->belongsTo('Country');
	}

	public function region() {
		return $this->belongsTo('Region');
	}
	public function district() {
		return $this->belongsTo('District');
	}

//	public function country() {
//		return $this->belongsTo('Country');
//	}

}
