<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'M4DAPP' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyB_I1EzjJtNy38rEkRDF9bAc0NNlC2hB-4',
        'service'     =>'gcm'
    )

);