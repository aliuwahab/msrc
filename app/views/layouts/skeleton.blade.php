<?php ?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	@section('links')
	<link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700|Open+Sans:400,700,300|Roboto:100,300,400,700|Roboto+Condensed:300,400,700' rel='stylesheet' type='text/css'>


	<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">-->
	<link href="/stylesheets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

	<link rel='stylesheet' href='/stylesheets/all_combined.css'>

	@show

	<link href="/favicon.ico" rel="shortcut icon">
	<link href="/apple-touch-icon.png" rel="apple-touch-icon">


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>

	<![endif]-->

	<title>School Monitoring Application</title>

</head>

<body>
@section('content')
<!--The section tag holds the partial views loaded  with angular-->
Some content here
@show


@section('scripts')
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>-->

<script type="text/javascript" src="/scripts/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src='/scripts/bootstrap_combined.js'></script>

<script type="text/javascript" src='/scripts/dataTable.js'></script>
@show


</body>
</html>