<?php ?>
@extends('layouts.master')

@section('links')
@parent
@stop


@section('content')
<div class="all-wrapper fixed-header left-menu">
  <div class="page-header">
  <div class="header-links hidden-xs">
    <div class="top-search-w pull-right">
      <input type="text" class="top-search" placeholder="Search"/>
    </div>
    <div class="dropdown hidden-sm hidden-xs">
      <a href="" data-toggle="dropdown" class="header-link"><i class="icon-bolt"></i> User Alerts <span class="badge alert-animated">5</span></a>
    </div>
    <div class="dropdown hidden-sm hidden-xs">
      <a href="" data-toggle="dropdown" class="header-link"><i class="icon-cog"></i> Settings</a>
    </div>

    <div class="dropdown">
      <a href="" class="header-link clearfix" data-toggle="dropdown">
        <div class="avatar">
          <img src="/images/avatar-small.jpg" alt="">
        </div>
        <div class="user-name-w">
Lionel Messi <i class="icon-caret-down"></i>
        </div>
      </a>
      <ul class="dropdown-menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li><a href="#">Separated link</a></li>
        <li><a href="#">One more separated link</a></li>
      </ul>
    </div>
  </div>
  <a class="logo hidden-xs" href="#/"><i class="icon-rocket"></i></a>
  <a class="menu-toggler" href="#"><i class="icon-reorder"></i></a>
  <h1>Dashboard</h1>
</div>
	<div class="side">
		<div class="sidebar-wrapper">
			<ul>
				<li class='current'>
					<a class='current' href="#/" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard" ng-click="set_sidebar_active('Dashboard')">
						<i class="icon-home"></i>
					</a>
				</li>
				<li>
					<a href="#/view" data-toggle="tooltip" data-placement="right" title="" data-original-title="View" ng-click="set_sidebar_active('View')">
						<i class="icon-eye-open"></i>
					</a>
				</li>
				<li>
					<a href="#/analytics" data-toggle="tooltip" data-placement="right" title="" data-original-title="Analytics" ng-click="set_sidebar_active('Analytics')">
						<i class="icon-bar-chart"></i>
					</a>
				</li>
				<li>
					<a href="#/stakeholders" data-toggle="tooltip" data-placement="right" title="" data-original-title="Stakeholders" ng-click="set_sidebar_active('Stakeholders')">
						<span class="badge"></span>
						<i class="icon-user"></i>
					</a>
				</li>
				<li>
					<a href="#/forms" data-toggle="tooltip" data-placement="right" title="" data-original-title="Forms" ng-click="set_sidebar_active('Forms')">
						<i class="icon-file-text-alt"></i>
					</a>
				</li>
				<li>
					<a href="#/android" data-toggle="tooltip" data-placement="right" title="" data-original-title="Android Phone">
						<i class="icon-phone"></i>
					</a>
				</li>
			</ul>
		</div>
  </div>
  <div class="main-content">
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><a href="#">Bread</a></li>
      <li><a href="#">Crumbs</a></li>
      <li class="active">Example</li>
    </ol>
    <div class="error-page-wrapper">
      <div class="picture-w">
        <i class="icon-exclamation-sign"></i>
      </div>
      <h1>Something's not quite right.</h1>
      <h3>We're fixing this issue and everything should be back up and running soon. <br/>We hope you'll bear with us during this down time. <br/>Thank you for your patience.</h3>
      <a href="#/" class="btn btn-primary btn-lg">Go back to the dashboard</a>
    </div>
  </div>
</div>
@stop

@section('scripts')
@parent
<script type="text/javascript">
	//    $('.menu-toggler').trigger('click');
	$(".all-wrapper").addClass("hide-sub-menu");
	$('.menu-toggler').addClass('hidden');

</script>
@stop





