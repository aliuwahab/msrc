@extends('mk_testing.app_testing')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">

                </div>

                <div class="card">
                    <div class="card-header">Targeted Instructions</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('filter_attendance') }}">
                        <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="academic_year" class="col-form-label text-md-right">Academic Year</label>
                                        <select class="form-control select" style="width: 100%;" name="academic_year" required id="academic_year">
                                            <option value="2014/2015">2014/2015</option>
                                            <option value="2015/2016">2015/2016</option>
                                            <option value="2016/2017">2016/2017</option>
                                            <option value="2017/2018">2017/2018</option>
                                            <option value="2018/2019">2018/2019</option>
                                            <option value="2019/2020">2019/2020</option>
                                            <option value="2020/2021">2020/2021</option>
                                            <option value="2021/2022">2021/2022</option>
                                            <option value="2022/2023">2022/2023</option>
                                            <option value="2023/2024">2023/2024</option>
                                            <option value="2024/2025">2024/2025</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="term" class="col-form-label text-md-right">Term</label>
                                        <select class="form-control select" style="width: 100%;" name="term" required id="term">
                                            <option value="pdf">Term 1</option>
                                            <option value="pdf">Term 2</option>
                                            <option value="pdf">Term 3</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="course_type" class="col-form-label text-md-right">Action</label>
                                        <button type="submit" class="form-control btn btn-primary">
                                            Load Data
                                        </button>
                                    </div>
                                </div>

                        </div>
                        </form>


                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col" width="5%">#</th>
                                    <th scope="col">Total Pupils Enrolled</th>
                                    <th scope="col">Average Attendance</th>
                                    <th scope="col">Student Attendance Rate</th>
                                    <th scope="col">Teacher Attendance</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($attendance as $key => $attend)
                                    <tr>
                                        {{--<td>{{ $value->id }}</td>--}}
                                        {{--<td>{{ $value->name }}</td>--}}
                                        {{--<td>{{ $value->email }}</td>--}}
                                        {{--<td>{{ $value->nerd_level }}</td>--}}
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
