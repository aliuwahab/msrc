
<?php
header("Expires: Sat, 25 Jun 2002 05:00:00 GMT");
header('Pragma: public');
header('Cache-Control: max-age=86400');
header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
header('Content-Type: image/png, image/jpeg, image/jpeg');
header('Cache-Control: max-age=864000, public');

if (substr_count($_SERVER["HTTP_ACCEPT_ENCODING"], "gzip")) ob_start("ob_gzhandler"); else ob_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="ico/favicon.ico">

    <title>Ghana School Monitoring System</title>

    <!-- Bootstrap core CSS -->
    <!--<link href="frontend/stylesheets/bootstrap.css" rel="stylesheet">-->

    <!-- Custom styles for this template -->
    <link href="frontend/stylesheets/style.css" rel="stylesheet"/>
    <link href="frontend/stylesheets/default.css" rel="stylesheet"/>

    <!-- Resources -->
    <link href="frontend/stylesheets/font-awesome.min.css" rel="stylesheet"/>
    <link href="frontend/stylesheets/animate.css" rel="stylesheet"/>
    <link href="frontend/stylesheets/lightbox.css" rel="stylesheet"/>

    <!-- Favicon-->
    	<link href="/img/favicon.ico" rel="icon" type="image/ico" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body ng-app="publicApp" ng-controller="psGlobalController">

    <!-- Navigation -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/#">
          	<img class="icon" src="/img/blue_logo_150.png" alt="mSRC logo" >
          <!--  <img src="frontend/images/unicef_icon.jpg" alt="..."> -->
          </a>
        </div>

        <div class="collapse navbar-collapse">

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown nav-home">
              <a href="/#" ui-sref="home" class="dropdown-toggle" data-toggle="dropdown">Home</a>
            </li>
            <li class="dropdown nav-schools">
              <a href="/#schools" ui-sref="schools" class="dropdown-toggle" data-toggle="dropdown">Schools</a>
            </li>
            <li class="dropdown nav-regions">
              <a class="dropdown-toggle">Regions <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="/#regions/5">Ashanti</a></li>
                <li><a href="/#regions/4">Brong-Ahafo</a></li>
                <li><a href="/#regions/6">Central</a></li>
                <li><a href="/#regions/8">Eastern</a></li>
                <li><a href="/#regions/10">Greater Accra</a></li>
                <li><a href="/#regions/3">Northern</a></li>
                <li><a href="/#regions/2">Upper East</a></li>
                <li><a href="/#regions/1">Upper West</a></li>
                <li><a href="/#regions/9">Volta</a></li>
                <li><a href="/#regions/7">Western</a></li>

              </ul>
            </li>
            <li class="dropdown nav-contact">
              <a href="#/contact" ui-sref="contact-us" class="dropdown-toggle" data-toggle="dropdown">Contact</a>
            </li>
            <li class="dropdown nav-about">
             <a href="#/about-us" ui-sref="about-us" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
            </li>

          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div> <!-- / .navigation -->

    <!-- Begin ng View -->
    <section class="" ui-view>

    </section>


    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <!-- Contact Us -->
          <div class="col-sm-6">
            <h4><i class="fa fa-map-marker text-blue"></i> Contact Us</h4>
            <p>Do not hesitate to contact us if you have any questions or feature requests:</p>
            <p>
             Ghana Education Service<br />
              P.O Box M45, Ministries<br />
              Accra - Ghana.<br/>
              Phone: +233 209 046 782 ||  +233 243 585 822 <br />
              Email: <a href="">info@msrcghana.org</a>
            </p>
          </div>
          <!-- Recent Tweets -->
          <div class="col-sm-6" style="min-height:250px">
          <a class="twitter-timeline"
           href="https://twitter.com/msrcghana"
           data-widget-id="513409383561056258"
             height="400"
             data-chrome="nofooter noheader noborders noscrollbar transparent"
              data-tweet-limit="5"
           >Tweets by @msrcghana</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<!--      <a class="twitter-timeline" href="https://twitter.com/msrcGh" data-widget-id="507257572001193984">Tweets by @msrcGh</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>-->

          </div>

        </div>
      </div>
    </footer>

    <!-- Copyright -->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="copyright">
            Copyright 2015 - <a href="/admin" style="cursor: text; color: black;"><i class="fa fa-wrench"></i></a>Site developed by <a href="http://www.techmerge.org">TechMerge</a> | All Rights Reserved
          </div>
        </div>
      </div>  <!-- / .row -->
    </div> <!-- / .container -->

    <!-- begin loading spinner -->
    <section class="loader" ng-show="loading">
      <img src="/frontend/images/ajax_loader_large_white.gif" />
    </section> <!-- end loading spinner -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="frontend/scripts/jquery/jquery.js"></script>
    <script type="text/javascript" src="frontend/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="frontend/scripts/jquery/jquery.lazyload.min.js"></script>

    <!-- Angular scripts -->
    <script type="text/javascript" src="scripts/angular/angular.min.js"></script>
    <script type="text/javascript" src="scripts/angular/angular-resource.min.js"></script>
    <script type="text/javascript" src="scripts/angular/angular-route.min.js"></script>
    <script type="text/javascript" src="scripts/angular/angular-ui-router.min.js"></script>


    <!-- ui bootstrap -->
    <script type="text/javascript" src="frontend/scripts/angular/ui-bootstrap-tpls-0.11.0.js"></script>

    <!-- Angular MainApp, Controllers, Services and Directives-->
    <script type="text/javascript" src="frontend/app/app.js"></script>
    <script type="text/javascript" src="frontend/app/services/psFeaturedSchool.js"></script>


    <script type="text/javascript" src="frontend/app/controllers/psHomeController.js"></script>
    <script type="text/javascript" src="frontend/app/controllers/psSchoolsController.js"></script>
    <script type="text/javascript" src="frontend/app/controllers/psFirstSchoolController.js"></script>
    <script type="text/javascript" src="frontend/app/controllers/psSecondSchoolController.js"></script>
    <script type="text/javascript" src="frontend/app/controllers/psRegionController.js"></script>
    <script type="text/javascript" src="frontend/app/controllers/psGlobalController.js"></script>
    <script type="text/javascript" src="frontend/app/controllers/psContactController.js"></script>
    <script type="text/javascript" src="frontend/app/controllers/psAboutController.js"></script>


    <script type="text/javascript" src="frontend/scripts/scrolltopcontrol.js"></script>
    <script type="text/javascript" src="frontend/scripts/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="frontend/scripts/custom.js"></script>
    <script type="text/javascript" src="frontend/scripts/index.js"></script>



  </body>

</html>