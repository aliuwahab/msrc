<!DOCTYPE html>
<html >
<head>
	<meta charset="UTF-8">
	<title>Mobile 4 Development</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!--All Css Files-->
    <link rel="stylesheet" href="/backend/msrc.css">
</head>

    <body >


        <div class="margin text-center" style="margin-top : 10%">
                <h4 class="page_header alert bg-red" style="font-size: 23px"><i class="fa fa-exclamation-triangle"></i> 404 - Page Not Found</h4>
                <br/>
               <p style="font-size: 20px">
                  Sorry, the page you requested does not exist. Please check your request or reach out to system admin for assistance or
                    click <a href="/" class="text-aqua">here</a> to go back to the home page.
               </p>

            </div>
        </div>


    </body>
</html>