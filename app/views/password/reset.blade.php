<! --
// @extends('layouts.master')
//
//@section('content')
//    <h1>Set Your New Password</h1>
//
//    {{ Form::open(array('url' => '/reset/save')) }}
//        <input type="hidden" name="token" value="{{ $token }}">
//
//        <div>
//            {{ Form::label('email', 'Email Address:') }}
//            {{ Form::email('email') }}
//        </div>
//
//        <div>
//            {{ Form::label('password', 'Password:') }}
//            {{ Form::password('password') }}
//        </div>
//
//        <div>
//            {{ Form::label('password_confirmation', 'Password Confirmation:') }}
//            {{ Form::password('password_confirmation') }}
//        </div>
//
//        <div>
//            {{ Form::submit('Submit') }}
//        </div>
//    </form>
//
//    @if (Session::has('error'))
//        <p style="color: red;">{{ Session::get('error') }}</p>
//    @endif
//@stop

-->

<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
   <title>mSRC: Schools that learn</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="/stylesheets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="/stylesheets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="/stylesheets/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body >

<div class="form-box" id="login-box">
<a href="/">
    <div class="header">
     <img  src="/img/white_logo_150.png" class="img-responsive" alt="mSRC logo"
        style="margin: auto; width: 37%">
        <p style="margin: auto; text-align: center"><small><b>Schools that learn</b></small></p>
    </div>
    </a>

    <form action="/reset/save" method="post">
     <input type="hidden" name="token" value="{{ $token }}">

        <div class="body bg-gray">
            <div class="alert alert-info" style="margin: auto; text-align: center">
            Set new password
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email"/>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password"/>
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password"/>
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-green btn-block">Reset Password</button>

 <p style="text-align: center"><a href="/admin">Back to Login</a></p>

        </div>
    </form>

</div>


<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
<script src="/scripts/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="/scripts/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>