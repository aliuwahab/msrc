
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
   <title>mSRC: Schools that learn</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="/stylesheets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="/stylesheets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="/stylesheets/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body >

<div class="form-box" id="login-box">
<a href="/">
    <div class="header">
     <img  src="/img/white_logo_150.png" class="img-responsive" alt="mSRC logo"
        style="margin: auto; width: 37%">
        <p style="margin: auto; text-align: center"><small><b>Schools that learn</b></small></p>
    </div>
    </a>

    <form action="/remind/email" method="post">
        <div class="body bg-gray">
            <div class="alert alert-info" style="margin: auto; text-align: center">
             Please enter admin email used to create the account
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email"/>
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-yellow btn-block">Recover Password</button>

         <p style="text-align: center"><a href="/admin">Back to Login</a></p>
        </div>

    </form>

</div>


<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
<script src="/scripts/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="/scripts/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
