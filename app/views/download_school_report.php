<?php
if (substr_count($_SERVER["HTTP_ACCEPT_ENCODING"], "gzip")) ob_start("ob_gzhandler"); else ob_start();

?>
<!--$url =  "{$_SERVER['HTTP_HOST']}";
echo($url);-->
<!DOCTYPE html>
<html><!--manifest="../msrc.appcache"-->
<head>
    <meta charset="UTF-8">
    <title>mSRC: Schools that learn</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!--All Css Files-->
    <link rel="stylesheet" href="/backend/msrc.css?v=1523370231473">
    <!-- Favicon-->
    <link href="/img/favicon.ico" rel="icon" type="image/ico" />

    <!--CDN FILES BEING USED FOR CACHING AND ALSO REDUCE SCRIPT FILE SIZE-->
    <!--JQUERY CDN FILES-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <!--ANGULAR CDN FILES-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-animate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular-resource.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>

<body class="fixed hide_until_angular-loads" ng-class="envCheck ? 'skin-black' : 'skin-blue'" ng-app="downloadReport">
<!-- header logo: style can be found in header.less -->

<header class="header">
    <a ui-sref="dashboard.home" class="logo icon">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        <img class="icon" src="/img/white_logo_150.png" alt="mSRC logo" style="width: 37%">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="" class="navbar-btn sidebar-toggle hidden" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">

        </div>
    </nav>
</header>

<div class="wrapper row-offcanvas row-offcanvas-left" ng-controller="scGlobalController">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas hidden">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/avatar.png" class="img-circle" alt="Avatar" />
                </div>

            </div>

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="" ><!--right-side-->
        <!-- Content Header (Page header) -->
        <section class="content-header hide_until_angular-loads" style="display : none">
            <h1>
                {{ content_header }}
                <small ng-bind="content_header_small"></small>
            </h1>
            <ol class="breadcrumb">
                <li><a ui-sref="dashboard.home"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active" ng-bind="selected_page"></li>
            </ol>
        </section>

        <!-- Main content -->
        <!--<section class="content" ng-view>-->
        <!-- <section class="content" ui-view autoscroll="true">-->

        <section class="content ng-cloak" ui-view >


        </section><!-- /.content -->



        <section class="loader" ng-show="loading">

            <img src="/img/ajax_loader_large_white.gif" />

        </section>

    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->



<toaster-container></toaster-container>
<!--<toaster-container toaster-options="close-button : true, limit : 5"></toaster-container>-->

<!--Begin loading JavaScript files -->

<!--Jquery test and load from local files-->
<script>window.jQuery || document.write('<script src="/backend/jquery.js"><\/script>');</script>

<!--Angular  test and load from local files-->
<script type="text/javascript">
    if(typeof angular == 'undefined') {
        document.write('<script type="text/javascript" src="/backend/angular.js"><\/script>');
        console.log("loading angular from local");
    }
</script>


<!--All script and vendor Files-->
<script type="text/javascript" src="/backend/scripts.js?v=1523370231473"></script>

<!--MSRC Angular Script files-->
<script type="text/javascript" src="/mobile/down_report_msrc.js?v=1523370231473"></script>

<!--Html Partials using template caching-->
<script src="/mobile/templates/report_app.js?v=1523370231473"></script>


<script src="/backend/large_scripts.js?v=1523370231473"></script>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>-->


<!--Load this last scripts in a non-blocking way, since we dont need it in the initial stages of the app, and its very large in size too-->
<!--<script>-->
    <!--var script = document.createElement("script");-->
    <!--script.type = "text/javascript";-->
    <!--script.src = "backend/large_scripts.js?v=1523370231473";-->
    <!--document.body.appendChild(script);-->
<!--</script>-->


</body>
</html>
