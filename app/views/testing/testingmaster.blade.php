<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>This is for Testing By Aliu</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script type="text/javascript" src="/scripts/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/scripts/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src='/scripts/js/bootstrap_combined.js'></script>
	<script type="text/javascript" src='/scripts/js/pusher.min.js'></script>
	<script type="text/javascript" src='/scripts/js/pusher_receiver.js'></script>
	<!-- {{ HTML::script('js/functions.js') }} -->
</head>
<body>

		<div class="container">
			@yield('container')
		</div>
	
</body>
</html>