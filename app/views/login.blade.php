<?php
$message = Session::get('message');
?>

<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
   <title>mSRC: Schools that learn</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!--All Css Files-->
    <link rel="stylesheet" href="/backend/msrc.css">

</head>

<body >

<div class="form-box" id="login-box">

<a href="/">

    <div class="header">
     <img  src="/img/white_logo_150.png" class="img-responsive" alt="mSRC logo"
        style="margin: auto; width: 37%">
        <!--<img src="../img/unicef_icon.jpg" class="img-circle" alt="Unicef Icon" style="width: 50px" />-->
        <p style="margin: auto; text-align: center"><small><b>Schools that learn</b></small></p>
        <!--<img src="../img/unicef_icon.jpg" class="img-circle" alt="Unicef Icon" style="width: 50px" />-->
    </div>

</a>

    <form action="/login" method="post">
        <div class="body bg-gray">
            @if ($message)
            <div class="alert alert-danger" style="margin: auto; text-align: center">
               Login failed.<br/>Kindly check the username and password
            </div>
            @else
            <div class="alert alert-info" style="margin: auto; text-align: center">
             Please enter your login details
            </div>
            @endif
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="User ID"/>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password"/>
            </div>
            <div class="form-group">
                <input type="checkbox" name="remember_me"/> Remember me
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-aqua btn-block">Sign in</button>

            <p style="text-align: center"><a href="/password">I forgot my password</a></p>

            <!-- <a href="register.html" class="text-center">Register a new membership</a> -->
        </div>
    </form>

    <!--  <div class="margin text-center">
          <span>Sign in using social networks</span>
          <br/>
          <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
          <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
          <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

      </div>-->
</div>

<!--- jQuery 1.10.2-->
<!--<script src="/backend/jquery.js"></script>-->

<!--All Other script Files-->
<!--<script type="text/javascript" src="/backend/msrc-scripts.js"></script>-->

</body>
</html>