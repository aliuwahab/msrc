<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Data Sharing Within App</title>
	<link rel="stylesheet" href="">
</head>
<body>

<p>Hello {{$salutation}} {{$intro}}</p>

<h3>{{$content}}</h3>

<h6>{{$warning}}</h6>
	
</body>
</html>