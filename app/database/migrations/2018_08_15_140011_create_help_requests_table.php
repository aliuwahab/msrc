<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHelpRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('help_requests', function(Blueprint $table) {

			$table->bigIncrements('id');
            $table->enum('help_request_format',array('text','audio','video'))->default('text')->index();
            $table->mediumText('request_title');
			$table->mediumText('request_description')->nullable();
            $table->enum('help_request_source',array('app','sms','whatsapp','email','ivr','direct_entry'))->default('app')->index();
            $table->enum('help_request_status',array('pending','assigned','progress','resolved'))->default('pending')->index();
            $table->enum('help_requester_type',array('public','head_master','circuit_supervisor','admin'))->default('head_master')->index();
            $table->bigInteger('requester_id')->nullable();
            $table->enum('request_assign_to_user_type',array('admin','circuit_supervisor','head_master'))->default('admin')->index();
            $table->bigInteger('assigned_user_id')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('help_requests');
	}

}
