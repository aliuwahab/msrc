<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('regions', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->string('code')->index();
				$table->bigInteger('country_id')->index();
				$table->string('name');
				$table->string('email');
				$table->string('phone_number');
				$table->string('slug');
				$table->string('description');
				$table->string('lat_long');
				$table->boolean('status')->default(1);
            $table->boolean('is_deleted')->default(false);
//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('regions');
	}

}
