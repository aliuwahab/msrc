<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolTotalsForTermlyDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('termly_totals', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code');
			$table->integer('average_ghanaian_language_score')->default(0);
			$table->integer('ghanaian_language_above_average')->default(0);
			$table->integer('average_english_score')->default(0);
			$table->integer('english_above_average')->default(0);
			$table->integer('average_maths_score')->default(0);
			$table->integer('maths_above_average')->default(0);
			$table->integer('num_ghanaian_language_books')->default(0);
			$table->integer('num_english_books')->default(0);
			$table->integer('num_maths_books')->default(0);
			$table->integer('data_collector_id')->index();
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->string('year')->index();
			$table->enum('term',array('first_term','second_term','third_term'))->index();
			$table->bigInteger('country_id')->default(1)->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_totals_for_termly_data');
	}

}
