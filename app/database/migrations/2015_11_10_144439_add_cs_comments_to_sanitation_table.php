<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCsCommentsToSanitationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sanitation', function(Blueprint $table)
		{
			$table->text('cs_comments')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sanitation', function(Blueprint $table)
		{
			$table->dropColumn('cs_comments');
		});
	}

}
