<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMkStarsSchoolsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('mk_stars_schools')) {
            Schema::create('mk_stars_schools', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('school_code')->index(); //Same as ges_code
                $table->string('district')->index();
                $table->string('circuit')->index();
                $table->string('school_name')->index();
                $table->string('circuit_status')->nullable();
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('mk_stars_schools');
	}

}
