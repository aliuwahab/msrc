<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateELibraryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('e_library', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->enum('targeted_groups', array('t_one','t_two','t_one_and_t_two','no_restriction'))->default('no_restriction');
			$table->enum('file_type', array('pdf','word','excel','image','video'))->default('pdf');
			$table->mediumText('file_s3_url');
			$table->mediumText('file_title')->nullable();
			$table->mediumText('file_description')->nullable();


            $table->integer('number_of_views')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->enum('status',array('approved','not_approved'))->default('approved')->index();
            $table->boolean('is_deleted')->default(false);


//			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('e_library');
	}

}
