<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolTotalsForWeeklyDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('weekly_totals', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code')->default(0)->index();
			$table->integer('normal_enrolment_total_boys')->default(0);
			$table->integer('normal_enrolment_total_girls')->default(0);
			$table->integer('normal_enrolment_total_students')->default(0);
			$table->integer('normal_attendance_total_boys')->default(0);
			$table->integer('normal_attendance_total_girls')->default(0);
			$table->integer('normal_attendance_total_students')->default(0);
			$table->integer('special_enrolment_total_boys')->default(0);
			$table->integer('special_enrolment_total_girls')->default(0);
			$table->integer('special_enrolment_total_students')->default(0);
			$table->integer('special_attendance_total_boys')->default(0);
			$table->integer('special_attendance_total_girls')->default(0);
			$table->integer('special_attendance_total_students')->default(0);
			$table->integer('average_teacher_attendance')->default(0);
			$table->integer('average_teacher_class_management')->default(0);
			$table->integer('number_of_days_school_was_in_session')->default(0);
			$table->integer('data_collector_id')->index();
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->string('year')->index();
			$table->enum('term',array('first_term','second_term','third_term'))->index();
			$table->bigInteger('week_number');
			$table->bigInteger('country_id')->default(1)->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_totals_for_weekly_data');
	}

}
