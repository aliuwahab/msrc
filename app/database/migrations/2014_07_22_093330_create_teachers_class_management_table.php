<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeachersClassManagementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teachers_class_management', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('teacher_id');
			$table->string('school_reference_code')->index();
			$table->string('questions_category_reference_code');
			$table->enum('class_control',array('good','fair','poor','N/A'))->index();
			$table->enum('children_behaviour',array('good','fair','poor','N/A'))->index();
			$table->enum('children_participation',array('good','fair','poor','N/A'))->index();
			$table->enum('teacher_discipline',array('good','fair','poor','N/A'))->index();
			$table->string('comment');
			$table->enum('term',array('first_term','second_term','third_term'))->index();
			$table->string('week_number')->index();
			$table->string('year')->index();
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->bigInteger('data_collector_id')->index();
			$table->bigInteger('country_id')->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teachers_class_management');
	}

}
