<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->integer('country_id')->default(1);
				$table->string('username')->index();
				$table->string('firstname');
				$table->string('lastname');
				$table->enum('gender',array('male','female'));
				$table->string('phone_number');
				$table->string('email')->index();
				$table->string('password');

				$table->enum('dashboard_access_type', array('msrc', 'targeted_instructions','all'))->default('msrc');
				$table->enum('level', array('national', 'regional', 'district', 'circuit', 'school'))->index();
				$table->enum('role', array('super', 'normal'));
				$table->bigInteger('level_id')->index()->unsigned()->default(1)->index();
				$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
//				$table->timestamps();
				$table->string('remember_token');
                $table->string('last_login');
                $table->boolean('is_deleted')->default(false);
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('users');

	}

}
