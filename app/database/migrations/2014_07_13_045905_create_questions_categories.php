<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuestionsCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questions_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('name', array('Teacher Attendance','Student Attendance',
				'Facilities, Safety and Sanitation','Lesson Plans', 
				'Teaching and Learning Materials','Community Involvement',
				'School Class Enrolment','Student Class Attendance',
				'Class Text Books','Student Class Performance',
				'School Management','Support Type','School Grant',
				'School Teachers Information','An Inclusive School',
				'Effective Teaching and Learning','Healthy Level of School',
				'Safe and Protective Schools','Friendly School for Both Girls and Boys',
				'Community Involvement in the School','School Information','Head Teacher Details',
				'School Problems','Level of community/school relationship',
				'Punctuality And Lessons Plans','Teacher Classroom Management','School Special Students Enrolment',
				'Special Student Class Attendance','School General Situation','Pupils Exams Perormance Average',
				'Number Of Units Covered By Teacher'))->index();
			$table->string('question_category_code')->index();
			$table->string('description');
			$table->integer('total_measure');
			$table->enum('data_category_code_reference',array('schoolvisits','schoolreportcard','schoolreview'))->index();
			$table->bigInteger('country_id')->index();
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questions_categories');
	}

}
