<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sms_messages', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->enum('from', array('admin','circuit_supervisor','head_teacher'));
			$table->bigInteger('sender_db_id')->nullable();
			$table->enum('recipient_type', array('admin','circuit_supervisor','head_teacher'));
			$table->bigInteger('recipient_db_id')->nullable();
			$table->string('recipient_number');
			$table->string('message');
			$table->bigInteger('country_id')->default(1)->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
		Schema::drop('sms_messages');
		
	}

}
