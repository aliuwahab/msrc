<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('answers', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('itenary_id')->default(0);
			$table->string('school_code_reference');
			$table->string('question_code_reference');
			$table->string('answer')->nullable();
			$table->string('location_of_submission')->nullable();
			$table->text('comment')->nullable();
			$table->enum('question_type', array('single_choice','multiple_choice','open_ended'))->nullable();
			$table->bigInteger('country_id')->default(1)->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('answers');
	}

}
