<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberOfExcercisesGivenToPunctualityAndLessonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('punctuality_and_lessons', function(Blueprint $table)
		{
			$table->integer('num_of_exercises_given')->nullable();

			$table->integer('num_of_exercises_marked')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('punctuality_and_lessons', function(Blueprint $table)
		{
			$table->dropColumn('num_of_exercises_given');

			$table->dropColumn('num_of_exercises_marked');
		});
	}

}
