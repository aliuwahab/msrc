<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpecialStudentsEnrolmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('special_enrolment', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code')->index();
			$table->string('questions_category_reference_code')->index();
			$table->enum('level', array('KG1','KG2','P1','P2','P3','P4','P5','P6','JHS1','JHS2','JHS3'))->index();
			$table->integer('num_males');
			$table->integer('num_females');
			$table->integer('num_of_streams');
			$table->integer('total_enrolment');
			$table->string('year')->index();
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->bigInteger('data_collector_id')->index();
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->integer('week_number')->index();
			$table->bigInteger('country_id')->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('special_students_enrolment');
	}

}
