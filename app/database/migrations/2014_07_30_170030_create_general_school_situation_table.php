<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGeneralSchoolSituationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('general_school_situation', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('data_collector_type', array('head_teacher','circuit_supervisor'));
			$table->bigInteger('data_collector_id');
			$table->string('situation');
			$table->enum('status',array('yes','no'));
			$table->string('comment');
			$table->string('date');
			$table->integer('week_number');
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->string('year');
			$table->bigInteger('country_id')->default(1);
			$table->bigInteger('region_id');
			$table->bigInteger('district_id');
			$table->bigInteger('circuit_id');
			$table->bigInteger('school_id');
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('general_school_situation');
	}

}
