<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMkPollsQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('mk_polls_questions')) {
            Schema::create('mk_polls_questions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('district_id')->index();
                $table->integer('circuit_id')->index();
                $table->string('school_name')->index();
                $table->string('school_id')->index();
                $table->string('school_code')->index();
                $table->string('phone_number')->index();
                $table->string('head_teacher_name')->nullable();
                $table->integer('data_collector_id')->index();
                $table->integer('poll_question_id')->nullable();
                $table->text('poll_answer')->nullable();
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('mk_polls_questions');
	}

}
