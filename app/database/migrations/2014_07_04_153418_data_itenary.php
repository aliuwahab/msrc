<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DataItenary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('itenary', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name');
			$table->string('description');
			$table->string('slug');
			$table->date('start_date');
			$table->date('end_date');
			$table->bigInteger('country_id')->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('itenary');
	}

}
