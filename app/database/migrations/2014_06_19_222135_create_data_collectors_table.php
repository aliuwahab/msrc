<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataCollectorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('data_collectors', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->string('first_name');
				$table->string('last_name');
				$table->enum('gender',array('male', 'female'));
				$table->string('employed_date_as_teacher');
				$table->string('employed_date_as_headteacher');
				$table->string('employed_date_as_circuit_supervisor');
				$table->string('period');
				$table->string('email');
				$table->string('identity_code');
				$table->string('universal_code')->index();
				$table->string('phone_number')->nullable();
				$table->enum('type', array('head_teacher', 'circuit_supervisor'));
				$table->bigInteger('country_id')->index();
				$table->bigInteger('region_id')->index();
				$table->bigInteger('district_id')->index();
				$table->bigInteger('circuit_id')->index();
                $table->boolean('is_deleted')->default(false);
//                $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('data_collectors');
	}

}
