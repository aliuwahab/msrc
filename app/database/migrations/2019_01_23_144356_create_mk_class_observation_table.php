<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMkClassObservationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('mk_class_observation')) {
            Schema::create('mk_class_observation', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('data_collector_id')->index();
                $table->enum('data_collector_type', array('head_teacher', 'circuit_supervisor'))->index();
                $table->integer('saved_class_ob_id')->index();
                $table->string('class_ob_teacher_name')->index();
                $table->string('class_ob_level')->index();
                $table->string('class_ob_subject')->index();
                $table->string('class_ob_date_monitoring')->index();
                $table->integer('class_ob_enrolled_p_four')->default(0);
                $table->integer('class_ob_enrolled_p_five')->default(0);
                $table->integer('class_ob_enrolled_p_six')->default(0);
                $table->integer('class_ob_enrolled_p_total')->default(0);
                $table->integer('class_ob_absent_p_four')->default(0);
                $table->integer('class_ob_absent_p_five')->default(0);
                $table->integer('class_ob_absent_p_six')->default(0);
                $table->integer('class_ob_absent_p_total')->default(0);
                $table->string('class_ob_duration');
                $table->string('class_ob_attendance_summary');
                $table->string('areas_summary');
                $table->string('areas_improvement');

                $table->string('ti_question_one_answer');
                $table->string('ti_question_two_answer');
                $table->string('ti_question_three_answer');
                $table->string('ti_question_one_comment');
                $table->string('ti_question_two_comment');
                $table->string('ti_question_three_comment');
                $table->string('ti_lesson_summary');

                $table->string('la_question_one_answer');
                $table->string('la_question_two_answer');
                $table->string('la_question_three_answer');
                $table->string('la_question_one_comment');
                $table->string('la_question_two_comment');
                $table->string('la_question_three_comment');
                $table->string('la_lesson_summary');

                $table->string('cl_question_one_reading_answer');
                $table->string('cl_question_one_saying_answer');
                $table->string('cl_question_one_doing_answer');
                $table->string('cl_question_one_writing_answer');
                $table->string('cl_question_one_speaking_answer');
                $table->string('cl_question_two_answer');
                $table->string('cl_question_three_answer');
                $table->string('cl_question_one_comment');
                $table->string('cl_question_two_comment');
                $table->string('cl_question_three_comment');
                $table->string('cl_question_four_comment');
                $table->string('cl_classroom_summary');

                $table->integer('school_id')->index();
                $table->integer('district_id')->index();
                $table->integer('circuit_id')->index();
                $table->integer('country_id')->index();
                $table->integer('region_id')->index();

                $table->double('longitude')->default(0.0);
                $table->double('latitude')->default(0.0);
                $table->boolean('is_deleted')->default(false);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('mk_class_observation');
	}

}
