<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolReportcardSubmissionStatusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('school_reportcard_submission_status', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('data_collector_type', array('head_teacher','circuit_supervisor'));
			$table->bigInteger('data_collector_id');
			$table->enum('type', array('sanitation','security','recreation','structure','furniture',
				'performance','relations','meeting','school_class_textbooks',
				'school_support_types','pupils_performance','school_grants_payments'));
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->string('year');
			$table->bigInteger('country_id')->default(1);
			$table->bigInteger('region_id');
			$table->bigInteger('district_id');
			$table->bigInteger('circuit_id');
			$table->bigInteger('school_id');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_reportcard_submission_status');
	}

}
