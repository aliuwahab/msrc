<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePupilsExamsPerformanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pupils_exams_performance', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code');
			$table->string('questions_category_reference_code');
			$table->enum('level', array('KG1','KG2','P1','P2','P3','P4','P5','P6','JHS1','JHS2','JHS3'))->index();
			$table->string('average_ghanaian_language_score');
			$table->integer('num_pupil_who_score_above_average_in_ghanaian_language');
			$table->string('average_english_score');
			$table->integer('num_pupil_who_score_above_average_in_english');
			$table->string('average_maths_score');
			$table->integer('num_pupil_who_score_above_average_in_maths');
			$table->string('comment');
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->string('year');
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->bigInteger('data_collector_id')->index();
			$table->bigInteger('country_id');
			$table->bigInteger('region_id');
			$table->bigInteger('district_id');
			$table->bigInteger('circuit_id');
			$table->bigInteger('school_id');
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pupils_exams_performance');
	}

}
