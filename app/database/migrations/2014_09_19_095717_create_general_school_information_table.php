<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGeneralSchoolInformationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('general_school_information', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code')->index();
			$table->string('total_number_of_days_in_a_term');
			$table->string('total_number_of_instructional_days_during_a_term');
			$table->enum('term',array('first_term','second_term','third_term'))->index();
			$table->bigInteger('week_number');
			$table->string('year')->index();
			$table->bigInteger('country_id')->default(1)->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->integer('data_collector_id')->index();
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('general_school_information');
	}

}
