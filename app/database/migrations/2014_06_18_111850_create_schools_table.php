<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('schools', function (Blueprint $table) {
				$table->bigIncrements('id');
				$table->string('ges_code')->index();
				$table->string('school_code')->index();
				$table->string('sms_code')->index();
				$table->bigInteger('region_id')->index();
				$table->bigInteger('district_id')->index();
				$table->bigInteger('circuit_id')->index();
				$table->bigInteger('country_id')->index();
				$table->bigInteger('headteacher_id')->index()->nullable();
				$table->bigInteger('supervisor_id')->index()->nullable();
				$table->string('email')->nullable();
				$table->string('phone_number')->nullable();
				$table->string('name');
				$table->string('ghanaian_language');
				$table->string('category');
				$table->string('slug');
				$table->string('address');
				$table->boolean('shift')->default(0);
				$table->string('description');
				$table->string('lat_long');
				$table->boolean('status')->default(1);
				$table->double('current_state_of_community_involvement')->default(0);
				$table->double('current_state_of_teaching_and_learning')->default(0);
				$table->double('current_state_of_friendliness_to_boys_girls')->default(0);
				$table->double('current_state_of_healthy_level')->default(0);
				$table->double('current_state_of_safety_and_protection')->default(0);
				$table->double('current_state_of_school_inclusiveness')->default(0);
				$table->double('current_teacher_attendance_by_headteacher')->default(0);
				$table->double('current_teacher_attendance_by_circuit_supervisor')->default(0);
                $table->boolean('is_deleted')->default(false);
//                $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
                $table->enum('targeted_groups', array('t_one', 't_two','none'))->default('none');


			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('schools');
	}

}
