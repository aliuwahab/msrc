<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeachersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teachers', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('school_reference_code')->nullable()->index();
			$table->string('questions_category_reference_code')->default('STI')->index();
			$table->string('first_name');
			$table->string('last_name')->nullable();
			$table->enum('gender',array('male','female'));
			$table->enum('level_taught',array('shs','jhs','primary','kg','nursery'));
			$table->string('class_subject_taught')->nullable();
			$table->string('class_taught')->nullable();
			$table->string('category')->nullable();
			$table->string('staff_number');
			$table->string('rank')->nullable();
			$table->string('highest_academic_qualification')->nullable();
			$table->string('highest_professional_qualification')->nullable();
			$table->string('years_in_the_school')->nullable();
			$table->enum('term',array('first_term','second_term','third_term'))->nullable()->index();
			$table->bigInteger('week_number')->nullable();
			$table->string('year')->nullable()->index();
			$table->bigInteger('country_id')->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->integer('data_collector_id')->nullable()->index();
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->nullable()->index();
			$table->string('lat')->nullable();
			$table->string('long')->nullable();
            $table->enum('teacher_status',array('atpost','studyleave','otherleave','retired','deceased'))->default('atpost')->index();
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teachers');
	}

}
