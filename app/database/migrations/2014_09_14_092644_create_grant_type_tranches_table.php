<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrantTypeTranchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grant_type_tranches', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code');
			$table->string('questions_category_reference_code');
			$table->enum('school_grant_type',array('school_grant','capitation'));
			$table->string('first_tranche_amount')->default(Null);
			$table->string('second_tranche_amount')->default(Null);
			$table->string('third_tranche_amount')->default(Null);
			$table->string('first_tranche_date')->default(Null);
			$table->string('second_tranche_date')->default(Null);
			$table->string('third_tranche_date')->default(Null);
			$table->string('comment')->default('empty');
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->string('year');
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->bigInteger('data_collector_id')->index();
			$table->bigInteger('country_id');
			$table->bigInteger('region_id');
			$table->bigInteger('district_id');
			$table->bigInteger('circuit_id');
			$table->bigInteger('school_id');
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grant_type_tranches');
	}

}
