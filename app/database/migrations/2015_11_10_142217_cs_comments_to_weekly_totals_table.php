<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CsCommentsToWeeklyTotalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('weekly_totals', function(Blueprint $table)
		{

			$table->text('cs_students_enrolment_comments')->nullable();
			$table->text('cs_students_attendance_comments')->nullable();
			$table->text('cs_teacher_attendance_comments')->nullable();
			$table->text('cs_teacher_class_management_comments')->nullable();


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('weekly_totals', function(Blueprint $table)
		{

			$table->dropColumn('cs_students_enrolment_comments');
			$table->dropColumn('cs_students_attendance_comments');
			$table->dropColumn('cs_teacher_attendance_comments');
			$table->dropColumn('cs_teacher_class_management_comments');
			
			
		});
	}

}
