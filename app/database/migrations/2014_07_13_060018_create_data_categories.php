<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDataCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('data_category_name',	array('School Visits','School Report Card','School Review'))->index();
			$table->enum('data_category_code',array('schoolvisits','schoolreportcard','schoolreview'))->index();
			$table->bigInteger('country_id')->index();
			$table->string('description');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('data_categories');
	}

}
