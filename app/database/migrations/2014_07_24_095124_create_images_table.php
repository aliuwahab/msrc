<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function(Blueprint $table) {
			$table->bigInteger('id'); //Error in production, so left it so.
			$table->bigIncrements('image_id');//This is now the primary ID.
			$table->string('image_url');
			$table->string('image_title');
			$table->string('image_description');
			$table->string('school_reference_code')->index();
			$table->string('year');
			$table->integer('country_id')->index();
			$table->integer('region_id')->index();
			$table->integer('district_id')->index();
			$table->integer('circuit_id')->index();
			$table->integer('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('images');
	}

}
