<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePunctualityAndLessonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('punctuality_and_lessons', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('teacher_id');
			$table->string('school_reference_code')->index();
			$table->string('questions_category_reference_code')->index();
			$table->integer('days_in_session')->default(0)->index();
			$table->integer('days_present')->index();
			$table->enum('days_punctual',array(0,1,2,3,4,5))->index()->default(0);
			$table->enum('days_absent_with_permission',array(0,1,2,3,4,5))->index()->default(0);
			$table->enum('days_absent_without_permission',array(0,1,2,3,4,5))->index()->default(0);
			$table->enum('lesson_plans',array('good','fair','poor','not_prepared','N/A'))->index();
			$table->string('comment')->index();
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->integer('week_number')->index();
			$table->string('year')->index();
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->bigInteger('data_collector_id')->index();
			$table->bigInteger('country_id')->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('punctuality_and_lessons');
	}

}
