<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSafeProtectiveTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('safe_protective', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('itenary_id');
			$table->string('school_code_reference');
			$table->string('question_code_reference');
			$table->string('submitter');
			$table->integer('answer');
			$table->text('comment');
			$table->string('year');
			$table->string('date');
			$table->bigInteger('country_id')->default(1)->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
			$table->bigInteger('school_id')->index();
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('safe_protective');
	}

}
