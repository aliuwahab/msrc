<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubmissionStatusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submission_status', function(Blueprint $table) {
			$table->increments('id');
			$table->string('data_collector_number');
			$table->string('name');
			$table->string('itenary_id');
			$table->boolean('status');
			$table->bigInteger('country_id')->index();
			$table->bigInteger('region_id')->index();
			$table->bigInteger('district_id')->index();
			$table->bigInteger('circuit_id')->index();
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submission_status');
	}

}
