<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCsCommentsToSchoolCommunityRelationshipTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('school_community_relationship', function(Blueprint $table)
		{
			$table->text('cs_comments')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('school_community_relationship', function(Blueprint $table)
		{
			$table->dropColumn('cs_comments');
		});
	}

}
