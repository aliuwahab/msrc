<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContinousAssessmentToSchoolRecordsPerformanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('school_records_performance', function(Blueprint $table)
		{

			$table->string('continous_assessment')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('school_records_performance', function(Blueprint $table)
		{

			$table->dropColumn('continous_assessment');
			
		});
	}

}
