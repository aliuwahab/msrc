<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScienceAndLiteracyToPupilsExamsPerformanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pupils_exams_performance', function(Blueprint $table)
		{
			$table->string('average_science_score')->nullable();
			$table->integer('num_pupil_who_score_above_average_in_science')->nullable();
			$table->integer('num_pupil_who_can_read_and_write')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pupils_exams_performance', function(Blueprint $table)
		{
			$table->dropColumn('average_science_score');
			$table->dropColumn('num_pupil_who_score_above_average_in_science');
			$table->dropColumn('num_pupil_who_can_read_and_write');
		});
	}

}
