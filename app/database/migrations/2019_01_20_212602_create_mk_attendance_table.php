<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMkAttendanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('mk_attendance')) {
            Schema::create('mk_attendance', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('data_collector_id')->index();
                $table->enum('data_collector_type', array('head_teacher', 'circuit_supervisor'))->index();
                $table->integer('saved_attendance_id')->index();
                $table->integer('school_id')->index();
                $table->integer('district_id')->index();
                $table->integer('circuit_id')->index();
                $table->integer('country_id')->index();
                $table->integer('region_id')->index();
                $table->string('attendance_academic_term')->index();
                $table->string('attendance_academic_year')->index();
                $table->string('attendance_academic_level')->index();
                $table->integer('attendance_academic_week')->default(0);
                $table->string('attendance_academic_subject')->index();
                $table->integer('attendance_no_pupil_level')->default(0);
                $table->integer('attendance_no_pupil_avg')->default(0);
                $table->integer('attendance_no_teacher_avg')->default(0);
                $table->double('longitude')->default(0.0);
                $table->double('latitude')->default(0.0);
                $table->boolean('is_deleted')->default(false);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('mk_attendance');
	}

}
