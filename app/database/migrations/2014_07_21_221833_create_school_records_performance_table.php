<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolRecordsPerformanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('school_records_performance', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code');
			$table->string('questions_category_reference_code');
			$table->enum('admission_register',array('yes','no'));
			$table->enum('class_registers',array('yes','no'));
			$table->enum('teacher_attendance_register',array('yes','no'));
			$table->enum('log',array('yes','no'));
			$table->enum('visitors',array('yes','no'));
			$table->enum('sba',array('yes','no'));
			$table->enum('movement',array('yes','no'));
			$table->enum('spip',array('yes','no'));
			$table->enum('inventory_books',array('yes','no'));
			$table->enum('cummulative_records_books',array('yes','no'));
			$table->string('comment');
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->string('year');
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->bigInteger('data_collector_id')->index();
			$table->bigInteger('country_id');
			$table->bigInteger('region_id');
			$table->bigInteger('district_id');
			$table->bigInteger('circuit_id');
			$table->bigInteger('school_id');
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_records_performance');
	}

}
