<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSessionDaysToSpecialEnrolment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('special_enrolment', function(Blueprint $table)
		{
			$table->integer('school_in_session')->nullable();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('special_enrolment', function(Blueprint $table)
		{
			$table->dropColumn('school_in_session');
			
		});
	}

}
