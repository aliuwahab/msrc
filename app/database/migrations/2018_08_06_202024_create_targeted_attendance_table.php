<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTargetedAttendanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('targeted_attendance', function(Blueprint $table) {
			$table->bigIncrements('id');
            $table->string('school_reference_code');
            $table->integer('primary_four_total_attendance')->default(0);
            $table->integer('primary_five_total_attendance')->default(0);
            $table->integer('primary_six_total_attendance')->default(0);


            $table->integer('number_of_week_days')->index();
            $table->integer('data_collector_id')->index();
            $table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
            $table->integer('week_number')->index();
            $table->string('year')->index();
            $table->enum('term',array('first_term','second_term','third_term'))->index();
            $table->bigInteger('country_id')->default(1)->index();
            $table->bigInteger('region_id')->index();
            $table->bigInteger('district_id')->index();
            $table->bigInteger('circuit_id')->index();
            $table->bigInteger('school_id')->index();
            $table->string('lat');
            $table->string('long');
            $table->boolean('is_deleted')->default(false);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->enum('submission_status',array('approved','re_submission_needed'))->default('approved')->index();

//			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('targeted_attendance');
	}

}
