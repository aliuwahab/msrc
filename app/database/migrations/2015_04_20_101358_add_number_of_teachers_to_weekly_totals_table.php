<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNumberOfTeachersToWeeklyTotalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('weekly_totals', function(Blueprint $table) {

			$table->integer('current_number_of_teachers')->default(0);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('weekly_totals', function(Blueprint $table) {

			$table->dropColumn(array('current_number_of_teachers'));

		});
	}

}
