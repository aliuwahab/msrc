<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMkPupilAggregatedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('mk_pupil_aggregated')) {
            Schema::create('mk_pupil_aggregated', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('data_collector_id')->index();
                $table->enum('data_collector_type', array('head_teacher', 'circuit_supervisor'))->index();
                $table->integer('saved_pupil_aggregated_id')->index();
                $table->string('academic_term')->index();
                $table->string('academic_year')->index();
                $table->string('grade')->index();

                $table->integer('no_student_grade')->default(0);
                $table->integer('no_student_assessed')->default(0);

                $table->integer('english_beginner_text')->default(0);
                $table->integer('english_letter_text')->default(0);
                $table->integer('english_paragraph_text')->default(0);
                $table->integer('english_word_text')->default(0);
                $table->integer('english_sentence_text')->default(0);
                $table->integer('english_level_one_text')->default(0);
                $table->integer('english_level_two_text')->default(0);
                $table->integer('english_level_three_text')->default(0);

                $table->integer('math_beginner_text')->default(0);
                $table->integer('math_digit_one_text')->default(0);
                $table->integer('math_digit_two_text')->default(0);
                $table->integer('math_addition_text')->default(0);
                $table->integer('math_subtraction_text')->default(0);
                $table->integer('math_level_one_text')->default(0);
                $table->integer('math_level_two_text')->default(0);
                $table->integer('math_level_three_text')->default(0);

                $table->integer('school_id')->index();
                $table->integer('district_id')->index();
                $table->integer('circuit_id')->index();
                $table->integer('country_id')->index();
                $table->integer('region_id')->index();

                $table->double('longitude')->default(0.0);
                $table->double('latitude')->default(0.0);
                $table->boolean('is_deleted')->default(false);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('mk_pupil_aggregated');
	}

}
