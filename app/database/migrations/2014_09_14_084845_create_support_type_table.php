<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupportTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('support_type', function(Blueprint $table) {
			$table->increments('id');
			$table->string('school_reference_code');
			$table->string('questions_category_reference_code');
			$table->string('donor_support_in_cash');
			$table->string('donor_support_in_kind');
			$table->string('community_support_in_cash');
			$table->string('community_support_in_kind');
			$table->string('district_support_in_cash');
			$table->string('district_support_in_kind');
			$table->string('pta_support_in_cash');
			$table->string('pta_support_in_kind');
			$table->string('other_support_in_cash');
			$table->string('other_support_in_kind');
			$table->string('comment');
			$table->enum('term',array('first_term','second_term','third_term'));
			$table->string('year');
			$table->enum('data_collector_type',array('head_teacher','circuit_supervisor'))->index();
			$table->bigInteger('data_collector_id')->index();
			$table->bigInteger('country_id');
			$table->bigInteger('region_id');
			$table->bigInteger('district_id');
			$table->bigInteger('circuit_id');
			$table->bigInteger('school_id');
			$table->string('lat');
			$table->string('long');
            $table->boolean('is_deleted')->default(false);

//            $table->timestamps();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('support_type');
	}

}
