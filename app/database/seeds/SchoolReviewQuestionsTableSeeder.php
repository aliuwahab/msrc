<?php

class SchoolReviewQuestionsTableSeeder extends Seeder {

	public function run()
	{
		

		DB::table('school_review_questions')->insert(array(
				array(
						'question_code' => 'AISQ1',
						'question_category_reference' => 'AIS',
						'question' => 'Do all children including those with special needs (children with disabilities, orphans , or from poor families) attend school?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'AISQ2',
						'question_category_reference' => 'AIS',
						'question' => 'Do teachers visit children\'s home regularly?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'AISQ3',
						'question_category_reference' => 'AIS',
						'question' => 'Does the school conduct a yearly enrolment campaign to attract non-attending or non-enrolled children?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'AISQ4',
						'question_category_reference' => 'AIS',
						'question' => 'Does the school have a system to regularly check on the attendance of its students and address problems concerning non-attendance?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'AISQ5',
						'question_category_reference' => 'AIS',
						'question' => 'Can children attend school without paying extra monies / levies / fees?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'ETLQ1',
						'question_category_reference' => 'ETL',
						'question' => 'Do teachers come to school regularly and punctually?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'ETLQ2',
						'question_category_reference' => 'ETL',
						'question' => 'Do teachers use different teaching methods, for example plenary lessons and dividing the class into small groups to work?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'ETLQ3',
						'question_category_reference' => 'ETL',
						'question' => 'Do teachers regularly inform parents about the progress of their children?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'ETLQ4',
						'question_category_reference' => 'ETL',
						'question' => 'Do teachers prepare lesson plan?',
						'question_type' => 'single_choice',
						),
						array(
						'question_code' => 'ETLQ5',
						'question_category_reference' => 'ETL',
						'question' => 'Does every child have a free textbook for each subject?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'HLSQ1',
						'question_category_reference' => 'HLS',
						'question' => 'Is there safe drinking water in the school (water from bore hole, stand pipe, drug wells, water reservoirs or filtered water)?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'HLSQ2',
						'question_category_reference' => 'HLS',
						'question' => 'Are there separate latrines for boys and girls in the school?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'HLSQ3',
						'question_category_reference' => 'HLS',
						'question' => 'Do children and teachers wash their hands with soap or ash?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'HLSQ4',
						'question_category_reference' => 'HLS',
						'question' => 'Do children lean about HIV/AIDS in school?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'HLSQ5',
						'question_category_reference' => 'HLS',
						'question' => 'Does the school have facilities and equipment for sports and play?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SPSQ1',
						'question_category_reference' => 'SPS',
						'question' => 'Does the school have a clear policy against criminal violent, or sexual activities?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SPSQ2',
						'question_category_reference' => 'SPS',
						'question' => 'Do the school have and enforce policies prohibiting corporal punishment (for ex. caning) and promoting positive disciplinary procedures?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SPSQ3',
						'question_category_reference' => 'SPS',
						'question' => 'Do vulnerable children (for ex. children with disabilities, orphans) receive guidance. special support at the school',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SPSQ4',
						'question_category_reference' => 'SPS',
						'question' => 'Is the school compound clean safe adn can children play safely? (for ex. no busy roads or rivers nearby?)',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SPSQ5',
						'question_category_reference' => 'SPS',
						'question' => 'Can children safely report a classmate senior or teacher who abuses them? (for ex. in case of beating, harassing or threatening)',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'FSBGBQ1',
						'question_category_reference' => 'FSBGB',
						'question' => 'Are children who are pregnant allowed to attend school?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'FSBGBQ2',
						'question_category_reference' => 'FSBGB',
						'question' => 'Are girls who have had a baby allowed to come back to school after delivery?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'FSBGBQ3',
						'question_category_reference' => 'FSBGB',
						'question' => 'Do teachers treat girls and boys equally? ( equal in class, assignments, access to
											materials, questions asked participation in sports)?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'FSBGBQ4',
						'question_category_reference' => 'FSBGB',
						'question' => 'Are there any female teachers in the school?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'FSBGBQ5',
						'question_category_reference' => 'FSBGB',
						'question' => 'Do girls and boys know who to report to in case of bullying or sexual harassment?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'CISQ1',
						'question_category_reference' => 'CIS',
						'question' => 'Do SMCs and PTA executives discuss and prepare the school performance improvement plans?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'CISQ2',
						'question_category_reference' => 'CIS',
						'question' => 'Do most parents attend PTA meetings?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'CISQ3',
						'question_category_reference' => 'CIS',
						'question' => 'Do parents or community members actively participate in school activities
									(eg. drama, sports, music, contests, open day, speech day, construction work,
									school feeding programme)?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'CISQ4',
						'question_category_reference' => 'CIS',
						'question' => 'Does the school develop and offer training to parents and community members to encourage helping children study at home?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'CISQ5',
						'question_category_reference' => 'CIS',
						'question' => 'Do community members volunteer to be involved in teaching at school?',
						'question_type' => 'single_choice',
						)
			));
	}

}
