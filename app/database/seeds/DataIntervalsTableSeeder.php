<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class DataIntervalsTableSeeder extends Seeder {

	public function run()
	{

			DataInterval::create([
				'name' => 'First Term',
				'description' => 'This is a test period, just noted to be first term',
				'slug' => 'FT',
				'period' => 'Sepetember - December'
			],
			[
				'name' => 'Second Term',
				'description' => 'This is test a period, just noted to be second term',
				'slug' => 'ST',
				'period' => 'January - April'
			],
			[
				'name' => 'Third Term',
				'description' => 'This is test period, just noted to be third term',
				'slug' => 'TT',
				'period' => 'May - August'
			]);
	}

}