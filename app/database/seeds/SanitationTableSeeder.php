<?php

class SanitationTableSeeder extends Seeder {

	public function run()
	{
		DB::table('sanitation')->insert(array(
				array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'toilet'  => 'available',
				'urinal'  => 'not_available',
				'water'  => 'available',
				'dust_bins' => 'available',
				'veronica_buckets' => 'available',
				'comment' => 'Just a comment on all the data entered',
				'term' => 'first_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'toilet'  => 'available',
				'urinal'  => 'not_available',
				'water'  => 'available',
				'dust_bins' => 'not_available',
				'veronica_buckets' => 'not_available',
				'comment' => 'Just a comment on all the data entered',
				'term' => 'second_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'toilet'  => 'available',
				'urinal'  => 'not_available',
				'water'  => 'available',
				'dust_bins' => 'not_available',
				'veronica_buckets' => 'not_available',
				'comment' => 'Just a comment on all the data entered',
				'term' => 'third_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
