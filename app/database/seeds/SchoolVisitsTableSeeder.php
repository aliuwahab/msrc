<?php

class SchoolVisitsTableSeeder extends Seeder {

	public function run()
	{
		
		DB::table("school_visits_questions")->insert(array(
					array(
						'question_code' => 'TAQ1',
						'question_category_reference' => 'TA',
						'question' => 'How many teachers are absent?',
						'question_type' => 'open_ended',
						),
					array(
						'question_code' => 'TAQ2',
						'question_category_reference' => 'TA',
						'question' => 'What are the names of the teachers who are absent?',
						'question_type' => 'open_ended',
						),
					array(
						'question_code' => 'TAQ3',
						'question_category_reference' => 'TA',
						'question' => 'Is or are their absence approved?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'TAQ4',
						'question_category_reference' => 'TA',
						'question' => 'If any teacher(s) were absent without approval, name them.',
						'question_type' => 'open_ended',
						),
					array(
						'question_code' => 'TAQ5',
						'question_category_reference' => 'TA',
						'question' => 'Have any teachers been absent without notice for longer than 10 days?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'TAQ6',
						'question_category_reference' => 'TA',
						'question' => 'Any teachers with ongoing punctuality problem?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'TAQ7',
						'question_category_reference' => 'TA',
						'question' => '(If teachers are having punctuality problem),  how many are they? ',
						'question_type' => 'open_ended',
						),
					array(
						'question_code' => 'SAQ1',
						'question_category_reference' => 'SA',
						'question' => 'Did any new students enrol?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SAQ2',
						'question_category_reference' => 'SA',
						'question' => '(If yes) How many new students?',
						'question_type' => 'open_ended',
						),
					array(
						'question_code' => 'SAQ3',
						'question_category_reference' => 'SA',
						'question' => 'KG 1-P6; Number of boys present',
						'question_type' => 'open_ended',
						),
					array(
						'question_code' => 'SAQ4',
						'question_category_reference' => 'SA',
						'question' => 'KG 1-P6; Number of girls present',
						'question_type' => 'open_ended',
						),
					array(
						'question_code' => 'FSSQ1',
						'question_category_reference' => 'FSS',
						'question' => 'Problems with the school grounds',
						'question_type' => 'multiple_choice',
						),
					array(
						'question_code' => 'FSSQ2',
						'question_category_reference' => 'FSS',
						'question' => 'Is there a fully stocked first aid box?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'FSSQ3',
						'question_category_reference' => 'FSS',
						'question' => 'Are dust bins in use?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'FSSQ4',
						'question_category_reference' => 'FSS',
						'question' => 'Do students wash their hands?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'LPQ1',
						'question_category_reference' => 'LP',
						'question' => 'Is the scheme of work available and up to date?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'LPQ2',
						'question_category_reference' => 'LP',
						'question' => 'Discussed lesson plan creation?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'LPQ3',
						'question_category_reference' => 'LP',
						'question' => 'Any issues with lesson plan adherence?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'LPQ4',
						'question_category_reference' => 'LP',
						'question' => 'Follow up needed with lesson plans?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'TLMQ1',
						'question_category_reference' => 'TLM',
						'question' => 'Any issues with teaching methods?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'TLMQ2',
						'question_category_reference' => 'TLM',
						'question' => 'Issues with classroom management?',
						'question_type' => 'multiple_choice',
						),
					array(
						'question_code' => 'TLMQ3',
						'question_category_reference' => 'TLM',
						'question' => 'Problems with in-class materials?',
						'question_type' => 'multiple_choice',
						),
					array(
						'question_code' => 'TLMQ4',
						'question_category_reference' => 'TLM',
						'question' => 'Follow-up  with GES on any of the following?',
						'question_type' => 'multiple_choice',
						),
					array(
						'question_code' => 'CIQ1',
						'question_category_reference' => 'CI',
						'question' => 'Discussed issues with any of the following?',
						'question_type' => 'multiple_choice',
						),
					array(
						'question_code' => 'SRPQ1',
						'question_category_reference' => 'SRP',
						'question' => 'Any enrolment records available?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SRPQ2',
						'question_category_reference' => 'SRP',
						'question' => 'Any store records available?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SRPQ3',
						'question_category_reference' => 'SRP',
						'question' => 'Are the staff meetings held?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'SRPQ4',
						'question_category_reference' => 'SRP',
						'question' => 'SPID prepared?',
						'question_type' => 'single_choice',
						)
			 	));
	}

}
