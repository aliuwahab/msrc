<?php

class DataCollectorsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('data_collectors')->truncate();

		DB::table("data_collectors")->insert(array(
					array(
						'first_name' => 'Gbeila Aliu',
						'last_name' => 'Wahab',
						'gender' => 'Male',
						'identity_code' => 'GH-UWR-SW-CS01',
						'universal_code' => 'QG2354GHEQ45',
						'phone_number' => '+233207361609',
						'type'  => 'circuit_supervisor',
						'country_id'  => '1',
						'region_id'  => '1',
						'district_id' => '1',
						'circuit_id' => '1'
						),
					array(
						'first_name' => 'Cisse',
						'last_name' => 'Munaya',
						'gender' => 'Female',
						'identity_code' => 'GH-UWR-SW-HT01',
						'universal_code' => 'QG2354GHEQ45',
						'phone_number' => '+2332435464643',
						'type'  => 'head_teacher',
						'country_id'  => '1',
						'region_id'  => '1',
						'district_id' => '1',
						'circuit_id' => '1'
						),
						array(
						'first_name' => 'Kanton',
						'last_name' => 'Latifa',
						'gender' => 'Female',
						'identity_code' => 'GH-UWR-SW-HT02',
						'universal_code' => 'QG2354GHEQ45',
						'phone_number' => '+2332435464644',
						'type'  => 'head_teacher',
						'country_id'  => '1',
						'region_id'  => '1',
						'district_id' => '1',
						'circuit_id' => '1'
						),
							array(
						'first_name' => 'Salia',
						'last_name' => 'Adams',
						'gender' => 'Male',
						'identity_code' => 'GH-UWR-SW-HT03',
						'universal_code' => 'QG2354GHEQ45',
						'phone_number' => '+2332435464645',
						'type'  => 'circuit_supervisor',
						'country_id'  => '1',
						'region_id'  => '2',
						'district_id' => '2',
						'circuit_id' => '2'
						),
						array(
						'first_name' => 'Gbanha Bawa',
						'last_name' => 'Samed',
						'gender' => 'Male',
						'identity_code' => 'GH-UWR-SW-HT04',
						'universal_code' => 'QG2354GHEQ45',
						'phone_number' => '+2332435464646',
						'type'  => 'circuit_supervisor',
						'country_id'  => '1',
						'region_id'  => '1',
						'district_id' => '1',
						'circuit_id' => '1'
						)
	));
}
}
