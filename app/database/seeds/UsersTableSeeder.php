<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run() {

		 		DB::table('users')->insert(array(
		 			array(
					'username' => 'tm_nat_admin',
					'firstname' => 'Francis',
					'lastname' => 'Kofigah',
					'gender' => 'male',
					'phone_number' => '0207361609',
					'email' => 'kaygee@yahoo.com',
					'password' => Hash::make('techmerge5102'),
					'level' => 'national',
					'role'  => 'super',
					'level_id'  => '1'
					),
					array(
					'username' => 'reg_nat_admin',
					'firstname' => 'Kofigah',
					'lastname' => 'Francis',
					'gender' => 'male',
					'phone_number' => '0207361609',
					'email' => 'kaygee1@yahoo.com',
					'password' => Hash::make('techmerge5102'),
					'level' => 'regional',
					'role'  => 'super',
					'level_id'  => '1'
					),
					array(
					'username' => 'dis_nat_admin',
					'firstname' => 'Kofi',
					'lastname' => 'Randy',
					'gender' => 'male',
					'phone_number' => '0207361609',
					'email' => 'kaygee2@yahoo.com',
					'password' => Hash::make('techmerge5102'),
					'level' => 'district',
					'role'  => 'super',
					'level_id'  => '1'
					),
					array(
					'username' => 'dis_nat_admin',
					'firstname' => 'Frank',
					'lastname' => 'Kofigah',
					'gender' => 'male',
					'phone_number' => '0207361609',
					'email' => 'kaygee3@yahoo.com',
					'password' => Hash::make('techmerge5102'),
					'level' => 'circuit',
					'role'  => 'super',
					'level_id'  => '1'
					),
					array(
					'username' => 'dis_nat_admin',
					'firstname' => 'Kaygee',
					'lastname' => 'Kofigah',
					'gender' => 'male',
					'phone_number' => '0207361609',
					'email' => 'kaygee4@yahoo.com',
					'password' => Hash::make('techmerge5102'),
					'level' => 'school',
					'role'  => 'super',
					'level_id'  => '1'
					),
                    array(
                        'username' => 'mantey',
                        'firstname' => 'Daniel',
                        'lastname' => 'Mantey',
                        'gender' => 'male',
                        'phone_number' => '0501377866',
                        'email' => 'contactmantey@gmail.com',
                        'password' => Hash::make('mantey'),
                        'level' => 'school',
                        'role'  => 'super',
                        'level_id'  => '1'
                    )
			));
	}

}