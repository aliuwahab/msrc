<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class CircuitsTableSeeder extends Seeder {

	public function run()
	{
		
			DB::table('circuits')->insert(array(array(
				'code' => 'GH-R1-D1-CC1',
				'region_id' => '1',
				'district_id' => '1',
				'country_id' => '1',
				'name' => 'Pulima Circuit',
				'email' => 'pulima@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UWR-SW-PC',
				'description'  => 'A Circuit in the Sissala West District of Upper West Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R1-D1-CC2',
				'region_id' => '1',
				'district_id' => '1',
				'country_id' => '1',
				'name' => 'Tumu Circuit',
				'email' => 'tumu@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UWR-SW-TC',
				'description'  => 'A Circuit in the Sissala East District of Upper West Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R10-D10-CC2005',
				'region_id' => '5',
				'district_id' => '1',
				'country_id' => '1',
				'name' => 'Accra Circuit',
				'email' => 'accra@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UWR-SW-PS',
				'description'  => 'A Circuit in the Sissala West District of Upper West Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R10-D10-CC2005',
				'region_id' => '2',
				'district_id' => '1',
				'country_id' => '1',
				'name' => 'Accra Circuit',
				'email' => 'legon@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UWR-SW-PS',
				'description'  => 'A Circuit in the Sissala West District of Upper West Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
                'code' => 'GH-R10-D10-CC2023',
            	'region_id' => '1',
            	'district_id' => '2',
            	'country_id' => '1',
            	'name' => 'Tema North Circuit',
            	'email' => 'tnc@yahoo.com',
            	'phone_number' => '0233333609',
            	'slug' => 'GAR-TN-PS',
            	'description'  => 'A Circuit in the Tema East District of Ghana',
            	'lat_long'  => '9.066666700000000000,-1.600000000000000000',
            	'status'  => '1'
            ),
            array(
                'code' => 'GH-R10-D10-CC2305',
            	'region_id' => '2',
            	'district_id' => '3',
            	'country_id' => '1',
            	'name' => 'Tema Gotoh Circuit',
            	'email' => 'tgc@yahoo.com',
            	'phone_number' => '0267641609',
            	'slug' => 'GAR-TG-PS',
            	'description'  => 'A Circuit in the Tema West District of Ghana',
            	'lat_long'  => '8.066666700000000000,-3.500000000000000000',
            	'status'  => '1'
            	)
			));
		
	}

}