<?php

class CommunitySmsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('community_sms')->insert(array(
				array(
				'sender' => '+233207361609',
				'message' => 'Pulima JHS School has a clean environment',
				'school_sms_code' => 'SMSCODE1',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'sender' => '+2334546484848',
				'message' => 'Egala JHS School environment can be improve',
				'school_sms_code' => 'SMSCODE2',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
				array(
				'sender' => '+233207361345',
				'message' => 'Pulima JHS School has a nice pitch',
				'school_sms_code' => 'SMSCODE2',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'sender' => '+233207361456',
				'message' => 'Presec JHS School has a clean environment',
				'school_sms_code' => 'SMSCODE2',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
				array(
				'sender' => '+233207361609',
				'message' => 'Achimota JHS School has a clean environment',
				'school_sms_code' => 'SMSCODE2',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
