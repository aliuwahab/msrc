<?php

class QuestionsCategoryTableSeeder extends Seeder {

	public function run()
	{
		DB::table('questions_categories')->insert(array(
				array(
				'name' => 'Teacher Attendance',
				'question_category_code' => 'TA',
				'description'  => 'This measures the adherence of teachers to their duty',
				'total_measure'  => 25,
				'data_category_code_reference' => 'schoolvisits',
				'country_id' => 1
			),
			array(
				'name' => 'Student Attendance',
				'question_category_code' => 'SA',
				'description'  => 'This measures the punctuality of students, and whether students stay in school',
				'total_measure'  => 300,
				'data_category_code_reference' => 'schoolvisits',
				'country_id' => 1
			),
			array(
				'name' => 'Facilities, Safety and Sanitation',
				'question_category_code' => 'FSS',
				'description'  => 'This measures the safety nature of the school and the sanitation situation',
				'total_measure'  => 400,
				'data_category_code_reference' => 'schoolvisits',
				'country_id' => 1
			),
			array(
				'name' => 'Lesson Plans',
				'question_category_code' => 'LP',
				'description'  => 'This measures how effective lessons are carried out in schools',
				'total_measure'  => 4,
				'data_category_code_reference' => 'schoolvisits',
				'country_id' => 1
			),
			array(
				'name' => 'Teaching and Learning Materials',
				'question_category_code' => 'TLM',
				'description'  => 'This measures the availability of learning materials',
				'total_measure'  => 4,
				'data_category_code_reference' => 'schoolvisits',
				'country_id' => 1
			),
			array(
				'name' => 'Community Involvement',
				'question_category_code' => 'CI',
				'description'  => 'This measures the participation level of Community in decision making concerning the school',
				'total_measure'  => 5,
				'data_category_code_reference' => 'schoolvisits',
				'country_id' => 1
			),
			array(
				'name' => 'School Records and performance',
				'question_category_code' => 'SRP',
				'description'  => 'Are the following record books available?',
				'total_measure'  => 5,
				'data_category_code_reference' => 'schoolvisits',
				'country_id' => 1
			),
			array(
				'name' => 'School Information',
				'question_category_code' => 'SI',
				'description'  => 'This contains the information of the school',
				'total_measure'  => 5,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'Head Teacher Details',
				'question_category_code' => 'HTD',
				'description'  => 'This contains information about the head teacher of the school',
				'total_measure'  => 5,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'School Class Enrolment',
				'question_category_code' => 'SCE',
				'description'  => 'This measures the number of pupils in each class',
				'total_measure'  => 100,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'School Special Students Enrolment',
				'question_category_code' => 'SSSE',
				'description'  => 'This measures the number of special students enrolled',
				'total_measure'  => 100,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'Student Class Attendance',
				'question_category_code' => 'SCA',
				'description'  => 'This the presence of pupils in class',
				'total_measure'  => 100,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'Special Student Class Attendance',
				'question_category_code' => 'SSCA',
				'description'  => 'This the presence of pupils with special problems',
				'total_measure'  => 100,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),

			array(
				'name' => 'Class Text Books',
				'question_category_code' => 'CTB',
				'description'  => 'This measures the availability of text books for each subject',
				'total_measure'  => 50,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			// array(
			// 	'name' => 'Student Class Performance',
			// 	'question_category_code' => 'SCP',
			// 	'description'  => 'This measures the performance of student in exams',
			// 	'total_measure'  => 100,
			// 	'data_category_code_reference' => 'schoolreportcard',
			// 	'country_id' => 1
			// ),
			array(
				'name' => 'School Management',
				'question_category_code' => 'SM',
				'description'  => 'This measures the efficiency of the school management team by meetings',
				'total_measure'  => 100,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'Support Type',
				'question_category_code' => 'ST',
				'description'  => 'This measures the sources of funding for each school',
				'total_measure'  => 100,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'School Grant',
				'question_category_code' => 'SG',
				'description'  => 'This measures the grant amounts a school receives',
				'total_measure'  => 5,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'School Teachers Information',
				'question_category_code' => 'STI',
				'description'  => 'This allows for the recording of details of teachers each school',
				'total_measure'  => 100,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'An Inclusive School',
				'question_category_code' => 'AIS',
				'description'  => 'A school open to every child',
				'total_measure'  => 10,
				'data_category_code_reference' => 'schoolreview',
				'country_id' => 1
			),
			array(
				'name' => 'Effective Teaching and Learning',
				'question_category_code' => 'ETL',
				'description'  => 'This allows for the recording of details of teachers each school',
				'total_measure'  => 10,
				'data_category_code_reference' => 'schoolreview',
				'country_id' => 1
			),
			array(
				'name' => 'Healthy Level of School',
				'question_category_code' => 'HLS',
				'description'  => 'This allows for the recording of details of teachers each school',
				'total_measure'  => 10,
				'data_category_code_reference' => 'schoolreview',
				'country_id' => 1
			),
			array(
				'name' => 'Safe and Protective Schools',
				'question_category_code' => 'SPS',
				'description'  => 'This allows for the recording of details of teachers each school',
				'total_measure'  => 10,
				'data_category_code_reference' => 'schoolreview',
				'country_id' => 1
			),
				array(
				'name' => 'Friendly School for Both Girls and Boys',
				'question_category_code' => 'FSBGB',
				'description'  => 'This allows for the recording of details of teachers each school',
				'total_measure'  => 10,
				'data_category_code_reference' => 'schoolreview',
				'country_id' => 1
			),
			array(
				'name' => 'Community Involvement in the School',
				'question_category_code' => 'CIS',
				'description'  => 'This allows for the recording of details of teachers each school',
				'total_measure'  => 10,
				'data_category_code_reference' => 'schoolreview',
				'country_id' => 1
			),
			array(
				'name' => 'School Problems',
				'question_category_code' => 'SP',
				'description'  => 'Were there any problems with the school grounds?',
				'total_measure'  => 10,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			// array(
			// 	'name' => 'School Records and performance',
			// 	'question_category_code' => 'SRP',
			// 	'description'  => 'Are the following record books available?',
			// 	'total_measure'  => 10,
			// 	'data_category_code_reference' => 'schoolvisits',
			// 	'country_id' => 1
			// ),
			array(
				'name' => 'Level of community/school relationship',
				'question_category_code' => 'LCSR',
				'description'  => 'This measures the level of community and school leadership relationship',
				'total_measure'  => 0,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'Punctuality And Lessons Plans',
				'question_category_code' => 'PLP',
				'description'  => 'This measures how teachers prepare their lessons plans',
				'total_measure'  => 0,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'Number Of Units Covered By Teacher',
				'question_category_code' => 'NUCP',
				'description'  => 'This measures the number of units covered by a teacher in a term',
				'total_measure'  => 0,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
			array(
				'name' => 'Teacher Classroom Management',
				'question_category_code' => 'TCM',
				'description'  => 'This measures teachers ability to control and manage students',
				'total_measure'  => 0,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
				array(
				'name' => 'School General Situation',
				'question_category_code' => 'SGS',
				'description'  => 'This are questions for knowing the general situation of the school',
				'total_measure'  => 0,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			),
				array(
				'name' => 'Pupils Exams Perormance Average',
				'question_category_code' => 'PEPA',
				'description'  => 'This to establish the performance level of pupils',
				'total_measure'  => 0,
				'data_category_code_reference' => 'schoolreportcard',
				'country_id' => 1
			)
			));

		// Uncomment the below to run the seeder
		// DB::table('questionscategory')->insert($questionscategory);
	}

}
