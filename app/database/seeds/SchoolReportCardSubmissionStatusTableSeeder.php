<?php

class SchoolReportCardSubmissionStatusTableSeeder extends Seeder {

	public function run()
	{
			DB::table('school_reportcard_submission_status')->insert(array(
				array(
						'question_code' => 'AISQ1',
						'question_category_reference' => 'AIS',
//						'question' => 'Do all children including those with special needs (children with disabilities, orphans , or from poor families) attend school?',
						'question_type' => 'single_choice',
						),
					array(
						'question_code' => 'CISQ2',
						'question_category_reference' => 'CIS',
//						'question' => 'Do most parents attend PTA meetings?',
						'question_type' => 'single_choice',
						)
			));
	}

}
