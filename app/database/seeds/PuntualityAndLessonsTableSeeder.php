<?php

class PuntualityAndLessonsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('punctuality_and_lessons')->insert(array(
				array(
				'teacher_id' => 1,
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'PLP',
				'days_punctual'  => 2,
				'total_number_of_days_absent'  => 1,
				'days_absent_without_permission'  => 0,
				'lesson_plans' => 'good',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'week_number' => 1,
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'teacher_id' => 2,
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'PLP',
				'days_punctual'  => 1,
				'total_number_of_days_absent'  => 1,
				'days_absent_without_permission'  => 0,
				'lesson_plans' => 'good',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'week_number' => 1,
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'teacher_id' => 3,
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'PLP',
				'days_punctual'  => 0,
				'total_number_of_days_absent'  => 1,
				'days_absent_without_permission'  => 0,
				'lesson_plans' => 'good',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'week_number' => 1,
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
