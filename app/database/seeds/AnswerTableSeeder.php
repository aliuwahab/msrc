<?php

class AnswerTableSeeder extends Seeder {

	public function run()
	{
		DB::table('answers')->insert(array(
				array(
				'itenary_id' => 1,
				'school_code_reference' => 'GH-R1-D1-CC2-SC1',
				'question_code_reference' => 'TAQ1',
				'answer' => '3',
				'comment' => 'no comment',
				'question_type'  => 'open_ended',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'itenary_id' => 1,
				'school_code_reference' => 'GH-R1-D1-CC2-SC1',
				'question_code_reference' => 'TAQ2',
				'answer' => 'Hadi, Kaygee',
				'comment' => 'no comment',
				'question_type'  => 'open_ended',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'itenary_id' => 1,
				'school_code_reference' => 'GH-R1-D1-CC2-SC1',
				'question_code_reference' => 'SAQ1',
				'answer' => 'Yes',
				'comment' => 'no comment',
				'question_type'  => 'single_choice',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'itenary_id' => 1,
				'school_code_reference' => 'GH-R1-D1-CC2-SC1',
				'question_code_reference' => 'FSSQ1',
				'answer' => 'Bathrooms, compound',
				'comment' => 'no comment',
				'question_type'  => 'open_ended',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'itenary_id' => 1,
				'school_code_reference' => 'GH-R1-D1-CC2-SC1',
				'question_code_reference' => 'FSSQ2',
				'answer' => 'No',
				'comment' => 'no comment',
				'question_type'  => 'open_ended',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'itenary_id' => 1,
				'school_code_reference' => 'GH-R1-D1-CC2-SC1',
				'question_code_reference' => 'LPQ1',
				'answer' => 'No',
				'comment' => 'no comment',
				'question_type'  => 'single_choice',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
