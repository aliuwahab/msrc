<?php

class EquipmentRecreationalTableSeeder extends Seeder {

	public function run()
	{
		DB::table('equipment_recreational')->insert(array(
				array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'football'  => 'available',
				'volleyball'  => 'available',
				'netball'  => 'available',
				'playing_field' => 'available',
				'sports_wear' => 'not_available',
				'seesaw' => 'not_available',
				'merry_go_round' => 'not_available',
				'first_aid_box' => 'available',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'football'  => 'available',
				'volleyball'  => 'not_available',
				'netball'  => 'available',
				'playing_field' => 'available',
				'sports_wear' => 'not_available',
				'seesaw' => 'not_available',
				'merry_go_round' => 'not_available',
				'first_aid_box' => 'available',
				'comment' => 'Just a comment on the data entered',
				'term' => 'second_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'football'  => 'available',
				'volleyball'  => 'not_available',
				'netball'  => 'available',
				'playing_field' => 'available',
				'sports_wear' => 'available',
				'seesaw' => 'not_available',
				'merry_go_round' => 'not_available',
				'first_aid_box' => 'available',
				'comment' => 'Just a comment on the data entered',
				'term' => 'third_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
