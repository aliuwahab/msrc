<?php

class TeachersClassManagementTableSeeder extends Seeder {

	public function run()
	{
		DB::table('teachers_class_management')->insert(array(
				array(
				'teacher_id' => 1,
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'TCM',
				'class_control'  => 'good',
				'children_behaviour'  => 'good',
				'children_participation'  => 'fair',
				'teacher_discipline' => 'poor',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'week_number' => 1,
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'teacher_id' => 2,
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'TCM',
				'class_control'  => 'good',
				'children_behaviour'  => 'good',
				'children_participation'  => 'fair',
				'teacher_discipline' => 'fair',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'week_number' => 1,
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'teacher_id' => 3,
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'TCM',
				'class_control'  => 'good',
				'children_behaviour'  => 'fair',
				'children_participation'  => 'fair',
				'teacher_discipline' => 'fair',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'week_number' => 1,
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
