<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class RegionsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('regions')->insert(array(
				array(
				'code' => 'GH-R1',
				'country_id' => '1',
				'name' => 'Upper West',
				'email' => 'upperwest@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UWR',
				'description'  => 'Upper West Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R2',
				'country_id' => '1',
				'name' => 'Upper East',
				'email' => 'uppereast@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UER',
				'description'  => 'Upper East Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R3',
				'country_id' => '1',
				'name' => 'Northern',
				'email' => 'northern@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'NR',
				'description'  => 'Northern Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R4',
				'country_id' => '1',
				'name' => 'Brong Ahafo',
				'email' => 'brongahafo@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'BAR',
				'description'  => 'Brong Ahafo Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R5',
				'country_id' => '1',
				'name' => 'Ashanti',
				'email' => 'ashanti@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'AR',
				'description'  => 'Ashanti Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R6',
				'country_id' => '1',
				'name' => 'Central',
				'email' => 'central@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'CR',
				'description'  => 'Central Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R7',
				'country_id' => '1',
				'name' => 'Western',
				'email' => 'western@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'CR',
				'description'  => 'Western Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R8',
				'country_id' => '1',
				'name' => 'Eastern',
				'email' => 'eastern@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'ER',
				'description'  => 'Eastern Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R9',
				'country_id' => '1',
				'name' => 'Volta',
				'email' => 'volta@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'VR',
				'description'  => 'Volta Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				),
				array(
				'code' => 'GH-R10',
				'country_id' => '1',
				'name' => 'Greater Accra',
				'email' => 'accra@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'GRA',
				'description'  => 'Greater Accra Region is located the northern part of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
				)
				));
		
	}

}