<?php

class ItineraryTableSeeder extends Seeder {

	public function run()
	{
		DB::table('itenary')->insert(array(
				array(
				'name' => 'First Itinerary',
				'description' => 'Just an Intinerary',
				'slug' => 'FI',
				'start_date' => '14-07-14',
				'end_date' => '14-18-14',
				'country_id' => 1,
				'region_id'  => 1,
				'district_id'  => 1
				),
				array(
				'name' => 'Second Itinerary',
				'description' => 'Just an Intinerary',
				'slug' => 'FI',
				'start_date' => '14-07-14',
				'end_date' => '14-18-14',
				'country_id' => 1,
				'region_id'  => 1,
				'district_id'  => 1
				),
				array(
				'name' => 'Third Itinerary',
				'description' => 'Just an Intinerary',
				'slug' => 'FI',
				'start_date' => '14-07-14',
				'end_date' => '14-18-14',
				'country_id' => 1,
				'region_id'  => 1,
				'district_id'  => 1
				),
				array(
				'name' => 'Fourth Itinerary',
				'description' => 'Just an Intinerary',
				'slug' => 'FI',
				'start_date' => '14-07-14',
				'end_date' => '14-18-14',
				'country_id' => 1,
				'region_id'  => 1,
				'district_id'  => 1
				),
				array(
				'name' => 'Fifth Itinerary',
				'description' => 'Just an Intinerary',
				'slug' => 'FI',
				'start_date' => '14-07-14',
				'end_date' => '14-18-14',
				'country_id' => 1,
				'region_id'  => 1,
				'district_id'  => 1
				)
				));
	}

}
