<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class DistrictsTableSeeder extends Seeder {

	public function run()
	{

			DB::table('districts')->insert(array(
				array(
				'code' => 'GH-R1-D1',
				'region_id' => '1',
				'country_id' => '1',
				'name' => 'Sissla West District',
				'email' => 'sissalawest@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UWR-SW',
				'description'  => 'A District in Upper West Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R1-D2',
				'region_id' => '1',
				'country_id' => '1',
				'name' => 'Sissla East District',
				'email' => 'sissalaeast@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UWR-SE',
				'description'  => 'A District in Upper West Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R2-D56',
				'region_id' => '5',
				'country_id' => '1',
				'name' => 'Bolgatanga Municipal',
				'email' => 'bolgatanat@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UE-BM',
				'description'  => 'A District in Upper East Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R2-D57',
				'region_id' => '5',
				'country_id' => '1',
				'name' => 'Garru District',
				'email' => 'garru@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'UE-GD',
				'description'  => 'A District in Upper East Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R10-D200',
				'region_id' => '2',
				'country_id' => '1',
				'name' => 'Accra Metropolis',
				'email' => 'metro@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'GR-AMA',
				'description'  => 'A Metropolis in Greater Accra Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'code' => 'GH-R10-D201',
				'region_id' => '1',
				'country_id' => '1',
				'name' => 'East Legon Municipality',
				'email' => 'legon@yahoo.com',
				'phone_number' => '0207361609',
				'slug' => 'GR-ELA',
				'description'  => 'A municipal in Greater Accra Region of Ghana',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
            				'code' => 'GH-R10-D291',
            				'region_id' => '1',
            				'country_id' => '1',
            				'name' => 'Tema East Municipality',
            				'email' => 'temaeast@gmail.com',
            				'phone_number' => '0247063602',
            				'slug' => 'GR-TEA',
            				'description'  => 'A municipal in Tema',
            				'lat_long'  => '9.066666700000000000,1.500000000000000000',
            				'status'  => '1'
            			),
            	array(
                    'code' => 'GH-R10-D201',
                    'region_id' => '1',
                    'country_id' => '1',
                    'name' => 'Tema West Municipality',
                    'email' => 'temawest@yahoo.com',
                    'phone_number' => '0245211239',
                    'slug' => 'GR-TWA',
                    'description'  => 'A municipal in Tema',
                    'lat_long'  => '8.066666700000000000,2.500000000000000000',
                    'status'  => '1'
                )
			));
	}

}