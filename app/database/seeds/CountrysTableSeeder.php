<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CountrysTableSeeder extends Seeder {

	public function run()
	{		
			DB::table("countries")->insert(array(
				array('code' => 'GH-WA-1',
						'name' => 'Ghana',
						'slug' => 'GH',
						'description'  => 'Ghana is a country which is the default one for this application',
						'lat_long'  => '10.066666700000000000,-2.500000000000000000',
						'status'  => '1'),
				array('code' => 'NG-WA-2',
						'name' => 'Nigeria',
						'slug' => 'NG',
						'description'  => 'Nigeria is located in West Africa',
						'lat_long'  => '9.081999000000000000,8.675277000000051000',
						'status'  => '1')
			));
	}

}