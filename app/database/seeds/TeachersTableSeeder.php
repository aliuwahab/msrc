<?php

class TeachersTableSeeder extends Seeder {

	public function run()
	{
		DB::table("teachers")->insert(array(
					array(
						'school_reference_code' => 'GH-R1-D1-CC2-SC1',
						'questions_category_reference_code' => 'STI',
						'name' => 'Hadi Mukaila',
						'gender' => 'male',
						'class_subject_taught' => 'Mathematics',
						'category' => 'trained',
						'term' => 'first_term',
						'week_number' => 1,
						'year' => '2014',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						),
					array(
						'school_reference_code' => 'GH-R1-D1-CC2-SC1',
						'questions_category_reference_code' => 'STI',
						'name' => 'Hadi Mukaila',
						'gender' => 'male',
						'class_subject_taught' => 'Mathematics',
						'category' => 'trained',
						'term' => 'first_term',
						'week_number' => 2,
						'year' => '2014',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						),
					array(
						'school_reference_code' => 'GH-R1-D1-CC2-SC1',
						'questions_category_reference_code' => 'STI',
						'name' => 'Francis Kofigah',
						'gender' => 'male',
						'class_subject_taught' => 'Mathematics',
						'category' => 'trained',
						'term' => 'first_term',
						'week_number' => 3,
						'year' => '2014',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						),
					array(
						'school_reference_code' => 'GH-R1-D1-CC2-SC1',
						'questions_category_reference_code' => 'STI',
						'name' => 'Alhassan Rasheeds',
						'gender' => 'female',
						'class_subject_taught' => 'Mathematics',
						'category' => 'trained',
						'term' => 'third_term',
						'week_number' => 2,
						'year' => '2014',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						)
			 	));
	}

}
