<?php

class SchoolCommunityRelationshipTableSeeder extends Seeder {

	public function run()
	{
		DB::table('school_community_relationship')->insert(array(
				array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'LCSR',
				'pta_involvement'  => 'good',
				'parents_notified_of_student_progress'  => 'fair',
				'community_sensitization_for_school_attendance'  => 'poor',
				'school_management_committees' => 'fair',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'LCSR',
				'pta_involvement'  => 'good',
				'parents_notified_of_student_progress'  => 'fair',
				'community_sensitization_for_school_attendance'  => 'poor',
				'school_management_committees' => 'fair',
				'comment' => 'Just a comment on the data entered',
				'term' => 'second_term',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'LCSR',
				'pta_involvement'  => 'good',
				'parents_notified_of_student_progress'  => 'fair',
				'community_sensitization_for_school_attendance'  => 'poor',
				'school_management_committees' => 'fair',
				'comment' => 'Just a comment on the data entered',
				'term' => 'third_term',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}
}
