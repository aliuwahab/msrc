<?php

class DataCategoryTableSeeder extends Seeder {

	public function run()
	{
		DB::table('data_categories')->insert(array(
			array(
				'data_category_name' => 'School Visits',
				'data_category_code' => 'schoolvisits',
				'country_id' => 1,
				'description'  => 'This data is collected by Circuit Supervisors to determine the state of each school'
			),
			array(
				'data_category_name' => 'School Report Card',
				'data_category_code' => 'schoolreportcard',
				'country_id' => 1,
				'description'  => 'This data is collected by head teachers to determine the state of each school'
			),
			array(
				'data_category_name' => 'School Review',
				'data_category_code' => 'schoolreview',
				'country_id' => 1,
				'description'  => 'This does a summary evaluation of schools base on health, teaching sanitaion and all facets that are necessary'
			)
			));
	
	}

}
