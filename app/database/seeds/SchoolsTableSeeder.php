<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class SchoolsTableSeeder extends Seeder {

	public function run()
	{
	
			DB::table('schools')->insert(array(
				array(
				'ges_code' => '001',
				'school_code' => 'GH-R1-D1-CC2-SC1',
				'sms_code' => 'SMSCODE1',
				'region_id' => '1',
				'district_id' => '1',
				'circuit_id' => '1',
				'country_id' => '1',
				'headteacher_id' => '1',
				'supervisor_id' => '1',
				'email' => 'pulima@gmail.com',
				'phone_number' => '+233207361609',
				'name' => 'Pulima School',
				'category' => 'jhs',
				'slug' => 'UWR-SW-TC-PS',
				'description'  => 'A School in pulima',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'ges_code' => '002',
				'school_code' => 'GH-R1-D1-CC2-SC2',
				'sms_code' => 'SMSCODE2',
				'region_id' => '1',
				'district_id' => '1',
				'circuit_id' => '1',
				'country_id' => '1',
				'headteacher_id' => '1',
				'supervisor_id' => '1',
				'email' => 'egala@gmail.com',
				'phone_number' => '+233207968538',
				'name' => 'Egala School',
				'category' => 'jhs',
				'slug' => 'UWR-SW-TC-ES',
				'description'  => 'A School in Tumu',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'ges_code' => '003',
				'school_code' => 'GH-R10-D1-CC2-SC3',
				'sms_code' => 'SMSCODE3',
				'region_id' => '1',
				'district_id' => '2',
				'circuit_id' => '1',
				'country_id' => '1',
				'headteacher_id' => '2',
				'supervisor_id' => '1',
				'email' => 'presec@gmail.com',
				'phone_number' => '+233207361610',
				'name' => 'Presec School',
				'category' => 'shs',
				'slug' => 'UWR-SW-TC-PRS',
				'description'  => 'A School in pulima',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'ges_code' => '004',
				'school_code' => 'GH-R10-D1-CC2-SC4',
				'sms_code' => 'SMSCODE5',
				'region_id' => '6',
				'district_id' => '2',
				'circuit_id' => '1',
				'country_id' => '1',
				'headteacher_id' => '3',
				'supervisor_id' => '1',
				'email' => 'achimota@gmail.com',
				'phone_number' => '+233207361609',
				'name' => 'Achimota School',
				'category' => 'shs',
				'slug' => 'UWR-SW-TC-PRS',
				'description'  => 'A School in pulima',
				'lat_long'  => '10.066666700000000000,-2.500000000000000000',
				'status'  => '1'
			),
			array(
				'ges_code' => '005',
            	'school_code' => 'GH-R10-D1-CC2-SC5',
            	'sms_code' => 'SMSCODE6',
            	'region_id' => '2',
            	'district_id' => '2',
            	'circuit_id' => '2',
            	'country_id' => '1',
            	'headteacher_id' => '3',
            	'supervisor_id' => '1',
            	'email' => 'achimota@gmail.com',
            	'phone_number' => '+233207361609',
            	'name' => 'Nyankotonkpala School',
            	'category' => 'shs',
            	'slug' => 'UWR-SW-TC-PRS',
            	'description'  => 'A School in Nyankotonkpala',
            	'lat_long'  => '10.066666700000000000,-2.500000000000000000',
            	'status'  => '1'
            ),
            array(
            	'ges_code' => '006',
                'school_code' => 'GH-R10-D1-CC2-TE5',
                'sms_code' => 'SMSCODE7',
                'region_id' => '10',
                'district_id' => '7',
                'circuit_id' => '7',
                'country_id' => '1',
                'headteacher_id' => '3',
                'supervisor_id' => '1',
                'email' => 'twedaase@gmail.com',
                'phone_number' => '+233207361609',
                'name' => 'Twedaase Primary School',
                'category' => 'jhs',
                'slug' => 'UWR-SW-TC-PRS',
                'description'  => 'A School in Tema',
                'lat_long'  => '3.066666700000000000,-2.200000000000000000',
                'status'  => '1'
            ),
            array(
            	'ges_code' => '007',
                'school_code' => 'GH-R10-D1-CC2-TW1',
                'sms_code' => 'SMSCODE8',
                'region_id' => '10',
                'district_id' => '8',
                'circuit_id' => '8',
                'country_id' => '1',
                'headteacher_id' => '3',
                'supervisor_id' => '1',
                'email' => 'twedaase@gmail.com',
                'phone_number' => '+233204671609',
                'name' => 'Sakumono Nr.1 Primary School',
                'category' => 'jhs',
                'slug' => 'GAR-TW-TC-PRS',
                'description'  => 'A School in Tema West',
                'lat_long'  => '3.466666700000000000,-2.500000000000000000',
                'status'  => '1'
                        )
			));
	}

}