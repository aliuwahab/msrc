<?php

class GCMIDSTableSeeder extends Seeder {

	public function run()
	{
		DB::table('gcm_ids')->insert(array(
			array(
				'phone_number' => '+233237361609',
				'gcm_id' => 'ASWFWJWK12KSKSKFFLJEKERLFKDFJ',
				'data_collector_id'  => 1,
				'type_of_data_collector'  => 'head_teacher',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1
			),
			array(
				'phone_number' => '+233237361610',
				'gcm_id' => 'ASWFWJWK12KSKSKFFLJEKERLF3rr454',
				'data_collector_id'  => 2,
				'type_of_data_collector'  => 'head_teacher',
				'country_id' => 1,
				'region_id' => 2,
				'district_id' => 1,
				'circuit_id' => 1
			),
			array(
				'phone_number' => '+23323736111',
				'gcm_id' => 'ASWFWJWK12KSKSKFFLJEKERLFKDrttyyrtr',
				'data_collector_id'  => 1,
				'type_of_data_collector'  => 'circuit_supervisor',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1
			),
			array(
				'phone_number' => '+233237361123 ',
				'gcm_id' => 'ASWFWJWK12KSKSKFFLJEKERLFKDFJ33ereee',
				'data_collector_id'  => 1,
				'type_of_data_collector'  => 'head_teacher',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1
			),
			array(
				'phone_number' => '+233237361224',
				'gcm_id' => 'ASWFWJWK12KSKSKFFLJEKERLFKDFJqweret',
				'data_collector_id'  => 1,
				'type_of_data_collector'  => 'circuit_supervisor',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1
			)
			));
	}

}
