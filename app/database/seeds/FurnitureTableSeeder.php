<?php

class FurnitureTableSeeder extends Seeder {

	public function run()
	{

		DB::table('funiture')->insert(array(
				array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'pupils_furniture'  => 'adequate',
				'teacher_tables'  => 'inadequate',
				'teacher_chairs'  => 'adequate',
				'classrooms_cupboard' => 'inadequate',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'pupils_furniture'  => 'adequate',
				'teacher_tables'  => 'inadequate',
				'teacher_chairs'  => 'adequate',
				'comment' => 'Just a comment on the data entered',
				'classrooms_cupboard' => 'inadequate',
				'term' => 'second_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'pupils_furniture'  => 'adequate',
				'teacher_tables'  => 'inadequate',
				'teacher_chairs'  => 'adequate',
				'classrooms_cupboard' => 'inadequate',
				'comment' => 'Just a comment on the data entered',
				'term' => 'third_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));



	}

}
