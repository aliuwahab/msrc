<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 * test
	 * @return void
	 */
	public function run() {
		Eloquent::unguard();
		$this->call('UsersTableSeeder');
		$this->call('CountrysTableSeeder');
		$this->call('RegionsTableSeeder');
		$this->call('DataCategoryTableSeeder');
		$this->call('QuestionsCategoryTableSeeder');
        $this->call('SchoolVisitsTableSeeder');
        $this->call('SchoolReviewQuestionsTableSeeder');

		// $this->call('DistrictsTableSeeder');
		// $this->call('CircuitsTableSeeder');  
		// $this->call('SchoolsTableSeeder');
		// $this->call('DataCollectorsTableSeeder');
		// $this->call('SchoolEnrolmentTableSeeder');
		// $this->call('StudentAttendanceTableSeeder');
		// //$this->call('TextbooksTableSeeder');
		// //$this->call('PeformanceTableSeeder');
		// $this->call('AnswerTableSeeder');
		// $this->call('ItineraryTableSeeder');
		// //$this->call('GrantTableSeeder');
		// $this->call('SupportTypeTableSeeder');
		// $this->call('TeachersTableSeeder');
		// $this->call('SchoolManagementTableSeeder');
		// $this->call('SchoolInclusiveAnswerTableSeeder');
		// $this->call('EffectiveTeachingLearningTableSeeder');
		// $this->call('HealthyLevelTableSeeder');
		// $this->call('SafeProtectiveTableSeeder');
		// $this->call('FriendlySchoolTableSeeder');
		// $this->call('CommunityInvolvementTableSeeder');
		// $this->call('CommunitySmsTableSeeder');
		// $this->call('SanitationTableSeeder');
		// $this->call('SecurityTableSeeder');
		// $this->call('EquipmentRecreationalTableSeeder');
		// $this->call('SchoolStructureTableSeeder');
		// $this->call('FurnitureTableSeeder');
		// $this->call('SchoolRecordsAndPerformanceTableSeeder');
		// $this->call('SchoolCommunityRelationshipTableSeeder');
		// $this->call('PuntualityAndLessonsTableSeeder');
		// $this->call('TeachersClassManagementTableSeeder');
		// $this->call('RecentDataSubmittedTableSeeder');
		// $this->call('SchoolReviewSubmissionStatusTableSeeder');
		// $this->call('SchoolReportCardSubmissionStatusTableSeeder');
		// $this->call('SchoolVisitSubmissionStatusTableSeeder');
		// $this->call('SchoolReportCardWeeklySubmissionStatusTableSeeder');
		// $this->call('GeneralSchoolSituationTableSeeder');
		// $this->call('SchoolsMeetingTableSeeder');
		// $this->call('SpecialStudentsEnrolmentTableSeeder');
		// $this->call('GCMIDSTableSeeder');
		
	}

}
