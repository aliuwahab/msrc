<?php

class HealthyLevelTableSeeder extends Seeder {

	public function run()
	{
		DB::table("healthy_level")->insert(array(
					array(
						'itenary_id' => 1,
						'school_code_reference' => 'GH-R1-D1-CC2-SC1',
						'question_code_reference' => 'HLSQ1',
						'submitter' => 'circuit_supervisor',
						'answer' => 1,
						'comment' => 'no comment',
						'year' => '2014',
						'date' => '12-12-14',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						),
					array(
						'itenary_id' => 1,
						'school_code_reference' => 'GH-R1-D1-CC2-SC1',
						'question_code_reference' => 'HLSQ2',
						'submitter' => 'circuit_supervisor',
						'answer' => 1,
						'comment' => 'ther are some things',
						'year' => '2014',
						'date' => '12-12-15',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						),
					array(
						'itenary_id' => 1,
						'school_code_reference' => 'GH-R1-D1-CC2-SC1',
						'question_code_reference' => 'HLSQ3',
						'submitter' => 'circuit_supervisor',
						'answer' => 2,
						'comment' => 20,
						'year' => '2014',
						'date' => '12-12-14',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						),
					array(
						'itenary_id' => 1,
						'school_code_reference' => 'GH-R1-D1-CC2-SC1',
						'question_code_reference' => 'HLSQ4',
						'submitter' => 'circuit_supervisor',
						'answer' => 0,
						'comment' => 'just a funny comment',
						'year' => '2014',
						'date' => '12-12-14',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						),
						array(
						'itenary_id' => 1,
						'school_code_reference' => 'GH-R1-D1-CC2-SC1',
						'question_code_reference' => 'HLSQ5',
						'submitter' => 'circuit_supervisor',
						'answer' => 2,
						'comment' => 'just a funny comment',
						'year' => '2014',
						'date' => '12-12-14',
						'country_id' => 1,
						'region_id' => 1,
						'district_id' => 1,
						'circuit_id' => 1,
						'school_id' => 1,
						)
			 	));
	}

}
