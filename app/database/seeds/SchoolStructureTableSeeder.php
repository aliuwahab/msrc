<?php

class SchoolStructureTableSeeder extends Seeder {

	public function run()
	{
		DB::table('school_structure')->insert(array(
				array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'walls'  => 'good',
				'doors'  => 'poor',
				'windows'  => 'good',
				'floors' => 'good',
				'blackboard' => 'good',
				'comment' => 'Just a comment on the data entered',
				'term' => 'first_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'walls'  => 'good',
				'doors'  => 'poor',
				'windows'  => 'good',
				'floors' => 'good',
				'blackboard' => 'good',
				'comment' => 'Just a comment on the data entered',
				'term' => 'second_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'school_reference_code' => 'GH-R1-D1-CC2-SC1',
				'questions_category_reference_code' => 'SP',
				'walls'  => 'good',
				'doors'  => 'poor',
				'windows'  => 'good',
				'floors' => 'good',
				'blackboard' => 'good',
				'comment' => 'Just a comment on the data entered',
				'term' => 'third_term',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
