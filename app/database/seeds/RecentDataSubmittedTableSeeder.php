<?php

class RecentDataSubmittedTableSeeder extends Seeder {

	public function run()
	{
		DB::table('recent_data_submitted')->insert(array(
				array(
				'data_collector_id' => 1,
				'data_type_submitted' => 'schoolreportcard',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'data_collector_id' => 1,
				'data_type_submitted' => 'schoolvisits',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'data_collector_id' => 1,
				'data_type_submitted' => 'schoolvisits',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'data_collector_id' => 1,
				'data_type_submitted' => 'schoolreportcard',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			),
			array(
				'data_collector_id' => 1,
				'data_type_submitted' => 'schoolreportcard',
				'year' => '2014',
				'country_id' => 1,
				'region_id' => 1,
				'district_id' => 1,
				'circuit_id' => 1,
				'school_id' => 1,
			)
			));
	}

}
