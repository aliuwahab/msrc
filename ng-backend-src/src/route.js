var app =  angular.module('main-App',[]);
app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/targetedinstruction/attendance', {
                templateUrl: 'partials/templates/attendance.html',
                controller: 'MkAttendanceController'
            }).
            when('/targetedinstruction/classobservation', {
                templateUrl: 'partials/templates/classobservation.html',
                controller: 'MkClassObservationController'
            }).
             when('/targetedinstruction/pupilaggregateddata', {
                templateUrl: 'partials/templates/pupilaggregateddata.html',
                controller: 'MkPupilAggregatedController'
            }); 
           

}]);

