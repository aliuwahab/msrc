/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.factory('scCircuitSupervisor', ['$resource' , '$rootScope', '$q', 'CacheFactory','$http','scNotifier',
    'scRegion','scDistrict','scCircuit', 'scSchool','DataCollectorHolder','$timeout',
    function($resource, $rootScope,$q, CacheFactory, $http, scNotifier
        ,scRegion, scDistrict, scCircuit, scSchool, DataCollectorHolder, $timeout){

        var scSupervisor = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('supervisorCache')) {
            var supervisorCache = CacheFactory('supervisorCache', {
                maxAge: 15 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        scSupervisor.supervisors = [];

        var url = '/admin/ghana/data_collectors/circuit_supervisor', loadingSupervisors = false;

        scSupervisor.getAllSupervisors = function(){
            if (loadingSupervisors) {
                return;
            }
            loadingSupervisors = true;
            var deferred = $q.defer();

            var cachedData = supervisorCache.get(url);

            if (cachedData == undefined ) {
                scNotifier.notifyInfo('Info', 'Updating Circuit Supervisors...');

                $http.get(url)
                    .success(function(data){
                        supervisorCache.put(url, data);
                        scSupervisor.supervisors = data;
                        loadingSupervisors = false;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        loadingSupervisors = false;
                        deferred.resolve(false);
                    })
            }else{
                scSupervisor.supervisors = cachedData;
                loadingSupervisors = false;
                deferred.resolve(cachedData);
            }
            return deferred.promise;

        };

        scSupervisor.updateSupervisorArray = function(){
            var defer = $q.defer();
            supervisorCache.remove(url);
            scSupervisor.supervisors = [];
            scSupervisor.getAllSupervisors().then(function () {
                sortSupervisors();

                $timeout(function () {
                    $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                    $rootScope.loading = false;
                    defer.resolve(true);
                }, 100);
            });

            return defer.promise;
        };
        
        $rootScope.reloadCircuitSupervisors = function () {
            $rootScope.loading = true;
            scSupervisor.updateSupervisorArray();
        };

        function sortSupervisors() {
            scSupervisor.supervisors.sort(function (a, b) {
                if (a.first_name > b.first_name) {
                    return 1;
                } else if (a.first_name < b.first_name) {
                    return -1
                } else {
                    return 0
                }
            });
        }

        scSupervisor.init = function(){

            scSupervisor.supervisorLookup = {};

            angular.forEach(this.supervisors, function(supervisor, index){

                //assign the property name to each supervisor
                //This formats the names of the supervisors to their full name
                supervisor.name = supervisor.first_name + ' ' + supervisor.last_name;
                supervisor.fullName = supervisor.first_name + ' ' + supervisor.last_name;

                //Increment Circuit circuit supervisor count
                if (scCircuit.circuitLookup[supervisor.circuit_id] !== undefined) {
                    scCircuit.circuitLookup[supervisor.circuit_id].circuitSupervisor = supervisor;
                }else{
                    console.log('The circuit with id of ' + supervisor.circuit_id + ' does not exist.');
                }

                //Increment District circuit supervisor count and push him into array
                if ( scDistrict.districtLookup[supervisor.district_id] !== undefined) {
                    scDistrict.districtLookup[supervisor.district_id].circuit_supervisors += 1;
                    scDistrict.districtLookup[supervisor.district_id].circuit_supervisors_holder.push(supervisor);
                }else{
                    console.log('The district with id of ' + supervisor.district_id + ' does not exist.');
                }

                //Increment Region circuit supervisor count and push him into array
                if (scRegion.regionLookup[supervisor.region_id] !== undefined) {
                    scRegion.regionLookup[supervisor.region_id].circuit_supervisors += 1;
                    scRegion.regionLookup[supervisor.region_id].circuit_supervisors_holder.push(supervisor);

                }else{
                    console.log('The region with id of ' + supervisor.region_id + ' does not exist.');
                }

                //Assign the data collector to a spot in the global data collector variable
                // via his id for easy retrieving
                DataCollectorHolder.dataCollectors.push(supervisor);
                DataCollectorHolder.lookupDataCollector[supervisor.id] = supervisor;
                scSupervisor.supervisorLookup[supervisor.id] = supervisor;
                //scSchool.schoolLookup[school.id].supervisor_name = supervisor.name;

                //$scope.lookupDataCollector[supervisor.id].school_name = school.name;

            });

            sortSupervisors();
        };


        scSupervisor.addDataCollector = function(formData){
            var defer = $q.defer();
            // process the form
            $http.post('/admin/data_collectors', formData).
                //success callback
                success(function (value) {
                    if (value[0] == 's' && value[1] == 'u') {
                        scSupervisor.updateSupervisorArray()
                            .then(function (data) {
                                if (data) defer.resolve('success');
                            });
                    }else {
                        defer.resolve(value);
                    }
                })
                //error callback
                .error(function (httpResponse) {
                    defer.resolve(false);
                });


            return defer.promise;
        };

        scSupervisor.deleteSupervisor = function(supervisorId){

            var defer = $q.defer();
            if (scSupervisor.supervisorLookup[supervisorId]) {
                var deleteUrl = '/admin/data_collector/'+ supervisorId +'/delete';
                $http.post(deleteUrl, {})
                    //success callback
                    .success(function(data){
                        if (data[0] == 's' && data[1] == 'u') {
                            scSupervisor.updateSupervisorArray()
                                .then(function (data) {
                                    if (data) defer.resolve(true);
                                });
                        }else  defer.resolve(false);
                    })
//                error callback
                    .error(function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };


        //After posting the form for editing, this gets called
        scSupervisor.editSupervisor = function(formData){
            var defer = $q.defer();
            var editUrl = '/admin/data_collector/' + formData.id +'/update';
            $http.post(editUrl, formData)
                //success callback
                .success(function (value, responseHeaders) {
                    if (value) {
                        //If successful, requery the new supervisors and assign them to the model service variable
                        scSupervisor.updateSupervisorArray()
                            .then(function (data) {
                                if (data) {
                                    defer.resolve('success');
                                }
                            });
                    } else {
                        defer.resolve(false)
                    }
                })
                //error callback
                .error(function (httpResponse) {
                    defer.reject(false);
                });
            return defer.promise;
        };


        return scSupervisor;



    }]);

