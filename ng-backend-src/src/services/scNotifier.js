/**
 * Created by sophismay on 6/17/14.
 */

/**
 *  Service to handle notifications
 *
 *  2 functions:
 *
 *  function notifySuccess:
 *      parameters: (title, message)
 *      returns:    returns toaster notification
 */



// wrapping around toaster.js
schoolMonitor.service('scUserService', ['toaster'])
    .factory('scNotifier', ['toaster', function(scToaster){
     return {
         notifySuccess: function(title, message){
             scToaster.pop('success', title, message);
         },
         notifyFailure: function(title, message){
            scToaster.pop('error', title, message);
         },
         notifyInfo: function(title, message){
            scToaster.pop('note', title, message);
         },
         notifyInfoWithCallback: function(title, message, functionToCall){
            scToaster.pop('note', title, message, 5000, 'trustedHtml', functionToCall);
         }
     }
}]);

// Display an info toast with
//$scope.popToast = function(type, title, html, timeOut, trustedHtml, callback){
//    toaster.pop('success', "title", '<ul><li>Render html</li></ul>', 5000, 'trustedHtml');
//    toaster.pop('error', "title", '<ul><li>Render html</li></ul>', null, 'trustedHtml');
//    toaster.pop('warning', "title", null, null, 'template');
//    toaster.pop('note', "title", "text");
//};
