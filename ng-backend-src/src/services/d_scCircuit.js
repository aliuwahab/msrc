/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Circuit resource
 *
 */

schoolMonitor.factory('scCircuit', ['scRegion', 'scDistrict', '$resource', '$q','CacheFactory','$http','scNotifier','$rootScope','$timeout',
    function(scRegion, scDistrict, $resource, $q, CacheFactory, $http, scNotifier, $rootScope, $timeout){

        var circuitResource = $resource('/admin/ghana/circuits',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/circuit/create', params:{}},
            retrieveOneCircuit:{method:'GET', url:'/admin/circuit/:id/edit', params:{id : '@id'}},
            editCircuit:{method:'POST', url:'/admin/circuit/:id/update', params:{id : '@id'}},
            deleteCircuit:{method:'POST', url:'/admin/circuit/:id/delete', params:{id : '@id'}}
        });

        var scCircuit = {};

        var circuits;

        scCircuit.calculateRank =  function(){
            return {};
        };

        scCircuit.calculateRating = function(circuit_id){
//        angular.forEach(scCircuit.circuitLookup[circuit_id].allSchools, function(circuit, index){
//            scCircuit.circuitLookup[circuit_id].inclusiveness = 0;
//        });
        };

        scCircuit.circuits = [];

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('circuitCache')) {
            var circuitCache = CacheFactory('circuitCache', {
                //maxAge: 60 * 60 * 1000 // 1 hour
                //deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        var allCircuitsUrl = '/admin/ghana/circuits', loadingCircuits = false;

        scCircuit.getAllCircuits = function(){
            if (loadingCircuits) {
                return;
            }
            loadingCircuits = true;
            var defer = $q.defer();
            var cachedData = circuitCache.get(allCircuitsUrl);

            if (!cachedData) {
                scNotifier.notifyInfo('Info', 'Updating circuits...');
                circuitResource.query().$promise.then(function(data){
                    if (data) {

                        data.sort(function (a, b) {
                            return a.name.toLowerCase() - b.name.toLowerCase();
                        });

                        scCircuit.circuits = data;
                        circuitCache.put(allCircuitsUrl, data);
                        loadingCircuits = false;
                        defer.resolve(true);
                    }else{
                        loadingCircuits = false;
                        defer.resolve(false);
                    }
                }, function(){
                    loadingCircuits = false;
                    defer.resolve(false);
                });
            }else{
                scCircuit.circuits = cachedData;
                loadingCircuits = false;
                defer.resolve(true);
            }
            return defer.promise;
        };



        scCircuit.updateCircuitArray = function(){
            var defer = $q.defer();
            circuitCache.remove(allCircuitsUrl);
            this.circuits = [];

            this.getAllCircuits().then(
                function(status){

                    if (status) {
                        $timeout(function () {
                            $rootScope.loading = false;
                            $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});
                            defer.resolve(true);
                        }, 100);
                    }else{
                        defer.resolve(false);
                    }
                }, function(){
                    $rootScope.loading = false;

                    defer.resolve(false);
                });
            return defer.promise;
        };

        $rootScope.reloadCircuits = function () {
            $rootScope.loading = true;
            scCircuit.updateCircuitArray();
        };

        scCircuit.init = function(){
            scCircuit.circuitLookup ={};
            angular.forEach(this.circuits, function(circuit, index){

                circuit.rating = 0;
                scCircuit.circuitLookup[circuit.id] = circuit;

                scCircuit.circuitLookup[circuit.id].inclusiveness = 0;
                scCircuit.circuitLookup[circuit.id].inclusivenessHolder = 0;

                scCircuit.circuitLookup[circuit.id].safe_protective = 0;
                scCircuit.circuitLookup[circuit.id].safe_protectiveHolder = 0;

                scCircuit.circuitLookup[circuit.id].healthy = 0;
                scCircuit.circuitLookup[circuit.id].healthyHolder = 0;

                scCircuit.circuitLookup[circuit.id].gender_friendly =0;
                scCircuit.circuitLookup[circuit.id].gender_friendlyHolder = 0;

                scCircuit.circuitLookup[circuit.id].effective_teaching_learning =0;
                scCircuit.circuitLookup[circuit.id].effective_teaching_learningHolder = 0;

                scCircuit.circuitLookup[circuit.id].community_involvement = 0;
                scCircuit.circuitLookup[circuit.id].community_involvementHolder = 0;

                scCircuit.circuitLookup[circuit.id].supervisorAttendanceHolder = 0;
                scCircuit.circuitLookup[circuit.id].supervisorAttendanceAverage = 0;

                scCircuit.circuitLookup[circuit.id].headteacherAttendanceHolder = 0;
                scCircuit.circuitLookup[circuit.id].headteacherAttendanceAverage = 0;

                //This is to keep track of how many schools are in each district
                scCircuit.circuitLookup[circuit.id].totalSchools = 0;

                //This is to contain all schools under this circuit to be looked up by id
                scCircuit.circuitLookup[circuit.id].allSchools = {};

                //This is to contain all schools under this circuit
                scCircuit.circuitLookup[circuit.id].allSchoolsHolder = [];

                //This is to keep track of the circuit supervisor of this circuit
                scCircuit.circuitLookup[circuit.id].circuitSupervisor = null;

                //This is to keep track of how many headteachers are in each region
                scCircuit.circuitLookup[circuit.id].head_teachers = 0;

                //This is to hold headteachers in each circuit
                scCircuit.circuitLookup[circuit.id].head_teachers_holder = [];

                //Assign region name to circuit object
                scCircuit.circuits[index].regionName = scRegion.regionLookup[circuit.region_id].name;

                //Increment its region by 1
                scRegion.regionLookup[circuit.region_id].totalCircuits += 1;

                //add circuit to the region lookup object and associate it by its id
                scRegion.regionLookup[circuit.region_id].allCircuits[circuit.id] = circuit;

                //add circuit to the region lookup object
                scRegion.regionLookup[circuit.region_id].allCircuitsHolder.push(circuit);

                //Assign district name to circuit object
                scCircuit.circuits[index].districtName = scDistrict.districtLookup[circuit.district_id].name;

                //Increment its district by 1
                scDistrict.districtLookup[circuit.district_id].totalCircuits += 1;

                //add circuit to the district lookup object and associate it by its id
                scDistrict.districtLookup[circuit.district_id].allCircuits[circuit.id] = circuit;

                //add circuit to the district lookup object
                scDistrict.districtLookup[circuit.district_id].allCircuitsHolder.push(circuit);

                /*kgTodo - Calculate Circuit Rating*/
                //Calculate circuits rating and position
//            scCircuit.circuits[index].rating = Math.floor(Math.random() * 100 + 1);
//            scCircuit.circuits[index].position = Math.floor(Math.random() * 10 + 1);


            })
        };


        scCircuit.addCircuit= function(formData){
            var defer = $q.defer();
            // process the form
            circuitResource.create(formData,
                //success callback
                function (value) {
                    if (value[0] == 's' && value[1] == 'u') {
                        scCircuit.updateCircuitArray().then(
                            function(status){
                                if (status) {
                                    defer.resolve(true);
                                }else{
                                    defer.resolve(false);
                                }
                            },
                            function(){
                                defer.resolve(false);
                            });

                    }else {
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };

//    Retrieve a single district to view or edit
        scCircuit.retrieveOneCircuit = function(circuit_id){
            var defer = $q.defer();
            circuitResource.retrieveOneCircuit({id: circuit_id},
                //After retrieving the region to edit, return it to the controller
                function (data, responseHeaders) {
                    defer.resolve(true);
                    scCircuit.circuitRetrieved = data
                },
                //Error function
                function (httpResponse) {
                    defer.resolve(false);
                    return false
                });
            return defer.promise;
        };


        scCircuit.editCircuit = function(formData){
            var defer = $q.defer();
            circuitResource.editCircuit(formData,
                //success callback
                function (value, responseHeaders) {
//                        the value returned is a json object, hence the wierd checking to see success
//                 if (value == 'success' || (value[0] == 's' && value[1] == 'u')) {
//                     //If successful, requery the new regions and assign them to the parent regions variable
//                     scCircuit.updateCircuitArray();
//                     defer.resolve(true);
//                 } else {
//                     scCircuit.updateCircuitArray();
//                     defer.resolve(false)
//                 }
                    scCircuit.updateCircuitArray().then(
                        function(status){
                            if (status) {
                                defer.resolve(true);
                            }else{
                                defer.reject(false);
                            }
                        },
                        function(){
                            defer.reject(false);
                        });
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };




        scCircuit.deleteCircuit = function(circuit_id){

            var defer = $q.defer();
            if (scCircuit.circuitLookup[circuit_id]) {
                circuitResource.deleteCircuit({id : circuit_id},
                    //success callback
                    function(data){
                        if (data[0] == 's' && data[1] == 'u') {

                            scCircuit.updateCircuitArray().then(
                                function(status){
                                    if (status) {
                                        defer.resolve(true);
                                    }else{
                                        defer.resolve(false);
                                    }
                                },
                                function(){
                                    defer.resolve(false);
                                }
                            );
                        }else{
                            defer.resolve(false);
                        }
                    },
//                error callback
                    function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };

        scCircuit.clearCache = function(){
            circuitCache.removeAll();
        };

        scCircuit.fetchTermlyData = function(params){
            //return $resource('/admin/ghana/districts', {}, {
            //    getTermlyData:{method:'GET', url:'/admin/totals/termly/data', params:{country_id: 1}, isArray:true, cache:false}
            //}).getTermlyData({circuit_id : circuit_id});

            var aCircuitsTermlyDataUrl = '/admin/totals/termly/data';
            var aCircuitsCachedTermlyDataUrl = '/admin/totals/termly/data?country_id=1'+JSON.stringify(params);

            var deferred = $q.defer();

            var cachedData;
            // cachedData = circuitCache.get(aCircuitsCachedTermlyDataUrl);

            if (!cachedData ) {
                $http.get(aCircuitsTermlyDataUrl , {params : params })
                    .success(function(data){
                        circuitCache.put(aCircuitsCachedTermlyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


        return scCircuit;



    }]);

