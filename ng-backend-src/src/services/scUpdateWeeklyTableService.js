/**
 * Created by sophismay on 6/18/14.
 */

/**
 *  Service for School resource API
 *
 *
 */

schoolMonitor.factory('scUpdateWeeklyTable', ['scRegion', 'scSchool', 'scCircuit', '$resource', '$q','scGeneralService',
    '$http','$rootScope','scNotifier','School','$timeout',
    function(scRegion, scSchool, scCircuit, $resource, $q, scGeneralService, $http, $rootScope, scNotifier, School, $timeout){


        var that = this;
        var dataloader = 0; // reset the variable that checks that both normal and special are loaded

        this.updatingModel = false;

        this.runWeeklyUpdateTotalsForModel = function (dataParams) {
            /*        var dataParams = {school_id: $stateParams.id};*/

            $timeout(function () {
                that.updatingModel = 5; //this variable checks to display the loading gif whiles loading data, and also updates the
                // loader gif
            });

            dataloader = 0; // reset the variable that checks that both normal and special are loaded
            /* if (week || term || year) {
                 angular.extend(dataParams, {
                     data_collector_type: "head_teacher",
                     week_number: week,
                     term: term,
                     year: year
                 });
             }
 */
            /*if not, load it from server and keep track of it in the variable */
            School.allSpecialEnrollmentData(dataParams).$promise
                .then(function (data) {
                    that.loadedSpecialEnrollmentInfoFromServer = data[0].special_school_enrolment;
                    updateDataLoaded({loader: ++dataloader});
                    that.updatingModel += 15;
                });


            /*if not load it from server and keep track of it in the variable */
            School.allEnrollmentData(dataParams).$promise
                .then(function (data) {
                    that.loadedEnrollmentInfoFromServer = data[0].school_enrolment;
                    updateDataLoaded({loader: ++dataloader});
                    that.updatingModel += 15;

                });
            /*Load special attendance data also and keep it in the service*/
            School.allSpecialAttendanceData(dataParams).$promise
                .then(function (data) {
                    that.loadedSpecialAttendanceInfoFromServer = data[0].special_students_attendance;
                    updateDataLoaded({loader: ++dataloader});
                    that.updatingModel += 15;

                });
            /* Load attendance data also and keep it in the service */
            School.allAttendanceData(dataParams).$promise
                .then(function (data) {
                    that.loadedAttendanceInfoFromServer = data[0].school_attendance;
                    updateDataLoaded({loader: ++dataloader});
                    that.updatingModel += 15;

                });
            /*for loading teacher attendance*/
            School.allSchoolTeachersPunctuality_LessonPlan(dataParams).$promise
                .then(function (punctuality_lesson_plans) {
                    that.allSchoolTeachersPunctuality_LessonPlan = punctuality_lesson_plans;
                    that.updatingModel += 15;
                    updateDataLoaded({loader: ++dataloader});
                });
        };

        //Fired from the below when data is deleted successfully
        function updateDataLoaded(data) {
            //this should be greater than 3. that's when both special and normal have loaded
            if (data.loader > 4) {
                runEnrollmentUpdate();
            }
        }


        var runEnrollmentUpdate = function () {
            dataloader = 0; //This variable prevents the data prepping from happening until both are loaded
            var enrollmentData = {
                totals : {}
            };/*an object whose keys are concatenation of the year,week,term and dc to track totals of data*/

            for (var enr_index = 0; enr_index < that.loadedEnrollmentInfoFromServer.length; enr_index ++){
                var item = that.loadedEnrollmentInfoFromServer[enr_index];

                if(item.data_collector_type === 'circuit_supervisor'){
                    continue;
                }


                var concatenate = item.year + item.term + item.week_number + item.data_collector_type + item.school_id;
                if (enrollmentData.totals[concatenate]) {
                    enrollmentData.totals[concatenate].normal += parseInt(item.total_enrolment);
                    enrollmentData.totals[concatenate].boys += parseInt(item.num_males);
                    enrollmentData.totals[concatenate].girls += parseInt(item.num_females);
                } else {
                    enrollmentData.totals[concatenate] = {
                        normal: parseInt(item.total_enrolment),
                        special: 0,
                        boys : parseInt(item.num_males),
                        special_boys : 0,
                        girls : parseInt(item.num_females),
                        special_girls : 0,
                        week_number :  item.week_number,
                        term : item.term,
                        year : item.year,
                        country_id : item.country_id,
                        region_id : item.region_id,
                        district_id : item.district_id,
                        circuit_id : item.circuit_id,
                        school_id : item.school_id
                    }
                }

                for (var enr_subIndex = 0; enr_subIndex < that.loadedSpecialEnrollmentInfoFromServer.length; enr_subIndex ++) {
                    var special_item = that.loadedSpecialEnrollmentInfoFromServer[enr_subIndex];
                    if (special_item.level == item.level &&
                        special_item.term == item.term &&
                        special_item.week_number == item.week_number &&
                        special_item.year == item.year &&
                        special_item.data_collector_type == item.data_collector_type) {
                        enrollmentData.totals[concatenate].special += parseInt(special_item.total_enrolment);
                        enrollmentData.totals[concatenate].special_boys += parseInt(special_item.num_males);
                        enrollmentData.totals[concatenate].special_girls += parseInt(special_item.num_females);
                    }
                }

            }
            that.updatingModel += 10;


            /*this function send the totals calculated to the server to update the weekly table*/
            var enrollmentHolder = [];

            angular.forEach(enrollmentData.totals, function (value, key) {
                enrollmentHolder.push({
                    normal_enrolment_total_boys : value.boys,
                    normal_enrolment_total_girls : value.girls,
                    normal_enrolment_total_students : value.normal,
                    // normal_attendance_total_boys : value.boys,
                    // normal_attendance_total_girls : value.boys,
                    // normal_attendance_total_students : value.boys,
                    special_enrolment_total_boys : value.special_boys,
                    special_enrolment_total_girls : value.special_girls,
                    special_enrolment_total_students : value.special,
                    // special_attendance_total_boys : value.boys,
                    // special_attendance_total_girls : value.boys,
                    // special_attendance_total_students : value.boys,
                    year : value.year,
                    term : value.term,
                    week_number : value.week_number,
                    country_id : value.country_id,
                    region_id : value.region_id,
                    district_id : value.district_id,
                    circuit_id : value.circuit_id,
                    school_id : value.school_id
                })
            });

            scSchool.updateWeeklyTableForEnrollment(enrollmentHolder)
                .success(function (successData) {
                    if (successData.status == 200) {
                        scNotifier.notifySuccess("Recalculation", "Enrolment totals have been updated.")
                    }
                })
                .error(function (errData) {
                    scNotifier.notifySuccess("Recalculation", "Enrolment totals have been updated.")
                    // scNotifier.notifyFailure("Recalculation", "Enrolment totals failed to update")
                })
                .finally(function () {
                    that.updatingModel += 5;
                    runAttendanceUpdate()
                });
        };

        var runAttendanceUpdate = function () {

            var attendanceData = {
                totals : {}
            };/*an object whose keys are concatenation of the year,week,term and dc to track totals of data*/
            for (var att_index = 0; att_index < that.loadedAttendanceInfoFromServer.length; att_index ++){
                item = that.loadedAttendanceInfoFromServer[att_index];
                concatenate = item.year + item.term + item.week_number + item.data_collector_type + item.school_id;

                if (attendanceData.totals[concatenate]) {
                    attendanceData.totals[concatenate].normal += parseInt(item.total);
                    attendanceData.totals[concatenate].boys += parseInt(item.num_males);
                    attendanceData.totals[concatenate].girls += parseInt(item.num_females);
                } else {
                    attendanceData.totals[concatenate] = {
                        normal: parseInt(item.total),
                        special: 0,
                        boys : parseInt(item.num_males),
                        special_boys : 0,
                        girls : parseInt(item.num_females),
                        special_girls : 0,
                        week_number :  item.week_number,
                        term : item.term,
                        year : item.year,
                        country_id : item.country_id,
                        region_id : item.region_id,
                        district_id : item.district_id,
                        circuit_id : item.circuit_id,
                        school_id : item.school_id
                    }
                }
                for (var att_subIndex = 0; att_subIndex < that.loadedSpecialAttendanceInfoFromServer.length; att_subIndex ++) {
                    special_item = that.loadedSpecialAttendanceInfoFromServer[att_subIndex];

                    if (special_item.level === item.level &&
                        special_item.term === item.term &&
                        special_item.week_number === item.week_number &&
                        special_item.year === item.year &&
                        special_item.data_collector_type === item.data_collector_type)
                    {
                        attendanceData.totals[concatenate].special += parseInt(special_item.total);
                        attendanceData.totals[concatenate].special_boys += parseInt(special_item.num_males);
                        attendanceData.totals[concatenate].special_girls += parseInt(special_item.num_females);
                    }
                }
            }

            that.updatingModel += 10;

            var attendanceHolder = [];

            angular.forEach(attendanceData.totals, function (value, key) {
                attendanceHolder.push({
                    // normal_enrolment_total_boys : value.boys,
                    // normal_enrolment_total_girls : value.girls,
                    // normal_enrolment_total_students : value.normal,
                    normal_attendance_total_boys : value.boys,
                    normal_attendance_total_girls : value.girls,
                    normal_attendance_total_students : value.normal,
                    // special_enrolment_total_boys : value.special_boys,
                    // special_enrolment_total_girls : value.special_girls,
                    // special_enrolment_total_students : value.special,
                    special_attendance_total_boys : value.special_boys,
                    special_attendance_total_girls : value.special_girls,
                    special_attendance_total_students : value.special,
                    year : value.year,
                    term : value.term,
                    week_number : value.week_number,
                    country_id : value.country_id,
                    region_id : value.region_id,
                    district_id : value.district_id,
                    circuit_id : value.circuit_id,
                    school_id : value.school_id
                })
            });

            scSchool.updateWeeklyTableForAttendance(attendanceHolder)
                .success(function (successData) {
                    if (successData.status == 200) {
                        scNotifier.notifySuccess("Recalculation", "Attendance totals have been updated.")
                    }
                })
                .error(function (errData) {
                    scNotifier.notifySuccess("Recalculation", "Attendance totals have been updated.")

                    // scNotifier.notifyFailure("Recalculation", "Attendance totals failed to update")
                })
                .finally(function () {
                    that.updatingModel += 5;
                    runTeacherAttendanceUpdate()
                });
        };



        function runTeacherAttendanceUpdate() {
            //These variables only keep track of all weeks, term and years in the punctuality and class management objects
            var teacherAttendanceTracker = [], teacherAttendanceTotals = {};

            for (var tp = 0; tp < that.allSchoolTeachersPunctuality_LessonPlan.length; tp ++) {

                var teacherPunctualityObject = that.allSchoolTeachersPunctuality_LessonPlan[tp];

                teacherPunctualityObject.week_number = parseInt(teacherPunctualityObject.week_number);//parse it to int to enforce consistency in searching and filtering

                /*start the total teacher attendance here*/

                /*the concat variable includes the teacher ID and School ID so we only add the teachers attendance once, to prevent duplicates for that week, term and year in that school*/
                var concatWeekly = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number +'-'+ teacherPunctualityObject.teacher_id +'-'+ teacherPunctualityObject.school_id;
                /*The concat period is only the period WITHOUT the teachers ID, but for that school*/
                var concatPeriod = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number +'-'+ teacherPunctualityObject.school_id;

                /*Begin Weekly Aggregations*/
                if (teacherAttendanceTracker.indexOf(concatWeekly) < 0) {

                    if (angular.isDefined(teacherAttendanceTotals[concatPeriod])) {
                        teacherAttendanceTotals[concatPeriod].totalAttendance += teacherPunctualityObject.days_present;
                        teacherAttendanceTotals[concatPeriod].actualTeacherTotal++;

                        teacherAttendanceTotals[concatPeriod].averageAttendance =
                            (teacherAttendanceTotals[concatPeriod].totalAttendance / (teacherPunctualityObject.days_in_session * teacherAttendanceTotals[concatPeriod].actualTeacherTotal)) * 100;
                        /*
                        * actualAtt
                        * ----------  x 100
                        * expectedAtt (total days in session x total num of teacher)*/

                    }else{
                        teacherAttendanceTotals[concatPeriod] = {
                            totalAttendance :  teacherPunctualityObject.days_present,
                            actualTeacherTotal : 1,
                            expectedTeacherTotal : scSchool.schoolLookup[teacherPunctualityObject.school_id].teachersInSchoolHolder.length,
                            teachersTotal : scSchool.schoolLookup[teacherPunctualityObject.school_id].teachersInSchoolHolder.length,
                            days_in_session : teacherPunctualityObject.days_in_session,
                            averageAttendance : (teacherPunctualityObject.days_present/(teacherPunctualityObject.days_in_session * scSchool.schoolLookup[teacherPunctualityObject.school_id].teachersInSchoolHolder.length)) * 100,
                            year: teacherPunctualityObject.year,
                            term: teacherPunctualityObject.term,
                            week_number: teacherPunctualityObject.week_number,
                            country_id : teacherPunctualityObject.country_id,
                            region_id : teacherPunctualityObject.region_id,
                            district_id : teacherPunctualityObject.district_id,
                            circuit_id : teacherPunctualityObject.circuit_id,
                            school_id : teacherPunctualityObject.school_id
                        };

                        teacherAttendanceTracker.push(concatWeekly);
                    }

                }




            }

            var teacherAttHolder = [];
            angular.forEach(teacherAttendanceTotals, function (value, key) {
                /*calculate the weeks percentage*/
                /*first get the expected total attendance = total teachers in school x number of days school was in session*/
                /*then use the actual total attendance, divided by the expected total number of teachers and multiply by 100 */
                var aveAttendance = (value.totalAttendance/(value.days_in_session * value.actualTeacherTotal)) * 100;

                teacherAttHolder.push({
                    average_teacher_attendance : Math.round(aveAttendance),
                    current_number_of_teachers : value.teachersTotal,
                    year : value.year,
                    term : value.term,
                    week_number : value.week_number,
                    country_id : value.country_id,
                    region_id : value.region_id,
                    district_id : value.district_id,
                    circuit_id : value.circuit_id,
                    school_id : value.school_id
                })
            });


            scSchool.updateWeeklyTableForTeacherAttendance(teacherAttHolder)
                .success(function (successData) {
                    if (successData.status === 200) {
                        scNotifier.notifySuccess("Recalculation", "Teacher Attendance totals have been updated.")
                    }
                })
                .error(function (errData) {
                    scNotifier.notifySuccess("Recalculation", "Teacher Attendance totals have been updated.")

                    // scNotifier.notifyFailure("Recalculation", "Teacher Attendance totals failed to update")
                })
                .finally(function () {
                    that.updatingModel += (101 - that.updatingModel);

                    $rootScope.$broadcast("finishedUpdating");
                });




        }

        return that;


    }]);

