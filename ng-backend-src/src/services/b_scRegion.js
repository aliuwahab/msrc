/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.factory('scRegion', ['$resource' , '$rootScope', '$q', 'CacheFactory','$http','scNotifier',
    function($resource, $rootScope,$q, CacheFactory, $http, scNotifier){

    var regionResource = $resource('/admin/ghana/regions',{},{
        query:{isArray:true,cache:false},
        create:{method:'POST', url:'/admin/region/create', params:{}},
        retrieveOneRegion:{method:'GET', url:'/admin/region/:id/edit', params:{id : '@id'}},
        editRegion:{method:'POST', url:'/admin/region/:id/update', params:{id : '@id'}},
        deleteRegion:{method:'POST', url:'/admin/region/:id/delete', params:{id : '@id'}}
    });

    var scRegion = {};

    // Check to make sure the cache doesn't already exist
    if (!CacheFactory.get('regionCache')) {
        var regionCache = CacheFactory('regionCache', {
            //maxAge: 60 * 60 * 1000 // 1 hour
            //deleteOnExpire: 'aggressive'
            //onExpire: function (key, value) {
            //    $http.get(key).success(function (data) {
            //        profileCache.put(key, data);
            //    });
            //}
        });
    }

//    var health_status = {
//        0 : 'very poor',
//        1 : 'bad',
//        2 : 'needs attention',
//        3 : 'healthy'
//    };

    scRegion.regions = [];

    var url = '/admin/ghana/regions', loadingRegions = false;

    scRegion.getAllRegions = function(){
        if (loadingRegions) {
            return;
        }
        loadingRegions = true;
        var deferred = $q.defer();

        var cachedData = regionCache.get(url);

        if (cachedData == undefined ) {
            scNotifier.notifyInfo('Info', 'Fetching regions from the server');
            $http.get(url)
                .success(function(data){
                    
                    data.sort(function (a, b) {
                        return a.name.toLowerCase() - b.name.toLowerCase();
                    });
                    
                    regionCache.put(url, data);
                    scRegion.regions = data;
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.resolve(false);
                })
                .finally(function () {
                    loadingRegions = false;
                })
        }else{
            scRegion.regions = cachedData;
            deferred.resolve(cachedData);
            loadingRegions = false;
        }
        return deferred.promise;
        //if (!regions) {
        //    var defer = $q.defer();
        //    regionResource.query({},
        //        function(data){
        //            if (data) {
        //                defer.resolve(true);
        //                scRegion.regions = data;
        //            }else{
        //                defer.resolve(false)
        //            }
        //        }, function(){
        //            defer.resolve(false);
        //        });
        //    return defer.promise;
        //}
        //return this.regions;
    };

    scRegion.updateSupervisorArray = function(){
        regionCache.remove(url);
        this.getAllRegions().then(function () {
            scRegion.regions.sort(function(a, b){
                if (a.name > b.name) {
                    return 1;
                }else if (a.name < b.name) {
                    return -1
                }else{
                    return 0
                }
            })
        });
    };


    scRegion.init = function(){

        scRegion.regionLookup = {};
        angular.forEach(this.regions, function(region, index){

            region.rating = 0;
            scRegion.regionLookup[region.id] = region;

            scRegion.regions[index].inclusiveness =  0;
            scRegion.regions[index].inclusivenessHolder = 0;

            scRegion.regions[index].safe_protective = 0;
            scRegion.regions[index].safe_protectiveHolder = 0;

            scRegion.regions[index].healthy = 0;
            scRegion.regions[index].healthyHolder = 0;

            scRegion.regions[index].gender_friendly = 0;
            scRegion.regions[index].gender_friendlyHolder = 0;

            scRegion.regions[index].effective_teaching_learning = 0;
            scRegion.regions[index].effective_teaching_learningHolder = 0;

            scRegion.regions[index].community_involvement = 0;
            scRegion.regions[index].community_involvementHolder = 0;

            scRegion.regions[index].supervisorAttendanceHolder = 0;
            scRegion.regions[index].supervisorAttendanceAverage = 0;

            scRegion.regions[index].headteacherAttendanceHolder = 0;
            scRegion.regions[index].headteacherAttendanceAverage = 0;

            scRegion.reviewDataExists = false;

            //This is to keep track of how many districts are in each region
            scRegion.regionLookup[region.id].totalDistricts = 0;

            //This is to contain all districts under this region that can be looked up by id
            scRegion.regionLookup[region.id].allDistricts = {};

            //This is to contain all districts under this region that can be looped over
            scRegion.regionLookup[region.id].allDistrictsHolder = [];

            //This is to keep track of how many circuits are in each region
            scRegion.regionLookup[region.id].totalCircuits = 0;

            //This is to contain all circuits under this region to be looked up by id
            scRegion.regionLookup[region.id].allCircuits = {};

            //This is to contain all circuits under this region
            scRegion.regionLookup[region.id].allCircuitsHolder = [];

            //This is to keep track of how many schools are in each region
            scRegion.regionLookup[region.id].totalSchools = 0;

            //This is to contain all schools under this region to be looked up by id
            scRegion.regionLookup[region.id].allSchools = {};

            //This is to contain all schools under this region
            scRegion.regionLookup[region.id].allSchoolsHolder = [];

            //This is to keep track of how many supervisors are in each region
            scRegion.regionLookup[region.id].circuit_supervisors = 0;

            //This is to hold supervisors in each region
            scRegion.regionLookup[region.id].circuit_supervisors_holder = [];

            //This is to keep track of how many headteachers are in each region
            scRegion.regionLookup[region.id].head_teachers = 0;

            //This is to hold headteachers in each region
            scRegion.regionLookup[region.id].head_teachers_holder = [];

            /*kgTodo - Calculate region rating and position*/
            //This is randomly generated positions and ratings pending actual implementation
            //scRegion.regions[index].rating =  Math.floor(Math.random() * 100);
//            scRegion.regions[index].position = Math.floor(Math.random() * 8) + 1;

            //This is randomly generated school review ratings pending actual implementation
        });

        scRegion.regions.sort(function(a, b){
            if (a.name > b.name) {
                return 1;
            }else if (a.name < b.name) {
                return -1
            }else{
                return 0
            }
        })

    };

    scRegion.addRegion = function(formData){
        var defer = $q.defer();
        // process the form
        regionResource.create(formData,
            //success callback
            function (value) {
                if (value[0] == 's' && value[1] == 'u') {
                    scRegion.updateSupervisorArray();
                    defer.resolve(true);
                }else {
//                    scRegion.addRegionStatus = value;
                    defer.resolve(false);
                }
            },
            //error callback
            function (httpResponse) {
                defer.resolve(false);
            });
        return defer.promise;
    };

    scRegion.deleteRegion = function(region_id){

        var defer = $q.defer();
        if (scRegion.regionLookup[region_id]) {
            regionResource.deleteRegion({id : region_id},
                //success callback
                function(data){
                    if (data[0] == 's' && data[1] == 'u') {
                        scRegion.updateSupervisorArray();
                        defer.resolve(true);
                    }else{
                        defer.resolve(false);
                    }
                },
//                error callback
                function(){
                    defer.resolve(false)
                });
        }else{
            defer.resolve(false);
        }
        return defer.promise;
    };

//    Retrieve a single region to view or edit
    scRegion.retrieveOneRegion = function(region_id){
        var defer = $q.defer();
        regionResource.retrieveOneRegion({id: region_id},
            //After retrieving the region to edit, return it to the controller

            function (data, responseHeaders) {
                scRegion.regionRetrieved = data;
                defer.resolve(true);
            },
            //Error function

            function (httpResponse) {
                defer.resolve(false);
                return false
            });
        return defer.promise;
    };

    //After posting the form for editing, this gets called
    scRegion.editRegion = function(formData){
        var defer = $q.defer();
        regionResource.editRegion(formData,
            //success callback
            function (value, responseHeaders) {
//                        the value returned is a json object, hence the wierd checking to see success
                if (value == 'success' || (value[0] == 's' && value[1] == 'u')) {
                    //If successful, requery the new regions and assign them to the parent regions variable
                    scRegion.updateSupervisorArray();
                    defer.resolve(true);
                } else {
                    defer.resolve(false)
                }
            },
            //error callback
            function (httpResponse) {
                defer.resolve(false);
            });
        return defer.promise;
    };

    scRegion.sendNotification = function(){

    };

        scRegion.fetchTermlyData = function(params){

            var aRegionsTermlyDataUrl = '/admin/totals/termly/data';
            var aRegionsCachedTermlyDataUrl = '/admin/totals/termly/data?country_id=1'+JSON.stringify(params);

            var deferred = $q.defer();

            var cachedData = regionCache.get(aRegionsCachedTermlyDataUrl);

            if (cachedData == undefined ) {
                $http.get(aRegionsTermlyDataUrl , {params : params })
                    .success(function(data){
                        regionCache.put(aRegionsCachedTermlyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


    return scRegion;



}]);

