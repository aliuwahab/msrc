/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Region resource
 *
 */

//Usage
//$resource(url, [paramDefaults], [actions], options);

//action parameters
//action – {string} – The name of action. This name becomes the name of the method on your resource object.
//    method – {string} – HTTP request method. Valid methods are: GET, POST, PUT, DELETE, and JSONP.
//    params – {Object=} – Optional set of pre-bound parameters for this action. If any of the parameter value is a function, it will be executed every time when a param value needs to be obtained for a request (unless the param was overridden).
//url – {string} – action specific url override. The url templating is supported just like for the resource-level urls.
//    isArray – {boolean=} – If true then the returned object for this action is an array, see returns section.
//    cache – {boolean|Cache} – If true, a default $http cache will be used to cache the GET request, otherwise if a cache instance built with $cacheFactory, this cache will be used for caching.
//                                                                                                                                                                                            timeout – {number|Promise} – timeout in milliseconds, or promise that should abort the request when resolved.
//    responseType - {string} - see requestType.
//    interceptor - {Object=} - The interceptor object has two optional methods - response and responseError. Both response and responseError interceptors get called with http response object. See $http interceptors.

//options
//stripTrailingSlashes – {boolean} – If true then the trailing slashes from any calculated URL will be stripped. (Defaults to true.)

schoolMonitor.factory('STARResource', function($resource){
        return $resource('/admin/ghana',{},{

            //<-- START of School Report Card  Methods-->
            getStarAttendance : {method : 'GET', url : '/admin/targeted/instructions/attendance',
                params:{ country_id : 1}, isArray:false, cache : false },

uploadELibraryFile : {method : 'POST', url : '/admin/targeted/instructions/upload/elibrary/file',
                params:{ country_id : 1}, isArray:false, cache : false },



        });
    });