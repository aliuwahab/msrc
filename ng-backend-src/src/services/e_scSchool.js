/**
 * Created by sophismay on 6/18/14.
 */

/**
 *  Service for School resource API
 *
 *
 */

schoolMonitor.factory('scSchool', ['scRegion', 'scDistrict', 'scCircuit', '$resource', '$q','scGeneralService',
    '$http','CacheFactory','scNotifier', '$rootScope','DataCollectorHolder','$timeout',
    function(scRegion, scDistrict, scCircuit, $resource, $q, scGeneralService, $http, CacheFactory, scNotifier, $rootScope, DataCollectorHolder, $timeout){
        var schoolResource = $resource('/admin/ghana/schools',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/create_school', params:{}, responseType : 'text'},

            newTeacher :{method:'POST', url:'/admin/school_report_card/form/create/teacher', params:{}, responseType : 'text'},

            transferTeacher :{method:'POST', url:'/admin/school_report_card/teacher/transfer', params:{}, responseType : 'json'},

            retrieveOneSchool : {method : 'GET', url : '/admin/school/:id/edit', params:{id : '@id'}},

            editSchool : {method : 'POST', url : '/admin/school/:id/update', params:{id : '@id'}},

            deleteSchool:{method:'POST', url:'/admin/school/:id/delete', params:{id : '@id'}}
        });

        var scSchool = {};

        var schools;

        scSchool.calculateRank =  function(){
            return {};
        };
        var allSchoolsUrl = '/admin/ghana/schools';

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('schoolCache')) {
            var schoolCache = CacheFactory('schoolCache', {
                maxAge:  24 * 60 * 60 * 1000, // 1 day
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    if (key == allSchoolsUrl) {
                        scSchool.updateSchoolArray().then(function (status) {
                            if (status) scSchool.init()
                        })
                    }
                    //$http.get(key).success(function (data) {
                    //    schoolCache.put(key, data);
                    //});
                }
            });
        }

        var calculateCircuitStats = function(school, total_number_of_schools){
            scCircuit.circuitLookup[school.circuit_id].inclusivenessHolder += parseFloat(school.current_state_of_school_inclusiveness);
            scCircuit.circuitLookup[school.circuit_id].safe_protectiveHolder += parseFloat(school.current_state_of_safety_and_protection);
            scCircuit.circuitLookup[school.circuit_id].healthyHolder += parseFloat(school.current_state_of_healthy_level);
            scCircuit.circuitLookup[school.circuit_id].gender_friendlyHolder += parseFloat(school.current_state_of_friendliness_to_boys_girls);
            scCircuit.circuitLookup[school.circuit_id].effective_teaching_learningHolder += parseFloat(school.current_state_of_teaching_and_learning);
            scCircuit.circuitLookup[school.circuit_id].community_involvementHolder +=  parseFloat(school.current_state_of_community_involvement);

            scCircuit.circuitLookup[school.circuit_id].supervisorAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_circuit_supervisor);
            scCircuit.circuitLookup[school.circuit_id].headteacherAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_headteacher);

            scCircuit.circuitLookup[school.circuit_id].inclusiveness = scGeneralService.roundToTwoDP(Math.floor ( scCircuit.circuitLookup[school.circuit_id].inclusivenessHolder /   total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].safe_protective = scGeneralService.roundToTwoDP(Math.floor( scCircuit.circuitLookup[school.circuit_id].safe_protectiveHolder /   total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].healthy = scGeneralService.roundToTwoDP(Math.floor(scCircuit.circuitLookup[school.circuit_id].healthyHolder / total_number_of_schools ) );
            scCircuit.circuitLookup[school.circuit_id].gender_friendly = scGeneralService.roundToTwoDP( Math.floor( scCircuit.circuitLookup[school.circuit_id].gender_friendlyHolder / total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].effective_teaching_learning = scGeneralService.roundToTwoDP(Math.floor( scCircuit.circuitLookup[school.circuit_id].effective_teaching_learningHolder / total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].community_involvement =  scGeneralService.roundToTwoDP(Math.floor( scCircuit.circuitLookup[school.circuit_id].community_involvementHolder / total_number_of_schools ));

            scCircuit.circuitLookup[school.circuit_id].supervisorAttendanceAverage = scGeneralService.roundToTwoDP(Math.floor(scCircuit.circuitLookup[school.circuit_id].supervisorAttendanceHolder / total_number_of_schools));
            scCircuit.circuitLookup[school.circuit_id].headteacherAttendanceAverage =  scGeneralService.roundToTwoDP(Math.floor(scCircuit.circuitLookup[school.circuit_id].headteacherAttendanceHolder /   total_number_of_schools));

            var currentTotal =
                parseFloat(scCircuit.circuitLookup[school.circuit_id].inclusiveness)
                + parseFloat( scCircuit.circuitLookup[school.circuit_id].safe_protective)
                +  parseFloat(scCircuit.circuitLookup[school.circuit_id].healthy)
                +parseFloat( scCircuit.circuitLookup[school.circuit_id].gender_friendly)
                + parseFloat(scCircuit.circuitLookup[school.circuit_id].effective_teaching_learning)
                +parseFloat( scCircuit.circuitLookup[school.circuit_id].community_involvement);

            scCircuit.circuitLookup[school.circuit_id].rating = scGeneralService.roundToTwoDP(Math.floor( (currentTotal / 60) * 100 ));

        };

        var calculateDistrictStats = function(school, total_number_of_schools){
            scDistrict.districtLookup[school.district_id].inclusivenessHolder += parseFloat(school.current_state_of_school_inclusiveness);
            scDistrict.districtLookup[school.district_id].safe_protectiveHolder += parseFloat(school.current_state_of_safety_and_protection);
            scDistrict.districtLookup[school.district_id].healthyHolder += parseFloat(school.current_state_of_healthy_level);
            scDistrict.districtLookup[school.district_id].gender_friendlyHolder += parseFloat(school.current_state_of_friendliness_to_boys_girls);
            scDistrict.districtLookup[school.district_id].effective_teaching_learningHolder += parseFloat(school.current_state_of_teaching_and_learning);
            scDistrict.districtLookup[school.district_id].community_involvementHolder +=  parseFloat(school.current_state_of_community_involvement);

            scDistrict.districtLookup[school.district_id].supervisorAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_circuit_supervisor);
            scDistrict.districtLookup[school.district_id].headteacherAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_headteacher);


            scDistrict.districtLookup[school.district_id].inclusiveness = scGeneralService.roundToTwoDP(Math.floor (scDistrict.districtLookup[school.district_id].inclusivenessHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].safe_protective = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].safe_protectiveHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].healthy = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].healthyHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].gender_friendly = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].gender_friendlyHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].effective_teaching_learning = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].effective_teaching_learningHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].community_involvement =  scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].community_involvementHolder /   total_number_of_schools));

            scDistrict.districtLookup[school.district_id].supervisorAttendanceAverage = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].supervisorAttendanceHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].headteacherAttendanceAverage =  scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].headteacherAttendanceHolder /   total_number_of_schools));


            var currentTotal =
                parseFloat(scDistrict.districtLookup[school.district_id].inclusiveness)
                + parseFloat(scDistrict.districtLookup[school.district_id].safe_protective)
                + parseFloat(scDistrict.districtLookup[school.district_id].healthy)
                +parseFloat(scDistrict.districtLookup[school.district_id].gender_friendly)
                +parseFloat(scDistrict.districtLookup[school.district_id].effective_teaching_learning)
                +parseFloat(scDistrict.districtLookup[school.district_id].community_involvement) ;

            scDistrict.districtLookup[school.district_id].rating = scGeneralService.roundToTwoDP(Math.floor(  ( currentTotal / 60 ) * 100));
        };

        var calculateRegionStats = function(school, total_number_of_schools){
            scRegion.regionLookup[school.region_id].inclusivenessHolder += parseFloat(school.current_state_of_school_inclusiveness);
            scRegion.regionLookup[school.region_id].safe_protectiveHolder += parseFloat(school.current_state_of_safety_and_protection);
            scRegion.regionLookup[school.region_id].healthyHolder += parseFloat(school.current_state_of_healthy_level);
            scRegion.regionLookup[school.region_id].gender_friendlyHolder += parseFloat(school.current_state_of_friendliness_to_boys_girls);
            scRegion.regionLookup[school.region_id].effective_teaching_learningHolder += parseFloat(school.current_state_of_teaching_and_learning);
            scRegion.regionLookup[school.region_id].community_involvementHolder +=  parseFloat(school.current_state_of_community_involvement);

            scRegion.regionLookup[school.region_id].supervisorAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_circuit_supervisor);
            scRegion.regionLookup[school.region_id].headteacherAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_headteacher);

            scRegion.regionLookup[school.region_id].inclusiveness = scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].inclusivenessHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].safe_protective =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].safe_protectiveHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].healthy =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].healthyHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].gender_friendly =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].gender_friendlyHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].effective_teaching_learning =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].effective_teaching_learningHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].community_involvement =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].community_involvementHolder /   total_number_of_schools));


            scRegion.regionLookup[school.region_id].supervisorAttendanceAverage = scGeneralService.roundToTwoDP(Math.floor(scRegion.regionLookup[school.region_id].supervisorAttendanceHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].headteacherAttendanceAverage =  scGeneralService.roundToTwoDP(Math.floor(scRegion.regionLookup[school.region_id].headteacherAttendanceHolder /   total_number_of_schools));


            var currentTotal =
                parseFloat(scRegion.regionLookup[school.region_id].inclusiveness)
                + parseFloat(scRegion.regionLookup[school.region_id].safe_protective)
                + parseFloat(scRegion.regionLookup[school.region_id].healthy)
                +parseFloat(scRegion.regionLookup[school.region_id].gender_friendly)
                +parseFloat(scRegion.regionLookup[school.region_id].effective_teaching_learning)
                +parseFloat(scRegion.regionLookup[school.region_id].community_involvement) ;

            scRegion.regionLookup[school.region_id].rating = scGeneralService.roundToTwoDP(Math.floor( (currentTotal / 60) * 100  ));

            scRegion.reviewDataExists = true;
        };

        var calculateSchoolStats = function(school){
            var overall =
                parseFloat(school.current_state_of_school_inclusiveness) +
                parseFloat(school.current_state_of_teaching_and_learning) +
                parseFloat(school.current_state_of_healthy_level) +
                parseFloat(school.current_state_of_friendliness_to_boys_girls) +
                parseFloat(school.current_state_of_safety_and_protection) +
                parseFloat(school.current_state_of_community_involvement);

            scSchool.schoolLookup[school.id].rating = scGeneralService.roundToTwoDP(Math.floor (( overall / 60) * 100)) ;


            scSchool.schoolLookup[school.id].current_teacher_attendance_by_headteacher =
                scGeneralService.roundToTwoDP(scSchool.schoolLookup[school.id].current_teacher_attendance_by_headteacher) ;

            scSchool.schoolLookup[school.id].current_teacher_attendance_by_circuit_supervisor =
                scGeneralService.roundToTwoDP(scSchool.schoolLookup[school.id].current_teacher_attendance_by_circuit_supervisor) ;


        };

        scSchool.schools = [];

        var loadingSchools = false;

        scSchool.getAllSchools = function(){
            if (loadingSchools) {
                return;
            }
            loadingSchools = true;
            var defer = $q.defer();

            var cachedData = schoolCache.get(allSchoolsUrl);

            if (!cachedData) {
                scNotifier.notifyInfo('Info', 'Updating schools...');
                schoolResource.query().$promise.then(function(data){
                    if (data) {
                        scSchool.schools = data;
                        schoolCache.put(allSchoolsUrl, data);
                        loadingSchools = false;
                        defer.resolve(true);
                    }
                }, function(){
                    loadingSchools = false;
                    defer.resolve(false)
                });
            }else{
                scSchool.schools = cachedData;
                loadingSchools = false;
                defer.resolve(true);
            }
            return defer.promise;
        };

        scSchool.updateSchoolArray = function(){
            var defer = $q.defer();
            scSchool.schools = [];
            schoolCache.remove(allSchoolsUrl);
            schoolCache.removeAll();

            this.getAllSchools().then(
                function(status){
                    if (status) {
                        $timeout(function () {
                            $rootScope.loading = false;
                            $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                            defer.resolve(true);
                        }, 100);
                    }else{
                        defer.resolve(false);
                    }
                }, function(){
                    $rootScope.loading = false;
                    defer.resolve(false);
                });
            return defer.promise;
        };

        $rootScope.reloadSchools = function () {
            $rootScope.loading = true;
            scSchool.updateSchoolArray();
        };

        scSchool.init = function(){
            scSchool.schoolLookup = {};
            this.schools.sort(function(a, b){
                return a.name.toLowerCase() - b.name.toLowerCase()
            });
            angular.forEach(this.schools, function(school, index){

                //Initiate a counter for how many teachers are in a school. It will be used in the district teacher count view
                school.totalTeachers = 0;
                school.totalMaleTeachers = 0;
                school.totalFemaleTeachers = 0;
                school.teachersInSchoolHolder = [];

                //initiate a temp parameter for the school to know its average_teacher_attendance for certain views
                school.average_teacher_attendance = {};

                //Initiate an array for the teacher attendance average for the district view
                school.teacherAttendanceAverages = [];

                //Initiate an array for the school weekly attendance for the district view
                school.weeklyAttendanceHolder = [];

                //Initiate an array for the school termly information for the district view
                school.termlyDataHolder = [];

                scSchool.schoolLookup[school.id] = school;

                //Assign region name to school object
                scSchool.schools[index].regionName = scRegion.regionLookup[school.region_id].name;

                //Increment the school's region by 1
                scRegion.regionLookup[school.region_id].totalSchools ++;

                //add school to the region lookup object and associate it by its id
                scRegion.regionLookup[school.region_id].allSchools[school.id] = school;

                //add school to the region lookup object
                scRegion.regionLookup[school.region_id].allSchoolsHolder.push(school);

                //Assign district name to school
                scSchool.schools[index].districtName = scDistrict.districtLookup[school.district_id].name;

                //Increment the school's region by 1
                scDistrict.districtLookup[school.district_id].totalSchools ++;

                //add school to the district lookup object and associate it by its id
                scDistrict.districtLookup[school.district_id].allSchools[school.id] = school;

                //Use the school_id in the scDistrict so that we can lookup a district using only the school_Id
                scDistrict.searchSchoolDistrict[school.id] = scDistrict.districtLookup[school.district_id];

                //add school to the district lookup object
                scDistrict.districtLookup[school.district_id].allSchoolsHolder.push(school);

                //Assign the school's circuit name to the school object
                scSchool.schools[index].circuitName = scCircuit.circuitLookup[school.circuit_id].name;

                //Increment the circuits schools by 1
                scCircuit.circuitLookup[school.circuit_id].totalSchools ++ ;

                //add school to the circuit lookup object and associate it by its id
                scCircuit.circuitLookup[school.circuit_id].allSchools[school.id] = school;

                //add school to the circuit lookup object
                scCircuit.circuitLookup[school.circuit_id].allSchoolsHolder.push(school);

                //Lookup data collector and assign school
                if (DataCollectorHolder.lookupDataCollector) {
                    if (DataCollectorHolder.lookupDataCollector[school.headteacher_id]){
                        DataCollectorHolder.lookupDataCollector[school.headteacher_id].school_name = school.name;
                        DataCollectorHolder.lookupDataCollector[school.headteacher_id].school_id = school.id;
                    }
                }



                //Increment the circuits review sections
                calculateSchoolStats(school);

                calculateCircuitStats(school,  scCircuit.circuitLookup[school.circuit_id].totalSchools);

                calculateDistrictStats(school, scDistrict.districtLookup[school.district_id].totalSchools);

                calculateRegionStats(school, scRegion.regionLookup[school.region_id].totalSchools);

            })
        };
//        inclusiveness
//        safe_protective
//        healthy
//        gender_friendly
//        effective_teaching_learning
//        community_involvement

        //The overall rating, add up all the ratings of each category
//       var overall =  school.current_state_of_school_inclusiveness +
//        school.current_state_of_teaching_and_learning +
//        school.current_state_of_healthy_level +
//        school.current_state_of_friendliness_to_boys_girls +
//        current_state_of_safety_and_protection +
//        current_state_of_community_involvement;

//        Now that we have the overall, we can assign it to the school,



// to/*
//          To calculate the the circuits rating, loop through the circuits' schools and
//        for each section, add them up and divide them by the expected total number
//        and multiply by 100 for the circuits rating
//
// */
        scSchool.addSchool= function(formData){
            var defer = $q.defer();
            // process the form
            schoolResource.create(formData,
                //success callback
                function (value) {
                    var status = '';
                    angular.forEach(value, function(key, prop){
                        if (prop != '$promise' && prop != '$resolved' ) status += value[prop]
                    });
                    if ($.trim(status) == 'success') {
                        scSchool.updateSchoolArray().then(function (value) {
                            if (value) {
                                defer.resolve(true);
                            }
                        });
                    }else {
                        scSchool.validationFields = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };

        scSchool.createTeacher= function(formData){
            var defer = $q.defer();
            // process the form
            schoolResource.newTeacher(formData,
                //success callback
                function (value) {
                    var status = '';
                    angular.forEach(value, function(key, prop){
                        if (prop != '$promise' && prop != '$resolved' ) status += value[prop]
                    });
                    if ($.trim(status) == 'success') {
                        defer.resolve(true);
                    }else {
                        scSchool.validationFields = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };



        scSchool.deleteSchool = function(school_id){

            var defer = $q.defer();
            if (scSchool.schoolLookup[school_id]) {
                schoolResource.deleteSchool({id : school_id},
                    //success callback
                    function(value){
                        var  data = '';
                        angular.forEach(value, function(key, prop){
                            if (prop != '$promise' && prop != '$resolved' ) data += value[prop]
                        });
                        if ($.trim(data) == 'success') {

                            scSchool.updateSchoolArray().then(
                                function(status){
                                    if (status) {
                                        defer.resolve(true);
                                    }else{
                                        defer.resolve(false);
                                    }
                                },
                                function(){
                                    defer.resolve(false);
                                }
                            );
                        }else{
                            defer.resolve(false);
                        }
                    },
//                error callback
                    function(httpResponse){
                        defer.resolve(httpResponse)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };



//    Retrieve a single district to view or edit
        scSchool.retrieveOneSchool = function(school_id){
            var defer = $q.defer();
            schoolResource.retrieveOneSchool({id: school_id},
                //After retrieving the region to edit, return it to the controller

                function (data) {
                    defer.resolve(true);
                    scSchool.schoolRetrieved = data
                },

                //Error function
                function (httpResponse) {
                    defer.resolve(httpResponse);
                    return false
                });
            return defer.promise;
        };



        scSchool.editSchool = function(formData){
            var defer = $q.defer();
            schoolResource.editSchool(formData,
                //success callback
                function (value, responseHeaders) {
                    var  data = '';
                    angular.forEach(value, function(key, prop){
                        if (prop != '$promise' && prop != '$resolved' ) data += value[prop]
                    });
                    if ($.trim(data) == 'success') {
                        //If successful, requery the new regions and assign them to the parent regions variable
                        scSchool.updateSchoolArray().then(function (value) {
                            if (value) {
                                defer.resolve(true);
                            }
                        });
                    } else {
                        scSchool.validationFields = value;
                        defer.resolve(false)
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };

        scSchool.getImages = function(school_id){
            var defer = $q.defer();

            var aSchoolsImagesUrl = '/admin/schools/images?country_id=1&school_id='+school_id;

            var cachedSchoolImageData = /*schoolCache.get(aSchoolsImagesUrl);*/'';

            if (!cachedSchoolImageData) {
                $http.get(aSchoolsImagesUrl)
                    .success(function(data){
                        schoolCache.put(aSchoolsImagesUrl, data);
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.resolve(false);
                    });
            }else{
                defer.resolve(cachedSchoolImageData)
            }
            return defer.promise;
        };

        scSchool.getRequiredImages = function(school_id){
            var defer = $q.defer();

            var aSchoolsImagesUrl = '/admin/school_report_card/school/required/images?country_id=1&school_id='+school_id;

            var cachedSchoolImageData =/* schoolCache.get(aSchoolsImagesUrl);*/'';

            if (!cachedSchoolImageData) {
                $http.get(aSchoolsImagesUrl)
                    .success(function(data){
                        schoolCache.put(aSchoolsImagesUrl, data);
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.resolve(false);
                    });
            }else{
                defer.resolve(cachedSchoolImageData)
            }
            return defer.promise;
        };

        scSchool.fetchItinerary = function(school_district_id){
            var defer = $q.defer();

            var aDistrictsItineraryUrl = '/admin/itinerary?country_id=1&district_id='+school_district_id;

            var cachedDistrictItineraryData = schoolCache.get(aDistrictsItineraryUrl);

            if (!cachedDistrictItineraryData) {
                $http.get(aDistrictsItineraryUrl)
                    .success(function(data){
                        schoolCache.put(aDistrictsItineraryUrl, data);
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.resolve(false);
                    });
            }else{
                defer.resolve(cachedDistrictItineraryData)
            }
            return defer.promise;

        };

        scSchool.fetchWeeklyData = function(school_id){

            var defer = $q.defer();

            var aSchoolsWeeklyDataUrl = '/admin/totals/weekly/data?country_id=1&school_id='+school_id;

            var cachedSchoolWeeklyData = schoolCache.get(aSchoolsWeeklyDataUrl);

            if (!cachedSchoolWeeklyData) {
                $http.get(aSchoolsWeeklyDataUrl)
                    .success(function(data){
                        schoolCache.put(aSchoolsWeeklyDataUrl, data);
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.resolve(false);
                    });
            }else{
                defer.resolve(cachedSchoolWeeklyData)
            }
            return defer.promise;

        };


        scSchool.deleteSubmittedData = function(type, id_of_selected){
            var defer = $q.defer();

            var data_to_delete = {
                id : id_of_selected,
                data_type : type
            };
            $http.post('/admin/delete/enrolment/attendance', data_to_delete)
                .success(function (returnedData) {
                    if (returnedData) {
                        defer.resolve(true)
                    }else{
                        defer.resolve(false)
                    }
                })
                .error(function () {
                    defer.resolve(false)
                });


            return defer.promise;
        };


        scSchool.teacherAttendanceClassSubmittedData = function(type, id_of_selected){
            var defer = $q.defer();

            var data_to_delete = {
                id : id_of_selected,
                data_type : type
            };
            $http.post('/admin/delete/teacher/data/submitted', data_to_delete)
                .success(function (returnedData) {
                    if (returnedData) {
                        defer.resolve(true)
                    }else{
                        defer.resolve(false)
                    }
                })
                .error(function () {
                    defer.resolve(false)
                });


            return defer.promise;
        };

        scSchool.deleteASectionOfSubmittedData = function(type, dataparams){
            var defer = $q.defer();


            var url = {
                normal_enrolment : 'delete/enrolment/attendance',
                special_enrolment : 'delete/enrolment/attendance',
                normal_attendance : 'delete/enrolment/attendance',
                special_attendance : 'delete/enrolment/attendance',

                teacher_attendance : 'delete/teacher/data/submitted',
                class_management : 'delete/teacher/data/submitted',

                textbooks : 'delete/textbooks/data',
                pupil_performance : 'delete/pupils/performance/data',

                record_books : 'delete/record/books',
                support : 'delete/support/type/data',
                grant : 'delete/grant/data',


                sanitation : 'delete/sanitation/data',
                security : 'delete/security/data',
                sports_recreation : 'delete/sports/recreational/data',
                structure : 'delete/school/structure/data',
                furniture : 'delete/furniture/data',


                community_relation : 'delete/community/relation/data',
                meeting : 'delete/meetings/held/data',
                general_issues : 'delete/general/issues/data'
            };

            if (url[type] === 'delete/enrolment/attendance' || url[type] === 'delete/teacher/data/submitted') {
                dataparams.data_type = type;
                delete dataparams.id;

            }else if(url[type] === 'delete/textbooks/data' || url[type] === 'delete/pupils/performance/data' || url[type] === 'delete/general/issues/data'){
                delete dataparams.week_number;
                delete dataparams.id;

            }else{
                delete dataparams.week_number;
            }


            $http.post('admin/' + url[type], dataparams)
                .success(function (returnedData) {

                    console.log("on success : ", type, url[type], returnedData, dataparams);
                    if (returnedData) {
                        defer.resolve(true)
                    }else{
                        defer.resolve(false)
                    }
                })
                .error(function () {
                    defer.resolve(false)
                });


            return defer.promise;
        };

        scSchool.fetchAvailableWeeklySubmittedData = function(school_id, category, data_submission_interval){
            var defer = $q.defer();

            var aSchoolsWeeklyDataUrl = '/admin/retrieve/weekly/submissions/under/school?school_id='+school_id+'&category='+category+'&data_submission_interval='+data_submission_interval;

            // var cachedSchoolWeeklyData = schoolCache.get(aSchoolsWeeklyDataUrl);

            /*do not use cache for this so it always reloads, perhaps the cs submitted a comment*/
            $http.get(aSchoolsWeeklyDataUrl)
                .success(function (data) {
                    // schoolCache.put(aSchoolsWeeklyDataUrl, data);
                    defer.resolve(data);
                })
                .error(function () {
                    defer.resolve(false);
                });/*else{
                defer.resolve(cachedSchoolWeeklyData)
            }*/
            return defer.promise;

        };

        scSchool.clearSchoolCache = function () {
            schoolCache.removeAll();
        };

        scSchool.allSchoolTeachersData = function (params) {
            return $http.get('/admin/school_report_card/school/teachers', {params : params});
        };

        scSchool.transferTeacher= function(formData){
            var defer = $q.defer();
            // process the form
            schoolResource.transferTeacher(formData,
                //success callback
                function (value) {
                    if ($.trim(value.status) == '200') {
                        defer.resolve(true);
                    }else {
                        scSchool.validationFields = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };


        scSchool.editTeacher = function (params) {
            return $http.post('/admin/school_report_card/teacher/edit', params);
        };

        scSchool.deleteTeacher = function (params) {
            return $http.post('/admin/school_report_card/teacher/delete', params);
        };

        scSchool.updateWeeklyTableForEnrollment = function (params) {
            return $http.post('/admin/update/weekly/table/enrollment', params);
        };

        scSchool.updateWeeklyTableForAttendance = function (params) {
            return $http.post('/admin/update/weekly/table/attendance', params);
        };

        scSchool.updateWeeklyTableForTeacherAttendance = function (params) {
            return $http.post('/admin/update/weekly/table/for/teachers/attendance', params);
        };

        scSchool.updateWeeklyTableAtBackend = function (params) {
            return $http.post('/admin/update/weekly/totals', params);
        };

        scSchool.updateTermlyTextbookData = function (params) {
            return $http.post('/admin/school_report_card/school/class_textbooks/update/totals', params);
        };

        scSchool.updateTermlyPupilPerformanceData = function (params) {
            return $http.post('/admin/school_report_card/school/pupil_performance/update/totals', params);
        };

        return scSchool;


    }]);

