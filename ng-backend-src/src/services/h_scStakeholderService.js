/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.service('scStakeholderService',[])
    .factory('DataCollector', ['$resource', function($resource){
//        return $resource('/ghana/data_collectors');
        return $resource('/admin/ghana/data_collectors/headteacher',{},{
            create:{method:'POST', url:'/admin/data_collectors', params:{}},
            retrieveOneDataCollector:{method:'GET', url:'/admin/data_collector/:id/edit', params:{id : '@id'}},
            resendLoginCredentials:{method:'POST',
                url:'/admin/resend/data_collectors/login/details/:firstName/:identityCode/:number',
                params:{id : '@id', firstName : '@firstName', identityCode : '@identityCode', number : '@number'}},
            retrieveMessagesSentByDataCollectors:{method:'GET', url:'/admin/retrieveMessagesSentByDataCollectors', params:{id : '@id'}, isArray: true},
            retrieveDataCollectorSMSMessages :{method:'GET', url:'/admin/retrieve/sms/messages',
                params : {id : '@id'}
            }

        });
    }])
    .factory('DataCollectorHolder', ['$resource', function($resource){
        var datacollectors = {};

        datacollectors.lookupDataCollector = {};
        datacollectors.dataCollectors = [];

        return datacollectors;
    }]);