/**
 * Created by Kaygee on 21/05/2015.
 */

schoolMonitor.filter('checkPercentage', ['$rootScope', function($rootScope){

    return function (number) {
        if (number > 100){
            number = number+'';
            var array = number.split('');
            return array[0] + array[1];
        }
        return number;
    };

}]);