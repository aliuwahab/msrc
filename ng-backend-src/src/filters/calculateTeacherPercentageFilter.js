/**
 * Created by Kaygee on 21/05/2015.
 */
/*this uses GSM Notification*/
schoolMonitor.filter('teacherAveragePercentage', ['$rootScope', function($rootScope){
    var array;
    return function (number) {
        console.log('teacherAveragePercentage:', number);

        if (!number || !angular.isNumber(number)) {
            return '-'
        }
        if (number <= 100 ) {
            return Math.round(number) + '%'
        }
        if (number > 100){
            number = number+'';
            array = number.split('');
            return array[0] + array[1] +'%';
        }
        var result = (Math.round(number * 100));
        if (result > 100) {
            result = result+'';
            array = result.split('');
            return array[0] + array[1] +'%';
        }else{
            return Math.round(result) + '%'
        }
    };

}]);