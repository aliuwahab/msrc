/**
 * Created by Kaygee on 21/05/2015.
 */

schoolMonitor.filter('replaceWithDash', [function(){

    return function (theString) {
        if (angular.isDefined(theString) || theString === 0){
            return theString
        }
        return "-";
    };

}]);