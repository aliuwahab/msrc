/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('showOnMap', ['$modal', function($modal){
    return {
        scope : {
            longitude : "@",
            latitude : "@"
        },
        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/showPointsOnMapModal.html',
                    controller: ['$sce', '$scope','$modalInstance', '$timeout', showGoogleMapCtrl],
                    size: 'md'
                });
            });

            function showGoogleMapCtrl ($sce, $scope, $modalInstance, $timeout) {


                $scope.longitude = scope.longitude;
                $scope.latitude = scope.latitude;

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };



                $scope.mapUrl = $sce.trustAsResourceUrl("https://www.google.com/maps/embed/v1/place?" +
                    "key=AIzaSyCi1u5OlpN5pu6ZR7SO9324dN8sklC4Okg&q="+scope.latitude+","+scope.longitude);


                    var mapIframe = $('#mapIFrame');

                mapIframe.ready(function(){
                    $timeout(function () {
                        $scope.mapLoaded = true;
                    }, 4000);
                });
            }
        }

    }

}]);