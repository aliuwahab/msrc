/**
 * Created by Kaygee on 24/07/2014.
 */

/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('badgeColor', function(){
//    var linker = function($scope, element, attr){
    return {
        scope : {
            value : '@'
        },
        controller : function($scope){
           $scope.badges = function (status) {
               return status < 1 ? 'bg-red' :
                   status < 2? 'bg-orange' :
                       status < 3 ? 'bg-teal' :
                           status < 4 ? 'bg-aqua' :
                               status < 5 ? 'bg-blue' : 'bg-green'
           }

        },
        replace : true,
        restrict : 'AE',
        template : ' <span class="badge  " ng-class="badges(value)">{{value}}</span>'
    }
});