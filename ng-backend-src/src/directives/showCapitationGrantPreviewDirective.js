/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('capitationGrantPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=capitationGrantPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewCapitationGrant.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', '$state', 'format_school_term', 'formatSchoolGrantType',  ShowCapitationGrantPreviewCtrl],
                    size: 'lg'
                });
            });

            function ShowCapitationGrantPreviewCtrl ($scope, $modalInstance, scNotifier, $state, format_school_term, formatSchoolGrantType) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;

                $scope.term = format_school_term[ item.term ].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;

                $scope.formatSchoolGrantType = formatSchoolGrantType;

                $scope.recordRow = [item];

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);