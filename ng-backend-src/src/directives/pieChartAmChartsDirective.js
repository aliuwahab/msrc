/**
 * Created by Kaygee on 24/07/2014.
 */

/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('amPieChart', ['$interval', function($interval){
    var chart, legend, chartScrollBar, categoryAxis, valueAxis, graph, chartCursor;

    return {
        scope : {
            chartData : '=amPieChart',
            chartTitle : '@chartTitle'
        },
        controller : function($scope){
            /*Start of amPieCharts*/
            $scope.changePieChart = function(chartData, chartTitle, chartDivID){

            };

            /*End of amPieCharts*/
        },
        link : function ($scope, element, attr) {

            $scope.chartData = [
                {"field" : "Boys",
                    "value" : 54
                },
                {"field" : "Girls",
                    "value" : 82
                },
                {"field" : "Both",
                    "value" : 62
                }
            ];


            // PIE CHART
            chart = new AmCharts.AmPieChart();
            chart.dataProvider = $scope.chartData;
            chart.titleField = "field";
            chart.valueField = "value";

            //COLORS
//			chart.colors = ["#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01", "#B0DE09", "#04D215", "#0D8ECF",
// "#0D52D1", "#2A0CD0", "#8A0CCF", "#CD0D74", "#754DEB", "#DDDDDD", "#999999", "#333333", "#000000",
// "#57032A", "#CA9726", "#990000", "#4B0C25"]
            chart.colors=["#0D8ECF","#FF6600","#8A0CCF","#04D215","#CD0D74","#754DEB","#CA9726","#F8FF01"];

            //3D SETTING
            chart.depth3D = 10; //0 for flat
            chart.angle = 10; //0 when it is flat

            // LEGEND
            legend = new AmCharts.AmLegend();
            legend.align = "center";
            legend.markerType = "circle";
            chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
            chart.addLegend(legend);

            //TITLE
            chart.addTitle($scope.chartTitle);

            //LABEL
            chart.labelRadius = 30;
            chart.labelText = "[[title]]: [[value]]";
//                chart.labelRadius = -30; // label Inside
//                chart.labelText = "[[percents]]%"; // display labels as percentage


            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // WRITE
            $interval(chart.write('amPieChartDiv'), 2000, 2);

        },
        replace : true,
        restrict : 'AE',
        template : '<div id="amPieChartDiv" style="width: 100%; height: 400px;"></div>'
    }
}]);