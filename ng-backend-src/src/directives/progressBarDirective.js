/**
 * Created by Kaygee on 12/08/2014.
 */

//This directive is used in the recent activities table. It is used to determine the color of the
//activity posted

schoolMonitor.directive('progressBar', function(){
//    var linker = function($scope, element, attr){
    return {
        scope : {
            value : '@'
        },
        controller : function($scope){

        },
        replace : true,
        restrict : 'AE',
        template : '<div class="progress-bar progress-bar-red" ng-style="{width: value /10 * 100}"></div>'
//            ' <span class="label " ng-class=" labels[type] ">Submitted under {{ display[type] }}</span>'

    }
});