/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('sparkLine', function(){
    var linker = function($scope, element, attr){
        var valueArray =attr.valueArray.split(",");
        element.sparkline(valueArray, {type : attr.sparkType});
    };
    return {
        restrict : 'AE',
        link : linker
    }
});