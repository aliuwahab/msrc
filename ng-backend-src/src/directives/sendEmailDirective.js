/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('sendEmail', ['$modal', function($modal){
    return function($scope, elem, attrs){

          elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/sendEmailModal.html',
                    controller: ['$rootScope', '$scope','$modalInstance', 'Email', sendEmailCtrl],
                    size: 'md'
                });
            });

            function sendEmailCtrl ($rootScope, $scope, $modalInstance, Email) {

                $scope.emailFormData = {};


                $scope.remove = function(file){
                    $rootScope.loading = true;
                    Email.removeFileUploaded({file_path : $scope.emailFormData.file_path},
                        //Success function
                        function(status){
                            file.cancel();
                            $scope.emailFormData.file_path = '';
                            $rootScope.loading = false;
                        },

                        //error from server
                        function(){
                            file.cancel();
                            $scope.emailFormData.file_path = '';

                            $rootScope.loading = false;
                        });
                };

                $scope.send = function() {
                    $rootScope.loading = true;
                    Email.sendAnEmail($scope.emailFormData,
                        //Success function
                        function(status){},

                        //error from server
                        function(){

                        });

                    $modalInstance.dismiss('cancel');

                    $rootScope.loading = false;

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
    }
}]);