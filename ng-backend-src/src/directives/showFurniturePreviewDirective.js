/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('furniturePreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=furniturePreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewFurniture.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatAdequecy', 'format_school_term',  ShowStructurePreviewCtrl],
                    size: 'md'
                });
            });

            function ShowStructurePreviewCtrl ($scope, $modalInstance, scNotifier, formatAdequecy, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;

                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "Pupils Furniture",
                    term :   item.term,
                    status : formatAdequecy[item.pupils_furniture],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Teacher Tables",
                    term :   item.term,
                    status : formatAdequecy[item.teacher_tables],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Teacher Chairs",
                    term :   item.term,
                    status : formatAdequecy[item.teacher_chairs],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Classrooms Cupboard",
                    term :   item.term,
                    status : formatAdequecy[item.classrooms_cupboard],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });



                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);