/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('securityPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=securityPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewSecurity.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatAvailability', 'format_school_term',  ShowSecurityPreviewCtrl],
                    size: 'md'
                });
            });

            function ShowSecurityPreviewCtrl ($scope, $modalInstance, scNotifier, formatAvailability, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;

                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "Walled / Fenced",
                    term :   item.term,
                    status : formatAvailability[item.walled],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });

                $scope.recordRow.push({
                    name: "Gated",
                    term :   item.term,
                    status : formatAvailability[item.gated],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Lights",
                    term :   item.term,
                    status : formatAvailability[item.lights],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Security Man",
                    term :   item.term,
                    status : formatAvailability[item.security_man],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);