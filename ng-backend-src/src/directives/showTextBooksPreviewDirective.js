/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('textbooksPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=textbooksPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewTextbooks.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'School', 'format_school_term',  ShowTextbooksPreviewCtrl],
                    size: 'md'
                });
            });

            function ShowTextbooksPreviewCtrl ($scope, $modalInstance, scNotifier, School, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                $scope.recordRow = [];

                $scope.loadingData = true;

                var dataToLoad = {
                    school_id : $scope.school_id,
                    year : scope.tappedRow.year,
                    term : scope.tappedRow.term
                };

                School.allSchoolTextbook(dataToLoad,
                    /*success function*/
                    function (allSchoolTextbookData) {

                        angular.forEach(allSchoolTextbookData[0].school_textbooks, function(item , index){
                            if (index == 0) {/*We just take the first item because its the same throughout*/
                                $scope.cs_comments = item.cs_comments;
                                $scope.submission_id = item.id;/*Comments are his only in the first item*/
                                $scope.year = item.year;
                                $scope.term = format_school_term[item.term].label;
                            }
                            var textbooksTempObj = {
                                class : item.level,
                                gh_lang : item.number_of_ghanaian_language_textbooks,
                                english : item.number_of_english_textbooks,
                                maths : item.number_of_maths_textbooks,
                                data_collector_type : item.data_collector_type
                            };

                            $scope.recordRow.push(textbooksTempObj);


                        });
                        $scope.loadingData = false;

                    });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);