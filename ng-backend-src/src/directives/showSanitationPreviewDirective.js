/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('sanitationPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=sanitationPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewSanitation.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatAvailability', 'format_school_term',  ShowSanitationPreviewCtrl],
                    size: 'md'
                });
            });

            function ShowSanitationPreviewCtrl ($scope, $modalInstance, scNotifier, formatAvailability, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;

                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "Toilet",
                    term :   item.term,
                    status : formatAvailability[item.toilet],
                    year :item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Urinal",
                    term :   item.term,
                    status : formatAvailability[item.urinal],
                    year :item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Water",
                    term :   item.term,
                    status : formatAvailability[item.water],
                    year :item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Dust Bins",
                    term :   item.term,
                    status : formatAvailability[item.dust_bins],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Veronica Buckets",
                    term :   item.term,
                    status : formatAvailability[ item.veronica_buckets],
                    year :  item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);