/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('deleteData', ['$rootScope', '$http', 'scSchool',
    function($rootScope, $http, scSchool){

    return {

        scope : true,

        link : function(scope, elem, attr){
            elem.bind('click', function () {
                scope.deletingData = true;
                $rootScope.$broadcast('deletingData');
                scSchool.teacherAttendanceClassSubmittedData(attr.type, attr.idOfType)
                    .then(function(status){
                        if (status) {
                            $rootScope.$broadcast('teacherAttendanceClassSubmittedDataDeleted',
                                {
                                    data_type : attr.type,
                                    deleted_id : attr.idOfType,
                                    teacher_index :  attr.teacherIndex
                                })
                        }
                    })
            })
        }
    }

}]);