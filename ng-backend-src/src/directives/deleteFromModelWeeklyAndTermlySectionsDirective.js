/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('deleteSectionData', ['$rootScope', '$modal', 'scSchool', '$timeout',
    function($rootScope, $modal, scSchool, $timeout){

        return {

            scope : {
                week : '@',
                term : '@',
                year : '@',
                schoolId : '@',
                deleteSectionData : '@',
                sectionType : '@',
                submissionId : '@',
                onComplete :'&function'
            },

            controller : function ($scope) {
                //This helps to quickly associate the term from the server to a term object above
                $scope.one_school_terms = {
                    first_term : {number : '1st', label : '1st Term', value : 'first_term'},
                    second_term: {number : '2nd', label : '2nd Term', value : 'second_term'},
                    third_term : {number : '3rd', label : '3rd Term', value : 'third_term'}
                };
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/deleteSectionModal.html',
                        controller: deleteSectionModalInstanceCtrl,
                        size: "md"
                        // scope : scope
                    });
                });

                // Please note that $modalInstance represents a modal window (instance) dependency.
                // It is not the same as the $modal service used above.
                var deleteSectionModalInstanceCtrl = function ($scope, $modalInstance) {

                    $scope.week = scope.week;
                    $scope.term = scope.term;
                    $scope.year = scope.year;
                    $scope.schoolId = scope.schoolId;
                    $scope.deleteSectionData = scope.deleteSectionData;
                    $scope.sectionType = scope.sectionType;

                    $scope.deleteObject = scSchool.schoolLookup[scope.schoolId];
                    $scope.objectType = 'School';

                    $scope.delete = function () {
                        $scope.deletingData = true;

                        scSchool.deleteASectionOfSubmittedData(scope.sectionType, {
                            school_id : scope.schoolId,
                            term : scope.term,
                            year : scope.year,
                            week_number : scope.week,
                            id : scope.submissionId
                        }).then(function(status){
                            if (status) {
                                $timeout(function () {
                                    $scope.cancel();
                                    scope.onComplete();
                                });
                            }
                        })
                    };

                    $scope.cancel = function () {
                        $modalInstance.close();
                    };

                };

            }
        }

    }]);