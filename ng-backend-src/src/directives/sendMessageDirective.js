/**
 * Created by Kaygee on 21/05/2015.
 */
/*this uses GSM Notification*/
schoolMonitor.directive('sendMessage', ['$rootScope', '$modal', 'scNotifier','DataCollectorHolder',
    function($rootScope, $modal, scNotifier, DataCollectorHolder){

        return {

            scope : {
                size : "@",/*sm, lg*/
                model : "@",/*circuit, district, region, national*/
                quantity : "@",/*multiple , (id = 5)*/
                selected_model : '=selectedModel', /*The object of the selected model*/
                dataCollectors_to_send_messages_to : '=multipleDataCollectors' /*Data collectors to send multiple messages to*/
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/sendNotificationModal.html',
                        controller: ['$scope', '$modalInstance', 'SMSService', 'selected_data_collectors', sendMessageModalCtrl],
                        size : scope.size,
                        resolve : {

                            selected_data_collectors: function(){
                                if(scope.quantity == 'multiple'){
                                    return scope.dataCollectors_to_send_messages_to;
                                }
                                else if (scope.quantity == 'all'){
                                    return [];
                                }
                                else if (typeof (parseInt(scope.quantity)) == 'number' ) {
                                    //Lookup the object of the datacollector by id
                                    //and put it in an array so that its the same for the multiple one too
                                    return [
                                        // DataCollectorHolder.lookupDataCollector[scope.quantity]
                                        {
                                            id : scope.quantity,
                                            name :  DataCollectorHolder.lookupDataCollector[scope.quantity].name,
                                            phone_number :  DataCollectorHolder.lookupDataCollector[scope.quantity].phone_number
                                        }
                                    ];
                                }
                            }
                        }
                    });


                    function sendMessageModalCtrl ($scope, $modalInstance, SMSService, selected_data_collectors) {
                        //initialise the sendMessage object variable
                        $scope.messageForm ={
                            // scope : null,
                            // id_of_scope : null,
                            recipients : []
                        };

                        $scope.selected_data_collectors = [];

                        if (scope.quantity == 'multiple') {
                            /*selected_data_collectors is an array of ids if the quantity is multiple*/
                            for (var i = 0; i < selected_data_collectors.length; i++) {
                                var collector = selected_data_collectors[i];
                                $scope.selected_data_collectors.push({
                                    id : collector,
                                    name :  DataCollectorHolder.lookupDataCollector[collector].name,
                                    phone_number :  DataCollectorHolder.lookupDataCollector[collector].phone_number
                                });
                            }

                        }else {
                            $scope.selected_data_collectors = selected_data_collectors;
                        }

                        $scope.messageForm.recipients = $scope.selected_data_collectors;

                        $scope.send = function() {
                            if(scope.quantity == 'all'){
                                /*selected_data_collectors is an empty array when quantity is 'all'*/

                                //Circuits only have the headteacher property
                                if (scope.selected_model.head_teachers_holder && scope.selected_model.head_teachers_holder.length) {
                                    for (var j = 0; j < scope.selected_model.head_teachers_holder.length; j++) {
                                        var collecta = scope.selected_model.head_teachers_holder[j];
                                        $scope.selected_data_collectors.push({
                                            id : collecta.id,
                                            name :  collecta.name,
                                            phone_number :  collecta.phone_number
                                        });
                                    }
                                }

                                if (scope.selected_model.circuit_supervisors_holder && scope.selected_model.circuit_supervisors_holder.length) {
                                    for (var k = 0; k < scope.selected_model.circuit_supervisors_holder.length; k++) {
                                        var collectar = scope.selected_model.circuit_supervisors_holder[k];
                                        $scope.selected_data_collectors.push({
                                            id : collectar.id,
                                            name :  collectar.name,
                                            phone_number :  collectar.phone_number
                                        });
                                    }
                                }
                                $scope.messageForm.recipients = $scope.selected_data_collectors;

                            }
                            $rootScope.loading = true;
                            SMSService.sendMessageToDataCollectors($scope.messageForm)
                            //Success callback
                                .success(function (value) {
                                    if (value.status == 200) {
                                        scNotifier.notifySuccess('Notification', 'Messages have been sent successfully')
                                    } else {
                                        scNotifier.notifyInfo('Notification', 'Messages have been queued up and will be sent shortly. No further action required')
                                    }

                                    $rootScope.loading = false;
                                })
                                //Error callback
                                .error(function () {
                                    scNotifier.notifyInfo('Notification', 'Please check network connection and try again');
                                    $rootScope.loading = false;
                                });
                            $scope.cancel();
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                })
            }
        }

    }]);