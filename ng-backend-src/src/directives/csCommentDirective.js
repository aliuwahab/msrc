/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('csComment', ['$http','scNotifier','$timeout','$rootScope', function($http, scNotifier, $timeout, $rootScope){
    return {

        scope : {
            submission_id : '@submissionId',
            submission_category : '@category',
            comment_exists : '@commentExists'

            /*
             case 'student_enrolment':
             case 'student_attendance':
             case 'teacher_attendance':
             case 'teachers_classmanagement':
             */
        },


        templateUrl: 'partials/miscellaneous/csCommentsTemplate.html',

        controller : function ($scope) {
            $scope.adminRight = $rootScope.adminRight;
            $scope.showComment = false;
            $scope.postingComment = false;
            $scope.csComment = angular.copy($scope.comment_exists);

            $scope.postComment = function () {
                $scope.postingComment = true;

                if ($scope.csComment == '' || $scope.csComment.length < 2) {
                    scNotifier.notifyInfo("No Comment Entered", "Enter a valid comment to continue");
                    return;
                }

                var data_to_post = {
                    cs_comment : $scope.csComment,
                    submission_id : $scope.submission_id,
                    submission_category : $scope.submission_category
                };

                $http.post('api/cs/comments/on/headteacher/submission', data_to_post)
                    .success(function (successData) {
                        if (successData.status == '200') {
                            scNotifier.notifySuccess("Comment Posted", "The comment has been posted successfully");
                            delete  $scope.showComment;
                        }
                    })
                    .error(function () {
                        scNotifier.notifyFailure("Network Error", "The comment could not be posted");

                    })
                    .finally(function () {
                        $scope.postingComment = false;
                    });
            }
        }

    }}]);