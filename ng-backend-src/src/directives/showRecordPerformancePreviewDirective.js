/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('recordBooksPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=recordBooksPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewRecordPerformance.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', '$state', 'format_school_term',  ShowRecordPreviewCtrl],
                    size: 'md'
                });
            });

            function ShowRecordPreviewCtrl ($scope, $modalInstance, scNotifier, $state, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;


                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "Admission Register",
                    term :   item.term,
                    status : item.admission_register,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Class Registers",
                    term :  item.term,
                    status : item.class_registers,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Teacher Attendance Register",
                    status: item.teacher_attendance_register,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Visitor\'s Book",
                    status: item.visitors,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Log Book",
                    status: item.log,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "SBA",
                    status: item.sba,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Movement Book",
                    status: item.movement,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "SPIP",
                    status: item.spip,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Inventory Books",
                    status: item.inventory_books,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Cummulative Record Books",
                    status: item.cummulative_records_books,
                    term : item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);