/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('editTeacher', ['$rootScope', '$modal', 'scSchool', 'scDistrict',
    function($rootScope, $modal, scSchool, scDistrict){

        return {

            scope : {
                jurisdiction : '@',
                teacher : '=editTeacher'
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/createTeacherModal.html',
                        controller: ['$rootScope', '$scope', '$modalInstance', '$upload', 'scNotifier', '$timeout', editTeacherModalCtrl],
                        size: 'lg'
                    });
                });

                function editTeacherModalCtrl ($rootScope, $scope, $modalInstance, $upload, scNotifier, $timeout) {

                    var teacher = angular.copy(scope.teacher);

                    teacher.gender = teacher.gender.toLowerCase();
                    teacher.level_taught = teacher.level_taught.toLowerCase();


                    $scope.formData = teacher;
                    $scope.formData.teacher_id =  teacher.id;


                    $scope.schools = scDistrict.districtLookup[teacher.district_id].allSchoolsHolder;

                    $scope.selected_school = scSchool.schoolLookup[teacher.school_id];

                    //This is to disable the uploading button
                    $scope.isSubmitting = false;

                    $scope.school_classes = [
                        {value: "Nursery 1", label:"Nursery 1"},
                        {value: "Nursery 2", label:"Nursery 2"},
                        {value: "KG1", label:"KG1"},
                        {value: "KG2", label:"KG2"},
                        {value: "P1", label:"P1"},
                        {value: "P2", label:"P2"},
                        {value: "P3", label:"P3"},
                        {value: "P4", label:"P4"},
                        {value: "P5", label:"P5"},
                        {value: "P6", label:"P6"}
                        //{value: "JHS1", label:"JHS1"},
                        //{value: "JHS2", label:"JHS2"},
                        //{value: "JHS3", label:"JHS3"},
                        //{value: "SHS1", label:"SHS1"},
                        //{value: "SHS2", label:"SHS2"},
                        //{value: "SHS3", label:"SHS3"}
                    ];

                    $scope.$watch('formData.level_taught', function(newValue , oldValue){
                        if (newValue === 'jhs' || newValue === 'shs') {
                            $scope.formData.class_taught = null;
                            $scope.formData.class_subject_taught = teacher.subject | '';
                            $scope.subjectLevel = true;
                            $scope.classLevel = false;
                        }
                        else if (newValue === 'nursery'){
                            $scope.classes = $scope.school_classes.slice(0,2);
                            $scope.formData.class_taught = teacher.subject || null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                        else if (newValue === 'kg'){
                            $scope.classes = $scope.school_classes.slice(2,4);
                            $scope.formData.class_taught = teacher.subject || null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                        else if (newValue === 'primary'){
                            $scope.classes = $scope.school_classes.slice(4,10);
                            $scope.formData.class_taught = teacher.subject || null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                        else{
                            $scope.classes = [];
                            $scope.formData.class_taught = null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                    });

                    $scope.teacherFormIsValid = function(){
                        if ( $scope.formData.level_taught === 'jhs' || $scope.formData.level_taught === 'shs') {
                            return !!($scope.formData.class_subject_taught)
                        }else  if ( $scope.formData.level_taught === 'nursery' || $scope.formData.level_taught === 'kg'|| $scope.formData.level_taught === 'primary') {
                            return !!($scope.formData.class_taught)
                        }else return false;
                    };


                    $scope.send = function() {
                        $rootScope.loading = true;
                        $scope.isSubmitting = true;

                        // $scope.formData.school_id = $scope.selected_school.id;
                        //backup school category before extending
                        // $scope.formData.category_backup =  $scope.formData.category;
                        //add the school data to form object
                        // angular.extend($scope.formData, $scope.selected_school);
                        //replace the category with what the user selected in the form
                        //because category in the form is being overwritten by the category of the school
                        // $scope.formData.category =  $scope.formData.category_backup;

                        //delete all unnecessary properties from the school obj before extending to be submittted
                        delete $scope.formData.average_teacher_attendance;
                        delete $scope.formData.category_backup;
                        delete $scope.formData.circuitName;
                        delete $scope.formData.created_at;
                        delete $scope.formData.current_state_of_community_involvement;
                        delete $scope.formData.current_state_of_friendliness_to_boys_girls;
                        delete $scope.formData.current_state_of_healthy_level;
                        delete $scope.formData.current_state_of_safety_and_protection;
                        delete $scope.formData.current_state_of_school_inclusiveness;
                        delete $scope.formData.current_state_of_teaching_and_learning;
                        delete $scope.formData.current_teacher_attendance_by_circuit_supervisor;
                        delete $scope.formData.current_teacher_attendance_by_headteacher;
                        delete $scope.formData.districtName;
                        delete $scope.formData.regionName;
                        delete $scope.formData.teacherAttendanceAverages;
                        delete $scope.formData.termlyDataHolder;
                        delete $scope.formData.totalFemaleTeachers;
                        delete $scope.formData.totalMaleTeachers;
                        delete $scope.formData.totalTeachers;
                        delete $scope.formData.updated_at;
                        delete $scope.formData.weeklyAttendanceHolder;
                        delete $scope.formData.full_name;

                        delete $scope.formData.classManagementHolder;
                        delete $scope.formData.punctualityHolder;
                        delete $scope.formData.school;
                        delete $scope.formData.unitsCoveredHolder;
                        delete $scope.formData.teachersInSchoolHolder;
                        delete $scope.formData.id;
                        delete $scope.formData.address;
                        delete $scope.formData.category_backup;
                        delete $scope.formData.description;
                        delete $scope.formData.email;
                        delete $scope.formData.ges_code;
                        delete $scope.formData.ghanaian_language;
                        delete $scope.formData.supervisor_id;
                        delete $scope.formData.headteacher_id;
                        delete $scope.formData.index;
                        delete $scope.formData.subject;
                        delete $scope.formData.name;
                        delete $scope.formData.lat_long;
                        delete $scope.formData.phone_number;
                        delete $scope.formData.rating;
                        delete $scope.formData.school_code;
                        delete $scope.formData.slug;
                        delete $scope.formData.shift;
                        delete $scope.formData.sms_code;
                        delete $scope.formData.status;
                        delete $scope.formData.showStatus;
                        delete $scope.formData.has_punctuality_comment;
                        delete $scope.formData.has_teacher_class_management_comment;
                        delete $scope.formData.has_units_covered_comment;


                        var dataParams = {};
                        dataParams[scope.jurisdiction + '_id'] = $scope.formData[scope.jurisdiction + '_id'];

                        scSchool.editTeacher($scope.formData)
                            //    success function
                            .success(function(value){
                                if (value === true || value.status === 200 || value.code === 200) {
                                    scNotifier.notifySuccess('School Teacher', 'Successfully edited teacher');
                                    scSchool.allSchoolTeachersData(dataParams)
                                        .success(function(value){
                                            $scope.isSubmitting = false;
                                            /*this updates the total number of teachers*/
                                            $rootScope.$broadcast('teachersUpdated', value);

                                            /*this updates the teachers loaded in the teacher Info view*/
                                            $timeout(function () {
                                                $rootScope.$broadcast('teacherInfoLoaded', 999);
                                            }, 500);

                                            scNotifier.notifyInfo('Please wait', 'Updating teacher information...');
                                            $scope.cancel();
                                            $rootScope.loading = false;
                                        });
                                }else{
                                    $rootScope.loading = false;
                                    $scope.isSubmitting = false;
                                    scNotifier.notifyFailure('School Teacher', 'An error occurred');
                                }
                            })
                            //    error function
                            .error(function(error){
                                //console.log("An error occured", error);
                                $scope.isSubmitting = false;
                                $rootScope.loading = false;

                                scNotifier.notifyFailure('School Teacher', 'An error occurred on the server. Please try again');
                            }
                        );
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }

            }
        }

    }]);