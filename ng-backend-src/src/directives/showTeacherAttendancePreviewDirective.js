/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('teacherAttendancePreview', ['$modal','scSchool','scNotifier', function($modal, scSchool, scNotifier){
    return {

        scope : {
            tappedRow : '=teacherAttendancePreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewTeacherAttendance.html',
                    controller: ['$scope', '$modalInstance', 'scSchool', 'School', 'format_school_term', '$timeout', ShowAttendancePreviewCtrl],
                    size: 'xlg'
                });
            });

            function ShowAttendancePreviewCtrl ($scope, $modalInstance, scSchool, School, format_school_term, $timeout) {

                $scope.school_name = scSchool.schoolLookup[scope.tappedRow.school_id].name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                $scope.submission_id = scope.tappedRow.id;
                $scope.week = scope.tappedRow.week_number;
                $scope.term = format_school_term[scope.tappedRow.term].label;
                $scope.year = scope.tappedRow.year;
                $scope.comment = scope.tappedRow.year;
                $scope.comment = scope.tappedRow.cs_students_attendance_comments;
                $scope.totalTeachers = scSchool.schoolLookup[ $scope.school_id].teachersInSchoolHolder.length;
                $scope.show = {
                    days_punctual : true,
                    exercises : false,
                    units_covered : false,
                    class_management : false
                };

                $scope.showSection = function (sectionToShow) {
                    angular.forEach($scope.show, function (val, key) {
                        $scope.show[key] = false;
                    });

                    $timeout(function () {
                        $scope.show[sectionToShow] = true;
                    }, 1);

                };

                /* cs_students_attendance_comments: null
                 cs_students_enrolment_comments: null
                 cs_teacher_attendance_comments: null
                 cs_teacher_class_management_comments: null*/

                var dataloader, loadedTeachersClassManagementFromServer, loadedTeacherAttendanceFromServer, loadedTeachersUnitsCoveredFromServer;
                dataloader = 0; //This variable prevents the data prepping from happening until both are loaded
                $scope.teacherInfoData = [];
                $scope.schoolNumberOfDaysInSession = 0;

                function removeTrailingComma(classSubject){
                    if ($.trim(classSubject.charAt(classSubject.length - 1)) == ',') {
                        return classSubject.substring(0, classSubject.length - 1)
                    }else return classSubject
                }

                //These variables only keep track of all weeks, term and years in the punctuality and class management objects
                var allWeeks =[],  allTerms = [], allYears = [], allDataCollectors = [], teacherAttendanceTracker = [], teacherAttendanceTotals = {};
                function prepTeacherData() {

                    for (var index = 0; index <  scSchool.schoolLookup[ $scope.school_id].teachersInSchoolHolder.length; index++) {
                        var item = scSchool.schoolLookup[ $scope.school_id].teachersInSchoolHolder[index], temp_lev_taught = '-';

                        if (item.class_taught == null || item.class_taught == 'null') item.class_taught = '';
                        if (angular.isDefined(item.level_taught)) temp_lev_taught = item.level_taught.toUpperCase();
                        if (item.class_subject_taught == null || item.class_subject_taught == 'null') item.class_subject_taught = '';
                        var tempObject = {
                            id: item.id,
                            staff_number: item.staff_number,
                            first_name: item.first_name,
                            last_name: item.last_name,
                            gender: item.gender.toUpperCase(),
                            level_taught: removeTrailingComma(temp_lev_taught),
                            subject: removeTrailingComma($.trim(item.class_subject_taught + item.class_taught)),
                            category: item.category,
                            rank: item.rank,
                            highest_academic_qualification: item.highest_academic_qualification,
                            highest_professional_qualification: item.highest_professional_qualification,
                            years_in_the_school: item.years_in_the_school,
                            punctualityHolder: {},
                            classManagementHolder: {},
                            unitsCoveredHolder: {}
                        };

                        var longitude = 0;
                        var latitude = 0;



                        for (var tp = 0; tp < loadedTeacherAttendanceFromServer.length; tp ++) {

                            var teacherPunctualityObject = loadedTeacherAttendanceFromServer[tp];
                            if (item.id == teacherPunctualityObject.teacher_id) {
                                teacherPunctualityObject.week_number = parseInt(teacherPunctualityObject.week_number);//parse it to int to enforce consistency in searching and filtering
                                tempObject.punctualityHolder = {
                                    id: teacherPunctualityObject.id,
                                    lesson_plan: teacherPunctualityObject.lesson_plans,
                                    days_present: teacherPunctualityObject.days_present,
                                    days_punctual: teacherPunctualityObject.days_punctual,
                                    days_absent_with_permission: teacherPunctualityObject.days_absent_with_permission,
                                    days_absent_without_permission: teacherPunctualityObject.days_absent_without_permission,
                                    longitude: teacherPunctualityObject.long,
                                    latitude: teacherPunctualityObject.lat,
                                    year: teacherPunctualityObject.year,
                                    term: teacherPunctualityObject.term,
                                    week: teacherPunctualityObject.week_number,
                                    data_collector_type: teacherPunctualityObject.data_collector_type,
                                    num_of_exercises_given: teacherPunctualityObject.num_of_exercises_given,
                                    num_of_exercises_marked: teacherPunctualityObject.num_of_exercises_marked,
                                    comment : teacherPunctualityObject.comment,
                                    cs_comments : teacherPunctualityObject.cs_comments
                                };

                                /*keep track that the punctuality obj has a comment*/
                                if (teacherPunctualityObject.cs_comments && teacherPunctualityObject.cs_comments.length) {
                                    tempObject.has_punctuality_comment = true;
                                }

                                /*start the total teacher attendance here*/

                                /*the concat variable includes the teacher ID so we only add the teachers attendance once, to prevent duplicates for theat week, term and year*/
                                var concat = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number +'-'+ teacherPunctualityObject.id;
                                /*The concat period is only the period WITHOUT the teachers ID*/
                                var concatPeriod = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number;

                                if (teacherAttendanceTracker.indexOf(concat) < 0) {
                                    if (angular.isDefined(teacherAttendanceTotals[concatPeriod])) {
                                        teacherAttendanceTotals[concatPeriod].totalAttendance += teacherPunctualityObject.days_present;
                                        teacherAttendanceTotals[concatPeriod].teachersTotal++;
                                    }else{
                                        teacherAttendanceTotals[concatPeriod] = {
                                            totalAttendance :  teacherPunctualityObject.days_present,
                                            teachersTotal : 1,
                                            expectedTeacherTotal : $scope.totalTeachers,
                                            days_in_session : teacherPunctualityObject.days_in_session,
                                            year: teacherPunctualityObject.year,
                                            term: teacherPunctualityObject.term,
                                            week_number: teacherPunctualityObject.week_number
                                        };
                                    }

                                }

                                allWeeks.push(teacherPunctualityObject.week_number);

                                allTerms.push(teacherPunctualityObject.term);

                                allYears.push(teacherPunctualityObject.year);

                                allDataCollectors.push(teacherPunctualityObject.data_collector_type);

                                $scope.filter_term = item.term;
                                $scope.filter_week = item.week_number;
                                $scope.filter_year = item.year;
                                $scope.filter_data_collector_type = item.data_collector_type;

                                var concatenate = teacherPunctualityObject.year + teacherPunctualityObject.term +
                                    teacherPunctualityObject.week_number + teacherPunctualityObject.data_collector_type;

                                $scope.schoolNumberOfDaysInSession = teacherPunctualityObject.days_in_session;

                                // tempObject.punctualityHolder.sort(function (a, b) {
                                //     return a.week - b.week
                                // });

                            }
                        }


                        for( var cm = 0; cm < loadedTeachersClassManagementFromServer.length; cm++){
                            var teacherClassManagementObject = loadedTeachersClassManagementFromServer[cm];
                            if (item.id == teacherClassManagementObject.teacher_id) {
                                teacherClassManagementObject.week_number = parseInt(teacherClassManagementObject.week_number);//parse it to int to enforce consistency in searching and filtering

                                tempObject.classManagementHolder = {
                                    id: teacherClassManagementObject.id,
                                    class_control: teacherClassManagementObject.class_control,
                                    children_behaviour: teacherClassManagementObject.children_behaviour,
                                    children_participation: teacherClassManagementObject.children_participation,
                                    teacher_discipline: teacherClassManagementObject.teacher_discipline,
                                    year: teacherClassManagementObject.year,
                                    term: teacherClassManagementObject.term,
                                    week: teacherClassManagementObject.week_number,
                                    data_collector_type: teacherClassManagementObject.data_collector_type,
                                    comment : teacherClassManagementObject.comment,
                                    cs_comments : teacherClassManagementObject.cs_comments

                                };

                                /*keep track that the class management obj has a comment*/
                                if (teacherClassManagementObject.cs_comments && teacherClassManagementObject.cs_comments.length) {
                                    tempObject.has_teacher_class_management_comment = true;
                                }

                                allWeeks.push(teacherClassManagementObject.week_number);

                                allTerms.push(teacherClassManagementObject.term);

                                allYears.push(teacherClassManagementObject.year);

                                allDataCollectors.push(teacherClassManagementObject.data_collector_type);
                                //
                                // tempObject.classManagementHolder.sort(function (a, b) {
                                //     return a.week - b.week
                                // });
                            }
                        }

                        for ( var uc = 0; uc < loadedTeachersUnitsCoveredFromServer.length; uc ++){

                            var teacherUnitsCoveredObject = loadedTeachersUnitsCoveredFromServer[uc];
                            if (item.id == teacherUnitsCoveredObject.teacher_id) {
                                teacherUnitsCoveredObject.week_number = parseInt(teacherUnitsCoveredObject.week_number);//parse it to int to enforce consistency in searching and filtering

                                tempObject.unitsCoveredHolder = {
                                    id: teacherUnitsCoveredObject.id,
                                    ghanaian_language_units_covered: teacherUnitsCoveredObject.ghanaian_language_units_covered,
                                    english_language_units_covered: teacherUnitsCoveredObject.english_language_units_covered,
                                    mathematics_units_covered: teacherUnitsCoveredObject.mathematics_units_covered,
                                    year: teacherUnitsCoveredObject.year,
                                    term: teacherUnitsCoveredObject.term,
                                    week: teacherUnitsCoveredObject.week_number,
                                    data_collector_type: teacherUnitsCoveredObject.data_collector_type,
                                    comment: teacherUnitsCoveredObject.comment,
                                    cs_comments : teacherUnitsCoveredObject.cs_comments
                                };

                                /*keep track that the units covered obj has a comment*/
                                if (teacherUnitsCoveredObject.cs_comments && teacherUnitsCoveredObject.cs_comments.length) {
                                    tempObject.has_units_covered_comment = true;
                                }

                                allWeeks.push(teacherUnitsCoveredObject.week_number);

                                allTerms.push(teacherUnitsCoveredObject.term);

                                allYears.push(teacherUnitsCoveredObject.year);

                                allDataCollectors.push(teacherUnitsCoveredObject.data_collector_type);

                                // tempObject.unitsCoveredHolder.sort(function (a, b) {
                                //     return a.week - b.week
                                // });
                            }
                        }



                        //This index variable helps to know which teacher was clicked on
                        // tempObject.index = index;
                        // $scope.lookupTeacherInfoData[index] = tempObject; /*this lookup object uses the id to lookup the clicked teacher's info*/

                        $scope.teacherInfoData.push(tempObject);
                        $scope.teacherInfoData.latitude = latitude;
                        $scope.teacherInfoData.longitude = longitude;

                    }
                }
                //Fired from the below when data is deleted successfully
                $scope.$on('dataLoaded', function(event, data){
                    //this should be greater than 2. that's when both all the three teacher segments are loaded
                    if (data.loader > 2) {
                        prepTeacherData();
                        $scope.loadingData = false;/*the variable to hide the gif is here because its is called here regardless of the start point of loadin data*/
                    }
                });

                function loadSchoolDataFromServer(year, term, week) {
                    $scope.loadingData = true; //this variable checks to display the loading gif whiles loading data
                    var dataParams = {school_id: scope.tappedRow.school_id};
                    dataloader = 0; // reset the variable that checks that both normal and special are loaded
                    if (week || term || year) {
                        angular.extend(dataParams, {
                            week_number: week,
                            term: term,
                            year: year,
                            data_collector_type : "head_teacher"
                        });
                    }else {
                        $scope.loadingEverything = true; //ths checks and hides the row that allows loading data
                    }
                    /* if not, load it from server and keep track of it in the variable */
                    School.allSchoolTeachersPunctuality_LessonPlan(dataParams).$promise
                        .then(function (data) {
                            loadedTeacherAttendanceFromServer = data;
                            $scope.$emit('dataLoaded', {loader: ++dataloader});
                        });

                    /* if not load it from server and keep track of it in the variable */
                    School.allSchoolTeachersClassManagement(dataParams).$promise
                        .then(function (data) {
                            loadedTeachersClassManagementFromServer = data;
                            $scope.$emit('dataLoaded', {loader: ++dataloader});
                        });

                    /* if not load it from server and keep track of it in the variable */
                    School.allSchoolTeachersUnitsCovered(dataParams).$promise
                        .then(function (data) {
                            loadedTeachersUnitsCoveredFromServer = data;
                            $scope.$emit('dataLoaded', {loader: ++dataloader});
                        });
                }

                loadSchoolDataFromServer(scope.tappedRow.year, scope.tappedRow.term, scope.tappedRow.week_number);



                               /*this function send the totals calculated to the server to update the weekly table*/
                $scope.updateSchoolWeeklyTotal = function () {
                    var holder = [];

                    $scope.updatingTotals = true;

                    angular.forEach(teacherAttendanceTotals, function (value, key) {
                        /*calculate the weeks percentage*/
                        /*first get the expected total attendance = total teachers in school x number of days school was in session*/
                        /*then use the actual total attendance, divided by the expected total number of teachers and multiply by 100 */
                        var aveAttendance = (value.totalAttendance/(value.days_in_session * value.expectedTeacherTotal)) * 100;

                        holder.push({
                            average_teacher_attendance : Math.round(aveAttendance),
                            current_number_of_teachers : value.teachersTotal,
                            year : value.year,
                            term : value.term,
                            week_number : value.week_number,
                            country_id : scope.tappedRow.country_id,
                            region_id : scope.tappedRow.region_id,
                            district_id : scope.tappedRow.district_id,
                            circuit_id : scope.tappedRow.circuit_id,
                            school_id : scope.tappedRow.school_id
                        })
                    });


                    scSchool.updateWeeklyTableForTeacherAttendance(holder)
                        .success(function (successData) {
                            if (successData.status == 200) {
                                scNotifier.notifySuccess("Recalculation", "Teacher Attendance totals have been updated.")
                            }
                        })
                        .error(function (errData) {
                            scNotifier.notifyFailure("Recalculation", "Teacher Attendance totals failed to update")
                        })
                        .finally(function () {
                            $scope.updatingTotals = false;
                        });


                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);