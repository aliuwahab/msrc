/**
 * Created by Kaygee on 21/05/2015.
 */
/*this uses GSM Notification*/
schoolMonitor.directive('addCommentSchoolTeacher', ['$rootScope', '$modal', 'scNotifier',
    function($rootScope, $modal, scNotifier){

        return {

            scope : {
                addCommentSchoolTeacher : "@",/*i.e. the id of the object to put the comment on*/
                section : "@",/*eg: attendance, class_management, units_covered*/
                previousComment : "@",/*eg: attendance, class_management, units_covered*/
                selected_teacher : '=selectedTeacher' /*The object of the selected teacher*/
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/addTeacherCommentSectionModal.html',
                        controller: ['$scope', '$modalInstance', '$http', addCommentController],
                        size : 'md'
                    });


                    function addCommentController ($scope, $modalInstance, $http) {

                        if (scope.section == 'punctuality_and_lessons') {
                            $scope.section = 'Attendance, Punctuality and Lesson Plan'
                        }
                        if (scope.section == 'teachers_class_management') {
                            $scope.section = 'Classroom Management'
                        }
                        if (scope.section == 'teacher_units_covered') {
                            $scope.section = 'Units Covered & Exercises'
                        }

                        $scope.selected_teacher = scope.selected_teacher.first_name + " " + scope.selected_teacher.last_name;

                        $scope.commentForm ={
                            cs_comment : scope.previousComment,
                            submission_id : scope.addCommentSchoolTeacher,
                            submission_category : scope.section
                        };

                        $scope.postingComment = false;

                        $scope.submit = function() {

                            if ($scope.commentForm.cs_comment == '' || $scope.commentForm.cs_comment.length < 2) {
                                scNotifier.notifyInfo("No Comment Entered", "Enter a valid comment to continue");
                                return;
                            }

                            $scope.postingComment = true;


                            $http.post('api/cs/comments/on/headteacher/submission', $scope.commentForm)
                                .success(function (successData) {
                                    if (successData.status == '200') {
                                        scNotifier.notifySuccess("Comment Posted", "The comment has been posted successfully");
                                        $scope.commentForm.cs_comment = "";
                                        $rootScope.$broadcast("reloadSubmissions", scope.section);
                                    }
                                })
                                .error(function () {
                                    scNotifier.notifyFailure("Network Error", "The comment could not be posted");

                                })
                                .finally(function () {
                                    $scope.postingComment = false;
                                    $scope.cancel();

                                });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                })
            }
        }

    }]);