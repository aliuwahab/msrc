/**
 * Created by Kaygee on 24/07/2014.
 */

//This directive is used in the recent activities table. It is used to determine the color of the
//activity posted

schoolMonitor.directive('teacherStatusLabelColor', function(){
//    var linker = function($scope, element, attr){
    return {
        scope : {
            'teacher_status' : '@teacherStatusLabelColor'
        },
        controller : function ($scope) {
            $scope.labelFormat = {
                atpost : 'At Post',
                studyleave : 'Study Leave',
                otherleave : 'Other Leave',
                retired : 'Retired',
                deceased : 'Deceased'
            }
        },
        replace : false,
        restrict : 'AE',
        template : '<span class="label "\n' +
        '                                      ng-class="{\'label-primary\' : teacher_status == \'atpost\',\n' +
        '                                      \'label-default\' : teacher_status == \'studyleave\',\n' +
        '                                      \'label-info\' : teacher_status == \'otherleave\',\n' +
        '                                      \'label-warning\' : teacher_status == \'retired\',\n' +
        '                                      \'label-danger\' : teacher_status == \'deceased\'\n' +
        '                                      }">{{ labelFormat[teacher_status] || \'At Post\' }}</span>'
    }
});