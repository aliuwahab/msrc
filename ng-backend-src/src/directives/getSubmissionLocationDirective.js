/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('getSubmissionLocation', ['$http', 'foursquare_client_id', 'foursquare_client_secret',
    'scDistrict',
    function($http, foursquare_client_id, foursquare_client_secret, scDistrict){

        return {
            scope: {
                stringParentData: '@getSubmissionLocation',
                filter_year: '@filterYear',
                filter_term: '@filterTerm',
                filter_week: '@filterWeek',
                filter_data_collector_type: '@filterDataCollectorType',
                section: '@section',
                district_id: '@districtId'/*used to display a default name when the location isn't found*/
            },
            controller: function ($scope) {
                //A function to request for the foursquare venue/search api
                function lookupLocation(lat, long) {
//            long =  5.644557; lat = -0.152182;
                    /*     $http.get('' +
                     '?client_id=CLIENT_ID' +
                     '&client_secret=CLIENT_SECRET' +
                     '&v=20130815&' +
                     'll=40.7,-74&' +
                     'query=sushi')*/
                    return $http.get("https://api.foursquare.com/v2/venues/search", {
                        params: {
                            client_id: foursquare_client_id,
                            client_secret: foursquare_client_secret,
                            v: 20130815,
                            ll: long + ',' + lat,
//                        near : 'Accra',
                            limit: 5,
                            intent: 'browse',
                            radius: 100
                        },
                        cache: true
                    });
                }

                //Name to display when the venue from foursquare is not known
                var defaultName = "Unavailable";
                if($scope.district_id){
                    defaultName = scDistrict.districtLookup[$scope.district_id].name;
                }


                $scope.getDataSubmissionLocation = function () {
                    var concatenater = "";

                    if ($scope.section && $scope.section == 'termly') {
                        /*if termly, don't check for weekly param*/
                        if (!$scope.filter_year || !$scope.filter_term || !$scope.filter_data_collector_type) {
                            return;
                        }
                        concatenater = $scope.filter_year + $scope.filter_term + $scope.filter_data_collector_type;

                    } else {
                        if (!$scope.filter_year || !$scope.filter_term || !$scope.filter_week || !$scope.filter_data_collector_type) {
                            return;
                        }
                        concatenater = $scope.filter_year + $scope.filter_term + $scope.filter_week + $scope.filter_data_collector_type;

                    }

                    $scope.parentData = JSON.parse($scope.stringParentData);

                    var long = $scope.parentData[concatenater].longitude;
                    var lat = $scope.parentData[concatenater].latitude;

                    if (long != 0 && lat != 0) {
                        lookupLocation(lat, long).success(function (data) {
                            if (data.response.venues.length) {
//                            $scope.parentDataLocationSubmitted = data.response.venues[0].name;
                                $scope.parentData[concatenater].venue = data.response.venues[0].name;
                            } else {
                                $scope.parentData[concatenater].venue = defaultName;
                            }
                        })
                            .error(function () {
                                $scope.parentData[concatenater].venue = defaultName;
                            })
                    } else {
                        $scope.parentData[concatenater].venue = defaultName;
                    }
                };
            },
            link: function ($scope, element, attr) {
            },

            templateUrl: 'partials/miscellaneous/getSubmissionLocation.html'
            }
    }]);