/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('meetingsPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=meetingsPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewMeetings.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatGoodPoorFair', 'format_school_term',  ShowMeetingsPreviewCtrl],
                    size: 'md'
                });
            });

            function ShowMeetingsPreviewCtrl ($scope, $modalInstance, scNotifier, formatGoodPoorFair, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;

                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;

                $scope.schoolMeetingsCSVisits = item.number_of_circuit_supervisor_visit;
                $scope.schoolMeetingsInsets = item.number_of_inset;

                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "Staff Meeting",
                    frequency: item.number_of_staff_meeting,
                    males_present: item.num_males_present_at_staff_meeting,
                    females_present: item.num_females_present_at_staff_meeting,
                    week :   item.week_number,
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    comment : item.comment
                });

                $scope.recordRow.push({
                    name: "SPAM",
                    frequency: item.number_of_spam_meeting,
                    males_present: item.num_males_present_at_spam_meeting,
                    females_present: item.num_females_present_at_spam_meeting,
                    week :   item.week_number,
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    comment : item.comment
                });

                $scope.recordRow.push({
                    name: "PTA",
                    frequency: item.number_of_pta_meeting,
                    males_present: item.num_males_present_at_pta_meeting,
                    females_present: item.num_females_present_at_pta_meeting,
                    week :   item.week_number,
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    comment : item.comment
                });

                $scope.recordRow.push({
                    name: "SMC",
                    frequency: item.number_of_smc_meeting,
                    males_present: item.num_males_present_at_smc_meeting,
                    females_present: item.num_females_present_at_smc_meeting,
                    week :   item.week_number,
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    comment : item.comment
                });


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);