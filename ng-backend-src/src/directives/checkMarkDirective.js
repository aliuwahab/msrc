/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('checkMark', function(){
//    var linker = function($scope, element, attr){
    return {
        scope : {
            status : '@'
        },

        restrict : 'AE',
        replace : true,
        template : "<i class='fa' style='font-size: 24px' ng-class=\"{'fa-check greenColor' : status == 'yes', 'fa-times redColor' : status == 'no'}\">"
    }
});