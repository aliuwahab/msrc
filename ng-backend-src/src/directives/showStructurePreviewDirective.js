/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('structurePreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=structurePreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewStructure.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatGoodPoorFair', 'format_school_term',  ShowStructurePreviewCtrl],
                    size: 'md'
                });
            });

            function ShowStructurePreviewCtrl ($scope, $modalInstance, scNotifier, formatGoodPoorFair, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;


                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "Walls",
                    term :   item.term,
                    status : formatGoodPoorFair[item.walls],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Doors",
                    term :   item.term,
                    status : formatGoodPoorFair[item.doors],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Windows",
                    term :   item.term,
                    status : formatGoodPoorFair[item.windows],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Floors",
                    term :   item.term,
                    status : formatGoodPoorFair[item.floors],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Blackboard",
                    term :   item.term,
                    status : formatGoodPoorFair[item.blackboard],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);