/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('communityPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=communityPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewCommunityInvolvement.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatGoodPoorFair', 'format_school_term',  ShowCommunityPreviewCtrl],
                    size: 'md'
                });
            });

            function ShowCommunityPreviewCtrl ($scope, $modalInstance, scNotifier, formatGoodPoorFair, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;

                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "PTA Involvement",
                    term :   item.term,
                    status : formatGoodPoorFair[item.pta_involvement],
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Parents Notified of Student Progress",
                    term :   item.term,
                    status : formatGoodPoorFair[item.parents_notified_of_student_progress],
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Community Sensitization on School Attendance",
                    term :   item.term,
                    status : formatGoodPoorFair[item.community_sensitization_for_school_attendance],
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "School Management Committees",
                    term :   item.term,
                    status : formatGoodPoorFair[item.school_management_committees],
                    year : item.year,
                    data_collector_type : item.data_collector_type
                });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);