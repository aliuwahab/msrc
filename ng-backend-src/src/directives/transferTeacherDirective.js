/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('transferTeacher', ['$rootScope', '$modal', 'scSchool',
    function($rootScope, $modal, scSchool){

        return {

            scope : {
                teacher : '=transferTeacher',
                closeSelection : '&'
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        controller : ['$scope', '$modalInstance', 'scRegion', 'scDistrict', 'scCircuit', 'scSchool', 'School', 'scNotifier', TransferTeacherController],
                        size : 'md',
                        templateUrl : 'partials/miscellaneous/transferTeacherModal.html'
                    });

                    function TransferTeacherController($scope, $modalInstance, scRegion, scDistrict, scCircuit, scSchool, School, scNotifier){
                        $scope.selected_teacher = angular.copy(scope.teacher);

                        $scope.regions = scRegion.regions;
                        $scope.districts = scDistrict.districts;
                        $scope.circuits = scCircuit.circuits;
                        $scope.schools = scSchool.schools;

                        $scope.cancel = function(){
                            $modalInstance.dismiss();
                        };

                        $scope.confirmTransfer = function () {
                            if (!$scope.formData.school_id) {
                                scNotifier.notifyInfo("No School Selected", "Select teacher's new school to continue");
                                return;
                            }

                            if ($.trim($scope.formData.school_id) == $.trim(scope.teacher.school.id)) {
                                scNotifier.notifyInfo("Same School Selected", "Select teacher's new school to continue");
                                return;
                            }
                            $scope.isSubmitting = true;

                            /*This is a temporal check to make sure that the teacher is really being transferred */
                            var staff_number =  prompt("Are you sure you want to transfer " + scope.teacher.first_name + " "
                                + scope.teacher.last_name + " to " + scSchool.schoolLookup[$scope.formData.school_id].name ,
                                "Enter teacher's staff number to confirm" );

                            if ($.trim(scope.teacher.staff_number) == $.trim(staff_number)) {
                                var dataToSend = {
                                    teacher_id :  scope.teacher.id,
                                    teacher_old_school_id : scope.teacher.school.id,
                                    old_school_reference_code : scope.teacher.school.school_code,
                                    teacher_new_school_id :  $scope.formData.school_id,
                                    new_school_reference_code :  scSchool.schoolLookup[$scope.formData.school_id].school_code
                                };
                                scSchool.transferTeacher(dataToSend).then(
                                    //    success function
                                    function(value){
                                        if (value == true) {
                                            scNotifier.notifySuccess('School Teacher', 'Successfully transferred teacher');
                                            scSchool.allSchoolTeachersData({school_id : scope.teacher.school.id})
                                                .success(function(value){
                                                    $rootScope.$broadcast('teachersUpdated', value);
                                                    $scope.isSubmitting = false;
                                                    scope.closeSelection();
                                                    $modalInstance.dismiss('cancel');
                                                });
                                        }else if(value == false){
                                            $scope.isSubmitting = false;
                                            scNotifier.notifyFailure('School Teacher', 'Teacher could not be transferred.');
                                        }else{
                                            $scope.isSubmitting = false;
                                            scNotifier.notifyFailure('School Teacher', 'An error occurred');
                                        }
                                    },
                                    //    error function
                                    function(error){
                                        //console.log("An error occured", error);
                                        $scope.isSubmitting = false;
                                        scNotifier.notifyFailure('School Teacher', 'An error occurred on the server. Please try again');
                                    }
                                );

                            }else{
                                $scope.isSubmitting = false;
                                scNotifier.notifyInfo("Wrong Confirmation", "A wrong staff number was entered");

                            }

                        }
                    }
                })
            }
        }

    }]);