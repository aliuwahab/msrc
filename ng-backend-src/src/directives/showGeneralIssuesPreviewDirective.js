/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('generalIssuesPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=generalIssuesPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewGeneralIssues.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatGoodPoorFair', 'format_school_term',  ShowGenerralIssuesPreviewCtrl],
                    size: 'lg'
                });
            });

            function ShowGenerralIssuesPreviewCtrl ($scope, $modalInstance, scNotifier, formatGoodPoorFair, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;

                var item = scope.tappedRow.data;
                if (item.length > 0) {
                    $scope.term = format_school_term[item[0].term].label;
                    $scope.year = item[0].year;
                    $scope.submission_id = item[0].id;
                    $scope.cs_comments = item[0].cs_comments;
                }

                $scope.recordRow = {
                    'Inclusive' : [],
                    'Effective Teaching and Learning' : [],
                    'Healthy' : [],
                    'Safe and Protective' : [],
                    'Friendliness to Boys and Girls' : [],
                    'Community Involvement' : []
                };

                $scope.recordRow['Effective Teaching and Learning'].push(item[0]);
                $scope.recordRow['Healthy'].push(item[1]);
                $scope.recordRow['Effective Teaching and Learning'].push(item[2]);
                $scope.recordRow['Friendliness to Boys and Girls'].push(item[3]);
                $scope.recordRow['Friendliness to Boys and Girls'].push(item[4]);
                $scope.recordRow['Community Involvement'].push(item[5]);
                $scope.recordRow['Friendliness to Boys and Girls'].push(item[6]);
                $scope.recordRow['Community Involvement'].push(item[7]);
                $scope.recordRow['Community Involvement'].push(item[8]);
                $scope.recordRow['Inclusive'].push(item[9]);
                $scope.recordRow['Inclusive'].push(item[10]);
                $scope.recordRow['Community Involvement'].push(item[11]);
                $scope.recordRow['Healthy'].push(item[12]);
                $scope.recordRow['Safe and Protective'].push(item[13]);


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);