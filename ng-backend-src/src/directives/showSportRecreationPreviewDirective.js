/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('sportsPreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=sportsPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewSports.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'formatAvailability', 'format_school_term',  ShowSportsPreviewCtrl],
                    size: 'md'
                });
            });

            function ShowSportsPreviewCtrl ($scope, $modalInstance, scNotifier, formatAvailability, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;


                $scope.recordRow = [];

                $scope.recordRow.push({
                    name: "Football",
                    term :   item.term,
                    status : formatAvailability[item.football],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Volleyball",
                    term :   item.term,
                    status : formatAvailability[item.volleyball],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Netball",
                    term :   item.term,
                    status : formatAvailability[item.netball],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Playing Field",
                    term :   item.term,
                    status : formatAvailability[item.playing_field],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Sports Wear",
                    term :   item.term,
                    status : formatAvailability[item.sports_wear],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Seesaw",
                    term :   item.term,
                    status : formatAvailability[item.seesaw],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "Merry-Go-Round",
                    term :   item.term,
                    status : formatAvailability[item.merry_go_round],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });
                $scope.recordRow.push({
                    name: "First Aid Box",
                    term :   item.term,
                    status : formatAvailability[item.first_aid_box],
                    year : item.year,
                    comment : item.comment,
                    data_collector_type : item.data_collector_type
                });


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);