/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('requiredImages', ['$rootScope', 'scSchool',
    function($rootScope, scSchool){

        return {

            scope : true,

            link : function(scope, elem, attr){
                scope.section = attr.requiredImages
            },

            controller : function($scope){
            },

            templateUrl : 'partials/miscellaneous/schoolRequiredImagesPartial.html'
        }

    }]);