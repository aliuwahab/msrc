/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('createTeacher', ['$rootScope', '$modal', 'scSchool',
    function($rootScope, $modal, scSchool){

        return {

            scope : {
                selected_school : '@createTeacher'
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/createTeacherModal.html',
                        controller: ['$rootScope', '$scope', '$modalInstance', '$upload', 'scNotifier', '$timeout', createTeacherModalCtrl],
                        size: 'lg'
                    });
                });

                function createTeacherModalCtrl ($rootScope, $scope, $modalInstance, $upload, scNotifier, $timeout) {

                    $scope.formData = {};

                    $scope.selected_school = JSON.parse(scope.selected_school);

                    $scope.myFiles = [];

                    //This is to disable the uploading button
                    $scope.isSubmitting = false;

                    $scope.school_classes = [
                        {value: "Nursery 1", label:"Nursery 1"},
                        {value: "Nursery 2", label:"Nursery 2"},
                        {value: "KG1", label:"KG1"},
                        {value: "KG2", label:"KG2"},
                        {value: "P1", label:"P1"},
                        {value: "P2", label:"P2"},
                        {value: "P3", label:"P3"},
                        {value: "P4", label:"P4"},
                        {value: "P5", label:"P5"},
                        {value: "P6", label:"P6"}
                        //{value: "JHS1", label:"JHS1"},
                        //{value: "JHS2", label:"JHS2"},
                        //{value: "JHS3", label:"JHS3"},
                        //{value: "SHS1", label:"SHS1"},
                        //{value: "SHS2", label:"SHS2"},
                        //{value: "SHS3", label:"SHS3"}
                    ];

                    $scope.$watch('formData.level_taught', function(newValue , oldValue){
                        if (newValue == 'jhs' || newValue == 'shs') {
                            $scope.formData.class_taught = null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = true;
                            $scope.classLevel = false;
                        }
                        else if (newValue == 'nursery'){
                            $scope.classes = $scope.school_classes.slice(0,2);
                            $scope.formData.class_taught = null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                        else if (newValue == 'kg'){
                            $scope.classes = $scope.school_classes.slice(2,4);
                            $scope.formData.class_taught = null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                        else if (newValue == 'primary'){
                            $scope.classes = $scope.school_classes.slice(4,10);
                            $scope.formData.class_taught = null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                        else{
                            $scope.classes = [];
                            $scope.formData.class_taught = null;
                            $scope.formData.class_subject_taught = '';
                            $scope.subjectLevel = false;
                            $scope.classLevel = true;
                        }
                    });

                    $scope.teacherFormIsValid = function(){
                        if ( $scope.formData.level_taught == 'jhs' || $scope.formData.level_taught == 'shs') {
                            return !!($scope.formData.class_subject_taught)
                        }else  if ( $scope.formData.level_taught == 'nursery' || $scope.formData.level_taught == 'kg'|| $scope.formData.level_taught == 'primary') {
                            return !!($scope.formData.class_taught)
                        }else return false;
                    };

                    $scope.fileSelected = function(files, evt) {
                        //if a file array is passed to this function
                        if (files) {
                            //get the first and only file in the array
                            var file = files[0];
                            //check if it is a csv file
                            if (file.type == "text/csv" ||
                                file.type == "application/csv" ||
                                file.type == "application/excel"||
                                file.type == "application/vnd.ms-excel"||
                                file.type == "text/comma-separated-values"||
                                file.type == "application/vnd.ms-excel") {
                                $scope.file = file;
                                $scope.fileIsCorrect = true;
                            }else{
                                scNotifier.notifyInfo('Upload Notification', "You selected a   \'" + file.type + "\'   file. Select a CSV file to upload")
                            }
                            //if not display the info toast that a csv file must be uploaded
                        }
                    };

                    $scope.cancelUpload = function(){
                        $scope.file = null;
                        $scope.fileIsCorrect = false;
                        $scope.fileUploaded = false;
                        $scope.isSubmitting = false;
                    };

                    $scope.uploadCsv = function(file){
                        $scope.isSubmitting = true;
                        $rootScope.loading = true;
                        if (file) $upload.upload({
                            url: '/admin/school_report_card/csv/upload/teachers', // upload.php script, node.js route, or servlet url
                            method: 'POST',
                            //headers: {'Authorization': 'xxx'}, // only for html5
                            //withCredentials: true,
                            data: $scope.selected_school,
                            file: file // single file or a list of files. list is only for html5
                            //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                            //fileFormDataName: myFile, // file formData name ('Content-Disposition'), server side request form name
                            // could be a list of names for multiple files (html5). Default is 'file'
                            //formDataAppender: function(formData, key, val){}  // customize how data is added to the formData.

                        }).progress(function(evt) {
                            $scope.max =  parseInt(100);
                            $scope.current =  parseInt(100.0 * evt.loaded  / evt.total );
                            //console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :'+ evt.config.file.name);
                        }).success(function(data, status, headscSchers, config) {
                            // file is uploaded successfully
                            $scope.fileUploaded = true;
                            scSchool.allSchoolTeachersData({school_id : $stateParams.id})
                                .success(function(value){
                                    $rootScope.$broadcast('teachersUpdated', value);
                                    scNotifier.notifySuccess('CSV Upload', 'Successfully uploaded CSV');
                                    $timeout(function(){
                                        $modalInstance.dismiss('cancel');
                                        $rootScope.loading = false;
                                    }, 3000);
                                });

                        });
                        //.error(...)
                        //.then(success, error, progress); // returns a promise that does NOT have progress/abort/xhr functions
                        //.xhr(function(xhr){xhr.upload.addEventListener(...)}) // access or attach event listeners to
                        //the underlying XMLHttpRequest

                    };

                    $scope.send = function() {
                        $rootScope.loading = true;
                        $scope.isSubmitting = true;
                        $scope.formData.school_id = $scope.selected_school.id;
                        //backup school category before extending
                        $scope.formData.category_backup =  $scope.formData.category;

                        //delete all unnecessary properties from the school obj before extending to be submittted
                        delete $scope.selected_school.average_teacher_attendance;
                        delete $scope.selected_school.category_backup;
                        delete $scope.selected_school.circuitName;
                        delete $scope.selected_school.created_at;
                        delete $scope.selected_school.current_state_of_community_involvement;
                        delete $scope.selected_school.current_state_of_friendliness_to_boys_girls;
                        delete $scope.selected_school.current_state_of_healthy_level;
                        delete $scope.selected_school.current_state_of_safety_and_protection;
                        delete $scope.selected_school.current_state_of_school_inclusiveness;
                        delete $scope.selected_school.current_state_of_teaching_and_learning;
                        delete $scope.selected_school.current_teacher_attendance_by_circuit_supervisor;
                        delete $scope.selected_school.current_teacher_attendance_by_headteacher;
                        delete $scope.selected_school.districtName;
                        delete $scope.selected_school.regionName;
                        delete $scope.selected_school.teacherAttendanceAverages;
                        delete $scope.selected_school.termlyDataHolder;
                        delete $scope.selected_school.totalFemaleTeachers;
                        delete $scope.selected_school.totalMaleTeachers;
                        delete $scope.selected_school.totalTeachers;
                        delete $scope.selected_school.updated_at;
                        delete $scope.selected_school.weeklyAttendanceHolder;

                        //add the school data to form object
                        angular.extend($scope.formData, $scope.selected_school);
                        //replace the category with what the user selected in the form
                        //because category in the form is being overwritten by the category of the school
                        $scope.formData.category =  $scope.formData.category_backup;
                        scSchool.createTeacher($scope.formData).then(
                            //    success function
                            function(value){
                                if (value == true) {
                                    scNotifier.notifySuccess('School Teacher', 'Successfully added a new teacher');
                                    scSchool.allSchoolTeachersData({school_id : $scope.selected_school.id})
                                        .success(function(value){
                                            $rootScope.$broadcast('teachersUpdated', value);
                                            $scope.isSubmitting = false;
                                            $modalInstance.dismiss('cancel');
                                        });
                                }else if(value == false){
                                    $scope.isSubmitting = false;
                                    scNotifier.notifyFailure('School Teacher', 'Teacher could not be added.');
                                }else{
                                    $scope.isSubmitting = false;
                                    scNotifier.notifyFailure('School Teacher', 'An error occurred');
                                }
                            },
                            //    error function
                            function(error){
                                //console.log("An error occured", error);
                                $scope.isSubmitting = false;
                                scNotifier.notifyFailure('School Teacher', 'An error occurred on the server. Please try again');
                            }
                        );
                        $rootScope.loading = false;
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }

            }
        }

    }]);