/**
 * Created by KayGee on 30-Jun-16.
 */

schoolMonitor.directive('updateSchool', ['$rootScope', 'scSchool', '$modal', 'scUpdateWeeklyTable','scNotifier','$interval',
    function ($rootScope, scSchool, $modal, scUpdateWeeklyTable, scNotifier, $interval) {

    var intervalPromise;

        return {
            scope : {
                school : '=updateSchool',
                years : '='
            },
            link : function (scope, elem, attr) {


                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/updateConfirmModelUpdateModal.html',
                        controller: ['$scope', '$modalInstance', confirmSchoolUpdateModalCtrl],
                        size : 'md',
                        backdrop : 'static'
                    });

                    function confirmSchoolUpdateModalCtrl ($scope, $modalInstance) {

                        $scope.model = {
                            type : 'school',
                            type_proper : 'School',
                            year : ''
                        };

                        $scope.yearsAvailable = scope.years;
                        $scope.selected_model = scope.school;
                        $scope.stepper = 0;
                        $scope.updatingModel = false;



                        $scope.doLikeUpdating = function () {
                            $scope.stepper +=5;
                            $scope.updatingModel +=5;

                           /* $scope.updatingModel = scUpdateWeeklyTable.updatingModel;

                            $scope.stepper = scUpdateWeeklyTable.updatingModel;*/

                            if ($scope.stepper < 25) {
                                $scope.barColor = 'danger';
                            } else if ($scope.stepper < 50) {
                                $scope.barColor = 'warning';
                            } else if ($scope.stepper < 75) {
                                $scope.barColor = 'info';
                            } else {
                                $scope.barColor = 'success';
                            }
                            if ($scope.stepper >= 99) {
                                $scope.updatingModel = false;
                                scNotifier.notifySuccess("School Updated Successfully", scope.school.name + " has successfully been updated");
                                $interval.cancel(intervalPromise);
                                $scope.cancel();
                            }
                        };

                        $scope.$on('finishedUpdating', function (event) {
                            $scope.doLikeUpdating()
                        });

                        $scope.update = function () {
                            $scope.stepper = 0;

                            scSchool.updateWeeklyTableAtBackend({
                                scope : 'school',
                                scope_level_id : scope.school.id,
                                year : $scope.model.year
                            }).success(function (returned) {
                                // console.log('returned : ',returned);
                            });

                            /* scUpdateWeeklyTable.runWeeklyUpdateTotalsForModel({
                                 school_id : scope.school.id,
                                 year : $scope.model.year
                             });*/

                            intervalPromise = $interval(function () {
                                $scope.doLikeUpdating();
                            }, 1000, 60)
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });
            }
        }

    }]);
