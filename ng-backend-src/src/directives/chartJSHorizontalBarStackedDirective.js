/**
 * Created by Kaygee on 16/06/2018.
 */


schoolMonitor.directive('chartJsHorizontalBar', function(scReportGenerator){

    var chart;
    function drawChart(labelsArray, labelArray, chartData, target) {
        var ctx = document.getElementById("horizontalBarChart").getContext("2d");
        chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labelsArray,
                datasets: chartData
            },
            options: {
                title: {
                    display: true,
                    text: labelArray
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
        if (chart) chart.update();
        scReportGenerator.barChart[target] = chart;

    }

    return {
        scope : {
            'labels' : '=labels',
            'label' : '@',
            'data' : '=chartJsHorizontalBar'
        },

        link : function($scope, element, attr){
            $scope.$watch($scope.data, function (newVal) {
                if (chart) chart.update();

                else drawChart($scope.labels, $scope.label, $scope.data, attr.target);
            }, true);
        },
        controller : function ($scope) {
            $scope.download2 = function () {
                if (chart) {
                    console.log(chart.toBase64Image());
                }
            }
        },
        replace : true,
        restrict : 'AE',
        template : '<div class="chart-container" style="position: relative; width:100%">\n' +
        '<canvas id="horizontalBarChart" width="400" height="350" style="width: 100%; height: 350px"></canvas>\n' +
        // '<button class="btn btn-primary" ng-click="download2()">Download</button>'+
        '</div>'
    }
});


/*
*
		var barChartData = {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: window.chartColors.red,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 2',
				backgroundColor: window.chartColors.blue,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}, {
				label: 'Dataset 3',
				backgroundColor: window.chartColors.green,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				]
			}]

		};
		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Chart.js Bar Chart - Stacked'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			barChartData.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});
			});
			window.myBar.update();
		});

* */