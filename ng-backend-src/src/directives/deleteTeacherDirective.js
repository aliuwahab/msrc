/**
 * Created by KayGee on 30-Jun-16.
 */

schoolMonitor.directive('deleteTeacher', ['$rootScope', '$modal', 'scSchool','scNotifier','$timeout',
    function ($rootScope, $modal, scSchool, scNotifier, $timeout) {

        return {
            scope : {
                teacher : '=deleteTeacher',
                jurisdiction : '@'

            },
            link : function (scope, elem, attr) {
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/confirmTeacherDeleteModal.html',
                        controller: ['$scope', '$modalInstance', deleteTeacherModalCtrl],
                        size : 'md'
                    });

                    function deleteTeacherModalCtrl ($scope, $modalInstance) {

                        $scope.teacherName = scope.teacher.first_name + ' ' + scope.teacher.last_name;
                        $scope.schoolName = scSchool.schoolLookup[scope.teacher.school_id].name;


                        var dataParams = {};
                        dataParams[scope.jurisdiction + '_id'] = scope.teacher[scope.jurisdiction + '_id'];

                        $scope.delete = function () {
                            $rootScope.loading = true;
                            scSchool.deleteTeacher({teacher_id : scope.teacher.id, school_id : scope.teacher.school_id})
                                .success(function (succData) {
                                    if (succData.status == 200) {
                                        scNotifier.notifySuccess('Teacher', $scope.teacherName + ' has been deleted successfully.');

                                        scSchool.allSchoolTeachersData(dataParams)
                                            .success(function(value){
                                                /*this updates the total number of teachers*/
                                                $rootScope.$broadcast('teachersUpdated', value);

                                                /*this updates the teachers loaded in the teacher Info view*/
                                                $timeout(function () {
                                                    $rootScope.$broadcast('teacherInfoLoaded', 999);
                                                }, 500);

                                                scNotifier.notifyInfo('Please wait', 'Updating teacher information...');
                                                $scope.cancel();
                                                $rootScope.loading = false;

                                            });

                                    }


                                })
                                .error(function () {
                                    $rootScope.loading = false;

                                })

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });
            }
        }

    }]);
