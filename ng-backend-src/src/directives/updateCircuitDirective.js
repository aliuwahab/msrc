/**
 * Created by KayGee on 30-Jun-16.
 */

schoolMonitor.directive('updateCircuit', ['$rootScope', 'scSchool', '$modal', 'scUpdateWeeklyTable','scNotifier','$interval',
    function ($rootScope, scSchool, $modal, scUpdateWeeklyTable, scNotifier, $interval) {

        var intervalPromise;

        return {
            scope : {
                circuit : '=updateCircuit',
                years : '='
            },
            link : function (scope, elem, attr) {


                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/updateConfirmModelUpdateModal.html',
                        controller: ['$scope', '$modalInstance', confirmCircuitUpdateModalCtrl],
                        size : 'md',
                        backdrop : 'static'
                    });

                    function confirmCircuitUpdateModalCtrl ($scope, $modalInstance) {

                        $scope.model = {
                            type : 'circuit',
                            type_proper : 'Circuit',
                            year : ''
                        };

                        $scope.yearsAvailable = scope.years;
                        $scope.selected_model = scope.circuit;
                        $scope.stepper = 0;
                        $scope.updatingModel = false;



                        $scope.doLikeUpdating = function () {

                            // $scope.updatingModel = scUpdateWeeklyTable.updatingModel;
                            // $scope.stepper = scUpdateWeeklyTable.updatingModel;
                            $scope.stepper +=5;
                            $scope.updatingModel +=5;

                            if ($scope.stepper < 25) {
                                $scope.barColor = 'danger';
                            } else if ($scope.stepper < 50) {
                                $scope.barColor = 'warning';
                            } else if ($scope.stepper < 75) {
                                $scope.barColor = 'info';
                            } else {
                                $scope.barColor = 'success';
                            }
                            if ($scope.stepper >= 99) {
                                $scope.updatingModel = false;
                                scNotifier.notifySuccess("Circuit Updated Successfully", scope.circuit.name + " has successfully been updated");
                                $interval.cancel(intervalPromise);
                                $scope.cancel();
                            }
                        };

                        $scope.$on('finishedUpdating', function (event) {
                            $scope.doLikeUpdating()
                        });

                        $scope.update = function () {
                            $scope.stepper = 0;

                            scSchool.updateWeeklyTableAtBackend({
                                scope : 'circuit',
                                scope_level_id : scope.circuit.id,
                                year : $scope.model.year
                            }).success(function (returned) {
                                // console.log('returned : ',returned);
                            });

                            /* scUpdateWeeklyTable.runWeeklyUpdateTotalsForModel({
                                 circuit_id : scope.circuit.id,
                                 year : $scope.model.year
                             });
 */
                            intervalPromise = $interval(function () {
                                $scope.doLikeUpdating();
                            }, 1000, 60)
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });
            }
        }

    }]);
