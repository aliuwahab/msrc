/**
 * Created by Kaygee on 24/07/2014.
 */

/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('anchorScroll', ['$location','$anchorScroll', function($location, $anchorScroll){
    return  function($scope, element, attr) {
        element.bind('click', function () {
            $('ul.nav.nav-tabs.nav-justified').find('li.active').removeClass('active');
            $('a[data-target="#' + attr.anchorScroll + '"]').parent('li').addClass('active');
            $location.hash(attr.anchorScroll);
            $anchorScroll();
        });

    };

}]);