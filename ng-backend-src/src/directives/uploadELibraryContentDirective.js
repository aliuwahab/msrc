/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('uploadElibrary', ['$rootScope', '$modal', 'scSchool', 'scDistrict',
    function($rootScope, $modal, scSchool, scDistrict){

        return {

            scope : {

            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/uploadElibraryModal.html',
                        controller: ['$rootScope', '$scope', '$modalInstance', 'FileUploader', 'scNotifier', '$timeout', editTeacherModalCtrl],
                        size: 'lg'
                    });
                });

                function editTeacherModalCtrl ($rootScope, $scope, $modalInstance, FileUploader, scNotifier, $timeout) {

                    var uploader = $scope.uploader = new FileUploader({
                        url: 'targeted/instructions/upload/elibrary/file'
                    });

                    $scope.formData = {
                        targeted_groups : '', /*'t_one','t_two','t_one_and_t_two','no_restriction'*/
                        file_type : '', /*'pdf','word','excel','image','video'*/
                        file_title : 'text',
                        file_description : 'text',
                        e_library_file : 'file itself'
                    }

                    // FILTERS

                    uploader.filters.push({
                        name: 'imageFilter',
                        fn: function(item /*{File|FileLikeObject}*/, options) {
                            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                        }
                    });

                    // CALLBACKS
                    //
                    // uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
                    //     console.info('onWhenAddingFileFailed', item, filter, options);
                    // };
                    // uploader.onAfterAddingFile = function(fileItem) {
                    //     console.info('onAfterAddingFile', fileItem);
                    // };
                    // uploader.onAfterAddingAll = function(addedFileItems) {
                    //     console.info('onAfterAddingAll', addedFileItems);
                    // };
                    // uploader.onBeforeUploadItem = function(item) {
                    //     console.info('onBeforeUploadItem', item);
                    // };
                    // uploader.onProgressItem = function(fileItem, progress) {
                    //     console.info('onProgressItem', fileItem, progress);
                    // };
                    // uploader.onProgressAll = function(progress) {
                    //     console.info('onProgressAll', progress);
                    // };
                    // uploader.onSuccessItem = function(fileItem, response, status, headers) {
                    //     console.info('onSuccessItem', fileItem, response, status, headers);
                    // };
                    // uploader.onErrorItem = function(fileItem, response, status, headers) {
                    //     console.info('onErrorItem', fileItem, response, status, headers);
                    // };
                    // uploader.onCancelItem = function(fileItem, response, status, headers) {
                    //     console.info('onCancelItem', fileItem, response, status, headers);
                    // };
                    // uploader.onCompleteItem = function(fileItem, response, status, headers) {
                    //     console.info('onCompleteItem', fileItem, response, status, headers);
                    // };
                    // uploader.onCompleteAll = function() {
                    //     console.info('onCompleteAll');
                    // };
                    //
                    // console.info('uploader', uploader);

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }

            }
        }

    }]);