/**
 * Created by Kaygee on 30/07/2014.
 */
//<--This is the Table Export Button-->
schoolMonitor.directive('exportTable', function($timeout, $rootScope){

    var instance;

    var linker = function(scope, element, attribute){
        /*  $(element).parent('div').prepend(

              '<button class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Table Data</button>' +
              '<ul class="dropdown-menu " role="menu">' +
              '<li><a class="pointer" id="customXLSButton"> <img src=\'/img/tableExportIcons/xls.png\' width=\'24px\'> TEST XLS</a></li>' +
              '<li><a class="pointer" onClick ="$(\'' + target + '\').tableExport({type:\'csv\',escape:\'false\'}); return false"> <img src=\'/img/tableExportIcons/csv.png\' width=\'24px\'> CSV</a></li>' +
              '<li><a class="pointer"  onClick ="$(\'' + target + '\').tableExport({type:\'png\',escape:\'false\'}); return false"> <img src=\'/img/tableExportIcons/png.png\' width=\'24px\'> PNG</a></li>' +
               '<li class="divider"></li>' +
               '<li><a class="pointer"  onClick ="$(\'' + target + '\').tableExport({type:\'excel\',escape:\'false\'}); return false"> <img src=\'/img/tableExportIcons/xls.png\' width=\'24px\'> XLS</a></li>' +
              '<li class="divider"></li>' +
              '<li><a class="pointer"  onClick ="$(\'' + target + '\').tableExport({type:\'doc\',escape:\'false\'}); return false"> <img src=\'/img/tableExportIcons/word.png\' width=\'24px\'> Word</a></li>' +
              '<li><a class="pointer"  onClick ="$(\'' + target + '\').tableExport({type:\'pdf\',pdfFontSize:\'7\',escape:\'false\',tableName : \'Sanitation\'}); return false"> <img src=\'/img/tableExportIcons/pdf.png\' width=\'24px\'> PDF</a></li>' +
              '</ul>'
          );
  */

        $(element).bind('click', function () {
            /*Show Export Button*/
            var table = $('#' + scope.exportTable);

            instance = table.tableExport({
                headers: true,
                // footers: true,
                formats: ['csv'],
                filename: attribute.fileName,
                bootstrap: true,
                position: 'top',
                // ignoreRows: null,
                // ignoreCols: null,
                // ignoreCSS: '.tableexport-ignore',
                // emptyCSS: '.tableexport-empty',
                trimWhitespace: true
            });


            $timeout(function () {
                instance.reset();
            }, 0);
        })

    };
    return {
        restrict : 'A',
        scope : {
            exportTable : '@exportTable'
        },
        link : linker
    }
});