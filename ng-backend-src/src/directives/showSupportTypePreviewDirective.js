/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('supportTypePreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=supportTypePreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewSupportType.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', '$state', 'format_school_term',  ShowSupportTypePreviewCtrl],
                    size: 'md'
                });
            });

            function ShowSupportTypePreviewCtrl ($scope, $modalInstance, scNotifier, $state, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                var item = scope.tappedRow.data;
                $scope.term = format_school_term[item.term].label;
                $scope.year = item.year;
                $scope.cs_comments = item.cs_comments;
                $scope.submission_id = item.id;


                $scope.recordRow = [];

                 $scope.recordRow .push({
                    name: "Donor Support",
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    in_cash : item.donor_support_in_cash,
                    in_kind : item.donor_support_in_kind,
                    comment : item.comment
                });
                 $scope.recordRow .push({
                    name: "Community Support",
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    in_cash : item.community_support_in_cash,
                    in_kind : item.community_support_in_kind,
                    comment : item.comment
                });
                 $scope.recordRow .push({
                    name: "District Support",
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    in_cash : item.district_support_in_cash,
                    in_kind : item.district_support_in_kind,
                    comment : item.comment
                });
                 $scope.recordRow .push({
                    name: "PTA Support",
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    in_cash : item.pta_support_in_cash,
                    in_kind : item.pta_support_in_kind,
                    comment : item.comment
                });
                 $scope.recordRow .push({
                    name: "Other",
                    term :   item.term,
                    year : item.year,
                    data_collector_type : item.data_collector_type,
                    in_cash : item.other_support_in_cash,
                    in_kind : item.other_support_in_kind,
                    comment : item.comment
                });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);