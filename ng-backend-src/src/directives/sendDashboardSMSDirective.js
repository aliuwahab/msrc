/**
 * Created by Kaygee on 21/05/2015.
 */
schoolMonitor.directive('sendSmsModal', ['$rootScope', '$modal', 'scNotifier','DataCollectorHolder',
    function($rootScope, $modal, scNotifier, DataCollectorHolder){

        return {

            scope : {
                size : "@",/*sm, lg*/
                selected_model : '=selectedModel', /*The object of the selected model*/
                dataCollectors_to_send_messages_to : '=multipleDataCollectors' /*Data collectors to send multiple messages to*/
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/sendSMSModal.html',
                        controller: ['$scope', '$modalInstance', 'SMSService', sendMessageModalCtrl],
                        size : scope.size || 'sm'
                    });


                    function sendMessageModalCtrl ($scope, $modalInstance, SMSService, selected_data_collectors) {
                        $scope.sendingSms = false;

                        $scope.messageForm = {
                            recipient : scope.selected_model,
                            message : ''
                        };

                        $scope.sendSms = function () {
                            if ($scope.messageForm.message.length < 5) {
                                scNotifier.notifyInfo("Please provide a message");
                                return;
                            }
                            $scope.sendingSms = true;

                            SMSService.sendSmsFromDashboard($scope.messageForm)
                                .success(function (successData) {
                                    if(successData.status == '200'){
                                        scNotifier.notifySuccess("Message sent successfully");
                                        $scope.cancel();
                                    }
                                })
                                .error(function () {
                                    scNotifier.notifyFailure("Oops! Something went wrong. Please check internet connection");
                                }).finally(function () {
                                    $scope.sendingSms = false;
                                });
                        };


                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                })
            }
        }

    }]);