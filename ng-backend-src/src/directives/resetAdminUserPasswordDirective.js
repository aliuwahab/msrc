/**
 * Created by Kaygee on 21/05/2015.
 */
/*this uses GSM Notification*/
schoolMonitor.directive('resetUserPassword', ['$rootScope', '$modal', 'scNotifier','$timeout',
    function($rootScope, $modal, scNotifier, $timeout){

        return {
            scope : {
                resetUserPassword : '='
            },

            link : function(scope, elem, attr){
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/resetPasswordModal.html',
                        controller: ['$scope', '$modalInstance','scUser', resetModalCtrl],
                        size : 'md'
                    });


                    function resetModalCtrl ($scope, $modalInstance, scUser) {
                        //initialise the password object variable
                        $scope.model ={

                        };

                        $scope.send = function() {
                            var dataToSend = {
                                id : scope.resetUserPassword.id,
                                email : scope.resetUserPassword.email,
                                password : ""
                            };
                            
                            if ($scope.model.old_password) {
                                if ($scope.model.new_password == $scope.model.confirm_new_password) {
                                    scNotifier.notifySuccess('Password Reset', 'Password will be reset shortly');
                                    dataToSend.password =  $scope.model.new_password;

                                }else{
                                    scNotifier.notifyFailure('Password Reset', 'Password mismatch. Confirm the correct password to change.');
                                    return false;
                                }
                            }else{
                                scNotifier.notifyFailure('Password Reset', 'Enter current password');
                                return false;
                            }
                            $rootScope.loading = true;
                            scUser.resetPassword(dataToSend).then(
                                //success callback
                                function (value) {
                                    // We use array because the server responseType is a json object
                                    // if (value[0] == 's' && value[1] == 'u') {
                                        //$scope.formData = {};
                                        scUser.reset();
                                        $rootScope.$broadcast('updateAdmin');
                                        scNotifier.notifySuccess('Success', 'Password changed successfully');
                                        $rootScope.loading = false;
                                        $scope.cancel();

                                        // $scope.successMessage = 'Admin user successfully created' ;
                                    // }else if(value){
                                    //     $rootScope.loading = false;
                                    //     angular.forEach(value, function(value, prop){
                                    //         $scope.validation [prop] = true;
                                    //     });
                                    //     scNotifier.notifyFailure('Password Reset', 'Something isn\'t right. Password couldn\'t be changed');
                                    //
                                    // }
                                    // else{
                                    //     scNotifier.notifyFailure('Error', 'There was an error editing the user');
                                    //     // $scope.errorMessageFromServer = value ;
                                    //     $rootScope.loading = false
                                    // }

                                },
                                // error callback
                                function (httpResponse) {
                                    scNotifier.notifyFailure('Error', 'There was an error creating the user');
                                    //$scope.errorMessageFromServer = 'There was an error in creating the user' ;
                                    $rootScope.loading = false
                                });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                })
            }
        }

    }]);