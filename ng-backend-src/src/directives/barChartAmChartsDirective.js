/**
 * Created by Kaygee on 24/07/2014.
 */

/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('amBarChart', ['$interval', function($interval){
    var chart, chartScrollBar, categoryAxis, valueAxis, graph, chartCursor;

    return {
        scope : {
            chartData : '=amBarChart',
            valueAxisTitle : '@axisTitle',
            chartTitle : '@chartTitle'
        },
        controller : function($scope){
            // $scope.changeBarChart = function (chartData, chartTitle, chartDivID, type, average){
            //
            // }
        },
        link : function ($scope, element, attr) {

            $scope.chartData = [
                {"field" : "Boys",
                    "value" : 54
                },
                {"field" : "Girls",
                    "value" : 82
                },
                {"field" : "Both",
                    "value" : 62
                }
            ];

            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = $scope.chartData;
            chart.categoryField = "field";
            chart.startDuration = 1;
            chart.maxSelectedSeries = 25;
            chart.mouseWheelScrollEnabled = true;
            chart.pathToImages = "http://cdn.amcharts.com/lib/3/images/";

            // SCROLLBAR
            chartScrollBar = new AmCharts.ChartScrollbar();
            chartScrollBar.graphType = 'column';
            chartScrollBar.resizeEnabled = true;
            chartScrollBar.scrollbarHeight = 21;
            chartScrollBar.scrollbarWIdth = 2;
            chartScrollBar.scrollDuration = 0;
            chartScrollBar.updateOnReleaseOnly = false;
            chart.addChartScrollbar(chartScrollBar);

            // AXES
            // category
            categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 0;
            categoryAxis.gridPosition = "start";
            categoryAxis.labelRotation = 45;

            //value
            valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = $scope.valueAxisTitle;
            valueAxis.dashLength = 1;
            valueAxis.axisAlpha = 0;
            valueAxis.minimum = 0;
            // if (chart_display_type == 'rating') {
            //     valueAxis.maximum = 10;
            // }
            valueAxis.stackType = "regular";
            valueAxis.integersOnly = true;
            valueAxis.gridCount = 10;
            valueAxis.reversed = false; // this line makes the value axis reversed
            chart.addValueAxis(valueAxis);

            // value
            chart.titles = [
                {
                    "id": "Title-1",
                    "size": 15,
                    text : $scope.chartTitle
                }
            ];

            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick: function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // GRAPH
            graph = new AmCharts.AmGraph();
            // if (chart_display_type) {
            //     graph.title = valueAxisTitle;
            // }
            graph.valueField = "value";
            graph.balloonText = "[[field]]: <b>[[value]]</b>";
            graph.type = "column";
            graph.labelText = "[[value]]";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);

//             if (chart_display_type) {
//                 // second graph
//                 graph = new AmCharts.AmGraph();
//                 graph.title = "Difference";
// //                graph.labelText = "[[value]]";
//                 graph.valueField = "remaining";
//                 graph.type = "column";
//                 graph.lineAlpha = 0;
//                 graph.fillAlphas = 1;
//                 graph.lineColor = "#ffc299";
//                 graph.balloonText = "Difference : <b>[[value]]</b>";
//                 chart.addGraph(graph);
//             }


            // CURSOR
            chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);

//            chart.creditsPosition = "top-right";

            // if (chart_display_type) {
            // LEGEND
            // var legend = new AmCharts.AmLegend();
            // legend.align = "center";
//            legend.markerType = "circle";
//                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
//                 chart.addLegend(legend);
//             }



            $interval(chart.write('amBarChartDiv'), 200, 2);


        },
        replace : true,
        restrict : 'AE',
        template : '<div id="amBarChartDiv" style="width: 100%; height: 400px;"></div>'
    }
}]);