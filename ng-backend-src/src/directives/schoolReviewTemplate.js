/**
 * Created by Kaygee on 22/05/2015.
 */
schoolMonitor.directive('reviewHtml', [function(){

    return {
        scope : {
            reviewData : '=reviewHtml',
            answers : '@',
            schoolsDistrictItinerary : '=',
            dataCollectorTypes : '=collectors',
            momentFormatDate : '&date'
        },

        link : function(scope, elem, attrs){
        },

        controller : function ($scope) {
            $scope.showFirstIndex = function (index) {
                return index != 0
            };
            $scope.filter_collector = 'head_teacher';
            $scope.filter_itinerary = '';
            if ($scope.schoolsDistrictItinerary.itinerary.length) {
                $scope.filter_itinerary = $scope.schoolsDistrictItinerary.itinerary[0].id;
            }

        },

        templateUrl : 'partials/templates_View/school/sch_review_template.html'
    }


}]);
