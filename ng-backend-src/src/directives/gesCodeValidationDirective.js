/**
 * Created by Kaygee on 22/01/2015.
 */

schoolMonitor.directive('gesCodeValidator', function(){

    return {
        restrict : 'AE',

        require : 'ngModel',

        link : function(scope, element, attr, ctrl){
            ctrl.$parsers.unshift(function(value){
                angular.forEach(scope.schools, function(school, index){
                    if (school.ges_code == value) {
                        var valid = school.ges_code == value;
                        ctrl.$setValidity('gesCodeExists', valid);
                        return valid ? value : undefined;
                    }
                });
            });
            ctrl.$formatters.unshift(function(value) {
                angular.forEach(scope.schools, function(school, index){
                    if (school.ges_code == value) {
                        var valid = school.ges_code == value;
                        ctrl.$setValidity('gesCodeExists', valid);
                        return value;
                    }
                });
            });
        }
    }

});