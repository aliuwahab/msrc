/**
 * Created by Kaygee on 16/06/2018.
 */


schoolMonitor.directive('chartJsPieSecond', function(){

    var chart;

    function drawChart(labelsArray, label, chartData, target) {
        chart = new Chart($(target).get(0).getContext("2d"), {
            type: 'pie',
            data: {
                labels: labelsArray,
                datasets: [{
                    data: chartData,
                    label: label,
                    backgroundColor: [
                        pattern.draw('zigzag-horizontal', 'rgba(255, 206, 86, 0.5)'),
                        pattern.draw('dot', 'rgba(153, 102, 255, 0.75)'),
                        pattern.draw('weave', 'rgba(119, 9, 146, 0.75)'),
                        pattern.draw('plus', 'rgba(255, 159, 64, 0.5)'),
                        pattern.draw('square', 'rgba(75, 192, 192, 0.5)'),
                        pattern.draw('diamond', 'rgba(54, 162, 235, 0.5)'),
                        pattern.draw('triangle', 'rgba(153, 102, 255, 0.5)'),
                        pattern.draw('circle', 'rgba(255, 99, 132, 0.5)'),
                        pattern.draw('cross', 'rgba(239, 7, 7, 0.75)'),
                        pattern.draw('disc', 'rgba(231, 235, 8, 0.75)'),
                        pattern.draw('ring', 'rgba(16, 204, 13, 0.75)')
                    ]
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Teacher Attendance Above 50%'
                },
                responsive: true
            }
        });

        if (chart) chart.update();
    }


    return {
        scope : {
            'labels' : '=labels',
            'label' : '@label',
            'data' : '=data'
        },

        link : function($scope, element, attr){
            var elem = $(element).find('canvas');
            // $parent.find('#' + idOfCanvas).get(0).getContext("2d");
            $scope.$watch($scope.data, function (newVal) {
                if (chart) chart.update();

                else drawChart($scope.labels, $scope.label, $scope.data, elem);
            }, true);

        },
        /* controller : function ($scope) {

         },*/
        replace : false,
        restrict : 'AE',
        template : '<div class="chart-container" style="position: relative; width:100%">' +
        '<canvas id="{{id}}" width="400" height="300" style="width: 100%; height: 300px"></canvas>\n' +
        '</div>'
    }
});


/*
* var config = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
					],
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.orange,
						window.chartColors.yellow,
						window.chartColors.green,
						window.chartColors.blue,
					],
					label: 'Dataset 1'
				}],
				labels: [
					'Red',
					'Orange',
					'Yellow',
					'Green',
					'Blue'
				]
			},
			options: {
				responsive: true
			}
		};*/