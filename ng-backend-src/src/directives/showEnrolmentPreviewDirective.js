/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('enrolmentPreview', ['$modal','scSchool','scNotifier', function($modal, scSchool,scNotifier){
    return {

        scope : {
            tappedRow : '=enrolmentPreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewEnrollment.html',
                    controller: ['$scope', '$modalInstance', 'scSchool', 'School', 'format_school_term',  ShowEnrolmentPreviewCtrl],
                    size: 'lg'
                });
            });

            function ShowEnrolmentPreviewCtrl ($scope, $modalInstance, scSchool, School, format_school_term) {

                $scope.school_name = scSchool.schoolLookup[scope.tappedRow.school_id].name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                $scope.submission_id = scope.tappedRow.id;
                $scope.week = scope.tappedRow.week_number;
                $scope.term = format_school_term[scope.tappedRow.term].label;
                $scope.year = scope.tappedRow.year;
                $scope.comment = scope.tappedRow.cs_students_enrolment_comments;
                /*
                 * cs_students_attendance_comments: null
                 cs_students_enrolment_comments: null
                 cs_teacher_attendance_comments: null
                 cs_teacher_class_management_comments: null*/

                var dataloader, loadedEnrollmentInfoFromServer, loadedSpecialEnrollmentInfoFromServer;
                dataloader = 0; //This variable prevents the data prepping from happening until both are loaded
                $scope.enrollmentData = [];
                $scope.enrollmentData.totals = {};/*an object whose keys are concatenation of the year,week,term and dc to track totals of data*/

                $scope.enrollmentDataTotals = {};/*an object whose keys are concatenation of the year,week,term and dc to track totals of data*/
                $scope.schoolNumberOfDaysInSession = 0;

                function prepEnrollmentData() {
                    for (var index = 0; index < loadedEnrollmentInfoFromServer.length; index++) {
                        var item = loadedEnrollmentInfoFromServer[index];


                        if(item.data_collector_type == 'circuit_supervisor'){
                            continue;
                        }


                        var concatenate = item.year + item.term + item.week_number + item.data_collector_type;
                        if ($scope.enrollmentData.totals[concatenate]) {
                            $scope.enrollmentData.totals[concatenate].normal += parseInt(item.total_enrolment);
                            $scope.enrollmentData.totals[concatenate].boys += parseInt(item.num_males);
                            $scope.enrollmentData.totals[concatenate].girls += parseInt(item.num_females);
                        } else {
                            $scope.enrollmentData.totals[concatenate] = {
                                normal: parseInt(item.total_enrolment),
                                special: 0,
                                boys : parseInt(item.num_males),
                                special_boys : 0,
                                girls : parseInt(item.num_females),
                                special_girls : 0,
                                week_number :  item.week_number,
                                term : item.term,
                                year : item.year
                            }
                        }


                        if (_.isEmpty($scope.enrollmentDataTotals)) {
                            $scope.enrollmentDataTotals = {
                                normal: parseInt(item.total_enrolment),
                                special: 0
                            }
                        } else {
                            $scope.enrollmentDataTotals.normal += parseInt(item.total_enrolment)
                        }
                        var tempObject = {
                            index: index, //This is used to know which index was clicked so we pass it into the chart function
                            selected: false, //This is used to know which index was clicked so we highlight that row
                            class: item.level,
                            boys: item.num_males,
                            girls: item.num_females,
                            streams: item.num_of_streams,
                            total: parseInt(item.num_males) + parseInt(item.num_females),
                            term: item.term,
                            week: item.week_number,
                            year: item.year,
                            special_boys: 0,
                            special_girls: 0,
                            data_collector_type: item.data_collector_type,
                            date_created: moment(item.created_at).utc().valueOf(),
                            normal_id: item.id
                        };
                        for (var subIndex = 0; subIndex < loadedSpecialEnrollmentInfoFromServer.length; subIndex++) {
                            var special_item = loadedSpecialEnrollmentInfoFromServer[subIndex];
                            if (special_item.level == item.level &&
                                special_item.term == item.term &&
                                special_item.week_number == item.week_number &&
                                special_item.year == item.year &&
                                special_item.data_collector_type == item.data_collector_type) {
                                tempObject.special_boys = special_item.num_males;
                                tempObject.special_girls = special_item.num_females;
                                tempObject.special_id = special_item.id;
                                $scope.enrollmentDataTotals.special += parseInt(special_item.total_enrolment);
                                $scope.enrollmentData.totals[concatenate].special += parseInt(special_item.total_enrolment);
                                $scope.enrollmentData.totals[concatenate].special_boys += parseInt(special_item.num_males);
                                $scope.enrollmentData.totals[concatenate].special_girls += parseInt(special_item.num_females);
                            }
                        }
                        $scope.enrollmentData.push(tempObject);

                        //Assign the total number of days for the year, term, week and dc type
                        $scope.schoolNumberOfDaysInSession = item.school_in_session || '-';

                    }
                }
                //Fired from the below when data is deleted successfully
                $scope.$on('dataLoaded', function(event, data){
                    //this should be greater than 1. that's when both special and normal have loaded
                    if (data.loader > 1) {
                        prepEnrollmentData();
                        $scope.loadingData = false;/*the variable to hide the gif is here because its is called here regardless of the start point of loadin data*/
                    }
                });

                function loadSchoolDataFromServer(year, term, week) {
                    $scope.loadingData = true; //this variable checks to display the loading gif whiles loading data
                    var dataParams = {school_id: scope.tappedRow.school_id};
                    dataloader = 0; // reset the variable that checks that both normal and special are loaded
                    if (week || term || year) {
                        angular.extend(dataParams, {
                            week_number: week,
                            term: term,
                            year: year,
                            data_collector_type : "head_teacher"
                        });
                    }else {
                        $scope.loadingEverything = true; //ths checks and hides the row that allows loading data
                    }
                    /*if not, load it from server and keep track of it in the variable */
                    School.allSpecialEnrollmentData(dataParams).$promise
                        .then(function (data) {
                            loadedSpecialEnrollmentInfoFromServer = data[0].special_school_enrolment;
                            $scope.$emit('dataLoaded', {loader: ++dataloader});
                        });

                    /*if not load it from server and keep track of it in the variable */
                    School.allEnrollmentData(dataParams).$promise
                        .then(function (data) {
                            loadedEnrollmentInfoFromServer = data[0].school_enrolment;
                            $scope.$emit('dataLoaded', {loader: ++dataloader});
                        });
                }

                loadSchoolDataFromServer(scope.tappedRow.year, scope.tappedRow.term, scope.tappedRow.week_number);

                /*this function send the totals calculated to the server to update the weekly table*/
                $scope.updateSchoolWeeklyTotal = function () {
                    var holder = [];

                    $scope.updatingTotals = true;

                    angular.forEach($scope.enrollmentData.totals, function (value, key) {
                        holder.push({
                            normal_enrolment_total_boys : value.boys,
                            normal_enrolment_total_girls : value.girls,
                            normal_enrolment_total_students : value.normal,
                            // normal_attendance_total_boys : value.boys,
                            // normal_attendance_total_girls : value.boys,
                            // normal_attendance_total_students : value.boys,
                            special_enrolment_total_boys : value.special_boys,
                            special_enrolment_total_girls : value.special_girls,
                            special_enrolment_total_students : value.special,
                            // special_attendance_total_boys : value.boys,
                            // special_attendance_total_girls : value.boys,
                            // special_attendance_total_students : value.boys,
                            year : value.year,
                            term : value.term,
                            week_number : value.week_number,
                            country_id : scope.tappedRow.country_id,
                            region_id : scope.tappedRow.region_id,
                            district_id : scope.tappedRow.district_id,
                            circuit_id : scope.tappedRow.circuit_id,
                            school_id : scope.tappedRow.school_id
                        })
                    });


                    scSchool.updateWeeklyTableForEnrollment(holder)
                        .success(function (successData) {
                            if (successData.status == 200) {
                                scNotifier.notifySuccess("Recalculation", "Enrolment totals have been updated.")
                            }
                        })
                        .error(function (errData) {
                            scNotifier.notifyFailure("Recalculation", "Enrolment totals failed to update")
                        })
                        .finally(function () {
                            $scope.updatingTotals = false;
                        });



                };


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);