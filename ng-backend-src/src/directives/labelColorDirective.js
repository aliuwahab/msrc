/**
 * Created by Kaygee on 24/07/2014.
 */

//This directive is used in the recent activities table. It is used to determine the color of the
//activity posted

schoolMonitor.directive('labelColor', function(){
//    var linker = function($scope, element, attr){
    return {
        scope : {
            type : '@'
        },
        controller : function($scope){
           $scope.labels = {
               schoolreportcard : 'label-primary',
               schoolvisits : 'label-success',
               schoolreview : 'label-warning'
            };

            $scope.display = {
                schoolreportcard : 'School Report Card',
                schoolvisits : 'School Visit',
                schoolreview : 'School Review'
            };

        },
        replace : true,
        restrict : 'AE',
        template : '<span> Submission under <span class="label " ng-class=" labels[type] "> {{ display[type] }}</span></span>'
    }
});