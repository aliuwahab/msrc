/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('scrollInfinitely', ['$timeout', function($timeout){
    return {
        transclude: true,

        template : "<div infinite-scroll='addMoreData()' infinite-scroll-distance='1' infinite-scroll-disabled='addingNextSet'><div ng-transclude=''></div></div>",
        //infinite-scroll-immediate-check='{boolean}'

        controller : function ($scope) {
            $scope.addingNextSet = false; /*variable to disable the inifinite scroller*/
            $scope.numberToShow = 25; /*Variable to show how much data to show*/
            $scope.addMoreData = function () {/*function called when the scoller reaches the bottom*/
                $scope.addingNextSet = true;
                $timeout(function () {
                    $scope.numberToShow = $scope.numberToShow + 25;
                    $scope.addingNextSet = false;
                }, 100);
            };
        }

    }
}]);