/**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('examScorePreview', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            tappedRow : '=examScorePreview'
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/previewPupilPerformanceScore.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', 'School', 'format_school_term',  ShowExamScorePreviewCtrl],
                    size: 'lg'
                });
            });

            function ShowExamScorePreviewCtrl ($scope, $modalInstance, scNotifier, School, format_school_term) {

                $scope.school_name = scope.tappedRow.name;
                $scope.school_id = scope.tappedRow.school_id;
                $scope.district_id = scope.tappedRow.district_id;
                $scope.recordRow = [];

                $scope.loadingData = true;

                var dataToLoad = {
                    school_id : $scope.school_id,
                    year : scope.tappedRow.year,
                    term : scope.tappedRow.term
                };

                School.allSchoolPupilPerformance(dataToLoad,
                    /*success function*/
                    function (allSchoolPupilPerformanceData) {

                        angular.forEach(allSchoolPupilPerformanceData[0].pupils_performance, function(item , index){
                            if (index === 0) {/*We just take the first item because its the same throughout*/
                                $scope.cs_comments = item.cs_comments;
                                $scope.submission_id = item.id;/*Comments are his only in the first item*/
                                $scope.year = item.year;
                                $scope.term = format_school_term[item.term].label;
                            }

                            var pupilPerformanceTempObj = {
                                class : item.level,
                                gh_lang_average_score : item.average_ghanaian_language_score,
                                number_scoring_above_gh_lang_average_score : item.num_pupil_who_score_above_average_in_ghanaian_language,
                                english_average_score : item.average_english_score,
                                number_scoring_above_english_score : item.num_pupil_who_score_above_average_in_english,
                                maths_average_score : item.average_maths_score,
                                number_scoring_above_maths_score : item.num_pupil_who_score_above_average_in_maths,
                                science_average_score : item.average_science_score,
                                number_scoring_above_science_score : item.num_pupil_who_score_above_average_in_science,
                                num_pupil_who_can_read_and_write : item.num_pupil_who_can_read_and_write,
                                year : item.year,
                                data_collector_type : item.data_collector_type,
                                term : item.term,
                                // week : item.week_number,
                                comment : item.comment
                            };

                            $scope.recordRow.push(pupilPerformanceTempObj);

                        });

                        $scope.loadingData = false;

                    });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);