 /**
 * Created by Kaygee on 14/07/2014.
 */

schoolMonitor.directive('reportSelectionModal', ['$modal','scSchool', function($modal, scSchool){
    return {

        scope : {
            selected_school : '=reportSelectionModal',
            schoolWeeks : '=',
            schoolTerms : '=',
            schoolYears : '='
        },

        link : function(scope, elem, attrs){

            elem.bind('click', function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/generateReportSelectionModal.html',
                    controller: ['$scope', '$modalInstance', 'scNotifier', '$state', GenerateReportModalCtrl],
                    size: 'md'
                });
            });

            function GenerateReportModalCtrl ($scope, $modalInstance, scNotifier, $state) {

                $scope.available_weeks = scope.schoolWeeks;
                $scope.available_terms = scope.schoolTerms;
                $scope.available_years = scope.schoolYears;

                $scope.selected_school = scope.selected_school;

                $scope.reportFormData = {};


                $scope.send = function() {


                    if ($scope.reportFormData.academic_year == null
                        || $scope.reportFormData.academic_term == null
                        || $scope.reportFormData.week_number == null ){

                        scNotifier.notifyInfo('Info', 'Please specify a duration');

                    }
                    else if (!$scope.reportFormData.student_enrollment &&
                        !$scope.reportFormData.student_attendance &&
                        !$scope.reportFormData.teacher_punctuality &&
                        !$scope.reportFormData.records_performance &&
                        !$scope.reportFormData.school_grounds &&
                        !$scope.reportFormData.community_involvement
                    ){
                        scNotifier.notifyInfo('Info', 'Please check at least one option');
                    }
                    else{

                        $scope.reportFormData.school = $scope.selected_school;

                        if ($scope.reportFormData.academic_term == '1st') {
                            $scope.reportFormData.term = 'first_term'
                        }else if ($scope.reportFormData.academic_term == '2nd') {
                            $scope.reportFormData.term = 'second_term'
                        }else if ($scope.reportFormData.academic_term == '3rd') {
                            $scope.reportFormData.term = 'third_term'
                        }


                        Storage.reportParams = $scope.reportFormData;

                        $state.go('dashboard.generate_report', $scope.reportFormData , {location : false, inherit : true, notify : true} );

                        $modalInstance.dismiss('cancel');

                    }

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        }

    }}]);