/**
 * Created by Kaygee on 16/06/2018.
 */


schoolMonitor.directive('chartJsBar', function(scReportGenerator, $timeout){

    var chart;
    function drawChart(labelsArray, labelArray, chartData, target) {
        var ctx = document.getElementById("barChart").getContext("2d");
        chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labelsArray,
                datasets: [{
                    label: "Pupil Enrollment",
                    data: chartData,
                    backgroundColor: [
                        pattern.draw('square', 'rgba(255, 99, 132, 0.75)'),
                        pattern.draw('circle', 'rgba(54, 162, 235, 0.75)'),
                        pattern.draw('diamond', 'rgba(255, 206, 86, 0.75)'),
                        pattern.draw('zigzag-horizontal', 'rgba(75, 192, 192, 0.75)'),
                        pattern.draw('triangle', 'rgba(153, 102, 255, 0.75)'),
                        pattern.draw('plus', 'rgba(255, 159, 64, 0.75)'),
                        pattern.draw('cross', 'rgba(239, 7, 7, 0.75)'),
                        pattern.draw('dot', 'rgba(31, 29, 171, 0.75)'),
                        pattern.draw('disc', 'rgba(231, 235, 8, 0.75)'),
                        pattern.draw('ring', 'rgba(16, 204, 13, 0.75)'),
                        pattern.draw('weave', 'rgba(119, 9, 146, 0.75)')
                    ]
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Pupil Enrollment'
                },
                legend: {
                    display: false
                    // labels: {
                    //     fontColor: 'rgb(255, 99, 132)'
                    // }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        if (chart){ chart.update();
            console.log("called in if chart after function");
        }else{
            console.log("else chart update in watch");

        }

        scReportGenerator.barChart[target] = chart;
    }

    return {
        scope : true,
        // 'labels' : '=labels',
        // 'label' : '@',
        // 'data' : '=chartJsBar'


        link : function($scope, element, attr){
            drawChart($scope.enrollmentBarChart.labels, $scope.label, $scope.enrollmentBarChart.chartData, "enrollmentBarChart");
            console.log("called in link");

        },
        controller : function ($scope) {
            $scope.$watch($scope.enrollmentBarChart.chartData, function (newVal, oldValue) {
                $timeout(function () {
                    console.log("called in watch in controller");
                    drawChart($scope.enrollmentBarChart.labels, $scope.label, $scope.enrollmentBarChart.chartData, "enrollmentBarChart");
                },1);

            }, true);

            $scope.download = function () {
                if (chart) {
                    console.log(chart.toBase64Image());
                }
            }
        },
        replace : true,
        restrict : 'AE',
        template : '<div class="chart-container" style="position: relative; width:100%">\n' +
        '<canvas id="barChart" width="400" height="300" style="width: 100%; height: 300px"></canvas>\n' +
        // '<button class="btn btn-primary" ng-click="download()">Download</button>'+
        '</div>'
    }
});


/*labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
datasets: [{
    label: '# of Votes',
    data: [12, 19, 3, 5, 2, 3],
          label: 'Line Dataset',
          data: [50, 50, 50, 50],
          // Changes this dataset to become a line
          type: 'line'
    backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1

    Pattern Keys
plus
cross
dash
cross-dash
dot
dot-dash
disc
ring
line
line-vertical
weave
zigzag
zigzag-vertical
diagonal
diagonal-right-left
square
box
triangle
triangle-inverted
diamond
diamond-box
}]*/