/**
 * Created by KayGee on 30-Jun-16.
 */

schoolMonitor.directive('updateDistrict', ['$rootScope', '$modal', 'scSchool','scNotifier','$interval','scUpdateWeeklyTable',
    function ($rootScope, $modal, scSchool, scNotifier, $interval, scUpdateWeeklyTable) {

        var intervalPromise;

        return {
            scope : {
                district : '=updateDistrict',
                years : '='

            },
            link : function (scope, elem, attr) {


                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/updateConfirmModelUpdateModal.html',
                        controller: ['$scope', '$modalInstance', confirmDistrictUpdateModalCtrl],
                        size : 'md',
                        backdrop : 'static'
                    });

                    function confirmDistrictUpdateModalCtrl ($scope, $modalInstance) {


                        $scope.model = {
                            type : 'district',
                            type_proper : 'District',
                            year : ''
                        };

                        $scope.yearsAvailable = scope.years;
                        $scope.selected_model = scope.district;
                        $scope.stepper = 0;
                        $scope.updatingModel = false;



                        $scope.doLikeUpdating = function () {
                            $scope.stepper +=5;
                            $scope.updatingModel +=5;
                            // scUpdateWeeklyTable.updatingModel;
                            // $scope.stepper = scUpdateWeeklyTable.updatingModel;

                            if ($scope.stepper < 25) {
                                $scope.barColor = 'danger';
                            } else if ($scope.stepper < 50) {
                                $scope.barColor = 'warning';
                            } else if ($scope.stepper < 75) {
                                $scope.barColor = 'info';
                            } else {
                                $scope.barColor = 'success';
                            }
                            if ($scope.stepper >= 90) {
                                $scope.updatingModel = false;
                                scNotifier.notifySuccess("Updates will continue to run in the background. Please refresh the page after a few minutes.");
                                $interval.cancel(intervalPromise);
                                $scope.cancel();
                            }
                        };

                        $scope.$on('finishedUpdating', function (event) {
                            $scope.doLikeUpdating()
                        });

                        $scope.update = function () {
                            $scope.stepper = 0;

                            scSchool.updateWeeklyTableAtBackend({
                                scope : 'district',
                                scope_level_id : scope.district.id,
                                year : $scope.model.year
                            }).success(function (returned) {
                                // console.log('returned : ',returned);
                            });

                            /*scUpdateWeeklyTable.runWeeklyUpdateTotalsForModel({
                                district_id : scope.district.id,
                                year : $scope.model.year
                            });
*/
                            intervalPromise = $interval(function () {
                                $scope.doLikeUpdating();
                            }, 1000, 60)
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });
            }
        }

    }]);
