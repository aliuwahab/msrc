/**
 * Created by KayGee on 30-Jun-16.
 */

schoolMonitor.directive('deleteCircuit', ['$rootScope', '$modal', 'scCircuit','scNotifier','$state',
    function ($rootScope, $modal, scCircuit, scNotifier, $state) {

        return {
            scope : {
                deleteCircuit : '@'
            },
            link : function (scope, elem, attr) {
                elem.bind('click', function () {
                    $modal.open({
                        templateUrl: 'partials/miscellaneous/deleteModal.html',
                        controller: ModalInstanceCtrl,
                        size: "sm"
                    });
                });


                // Please note that $modalInstance represents a modal window (instance) dependency.
                // It is not the same as the $modal service used above.

                var ModalInstanceCtrl = function ($scope, $modalInstance) {

                    $scope.deleteObject = scCircuit.circuitLookup[scope.deleteCircuit];
                    $scope.deleteObject.objectType = 'Circuit';

                    $scope.ok = function () {
                        $modalInstance.close('close');
                //            $log.info('Modal ok\'ed at: ' + new Date());
                        scCircuit.deleteCircuit(scope.deleteCircuit).then(
                            function(status){
                                if (status) {
                                    scNotifier.notifySuccess('Success', 'Circuit deleted successfully');
                                    $state.go('dashboard.view_circuit', {} , {reload : true});
                                }else{
                                    scNotifier.notifyFailure('Error', 'There was an error deleting the circuit');
                                }
                            },
                            function(){
                                scNotifier.notifyFailure('Error', 'There was an error deleting the circuit');
                            })
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                        //            $log.info('Modal cancelled at: ' + new Date());
                    };
                };

            }
        }

    }]);


