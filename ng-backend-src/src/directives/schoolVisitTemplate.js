/**
 * Created by Kaygee on 22/05/2015.
 */
schoolMonitor.directive('visitHtml', [function(){

    return {
        scope : {
            schoolVisitData : '=visitHtml',
            schoolsDistrictItinerary : '=',
            data_collector_types : '=collectors',
            momentFormatDate : '&date'
        },

        compile : function (compElem, compAttr) {
            return  function($scope, elem, attrs){

                $scope.filteredSchVisitData = [];
                var tempObj = {};
                var question_category_reference =attrs.questionCategoryReference;
                var question_code_base = attrs.questionCodeBase;
                var num_questions = attrs.numQuestions;


                for (var questionObject = 0; questionObject < $scope.schoolVisitData.length; questionObject++) {

                    if ($scope.schoolVisitData[questionObject].question_category_reference == question_category_reference) {

                        for (var holder = 1; holder <= num_questions; holder++) {
                            if ($scope.schoolVisitData[questionObject].question_code == question_code_base + holder) {
                                tempObj['visitQuestion' + holder] = {
                                    question : $scope.schoolVisitData[questionObject].question
                                }
                            }
                        }

                        for (var answerObject = 0;
                             answerObject < $scope.schoolVisitData[questionObject].school_visit_answers.length; answerObject ++ ) {

                            for (var i = 1; i <= num_questions; i++) {

                                if ($scope.schoolVisitData[questionObject].question_code == question_code_base + i) {

                                    if (  tempObj['visitQuestion' + i].answers) {
                                        tempObj['visitQuestion' + i].answers.push($scope.schoolVisitData[questionObject].school_visit_answers[answerObject]);
                                    }else{
                                        tempObj['visitQuestion' + i].answers = [ $scope.schoolVisitData[questionObject].school_visit_answers[answerObject] ];
                                    }
                                }
                            }
                        }
                    }
                }

                for (var pusher = 1; pusher <= num_questions; pusher++) {
                    $scope.filteredSchVisitData.push(  tempObj['visitQuestion' + pusher] )
                }

            }

        },

        controller : function ($scope) {
            $scope.showFirstIndex = function (index) {
                return index != 0
            };
            $scope.filter_collector = 'head_teacher';
            $scope.filter_itinerary = '';
            if ($scope.schoolsDistrictItinerary.itinerary.length) {
                $scope.filter_itinerary = $scope.schoolsDistrictItinerary.itinerary[0].id;
            }

            $scope.removeTrailingComma = function (text) {
                var trimmed = text;
                if (text && text.length) {
                    trimmed =  $.trim(text.split(',')[0])
                }
                return trimmed;
            }
        },

        templateUrl : 'partials/templates_View/school/sch_visit_template.html'
    }


}]);
