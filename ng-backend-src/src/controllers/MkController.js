schoolMonitor.controller('submitAttendanceDistrictGraphFormController', ['$stateParams', 'scDistrict', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scDistrict, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {


            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];


            $scope.formData.url_id = id;


            $http({
                method: 'POST',
                url: '/save/AttendanceDistrictGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {




                var attendanceData = response.data;

                if(attendanceData.length>0) {
                const Beginner = [];
                const Letter = [];
                const Paragraph = [];
                const Sentence = [];
                const Word = [];

                const One = [];
                const Two = [];
                const Addition = [];
                const Subtraction = [];


                attendanceData.forEach(function (data) {

                    Beginner.push(data.Beginner);
                    Letter.push(data.Letter);
                    Paragraph.push(data.Paragraph);
                    Sentence.push(data.Sentence);
                    Word.push(data.Word);

                    One.push(data.One);
                    Two.push(data.Two);
                    Addition.push(data.Addition);
                    Subtraction.push(data.Subtraction);

                });


                if (attendanceData[0].Letter) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }

//


                    //////////////////


                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }


                /////


                if (attendanceData[0].One) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }
//
// //
//
//
//                     //////////////////
//
//
                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }
                scNotifier.notifySuccess(" Selection Successful ");
            }else{

                scNotifier.notifyInfo("No data currently available");


                    // if (attendanceData[0].Letter) {
                    //
                    //     if (attendanceData[0].Grade == 'P4') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        // }


                        // if (attendanceData[0].Grade == 'P5') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        // }

//


                        //////////////////


                        // if (attendanceData[0].Grade == 'P6') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        // }


                    // }


                    /////


                    // if (attendanceData[0].One) {
                    //
                    //     if (attendanceData[0].Grade == 'P4') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        // }


                        // if (attendanceData[0].Grade == 'P5') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        // }
//

                        // if (attendanceData[0].Grade == 'P6') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                    //     }
                    //
                    //
                    // }


            }
                $window.location.href;

            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server." +
                    "Please try again or  contact the system administrator.");

            });
        };

    }]);


schoolMonitor.controller('submitAttendanceRegionGraphFormController', ['$stateParams', 'scRegion', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scRegion, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {


            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];


            $scope.formData.url_id = id;


            $http({
                method: 'POST',
                url: '/save/AttendanceRegionGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {




                var attendanceData = response.data;
                if(attendanceData.length>0) {

                const Beginner = [];
                const Letter = [];
                const Paragraph = [];
                const Sentence = [];
                const Word = [];

                const One = [];
                const Two = [];
                const Addition = [];
                const Subtraction = [];


                attendanceData.forEach(function (data) {

                    Beginner.push(data.Beginner);
                    Letter.push(data.Letter);
                    Paragraph.push(data.Paragraph);
                    Sentence.push(data.Sentence);
                    Word.push(data.Word);

                    One.push(data.One);
                    Two.push(data.Two);
                    Addition.push(data.Addition);
                    Subtraction.push(data.Subtraction);

                });


                if (attendanceData[0].Letter) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }

//


                    //////////////////


                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }


                /////


                if (attendanceData[0].One) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }
//

                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }
                scNotifier.notifySuccess(" Selection Successful ");
            }else{



                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });





                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });




//


                        //////////////////




                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });







                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });






                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });



//



                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                scNotifier.notifyInfo("No data currently available");



            }

                $window.location.href;
            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };
    }]);

schoolMonitor.controller('submitAttendanceCircuitGraphFormController', ['$stateParams', 'scCircuit', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scCircuit, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {


            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];


            $scope.formData.urlid = id;


            $http({
                method: 'POST',
                url: '/save/AttendanceCircuitGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {


                var attendanceData = response.data;
                if(attendanceData.length>0) {

                const Beginner = [];
                const Letter = [];
                const Paragraph = [];
                const Sentence = [];
                const Word = [];

                const One = [];
                const Two = [];
                const Addition = [];
                const Subtraction = [];


                attendanceData.forEach(function (data) {

                    Beginner.push(data.Beginner);
                    Letter.push(data.Letter);
                    Paragraph.push(data.Paragraph);
                    Sentence.push(data.Sentence);
                    Word.push(data.Word);

                    One.push(data.One);
                    Two.push(data.Two);
                    Addition.push(data.Addition);
                    Subtraction.push(data.Subtraction);

                });


                if (attendanceData[0].Letter) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }



                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph ",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }


                if (attendanceData[0].One) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }
//
// //
//
//
//                     //////////////////
//
//
                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }

                scNotifier.notifySuccess(" Selection Successful ");
            }else{

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });



                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph ",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });






                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });






                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });



                scNotifier.notifyInfo("No data currently available");
            }
                $window.location.href;


            }, function errorCallback(response) {


                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };
    }]);


schoolMonitor.controller('submitAttendanceSchoolGraphFormController', ['$stateParams', 'scSchool', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scSchool, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {

            var mylength = window.location.href.split("/").length;

            var id = window.location.href.split("/")[mylength - 1];

            $scope.formData.urlid = id;
            $http({
                method: 'POST',
                url: '/save/AttendanceSchoolGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {





                var attendanceData = response.data;
                if(attendanceData.length>0) {

                const Beginner = [];
                const Letter = [];
                const Paragraph = [];
                const Sentence = [];
                const Word = [];

                const One = [];
                const Two = [];
                const Addition = [];
                const Subtraction = [];


                attendanceData.forEach(function (data) {

                    Beginner.push(data.Beginner);
                    Letter.push(data.Letter);
                    Paragraph.push(data.Paragraph);
                    Sentence.push(data.Sentence);
                    Word.push(data.Word);

                    One.push(data.One);
                    Two.push(data.Two);
                    Addition.push(data.Addition);
                    Subtraction.push(data.Subtraction);

                });


                if (attendanceData[0].Letter) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }

//


                    //////////////////


                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "Letter",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": Letter || One
                                },
                                {
                                    "label": "Word",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Word || Two
                                },
                                {
                                    "label": "Story",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Sentence || Addition
                                },
                                {
                                    "label": "Paragraph",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Paragraph || Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }



                if (attendanceData[0].One) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }
//
// //
//
//
//                     //////////////////
//
//
                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }


                scNotifier.notifySuccess(" Selection Successful ");
            }else{



                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });







                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });












                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                    /////



                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });




                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });



                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });



                    scNotifier.notifyInfo("No data currently available");
            }
                $window.location.href;


            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };
    }]);

schoolMonitor.controller('submitPupilAggregatedDataSchoolGraphFormController', ['$stateParams', 'scSchool', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scSchool, scNotifier, $scope, $http, $window) {
        $scope.submitForm = function () {


            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];


            $scope.formData.urlid = id;



            $http({
                method: 'POST',
                url: '/save/PupilAggregatedDataSchoolGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {



                var PupilAggregatedData = response.data;

                    if(PupilAggregatedData.length>0) {


                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "w1",
                                "w2",
                                "w3",
                                "w4",
                                "w5",
                                "w6",
                                "w7",
                                "w8"
                            ],
                            "datasets": PupilAggregatedData
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Attendance Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "top"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10
                            }
                        };

                        new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });

                        scNotifier.notifySuccess(" Selection Successful ");
                    }else{


                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "w1",
                                "w2",
                                "w3",
                                "w4",
                                "w5",
                                "w6",
                                "w7",
                                "w8"
                            ],
                            "datasets": []
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Attendance Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "top"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10
                            }
                        };

                        new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });
                    }

                scNotifier.notifyInfo("No data currently available");
                $window.location.href;

            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });


        };
    }]);

schoolMonitor.controller('submitAttendanceCountryGraphFormController', ['scSchool', 'scNotifier', '$scope', '$http', '$window',
    function (scDistrict, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {


            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];


            $scope.formData.url_id = id;


            $http({
                method: 'POST',
                url: '/save/AttendanceCountryGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {



                var attendanceData = response.data;
                if(attendanceData.length>0) {

                const Beginner = [];
                const Letter = [];
                const Paragraph = [];
                const Sentence = [];
                const Word = [];

                const One = [];
                const Two = [];
                const Addition = [];
                const Subtraction = [];


                attendanceData.forEach(function (data) {

                    Beginner.push(data.Beginner);
                    Letter.push(data.Letter);
                    Paragraph.push(data.Paragraph);
                    Sentence.push(data.Sentence);
                    Word.push(data.Word);

                    One.push(data.One);
                    Two.push(data.Two);
                    Addition.push(data.Addition);
                    Subtraction.push(data.Subtraction);

                });




                    if (attendanceData[0].Letter) {

                        if (attendanceData[0].Grade == 'P4') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": Beginner,
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": Letter
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": Word
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": Sentence
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": Paragraph
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }


                        if (attendanceData[0].Grade == 'P5') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": Beginner,
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": Letter || One
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": Word || Two
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": Sentence || Addition
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": Paragraph || Subtraction
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }

//


                        //////////////////


                        if (attendanceData[0].Grade == 'P6') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": Beginner,
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": Letter || One
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": Word || Two
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": Sentence || Addition
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": Paragraph || Subtraction
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }


                    }


                /////


                if (attendanceData[0].One) {

                    if (attendanceData[0].Grade == 'P4') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P4"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                    if (attendanceData[0].Grade == 'P5') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P5"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }

//


                    //////////////////


                    if (attendanceData[0].Grade == 'P6') {

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var data = {
                            "labels": [
                                "P6"
                            ],
                            "datasets": [
                                {
                                    "label": "Beginner",
                                    "backgroundColor": "#ff0000",
                                    "fill": true,
                                    "data": Beginner,
                                    "borderColor": "#ffffff",
                                    "borderWidth": "1"
                                },
                                {
                                    "label": "One",
                                    "backgroundColor": "#0080ff",
                                    "fill": true,
                                    "data": One
                                },
                                {
                                    "label": "Two",
                                    "backgroundColor": "#ffff00",
                                    "fill": true,
                                    "data": Two
                                },
                                {
                                    "label": "Addition",
                                    "backgroundColor": "#00ff40",
                                    "fill": true,
                                    "data": Addition
                                },
                                {
                                    "label": "Subtraction",
                                    "backgroundColor": "#8000ff",
                                    "fill": true,
                                    "data": Subtraction
                                }
                            ]
                        };
                        var options = {
                            "title": {
                                "display": true,
                                "text": "Pupil Aggregated Data Graph",
                                "position": "bottom",
                                "fullWidth": true,
                                "fontColor": "#aa7942",
                                "fontSize": 16
                            },
                            "legend": {
                                "display": true,
                                "fullWidth": true,
                                "position": "bottom"
                            },
                            "scales": {
                                "yAxes": [
                                    {
                                        "ticks": {
                                            "beginAtZero": true,
                                            "display": true
                                        },
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": true,
                                            "drawTicks": true,
                                            "tickMarkLength": 1,
                                            "offsetGridLines": true,
                                            "zeroLineColor": "#942192",
                                            "color": "#d6d6d6",
                                            "zeroLineWidth": 2
                                        },
                                        "scaleLabel": {
                                            "display": true,
                                            "labelString": ""
                                        },
                                        "display": true
                                    }
                                ],
                                "xAxes": {
                                    "0": {
                                        "ticks": {
                                            "display": true,
                                            "fontSize": 14,
                                            "fontStyle": "italic"
                                        },
                                        "display": true,
                                        "gridLines": {
                                            "display": true,
                                            "lineWidth": 2,
                                            "drawOnChartArea": false,
                                            "drawTicks": true,
                                            "tickMarkLength": 12,
                                            "zeroLineWidth": 2,
                                            "offsetGridLines": true,
                                            "color": "#942192",
                                            "zeroLineColor": "#942192"
                                        },
                                        "scaleLabel": {
                                            "fontSize": 16,
                                            "display": true,
                                            "fontStyle": "normal"
                                        }
                                    }
                                }
                            },
                            "tooltips": {
                                "enabled": true,
                                "mode": "label",
                                "caretSize": 10,
                                "backgroundColor": "#00fa92"
                            }
                        };

                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });


                    }


                }


                var ctx = document.getElementById('myCharttwo').getContext('2d');
                var data = {
                    "labels": [
                        "Beginner",
                        "Letter",
                        "Word",
                        "Paragraph",
                        "Story"
                    ],
                    "datasets": [
                        {
                            "label": "Beginner",
                            "backgroundColor": ['Red', 'Blue', 'Yellow', 'Green', 'Purple'],
                            "fill": true,
                            "data": [],
                            "borderColor": "#ffffff",
                            "borderWidth": "1"
                        },
                        {
                            "label": "Letter",
                            "backgroundColor": "#407aaa",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Word",
                            "backgroundColor": "#ffff00",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Paragraph",
                            "backgroundColor": "#008040",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Story",
                            "backgroundColor": "#800080",
                            "fill": true,
                            "data": []
                        }
                    ]
                };
                var options = {
                    "title": {
                        "display": true,
                        "text": "P4 English Analytics",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#400000",
                        "fontSize": 16
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "bottom"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10,
                        "backgroundColor": "#00fa92"
                    }
                };

                var myCharttwo = new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: options
                });


                //////////////////

                var ctx = document.getElementById('myChartthree').getContext('2d');
                var data = {
                    "labels": [
                        "Beginner",
                        "Letter",
                        "Word",
                        "Paragraph",
                        "Story"
                    ],
                    "datasets": [
                        {
                            "label": "Beginner",
                            "backgroundColor": ['Red', 'Blue', 'Yellow', 'Green', 'Purple'],
                            "fill": true,
                            "data": [],
                            "borderColor": "#ffffff",
                            "borderWidth": "1"
                        },
                        {
                            "label": "Letter",
                            "backgroundColor": "#407aaa",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Word",
                            "backgroundColor": "#ffff00",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Paragrapgh",
                            "backgroundColor": "#008040",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Story",
                            "backgroundColor": "#800080",
                            "fill": true,
                            "data": []
                        }
                    ]
                };
                var options = {
                    "title": {
                        "display": true,
                        "text": "P5 English Analytics",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#400000",
                        "fontSize": 16
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "bottom"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10,
                        "backgroundColor": "#00fa92"
                    }
                };

                var myChartthree = new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: options
                });


                ////////////

                var ctx = document.getElementById('myChartfour').getContext('2d');
                var data = {
                    "labels": [
                        "Beginner",
                        "Letter",
                        "Word",
                        "Paragraph",
                        "Story"
                    ],
                    "datasets": [
                        {
                            "label": "Beginner",
                            "backgroundColor": ['Red', 'Blue', 'Yellow', 'Green', 'Purple'],
                            "fill": true,
                            "data": [],
                            "borderColor": "#ffffff",
                            "borderWidth": "1"
                        },
                        {
                            "label": "Letter",
                            "backgroundColor": "#808000",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Word",
                            "backgroundColor": "#ff0000",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Paragraph",
                            "backgroundColor": "#800080",
                            "fill": true,
                            "data": []
                        },
                        {
                            "label": "Story",
                            "backgroundColor": "#8080ff",
                            "fill": true,
                            "data": []
                        }
                    ]
                };
                var options = {
                    "title": {
                        "display": true,
                        "text": "P6 Math Analytics",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#000000",
                        "fontSize": 14
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "bottom"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10,
                        "backgroundColor": "#00fa92"
                    }
                };

                var myChartfour = new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: options
                });

                scNotifier.notifySuccess(" Selection Successful ");
            }else{




                    if (attendanceData[0].Letter) {

                        if (attendanceData[0].Grade == 'P4') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }


                        if (attendanceData[0].Grade == 'P5') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }

//


                        //////////////////


                        if (attendanceData[0].Grade == 'P6') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "Letter",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Word",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Story",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Paragraph",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }


                    }


                    /////


                    if (attendanceData[0].One) {

                        if (attendanceData[0].Grade == 'P4') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P4"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }


                        if (attendanceData[0].Grade == 'P5') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P5"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }

//


                        //////////////////


                        if (attendanceData[0].Grade == 'P6') {

                            var ctx = document.getElementById('myChart').getContext('2d');
                            var data = {
                                "labels": [
                                    "P6"
                                ],
                                "datasets": [
                                    {
                                        "label": "Beginner",
                                        "backgroundColor": "#ff0000",
                                        "fill": true,
                                        "data": [],
                                        "borderColor": "#ffffff",
                                        "borderWidth": "1"
                                    },
                                    {
                                        "label": "One",
                                        "backgroundColor": "#0080ff",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Two",
                                        "backgroundColor": "#ffff00",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Addition",
                                        "backgroundColor": "#00ff40",
                                        "fill": true,
                                        "data": []
                                    },
                                    {
                                        "label": "Subtraction",
                                        "backgroundColor": "#8000ff",
                                        "fill": true,
                                        "data": []
                                    }
                                ]
                            };
                            var options = {
                                "title": {
                                    "display": true,
                                    "text": "Pupil Aggregated Data Graph",
                                    "position": "bottom",
                                    "fullWidth": true,
                                    "fontColor": "#aa7942",
                                    "fontSize": 16
                                },
                                "legend": {
                                    "display": true,
                                    "fullWidth": true,
                                    "position": "bottom"
                                },
                                "scales": {
                                    "yAxes": [
                                        {
                                            "ticks": {
                                                "beginAtZero": true,
                                                "display": true
                                            },
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": true,
                                                "drawTicks": true,
                                                "tickMarkLength": 1,
                                                "offsetGridLines": true,
                                                "zeroLineColor": "#942192",
                                                "color": "#d6d6d6",
                                                "zeroLineWidth": 2
                                            },
                                            "scaleLabel": {
                                                "display": true,
                                                "labelString": ""
                                            },
                                            "display": true
                                        }
                                    ],
                                    "xAxes": {
                                        "0": {
                                            "ticks": {
                                                "display": true,
                                                "fontSize": 14,
                                                "fontStyle": "italic"
                                            },
                                            "display": true,
                                            "gridLines": {
                                                "display": true,
                                                "lineWidth": 2,
                                                "drawOnChartArea": false,
                                                "drawTicks": true,
                                                "tickMarkLength": 12,
                                                "zeroLineWidth": 2,
                                                "offsetGridLines": true,
                                                "color": "#942192",
                                                "zeroLineColor": "#942192"
                                            },
                                            "scaleLabel": {
                                                "fontSize": 16,
                                                "display": true,
                                                "fontStyle": "normal"
                                            }
                                        }
                                    }
                                },
                                "tooltips": {
                                    "enabled": true,
                                    "mode": "label",
                                    "caretSize": 10,
                                    "backgroundColor": "#00fa92"
                                }
                            };

                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: data,
                                options: options
                            });


                        }


                    }


                    var ctx = document.getElementById('myCharttwo').getContext('2d');
                    var data = {
                        "labels": [
                            "Beginner",
                            "Letter",
                            "Word",
                            "Paragraph",
                            "Story"
                        ],
                        "datasets": [
                            {
                                "label": "Beginner",
                                "backgroundColor": ['Red', 'Blue', 'Yellow', 'Green', 'Purple'],
                                "fill": true,
                                "data": [],
                                "borderColor": "#ffffff",
                                "borderWidth": "1"
                            },
                            {
                                "label": "Letter",
                                "backgroundColor": "#407aaa",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Word",
                                "backgroundColor": "#ffff00",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Paragraph",
                                "backgroundColor": "#008040",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Story",
                                "backgroundColor": "#800080",
                                "fill": true,
                                "data": []
                            }
                        ]
                    };
                    var options = {
                        "title": {
                            "display": true,
                            "text": "P4 English Analytics",
                            "position": "bottom",
                            "fullWidth": true,
                            "fontColor": "#400000",
                            "fontSize": 16
                        },
                        "legend": {
                            "display": true,
                            "fullWidth": true,
                            "position": "bottom"
                        },
                        "scales": {
                            "yAxes": [
                                {
                                    "ticks": {
                                        "beginAtZero": true,
                                        "display": true
                                    },
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": true,
                                        "drawTicks": true,
                                        "tickMarkLength": 1,
                                        "offsetGridLines": true,
                                        "zeroLineColor": "#942192",
                                        "color": "#d6d6d6",
                                        "zeroLineWidth": 2
                                    },
                                    "scaleLabel": {
                                        "display": true,
                                        "labelString": ""
                                    },
                                    "display": true
                                }
                            ],
                            "xAxes": {
                                "0": {
                                    "ticks": {
                                        "display": true,
                                        "fontSize": 14,
                                        "fontStyle": "italic"
                                    },
                                    "display": true,
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": false,
                                        "drawTicks": true,
                                        "tickMarkLength": 12,
                                        "zeroLineWidth": 2,
                                        "offsetGridLines": true,
                                        "color": "#942192",
                                        "zeroLineColor": "#942192"
                                    },
                                    "scaleLabel": {
                                        "fontSize": 16,
                                        "display": true,
                                        "fontStyle": "normal"
                                    }
                                }
                            }
                        },
                        "tooltips": {
                            "enabled": true,
                            "mode": "label",
                            "caretSize": 10,
                            "backgroundColor": "#00fa92"
                        }
                    };

                    var myCharttwo = new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
                    });


                    //////////////////

                    var ctx = document.getElementById('myChartthree').getContext('2d');
                    var data = {
                        "labels": [
                            "Beginner",
                            "Letter",
                            "Word",
                            "Paragraph",
                            "Story"
                        ],
                        "datasets": [
                            {
                                "label": "Beginner",
                                "backgroundColor": ['Red', 'Blue', 'Yellow', 'Green', 'Purple'],
                                "fill": true,
                                "data": [],
                                "borderColor": "#ffffff",
                                "borderWidth": "1"
                            },
                            {
                                "label": "Letter",
                                "backgroundColor": "#407aaa",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Word",
                                "backgroundColor": "#ffff00",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Paragrapgh",
                                "backgroundColor": "#008040",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Story",
                                "backgroundColor": "#800080",
                                "fill": true,
                                "data": []
                            }
                        ]
                    };
                    var options = {
                        "title": {
                            "display": true,
                            "text": "P5 English Analytics",
                            "position": "bottom",
                            "fullWidth": true,
                            "fontColor": "#400000",
                            "fontSize": 16
                        },
                        "legend": {
                            "display": true,
                            "fullWidth": true,
                            "position": "bottom"
                        },
                        "scales": {
                            "yAxes": [
                                {
                                    "ticks": {
                                        "beginAtZero": true,
                                        "display": true
                                    },
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": true,
                                        "drawTicks": true,
                                        "tickMarkLength": 1,
                                        "offsetGridLines": true,
                                        "zeroLineColor": "#942192",
                                        "color": "#d6d6d6",
                                        "zeroLineWidth": 2
                                    },
                                    "scaleLabel": {
                                        "display": true,
                                        "labelString": ""
                                    },
                                    "display": true
                                }
                            ],
                            "xAxes": {
                                "0": {
                                    "ticks": {
                                        "display": true,
                                        "fontSize": 14,
                                        "fontStyle": "italic"
                                    },
                                    "display": true,
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": false,
                                        "drawTicks": true,
                                        "tickMarkLength": 12,
                                        "zeroLineWidth": 2,
                                        "offsetGridLines": true,
                                        "color": "#942192",
                                        "zeroLineColor": "#942192"
                                    },
                                    "scaleLabel": {
                                        "fontSize": 16,
                                        "display": true,
                                        "fontStyle": "normal"
                                    }
                                }
                            }
                        },
                        "tooltips": {
                            "enabled": true,
                            "mode": "label",
                            "caretSize": 10,
                            "backgroundColor": "#00fa92"
                        }
                    };

                    var myChartthree = new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
                    });


                    ////////////

                    var ctx = document.getElementById('myChartfour').getContext('2d');
                    var data = {
                        "labels": [
                            "Beginner",
                            "Letter",
                            "Word",
                            "Paragraph",
                            "Story"
                        ],
                        "datasets": [
                            {
                                "label": "Beginner",
                                "backgroundColor": ['Red', 'Blue', 'Yellow', 'Green', 'Purple'],
                                "fill": true,
                                "data": [],
                                "borderColor": "#ffffff",
                                "borderWidth": "1"
                            },
                            {
                                "label": "Letter",
                                "backgroundColor": "#808000",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Word",
                                "backgroundColor": "#ff0000",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Paragraph",
                                "backgroundColor": "#800080",
                                "fill": true,
                                "data": []
                            },
                            {
                                "label": "Story",
                                "backgroundColor": "#8080ff",
                                "fill": true,
                                "data": []
                            }
                        ]
                    };
                    var options = {
                        "title": {
                            "display": true,
                            "text": "P6 Math Analytics",
                            "position": "bottom",
                            "fullWidth": true,
                            "fontColor": "#000000",
                            "fontSize": 14
                        },
                        "legend": {
                            "display": true,
                            "fullWidth": true,
                            "position": "bottom"
                        },
                        "scales": {
                            "yAxes": [
                                {
                                    "ticks": {
                                        "beginAtZero": true,
                                        "display": true
                                    },
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": true,
                                        "drawTicks": true,
                                        "tickMarkLength": 1,
                                        "offsetGridLines": true,
                                        "zeroLineColor": "#942192",
                                        "color": "#d6d6d6",
                                        "zeroLineWidth": 2
                                    },
                                    "scaleLabel": {
                                        "display": true,
                                        "labelString": ""
                                    },
                                    "display": true
                                }
                            ],
                            "xAxes": {
                                "0": {
                                    "ticks": {
                                        "display": true,
                                        "fontSize": 14,
                                        "fontStyle": "italic"
                                    },
                                    "display": true,
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": false,
                                        "drawTicks": true,
                                        "tickMarkLength": 12,
                                        "zeroLineWidth": 2,
                                        "offsetGridLines": true,
                                        "color": "#942192",
                                        "zeroLineColor": "#942192"
                                    },
                                    "scaleLabel": {
                                        "fontSize": 16,
                                        "display": true,
                                        "fontStyle": "normal"
                                    }
                                }
                            }
                        },
                        "tooltips": {
                            "enabled": true,
                            "mode": "label",
                            "caretSize": 10,
                            "backgroundColor": "#00fa92"
                        }
                    };

                    var myChartfour = new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
                    });
            }

                scNotifier.notifyInfo("No data currently available");
                $window.location.href;
            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };


    }]);





schoolMonitor.controller('submitPupilAggregatedDataRegionGraphFormController', ['$stateParams', 'scRegion', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scRegion, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {

            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];


            $scope.formData.urlid = id;


            $http({
                method: 'POST',
                url: '/save/PupilAggregatedDataRegionGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {



                $scope.selected_region = scRegion.regionLookup[$stateParams.id];



                var PupilAggregatedData = response.data;

                if(PupilAggregatedData.length>0) {

                var ctx = document.getElementById('myChart').getContext('2d');
                var data = {
                    "labels": [
                        "w1",
                        "w2",
                        "w3",
                        "w4",
                        "w5",
                        "w6",
                        "w7",
                        "w8"
                    ],
                    "datasets": PupilAggregatedData
                };
                var options = {
                    "title": {
                        "display": true,
                        "text": "Attendance Graph",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#aa7942",
                        "fontSize": 16
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "top"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10
                    }
                };

                new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: options
                });

                scNotifier.notifySuccess(" Selection Successful ");
            }else{
                    var ctx = document.getElementById('myChart').getContext('2d');
                    var data = {
                        "labels": [
                            "w1",
                            "w2",
                            "w3",
                            "w4",
                            "w5",
                            "w6",
                            "w7",
                            "w8"
                        ],
                        "datasets":[]
                    };
                    var options = {
                        "title": {
                            "display": true,
                            "text": "Attendance Graph",
                            "position": "bottom",
                            "fullWidth": true,
                            "fontColor": "#aa7942",
                            "fontSize": 16
                        },
                        "legend": {
                            "display": true,
                            "fullWidth": true,
                            "position": "top"
                        },
                        "scales": {
                            "yAxes": [
                                {
                                    "ticks": {
                                        "beginAtZero": true,
                                        "display": true
                                    },
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": true,
                                        "drawTicks": true,
                                        "tickMarkLength": 1,
                                        "offsetGridLines": true,
                                        "zeroLineColor": "#942192",
                                        "color": "#d6d6d6",
                                        "zeroLineWidth": 2
                                    },
                                    "scaleLabel": {
                                        "display": true,
                                        "labelString": ""
                                    },
                                    "display": true
                                }
                            ],
                            "xAxes": {
                                "0": {
                                    "ticks": {
                                        "display": true,
                                        "fontSize": 14,
                                        "fontStyle": "italic"
                                    },
                                    "display": true,
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": false,
                                        "drawTicks": true,
                                        "tickMarkLength": 12,
                                        "zeroLineWidth": 2,
                                        "offsetGridLines": true,
                                        "color": "#942192",
                                        "zeroLineColor": "#942192"
                                    },
                                    "scaleLabel": {
                                        "fontSize": 16,
                                        "display": true,
                                        "fontStyle": "normal"
                                    }
                                }
                            }
                        },
                        "tooltips": {
                            "enabled": true,
                            "mode": "label",
                            "caretSize": 10
                        }
                    };

                    new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
                    });

                scNotifier.notifyInfo("No data currently available");

            }
                $window.location.href;

            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };
    }]);

schoolMonitor.controller('submitPupilAggregatedDataCircuitGraphFormController', ['$stateParams', 'scCircuit', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scCircuit, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {

            var mylength = window.location.href.split("/").length;

            var id = window.location.href.split("/")[mylength - 1];

            $scope.formData.urlid = id;


            $http({
                method: 'POST',
                url: '/save/PupilAggregatedDataCircuitGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {




                var PupilAggregatedData = response.data;
                if(PupilAggregatedData.length>0) {

                var ctx = document.getElementById('myChart').getContext('2d');
                var data = {
                    "labels": [
                        "w1",
                        "w2",
                        "w3",
                        "w4",
                        "w5",
                        "w6",
                        "w7",
                        "w8"
                    ],
                    "datasets": PupilAggregatedData
                };
                var options = {
                    "title": {
                        "display": true,
                        "text": "Attendance Graph",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#aa7942",
                        "fontSize": 16
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "top"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10
                    }
                };

                new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: options
                });

                scNotifier.notifySuccess(" Selection Successful ");
            }else{

                    var ctx = document.getElementById('myChart').getContext('2d');
                    var data = {
                        "labels": [
                            "w1",
                            "w2",
                            "w3",
                            "w4",
                            "w5",
                            "w6",
                            "w7",
                            "w8"
                        ],
                        "datasets": []
                    };
                    var options = {
                        "title": {
                            "display": true,
                            "text": "Attendance Graph",
                            "position": "bottom",
                            "fullWidth": true,
                            "fontColor": "#aa7942",
                            "fontSize": 16
                        },
                        "legend": {
                            "display": true,
                            "fullWidth": true,
                            "position": "top"
                        },
                        "scales": {
                            "yAxes": [
                                {
                                    "ticks": {
                                        "beginAtZero": true,
                                        "display": true
                                    },
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": true,
                                        "drawTicks": true,
                                        "tickMarkLength": 1,
                                        "offsetGridLines": true,
                                        "zeroLineColor": "#942192",
                                        "color": "#d6d6d6",
                                        "zeroLineWidth": 2
                                    },
                                    "scaleLabel": {
                                        "display": true,
                                        "labelString": ""
                                    },
                                    "display": true
                                }
                            ],
                            "xAxes": {
                                "0": {
                                    "ticks": {
                                        "display": true,
                                        "fontSize": 14,
                                        "fontStyle": "italic"
                                    },
                                    "display": true,
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": false,
                                        "drawTicks": true,
                                        "tickMarkLength": 12,
                                        "zeroLineWidth": 2,
                                        "offsetGridLines": true,
                                        "color": "#942192",
                                        "zeroLineColor": "#942192"
                                    },
                                    "scaleLabel": {
                                        "fontSize": 16,
                                        "display": true,
                                        "fontStyle": "normal"
                                    }
                                }
                            }
                        },
                        "tooltips": {
                            "enabled": true,
                            "mode": "label",
                            "caretSize": 10
                        }
                    };

                    new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
                    });

                scNotifier.notifyInfo("No data currently available");
            }
                $window.location.href;

            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };
    }]);


schoolMonitor.controller('submitPupilAggregatedDataDistrictGraphFormController', ['$stateParams', 'scDistrict', 'scNotifier', '$scope', '$http', '$window',
    function ($stateParams, scDistrict, scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {

            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];


            $scope.formData.urlid = id;


            $http({
                method: 'POST',
                url: '/save/PupilAggregatedDataDistrictGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {


                $scope.selected_District = scDistrict.districtLookup[$stateParams.id];




                var PupilAggregatedData = response.data;
                if(PupilAggregatedData.length>0) {

                var ctx = document.getElementById('myChart').getContext('2d');
                var data = {
                    "labels": [
                        "w1",
                        "w2",
                        "w3",
                        "w4",
                        "w5",
                        "w6",
                        "w7",
                        "w8"
                    ],
                    "datasets": PupilAggregatedData
                };
                var options = {
                    "title": {
                        "display": true,
                        "text": "Attendance Graph",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#aa7942",
                        "fontSize": 16
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "top"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10
                    }
                };

                new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: options
                });

                scNotifier.notifySuccess(" Selection Successful ");
            }else{

                    var ctx = document.getElementById('myChart').getContext('2d');
                    var data = {
                        "labels": [
                            "w1",
                            "w2",
                            "w3",
                            "w4",
                            "w5",
                            "w6",
                            "w7",
                            "w8"
                        ],
                        "datasets": []
                    };
                    var options = {
                        "title": {
                            "display": true,
                            "text": "Attendance Graph",
                            "position": "bottom",
                            "fullWidth": true,
                            "fontColor": "#aa7942",
                            "fontSize": 16
                        },
                        "legend": {
                            "display": true,
                            "fullWidth": true,
                            "position": "top"
                        },
                        "scales": {
                            "yAxes": [
                                {
                                    "ticks": {
                                        "beginAtZero": true,
                                        "display": true
                                    },
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": true,
                                        "drawTicks": true,
                                        "tickMarkLength": 1,
                                        "offsetGridLines": true,
                                        "zeroLineColor": "#942192",
                                        "color": "#d6d6d6",
                                        "zeroLineWidth": 2
                                    },
                                    "scaleLabel": {
                                        "display": true,
                                        "labelString": ""
                                    },
                                    "display": true
                                }
                            ],
                            "xAxes": {
                                "0": {
                                    "ticks": {
                                        "display": true,
                                        "fontSize": 14,
                                        "fontStyle": "italic"
                                    },
                                    "display": true,
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": false,
                                        "drawTicks": true,
                                        "tickMarkLength": 12,
                                        "zeroLineWidth": 2,
                                        "offsetGridLines": true,
                                        "color": "#942192",
                                        "zeroLineColor": "#942192"
                                    },
                                    "scaleLabel": {
                                        "fontSize": 16,
                                        "display": true,
                                        "fontStyle": "normal"
                                    }
                                }
                            }
                        },
                        "tooltips": {
                            "enabled": true,
                            "mode": "label",
                            "caretSize": 10
                        }
                    };

                    new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
                    });

                    scNotifier.notifyInfo("No data currently available");
            }
                $window.location.href;


            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };
    }]);



schoolMonitor.controller('submitPupilAggregatedDataCountryGraphFormController', ['scNotifier', '$scope', '$http', '$window',
    function (scNotifier, $scope, $http, $window) {

        $scope.submitForm = function () {


            $http({
                method: 'POST',
                url: '/save/PupilAggregatedDataCountryGraphForm',
                data: $scope.formData,

            }).then(function successCallback(response) {


                var attendanceData = response.data;


                if(attendanceData.length>0) {

                var ctx = document.getElementById('myChart').getContext('2d');
                var data = {
                    "labels": [
                        "w1",
                        "w2",
                        "w3",
                        "w4",
                        "w5",
                        "w6",
                        "w7",
                        "w8"
                    ],
                    "datasets": attendanceData
                };
                var options = {
                    "title": {
                        "display": true,
                        "text": "Attendance Graph",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#aa7942",
                        "fontSize": 16
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "top"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10
                    }
                };

                new Chart(ctx, {
                    type: 'bar',
                    data: data,
                    options: options
                });

                    scNotifier.notifySuccess(" Selection Successful ");
                }else{
                    var ctx = document.getElementById('myChart').getContext('2d');
                    var data = {
                        "labels": [
                            "w1",
                            "w2",
                            "w3",
                            "w4",
                            "w5",
                            "w6",
                            "w7",
                            "w8"
                        ],
                        "datasets": []
                    };
                    var options = {
                        "title": {
                            "display": true,
                            "text": "Attendance Graph",
                            "position": "bottom",
                            "fullWidth": true,
                            "fontColor": "#aa7942",
                            "fontSize": 16
                        },
                        "legend": {
                            "display": true,
                            "fullWidth": true,
                            "position": "top"
                        },
                        "scales": {
                            "yAxes": [
                                {
                                    "ticks": {
                                        "beginAtZero": true,
                                        "display": true
                                    },
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": true,
                                        "drawTicks": true,
                                        "tickMarkLength": 1,
                                        "offsetGridLines": true,
                                        "zeroLineColor": "#942192",
                                        "color": "#d6d6d6",
                                        "zeroLineWidth": 2
                                    },
                                    "scaleLabel": {
                                        "display": true,
                                        "labelString": ""
                                    },
                                    "display": true
                                }
                            ],
                            "xAxes": {
                                "0": {
                                    "ticks": {
                                        "display": true,
                                        "fontSize": 14,
                                        "fontStyle": "italic"
                                    },
                                    "display": true,
                                    "gridLines": {
                                        "display": true,
                                        "lineWidth": 2,
                                        "drawOnChartArea": false,
                                        "drawTicks": true,
                                        "tickMarkLength": 12,
                                        "zeroLineWidth": 2,
                                        "offsetGridLines": true,
                                        "color": "#942192",
                                        "zeroLineColor": "#942192"
                                    },
                                    "scaleLabel": {
                                        "fontSize": 16,
                                        "display": true,
                                        "fontStyle": "normal"
                                    }
                                }
                            }
                        },
                        "tooltips": {
                            "enabled": true,
                            "mode": "label",
                            "caretSize": 10
                        }
                    };

                    new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
                    });

                    scNotifier.notifyInfo("No data currently available");
                }
                $window.location.href;


            }, function errorCallback(response) {
                console.log(response);

                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

            });
        };
    }]);

schoolMonitor.controller('submitFormController', ['scNotifier', '$scope', '$http', '$window',
    function (scNotifier, $scope, $http, $window) {
        $scope.changePageTitle('View', 'Pupil Aggregated Data', 'View');

        $scope.submitForm = function () {


            $scope.formData.pupilag = "                                       Pupil Aggregated Data";
            var ConfirmSubmission = confirm($scope.formData.pupilag + " \n" +
                "No. of Students in class:                                        " + $scope.formData.no_student_grade + " \n" +
                "No. of students assessed:                                      " + $scope.formData.no_student_assessed + " \n" +
                "Academic term:                                                     " + $scope.formData.academic_year + " \n" +
                "Academic year:                                                      " + $scope.formData.academic_term + " \n" +
                "Grade:                                                                    " + $scope.formData.grade + " \n" +
                "Beginner:                                                                " + $scope.formData.math_beginner_text + " \n" +
                "1-Digit:                                                                   " + $scope.formData.math_digit_one_text + " \n" +
                "2-Digit:                                                                   " + $scope.formData.math_digit_two_text + " \n" +
                "Addition:                                                                " + $scope.formData.math_addition_text + " \n" +
                "Addition:                                                                " + $scope.formData.math_subtraction_text + " \n" +
                "Level 1:                                                                   " + $scope.formData.math_level_one_text + " \n" +
                "Level 2:                                                                   " + $scope.formData.math_level_two_text + " \n" +
                "Level 3:                                                                   " + $scope.formData.math_level_three_text + " \n" +
                "Beginner:                                                                " + $scope.formData.english_beginner_text + " \n" +
                "Letter:                                                                     " + $scope.formData.english_letter_text + " \n" +
                "Paragraph:                                                              " + $scope.formData.english_paragraph_text + " \n" +
                "Word:                                                                      " + $scope.formData.english_word_text + " \n" +
                "Sentence:                                                                " + $scope.formData.english_sentence_text + " \n" +
                "Level 1:                                                                    " + $scope.formData.english_level_one_text + " \n" +
                "Level 2:                                                                    " + $scope.formData.english_level_two_text + " \n" +
                "Level 3:                                                                    " + $scope.formData.english_level_three_text + " \n" +
                "                                  Are you sure you want to submit ?");
            if (ConfirmSubmission) {

                console.log($scope.formData);
                $http({
                    method: 'POST',
                    url: '/save/pupilaggregateddata',
                    data: $scope.formData

                }).then(function successCallback(response) {
                    console.log(response.data);

                    if (response.data === "1") {
                        scNotifier.notifySuccess("Success", "Pupil Aggregated Data successfully sent.");
                        location.reload();
                    } else {
                        scNotifier.notifyFailure("Error", "Issue sending Pupil Aggregated Data.");
                    }

                }, function errorCallback(response) {
                    scNotifier.notifyInfo("Error", "An error occurred on the server. " +
                        "Please try again or  contact the system administrator.");

                });
            }

            else {
                return false;
            }
        };
    }]);


schoolMonitor.controller('analyticsPollController', ['scNotifier', '$scope', '$http', '$window',
    function (scNotifier, $scope, $http, $window) {

        $scope.submitPollForm = function () {
            $http({
                method: 'POST',
                url: '/save/targetedinstruction/analytics/poll',
                data: $scope.formData,

            }).then(function successCallback(response) {



                if (response.data.success === 1) {
                    scNotifier.notifySuccess("Success", "Poll Question sent successfully.");
                    location.reload();
                    // $window.location.href;
                } else {
                    scNotifier.notifyFailure("Error", "Issue sending Poll Question.");
                }

            }, function errorCallback(response) {
                scNotifier.notifyFailure("Error", "An error occurred on the server. Please try again or contact the system administrator.");

            });
        };

    }]);


schoolMonitor.controller('analyticsPollEditListController', ['$modal', 'scNotifier', '$scope', '$http', '$window',
    function ($modal, scNotifier, $scope, $http, $window) {
        $scope.changePageTitle('View', 'Polls', 'View');




        $scope.save = function () {


            var mylength = window.location.href.split("/").length;


            var id = window.location.href.split("/")[mylength - 1];

            $scope.formData.urlid = id;
            console.log($scope.formData);
            $http({
                method: 'POST',
                url: '/targetedinstruction/poll/update',
                data: $scope.formData,
                // headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                console.log("123");
                console.log(response);

                console.log("abc");
                if (response.data.success === 1) {
                    scNotifier.notifySuccess("Success", "Poll Question Edited  successfully.");
                    location.reload();
                    // $window.location.href;
                } else {
                    scNotifier.notifyFailure("Error", "Issue Editing Poll Question.");
                }


            }).error(function (response) {
                console.log(response);
                alert('This is embarassing. An error has occured. Please check the log for details');
            });
        }




        console.log("test 0");



        $scope.deleteModal = function (id) {

//$scope.messageToDataCollector = $scope.lookupDataCollector[id];

            console.log("test1");
            $modal.open({
                templateUrl: '<h1>Nice</h1>',
                controller: deleteCircuitSupervisorModalCtrl, //deleteCircuitSupervisorModalCtrl
                size: 'sm',
                resolve: []
            });
            console.log("test2");

        };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        function deleteCircuitSupervisorModalCtrl($scope, $modalInstance, selected_data_collector, scCircuitSupervisor) {

//            $scope.datacollector = selected_data_collector;

            $scope.deleteObject = selected_data_collector;
            $scope.deleteObject.objectType = 'Data Collector';

            $scope.ok = function () {
                $modalInstance.dismiss('cancel');
                $rootScope.loading = true;
                scCircuitSupervisor.deleteSupervisor(selected_data_collector.id)
                //Success callback
                    .then(function (value) {
                            if (value) {
                                scNotifier.notifySuccess('Notification', 'Data Collector deleted');
//                            $state.go('dashboard.dashboard.data_collectors_circuit_supervisors', {reload : true})
                                $rootScope.$broadcast('supervisorsUpdated');
                            } else {
                                scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector')
                            }
                            $rootScope.loading = false;
                        },
                        //Error callback
                        function () {
                            scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector');
                            $rootScope.loading = false;
                        });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }


        console.log("iron man");


    }]);


schoolMonitor.controller('analyticsPollListController', ['$modal', 'scNotifier', '$scope', '$http', '$window',
    function ($modal, scNotifier, $scope, $http, $window) {
        $scope.changePageTitle('View', 'Polls', 'View');


        $http.get('/get/targetedinstruction/analytics/polllist/view')
            .then(function (response) {

                $scope.poll_data = response.data;

            });


        $scope.delete = function (id) {
            var isConfirmDelete = confirm('Are you sure you want to delete this record?');
            if (isConfirmDelete) {

                $http({
                    method: 'DELETE',
                    url: '/targetedinstruction/poll/destroy',
                    data: id
                }).success(function (data) {

                    location.reload();
                    // $window.location.href;
                }).error(function (data) {

                    alert('Unable to delete');
                });
            } else {
                return false;
            }
        }


        $scope.save = function (id) {

            $http({
                method: 'POST',
                url: '/targetedinstruction/poll/update',
                data: $scope.formData,
                // headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {

                location.reload();
            }).error(function (response) {

                alert('This is embarassing. An error has occured. Please check the log for details');
            });
        }


        $scope.submitPollForm = function () {
            $http({
                method: 'POST',
                url: '/save/targetedinstruction/analytics/polllist',
                data: $scope.formData,


            }).then(function successCallback(response) {


            }, function errorCallback(response) {
                scNotifier.notifyFailure("Error", "An error occurred on the server. Please try again or contact the system administrator.");

            });
        };







        $scope.deleteModal = function (id) {

//$scope.messageToDataCollector = $scope.lookupDataCollector[id];


            $modal.open({
                templateUrl: '<h1>Nice</h1>',
                controller: deleteCircuitSupervisorModalCtrl, //deleteCircuitSupervisorModalCtrl
                size: 'sm',
                resolve: []
            });


        };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        function deleteCircuitSupervisorModalCtrl($scope, $modalInstance, selected_data_collector, scCircuitSupervisor) {

//            $scope.datacollector = selected_data_collector;

            $scope.deleteObject = selected_data_collector;
            $scope.deleteObject.objectType = 'Data Collector';

            $scope.ok = function () {
                $modalInstance.dismiss('cancel');
                $rootScope.loading = true;
                scCircuitSupervisor.deleteSupervisor(selected_data_collector.id)
                //Success callback
                    .then(function (value) {
                            if (value) {
                                scNotifier.notifySuccess('Notification', 'Data Collector deleted');
//                            $state.go('dashboard.dashboard.data_collectors_circuit_supervisors', {reload : true})
                                $rootScope.$broadcast('supervisorsUpdated');
                            } else {
                                scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector')
                            }
                            $rootScope.loading = false;
                        },
                        //Error callback
                        function () {
                            scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector');
                            $rootScope.loading = false;
                        });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }





    }]);


schoolMonitor.controller('analyticsPollGraphController', ['scNotifier', '$scope', '$http', '$window',
    function (scNotifier, $scope, $http, $window) {

        $http.get('/get/targetedinstruction/analytics/poll/questions')
            .then(function (response) {
                $scope.poll_data = response.data;
            });

        $scope.exportData = function () {
            var pollTitle = angular.element(document).find('#poll_title');
            var pollData = {};
            pollData['id'] = pollTitle.val();



            $http({
                method: 'POST',
                url: '/export/targetedinstruction/polls',
                data: pollData,

            }).then(function successCallback(response) {

            });
        }

        $scope.filterPoll = function () {

            let exportBtn = angular.element(document).find('#export_wrapper');
            let downloadBtn = angular.element(document).find('#download_export');
            let pollTitle = angular.element(document).find('#poll_title');

            $http({
                method: 'POST',
                url: '/save/targetedinstruction/analytics/pollgraph',
                data: $scope.formData,

            }).then(function successCallback(response) {

                let answer_count = response.data[0].answers_count;
                let poll_answers = response.data[0].poll_answers;

                exportBtn.css("display", "block");

                let aLength = downloadBtn.length;
                for (let i = 0; i < aLength; i++) {
                    downloadBtn[i].href += "?id=" + pollTitle.val();
                }

                if (answer_count.length <= 0 && poll_answers.length <= 0) {
                    exportBtn.css("display", "none");
                }

                let ctx = document.getElementById('myChart').getContext('2d');
                let data = {
                    "labels": poll_answers,
                    "datasets": [
                        {
                            "label": "2014",
                            "backgroundColor": ["#3e95cd", "#8e5ea2", "#3cba9f"],
                            "fill": true,
                            "data": answer_count,
                            "borderColor": "#ffffff",
                            "borderWidth": "1"
                        }

                    ]
                };
                let options = {
                    "title": {
                        "display": true,
                        "text": "Poll Graph",
                        "position": "bottom",
                        "fullWidth": true,
                        "fontColor": "#000000",
                        "fontSize": 16
                    },
                    "legend": {
                        "display": true,
                        "fullWidth": true,
                        "position": "top"
                    },
                    "scales": {
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true,
                                    "display": true
                                },
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": true,
                                    "drawTicks": true,
                                    "tickMarkLength": 1,
                                    "offsetGridLines": true,
                                    "zeroLineColor": "#942192",
                                    "color": "#d6d6d6",
                                    "zeroLineWidth": 2
                                },
                                "scaleLabel": {
                                    "display": true,
                                    "labelString": ""
                                },
                                "display": true
                            }
                        ],
                        "xAxes": {
                            "0": {
                                "ticks": {
                                    "display": true,
                                    "fontSize": 14,
                                    "fontStyle": "italic"
                                },
                                "display": true,
                                "gridLines": {
                                    "display": true,
                                    "lineWidth": 2,
                                    "drawOnChartArea": false,
                                    "drawTicks": true,
                                    "tickMarkLength": 12,
                                    "zeroLineWidth": 2,
                                    "offsetGridLines": true,
                                    "color": "#942192",
                                    "zeroLineColor": "#942192"
                                },
                                "scaleLabel": {
                                    "fontSize": 16,
                                    "display": true,
                                    "fontStyle": "normal"
                                }
                            }
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "mode": "label",
                        "caretSize": 10,
                        "backgroundColor": "#00fa92"
                    }
                };

                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: data,
                    options: options
                });

                $window.location.href = '/admin#/targetedinstruction/analytics/pollgraph';

            }, function errorCallback(response) {
                scNotifier.notifyFailure("Error", "An error occurred on the server. " +
                    "Please try again or contact the system administrator.");
            });

        };
    }]);


schoolMonitor.controller('submitattendanceFormController', ['scNotifier', '$scope', '$http', '$window',
    function (scNotifier, $scope, $http, $window) {
        // $scope.changePageTitle('View','Attendance','View');
        //


        $scope.submitattendanceForm = function () {

            $scope.formData.Attendance = "                                             Attendance";
            var ConfirmSubmission = confirm($scope.formData.Attendance + " \n" +
                "Attendance academic term:                     " + $scope.formData.attendance_academic_term + " \n" +
                "Attendance academic year:                      " + $scope.formData.attendance_academic_year + " \n" +
                "Week number:                                          " + $scope.formData.attendance_academic_week + " \n" +
                "Attendance academic level :                    " + $scope.formData.attendance_academic_level + " \n" +
                "Attendance academic subject:                 " + $scope.formData.attendance_academic_subject + " \n" +

                "No. of pupils at level:                               " + $scope.formData.attendance_academic_level + " \n" +
                "Average pupil attendance for week:         " + $scope.formData.attendance_no_pupil_avg + " \n" +
                "Average teacher attendance for week:     " + $scope.formData.attendance_no_teacher_avg + " \n" +
                "                               Are you sure you want to submit ?");

            if (ConfirmSubmission) {

                $http({
                    method: 'POST',
                    url: '/save/attendance',
                    data: $scope.formData,

                }).then(function successCallback(response) {



                    if (response.data === "1") {
                        scNotifier.notifySuccess("Success", "Attendance Data successfully sent.");
                        location.reload();
                    } else {
                        scNotifier.notifyFailure("Error", "Issue sending Attendance Data.");
                    }

                }, function errorCallback(response) {


                    scNotifier.notifyInfo("Error", "An error occurred on the server. " +
                        "Please try again or  contact the system administrator.");

                });

            }

            else {
                return false;
            }

        };
    }]);


schoolMonitor.controller('submitClassObservationFormController',
    ['scNotifier', '$scope', '$http', '$window',
        function (scNotifier, $scope, $http, $window) {
            $scope.changePageTitle('View', 'Class Observation', 'View');

            $http.get('/get/class_observation/teachers')
                .then(function (response) {
                    $scope.teachers_data = response.data;
                });


            $scope.submitClassObservationForm = function () {

                $scope.formData.classobservation = "Class Observation";
                $scope.formData.class_ob_teacher_name = $("#class_ob_teacher_name").val();

                var ConfirmSubmission = confirm($scope.formData.classobservation + " \n" +
                    "Teacher Name: " + $scope.formData.class_ob_teacher_name + " \n" +
                    "Subject: " + $scope.formData.class_ob_subject + " \n" +
                    "Level:                                                    " + $scope.formData.class_ob_level + "   \n" +
                    "Students Enrolled(No assigned to TI) from this class " + " \n" +
                    "P4:                                                        " + $scope.formData.class_ob_enrolled_p_four + " \n" +
                    "P5:                                                        " + $scope.formData.class_ob_enrolled_p_five + " \n" +
                    "P6:                                                        " + $scope.formData.class_ob_enrolled_p_six + "   \n" +
                    "No. Students Absent(take a roll call)" + " \n" +
                    // "Total: "+$scope.formData.class_ob_enrolled_p_total +" \n"+
                    "P4:                                                        " + $scope.formData.class_ob_absent_p_four + " \n" +
                    "P5:                                                        " + $scope.formData.class_ob_absent_p_five + " \n" +
                    "P6:                                                        " + $scope.formData.class_ob_absent_p_six + " \n" +
                    // "Total: "+$scope.formData.class_ob_absent_p_total +" \n"+
                    "Duration:                                              " + $scope.formData.class_ob_duration
                );


                var ConfirmSubmission2 = confirm("Date:" + $scope.formData.class_ob_date_monitoring + " \n" +

                    "TI Pupil Attendance Summary: " + $scope.formData.class_ob_attendance_summary + "\n" +


                    "Is the teacher talking less than half of the time?:" + $scope.formData.la_question_one_answer + "\n" +

                    "Comment: " + $scope.formData.la_question_one_comment + "\n" +


                    "Does teacher use a variety of methods to engage children (engage children as whole class,individual,and group)?: " + $scope.formData.la_question_two_answer + "\n" +


                    "Comment:" + $scope.formData.la_question_two_comment
                );


                var ConfirmSubmission3 = confirm("Was the teacher able to complete lesson plan as per teacher guide?:" + $scope.formData.la_question_three_answer + " \n" +
                    "Comment: " + $scope.formData.la_question_three_comment + " \n" +
                    "Lesson Content Summary: " + $scope.formData.la_lesson_summary + " \n" +
                    "Are learners grouped into learning levels?: " + $scope.formData.ti_question_one_answer + " \n" +

                    "Comment: " + $scope.formData.ti_question_one_comment + " \n" +
                    "Does teacher teach the right content according to the lesson plan given the level?:" + $scope.formData.ti_question_two_answer + " \n" +
                    "Comment: " + $scope.formData.ti_question_two_comment + " \n" +
                    "Does the teacher teach with appropriate materials?: " + $scope.formData.ti_question_three_answer + " \n" +
                    "Comment: " + $scope.formData.ti_question_three_comment + " \n" +
                    "Lesson Preparation Summary: " + $scope.formData.ti_lesson_summary + " \n" +
                    "Did the lesson today involve the following aspects of learning ?" + $scope.formData.cl_question_one_comment + " \n" +
                    "Are the children following the lesson being taught for the day? " + $scope.formData.cl_question_two_answer + " \n" +
                    "Comment: " + $scope.formData.cl_question_two_comment + " \n" +
                    "Were more than half of pupils participating during the lesson?  " + $scope.formData.cl_question_three_answer + " \n" +
                    "Comment: " + $scope.formData.cl_question_three_comment + " \n" +
                    "What key challenges is this teacher facing during T1 implementation?List at least two." + $scope.formData.cl_question_four_comment
                    + " \n" +
                    "Lesson Content Summary: " + $scope.formData.cl_classroom_summary + " \n" +
                    "Comment: " + $scope.formData.cl_question_one_comment + " \n" +
                    // ""+$scope.formData.english_level_three_text +" \n"+
                    "                                  Are you sure you want to submit ?");


                if (ConfirmSubmission) {

                    if (ConfirmSubmission2) {


                        if (ConfirmSubmission3) {



                            $http({
                                method: 'POST',
                                url: '/save/class_observation',
                                data: $scope.formData,

                            }).then(function successCallback(response) {



                                if (response.data === "1") {
                                    scNotifier.notifySuccess("Success", "Class Observation Data successfully sent.");
                                    // location.reload();
                                } else if (response.data === "3") {
                                    scNotifier.notifyFailure("Error", "Incorrect Teacher Name.");
                                } else {
                                    scNotifier.notifyFailure("Error", "Issue sending Class Observation Data.");

                                }

                            }, function errorCallback(response) {


                                scNotifier.notifyInfo("Error", "An error occurred on the server. " +
                                    "Please try again or  contact the system administrator.");

                            });


                        }
                    }
                }

                else {
                    return false;
                }

            };
        }]);



schoolMonitor.controller('sct1DistrictController',
    ['$rootScope', '$scope', '$filter', '$state', '$stateParams', 'scNotifier', 'District', '$modal', '$log',
        'scRegion', 'scDistrict', 'scCircuit', 'scSchool', '$timeout', '$http',
        function ($rootScope, $scope, $filter, $state, $stateParams, scNotifier, District, $modal, $log,
                  scRegion, scDistrict, scCircuit, scSchool, $timeout, $http) {

            //$parent is required to be able to access the outer rootScope of the dashboard
            $scope.$parent.changePageTitle('View', 'District(Attendance/Pupil Aggregated Data)', 'View');

            $http.get('/ti/only/districts')
                .then(function (response) {
                    $scope.ti_districts = response.data.districts;
                    $scope.attendance_count = response.data.attendance_count;
                    $scope.lessons_count = response.data.lessons_count;
                    $scope.pupil_aggregated_count = response.data.pupil_aggregated_count;
                });


            function loadDistricts() {
                $timeout(function () {
                    $state.reload();

                    $scope.regionLookup = scRegion.regionLookup;

                    //$scope.selected_district = scDistrict.districtLookup[$stateParams.id];
                    console.log('admin right ', $scope.adminRight);
                    if ($scope.adminRight > 4) {
                        $scope.districts = scDistrict.districts;
                        $scope.districtsSlice = scDistrict.districts;
                        $scope.districtsSearch = scDistrict.districts;
                        $scope.totalItems = scDistrict.districts.length;
                    } else if ($scope.adminRight == 4) {
                        $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                        $scope.districtsSlice = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                        $scope.districtsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                        $scope.totalItems = scRegion.regionLookup[$scope.currentUser.level_id].totalDistricts;
                    } else {
                        $scope.districts = [
                            scDistrict.districtLookup[$scope.currentUser.level_id]
                        ];
                    }


                }, 10);
            }

            $scope.$on('updateLoadedModelsFromRefresh', function () {
                // loadDistricts()
            });

            if (scDistrict.districts.length && scDistrict.districtLookup) {
                // loadDistricts()
            }

            $scope.districtSortOrder = 'name';
            $scope.currentPage = 1;
            $scope.maxSize = 15;
            $scope.nextPage = 0;

            $scope.pageChanged = function (currentPage) {
                $scope.nextPage = ($scope.maxSize * currentPage) - $scope.maxSize;
                $scope.districts = ($scope.districtsSlice).slice($scope.nextPage, ($scope.nextPage + 15));
                $filter('filter')($scope.districts, $scope.districtSortOrder, false);
            };

            $scope.formData = {};

            $scope.paramAction = function () {
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };

            $scope.searchModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/miscellaneous/searchDistrictModal.html',
                    controller: searchModalInstanceCtrl,
                    size: 'lg'
                });

            };

            var searchModalInstanceCtrl = function ($scope, $modalInstance) {

                if ($scope.adminRight > 4) {
                    $scope.districts = scDistrict.districts;
                } else if ($scope.adminRight == 4) {
                    $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                } else {
                    $scope.districts = [
                        scDistrict.districtLookup[$scope.currentUser.level_id]
                    ];
                }

                $scope.districtSortOrder = 'name';

                $scope.regionLookup = scRegion.regionLookup;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };

            }

        }]);



schoolMonitor.controller('sct1RegionController', ['$rootScope', '$scope', '$state', '$stateParams',
    'Region', 'scRegion', 'scNotifier', '$modal', '$log', 'scDistrict', 'scCircuit', 'scSchool', '$timeout', 'DataCollectorHolder',
    function ($rootScope, $scope, $state, $stateParams, Region, scRegion, scNotifier,
              $modal, $log, scDistrict, scCircuit, scSchool, $timeout, DataCollectorHolder) {

        $scope.$parent.changePageTitle('View', 'Region(Attendance/Pupil Aggregated Data)', 'View');

        function assignAdmin() {
            $timeout(function () {
                if ($scope.adminRight > 4) {
                    $scope.regions = scRegion.regions;
                } else {
                    $scope.regions = [
                        scRegion.regionLookup[$scope.currentUser.level_id]
                    ];
                }
            }, 10)
        }

        $scope.$on('updateLoadedModelsFromRefresh', function (evt, data) {
            if (data.counter == 6) {
                assignAdmin();
            }
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scRegion.regions.length && scRegion.regionLookup) {
            assignAdmin();
        }


        //initialise form Object
        $scope.formData = {};

        //Make the first letter of the action passed via stateParams a capital one
        $scope.paramAction = function () {
            return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
        };

//        if the action is to edit a Region do this
        if ($stateParams.action == 'edit') {
//            var data = scRegion.retrieveOneRegion($stateParams.id);
            scRegion.retrieveOneRegion($stateParams.id).then(
                function (data) {
                    if (data) {
                        $scope.formData = {
                            id: scRegion.regionRetrieved.id,
                            region_name: scRegion.regionRetrieved.name,
                            description: scRegion.regionRetrieved.description,
                            region_email: scRegion.regionRetrieved.email,
                            region_phone_number: scRegion.regionRetrieved.phone_number,
                            country_id: scRegion.regionRetrieved.country_id
                        };
                    }
                },
                function () {
                    scNotifier.notifyFailure('Error', 'There was an error in trying to edit the region')
                });

            //Else if the action is to add, set the form object to empty
        } else if ($stateParams.action == 'add') {
            $scope.formData = {};
        }

        // process the form on click submit
        $scope.processForm = function () {
            $rootScope.loading = true;
            if ($stateParams.action == 'edit') {
                scRegion.editRegion($scope.formData).then(
                    function (data) {
                        if (data) {
                            scNotifier.notifySuccess('Success', 'Region edited successfully ');
                            $state.go('dashboard.view_region', {}, {reload: true});
                        } else {
                            scNotifier.notifyFailure('Error', 'There was an error editing the region');
                        }
                    },
                    function () {
                        scNotifier.notifyFailure('Error', 'There was an error editing the region');
                    });
            } else {
                // process the form
                scRegion.addRegion($scope.formData).then(function (status) {
                    if (status == true) {
                        $scope.formData = {};
                        scNotifier.notifySuccess('Region', 'Successfully created the region');
                        $state.go('dashboard.view_region', {}, {reload: true});
                    } else if (status == false) {
                        scNotifier.notifyFailure('Region', 'There was an error creating the region');
                    } else {
                        scNotifier.notifyFailure('Region', 'Please check the fields again');
                    }
                    $rootScope.loading = false;
                });


            }
        };


        $scope.deleteModal = function (id) {

            var modalInstance = $modal.open({
                templateUrl: 'partials/miscellaneous/deleteModal.html',
                controller: ModalInstanceCtrl,
                size: 'sm',
                resolve: {
                    deleteRegionId: function () {
                        return id;
                    }
//                    },
//                    clickedElement: function () {
////                    return $(element.target).parent('tr');
//                        return $(element.target).parents('tr');
                }
            })
        };


        var ModalInstanceCtrl = function ($scope, $modalInstance, deleteRegionId, scRegion, $state) {
            $scope.deleteObject = scRegion.regionLookup[deleteRegionId];
            $scope.deleteObject.objectType = 'Region';

            $scope.ok = function () {
                scRegion.deleteRegion(deleteRegionId).then(
                    function (status) {
                        if (status) {
                            scNotifier.notifySuccess('Region', 'Region deleted successfully');
                            $state.go('dashboard.view_region', {}, {reload: true});
                        } else {
                            scNotifier.notifyFailure('Region', 'There was an error in deleting the region');
                        }
                    });
                $modalInstance.close('close');
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };

    }]);


schoolMonitor.controller('sct1SchoolController',
    ['$rootScope', '$scope', '$state', '$stateParams', 'scNotifier', 'School', '$modal',
        '$log', '$filter', '$http', 'scRegion', 'scDistrict', 'scCircuit', 'scSchool',
        'scHeadTeacher', 'scCircuitSupervisor', '$timeout', 'DataCollectorHolder',
        function ($rootScope, $scope, $state, $stateParams, scNotifier, School, $modal,
                  $log, $filter, $http, scRegion, scDistrict, scCircuit, scSchool, scHeadTeacher,
                  scCircuitSupervisor, $timeout, DataCollectorHolder) {

            $scope.changePageTitle('View', 'School(Attendance/Pupil Aggregated Data)', 'View');

            $http.get('/ti/only/schools')
                .then(function (response) {
                    $scope.mk_schools = response.data.schools;
                    $scope.attendance_count = response.data.attendance_count;
                    $scope.lessons_count = response.data.lessons_count;
                    $scope.pupil_aggregated_count = response.data.pupil_aggregated_count;
            });

            var schoolHolder;

            function assignModels() {

                $scope.regionLookup = scRegion.regionLookup;
                $scope.districtLookup = scDistrict.districtLookup;
                $scope.circuitLookup = scCircuit.circuitLookup;
                $scope.schoolLookup = scSchool.schoolLookup;

                if ($scope.adminRight == 5) {
                    $scope.regions = scRegion.regions;
                    $scope.districts = scDistrict.districts;
                    $scope.circuits = scCircuit.circuits;
                    $scope.head_teachers = scHeadTeacher.headteachers;
                    $scope.supervisors = scCircuitSupervisor.supervisors;

                    $scope.schools = scSchool.schools;
                    schoolHolder = scSchool.schools;
                    $scope.schoolsSearch = scSchool.schools;
                    $scope.totalItems = scSchool.schools.length;

                }
                else if ($scope.adminRight == 4) {
                    $scope.regions = new Array(scRegion.regionLookup[$scope.currentUser.level_id]);
                    $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                    $scope.circuits = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers = scRegion.regionLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors = scRegion.regionLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.schools = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    schoolHolder = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;

                    $scope.schoolsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems = scRegion.regionLookup[$scope.currentUser.level_id].totalSchools;

                }
                else if ($scope.adminRight == 3) {
                    $scope.regions = new Array(scRegion.regionLookup[scDistrict.districtLookup[$scope.currentUser.level_id].region_id]);
                    $scope.districts = new Array(scDistrict.districtLookup[$scope.currentUser.level_id]);
                    $scope.circuits = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers = scDistrict.districtLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors = scDistrict.districtLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.schools = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    schoolHolder = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems = scDistrict.districtLookup[$scope.currentUser.level_id].totalSchools;

                }
                else if ($scope.adminRight == 2) {
                    $scope.regions = new Array(scRegion.regionLookup[
                        scDistrict.districtLookup[scCircuit.circuitLookup[$scope.currentUser.level_id].district_id].region_id]);

                    $scope.districts = new Array(
                        scDistrict.districtLookup[scCircuit.circuitLookup[$scope.currentUser.level_id].district_id]);

                    $scope.circuits = new Array(scCircuit.circuitLookup[$scope.currentUser.level_id]);
                    $scope.head_teachers = scCircuit.circuitLookup[$scope.currentUser.level_id].head_teachers_holder;

                    $scope.schools = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    schoolHolder = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems = scCircuit.circuitLookup[$scope.currentUser.level_id].totalSchools;

                }
                else {
                    $scope.schools = new Array($scope.schoolLookup[$scope.currentUser.level_id]);
                    schoolHolder = new Array($scope.schoolLookup[$scope.currentUser.level_id])
                }
                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;
            }


            $scope.$on('updateLoadedModelsFromRefresh', function (evt, data) {
                if (data.counter == 6) {
                    $timeout(function () {
                        // assignModels();
                    }, 10)
                }

            });


            if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector
                && scSchool.schools.length && scSchool.schoolLookup) {
                // assignModels()
            }

            $scope.schoolSortOrder = 'name';
            $scope.maxSize = 15;
            $scope.currentPage = 1;
            $scope.nextPage = 0;

            $scope.pageChanged = function (currentPage) {
                $scope.nextPage = ($scope.maxSize * currentPage) - $scope.maxSize;
                $scope.schools = schoolHolder.slice($scope.nextPage, ($scope.nextPage + 15));
                $filter('filter')($scope.schools, $scope.schoolSortOrder, false);
            };


            $scope.paramAction = function () {
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };

            $scope.getDistrictName = function (id) {
                $scope.formData.district_name = $scope.districtLookup[id].name;
            };

            $scope.searchModal = function () {
                $modal.open({
                    templateUrl: 'partials/miscellaneous/searchSchoolModal.html',
                    controller: searchModalInstanceCtrl,
                    size: 'lg'
                });

            };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

            var searchModalInstanceCtrl = function ($scope, $modalInstance) {

                $scope.schoolSortOrder = 'name';

                if ($scope.adminRight == 5) {
                    $scope.schools = scSchool.schools;
                    $scope.schoolsSearch = scSchool.schools;
                    $scope.totalItems = scSchool.schools.length;
                } else if ($scope.adminRight == 4) {
                    $scope.schools = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems = scRegion.regionLookup[$scope.currentUser.level_id].totalSchools;
                } else if ($scope.adminRight == 3) {
                    $scope.schools = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems = scDistrict.districtLookup[$scope.currentUser.level_id].totalSchools;
                } else if ($scope.adminRight == 2) {
                    $scope.schools = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems = scCircuit.circuitLookup[$scope.currentUser.level_id].totalSchools;
                } else {
                    $scope.schools = [
                        $scope.schoolLookup[$scope.currentUser.level_id]
                    ];
                }

                $scope.districtLookup = scDistrict.districtLookup;

                $scope.circuitLookup = scCircuit.circuitLookup;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };

            };


        }]);


schoolMonitor.controller('sct1DistrictController',
    ['$rootScope', '$scope', '$filter', '$state', '$stateParams', 'scNotifier', 'District', '$modal', '$log',
        'scRegion', 'scDistrict', 'scCircuit', 'scSchool', '$timeout', '$http',
        function ($rootScope, $scope, $filter, $state, $stateParams, scNotifier, District, $modal, $log,
                  scRegion, scDistrict, scCircuit, scSchool, $timeout, $http) {

            //$parent is required to be able to access the outer rootScope of the dashboard
            $scope.$parent.changePageTitle('View', 'District(Attendance/Pupil Aggregated Data)', 'View');

            $http.get('/ti/only/districts')
                .then(function (response) {
                    $scope.ti_districts = response.data.districts;
                    $scope.attendance_count = response.data.attendance_count;
                    $scope.lessons_count = response.data.lessons_count;
                    $scope.pupil_aggregated_count = response.data.pupil_aggregated_count;
                });


            function loadDistricts() {
                $timeout(function () {
                    $state.reload();

                    $scope.regionLookup = scRegion.regionLookup;

                    //$scope.selected_district = scDistrict.districtLookup[$stateParams.id];
                    console.log('admin right ', $scope.adminRight);
                    if ($scope.adminRight > 4) {
                        $scope.districts = scDistrict.districts;
                        $scope.districtsSlice = scDistrict.districts;
                        $scope.districtsSearch = scDistrict.districts;
                        $scope.totalItems = scDistrict.districts.length;
                    } else if ($scope.adminRight == 4) {
                        $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                        $scope.districtsSlice = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                        $scope.districtsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                        $scope.totalItems = scRegion.regionLookup[$scope.currentUser.level_id].totalDistricts;
                    } else {
                        $scope.districts = [
                            scDistrict.districtLookup[$scope.currentUser.level_id]
                        ];
                    }


                }, 10);
            }

            $scope.$on('updateLoadedModelsFromRefresh', function () {
                // loadDistricts()
            });

            if (scDistrict.districts.length && scDistrict.districtLookup) {
                // loadDistricts()
            }

            $scope.districtSortOrder = 'name';
            $scope.currentPage = 1;
            $scope.maxSize = 15;
            $scope.nextPage = 0;

            $scope.pageChanged = function (currentPage) {
                $scope.nextPage = ($scope.maxSize * currentPage) - $scope.maxSize;
                $scope.districts = ($scope.districtsSlice).slice($scope.nextPage, ($scope.nextPage + 15));
                $filter('filter')($scope.districts, $scope.districtSortOrder, false);
            };

            $scope.formData = {};

            $scope.paramAction = function () {
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };

            $scope.searchModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/miscellaneous/searchDistrictModal.html',
                    controller: searchModalInstanceCtrl,
                    size: 'lg'
                });

            };

            var searchModalInstanceCtrl = function ($scope, $modalInstance) {

                if ($scope.adminRight > 4) {
                    $scope.districts = scDistrict.districts;
                } else if ($scope.adminRight == 4) {
                    $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                } else {
                    $scope.districts = [
                        scDistrict.districtLookup[$scope.currentUser.level_id]
                    ];
                }

                $scope.districtSortOrder = 'name';

                $scope.regionLookup = scRegion.regionLookup;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };

            }

        }]);


schoolMonitor.controller('sct1CircuitController',
    ['$rootScope', '$scope', '$state', '$filter', '$stateParams', 'Circuit', '$modal', '$log', 'scNotifier',
        'scRegion', 'scDistrict', 'scCircuit', 'scSchool', '$timeout', 'DataCollectorHolder', '$http',
        function ($rootScope, $scope, $state, $filter, $stateParams, Circuit, $modal, $log, scNotifier,
                  scRegion, scDistrict, scCircuit, scSchool, $timeout, DataCollectorHolder, $http) {

            $scope.changePageTitle('View', 'Circuit(Attendance/Pupil Aggregated Data)', 'View');

            $http.get('/ti/only/circuits')
                .then(function (response) {
                    $scope.ti_circuits = response.data.circuits;
                    $scope.attendance_count = response.data.attendance_count;
                    $scope.lessons_count = response.data.lessons_count;
                    $scope.pupil_aggregated_count = response.data.pupil_aggregated_count;
                });

            $scope.formData = {};

            //this is used to sort the districts alphabetically
            $scope.circuitSortOrder = 'name';

            function assignAdmin() {
                $scope.districtLookup = scDistrict.districtLookup;

                if ($scope.adminRight == 5) {
                    $scope.regions = scRegion.regions;
                    $scope.districts = scDistrict.districts;
                    $scope.circuits = scCircuit.circuits;
                    $scope.circuitsSlice = scCircuit.circuits;

                    $scope.circuitsSearch = scCircuit.circuits;
                    $scope.totalItems = scCircuit.circuits.length;

                    $scope.schools = scSchool.schools;
                    $scope.schoolsSearch = scSchool.schools;

                }
                else if ($scope.adminRight == 4) {
                    $scope.regions = new Array(scRegion.regionLookup[$scope.currentUser.level_id]);
                    $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                    $scope.circuits = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.circuitsSlice = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers = scRegion.regionLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors = scRegion.regionLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.circuitsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.totalItems = scRegion.regionLookup[$scope.currentUser.level_id].totalCircuits;

                    $scope.schools = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;

                }
                else if ($scope.adminRight == 3) {
                    $scope.regions = new Array(scRegion.regionLookup[scDistrict.districtLookup[$scope.currentUser.level_id].region_id]);
                    $scope.districts = new Array(scDistrict.districtLookup[$scope.currentUser.level_id]);
                    $scope.circuits = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.circuitsSlice = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers = scDistrict.districtLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors = scDistrict.districtLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.circuitsSearch = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.totalItems = scDistrict.districtLookup[$scope.currentUser.level_id].totalCircuits;

                    $scope.schools = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;

                }
                else {
                    $scope.circuits = new Array(scCircuit.circuitLookup[$scope.currentUser.level_id]);
                    $scope.schools = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                }
            }

            $scope.$on('updateLoadedModelsFromRefresh', function (evt, data) {
                if (data.counter == 6) {
                    $timeout(function () {
                        // assignAdmin();
                    }, 10)
                }

            });

            if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector
                && scCircuit.circuits.length && scCircuit.circuitLookup) {
                // assignAdmin();
            }

            $scope.currentPage = 1;
            $scope.maxSize = 15;
            $scope.nextPage = 0;

            $scope.pageChanged = function (currentPage) {
                $scope.nextPage = ($scope.maxSize * currentPage) - $scope.maxSize;
                $scope.circuits = ($scope.circuitsSlice).slice($scope.nextPage, ($scope.nextPage + 15));
                $filter('filter')($scope.circuits, $scope.circuitSortOrder, false);
            };

            $scope.paramAction = function () {
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };


            $scope.searchModal = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/miscellaneous/searchCircuitModal.html',
                    controller: searchModalInstanceCtrl,
                    size: 'lg'
                });

            };


            var searchModalInstanceCtrl = function ($scope, $modalInstance) {

                if ($scope.adminRight == 5) {
                    $scope.circuits = scCircuit.circuits;
                } else if ($scope.adminRight == 4) {
                    $scope.circuits = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                } else if ($scope.adminRight == 3) {
                    $scope.circuits = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                } else {
                    $scope.circuits = [
                        scCircuit.circuitLookup[$scope.currentUser.level_id]
                    ];
                }

                $scope.circuitSortOrder = 'name';

                $scope.districtLookup = scDistrict.districtLookup;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };

            }


        }]);