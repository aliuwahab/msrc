/**
 * Created by KayGee on 6/16/14.
 */


schoolMonitor.controller('scStarsPanelController', ['$rootScope', '$scope', 'scUser','scNotifier', '$state', '$modal','scRegion','scDistrict',
    function($rootScope,$scope, scUser, scNotifier, $state, $modal, scRegion, scDistrict) {

        //$parent is required to be able to access the outer rootScope of the dashboard
        $scope.$parent.changePageTitle('View', 'STARS Admin', 'STARS');


    }]);
