/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewDistrictController',
    [ '$rootScope', '$scope','$filter', '$state', '$stateParams', 'scNotifier','District', '$modal', '$log',
        'scRegion','scDistrict','scCircuit', 'scSchool','$timeout',
        function($rootScope, $scope, $filter, $state, $stateParams, scNotifier, District, $modal, $log,
                 scRegion, scDistrict, scCircuit, scSchool, $timeout) {

//            $scope.scroll_to_top();

            //$parent is required to be able to access the outer rootScope of the dashboard
            $scope.$parent.changePageTitle('View', 'District', 'View');


            function loadDistricts() {
                $timeout(function () {
                    $state.reload();

                    $scope.regionLookup = scRegion.regionLookup;

                    //$scope.selected_district = scDistrict.districtLookup[$stateParams.id];
                    console.log('admin right ',$scope.adminRight);
                    if ($scope.adminRight > 4){
                        $scope.districts =  scDistrict.districts;
                        $scope.districtsSlice =  scDistrict.districts;
                        $scope.districtsSearch =  scDistrict.districts;
                        $scope.totalItems =  scDistrict.districts.length;
                    }else if ($scope.adminRight == 4){
                        $scope.districts =  scRegion.regionLookup[ $scope.currentUser.level_id ].allDistrictsHolder;
                        $scope.districtsSlice = scRegion.regionLookup[ $scope.currentUser.level_id ].allDistrictsHolder;
                        $scope.districtsSearch =  scRegion.regionLookup[ $scope.currentUser.level_id ].allDistrictsHolder;
                        $scope.totalItems =   scRegion.regionLookup[ $scope.currentUser.level_id ].totalDistricts;
                    }else{
                        $scope.districts = [
                            scDistrict.districtLookup[$scope.currentUser.level_id]
                        ] ;
                    }


                }, 10);
            }

            $scope.$on('updateLoadedModelsFromRefresh',function(){
                loadDistricts()
            });

            if (scDistrict.districts.length && scDistrict.districtLookup) {
                loadDistricts()
            }


            //this is used to sort the districts alphabetically
            $scope.districtSortOrder = 'name';



            $scope.currentPage = 1;
            $scope.maxSize = 15;
            $scope.nextPage = 0;


            //            array.slice(start,end)
            /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
             * It does not include the element at that index*/

            $scope.pageChanged = function(currentPage) {
                $scope.nextPage = ($scope.maxSize  * currentPage) - $scope.maxSize;
                $scope.districts =  ($scope.districtsSlice).slice($scope.nextPage , ($scope.nextPage + 15) );
                $filter('filter')( $scope.districts, $scope.districtSortOrder, false);
            };


            $scope.formData = {};

            $scope.paramAction = function () {
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };




//        if the action is to edit a district do this
            if ($stateParams.action == 'edit') {
                $rootScope.loading = true;
//            var data = scRegion.retrieveOneRegion($stateParams.id);
                scDistrict.retrieveOneDistrict($stateParams.id).then(
                    function(data){
                        if (data) {

                            $scope.formData = {
                                id: scDistrict.districtRetrieved.id,
                                district_name: scDistrict.districtRetrieved.name,
                                description: scDistrict.districtRetrieved.description,
                                district_email: scDistrict.districtRetrieved.email,
                                district_phone_number: scDistrict.districtRetrieved.phone_number,
                                country_id: scDistrict.districtRetrieved.country_id,
                                region: scDistrict.districtRetrieved.region_id,
                                district_location: scDistrict.districtRetrieved.lat_long
                            };
                            $rootScope.loading = false;
                        }
                    },
                    function(){
                        scNotifier.notifyFailure('Error', 'There was an error in trying to edit the region');
                        $rootScope.loading = false;
                    });

                //Else if the action is to add, set the form object to empty
            } else if ($stateParams.action == 'add') {
                $scope.formData = {};
            }

            // process the form
            $scope.processForm = function () {



                $rootScope.loading = true;
                if ($stateParams.action == 'edit') {
                    scDistrict.editDistrict($scope.formData).then(
                        function(data){
                            if (data) {
                                scNotifier.notifySuccess('Success', 'District edited successfully ');
                                $state.go('dashboard.view_district', {} , {reload : true});
                            }else{
                                scNotifier.notifyFailure('Error', 'There was an error editing the district');
                            }
                        },
                        function(){
                            scNotifier.notifyFailure('Error', 'There was an error editing the district');
                        });
                } else {
                    // process the form
                    scDistrict.addDistrict($scope.formData).then(function (status) {
                        if (status == true) {
                            $scope.formData = {};
                            scNotifier.notifySuccess('Success', 'Successfully created the district');
                            $state.go('dashboard.view_district', {} , {reload : true});
                        } else if (status == false) {
                            scNotifier.notifyFailure('Error', 'There was an error creating the district');
                        } else {
                            scNotifier.notifyFailure('Error', 'Please check the fields again');
                        }
                        $rootScope.loading = false;
                    });
                }


            };


            $scope.deleteModal = function (size, id, element) {

                var modalInstance = $modal.open({
                    templateUrl: 'partials/miscellaneous/deleteModal.html',
                    controller: ModalInstanceCtrl,
                    size: size,
                    resolve: {
                        deleteDistrictObject: function () {
                            return id;
                        }
                    }
                });

            };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

            var ModalInstanceCtrl = function ($scope, $modalInstance, deleteDistrictObject) {

                $scope.deleteObject = scDistrict.districtLookup[deleteDistrictObject];
                $scope.deleteObject.objectType = 'District';

                $scope.ok = function () {
                    $modalInstance.close('close');
//            $log.info('Modal ok\'ed at: ' + new Date());
                    scDistrict.deleteDistrict(deleteDistrictObject).then(
                        function(status){
                            if (status) {
                                scNotifier.notifySuccess('Success', 'District deleted successfully');
                                $state.go('dashboard.view_district', {} , {reload : true});
                            }else{
                                scNotifier.notifyFailure('Error', 'There was an error deleting the district');
                            }
                        },
                        function(){
                            scNotifier.notifyFailure('Error', 'There was an error deleting the district');
                        })
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
//            $log.info('Modal cancelled at: ' + new Date());
                };
            };

            $scope.searchModal = function () {

                var modalInstance = $modal.open({
                    templateUrl: 'partials/miscellaneous/searchDistrictModal.html',
                    controller: searchModalInstanceCtrl,
                    size: 'lg'
                });

            };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

            var searchModalInstanceCtrl = function ($scope, $modalInstance) {

                if ($scope.adminRight > 4){
                    $scope.districts =  scDistrict.districts;
                }else if ($scope.adminRight == 4){
                    $scope.districts =  scRegion.regionLookup[ $scope.currentUser.level_id ].allDistrictsHolder;
                }else{
                    $scope.districts = [
                        scDistrict.districtLookup[$scope.currentUser.level_id]
                    ] ;
                }

                $scope.districtSortOrder = 'name';

                $scope.regionLookup = scRegion.regionLookup;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };

            }


        }]);