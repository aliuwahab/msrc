/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedDistrictCSAttendanceController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'District', '$modal', '$log',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', '$interval',
    function($rootScope, $scope, $state, $stateParams, District, $modal, $log,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval) {

        $scope.selected_district = scDistrict.districtLookup[$stateParams.id];

        //This variable is for a single district rating's ng-show
        $scope.districtAverage = true;
        //This variable is for a comparing regional  district rating's ng-show
        $scope.compareRegionDistrictAverages = false;

        //Shuffle between a single district rating and region districts
        $scope.compareAverages =function (){
            $scope.districtAverage = false;
            $scope.compareRegionDistrictAverages = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scRegion.regionLookup[$scope.selected_district.region_id].allDistrictsHolder, 'name',
                    'Districts under ' + scRegion.regionLookup[$scope.selected_district.region_id].name, 'regionsDistrictAverageBarChart', 'cs_average');
                chart.validateNow();
            }, 100, 1)
        };


        //Shuffle between a single district rating and region districts
        $scope.singleAverage = function(){
            $scope.districtAverage = true;
            $scope.compareRegionDistrictAverages = false;
        };

        //An object to easily display between table and graph views under comparing districts
        $scope.view_to_show = {
            'chart' : true,
            'table' : false
        };

        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                $scope.view_to_show[prop] = false;
            });
            $scope.view_to_show[view_to_show] = true;
            if ( $scope.compareRegionDistrictAverages) {
                $scope.compareAverages();
            }
        };


        //Format the data to send to amCharts to 'field' and 'value'
        $scope.tempArray = [];

        //loop through schools of this district and assign the teacher attendance average
        // into objects and shove it into the temp array for display
        angular.forEach($scope.selected_district.allSchoolsHolder, function(school, index){
            var remainder = $scope.selected_district.supervisorAttendanceAverage - school.current_teacher_attendance_by_circuit_supervisor;
            var obj = {
                field: school.name,
                value: school.current_teacher_attendance_by_circuit_supervisor,
                remaining : remainder
            };

            $scope.tempArray.push(obj);
        });

        $scope.changeBarChart($scope.tempArray, $scope.selected_district.name + ' Teacher Attendance Average by Circuit Supervisors',
            'districtCsAttendanceBarChart', 'cs_average', $scope.selected_district.supervisorAttendanceAverage);

        // Assign the regions districts to a scope variable
        $scope.districtChartData = scRegion.regionLookup[$scope.selected_district.region_id].allDistrictsHolder;

    }]);

