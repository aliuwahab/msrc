/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedDistrictRatingController',  [ '$rootScope', '$scope', '$state', '$stateParams', 'District',
    '$modal', 'DataCollectorHolder',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, District, $modal, DataCollectorHolder,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        //Shuffle between a single district rating and region districts
        $scope.compareRatings =function (){
            $scope.districtRating = false;
            $scope.compareRegionDistrictRating = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scRegion.regionLookup[$scope.selected_district.region_id].allDistrictsHolder, 'name',
                    'Districts under ' + scRegion.regionLookup[$scope.selected_district.region_id].name, 'regionsDistrictRatingBarChart');
                chart.validateNow();
            }, 100, 1)
        };

        //Shuffle between a single district rating and region districts
        $scope.singleRating = function(){
            $scope.districtRating = true;
            $scope.compareRegionDistrictRating = false;
        };

        //An object to easily display between table and graph views under comparing districts
        $scope.view_to_show = {
            'chart' : true,
            'table' : false
        };
        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                $scope.view_to_show[prop] = false;
            });
            $scope.view_to_show[view_to_show] = true
        };


        function prepDistrictRating() {
            $scope.selected_district = scDistrict.districtLookup[$stateParams.id];

            //This variable is for a single district rating's ng-show
            $scope.districtRating = true;
            //This variable is for a comparing regional  district rating's ng-show
            $scope.compareRegionDistrictRating = false;


            //Prepare the district rating bar chart
            $scope.tempArray =[{
                field : 'Inclusiveness',
                value : $scope.selected_district.inclusiveness,
                remaining : 10 - $scope.selected_district.inclusiveness
            },{
                field : 'Effective Teaching and Learning',
                value : $scope.selected_district.effective_teaching_learning,
                remaining : 10 - $scope.selected_district.effective_teaching_learning
            },{
                field : 'Healthy',
                value : $scope.selected_district.healthy,
                remaining : 10 - $scope.selected_district.healthy
            },{
                field : 'Gender Friendliness',
                value : $scope.selected_district.gender_friendly,
                remaining : 10 - $scope.selected_district.gender_friendly
            },{
                field : 'Safety and Protection',
                value : $scope.selected_district.safe_protective,
                remaining : 10 - $scope.selected_district.safe_protective
            },{
                field : 'Community Involvement',
                value : $scope.selected_district.community_involvement,
                remaining : 10 - $scope.selected_district.community_involvement
            }];

            $timeout(function () {
                $scope.changeBarChart($scope.tempArray, $scope.selected_district.name + ' District Rating', 'districtRatingBarChart', 'rating');
            }, 1000, 3);

            // Assign the regions districts to a scope variable
            $scope.districtChartData = scRegion.regionLookup[$scope.selected_district.region_id].allDistrictsHolder;

        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepDistrictRating()
        });

        $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            if (data.counter == 6) {
                $timeout(function () {

                    prepDistrictRating();

                }, 10)
            }});



        if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scDistrict.districts.length) {
            prepDistrictRating()
        }

    }]);