/**
 * Created by Kaygee on 23/04/2015.
 */


schoolMonitor.controller('scViewSelectedDistrictTeachersController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'District', '$modal', 'DataCollectorHolder','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', '$interval', '$timeout','Report',
    function($rootScope, $scope, $state, $stateParams, District, $modal, DataCollectorHolder,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout, Report) {

        function  prepDistrictTeachersInfo(){

            $scope.districtTeachers = $scope.teacherSummaryData.teachersInfo;

            $scope.selected_district = scDistrict.districtLookup[$stateParams.id];

           

            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'list' : true,
                'summary' : false
            };

            $scope.compare_view_to_show = {
                'chart' : true,
                'table' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;

        }

        //This function changes between displaying the list of teachers and the grouped table
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };

        //This function changes between displaying the table or charts for comparing districts
        $scope.changeCompareView = function(view_to_show){
            angular.forEach($scope.compare_view_to_show, function(val, prop){
                //set all view to false
                $scope.compare_view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.compare_view_to_show[view_to_show] = true;
        };

        /*if the teachers are still being loaded don't show the view go to the home district view*/
        if ($scope.loadingTeachers) {
            $state.go('dashboard.view_selected_district');
            return;
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepDistrictTeachersInfo()
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scDistrict.districts.length&& scDistrict.districtLookup) {
            prepDistrictTeachersInfo()
        }

       

        //This variable is for a single district rating's ng-show
        $scope.districtAverage = true;
        //This variable is for a comparing regional  district rating's ng-show
        $scope.compareRegionsDistrictAverages = false;

        //Shuffle between a single district rating and region districts
        $scope.compareAverages =function (){
            $scope.districtAverage = false;
            $scope.compareRegionsDistrictAverages = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scRegion.regionLookup[$scope.selected_district.region_id].allDistrictsHolder, 'name',
                    'Districts under ' + scRegion.regionLookup[$scope.selected_district.region_id].name, 'regionsDistrictAverageBarChart', 'ht_average');
                chart.validateNow();
            }, 100, 1)
        };

        $scope.singleAverage = function(){
            $scope.districtAverage = true;
            $scope.compareRegionsDistrictAverages = false;
        };

        function processTeacherAttendanceAverage(loadedWeeklyData){

            $scope.teacherAttendanceSummaryData = {
                attendanceAverageInfo : loadedWeeklyData,
                districtInfo : {}
            };

            var district_totals = {
                average_teacher_attendance : {
                    head_teacher : 0,
                    circuit_supervisor : 0
                },
                average_teacher_attendance_holder : {
                    head_teacher : 0,
                    circuit_supervisor : 0
                }
            };

            angular.forEach(loadedWeeklyData, function (value, key) {

                district_totals.average_teacher_attendance_holder[value.data_collector_type] =
                    parseFloat(district_totals.average_teacher_attendance_holder[value.data_collector_type]) + parseFloat(value.average_teacher_attendance);

                district_totals.average_teacher_attendance[value.data_collector_type] =  parseFloat(district_totals.average_teacher_attendance_holder[value.data_collector_type]) / parseInt(scDistrict.districtLookup[$stateParams.id].totalSchools);

                scSchool.schoolLookup[value.school_id].average_teacher_attendance[value.data_collector_type] = value.average_teacher_attendance;
            });

            $scope.teacherAttendanceSummaryData.districtInfo = district_totals;
        }


        $scope.fetchAttendanceAverageSummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;
            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                week_number : $scope.summary_week,
                district_id : $stateParams.id
            };
            Report.totalsWeekly(data_to_post).$promise
                .then(
                /*success function*/
                function(loadedWeeklyData){
                    processTeacherAttendanceAverage(loadedWeeklyData);
                },

                /*error function*/
                function(){

                })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
        };

    }]);