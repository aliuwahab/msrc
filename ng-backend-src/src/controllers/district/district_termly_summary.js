//Created by Kaygee on 17/06/14.
schoolMonitor.controller('scViewSelectedDistrictSchoolsStatsTermlyController',  [
    '$rootScope', '$scope', '$state', '$stateParams','District', '$timeout', 'DataCollectorHolder', 'Report',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval', 'School',
    function($rootScope, $scope, $state, $stateParams, District, $timeout, DataCollectorHolder, Report,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, School) {

        function loadTermlyData(termlyDistrictData) {
            $scope.termlyDistrictData = {
                schoolInfo : termlyDistrictData,
                districtInfo : {}
            };
            var district_totals = {
                average_english_score : 0,
                average_english_score_holder : 0,

                average_ghanaian_language_score: 0,
                average_ghanaian_language_score_holder: 0,

                average_maths_score: 0,
                average_maths_score_holder: 0,

                scoring_english_above_average: 0,
                scoring_ghanaian_language_above_average: 0,
                scoring_maths_above_average: 0,

                num_english_books: 0,
                num_ghanaian_language_books: 0,
                num_maths_books: 0

            };

            angular.forEach(termlyDistrictData, function (value, key) {
                district_totals.average_english_score_holder = Number(district_totals.average_english_score_holder) + Number(value.average_english_score);
                district_totals.average_english_score =  Number(district_totals.average_english_score_holder) / $scope.selected_district.totalSchools;

                district_totals.average_ghanaian_language_score_holder = Number(district_totals.average_ghanaian_language_score_holder) + Number(value.average_ghanaian_language_score);
                district_totals.average_ghanaian_language_score =  Number(district_totals.average_ghanaian_language_score_holder)  / $scope.selected_district.totalSchools;

                district_totals.average_maths_score_holder = Number(district_totals.average_maths_score_holder) + Number(value.average_maths_score);
                district_totals.average_maths_score =  Number(district_totals.average_maths_score_holder) / $scope.selected_district.totalSchools;


                district_totals.scoring_english_above_average =  Number(district_totals.scoring_english_above_average) + Number(value.english_above_average);

                district_totals.scoring_ghanaian_language_above_average =  Number(district_totals.scoring_ghanaian_language_above_average) + Number(value.ghanaian_language_above_average);

                district_totals.scoring_maths_above_average =  Number(district_totals.scoring_maths_above_average) + Number(value.maths_above_average);


                district_totals.num_english_books =  Number(district_totals.num_english_books) + Number(value.num_english_books);

                district_totals.num_ghanaian_language_books =  Number(district_totals.num_ghanaian_language_books) + Number(value.num_ghanaian_language_books);

                district_totals.num_maths_books =  Number(district_totals.num_maths_books) + Number(district_totals.num_maths_books);


            });

            $scope.termlyDistrictData.districtInfo = district_totals
        }

        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };


        $scope.fetchTermlySummaryData = function (reloader) {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary = true;
            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                district_id : $stateParams.id
            };
            scDistrict.fetchTermlyData(data_to_post, reloader)
                .then(
                    /*success function*/
                    function(loadedWeeklyData){
                        loadTermlyData(loadedWeeklyData);
                    },

                    /*error function*/
                    function(){

                    })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
        };

        function prepDistrictSchoolTermlyStats() {
            //This variable toggle to display the loading gif if the summary tab
            $scope.loadingSummary= false;
            //A variable to check if data has been loaded before, so we change the instruction on the summary tab
            $scope.initialSummaryInfoCount = 0;

            $scope.schoolYears_Termly = [];
            $scope.schoolTerms_Termly = [];

            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'display_average' : true,
                'display_textbooks' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepDistrictSchoolTermlyStats()
        });

        if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scDistrict.districts.length) {
            prepDistrictSchoolTermlyStats()
        }


        $scope.updateTextBooks = function () {
            $scope.loadingSummary= true;

            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                district_id : $stateParams.id
            };

            School.allSchoolTextbook(data_to_post).$promise
                .then(function (data) {
                    if (data) {
                        // console.log('textbook data returned', data);
                        // loadedTxtBkInfoFromServer = data[0].school_textbooks;
                        prepTextBookTotals(data[0].school_textbooks);
                        $scope.loadingSummary= false;
                    }
                }, function(){
                    $scope.loadingSummary= false;
                });
        };

        function prepTextBookTotals(loadedTextArray) {
            $scope.updatingTotals = true;

            var schoolHolder = {};

            angular.forEach(loadedTextArray, function (texBookClassItem, index) {
                if (schoolHolder[texBookClassItem.school_id]) {
                    schoolHolder[texBookClassItem.school_id].num_english_books += Number(texBookClassItem.number_of_english_textbooks);
                    schoolHolder[texBookClassItem.school_id].num_maths_books += Number(texBookClassItem.number_of_maths_textbooks);
                    schoolHolder[texBookClassItem.school_id].num_ghanaian_language_books += Number(texBookClassItem.number_of_ghanaian_language_textbooks);
                }else{
                    schoolHolder[texBookClassItem.school_id] = {
                        num_english_books  : Number(texBookClassItem.number_of_english_textbooks),
                        num_maths_books : Number(texBookClassItem.number_of_maths_textbooks),
                        num_ghanaian_language_books : Number(texBookClassItem.number_of_ghanaian_language_textbooks),
                        year: texBookClassItem.year,
                        term: texBookClassItem.term,
                        district_id: texBookClassItem.district_id,
                        school_id: texBookClassItem.school_id
                    }
                }
            });


            // var selected_school = $scope.schoolLookup[school_id];
            var holder = [];
            angular.forEach(schoolHolder, function (schoolItem, key) {
                holder.push({
                    num_ghanaian_language_books: schoolItem.num_ghanaian_language_books,
                    num_english_books: schoolItem.num_english_books,
                    num_maths_books: schoolItem.num_maths_books,
                    year: schoolItem.year,
                    term: schoolItem.term,
                    district_id: schoolItem.district_id,
                    school_id: schoolItem.school_id
                })
            });

            scSchool.updateTermlyTextbookData(holder)
                .success(function (successData) {
                    if (successData.status === 200) {
                        scNotifier.notifySuccess("Recalculation", "Schools Textbooks totals have been updated.");
                        $scope.fetchTermlySummaryData(true);
                    }
                })
                .error(function (errData) {
                    scNotifier.notifyFailure("Recalculation", "Schools Textbooks totals failed to update")
                })
                .finally(function () {
                    $timeout(function () {
                        $scope.updatingTotals = false;
                    });
                });

            /*
                $table->integer('average_ghanaian_language_score')->default(0);
                $table->integer('ghanaian_language_above_average')->default(0);
                $table->integer('average_english_score')->default(0);
                $table->integer('english_above_average')->default(0);
                $table->integer('average_maths_score')->default(0);
                $table->integer('maths_above_average')->default(0);
    */
            $scope.loadingSummary = false;

        }



        $scope.updatePupilPerformance = function () {
            $scope.loadingSummary= true;

            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                district_id : $stateParams.id
            };

            School.allSchoolPupilPerformance(data_to_post).$promise
                .then(function (data) {
                    if (data) {
                        console.log('allSchoolPupilPerformance', data);
                        prepPupilPerformanceTotals(data[0].pupils_performance);
                        $scope.loadingSummary= false;
                    }
                }, function(){
                    $scope.loadingSummary= false;
                });
        };

        function prepPupilPerformanceTotals(loadedPerformanceArray) {
            console.log('loadedPerformanceArray : ', loadedPerformanceArray);
            $scope.updatingTotals = true;

            var schoolHolder = {};

            angular.forEach(loadedPerformanceArray, function (performanceClassItem, index) {
                if (schoolHolder[performanceClassItem.school_id]) {
                    schoolHolder[performanceClassItem.school_id].english_average_score += Number(performanceClassItem.average_english_score);
                    schoolHolder[performanceClassItem.school_id].maths_average_score += Number(performanceClassItem.average_maths_score);
                    schoolHolder[performanceClassItem.school_id].gh_lang_average_score += Number(performanceClassItem.average_ghanaian_language_score);
                    schoolHolder[performanceClassItem.school_id].number_of_classes ++ ;
                    // schoolHolder[performanceClassItem.school_id].science_average_score += Number(performanceClassItem.average_science_score);
                    // schoolHolder[performanceClassItem.school_id].num_pupil_who_can_read_and_write += Number(performanceClassItem.num_pupil_who_can_read_and_write);
                    schoolHolder[performanceClassItem.school_id].english_above_average += Number(performanceClassItem.num_pupil_who_score_above_average_in_english);
                    schoolHolder[performanceClassItem.school_id].maths_above_average += Number(performanceClassItem.num_pupil_who_score_above_average_in_maths);
                    schoolHolder[performanceClassItem.school_id].ghanaian_language_above_average += Number(performanceClassItem.num_pupil_who_score_above_average_in_ghanaian_language);

                }else{
                    schoolHolder[performanceClassItem.school_id] = {
                        english_average_score  : Number(performanceClassItem.average_english_score),
                        maths_average_score : Number(performanceClassItem.average_maths_score),
                        gh_lang_average_score : Number(performanceClassItem.average_ghanaian_language_score),
                        // science_average_score : Number(performanceClassItem.science_average_score),
                        // num_pupil_who_can_read_and_write : Number(performanceClassItem.num_pupil_who_can_read_and_write),

                        english_above_average : performanceClassItem.num_pupil_who_score_above_average_in_english,
                        maths_above_average : performanceClassItem.num_pupil_who_score_above_average_in_maths,
                        ghanaian_language_above_average : performanceClassItem.num_pupil_who_score_above_average_in_ghanaian_language,

                        year: performanceClassItem.year,
                        number_of_classes : 1, /*Used to calculate the average by total classes*/
                        term: performanceClassItem.term,
                        district_id: performanceClassItem.district_id,
                        school_id: performanceClassItem.school_id
                    }
                }
            });


            var holder = [];
            angular.forEach(schoolHolder, function (schoolItem, key) {
                holder.push({
                    average_english_score: (schoolItem.english_average_score / schoolItem.number_of_classes).toFixed(2),
                    average_maths_score: (schoolItem.maths_average_score / schoolItem.number_of_classes).toFixed(2),
                    average_ghanaian_language_score: (schoolItem.gh_lang_average_score / schoolItem.number_of_classes).toFixed(2),
                    ghanaian_language_above_average :schoolItem.ghanaian_language_above_average,
                    english_above_average : schoolItem.english_above_average,
                    maths_above_average : schoolItem.maths_above_average,

                    year: schoolItem.year,
                    term: schoolItem.term,
                    district_id: schoolItem.district_id,
                    school_id: schoolItem.school_id
                })
            });

            scSchool.updateTermlyPupilPerformanceData(holder)
                .success(function (successData) {
                    if (successData.status === 200) {
                        scNotifier.notifySuccess("Recalculation", "Schools Pupil Performance totals have been updated.")
                    }
                })
                .error(function (errData) {
                    scNotifier.notifyFailure("Recalculation", "Schools Pupil Performance totals failed to update")
                })
                .finally(function () {
                    $timeout(function () {
                        $scope.updatingTotals = false;
                        $scope.loadingSummary = false;
                    });
                });


        }

    }]);
