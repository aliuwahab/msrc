/**
 * Created by Kaygee on 23/04/2015.
 */
schoolMonitor.controller('scViewSelectedDistrictController',
    [ '$rootScope', '$scope', '$state', '$stateParams', 'District', '$modal', 'RecentSubmissionService','$timeout',
        'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', 'Timelines','DataCollectorHolder',
        function($rootScope, $scope, $state, $stateParams, District, $modal, RecentSubmissionService, $timeout,
                 scNotifier, scRegion, scDistrict, scCircuit, scSchool, Timelines, DataCollectorHolder ) {

            $scope.changePageTitle('View', 'District', 'View');

            $scope.loadItinerary = function(reloader){
                $scope.loadingItinerary = true;
                scDistrict.fetchItinerary($stateParams.id, reloader)
                    .then(function (allDistrictsItinerary) {
                        $scope.districtItineraries = allDistrictsItinerary.itinerary;
                        $scope.loadingItinerary = false;
                    });
            };

            $scope.districtTerms = [];
            $scope.districtYears = [];
            $scope.districtWeeks = [];
            $scope.districtWeeksTrendHolderTracker = [];//this only ensures we don't save one week twice
            $scope.districtWeeksTrendHolder = [];//this holds the weeks to be displayed in the trends

            function prepDistrictData(){

                $scope.loadItinerary(false);

                $scope.selected_district = scDistrict.districtLookup[$stateParams.id];

                $scope.districtTerms = [];
                $scope.districtYears = [];
                $scope.districtWeeks = [];
                $scope.districtWeeksTrendHolderTracker = [];//this only ensures we don't save one week twice
                $scope.districtWeeksTrendHolder = [];//this holds the weeks to be displayed in the trends


                $scope.average_teacher_attendance = 0;

                //This helps the DataCollectors Tab to show only the filtered data collectors
                if (scDistrict.districtLookup[$stateParams.id]){
                    $scope.dataCollectors = {
                        head_teachers : scDistrict.districtLookup[$stateParams.id].head_teachers_holder,
                        circuit_supervisors : scDistrict.districtLookup[$stateParams.id].circuit_supervisors_holder,
                        all : scDistrict.districtLookup[$stateParams.id].head_teachers_holder.concat(scDistrict.districtLookup[$stateParams.id].circuit_supervisors_holder)
                    };

                    //Assign an initial data collector set to the view
                    $scope.districtDataCollectors = $scope.dataCollectors['all'];

                }



                //This object is used to change the type to a readable format,
                // It is used when there are no data collectors of a particular type
                $scope.data_collector_types_to_filter = {
                    all : 'data collectors',
                    circuit_supervisors : 'circuit supervisors',
                    head_teachers : 'head teachers'
                };
                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;

            }

            $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
                if (data.counter == 6) {
                    $timeout(function () {

                        checkDistrictsAreLoadAndPrepDistricts();

                    }, 10)
                }});


            $scope.totalStudentsInDistrict_ht = 0;
            $scope.totalStudentsInDistrict_cs = 0;


            function checkDistrictsAreLoadAndPrepDistricts() {
                if (!(_.isEmpty(scDistrict.districtLookup)) && angular.isDefined(scDistrict.districtLookup[$stateParams.id])) {
                    prepDistrictData();
                } else {
                    $timeout(function () {
                        checkDistrictsAreLoadAndPrepDistricts();
                    }, 2500)
                }
            }


            /*Starts from here*/
            if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scDistrict.districts.length) {
                checkDistrictsAreLoadAndPrepDistricts()
            }

            $scope.$on('updateItinerary', function () {
                $scope.loadItinerary(true);
            });

            $scope.showDataCollectors = function(type){
                //Remove all the multiple checked boxes
                $('input:checked').attr('checked', false);

                //reset the datacollectors array
                $scope.dataCollectors_to_send_messages_to = [];

                if ($scope.data_collector_types_to_filter) {
                    $scope.data_collector_type = $scope.data_collector_types_to_filter[type];
                }

                //change the data collectors being displayed by type
                if ($scope.dataCollectors) {
                    $scope.districtDataCollectors =   $scope.dataCollectors[type];
                }else{
                    $scope.districtDataCollectors =  {};
                }
            };

            $scope.loadingTeachers = true;

            function prepDistrictTeachers(allSchoolTeachersData) {
                if (!$scope.selected_district) {
                    checkDistrictsAreLoadAndPrepDistricts();
                    return;
                }

                $timeout(function () {
                    $scope.teacherSummaryData = {
                        teachersInfo : allSchoolTeachersData[0].school_teachers,
                        districtInfo : {
                            teachers_total : 0,
                            male_total : 0,
                            female_total : 0,
                            averageTeacherAttendance : $scope.average_teacher_attendance,
                            trained : 0,
                            atpost : 0,
                            studyleave : 0,
                            otherleave : 0,
                            retired : 0,
                            deceased : 0,
                            percentage_trained : 0
                        }
                    };

                    var schoolTeacherHolder = {
                        teachersInSchoolHolder : []
                    };

                    /*calculate totals with a variable. using the school directly, increases it on each page load*/
                    var schoolTeacherTotalTracker = {};

                    angular.forEach($scope.teacherSummaryData.teachersInfo, function (teacher, key) {
                        if (teacher.class_taught == null || teacher.class_taught == 'null') teacher.class_taught = '';
                        if (teacher.class_subject_taught == null ||teacher.class_subject_taught == 'null') teacher.class_subject_taught = '';

                        teacher.subject = ($.trim(teacher.class_subject_taught +  teacher.class_taught)).split(',')[0];

                        /*whole district totals*/
                        if (teacher.gender == 'male') {
                            $scope.teacherSummaryData.districtInfo.male_total ++;
                        }else $scope.teacherSummaryData.districtInfo.female_total++;

                        /*assign a name property for filtering*/
                        teacher.full_name = teacher.first_name + teacher.last_name;

                        $scope.teacherSummaryData.districtInfo.teachers_total ++;
                        $scope.teacherSummaryData.districtInfo[teacher.teacher_status]++;


                        /*Calculate for professional teachers*/
                        // Professional teachers are those with professional qualifications

                        if (angular.isUndefined(teacher.highest_professional_qualification)  || teacher.highest_professional_qualification === '' || teacher.highest_professional_qualification.toLowerCase() === 'others' || teacher.highest_professional_qualification.toLowerCase() === 'other') {

                        }else{
                            $scope.teacherSummaryData.districtInfo.trained ++;
                            if (isFinite(Number($scope.teacherSummaryData.districtInfo.trained / $scope.teacherSummaryData.districtInfo.teachers_total))) {
                                $scope.teacherSummaryData.districtInfo.percentage_trained = parseInt(($scope.teacherSummaryData.districtInfo.trained / $scope.teacherSummaryData.districtInfo.teachers_total) * 100);
                            }
                        }


                        /*per school totals*/
                        if (schoolTeacherTotalTracker[teacher.school_id] !== undefined) {
                            schoolTeacherTotalTracker[teacher.school_id].totalTeachers ++;
                            schoolTeacherTotalTracker[teacher.school_id].totalMaleTeachers ++;
                            schoolTeacherTotalTracker[teacher.school_id].totalFemaleTeachers ++;
                            schoolTeacherTotalTracker[teacher.school_id].teacherObjects[teacher.id] = teacher;


                            if ($scope.selected_district.allSchools[teacher.school_id] && $scope.selected_district.allSchools[teacher.school_id] !== undefined) {
                                if (teacher.gender === 'male') {
                                    $scope.selected_district.allSchools[teacher.school_id].totalMaleTeachers ++;
                                }else{
                                    $scope.selected_district.allSchools[teacher.school_id].totalFemaleTeachers ++;
                                }
                            }
                        }else{
                            schoolTeacherTotalTracker[teacher.school_id] ={
                                totalTeachers : 1,
                                school_id : teacher.school_id,
                                teacherObjects : {}
                            };
                            schoolTeacherTotalTracker[teacher.school_id].teacherObjects[teacher.id] = teacher;


                            if (teacher.gender === 'male') {
                                schoolTeacherTotalTracker[teacher.school_id].totalMaleTeachers = 1;
                            }else{
                                schoolTeacherTotalTracker[teacher.school_id].totalFemaleTeachers = 1;
                            }
                        }

                        if (schoolTeacherHolder[teacher.school_id] !== undefined) {
                            schoolTeacherHolder[teacher.school_id].teachersInSchoolHolder.push(teacher);
                        }else{
                            schoolTeacherHolder[teacher.school_id] = {
                                teachersInSchoolHolder : []
                            };
                            schoolTeacherHolder[teacher.school_id].teachersInSchoolHolder.push(teacher);
                        }

                    });

                    angular.forEach(schoolTeacherHolder, function (value, key) {

                        if (scSchool.schoolLookup[key] !== undefined) {
                            scSchool.schoolLookup[key].teachersInSchoolHolder = value.teachersInSchoolHolder;

                        }
                    });

                    angular.forEach(schoolTeacherTotalTracker, function (value, prop) {
                        if ( $scope.selected_district.allSchools[value.school_id] !== undefined) {
                            $scope.selected_district.allSchools[value.school_id].totalTeachers = value.totalTeachers;
                            $scope.selected_district.allSchools[value.school_id].totalMaleTeachers = value.totalMaleTeachers;
                            $scope.selected_district.allSchools[value.school_id].totalFemaleTeachers = value.totalFemaleTeachers;
                        }
                    });

                    $scope.schoolTeacherTotalTracker = angular.copy(schoolTeacherTotalTracker);

                    $scope.loadingTeachers = false;
                }, 1);
            }
            scSchool.allSchoolTeachersData({district_id : $stateParams.id})
                .success(function (allSchoolTeachersData) {
                    prepDistrictTeachers(allSchoolTeachersData);
                });

            $scope.$on('teachersUpdated', function(evt, teachersObject){
                prepDistrictTeachers(teachersObject);
            });

//This variable shows the checkbox to select multiple data collectors to send to
            $scope.selectMultiple = false;

//This variable holds data collectors to receive a message
            $scope.dataCollectors_to_send_messages_to = [];

//This function checks whether a data collector is already added
            $scope.addRemoveCollector = function(id_of_checked){
                var index_of_personnel = $scope.dataCollectors_to_send_messages_to.indexOf(id_of_checked);
                if (index_of_personnel < 0) {
                    $scope.dataCollectors_to_send_messages_to.push(id_of_checked);
                }else{
                    //array.splice(index,how many,item1,.....,itemX)
                    $scope.dataCollectors_to_send_messages_to.splice(index_of_personnel, 1);
                }
            };

            //Create Itinerary Modal
            $scope.createItineraryModal = function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/addItineraryModal.html',
                    controller: CreateItineraryModalCtrl,
                    size: 'md',
                    resolve: {
                        selectedDistrict: function () {
                            return $scope.selected_district;
                        }
                    }
                });
            };

            var CreateItineraryModalCtrl = function ($scope, $modalInstance, selectedDistrict) {
                $scope.itineraryFormData = {};

                //this variable disables the submit button when the form is posted
                $scope.isSubmitting = false;

                $scope.startOpened = false;
                $scope.endOpened = false;

                $scope.itineraryAction = "Add";

                $scope.disabled = function(date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                };

                $scope.minDate =  new Date();
                $scope.maxDate =  '31-12-2020';

                $scope.startOpen = function($event) {
                    $scope.startOpened = true;
                    $event.preventDefault();
                    $event.stopPropagation();
                };

                $scope.endOpen = function($event) {
                    $scope.endOpened = true;
                    $event.preventDefault();
                    $event.stopPropagation();
                };

                $scope.resetEndOpen= function(){
                    $scope.endOpened = true;
                };

                $scope.dateOptions = {
                    formatYear: 'yyyy',
                    startingDay: 1
//        initDate :  $scope.minDate
                };

                $scope.initDate = new Date('2016-15-20');

                $scope.ok = function () {
                    $scope.isSubmitting = true;
                    if ($scope.itineraryFormData.start_date > $scope.itineraryFormData.end_date) {
                        $scope.errorMessage = "Start date cannot be after end date.";
                        $scope.isSubmitting = false;
                        return false;
                    }

                    if (!($scope.itineraryFormData.name && $scope.itineraryFormData.description &&
                            $scope.itineraryFormData.start_date && $scope.itineraryFormData.end_date)) {

                        $scope.errorMessage = "All fields are required. Please check if any is not left blank.";
                        $scope.isSubmitting = false;
                        return false;

                    }
                    //Assign the region and district id of to the form being posted
                    $scope.itineraryFormData.region_id = selectedDistrict.region_id;
                    $scope.itineraryFormData.district_id = selectedDistrict.id;

                    District.createItinerary($scope.itineraryFormData,
                        //success callback
                        function (value, responseHeaders) {
                            scNotifier.notifySuccess('Itinerary', 'Successfully created');
                            $rootScope.$broadcast('updateItinerary');
                            $modalInstance.dismiss('cancel');
                        },
                        //error callback
                        function (httpResponse){
                            $scope.isSubmitting = false;
                            scNotifier.notifyFailure('Itinerary', 'There was an error in creating the district itinerary');
                        });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            };

            //Edit Itinerary Modal
            $scope.editItinerary = function (itineraryObject) {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/addItineraryModal.html',
                    controller: EditItineraryModalCtrl,
                    size: 'md',
                    resolve: {
                        itineraryObject : function () {
                            return itineraryObject;
                        }
                    }
                });
            };

            var EditItineraryModalCtrl= function ($scope, $modalInstance, itineraryObject) {
                $scope.itineraryFormData = {};
                angular.extend( $scope.itineraryFormData, itineraryObject );

                //this variable disables the submit button when the form is posted
                $scope.isSubmitting = false;

                $scope.startOpened = false;

                $scope.itineraryAction = "Edit";

                $scope.disabled = function(date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                };

                $scope.minDate =  new Date();
                $scope.maxDate =  '31-12-2099';

                $scope.startOpen = function($event) {
                    $scope.startOpened = true;
                    $event.preventDefault();
                    $event.stopPropagation();
                };

                $scope.endOpen = function($event) {
                    $scope.endOpened = true;
                    $event.preventDefault();
                    $event.stopPropagation();
                };

                $scope.resetEndOpen= function(){
                    $scope.endOpened = true;
                };

                $scope.dateOptions = {
                    formatYear: 'yyyy',
                    startingDay: 1
//        initDate :  $scope.minDate
                };

                $scope.ok = function () {
                    $scope.isSubmitting = true;
                    //Assign the region and district id of to the form being posted

                    District.editAnItinerary($scope.itineraryFormData,
                        //success callback
                        function (value, responseHeaders) {
                            scNotifier.notifySuccess('Itinerary', 'Successfully edited and saved');
                            $rootScope.$broadcast('updateItinerary');
                            $modalInstance.dismiss('cancel');

                        },
                        //error callback
                        function (httpResponse){
                            scNotifier.notifyFailure('Itinerary', 'There was an error in editing the district itinerary');
                            $scope.isSubmitting = false;
                        });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            };

            //Delete Itinerary Modal
            $scope.deleteItinerary = function (itineraryObject) {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/addItineraryModal.html',
                    controller: DeleteItineraryModalCtrl,
                    size: 'md',
                    resolve: {
                        itineraryObject : function () {
                            return itineraryObject;
                        }
                    }
                });
            };

            var DeleteItineraryModalCtrl= function ($scope, $modalInstance, itineraryObject) {
                $scope.itineraryFormData = {};
                angular.extend( $scope.itineraryFormData, itineraryObject );

                //this variable disables the submit button when the form is posted
                $scope.isSubmitting = false;

                $scope.startOpened = false;
                $scope.endOpened = false;

                $scope.itineraryAction = "Delete";

                $scope.disabled = function(date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                };

                $scope.ok = function () {
                    //this variable disables the submit button when the form is posted
                    $scope.isSubmitting = true;
                    District.deleteAnItinerary($scope.itineraryFormData,
                        //success callback
                        function (value, responseHeaders) {
                            scNotifier.notifySuccess('Itinerary', 'Successfully deleted');
                            $rootScope.$broadcast('updateItinerary');
                            $modalInstance.dismiss('cancel');

                        },
                        //error callback
                        function (httpResponse){
                            scNotifier.notifyFailure('Itinerary', 'There was an error in deleting the district itinerary');
                            //this variable disables the submit button when the form is posted
                            $scope.isSubmitting = false;
                        });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            };

            Timelines.getAvailableTimeline({category : 'normal_students_enrolment', district_id : $stateParams.id})
                .then(function (availableTimelines) {
                    for (var i = 0; i < availableTimelines.data.length; i++) {
                        var item = availableTimelines.data[i];
                        //save the current term in the loop to a variable
                        //one school term is an object that formats the format returned
                        // from the server to a user friendlier one
                        var currentTerm = $scope.one_school_terms[item.term];

                        //save the current year in the loop to a variable
                        var currentYear = item.year;

                        //save the current week in the loop to a variable
                        var currentWeek = item.week_number;

                        // check if the current term isn't already added to the scope variable that displays the available terms
                        //in the submitted data
                        if ($scope.districtTerms.indexOf(currentTerm) < 0) {

                            $scope.districtTerms.push(currentTerm);

                            $scope.districtTerms.sort(function(a, b){
                                return a.number.charAt(0) - b.number.charAt(0)
                            });
                        }

                        // check if the current year isn't already added to the scope variable that displays the available years
                        //in the submitted data
                        if ($scope.districtYears.indexOf(currentYear) < 0) {
                            $scope.districtYears.push(currentYear);
                        }

                        // check if the current week isn't already added to the scope variable that displays the available weeks
                        //in the submitted data
                        if ($scope.districtWeeks.indexOf(currentWeek) < 0) {
                            $scope.districtWeeks.push(currentWeek);

                            $scope.districtWeeks.sort(function(a,b){
                                return a - b
                            });
                        }

                        // check if the current week isn't already added to the scope variable
                        // for the data trending in the submitted data
                        if ($scope.districtWeeksTrendHolderTracker.indexOf(currentYear+currentTerm.value+currentWeek) < 0) {
                            $scope.districtWeeksTrendHolder.push({
                                week: currentWeek,
                                term: currentTerm.number,
                                termValue: currentTerm.value,
                                termObj: currentTerm,
                                year: currentYear
                            });
                            $scope.districtWeeksTrendHolder.sort(function (a, b) {
                                return a.week - b.week
                            });

                            $scope.districtWeeksTrendHolderTracker.push(currentYear + currentTerm.value + currentWeek);
                        }

                            $scope.summary_term =  item.term;

                        $scope.summary_week = item.week_number;

                        $scope.summary_year =  item.year;

                        $scope.summary_data_collector_type = item.data_collector_type || "head_teacher";

                    }
                });

            RecentSubmissionService.getAllRecentSubmissions('district', $stateParams.id)
                .then(function (districtRecentSubmissions) {

                    $scope.districtRecentSubmissions = districtRecentSubmissions;

                    $scope.totalItems = districtRecentSubmissions.length;
                    $scope.currentPage = 1;
                    $scope.maxSize = 3;
                    $scope.nextPage = 0;

                    $scope.setPage = function (pageNo) {
                        $scope.currentPage = pageNo;
                    };

//            array.slice(start,end)
                    /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
                     * It does not include the element at that index*/

                    $scope.pageChanged = function() {
                        $scope.nextPage = ($scope.maxSize  * $scope.currentPage) - $scope.maxSize;
                        $scope.districtRecentSubmissions = districtRecentSubmissions.slice($scope.nextPage);
                    };

                })

        }]);
