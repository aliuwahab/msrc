//Created by Kaygee on 17/06/14.

schoolMonitor.controller('scViewSelectedDistrictSchoolsStatsController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'District', 'School', 'scReportGenerator','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', 'scGlobalService', 'msrc_datauri_logo',
    '$interval',
    function($rootScope, $scope, $state, $stateParams, District, School, scReportGenerator,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, scGlobalService, msrc_datauri_logo) {


        $scope.loadDistrictStats = function () {
            if (!$scope.summary_term && !$scope.summary_year) {
                scNotifier.notifyInfo('No Period Specified', 'Set Term and Year to proceed.');
                return;
            }
            var dataParams = {
                data_collector_type: "head_teacher",
                term: $scope.summary_term,
                year: $scope.summary_year,
                district_id: $stateParams.id
            };
            scReportGenerator.loadStats(dataParams);
            $scope.loading = true;
            $scope.isNotVirgin = true;
        };


        $scope.$on('pupilPerformanceDataLoaded', function (event, allDistrictPupilPerformanceData) {
            var districtPerformanceTotals = {
                english: {
                    class_score: {}
                },
                math: {
                    class_score: {}
                },
                gh_lang: {
                    class_score: {}
                }
            };

            var performanceBarChartHolder = {};

            angular.forEach(allDistrictPupilPerformanceData, function (item, index) {

                /*
                * average_english_score:"50"
                    average_ghanaian_language_score:"50"
                    average_maths_score:"50"
                    data_collector_type:"head_teacher"
                    district_id:97
                    level:"KG1"
                    num_pupil_who_score_above_average_in_english:24
                    num_pupil_who_score_above_average_in_ghanaian_language:20
                    num_pupil_who_score_above_average_in_maths:24
                    term:"third_term"
                    year:"2015/2016"
                    */
                //find the average score for the district, by subject
                //so for each item
                //add the averages of each subject, class by class
                //add the class in one property under the subject


                if (isFinite(Number(item.average_english_score))) {
                    if (angular.isDefined(districtPerformanceTotals.english.class_score[item.level])) {
                        districtPerformanceTotals.english.class_score[item.level].total_score += Number(item.average_english_score);
                        districtPerformanceTotals.english.class_score[item.level].average_score = Math.round(districtPerformanceTotals.english.class_score[item.level].total_score / $scope.selected_district.totalSchools);
                    } else {
                        districtPerformanceTotals.english.class_score[item.level] = {
                            total_score: Number(item.average_english_score),
                            average_score: Math.round(item.average_english_score / $scope.selected_district.totalSchools)
                        }
                    }
                }


                if (isFinite(Number(item.average_maths_score))) {
                    if (angular.isDefined(districtPerformanceTotals.math.class_score[item.level])) {

                        districtPerformanceTotals.math.class_score[item.level].total_score += Number(item.average_maths_score);
                        districtPerformanceTotals.math.class_score[item.level].average_score = Math.round(districtPerformanceTotals.math.class_score[item.level].total_score / $scope.selected_district.totalSchools);
                    } else {
                        districtPerformanceTotals.math.class_score[item.level] = {
                            total_score: Number(item.average_maths_score),
                            average_score: Math.round(item.average_maths_score / $scope.selected_district.totalSchools)
                        }
                    }
                }


                if (isFinite(Number(item.average_ghanaian_language_score))) {
                    if (angular.isDefined(districtPerformanceTotals.gh_lang.class_score[item.level])) {
                        districtPerformanceTotals.gh_lang.class_score[item.level].total_score += Number(item.average_ghanaian_language_score);
                        districtPerformanceTotals.gh_lang.class_score[item.level].average_score = Math.round(districtPerformanceTotals.gh_lang.class_score[item.level].total_score / $scope.selected_district.totalSchools);
                    } else {
                        districtPerformanceTotals.gh_lang.class_score[item.level] = {
                            total_score: Number(item.average_ghanaian_language_score),
                            average_score: Math.round(item.average_ghanaian_language_score / $scope.selected_district.totalSchools)
                        }
                    }
                }

                performanceBarChartHolder[item.level] = {
                    gh_lang : {
                        average_score : districtPerformanceTotals.gh_lang.class_score[item.level].average_score,
                        above_average : districtPerformanceTotals.gh_lang.class_score[item.level].above_average
                    },
                    english : {
                        average_score : districtPerformanceTotals.english.class_score[item.level].average_score,
                        above_average : districtPerformanceTotals.english.class_score[item.level].above_average
                    },
                    math : {
                        average_score : districtPerformanceTotals.math.class_score[item.level].average_score,
                        above_average : districtPerformanceTotals.math.class_score[item.level].above_average
                    }
                }

                //then add for the total in another property under the subject
                //

            });
            var performanceBarChart = {
                label : "Pupil Performance By Class",
                labels : [],
                datasets : []
            };
            var langHolder = {
                gh_lang : [],
                gh_lang_line : [],
                english : [],
                english_line : [],
                math : [],
                math_line: []
            };

            /*get the class key*/
            /*get all the language datasets*/
            /*get the above average data for the line*/
            /*get one object with dataset title, class by class and language score per class*/

            /*get the class key*/
            angular.forEach(performanceBarChartHolder, function (perfData, classKey) {
                performanceBarChart.labels.push(classKey);
                /*get all the language datasets*/
                langHolder.gh_lang.push(perfData.gh_lang.average_score);
                langHolder.gh_lang_line.push(perfData.gh_lang.above_average);
                langHolder.english.push(perfData.english.average_score);
                langHolder.english_line.push(perfData.english.above_average);
                langHolder.math.push(perfData.math.average_score);
                langHolder.math_line.push(perfData.math.above_average);
            });

            performanceBarChart.datasets.push({
                label: 'Ghanaian Language',
                backgroundColor: pattern.draw('triangle-inverted', 'rgba(231, 235, 8, 0.75)'),
                data:langHolder.gh_lang
            });

            performanceBarChart.datasets.push({
                label: 'English',
                backgroundColor: pattern.draw('diagonal-right-left', 'rgba(16, 204, 13, 0.75)'),
                data:langHolder.english
            });
            performanceBarChart.datasets.push({
                label: 'Maths',
                backgroundColor: pattern.draw('dot-dash', 'rgba(75, 192, 192, 0.75)'),
                data: langHolder.math
            });


            scGlobalService.drawStatisticsBarStackedChart(performanceBarChart.labels,
                performanceBarChart.label, performanceBarChart.datasets,
                'performanceBarChart', "horizontalBarChart");


            $scope.districtPerformanceTotals = angular.copy(districtPerformanceTotals);
            $scope.districtPerformanceTotals.loaded = true;
        });




        $scope.$on('pupilEnrollmentDataLoaded', function (event, allDistrictPupilEnrollmentData) {

            var districtEnrollmentTotals = {
                total_male: 0,
                total_female: 0,
                total_pupils: 0,
                total_by_class: {}
            };

            $scope.enrollmentBarChart = {
                label : "Pupil Enrollment By Class",
                labels : [],
                chartData : []
            };

            var schoolIdWeekTracker = {};
            angular.forEach(allDistrictPupilEnrollmentData, function (item, index) {

                if (schoolIdWeekTracker[item.school_id]) {

                    if (Number(schoolIdWeekTracker[item.school_id].lastWeekOfSubmission) === Number(item.week_number)) {
                        schoolIdWeekTracker[item.school_id].lastWeekOfSubmission = item.week_number;

                        if (!schoolIdWeekTracker[item.school_id].classes[item.level]) schoolIdWeekTracker[item.school_id].classes[item.level] = {
                            total_male : 0,
                            total_female : 0,
                            total_pupils : 0
                        };

                        schoolIdWeekTracker[item.school_id].classes[item.level].total_male += Number(item.num_males);
                        schoolIdWeekTracker[item.school_id].classes[item.level].total_female += Number(item.num_females);
                        schoolIdWeekTracker[item.school_id].classes[item.level].total_pupils += Number(item.total_enrolment);


                        schoolIdWeekTracker[item.school_id].total_male += item.num_males;
                        schoolIdWeekTracker[item.school_id].total_female += item.num_females;
                        schoolIdWeekTracker[item.school_id].total_pupils += item.total_enrolment;

                    }else{
                        if (Number(item.week_number) > Number(schoolIdWeekTracker[item.school_id].lastWeekOfSubmission)) {
                            schoolIdWeekTracker[item.school_id].lastWeekOfSubmission = item.week_number;

                            if (!schoolIdWeekTracker[item.school_id].classes[item.level]) schoolIdWeekTracker[item.school_id].classes[item.level] = {
                                total_male : 0,
                                total_female : 0,
                                total_pupils : 0
                            };

                            schoolIdWeekTracker[item.school_id].classes[item.level].total_male = Number(item.num_males);
                            schoolIdWeekTracker[item.school_id].classes[item.level].total_female = Number(item.num_females);
                            schoolIdWeekTracker[item.school_id].classes[item.level].total_pupils = Number(item.total_enrolment);


                            schoolIdWeekTracker[item.school_id].total_male = item.num_males;
                            schoolIdWeekTracker[item.school_id].total_female = item.num_females;
                            schoolIdWeekTracker[item.school_id].total_pupils = item.total_enrolment;
                        }
                    }
                }
                else {

                    schoolIdWeekTracker[item.school_id] = {
                        lastWeekOfSubmission: item.week_number,
                        classes: {},
                        total_male: Number(item.num_males),
                        total_female: Number(item.num_females),
                        total_pupils: Number(item.total_enrolment)
                    };

                    schoolIdWeekTracker[item.school_id].classes[item.level] = {
                        total_male: Number(item.num_males),
                        total_female: Number(item.num_females),
                        total_pupils: Number(item.total_enrolment)
                    }
                }
            });


            angular.forEach(schoolIdWeekTracker, function (enrolObjVal, schIdProp) {
                if (angular.isDefined(scSchool.schoolLookup[schIdProp])) {
                    scSchool.schoolLookup[schIdProp].testEnrollment =  enrolObjVal.total_pupils;
                }
                districtEnrollmentTotals.total_male += enrolObjVal.total_male;
                districtEnrollmentTotals.total_female += enrolObjVal.total_female;
                districtEnrollmentTotals.total_pupils += enrolObjVal.total_pupils;

                angular.forEach(enrolObjVal.classes, function(classObj, classLevelProp){
                    if (angular.isDefined(districtEnrollmentTotals.total_by_class[classLevelProp])) {
                        districtEnrollmentTotals.total_by_class[classLevelProp].total_male += classObj.total_male;
                        districtEnrollmentTotals.total_by_class[classLevelProp].total_female += classObj.total_female;
                        districtEnrollmentTotals.total_by_class[classLevelProp].total_pupils += classObj.total_pupils;
                    }else{
                        districtEnrollmentTotals.total_by_class[classLevelProp] = {
                            total_male: Number(classObj.total_male),
                            total_female: Number(classObj.total_female),
                            total_pupils: Number(classObj.total_pupils)
                        }
                    }
                });
            });

            angular.forEach(districtEnrollmentTotals.total_by_class, function(eachClass, prop){
                $scope.enrollmentBarChart.labels.push(prop);
                $scope.enrollmentBarChart.chartData.push(eachClass.total_pupils);
            });

            scGlobalService.drawStatisticsBarChart($scope.enrollmentBarChart.labels,
                $scope.enrollmentBarChart.label, $scope.enrollmentBarChart.chartData,
                'enrollmentBarChart', "barChart");


            $scope.districtEnrollmentTotals = angular.copy(districtEnrollmentTotals);
            $scope.districtEnrollmentTotals.loaded = true;
        });


        $scope.$on('specialPupilEnrollmentDataLoaded', function (event, allSchoolSpecialEnrollmentData) {
            var districtSpecialEnrollmentTotals = {
                total_male: 0,
                total_female: 0,
                total_pupils: 0,
                total_by_class: {}
            };

            angular.forEach(allSchoolSpecialEnrollmentData, function (item, index) {
                /*
                    level:"KG1"
                    num_females:0
                    num_males:3
                    num_of_streams:1
                    school_in_session:1
                    term:"second_term"
                    total_enrolment:3
                */

                districtSpecialEnrollmentTotals.total_male += Number(item.num_males);
                districtSpecialEnrollmentTotals.total_female += Number(item.num_females);
                districtSpecialEnrollmentTotals.total_pupils += Number(item.total_enrolment);

                if (angular.isDefined(districtSpecialEnrollmentTotals.total_by_class[item.level])) {
                    districtSpecialEnrollmentTotals.total_by_class[item.level].total_male += Number(item.num_males);
                    districtSpecialEnrollmentTotals.total_by_class[item.level].total_female += Number(item.num_females);
                    districtSpecialEnrollmentTotals.total_by_class[item.level].total_pupils += Number(item.total_enrolment);
                } else {
                    districtSpecialEnrollmentTotals.total_by_class[item.level] = {
                        total_male: Number(item.num_males),
                        total_female: Number(item.num_females),
                        total_pupils: Number(item.total_enrolment)
                    }
                }
            });

            $scope.districtSpecialEnrollmentTotals = angular.copy(districtSpecialEnrollmentTotals);
            $scope.districtSpecialEnrollmentTotals.loaded = true;
            $scope.isNotVirgin = true;
        });



        function runPupilAttendanceData(allPupilAttendanceDataLoaded) {
            var districtAttendanceTotals = {
                total_attendance_for_term: 0,
                total_school_session: 0
            };

            var classTotalHolder = {};
            var daysInSessionTotalHolder = {};

            /*
                level:"KG1"
                num_females:93
                num_males:84
                number_of_week_days:4
                total:177

                loop over all the attendances
                add the attendances per class,
                add the number of days in session for that week
                then
                multiply total enrolment by the number of days school was in session

                divide the totalAttendance by expected attendance and multiply by 100
            */

            angular.forEach(allPupilAttendanceDataLoaded, function (attendanceItem, index) {
                var classConcat = attendanceItem.level + attendanceItem.term + attendanceItem.year;
                var daysInSesConcat = attendanceItem.week_number + attendanceItem.term + attendanceItem.year;

                if (classTotalHolder[classConcat]) {
                    classTotalHolder[classConcat] += attendanceItem.total;
                }else{
                    classTotalHolder[classConcat] = attendanceItem.total;
                }

                if (!daysInSessionTotalHolder[daysInSesConcat]) {
                    daysInSessionTotalHolder[daysInSesConcat] = attendanceItem.number_of_week_days;
                }
            });

            // just for a week, the loop above prevents duplicate addition
            angular.forEach(classTotalHolder, function (classItem, index) {
                districtAttendanceTotals.total_attendance_for_term += classItem
            });

            angular.forEach(daysInSessionTotalHolder, function (weekItem, index) {
                districtAttendanceTotals.total_school_session += weekItem
            });



            $scope.districtAttendanceTotals = angular.copy(districtAttendanceTotals);
            $scope.districtAttendanceTotals.loaded = true;

            $scope.isNotVirgin = true;
        }
        $scope.$on('allPupilAttendanceDataLoaded', function (event, allPupilAttendanceDataLoaded) {
            runPupilAttendanceData(allPupilAttendanceDataLoaded)
        });



        $scope.$on('teacherPunctualityLessonPlansDataLoaded', function (event, allDistrictTeacherPupilPunctualityLessonPlansData) {
            var districtTeacherAttendanceTotals = {
                expectedAttendance: 0,
                actualAttendance: 0,
                attendanceRatio: 0,
                total_above_50: 0,
                adequately_marked_schools: 0,
                num_of_exercises_given: 0,
                num_of_exercises_marked: 0,
                district_exercises_marked_average: 0,
                total_by_teachers: {}
            };
            angular.forEach(allDistrictTeacherPupilPunctualityLessonPlansData, function (item, index) {

                if (!isFinite(Number(item.num_of_exercises_given))) item.num_of_exercises_given = 0;
                if (!isFinite(Number(item.num_of_exercises_marked))) item.num_of_exercises_marked = 0;
                if (!isFinite(Number(item.days_present))) item.days_present = 0;
                if (!isFinite(Number(item.days_in_session))) item.days_in_session = 5;

                districtTeacherAttendanceTotals.actualAttendance += (item.days_present);
                districtTeacherAttendanceTotals.expectedAttendance += (item.days_in_session);

                if (isFinite(districtTeacherAttendanceTotals.actualAttendance / districtTeacherAttendanceTotals.expectedAttendance)) {
                    districtTeacherAttendanceTotals.attendanceRatio = Math.round(100 * (districtTeacherAttendanceTotals.actualAttendance / districtTeacherAttendanceTotals.expectedAttendance));
                }


                districtTeacherAttendanceTotals.num_of_exercises_given += (item.num_of_exercises_given);
                districtTeacherAttendanceTotals.num_of_exercises_marked += (item.num_of_exercises_marked);
                if (isFinite(districtTeacherAttendanceTotals.num_of_exercises_marked / districtTeacherAttendanceTotals.num_of_exercises_given)) {
                    districtTeacherAttendanceTotals.district_exercises_marked_average +=
                        Math.round(100 * (districtTeacherAttendanceTotals.num_of_exercises_marked / districtTeacherAttendanceTotals.num_of_exercises_given));
                }


                if (angular.isDefined(districtTeacherAttendanceTotals.total_by_teachers[item.school_id])) {

                    districtTeacherAttendanceTotals.total_by_teachers[item.school_id].actualAttendance += (item.days_present);
                    districtTeacherAttendanceTotals.total_by_teachers[item.school_id].expectedAttendance += (item.days_in_session);
                    if (isFinite(districtTeacherAttendanceTotals.total_by_teachers[item.school_id].actualAttendance / districtTeacherAttendanceTotals.total_by_teachers[item.school_id].expectedAttendance)) {
                        districtTeacherAttendanceTotals.total_by_teachers[item.school_id].attendanceRatio =
                            Math.round(100 * (districtTeacherAttendanceTotals.total_by_teachers[item.school_id].actualAttendance / districtTeacherAttendanceTotals.total_by_teachers[item.school_id].expectedAttendance));
                    }

                    districtTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_given += (item.num_of_exercises_given);
                    districtTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_marked += (item.num_of_exercises_marked);

                    if (isFinite(districtTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_marked / districtTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_given)) {
                        districtTeacherAttendanceTotals.total_by_teachers[item.school_id].exercises_marked_average +=
                            Math.round(100 * (districtTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_marked / districtTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_given))
                    }

                } else {
                    var attendanceRatio = 0;
                    var exercisesMarkedAverage = 0;
                    if (isFinite(item.days_present / item.days_in_session)) attendanceRatio = item.days_present / item.days_in_session;
                    if (isFinite(item.num_of_exercises_marked / item.num_of_exercises_given)) exercisesMarkedAverage = item.num_of_exercises_marked / item.num_of_exercises_given

                    districtTeacherAttendanceTotals.total_by_teachers[item.school_id] = {
                        name_of_school: scSchool.schoolLookup[item.school_id] ? scSchool.schoolLookup[item.school_id].name : 'Name Unavailable',
                        expectedAttendance: parseInt(item.days_in_session),
                        actualAttendance: parseInt(item.days_present),
                        attendanceRatio: Math.round(100 * attendanceRatio),
                        num_of_exercises_given: item.num_of_exercises_given,
                        num_of_exercises_marked: item.num_of_exercises_marked,
                        exercises_marked_average: Math.round(100 * (exercisesMarkedAverage))
                    }
                }
            });

            /*Find the total number of schools with attendance over 50%*/
            angular.forEach(districtTeacherAttendanceTotals.total_by_teachers, function (attendanceBySchool, index) {
                if (attendanceBySchool.attendanceRatio > 50) {
                    districtTeacherAttendanceTotals.total_above_50++;
                }

                if (attendanceBySchool.exercises_marked_average > districtTeacherAttendanceTotals.district_exercises_marked_average) {
                    districtTeacherAttendanceTotals.adequately_marked_schools++;
                }

                console.log(scSchool.schoolLookup[index] ? scSchool.schoolLookup[index].name : 'Deleted School', Math.round(attendanceBySchool.attendanceRatio));
            });

            console.log('districtTeacherAttendanceTotals', districtTeacherAttendanceTotals);



            $scope.districtTeacherAttendanceTotals = angular.copy(districtTeacherAttendanceTotals);
            $scope.districtTeacherAttendanceTotals.loaded = true;
        });

        $scope.$on('recordBooksDataLoaded', function (event, allDistrictRecordBooksData) {
            var districtRecordBooks = {
                admission_register: {
                    name: "Admission Register",
                    total: 0
                },
                class_registers: {
                    name: "Class Registers",
                    total: 0
                },
                teacher_attendance_register: {
                    name: "Teacher Attendance Register",
                    total: 0
                },
                visitors: {
                    name: "Visitors\' Book",
                    total: 0
                },
                log: {
                    name: "Log Book",
                    total: 0
                },
                sba: {
                    name: "SBA",
                    total: 0
                },
                movement: {
                    name: "Movement Book",
                    total: 0
                },
                spip: {
                    name: "SPIP",
                    total: 0
                },
                inventory_books: {
                    name: "Inventory Books",
                    total: 0
                },
                cummulative_records_books: {
                    name: "Cumulative Record Books",
                    total: 0
                },
                continous_assessment: {
                    name: "Continuous Assessment / SMB",
                    total: 0
                },
                schools: {},
                schools_with_books: 0,
            };
            angular.forEach(allDistrictRecordBooksData, function (item, index) {
                angular.forEach(districtRecordBooks, function (itemValue, itemProp) {

                    if (item[itemProp] === 'yes') {
                        districtRecordBooks[itemProp].total++;
                        if (districtRecordBooks.schools[item.school_id]) {
                            districtRecordBooks.schools[item.school_id].books_available.push(itemProp)
                        } else {
                            districtRecordBooks.schools[item.school_id] = {
                                books_available: [itemProp],
                                school_name: scSchool.schoolLookup[item.school_id] ? scSchool.schoolLookup[item.school_id].name : 'Name Unavailable',
                            }
                        }
                    }
                })
            });
            /*
            *        booksHolder.push({
         name: "Admission Register",
         status : item.admission_register,
         data_collector_type : item.data_collector_type

         name: "Class Registers",
         status : item.class_registers,

         name: "Teacher Attendance Register",
         status: item.teacher_attendance_register,

         name: "Visitors\' Book",
         status: item.visitors,

         name: "Log Book",
         status: item.log,

         name: "SBA",
         status: item.sba,

         name: "Movement Book",
         status: item.movement,

         name: "SPIP",
         status: item.spip,

         name: "Inventory Books",
         status: item.inventory_books,

         name: "Cumulative Record Books",
         status: item.cummulative_records_books,

         name: "Continuous Assessment / SMB",
         status: item.continous_assessment,
         */


            angular.forEach(districtRecordBooks.schools, function (school, index) {
                if (school.books_available.length > 5) {
                    districtRecordBooks.schools_with_books++;
                    districtRecordBooks.percentage_of_schools_with_books = Math.round(100 * (districtRecordBooks.schools_with_books / $scope.selected_district.totalSchools));
                }
            });

            $scope.districtRecordBooks = angular.copy(districtRecordBooks);
        });

        $scope.$on('grantCapitationPaymentsDataLoaded', function (event, districtGrantCapitationPaymentsData) {
            var districtGrantStuff = {
                totalAmount : 0,
                total_first : 0,
                total_second : 0,
                total_third : 0,
                schools_received : 0,
                percentage_received : 0,
                schools : {}
            };

            angular.forEach(districtGrantCapitationPaymentsData, function (item, index) {
                if (angular.isDefined(item.first_tranche_amount) && item.first_tranche_amount > 0) {
                    districtGrantStuff.totalAmount += parseInt(item.first_tranche_amount);
                    districtGrantStuff.total_first += parseInt(item.first_tranche_amount);
                }
                if (angular.isDefined(item.second_tranche_amount) && item.second_tranche_amount > 0) {
                    districtGrantStuff.totalAmount += parseInt(item.second_tranche_amount);
                    districtGrantStuff.total_second += parseInt(item.second_tranche_amount);
                }
                if (angular.isDefined(item.third_tranche_amount) && item.third_tranche_amount > 0) {
                    districtGrantStuff.totalAmount += parseInt(item.third_tranche_amount);
                    districtGrantStuff.total_third += parseInt(item.third_tranche_amount);
                }

                if (districtGrantStuff.schools[item.school_id]) {
                    districtGrantStuff.schools[item.school_id].first_tranche_amount += parseInt(item.first_tranche_amount);
                    districtGrantStuff.schools[item.school_id].second_tranche_amount += parseInt(item.second_tranche_amount);
                    districtGrantStuff.schools[item.school_id].third_tranche_amount += parseInt(item.third_tranche_amount);
                    districtGrantStuff.schools[item.school_id].total += parseInt(item.first_tranche_amount) + parseInt(item.second_tranche_amount) + parseInt(item.third_tranche_amount)

                }else{
                    districtGrantStuff.schools[item.school_id] = {
                        total : parseInt(item.first_tranche_amount) + parseInt(item.second_tranche_amount) + parseInt(item.third_tranche_amount),
                        first_tranche_amount : parseInt(item.first_tranche_amount),
                        second_tranche_amount : parseInt(item.second_tranche_amount),
                        third_tranche_amount : parseInt(item.third_tranche_amount)
                    }
                }
                /*
                    grant.first_tranche_date
                    grant.first_tranche_amount
                    grant.second_tranche_date
                    grant.second_tranche_amount
                    grant.third_tranche_date
                    grant.third_tranche_amount
                */
            });

            angular.forEach(districtGrantStuff.schools, function (school, index) {
                if (angular.isDefined(school.total) && school.total > 0) {
                    districtGrantStuff.schools_received ++;
                }
            });
            districtGrantStuff.percentage_received = Math.round(100 * (districtGrantStuff.schools_received / $scope.selected_district.totalSchools));

            $scope.districtGrantStuff = angular.copy(districtGrantStuff);
        });

        $scope.$on('supportTypesDataLoaded', function (event, districtSupportData) {
            var districtSupportStuff = {
                donor_support : {
                    name: "Donor Support",
                    cash : 0,
                    kind : ''
                },
                community_support: {
                    name: "Community Support",
                    cash : 0,
                    kind : ''
                },
                district_support: {
                    name: "District Support",
                    cash : 0,
                    kind : ''
                },
                pta_support: {
                    name: "PTA Support",
                    cash : 0,
                    kind : ''
                },
                other_support: {
                    name: "Other",
                    cash : 0,
                    kind : ''
                },
                total_cash : 0,
                schools : {},
                received_support_tracker : [],
                received_support : 0,
                percentage_received_support : 0
            };
            /*
            name: "Donor Support",
            in_cash : item.donor_support_in_cash,
            in_kind : item.donor_support_in_kind,

            name: "Community Support",
            in_cash : item.community_support_in_cash,
            in_kind : item.community_support_in_kind,

            name: "District Support",
            in_cash : item.district_support_in_cash,
            in_kind : item.district_support_in_kind,

            name: "PTA Support",
            in_cash : item.pta_support_in_cash,
            in_kind : item.pta_support_in_kind,
            comment : item.comment


            name: "Other",
            in_cash : item.other_support_in_cash,
            in_kind : item.other_support_in_kind,

        });*/
            angular.forEach(districtSupportData, function (item, index) {

                angular.forEach(districtSupportStuff, function (itemVal, itemProp) {

                    districtSupportStuff.total_cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                    districtSupportStuff[itemProp].cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                    districtSupportStuff[itemProp].kind  = angular.isDefined(item[itemProp + '_in_kind']);

                    if (isFinite(Number(item[itemProp + '_in_cash'])) ||  Number(item[itemProp + '_in_cash']) > 0 || angular.isDefined(item[itemProp + '_in_kind'])) {
                        if (districtSupportStuff.received_support_tracker.indexOf(item.school_id) < 0) {
                            districtSupportStuff.received_support_tracker.push(item.school_id);
                            districtSupportStuff.received_support ++;
                            districtSupportStuff.percentage_received_support = parseInt(100 * (districtSupportStuff.received_support / $scope.selected_district.totalSchools));
                        }
                    }

                    if( districtSupportStuff.schools[item.school_id]){
                        districtSupportStuff.schools[item.school_id].total += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                        districtSupportStuff.schools[item.school_id].in_kind = angular.isDefined(item[itemProp + '_in_kind']);

                    }else{
                        districtSupportStuff.schools[item.school_id] = {};
                        districtSupportStuff.schools[item.school_id].name_of_school = scSchool.schoolLookup[item.school_id] ? scSchool.schoolLookup[item.school_id].name : 'Name Unavailable',

                            districtSupportStuff.schools[item.school_id].total = isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                        districtSupportStuff.schools[item.school_id].in_kind = angular.isDefined(item[itemProp + '_in_kind']);
                    }
                });

            });
            $scope.districtSupportStuff = angular.copy(districtSupportStuff);
        });

        $scope.$on('furnitureDataLoaded', function (event, districtSchoolFurnitureItems) {
            var districtFurniture = {
                schools : {},
                total_schools_with_adequates : 0,
                adequate_percentage : 0
            };
            /*
            *    furnitureHolder.push({
            name: "Pupils Furniture",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.pupils_furniture],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });
        furnitureHolder.push({
            name: "Teacher Tables",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.teacher_tables],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });
        furnitureHolder.push({
            name: "Teacher Chairs",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.teacher_chairs],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });
        furnitureHolder.push({
            name: "Classrooms Cupboard",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.classrooms_cupboard],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });

        */

            var adequacyElems = ['pupils_furniture','teacher_tables','teacher_chairs','classrooms_cupboard'];

            angular.forEach(districtSchoolFurnitureItems, function (item, index) {
                districtFurniture.schools[item.school_id] = {
                    adequacy_count : 0
                };

                for (var i = 0; i < adequacyElems.length; i++) {
                    var adequateProp = adequacyElems[i];

                    if (item[adequateProp] === 'adequate') {
                        districtFurniture.schools[item.school_id].adequacy_count++;
                    }

                }
                if (districtFurniture.schools[item.school_id].adequacy_count >= 3) {
                    districtFurniture.total_schools_with_adequates ++;
                    districtFurniture.adequate_percentage = parseInt(100 * (districtFurniture.total_schools_with_adequates / $scope.selected_district.totalSchools));
                }

            });
            $scope.districtFurniture = angular.copy(districtFurniture);
        });

        $scope.$on('structureDataLoaded', function (event, districtSchoolStructureItems) {
            var districtStructure = {
                schools : {},
                total_schools_with_goods : 0,
                good_percentage : 0
            };
            /*structureHolder.push({
                name: "Walls",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.walls],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Doors",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.doors],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Windows",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.windows],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Floors",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.floors],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Blackboard",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.blackboard],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
                structureHolder.push({
                    name: "Illumination",
                    term :   structureObject.term,
                    status : $scope.formatGoodPoorFair[structureObject.illumination],
                    year : structureObject.year,
                    comment : structureObject.comment,
                    data_collector_type : structureObject.data_collector_type
                });
*/
            var goodElems = ['walls','doors','windows','floors','blackboard','illumination'];

            angular.forEach(districtSchoolStructureItems, function (item, index) {
                districtStructure.schools[item.school_id] = {
                    good_count : 0
                };

                for (var i = 0; i < goodElems.length; i++) {
                    var statusProp = goodElems[i];

                    if (item[statusProp] === 'good') {
                        districtStructure.schools[item.school_id].good_count++;
                    }
                }
                if (districtStructure.schools[item.school_id].good_count >= 4) {
                    districtStructure.total_schools_with_goods ++;
                    districtStructure.good_percentage = parseInt(100 * (districtStructure.total_schools_with_goods / $scope.selected_district.totalSchools));
                }
            });

            $scope.districtStructure = angular.copy(districtStructure);
        });

        $scope.$on('sanitationDataLoaded', function (event, districtSanitationItems) {
            var districtSanitation = {
                schools : {},
                total_schools_with_available : 0,
                available_percentage : 0
            };
            /*
            *   sanitationHolder.push({
           name: "Toilet",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.toilet],
           year :sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Urinal",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.urinal],
           year :sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Water",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.water],
           year :sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Dust Bins",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.dust_bins],
           year : sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Veronica Buckets",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
           year :  sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
            * */

            var availabilityItems = ['toilet','urinal','water','dust_bins','veronica_buckets'];

            angular.forEach(districtSanitationItems, function (item, index) {
                districtSanitation.schools[item.school_id] = {
                    available_count : 0
                };

                for (var i = 0; i < availabilityItems.length; i++) {
                    var statusProp = availabilityItems[i];

                    if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                        districtSanitation.schools[item.school_id].available_count++;
                    }
                }
                if (districtSanitation.schools[item.school_id].available_count >= 4) {
                    districtSanitation.total_schools_with_available ++;
                    districtSanitation.available_percentage = parseInt(100 * (districtSanitation.total_schools_with_available / $scope.selected_district.totalSchools));
                }
            });
            $scope.districtSanitation = angular.copy(districtSanitation);
        });

        $scope.$on('recreationDataLoaded', function (event, districtRecreationItems) {
            var districtRecreation = {
                schools : {},
                total_schools_with_available : 0,
                available_percentage : 0
            };
            /*
            *   recreationHolder.push({
           name: "Football",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.football],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Volleyball",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.volleyball],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Netball",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.netball],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Playing Field",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.playing_field],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Sports Wear",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.sports_wear],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       try{
           recreationHolder.push({
               name: "Seesaw",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.seesaw],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           })
           recreationHolder.push({
               name: "Merry-Go-Round",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.merry_go_round],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
       recreationHolder.push({
           name: "First Aid Box",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.first_aid_box],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });

            * */

            var availabilityItems = ['football','volleyball','netball','playing_field','sports_wear','seesaw','merry_go_round','first_aid_box'];

            angular.forEach(districtRecreationItems, function (item, index) {

                districtRecreation.schools[item.school_id] = {
                    available_count : 0
                };

                for (var i = 0; i < availabilityItems.length; i++) {
                    var statusProp = availabilityItems[i];

                    if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                        districtRecreation.schools[item.school_id].available_count++;
                    }
                }
                if (districtRecreation.schools[item.school_id].available_count > 7) {
                    districtRecreation.total_schools_with_available ++;
                    districtRecreation.available_percentage = parseInt(100 * (districtRecreation.total_schools_with_available / $scope.selected_district.totalSchools));
                }
            });
            $scope.districtRecreation = angular.copy(districtRecreation);

        });

        $scope.$on('securityDataLoaded', function (event, districtSecurityItems) {
            var districtSecurity = {
                schools : {},
                total_schools_with_available : 0,
                available_percentage : 0
            };
            /*
            *  securityHolder.push({
            name: "Walled / Fenced",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.walled],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });

        securityHolder.push({
            name: "Gated",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.gated],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });
        securityHolder.push({
            name: "Lights",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.lights],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });
        securityHolder.push({
            name: "Security Man",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.security_man],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });

            * */
            var availabilityItems = ['security_man','lights','gated','walled'];

            angular.forEach(districtSecurityItems, function (item, index) {

                districtSecurity.schools[item.school_id] = {
                    available_count : 0
                };

                for (var i = 0; i < availabilityItems.length; i++) {
                    var statusProp = availabilityItems[i];

                    if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                        districtSecurity.schools[item.school_id].available_count++;
                    }
                }
                if (districtSecurity.schools[item.school_id].available_count >= 3) {
                    districtSecurity.total_schools_with_available ++;
                    districtSecurity.available_percentage = parseInt(100 * (districtSecurity.total_schools_with_available / $scope.selected_district.totalSchools));
                }
            });
            $scope.districtSecurity = angular.copy(districtSecurity);
        });

        $scope.$on('meetingDataLoaded', function (event, districtSchoolMeetingItems) {
            var districtMeeting = {
                schools : {},
                total_schools_with_frequency : 0,
                frequency_percentage : 0
            };
            /*
            *
       meetingsHolder.push({
           name: "Staff Meeting",
           frequency: meetingObject.number_of_staff_meeting,
           males_present: meetingObject.num_males_present_at_staff_meeting,
           females_present: meetingObject.num_females_present_at_staff_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

       meetingsHolder.push({
           name: "SPAM",
           frequency: meetingObject.number_of_spam_meeting,
           males_present: meetingObject.num_males_present_at_spam_meeting,
           females_present: meetingObject.num_females_present_at_spam_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

       meetingsHolder.push({
           name: "PTA",
           frequency: meetingObject.number_of_pta_meeting,
           males_present: meetingObject.num_males_present_at_pta_meeting,
           females_present: meetingObject.num_females_present_at_pta_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

       meetingsHolder.push({
           name: "SMC",
           frequency: meetingObject.number_of_smc_meeting,
           males_present: meetingObject.num_males_present_at_smc_meeting,
           females_present: meetingObject.num_females_present_at_smc_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

            * */
            var frequencyItems = ['number_of_smc_meeting','number_of_pta_meeting','number_of_spam_meeting','number_of_staff_meeting'];

            angular.forEach(districtSchoolMeetingItems, function (item, index) {
                districtMeeting.schools[item.school_id] = {
                    frequency_count : 0
                };

                for (var i = 0; i < frequencyItems.length; i++) {
                    var freqProp = frequencyItems[i];

                    if (isFinite(Number(item[freqProp])) && Number(item[freqProp]) > 0) {
                        districtMeeting.schools[item.school_id].frequency_count++;
                    }
                }
                if (districtMeeting.schools[item.school_id].frequency_count >= 3) {
                    districtMeeting.total_schools_with_frequency ++;
                    districtMeeting.frequency_percentage = parseInt(100 * (districtMeeting.total_schools_with_frequency / $scope.selected_district.totalSchools));
                }
            });
            $scope.districtMeeting = angular.copy(districtMeeting);
        });

        $scope.$on('communityDataLoaded', function (event, districtCommunityItems) {
            var districtCommInvolvement = {
                schools : {},
                total_schools_with_goods : 0,
                good_percentage : 0
            };

            /*
            *       var format_term = $scope.academicTermDisplay(communityObject.term);
        communityHolder.push({
            name: "PTA Involvement",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.pta_involvement],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });
        communityHolder.push({
            name: "Parents Notified of Student Progress",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.parents_notified_of_student_progress],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });
        communityHolder.push({
            name: "Community Sensitization on School Attendance",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.community_sensitization_for_school_attendance],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });
        communityHolder.push({
            name: "School Management Committees",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.school_management_committees],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });

            * */
            var goodElems = ['school_management_committees','community_sensitization_for_school_attendance','parents_notified_of_student_progress','pta_involvement'];

            angular.forEach(districtCommunityItems, function (item, index) {
                districtCommInvolvement.schools[item.school_id] = {
                    good_count : 0
                };

                for (var i = 0; i < goodElems.length; i++) {
                    var statusProp = goodElems[i];

                    if (item[statusProp] === 'good') {
                        districtCommInvolvement.schools[item.school_id].good_count++;
                    }
                }
                if (districtCommInvolvement.schools[item.school_id].good_count >= 3) {
                    districtCommInvolvement.total_schools_with_goods ++;
                    districtCommInvolvement.good_percentage = parseInt(100 * (districtCommInvolvement.total_schools_with_goods / $scope.selected_district.totalSchools));
                }
            });
            $scope.districtCommInvolvement = angular.copy(districtCommInvolvement);

        });





        $scope.$watchGroup(['teacherSummaryData.districtInfo.teachers_total', 'districtEnrollmentTotals.total_pupils'], function(newVal, oldVal) {
            if (angular.isDefined(newVal[0]) && newVal[0] > 0) {

                if (angular.isDefined(newVal[1]) && newVal[1] > 0) {

                    $scope.pupilTeacherRatio = Math.round((newVal[1]) / (newVal[0]))
                }
            }
        });

        $scope.$watchGroup(['districtAttendanceTotals', 'districtEnrollmentTotals.total_pupils', 'pupilTeacherRatio', 'districtTeacherAttendanceTotals'], function(newVal, oldVal) {

            if ($scope.districtAttendanceTotals && $scope.districtEnrollmentTotals) {

                /*  multiply total enrolment by the number of days school was in session

                  divide the totalAttendance by expected attendance and multiply by 100*/
                var expectedAttendance = $scope.districtEnrollmentTotals.total_pupils * $scope.districtAttendanceTotals.total_school_session;

                $scope.districtAttendanceTotals.attendance_ratio = Math.round(($scope.districtAttendanceTotals.total_attendance_for_term / expectedAttendance) * 100) ;

                if ($scope.pupilTeacherRatio && $scope.districtTeacherAttendanceTotals) {
                    $scope.loading = false;
                }
            }


        });


        $scope.downloadPDF = function (){
            var pdfDocVariables = {
                title : 'mSRC District Report',
                entity_name : $scope.selected_district.name + " (District)",
                entity : $scope.selected_district,
                upper_level_name: $scope.selected_district.regionName + " Region",

                term_in_scope : $scope.one_school_terms[$scope.summary_term].label,

                top_box_left: '' + $scope.districtEnrollmentTotals.total_pupils,
                top_box_middle : '',
                top_box_up_middle_up: '' +$scope.districtEnrollmentTotals.total_male,
                top_box_up_middle_down:  '' + $scope.districtEnrollmentTotals.total_female,
                top_box_right: 'Number of Teachers : ' + $scope.teacherSummaryData.districtInfo.teachers_total,
                top_box_down_left:  $scope.districtAttendanceTotals.attendance_ratio + "%",
                top_box_down_middle_up: '' + $scope.districtSpecialEnrollmentTotals.total_male || 0,
                top_box_down_middle_down:  '' + $scope.districtSpecialEnrollmentTotals.total_female || 0,

                percentage_trained_teachers: $scope.teacherSummaryData.districtInfo.percentage_trained + '',

                english_lang_performance_array : $scope.districtPerformanceTotals.english.class_score,
                gh_lang_performance_array : $scope.districtPerformanceTotals.gh_lang.class_score,
                maths_lang_performance_array : $scope.districtPerformanceTotals.math.class_score,
                teacher_pupil_ratio : ($scope.pupilTeacherRatio + "  :  1"),
                teacher_attendance_ratio : ($scope.districtTeacherAttendanceTotals.attendanceRatio || 0) + '%',
                punctuality_title : 'Schools with Teacher',
                teacher_punctuality : ($scope.districtTeacherAttendanceTotals.total_above_50 || 0)+ '%',
                adequate_marked_title : "Schools with Adequately",
                adequately_marked_exercises : ($scope.districtTeacherAttendanceTotals.adequately_marked_schools || 0) + '%',
                schools_with_or_number_of_title : "Schools with",
                schools_that_or_amount_of_title : "Schools that",
                schools_that_received_or_any_type_of_support : "Schools that received",
                record_books : ($scope.districtRecordBooks.percentage_of_schools_with_books || 0) + '%',
                grant_received : ($scope.districtGrantStuff.percentage_received || 0) + "%",
                support_received : ($scope.districtSupportStuff.percentage_received_support || 0) + "%",
                adequate_furniture : ($scope.districtFurniture.adequate_percentage || 0) + '%',
                meetings_title : "Schools that Hold Meetings" || 'Number of Meetings Held',
                community_title : "Schools with Active Community\n         \tParticipation",
                good_structure : ($scope.districtStructure.good_percentage || 0) + '%',
                sanitation_facilities : ($scope.districtSanitation.available_percentage) + "%",
                recreational_facilities : ($scope.districtRecreation.available_percentage) + "%",
                security_facilities : ($scope.districtSecurity.available_percentage) + "%",
                meetings : ($scope.districtMeeting.frequency_percentage) + "%",
                community_participation : ($scope.districtCommInvolvement.good_percentage) + "%",
                pdf_export_name : $scope.selected_district.name + '_' +$scope.summary_term + '_district_report.pdf'
            };



            scReportGenerator.districtDownloadPDF(pdfDocVariables)
        };

    }]);
