/**
 * Created by sophismay on 6/16/14.
 */

/**
 * Main Controller
 */


schoolMonitor.controller('scMainController',['$scope', '$state', 'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool',
    'RecentSubmissionService','SMSService','$timeout',
    function ($scope, $state, scNotifier, scRegion, scDistrict, scCircuit, scSchool,
              RecentSubmissionService, SMSService, $timeout) {

        //Use Jquery to replace the name on the dashboard so the
        //curly braces don't show on slow internet
        $('.hide_until_angular-loads').css('display', 'block');


        $scope.changePageTitle('Dashboard', 'Overview', 'Dashboard');

        $scope.changeSelectedRegion = function(id){
            $scope.selected_region = scRegion.regionLookup[id];
        };

        $scope.$on('regionChartClicked', function(event, data){
            try{
                if (scRegion.regionLookup && scRegion.regionLookup[data]) {
                    $scope.selected_region = scRegion.regionLookup[data];
                }
            }catch(e){
                console.log(e);
            }

        });

        $scope.$on('districtChartClicked', function(event, data){
            try{
                if (scDistrict.districtLookup && scDistrict.districtLookup[data]) {
                    $scope.selected_district = scDistrict.districtLookup[data];
                }
            }catch(e){
                console.log(e);
            }

        });

        $scope.$on('circuitChartClicked', function(event, data){
            try{
                if (scCircuit.circuitLookup && scCircuit.circuitLookup[data]) {
                    $scope.selected_circuit = scCircuit.circuitLookup[data];
                }
            }catch(e){
                console.log(e);
            }

        });
        var community_sms = [];

        $scope.community_sms_loading = true;
        $scope.recent_activity_loading = true;

        if ($scope.adminRight == 5) {
//            $scope.recent_submissions = recent_submissions;
            RecentSubmissionService.getAllRecentSubmissions('country', 1)
                .then(function (submission_data) {
                    $scope.recent_submissions = submission_data;
                    $scope.recent_activity_loading = false;
                });
            SMSService.getSMSes('country', 1)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    community_sms = sms_data;
                    $scope.community_smses = sms_data;
                    $scope.community_sms_loading = false;
                });
        }
        else if ($scope.adminRight == 4) {
            RecentSubmissionService.getAllRecentSubmissions('region', $scope.currentUser.level_id)
                .then(function (submission_data) {
                    $scope.recent_submissions = submission_data;
                    $scope.recent_activity_loading = false;
                });
            SMSService.getSMSes('region', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    community_sms = sms_data;
                    $scope.community_smses = sms_data;
                    $scope.community_sms_loading = false;
                });
        }
        else if ($scope.adminRight == 3) {
            RecentSubmissionService.getAllRecentSubmissions('district', $scope.currentUser.level_id)
                .then(function (submission_data) {
                    $scope.recent_submissions = submission_data;
                    $scope.recent_activity_loading = false;

                });

            SMSService.getSMSes('district', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    community_sms = sms_data;
                    $scope.community_smses = sms_data;
                    $scope.community_sms_loading = false;
                });

        }
        else if ($scope.adminRight == 2) {
            RecentSubmissionService.getAllRecentSubmissions('circuit', $scope.currentUser.level_id)
                .then(function (submission_data) {
                    $scope.recent_submissions = submission_data;
                    $scope.recent_activity_loading = false;
                });
            SMSService.getSMSes('circuit', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    community_sms = sms_data;
                    $scope.community_smses = sms_data;
                    $scope.community_sms_loading = false;
                });
        }
        else if ($scope.adminRight == 1) {
            RecentSubmissionService.getAllRecentSubmissions('school', $scope.currentUser.level_id)
                .then(function (submission_data) {
                    $scope.recent_submissions = submission_data;
                    $scope.recent_activity_loading = false;
                });
            SMSService.getSMSes('school', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    community_sms = sms_data;
                    $scope.community_smses = sms_data;
                    $scope.community_sms_loading = false;
                });
        }


        $scope.totalItems = community_sms.length;
        $scope.currentPage = 1;
        $scope.maxSize = 3;
        $scope.nextPage = 0;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

//            array.slice(start,end)
        /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
         * It does not include the element at that index*/

        $scope.pageChanged = function() {
//                console.log('Page changed to: ' + $scope.currentPage);
            $scope.nextPage = ($scope.maxSize  * $scope.currentPage) - $scope.maxSize;
            $scope.community_smses = community_sms.slice($scope.nextPage);
        };

        $scope.reviewDataExists = scRegion.reviewDataExists;

        //This function is called when a region is clicked on the Ghana map
        function clickMap(event){
            $scope.$apply(function(){
                $scope.changeSelectedRegion(event.mapObject.identity);
            });
        }

        $scope.noOfPages = 7;
        $scope.currentPage = 4;
        $scope.maxSize = 5;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.checkCircuitVisibility = function( circuit_id ){
            if ($scope.adminRight == 2) {
                if (circuit_id == $scope.currentUser.level_id) {
                    return true
                }
            }else if ($scope.adminRight == 3) {
                if (scDistrict.districtLookup[$scope.currentUser.level_id].allCircuits[circuit_id]) {
                    return true
                }
            }else if ($scope.adminRight == 4) {
                if (scRegion.regionLookup[$scope.currentUser.level_id].allCircuits[circuit_id]) {
                    return true
                }
            }else if ($scope.adminRight == 5) {
                return true
            }
        };

        $scope.checkDistrictVisibility = function( district_id ){
            if ($scope.adminRight == 3) {
                if (district_id == $scope.currentUser.level_id) {
                    return true
                }
            }else if ($scope.adminRight == 4) {
                if (scRegion.regionLookup[$scope.currentUser.level_id].allDistricts[district_id]) {
                    return true
                }
            }else if ($scope.adminRight == 5) {
                return true
            }
        };

        $scope.checkRegionVisibility = function( region_id ){
            if ($scope.adminRight == 4) {
                if (region_id == $scope.currentUser.level_id) {
                    return true
                }
            }else if ($scope.adminRight == 5) {
                return true
            }
        };

        function prepCharts() {
            $timeout(function () {
                $scope.loadRegionView();
                $scope.$broadcast('regionChartClicked', 1);
                $scope.loadDistrictView();
                $scope.$broadcast('districtChartClicked', 1);
                $scope.loadCircuitView();
                $scope.$broadcast('circuitChartClicked', 1);
            }, 2000)
        }

        function initializeMain() {
            $scope.regions = scRegion.regions;

            $scope.districts = scDistrict.districts;

            $scope.circuits = scCircuit.circuits;

            $scope.schools = scSchool.schools;

            //generate a random number to choose which region to display
            //on the home screen
            var random = Math.floor(Math.random() * 9);

            $scope.selected_region = $scope.regions[random];

            $scope.regionSortOrder = 'rating';

            $scope.numberOfRegionsToDisplay = 10;

            prepCharts();
        }

        $scope.loadRegionView = function() {

            /*Script that renders Ghana Map*/
            /* var map = new AmCharts.AmMap();

             map.pathToImages = "../../scripts/js/plugins/amMaps/images/";

             map.colorSteps = 100;

             map.balloon.color = "#000000";

             map.dragMap = false;

             map.fitMapToContainer = true;

             map.useObjectColorForBalloon = true;

             map.zoomControl.zoomControlEnabled = false;

             map.zoomControl.maxZoomLevel = 1;

             map.dataProvider = {
                 mapVar: AmCharts.maps.ghanaMap,
                 getAreasFromMap: true,

                 /!*
                  maxZoomLevel	Number	64	Max zoom level.
                  minZoomLevel	Number	1	Min zoom level.
                  panControlEnabled	Boolean	true	Specifies if pan control is enabled.
                  panStepSize	Number	0.1	Specifies by what part of a map container width/height the map will be moved when user clicks on pan arrows.
                  right	Number		Distance from right side of map container to the zoom control.
                  top	Number	10	Distance from top of map container to the zoom control.
                  zoomControlEnabled 	Boolean	true	Specifies if zoom control is enabled.
                  zoomFactor*!/

 //            var dataProvider = {
 //                map: "worldLow",
 //                images:[{latitude:40.3951,
 //                    longitude:-73.5619,
 //                    svgPath:icon, color:"#CC0000",
 //                    scale:0.5,
 //                    label:"New York",
 //                    labelShiftY:2,
 //                    zoomLevel:5,
 //                    title:"New York",
 //                    description:"New York is the most populous city in the United States and the center of the New York Metropolitan Area, one of the most populous metropolitan areas in the world."}]
 //            };

                 areas: [
                     {
                         id: "AR",
                         value: $scope.regions[0].rating,
                         identity: $scope.regions[0].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[0].description
                     },
                     {
                         id: "BAR",
                         value: $scope.regions[1].rating,
                         identity: $scope.regions[1].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[1].description
                     },
                     {
                         id: "CR",
                         value: $scope.regions[2].rating,
                         identity: $scope.regions[2].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[2].description
                     },
                     {
                         id: "ER",
                         value: $scope.regions[3].rating,
                         identity: $scope.regions[3].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[3].description
                     },
                     {
                         id: "GAR",
                         value: $scope.regions[4].rating,
                         identity: $scope.regions[4].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[4].description
                     },
                     {
                         id: "NR",
                         value: $scope.regions[5].rating,
                         identity: $scope.regions[5].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[5].description
                     },
                     {
                         id: "UER",
                         value: $scope.regions[6].rating,
                         identity: $scope.regions[6].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[6].description
                     },
                     {
                         id: "UWR",
                         value: $scope.regions[7].rating,
                         identity: $scope.regions[7].id,
 //                  //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[7].description
                     },
                     {
                         id: "VR",
                         value: $scope.regions[8].rating,
                         identity: $scope.regions[8].id,
                         //   zoomLevel: 1,
 //                    color:"#6699FF",
                         description: $scope.regions[8].description
                     },
                     {
                         id: "WR",
                         value: $scope.regions[9].rating,
                         identity: $scope.regions[9].id,
                         //   zoomLevel: 1,
 //                    color: '',color
                         description: $scope.regions[9].description
                     }
                 ]*/

        };

//        var mapObject = map.getObjectById('US');
//        map.clickMapObject(mapObject);
        // monitor when an area has been clicked show in view window
        // map.addListener("clickMapObject", clickMap);
        //
        // map.centerMap = true;
        //
        // map.developerMode = true;

//            map.maxZoomLevel = 10;
//        map.addTitle('Ratings of regions in Ghana');

//         map.areasSettings = {
//             balloonText: "<span style='font-size:14px;'><b>[[title]]</b>: [[value]] / 100</span>",
//             autoZoom: false,
//             selectedColor: "#428bca",
//             rollOverColor: '#3c8dbc',
//             remainVisible: true
//         };
//
// //        map.zoomLevel();
// //        maxZoomLevel
//
//         var valueLegend = new AmCharts.ValueLegend();
//         valueLegend.right = 10;
//         valueLegend.minValue = "little";
//         valueLegend.maxValue = "a lot!";
//         map.valueLegend = valueLegend;
//
//
//         map.smallMap = new AmCharts.SmallMap();
//
//         map.write("mapdiv");
//         /*End of amMap*/
//
//         //This variable holds the temporal objects to be passed to the serial chart function
//         $scope.regionChartData = [];
//
//         for (var i = 0; i < scRegion.regions.length; i++) {
//             var tempDistrictObject = {
//                 "region": scRegion.regions[i].name,
//                 "rating": scRegion.regions[i].rating,
//                 "id": scRegion.regions[i].id
//             };
//             $scope.regionChartData.push( tempDistrictObject );
//         }
//
//         $scope.changeSerialChart($scope.regionChartData, 'region', 'Regions' , 'mainViewRegionChart');
//

        // };

        /*District Overview --- this is where we select ten random districts to show on the main screen*/

        $scope.loadDistrictView = function(){

            //This variable holds the temporal objects to be passed to the serial chart function
            $scope.districtChartData = [];

            //generate a random number to start getting the districts from
            var randomDistrictIndex = Math.floor(Math.random() * scDistrict.districts.length);

            for (var i = randomDistrictIndex; i < scDistrict.districts.length; i++) {
                if (i >= 0 && 1 < scDistrict.districts.length) {
                    var tempDistrictObject = {
                        "district":  scDistrict.districts[i].name,
                        "rating":  scDistrict.districts[i].rating,
                        "id":  scDistrict.districts[i].id
                    };
                    if ( $scope.districtChartData.length < 10) {
                        $scope.districtChartData.push(tempDistrictObject);

                        $scope.selected_district = scDistrict.districts[i];
                    }else {
                        break;
                    }
                }
            }

            $scope.changeSerialChart($scope.districtChartData, 'district', 'Districts' , 'mainViewDistrictChart');

        };

        $scope.showChangedDistrict = function(district_id){
            $scope.selected_district = scDistrict.districtLookup[district_id]
        };

        /*Circuit Overview -- this is where we select ten random circuits to show on the main screen*/

        $scope.loadCircuitView = function() {


            //This variable holds the temporal objects to be passed to the serial chart function
            $scope.circuitChartData = [];

            //generate a random number to start getting the districts from
            var randomCircuitIndex = Math.floor(Math.random() * scCircuit.circuits.length);

            for (var j = randomCircuitIndex; j < scCircuit.circuits.length; j++) {
                if (j >= 0 && 1 < scCircuit.circuits.length) {
                    var tempCircuitObject = {
                        "circuit": scCircuit.circuits[j].name,
                        "rating": scCircuit.circuits[j].rating,
                        "id": scCircuit.circuits[j].id
                    };
                    if ($scope.circuitChartData.length < 10) {
                        $scope.circuitChartData.push(tempCircuitObject);

                        $scope.selected_circuit = scCircuit.circuits[j];
                    } else {
                        break;
                    }
                }
            }

            $scope.changeSerialChart($scope.circuitChartData, 'circuit', 'Circuits', 'mainViewCircuitChart');
        };

        $scope.showChangedCircuit = function(circuit_id){
            $scope.selected_circuit = scCircuit.circuitLookup[circuit_id]
        };

        $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            if (data.counter == 6) {

                $timeout(function () {
                    initializeMain();
                }, 2000)
            }
        });

        if (scRegion.regions.length && scDistrict.districts.length &&  scCircuit.circuits && scSchool.schools ) {
            initializeMain();
        }



    }]);

