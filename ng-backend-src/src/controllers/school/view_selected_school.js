/**
 * Created by Kaygee on 18/05/2015.
 */
//This controller is responsible for the selected school
schoolMonitor.controller('scViewSelectedSchoolController', ['$rootScope', '$scope', '$state', '$stateParams','$modal',
    'scSchool', 'Email', 'scNotifier','districtItinerary', '$timeout', 'DataCollectorHolder', 'scGlobalService',
    function($rootScope, $scope, $state, $stateParams, $modal,
             scSchool, Email, scNotifier,districtItinerary, $timeout, DataCollectorHolder, scGlobalService){

        $scope.changePageTitle('View', 'School', 'View');

        scSchool.tracker = {};
        //Variable to keep the stats when you change pages


        $scope.loadingTeachers = true; $scope.loadingData = true;//show a gif when loading teachers in the school
        $scope.teachers_in_school = [];//define the variable so it is inherited in child scopes
        scSchool.allSchoolTeachersData({school_id : $stateParams.id})
            .success(function (successData) {
                //Assign school teachers to scope variable
                //to be used to find the number of teachers in the school
                $timeout(function () {
                    $scope.teachers_in_school = successData[0].school_teachers;
                    $scope.teachers_in_school.trained = 0;
                    $scope.teachers_in_school.atpost = 0;
                    $scope.teachers_in_school.studyleave = 0;
                    $scope.teachers_in_school.otherleave = 0;
                    $scope.teachers_in_school.retired = 0;
                    $scope.teachers_in_school.deceased = 0;
                    prepTeachers();

                    $scope.loadingTeachers = false;
                    $scope.loadingData = false;

                },1);
            });


        /*Set a variable to allow sharing of data between enrollment and attendance states*/
        scSchool.loadedEnrollmentInfoFromServer = [];
        scSchool.loadedSpecialEnrollmentInfoFromServer = [];
        scSchool.loadedAttendanceInfoFromServer = [];
        scSchool.loadedSpecialAttendanceInfoFromServer = [];
        scSchool.allSchoolTeachersPunctuality_LessonPlan =[];
        scSchool.allSchoolTeachersClassManagement =[];
        scSchool.allSchoolTeachersUnitsCoveredData =[];



        function prepSchoolData(){
            //assign the selected school to the scope variable
            if (scSchool.schoolLookup && scSchool.schoolLookup[$stateParams.id]) {
                $scope.selected_school = scSchool.schoolLookup[$stateParams.id];
                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;


                /*Load the school images submitted*/
                $scope.loadingImages = true;
                scSchool.getImages($scope.selected_school.id)
                    .then(function (schoolImages) {/*Image fulfilled callback*/
                        /*Image Prep Section*/
                        $scope.image = {
                            indicators : [],
                            slides : schoolImages
                        };
                        //get the number of carousel markers to display
                        for (var ci=0; ci <  schoolImages.length; ci++ ) {
                            $scope.image.indicators.push(ci)
                        }
                        $scope.loadingImages = false;
                    }, function () {/*Images rejected callback*/
                        $scope.loadingImages = false;
                    });

                scSchool.getRequiredImages($scope.selected_school.id)
                    .then(function (schoolImages) {/*Image fulfilled callback*/
                        /*Image Prep Section*/
                        $scope.requiredImages = {
                            sections : {},
                            allImages : schoolImages
                        };
                        //get the number of carousel markers to display
                        angular.forEach(schoolImages, function (image, index) {
                            if (!angular.isDefined($scope.requiredImages.sections[image.image_category_type])) {
                                $scope.requiredImages.sections[image.image_category_type] = [];
                            }
                            $scope.requiredImages.sections[image.image_category_type].push(image);
                        });

                        $scope.loadingImages = false;
                    }, function () {/*Images rejected callback*/
                        $scope.loadingImages = false;
                    });

            }else{
                $timeout(function () {
                    prepSchoolData();
                }, 2000)
            }
        }

        function checkSchoolsAreLoadAndPrepSchools() {
            if (!(_.isEmpty(scSchool.schoolLookup)) && angular.isDefined(scSchool.schoolLookup[$stateParams.id])) {
                prepSchoolData();
            } else {
                $timeout(function () {
                    checkSchoolsAreLoadAndPrepSchools();
                }, 2500)
            }
        }


        $scope.$on('updateLoadedModelsFromRefresh', function(evt, data){
            if (data.counter == 6) {
                checkSchoolsAreLoadAndPrepSchools();
            }
        });

        /*starts from here*/

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scSchool.schools.length && scSchool.schoolLookup) {
            checkSchoolsAreLoadAndPrepSchools();
        }



        $scope.$on('teachersUpdated', function(evt, teachersObject){
            $scope.teachers_in_school = teachersObject[0].school_teachers;
            $scope.teachers_in_school.trained = 0;
            $scope.teachers_in_school.atpost = 0;
            $scope.teachers_in_school.studyleave = 0;
            $scope.teachers_in_school.otherleave = 0;
            $scope.teachers_in_school.retired = 0;
            $scope.teachers_in_school.deceased = 0;
            prepTeachers();
        });

        function prepTeachers() {
            /*This variable holds teacher IDs in the school so we can know transferred teachers data of this school*/
            $scope.teachersInSchoolIDHolder = [];

            $timeout(function () {
                angular.forEach($scope.teachers_in_school, function (teacher, index) {
                    if (angular.isUndefined(teacher.highest_professional_qualification)
                        || teacher.highest_professional_qualification === ''
                        || teacher.highest_professional_qualification.toLowerCase() === 'others'
                        || teacher.highest_professional_qualification.toLowerCase() === 'other') {

                    }else{
                        $scope.teachers_in_school.trained ++;
                    }

                    $scope.teachers_in_school[teacher.teacher_status] ++;

                    $scope.teachersInSchoolIDHolder.push(teacher.id);
                })
            });

        }

        //define the variable so it is inherited in child scopes
        $scope.schoolTerms = [];
        $scope.schoolYears = [];
        $scope.schoolWeeks = [];
        $scope.schoolWeeksTrendHolderTracker = [];//this only ensures we don't save one week twice
        $scope.schoolWeeksTrendHolder = [];//this holds the weeks to be displayed in the trends
        $scope.cs_comments = {};
        $scope.loadingWeeklyDataParams = true;//show a gif when loading teachers in the school



        function prepWeeklyDataParams(weeklyData) {
            //Loop through the weekly submitted data and save the weeks, terms, and years for the  generate report dropdown
            angular.forEach(weeklyData.data, function(week, week_index){

                if ( $scope.cs_comments[week.week_number + week.term + week.year]) {
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_students_attendance_comments += scGlobalService.appendIfNotNull(week.cs_students_attendance_comments);
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_students_enrolment_comments += scGlobalService.appendIfNotNull(week.cs_students_enrolment_comments);
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_teacher_attendance_comments += scGlobalService.appendIfNotNull(week.cs_teacher_attendance_comments);
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_teacher_class_management_comments += scGlobalService.appendIfNotNull(week.cs_teacher_class_management_comments);
                }else{
                    /*The comment may occur in different rows in the db, the appending function is to concatenate all available comments*/
                    $scope.cs_comments[week.week_number + week.term + week.year] = {
                        cs_students_attendance_comments: scGlobalService.appendIfNotNull(week.cs_students_attendance_comments ),
                        cs_students_enrolment_comments: scGlobalService.appendIfNotNull(week.cs_students_enrolment_comments ),
                        cs_teacher_attendance_comments: scGlobalService.appendIfNotNull(week.cs_teacher_attendance_comments ),
                        cs_teacher_class_management_comments: scGlobalService.appendIfNotNull(week.cs_teacher_class_management_comments)
                    };
                }


                //save the current term in the loop to a variable
                //one school term is an object that formats the format returned
                // from the server to a user friendlier one
                var currentTerm = $scope.one_school_terms[week.term];

                //save the current year in the loop to a variable
                var currentYear = week.year;

                //save the current week in the loop to a variable
                var currentWeek = week.week_number;


                // check if the current term isn't already added to the scope variable that displays the available terms
                //in the submitted data
                if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                    $scope.schoolTerms.push(currentTerm);

                    $scope.schoolTerms.sort(function(a,b){
                        return a.number.charAt(0) - b.number.charAt(0)
                    });

                    $scope.filter_term = week.term;
                }

                // check if the current year isn't already added to the scope variable that displays the available years
                //in the submitted data
                if ($scope.schoolYears.indexOf(currentYear) < 0) {
                    $scope.schoolYears.push(currentYear);
                    $scope.filter_year = currentYear;
                }

                // check if the current week isn't already added to the scope variable that displays the available weeks
                //in the submitted data
                if ($scope.schoolWeeks.indexOf(currentWeek) < 0) {
                    $scope.schoolWeeks.push(currentWeek);

                    $scope.schoolWeeks.sort(function(a,b){
                        return a - b
                    });

                    $scope.filter_week = currentWeek;
                }
                // check if the current week isn't already added to the scope variable 
                // for the data trending in the submitted data
                if ($scope.schoolWeeksTrendHolderTracker.indexOf(currentYear+currentTerm.value+currentWeek) < 0) {
                    $scope.schoolWeeksTrendHolder.push({
                        week : currentWeek,
                        term : currentTerm.number,
                        termValue : currentTerm.value,
                        termObj : currentTerm,
                        year : currentYear
                    });
                    $scope.schoolWeeksTrendHolder.sort(function(a,b){
                        return a.week - b.week
                    });

                    $scope.schoolWeeksTrendHolderTracker.push(currentYear+currentTerm.value+currentWeek);

                }
            });


            delete $scope.schoolWeeksTrendHolderTracker; //free some memory of the scope

        }

        scSchool.fetchAvailableWeeklySubmittedData($stateParams.id, 'weekly_total', 'weekly')
            .then(function (data) {
                if (data) {

                    $timeout(function () {
                        prepWeeklyDataParams(data);
                        $scope.loadingWeeklyDataParams = false;
                    }, 1);
                }
            });

        $scope.emptyCache = function () {
            scSchool.clearSchoolCache();
            location.reload(true);
        }
    }]);