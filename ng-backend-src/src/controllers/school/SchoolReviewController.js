/**
 * Created by Kaygee on 14/07/2014.
 */


//School Review Main Controller
schoolMonitor.controller('scViewSchoolReviewMainController',
    ['$scope', '$stateParams', 'scSchool', '$timeout',
        'schoolInclusiveData', 'schoolEffectiveTeachingData', 'schoolHealthyData',
        'schoolSafeProtectiveData','schoolFriendlyData', 'schoolCommunityInvolvementData','districtItinerary',

        function ($scope, $stateParams, scSchool, $timeout, schoolInclusiveData, schoolEffectiveTeachingData,
                  schoolHealthyData, schoolSafeProtectiveData,schoolFriendlyData, schoolCommunityInvolvementData,districtItinerary ) {





            function prepSchoolData() {
                $scope.schoolReviewRatingStar = ( $scope.selected_school.rating / 20 );

                $scope.schoolsDistrictItinerary = districtItinerary;

            }

            $scope.$on('updateLoadedModelsFromRefresh', function(evt, data){
                if (data.counter == 6) {
                    $timeout(function () {
                        prepSchoolData();
                    }, 10)
                }

            });

            if (scSchool.schools.length&& scSchool.schoolLookup) {
                prepSchoolData()
            }


        }
    ]);

//School Review Inclusive Controller
schoolMonitor.controller('scViewSchoolReviewInclusiveController',
    ['$scope', '$stateParams', 'scSchool', '$modal', '$log', 'schoolInclusiveData',

        function ($scope, $stateParams, scSchool, $modal, $log, schoolInclusiveData) {

            $scope.reviewData = schoolInclusiveData;

        }
    ]);

//School Review Effective Teaching Controller
schoolMonitor.controller('scViewSchoolReviewEffectiveTeachingController',
    ['$scope', '$stateParams', 'scSchool', '$modal', '$log', 'schoolEffectiveTeachingData',

        function ($scope, $stateParams, scSchool, $modal, $log, schoolEffectiveTeachingData) {


            $scope.reviewData = schoolEffectiveTeachingData;

        }
    ]);

//School Review Healthy Controller
schoolMonitor.controller('scViewSchoolReviewHealthyController',
    ['$scope', '$stateParams', 'scSchool', '$modal', '$log', 'schoolHealthyData',

        function ($scope, $stateParams, scSchool, $modal, $log, schoolHealthyData) {


            $scope.reviewData = schoolHealthyData;

        }
    ]);

//School Review Safe and Protective Controller
schoolMonitor.controller('scViewSchoolReviewSafeProtectiveController',
    ['$scope', '$stateParams', 'scSchool', '$modal', '$log', 'schoolSafeProtectiveData',

        function ($scope, $stateParams, scSchool, $modal, $log, schoolSafeProtectiveData) {


            $scope.reviewData = schoolSafeProtectiveData;

        }
    ]);

//School Review Friendly for Boys and Girls Controller
schoolMonitor.controller('scViewSchoolReviewFriendlyController',
    ['$scope', '$stateParams', 'scSchool', '$modal', '$log', 'schoolFriendlyData',

        function ($scope, $stateParams, scSchool, $modal, $log, schoolFriendlyData) {


            $scope.reviewData = schoolFriendlyData;


        }
    ]);

//School ReviewCommunity Involvement Controller
schoolMonitor.controller('scViewSchoolReviewCommunityInvolvementController',
    ['$scope', '$stateParams', 'scSchool', '$modal', '$log', 'schoolCommunityInvolvementData',

        function ($scope, $stateParams, scSchool, $modal, $log, schoolCommunityInvolvementData) {

            $scope.reviewData = schoolCommunityInvolvementData;

        }]);

