/**
 * Created by Kaygee on 21/05/2015.
 */


//School Visit Controller (SV)
schoolMonitor.controller('scViewSchoolVisitController',
    ['$scope', '$stateParams', 'School', '$modal', '$log', 'allSchoolVisitsData','districtItinerary',

        function ($scope, $stateParams, School, $modal, $log, allSchoolVisitsData, districtItinerary) {

            $scope.schoolVisitData = allSchoolVisitsData;

            $scope.schoolsDistrictItinerary = districtItinerary;

        }
    ]);

