/**
 * Created by Kaygee on 21/05/2015.
 */
//SRC - School Management
schoolMonitor.controller('scViewSchoolManagementController',
    ['$scope', '$stateParams', 'School', '$modal', '$rootScope','allSchoolTextbookData','allSchoolPupilPerformanceData', 'allRecordPerformanceData',
        'allSupportTypesData', 'allSchoolGrantCapitationPaymentsData', 'scGlobalService',

        function ($scope, $stateParams, School, $modal, $rootScope, allSchoolTextbookData, allSchoolPupilPerformanceData, allRecordPerformanceData,
                  allSupportTypesData, allSchoolGrantCapitationPaymentsData, scGlobalService) {

            $scope.schoolTerms = [];
            $scope.schoolYears = [];
            $scope.schoolWeeks = [];

            /*--- Pupil Textbooks  ---*/
            var runTextBooks = function(allSchoolTextbookData){
                $scope.pupilTextbooks = [];
                $scope.pupilTextbooksLocation = {};
                $scope.textBookComment = {};
                $scope.textBookUpdated = {};
                angular.forEach(allSchoolTextbookData[0].school_textbooks, function(item , index){

                    var textbooksTempObj = {
                        class : item.level,
                        gh_lang : item.number_of_ghanaian_language_textbooks,
                        english : item.number_of_english_textbooks,
                        maths : item.number_of_maths_textbooks,
                        year : item.year,
                        data_collector_type : item.data_collector_type,
                        term : item.term
                        //comment : item.comment
                    };

                    $scope.pupilTextbooks.push(textbooksTempObj);

                    var concatenate =item.year + item.term + item.data_collector_type;

                    //Assign the comments to an associative object based on term so we can change it from the dropdown
                    $scope.textBookComment[concatenate] = {
                        ht_comment : item.comment,
                        cs_comments : item.cs_comments
                    };

                    //Assign the date updated to an associative object based on term so we can change it from the dropdown
                    $scope.textBookUpdated[concatenate]= item.updated_at;


                    $scope.pupilTextbooksLocation[concatenate] = {
                        venue : null,
                        longitude :  item.long,
                        latitude :  item.lat
                    };

                    //save the current term in the loop to a variable
                    //one school term is an object that formats the fromat returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[item.term];

                    //save the current year in the loop to a variable
                    var currentYear = item.year;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }


                    $scope.filter_term = item.term;
                    $scope.filter_year = item.year;
                    $scope.filter_data_collector_type = item.data_collector_type;

                });
            };


            /*--- Pupil Performance  ---*/
            var runPupilPerformance = function(allSchoolPupilPerformanceData){
                $scope.pupilPerformance = [];
                $scope.pupilPerformanceLocation = {};
                $scope.pupilPerformanceComment = {};
                $scope.pupilPerformanceUpdated = {};


                angular.forEach(allSchoolPupilPerformanceData[0].pupils_performance, function(item , index){

                    var pupilPerformanceTempObj = {
                        class : item.level,
                        gh_lang_average_score : item.average_ghanaian_language_score,
                        number_scoring_above_gh_lang_average_score : item.num_pupil_who_score_above_average_in_ghanaian_language,
                        english_average_score : item.average_english_score,
                        number_scoring_above_english_score : item.num_pupil_who_score_above_average_in_english,
                        maths_average_score : item.average_maths_score,
                        number_scoring_above_maths_score : item.num_pupil_who_score_above_average_in_maths,
                        science_average_score : item.average_science_score,
                        number_scoring_above_science_score : item.num_pupil_who_score_above_average_in_science,
                        num_pupil_who_can_read_and_write : item.num_pupil_who_can_read_and_write,
                        year : item.year,
                        data_collector_type : item.data_collector_type,
                        term : item.term,
//                    week : item.week_number,
                        comment : item.comment
                    };

                    $scope.pupilPerformance.push(pupilPerformanceTempObj);

                    //Assign the comments to an associative object based on term so we can change it from the dropdown
                    var concatenate =item.year + item.term + item.data_collector_type;
                    $scope.pupilPerformanceComment[concatenate] = {
                        ht_comment : item.comment,
                        cs_comments : item.cs_comments
                    };


                    //Assign the date updated to an associative object based on term so we can change it from the dropdown
                    $scope.pupilPerformanceUpdated[concatenate]= item.updated_at;


                    $scope.pupilPerformanceLocation[concatenate] = {
                        venue : null,
                        longitude :  item.long,
                        latitude :  item.lat
                    };


                    //save the current term in the loop to a variable
                    //one school term is an object that formats the fromat returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[item.term];

                    //save the current year in the loop to a variable
                    var currentYear = item.year;

                    //save the current week in the loop to a variable
//                var currentWeek = item.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }


                    $scope.filter_term = item.term;
                    $scope.filter_year = item.year;

                });
            };


            /*----Record and Performance ----*/
            var runSchoolRecords = function(allRecordPerformanceData){
                $scope.books = [];
                $scope.booksLocation = {};
                $scope.booksUpdated = {};
                $scope.booksComment = {};

                angular.forEach(allRecordPerformanceData, function(item,index){

                    var booksHolder =[];


                    booksHolder.push({
                        name: "Admission Register",
                        term :   item.term,
                        status : scGlobalService.makeFirstLetterCapital(item.admission_register),
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Class Registers",
                        term :  item.term,
                        status : scGlobalService.makeFirstLetterCapital(item.class_registers),
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Teacher Attendance Register",
                        status: scGlobalService.makeFirstLetterCapital(item.teacher_attendance_register),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Visitors\' Book",
                        status: scGlobalService.makeFirstLetterCapital(item.visitors),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Log Book",
                        status: scGlobalService.makeFirstLetterCapital(item.log),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "SBA",
                        status: scGlobalService.makeFirstLetterCapital(item.sba),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Movement Book",
                        status: scGlobalService.makeFirstLetterCapital(item.movement),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "SPIP",
                        status: scGlobalService.makeFirstLetterCapital(item.spip),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Inventory Books",
                        status: scGlobalService.makeFirstLetterCapital(item.inventory_books),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Cumulative Record Books",
                        status: scGlobalService.makeFirstLetterCapital(item.cummulative_records_books),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });

                    booksHolder.push({
                        name: "Continuous Assessment / SMB",
                        status: scGlobalService.makeFirstLetterCapital(item.continous_assessment || 'no'),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    $scope.books = $scope.books.concat(booksHolder);

                    var concatenate =item.year + item.term + item.data_collector_type;

                    //Assign the comments to an associative object based on term so we can change it from the dropdown
                    $scope.booksComment[concatenate] = {
                        ht_comment : item.comment,
                        cs_comments : item.cs_comments
                    };

                    //Assign the date updated to an associative object based on term so we can change it from the dropdown
                    $scope.booksUpdated[concatenate] = item.updated_at;

                    $scope.booksLocation[concatenate] = {
                        venue : null,
                        longitude :  item.long,
                        latitude :  item.lat,
                        id : item.id
                    };

                    //save the current term in the loop to a variable
                    //one school term is an object that formats the fromat returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[item.term];

                    //save the current year in the loop to a variable
                    var currentYear = item.year;

                    //save the current week in the loop to a variable
//                var currentWeek = item.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    // check if the current week isn't already added to the scope variable that displays the available weeks
                    //in the submitted data
//                if ($scope.schoolWeeks.indexOf(currentWeek) < 0) {
//                    $scope.schoolWeeks.push(currentWeek);
//                }

                    $scope.filter_term = item.term;
//                $scope.filter_week = item.week_number;
                    $scope.filter_year = item.year;
                    $scope.filter_data_collector_type = item.data_collector_type;

                });
            };


            /*----Support Types ----*/
            var runSupport = function(allSupportTypesData){
                $scope.supportTypes = [];
                $scope.supportTypesLocation = {};
                $scope.supportTypesComment = {};
                $scope.supportTypesUpdated = {};

                angular.forEach(allSupportTypesData[0].school_support_type, function(item,index){

                    var supportTypeHolder =[];
//                var format_term = $scope.academicTermDisplay(item.term);
                    supportTypeHolder.push({
                        name: "Donor Support",
                        term :   item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type,
                        in_cash : item.donor_support_in_cash,
                        in_kind : item.donor_support_in_kind,
                        comment : item.comment
                    });
                    supportTypeHolder.push({
                        name: "Community Support",
                        term :   item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type,
                        in_cash : item.community_support_in_cash,
                        in_kind : item.community_support_in_kind,
                        comment : item.comment
                    });
                    supportTypeHolder.push({
                        name: "District Support",
                        term :   item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type,
                        in_cash : item.district_support_in_cash,
                        in_kind : item.district_support_in_kind,
                        comment : item.comment
                    });
                    supportTypeHolder.push({
                        name: "PTA Support",
                        term :   item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type,
                        in_cash : item.pta_support_in_cash,
                        in_kind : item.pta_support_in_kind,
                        comment : item.comment
                    });
                    supportTypeHolder.push({
                        name: "Other",
                        term :   item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type,
                        in_cash : item.other_support_in_cash,
                        in_kind : item.other_support_in_kind,
                        comment : item.comment
                    });
                    $scope.supportTypes = $scope.supportTypes.concat(supportTypeHolder);

                    var concatenate =item.year + item.term + item.data_collector_type;

                    //Assign the comments to an associative object based on term so we can change it from the dropdown
                    $scope.supportTypesComment[concatenate] = {
                        ht_comment : item.comment,
                        cs_comments : item.cs_comments
                    };

                    //Assign the date updated to an associative object based on term so we can change it from the dropdown
                    $scope.supportTypesUpdated[concatenate]= item.updated_at;


                    $scope.supportTypesLocation[concatenate] = {
                        venue : null,
                        longitude :  item.long,
                        latitude :  item.lat,
                        id : item.id
                    };

                    //save the current term in the loop to a variable
                    //one school term is an object that formats the fromat returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[item.term];

                    //save the current year in the loop to a variable
                    var currentYear = item.year;

                    //save the current week in the loop to a variable
//                var currentWeek = item.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    // check if the current week isn't already added to the scope variable that displays the available weeks
                    //in the submitted data
//                if ($scope.schoolWeeks.indexOf(currentWeek) < 0) {
//                    $scope.schoolWeeks.push(currentWeek);
//                }

                    $scope.filter_term = item.term;
//                $scope.filter_week = item.week_number;
                    $scope.filter_year = item.year;
                    $scope.filter_data_collector_type = item.data_collector_type;

                });
            };

            /*----School Grant ----*/

            var runGrants = function(allSchoolGrantCapitationPaymentsData){
                $scope.schoolGrants = [];
                $scope.schoolGrantsLocation = {};
                $scope.schoolGrantsUpdated = {};
                $scope.schoolGrantsComment = {};

                angular.forEach(allSchoolGrantCapitationPaymentsData[0].school_grant, function(item,index){
                    $scope.schoolGrants.push(item);

                    //Assign the date updated to an associative object based on term so we can change it from the dropdown
//                $scope.schoolGrants[item.term]= item.updated_at;

                    //Assign the comments to an associative object based on term so we can change it from the dropdown
                    var concatenate = item.year + item.term  + item.data_collector_type;
                    $scope.schoolGrantsComment
                        [concatenate] = {
                        ht_comment : item.comment,
                        cs_comments : item.cs_comments
                    };


                    //Assign the date updated to an associative object based on term so we can change it from the dropdown
                    $scope.schoolGrantsUpdated[concatenate]= item.updated_at;


                    //save the current term in the loop to a variable
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[item.term];

                    //save the current year in the loop to a variable
                    var currentYear = item.year;

                    //save the current week in the loop to a variable
//                var currentWeek = item.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    // check if the current week isn't already added to the scope variable that displays the available weeks
                    //in the submitted data
//                if ($scope.schoolWeeks.indexOf(currentWeek) < 0) {
//                    $scope.schoolWeeks.push(currentWeek);
//                }

                    $scope.filter_term = item.term;
//                $scope.filter_week = item.week_number;
                    $scope.filter_year = item.year;
                    $scope.filter_data_collector_type = item.data_collector_type;

                    $scope.schoolGrantsLocation[concatenate] = {
                        venue : null,
                        longitude :  item.long,
                        latitude :  item.lat,
                        id : item.id
                    };


                });
            };



            runTextBooks(allSchoolTextbookData);
            $scope.runTextBooks = function(){
                $rootScope.loading = true;
                School.allSchoolTextbook({school_id : $stateParams.id}).$promise
                    .then(function(allSchoolTextbookData){
                        runTextBooks(allSchoolTextbookData);
                        $rootScope.loading = false;
                    });
            };

            runPupilPerformance(allSchoolPupilPerformanceData);
            $scope.runPupilPerformance = function(){
                $rootScope.loading = true;
                School.allSchoolPupilPerformance({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolPupilPerformanceData) {
                        runPupilPerformance(allSchoolPupilPerformanceData);
                        $rootScope.loading = false;
                    })
            };

            runSchoolRecords(allRecordPerformanceData);
            $scope.runSchoolRecords = function(){
                $rootScope.loading = true;
                School.allRecordPerformanceData({school_id : $stateParams.id}).$promise
                    .then(function (allRecordPerformanceData) {
                        runSchoolRecords(allRecordPerformanceData);
                        $rootScope.loading = false;
                    })
            };

            runSupport(allSupportTypesData);
            $scope.runSupport = function(){
                $rootScope.loading = true;
                School.allSupportTypes({school_id : $stateParams.id}).$promise
                    .then(function (allSupportTypesData) {
                        runSupport(allSupportTypesData);
                        $rootScope.loading = false;
                    })
            };

            runGrants(allSchoolGrantCapitationPaymentsData);
            $scope.runGrants = function(){
                $rootScope.loading = true;
                School.allSchoolGrantCapitationPayments({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolGrantCapitationPaymentsData) {
                        runGrants(allSchoolGrantCapitationPaymentsData);
                        $rootScope.loading = false;
                    })
            };

        }
    ]);

