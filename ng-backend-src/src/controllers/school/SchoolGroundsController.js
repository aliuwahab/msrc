/**
 * Created by Kaygee on 21/05/2015.
 */


//SRC - School Grounds
schoolMonitor.controller('scViewSchoolGroundsController',
    ['$scope', '$stateParams', 'School', '$modal', '$rootScope',

        'allSchoolSanitation', 'allSchoolSecurity', 'allSchoolRecreation_Equipment', 'allSchoolStructure', 'allSchoolFurniture',

        function ($scope, $stateParams, School, $modal, $rootScope,
                  allSchoolSanitation, allSchoolSecurity, allSchoolRecreation_Equipment,  allSchoolStructure, allSchoolFurniture) {

            /* --------------------------  Sanitation   -------------------------*/


            $scope.schoolTerms = [];
            $scope.schoolYears = [];
            $scope.schoolWeeks = [];

            /* ---  Sanitation   ---*/
            var fetchSanitationData = function(allSchoolSanitation){
                $scope.sanitationItems = [];
                $scope.sanitationComment = {};
                $scope.sanitationUpdated = {};
                $scope.sanitationLocation = {};
                angular.forEach(allSchoolSanitation, function(sanitationObject, index){

                    //An empty object that gets initialized every time the loop runs.
                    //It is used to hold the items in object being looped over for the purpose of display
                    var sanitationHolder =[];

                    //reformatting the way the objects are coming from the server tp be used in the table for display
                    sanitationHolder.push({
                        name: "Toilet",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.toilet],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Urinal",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.urinal],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Water",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.water],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Dust Bins",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.dust_bins],
                        year : sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Veronica Buckets",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
                        year :  sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });

                    var concatenate = sanitationObject.year + sanitationObject.term + sanitationObject.data_collector_type;

                    //Assign the comments to an associative object based on term so we can change it from the dropdown
                    $scope.sanitationComment[concatenate] = {
                        ht_comment : sanitationObject.comment,
                        cs_comments : sanitationObject.cs_comments
                    };

                    //Assign the date updated to an associative object based on term so we can change it from the dropdown
                    $scope.sanitationUpdated[concatenate]= sanitationObject.updated_at;

                    //Merge the contents of the temp object to the scope variable that will display in the view
                    $scope.sanitationItems = $scope.sanitationItems.concat(sanitationHolder);


                    $scope.sanitationLocation[concatenate] = {
                        venue : null,
                        longitude :  sanitationObject.long,
                        latitude :  sanitationObject.lat,
                        id : sanitationObject.id
                    };


                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms [sanitationObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = sanitationObject.year;

                    //save the current week in the loop to a variable
//                var currentWeek = sanitationObject.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }


                    //These assign the scope variable to the latest term, week and year
                    $scope.filter_term = currentTerm.value;
//                $scope.filter_week = sanitationObject.week_number;
                    $scope.filter_year = currentYear.toString();

                    $scope.filter_data_collector_type = sanitationObject.data_collector_type;

                }) ;

            };

            /* ---  Security   ---*/
            function fetchSecurityData(allSchoolSecurity) {
                $scope.securityItems = [];
                $scope.securityComment = {};
                $scope.securityUpdated = {};
                $scope.securityLocation = {};

                angular.forEach(allSchoolSecurity, function (securityObject, index) {

//                $scope.filter_term = $scope.academicTermDisplay(securityObject.term);
//                $scope.filter_year = $scope.thisYear;

                    var securityHolder = [];
//                var format_term = $scope.academicTermDisplay(securityObject.term);
                    securityHolder.push({
                        name: "Walled / Fenced",
                        term: securityObject.term,
                        status: $scope.formatAvailability[securityObject.walled],
                        year: securityObject.year,
                        comment: securityObject.comment,
                        data_collector_type: securityObject.data_collector_type
                    });

                    securityHolder.push({
                        name: "Gated",
                        term: securityObject.term,
                        status: $scope.formatAvailability[securityObject.gated],
                        year: securityObject.year,
                        comment: securityObject.comment,
                        data_collector_type: securityObject.data_collector_type
                    });
                    securityHolder.push({
                        name: "Lights",
                        term: securityObject.term,
                        status: $scope.formatAvailability[securityObject.lights],
                        year: securityObject.year,
                        comment: securityObject.comment,
                        data_collector_type: securityObject.data_collector_type
                    });
                    securityHolder.push({
                        name: "Security Man",
                        term: securityObject.term,
                        status: $scope.formatAvailability[securityObject.security_man],
                        year: securityObject.year,
                        comment: securityObject.comment,
                        data_collector_type: securityObject.data_collector_type
                    });

                    var concatenate = securityObject.year + securityObject.term + securityObject.data_collector_type;

                    $scope.securityLocation[concatenate] = {
                        venue: null,
                        longitude: securityObject.long,
                        latitude: securityObject.lat,
                        id: securityObject.id
                    };

                    $scope.securityComment[concatenate] = {
                        ht_comment: securityObject.comment,
                        cs_comments: securityObject.cs_comments
                    };

                    $scope.securityUpdated[concatenate] = securityObject.updated_at;

                    $scope.securityItems = $scope.securityItems.concat(securityHolder);


                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[securityObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = securityObject.year;

                    //save the current week in the loop to a variable
//                var currentWeek = sanitationObject.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    //These assign the scope variable to the latest term, week and year
                    $scope.filter_term = currentTerm.value;
//                $scope.filter_week = sanitationObject.week_number;
                    $scope.filter_year = currentYear.toString();
                    $scope.filter_data_collector_type = securityObject.data_collector_type;
                });
            }

            /* ---  Recreation / Sports   ---*/
            function fetchSportsData(allSchoolRecreation_Equipment) {
                $scope.recreationItems = [];
                $scope.recreationComment = {};
                $scope.recreationUpdated = {};
                $scope.recreationLocation = {};

                angular.forEach(allSchoolRecreation_Equipment, function (recreationObject, index) {

                    var recreationHolder = [];
                    recreationHolder.push({
                        name: "Football",
                        term: recreationObject.term,
                        status: $scope.formatAvailability[recreationObject.football],
                        year: recreationObject.year,
                        comment: recreationObject.comment,
                        data_collector_type: recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Volleyball",
                        term: recreationObject.term,
                        status: $scope.formatAvailability[recreationObject.volleyball],
                        year: recreationObject.year,
                        comment: recreationObject.comment,
                        data_collector_type: recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Netball",
                        term: recreationObject.term,
                        status: $scope.formatAvailability[recreationObject.netball],
                        year: recreationObject.year,
                        comment: recreationObject.comment,
                        data_collector_type: recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Playing Field",
                        term: recreationObject.term,
                        status: $scope.formatAvailability[recreationObject.playing_field],
                        year: recreationObject.year,
                        comment: recreationObject.comment,
                        data_collector_type: recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Sports Wear",
                        term: recreationObject.term,
                        status: $scope.formatAvailability[recreationObject.sports_wear],
                        year: recreationObject.year,
                        comment: recreationObject.comment,
                        data_collector_type: recreationObject.data_collector_type
                    });
                    try {
                        recreationHolder.push({
                            name: "Seesaw",
                            term: recreationObject.term,
                            status: $scope.formatAvailability[recreationObject.seesaw],
                            year: recreationObject.year,
                            comment: recreationObject.comment,
                            data_collector_type: recreationObject.data_collector_type
                        })
                    } catch (e) {

                    }

                    try {
                        recreationHolder.push({
                            name: "Merry-Go-Round",
                            term: recreationObject.term,
                            status: $scope.formatAvailability[recreationObject.merry_go_round],
                            year: recreationObject.year,
                            comment: recreationObject.comment,
                            data_collector_type: recreationObject.data_collector_type
                        });
                    } catch (e) {

                    }


                    recreationHolder.push({
                        name: "First Aid Box",
                        term: recreationObject.term,
                        status: $scope.formatAvailability[recreationObject.first_aid_box],
                        year: recreationObject.year,
                        comment: recreationObject.comment,
                        data_collector_type: recreationObject.data_collector_type
                    });

                    var concatenate = recreationObject.year + recreationObject.term + recreationObject.data_collector_type;

                    $scope.recreationLocation[concatenate] = {
                        venue: null,
                        longitude: recreationObject.long,
                        latitude: recreationObject.lat,
                        id: recreationObject.id
                    };

                    $scope.recreationComment[concatenate] = {
                        ht_comment: recreationObject.comment,
                        cs_comments: recreationObject.cs_comments
                    };

                    $scope.recreationUpdated[concatenate] = recreationObject.updated_at;

                    $scope.recreationItems = $scope.recreationItems.concat(recreationHolder);

                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[recreationObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = recreationObject.year;

                    //save the current week in the loop to a variable
//                var currentWeek = sanitationObject.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    //These assign the scope variable to the latest term, week and year
                    $scope.filter_term = currentTerm.value;
//                $scope.filter_week = sanitationObject.week_number;
                    $scope.filter_year = currentYear.toString();

                    $scope.filter_data_collector_type = recreationObject.data_collector_type;

                });
            }

            /* ---  Structure   ---*/
            function fetchStructureData(allSchoolStructure) {
                $scope.structureItems = [];
                $scope.structureComment = {};
                $scope.structureUpdated = {};
                $scope.structureLocation = {};

                angular.forEach(allSchoolStructure, function (structureObject, index) {

                    $scope.filter_term = $scope.academicTermDisplay(structureObject.term);
                    $scope.filter_year = $scope.thisYear;

                    var structureHolder = [];

                    structureHolder.push({
                        name: "Walls",
                        term: structureObject.term,
                        status: $scope.formatGoodPoorFair[structureObject.walls],
                        year: structureObject.year,
                        comment: structureObject.comment,
                        data_collector_type: structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Doors",
                        term: structureObject.term,
                        status: $scope.formatGoodPoorFair[structureObject.doors],
                        year: structureObject.year,
                        comment: structureObject.comment,
                        data_collector_type: structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Windows",
                        term: structureObject.term,
                        status: $scope.formatGoodPoorFair[structureObject.windows],
                        year: structureObject.year,
                        comment: structureObject.comment,
                        data_collector_type: structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Floors",
                        term: structureObject.term,
                        status: $scope.formatGoodPoorFair[structureObject.floors],
                        year: structureObject.year,
                        comment: structureObject.comment,
                        data_collector_type: structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Blackboard",
                        term: structureObject.term,
                        status: $scope.formatGoodPoorFair[structureObject.blackboard],
                        year: structureObject.year,
                        comment: structureObject.comment,
                        data_collector_type: structureObject.data_collector_type
                    });
                    try {
                        structureHolder.push({
                            name: "Illumination",
                            term: structureObject.term,
                            status: $scope.formatGoodPoorFair[structureObject.illumination],
                            year: structureObject.year,
                            comment: structureObject.comment,
                            data_collector_type: structureObject.data_collector_type
                        });
                    } catch (e) {
                        console.log(e);
                        structureHolder.push({
                            name: "Illumination",
                            term: structureObject.term,
                            status: ' ',
                            year: structureObject.year,
                            comment: structureObject.comment,
                            data_collector_type: structureObject.data_collector_type
                        });
                    }


                    var concatenate = structureObject.year + structureObject.term + structureObject.data_collector_type;

                    $scope.structureLocation[concatenate] = {
                        venue: null,
                        longitude: structureObject.long,
                        latitude: structureObject.lat,
                        id: structureObject.id
                    };

                    $scope.structureComment[concatenate] = {
                        ht_comment: structureObject.comment,
                        cs_comments: structureObject.cs_comments
                    };

                    $scope.structureUpdated[concatenate] = structureObject.updated_at;

                    $scope.structureItems = $scope.structureItems.concat(structureHolder);

                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[structureObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = structureObject.year;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }


                    //These assign the scope variable to the latest term, week and year
                    $scope.filter_term = currentTerm.value;
//                $scope.filter_week = sanitationObject.week_number;
                    $scope.filter_year = currentYear.toString();

                    $scope.filter_data_collector_type = structureObject.data_collector_type;

                });
            }

            /* ---  Furniture   ---*/
            function fetchFurnitureData(allSchoolFurniture) {
                $scope.furnitureItems = [];
                $scope.furnitureUpdated = {};
                $scope.furnitureComment = {};
                $scope.furnitureLocation = {};

                angular.forEach(allSchoolFurniture, function (furnitureObject, index) {

                    var furnitureHolder = [];

                    furnitureHolder.push({
                        name: "Pupils Furniture",
                        term: furnitureObject.term,
                        status: $scope.formatAdequecy[furnitureObject.pupils_furniture],
                        year: furnitureObject.year,
                        comment: furnitureObject.comment,
                        data_collector_type: furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Teacher Tables",
                        term: furnitureObject.term,
                        status: $scope.formatAdequecy[furnitureObject.teacher_tables],
                        year: furnitureObject.year,
                        comment: furnitureObject.comment,
                        data_collector_type: furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Teacher Chairs",
                        term: furnitureObject.term,
                        status: $scope.formatAdequecy[furnitureObject.teacher_chairs],
                        year: furnitureObject.year,
                        comment: furnitureObject.comment,
                        data_collector_type: furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Classrooms Cupboard",
                        term: furnitureObject.term,
                        status: $scope.formatAdequecy[furnitureObject.classrooms_cupboard],
                        year: furnitureObject.year,
                        comment: furnitureObject.comment,
                        data_collector_type: furnitureObject.data_collector_type
                    });

                    var concatenate = furnitureObject.year + furnitureObject.term + furnitureObject.data_collector_type;

                    $scope.furnitureLocation[concatenate] = {
                        venue: null,
                        longitude: furnitureObject.long,
                        latitude: furnitureObject.lat,
                        id: furnitureObject.id
                    };

                    $scope.furnitureComment[concatenate] = {
                        ht_comment: furnitureObject.comment,
                        cs_comments: furnitureObject.cs_comments
                    };

                    $scope.furnitureUpdated[concatenate] = furnitureObject.updated_at;

                    $scope.furnitureItems = $scope.furnitureItems.concat(furnitureHolder);

                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[furnitureObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = furnitureObject.year;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    //These assign the scope variable to the latest term, week and year
                    $scope.filter_term = currentTerm.value;
                    $scope.filter_year = currentYear.toString();

                    $scope.filter_data_collector_type = furnitureObject.data_collector_type;

                });
            }




            /*
                allSchoolRecreation_Equipment : function(School, $stateParams){
                    return School.allSchoolRecreation_Equipment({school_id : $stateParams.id}).$promise
                },
                allSchoolSecurity : function(School, $stateParams){
                    return School.allSchoolSecurity({school_id : $stateParams.id}).$promise
                },
                allSchoolStructure : function(School, $stateParams){
                    return School.allSchoolStructure({school_id : $stateParams.id}).$promise
                },
                allSchoolFurniture : function(School, $stateParams){
                    return School.allSchoolFurniture({school_id : $stateParams.id}).$promise
                }
    */

            fetchSanitationData(allSchoolSanitation);
            $scope.fetchSanitationData = function(){
                $rootScope.loading = true;

                School.allSchoolSanitation({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolSanitation) {
                        fetchSanitationData(allSchoolSanitation);
                        $rootScope.loading = false;

                    })
            };


            fetchSecurityData(allSchoolSecurity);
            $scope.fetchSecurityData = function(){
                $rootScope.loading = true;
                School.allSchoolSecurity({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolSecurity) {
                        fetchSecurityData(allSchoolSecurity);
                        $rootScope.loading = false;
                    });
            };

            fetchSportsData(allSchoolRecreation_Equipment);
            $rootScope.loading = true;
            $scope.fetchSportsData = function(){
                School.allSchoolRecreation_Equipment({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolRecreation_Equipment) {
                        fetchSportsData(allSchoolRecreation_Equipment);
                        $rootScope.loading = false;
                    })
            };

            fetchStructureData(allSchoolStructure);
            $scope.fetchStructureData = function(){
                $rootScope.loading = true;
                School.allSchoolStructure({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolStructure) {
                        fetchStructureData(allSchoolStructure);
                        $rootScope.loading = false;
                    })
            };

            fetchFurnitureData(allSchoolFurniture);
            $scope.fetchFurnitureData = function () {
                $rootScope.loading = true;
                School.allSchoolFurniture({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolFurniture) {
                        fetchFurnitureData(allSchoolFurniture);
                    });
                $rootScope.loading = false;
            }

        }
    ]);
