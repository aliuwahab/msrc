/**
 * Created by Kaygee on 14/07/2014.
 */


//School Review Main Controller
schoolMonitor.controller('scViewSchoolStarsPupilLevellingController',
    ['$scope', '$stateParams', 'scSchool', '$timeout',

        function ($scope, $stateParams, scSchool, $timeout) {

            $scope.enrollmentData = [{
                boys: 55,
                class: "KG1",
                data_collector_type: "head_teacher",
                date_created: 1433258454000,
                girls: 60,
                index: 0,
                normal_id: 323965,
                selected: false,
                special_boys: 0,
                special_girls: 0,
                streams: 1,
                term: "second_term",
                total: 115,
                week: 6,
                year: "2014/2015"
            },
                {
                    boys: 66,
                    class: "KG2",
                    data_collector_type: "head_teacher",
                    date_created: 1433258454000,
                    girls: 70,
                    index: 0,
                    normal_id: 326,
                    selected: false,
                    special_boys: 20,
                    special_girls: 10,
                    streams: 1,
                    term: "second_term",
                    total: 115,
                    week: 6,
                    year: "2014/2015"
                }]

        }]);





