/**
 * Created by Kaygee on 14/07/2014.
 */


//School Review Main Controller
schoolMonitor.controller('scViewSchoolStarsPupilAttendanceController',
    ['$scope', '$stateParams', 'scSchool', 'School', '$timeout', 'STARResource',

        function ($scope, $stateParams, scSchool, School, $timeout, STARResource) {



            $scope.fetchWeeklyData = function loadSchoolDataFromServer(year, term, week) {
                $timeout(function () {
                    $scope.loadingData = true; //this variable checks to display the loading gif whiles loading data
                });
                var dataParams = {school_id: $stateParams.id};
                dataloader = 0; // reset the variable that checks that both normal and special are loaded
                if (week || term || year) {
                    angular.extend(dataParams, {
                        data_collector_type: "head_teacher",
                        week_number: week,
                        term: term,
                        year: year
                    });
                }else {
                    $scope.loadingEverything = true; //ths checks and hides the row that allows loading data
                }
                /* if not, load it from server and keep track of it in the variable */
                STARResource.getStarAttendance(dataParams).$promise
                    .then(function (data) {

                        /*targeted_attendance
:
[{id: 1, school_reference_code: "tr537", primary_four_total_attendance: 40,…}]
0
:
{id: 1, school_reference_code: "tr537", primary_four_total_attendance: 40,…}
circuit_id
:
13
country_id
:
1
created_at
:
"2018-08-13 04:57:18"
data_collector_id
:
23
data_collector_type
:
"head_teacher"
district_id
:
597
id
:
1
is_deleted
:
0
lat
:
"5.044"
long
:
"-0.1792"
number_of_week_days
:
5
primary_five_total_attendance
:
63
primary_four_total_attendance
:
40
primary_six_total_attendance
:
84
region_id
:
9
school_id
:
49
school_reference_code
:
"tr537"
submission_status
:
"approved"
term
:
"second_term"
updated_at
:
"0000-00-00 00:00:00"
week_number
:
1
year
:
"2014/2015"*/
                        STARResource.loadedSpecialAttendanceInfoFromServer = data.targeted_attendance;
                        $scope.$emit('dataLoaded', {loader: ++dataloader});
                    });

                /* if not load it from server and keep track of it in the variable */
                School.allAttendanceData(dataParams).$promise
                    .then(function (data) {
                        scSchool.loadedAttendanceInfoFromServer = data[0].school_attendance;
                        $scope.$emit('dataLoaded', {loader: ++dataloader});
                    });

                /*Load enrolment data and keep it in the service*/
                School.allEnrollmentData(dataParams).$promise
                    .then(function (data) {
                        scSchool.loadedEnrollmentInfoFromServer = data[0].school_enrolment;
                        $scope.$emit('dataLoaded', {loader: ++dataloader});
                    });
            }
        }]);





