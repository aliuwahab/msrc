/**
 * Created by Kaygee on 18/05/2015.
 */
schoolMonitor.controller('scViewSchoolController',
    ['$rootScope', '$scope', '$state', '$stateParams', 'scNotifier', 'School', '$modal', '$log','$filter',
        'scRegion','scDistrict','scCircuit', 'scSchool', 'scHeadTeacher','scCircuitSupervisor','$timeout','DataCollectorHolder',
        function ($rootScope, $scope, $state, $stateParams,scNotifier, School, $modal, $log, $filter,
                  scRegion, scDistrict, scCircuit, scSchool, scHeadTeacher, scCircuitSupervisor, $timeout, DataCollectorHolder) {

            $scope.changePageTitle('View', 'School', 'View');
            //this is a global variable to hold schools to hold schools based on the adminRight login type
            var schoolHolder;
            function assignModels(){

                $scope.regionLookup = scRegion.regionLookup;
                $scope.districtLookup = scDistrict.districtLookup;
                $scope.circuitLookup = scCircuit.circuitLookup;
                $scope.schoolLookup = scSchool.schoolLookup;



                if ($scope.adminRight == 5){
                    $scope.regions = scRegion.regions;
                    $scope.districts = scDistrict.districts;
                    $scope.circuits = scCircuit.circuits;
                    $scope.head_teachers = scHeadTeacher.headteachers;
                    $scope.supervisors = scCircuitSupervisor.supervisors;

                    $scope.schools = scSchool.schools;
                    schoolHolder = scSchool.schools;
                    $scope.schoolsSearch = scSchool.schools;
                    $scope.totalItems =  scSchool.schools.length;

                }
                else if ($scope.adminRight  == 4){
                    $scope.regions = new Array( scRegion.regionLookup[$scope.currentUser.level_id] );
                    $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                    $scope.circuits =  scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers =  scRegion.regionLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors =  scRegion.regionLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.schools = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    schoolHolder = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;

                    $scope.schoolsSearch =  scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems =   scRegion.regionLookup[$scope.currentUser.level_id].totalSchools;

                }
                else if ($scope.adminRight == 3){
                    $scope.regions = new Array( scRegion.regionLookup[scDistrict.districtLookup[$scope.currentUser.level_id].region_id] );
                    $scope.districts = new Array ( scDistrict.districtLookup[$scope.currentUser.level_id] );
                    $scope.circuits = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers = scDistrict.districtLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors =  scDistrict.districtLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.schools = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    schoolHolder = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch =  scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems =   scDistrict.districtLookup[$scope.currentUser.level_id].totalSchools;

                }
                else if ($scope.adminRight == 2){
                    $scope.regions = new Array( scRegion.regionLookup[
                        scDistrict.districtLookup[scCircuit.circuitLookup[$scope.currentUser.level_id].district_id].region_id] );

                    $scope.districts = new Array(
                        scDistrict.districtLookup[scCircuit.circuitLookup[$scope.currentUser.level_id].district_id] );

                    $scope.circuits = new Array( scCircuit.circuitLookup[$scope.currentUser.level_id] );
                    $scope.head_teachers =  scCircuit.circuitLookup[$scope.currentUser.level_id].head_teachers_holder;

                    $scope.schools = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    schoolHolder = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch =  scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems =   scCircuit.circuitLookup[$scope.currentUser.level_id].totalSchools;

                }
                else{
                    $scope.schools = new Array ( $scope.schoolLookup[$scope.currentUser.level_id] );
                    schoolHolder = new Array ( $scope.schoolLookup[$scope.currentUser.level_id] )
                }
                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;
            }



            $scope.$on('updateLoadedModelsFromRefresh', function(evt, data){
                if (data.counter == 6) {
                    $timeout(function () {
                        assignModels();
                    }, 10)
                }

            });


            if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scSchool.schools.length&& scSchool.schoolLookup) {
                assignModels()
            }

            //this is used to sort the districts alphabetically
            $scope.schoolSortOrder = 'name';


            $scope.maxSize = 15;
            $scope.currentPage = 1;
            $scope.nextPage = 0;
//       
//          array.slice(start,end)
            /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
             * It does not include the element at that index*/


            $scope.pageChanged = function(currentPage) {
                $scope.nextPage = ($scope.maxSize  * currentPage) - $scope.maxSize;
                $scope.schools =  schoolHolder.slice($scope.nextPage , ($scope.nextPage + 15) );
                $filter('filter')(  $scope.schools, $scope.schoolSortOrder, false);
            };




            //This is for the capitalization of the 'Add' or 'Edit' parameter to be displayed when
            //adding or editing a school
            $scope.paramAction = function(){
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };


//            This returns the name of the district for the dropdown when selecting a district in add/edit school
            $scope.getDistrictName = function(id){
                $scope.formData.district_name = $scope.districtLookup[id].name;
            };

            //These are the category options for the school category field
            $scope.categoryOptions = [
                {name: "Nursery", option: "nursery", ticked: false  },
                {name: "KG", option: "kg", ticked: false  },
                {name: "Primary", option: "primary", ticked: false  },
                {name: "JHS", option: "jhs", ticked: false  },
                {name: "SHS", option: "shs", ticked: false  },
                {name: "Tertiary", option: "tertiary", ticked: false  }
            ];

            $scope.validateForm= function () {
                $scope.highlightErrors = true;
            };

//        if the action is to edit a district do this
            if ($stateParams.action == 'edit') {
                $rootScope.loading = true;
//            var data = scRegion.retrieveOneRegion($stateParams.id);
                scSchool.retrieveOneSchool($stateParams.id).then(
                    function(data){
                        if (data) {
                            $scope.formData = {
                                id: scSchool.schoolRetrieved.id,
                                school_name: scSchool.schoolRetrieved.name,
                                shift_status: scSchool.schoolRetrieved.shift,
                                description: scSchool.schoolRetrieved.description,
                                school_address: scSchool.schoolRetrieved.address,
                                school_email: scSchool.schoolRetrieved.email,
                                ges_code : scSchool.schoolRetrieved.ges_code,
                                school_phone_number: scSchool.schoolRetrieved.phone_number,
                                country_id: scSchool.schoolRetrieved.country_id,
                                region: scSchool.schoolRetrieved.region_id,
                                district: scSchool.schoolRetrieved.district_id,
                                circuit: scSchool.schoolRetrieved.circuit_id,
                                school_location: scSchool.schoolRetrieved.lat_long,
                                ghanaian_language: scSchool.schoolRetrieved.ghanaian_language,
                                head_teacher: scSchool.schoolRetrieved.headteacher_id,
                                circuit_supervisor: scSchool.schoolRetrieved.supervisor_id
                            };
                            var categoryExplosion = scSchool.schoolRetrieved.category.split(',');
                            angular.forEach( $scope.categoryOptions, function( category, index ) {
                                angular.forEach(categoryExplosion, function( retrievedCategory, index ) {
                                    if ( category.option === retrievedCategory ) {
                                        category.ticked = true;
                                    }
                                });
                            });
                        }
                        $rootScope.loading = false;
                    },
                    function(){
                        scNotifier.notifyFailure('Error', 'There was an error in trying to edit the school');
                        $rootScope.loading = false;
                    });

                //Else if the action is to add, set the form object to empty
            } else if ($stateParams.action == 'add') {
                $scope.formData = {};
            }

            $scope.checkGESCode = function(){
                $scope.gesCodeExists = false;
                if ($scope.formData.ges_code) {
                    angular.forEach($scope.schools, function(school, index){
                        if (school.ges_code == $scope.formData.ges_code  ){
                            if ($stateParams.action == 'add') {
                                $scope.gesCodeExists = true;
                                $scope.gesCodeExistsSchoolName = school.name;
                            }else  if (school.id != $scope.formData.id  ){
                                $scope.gesCodeExists = true;
                                $scope.gesCodeExistsSchoolName = school.name;
                            }
                        }
                    })
                }
            };

            // process the form
            $scope.processForm = function () {
                $scope.validation ={};
                $rootScope.loading = true;
                $scope.checkGESCode();
                if($scope.gesCodeExists){
                    return false;
                }

                var tempArray = [];
//                    assign the mulitiselect options to the form object to be posted
                angular.forEach( $scope.categoryOptions, function( category, index ) {
                    if ( category.ticked === true ) {
                        tempArray.push( category.option)
                    }
                });
                $scope.formData.category = tempArray.toString();


                if ($stateParams.action == 'edit') {
                    scSchool.editSchool($scope.formData).then(
                        function(data){
                            if (data) {
                                scNotifier.notifySuccess('Success', 'School edited successfully ');
                                $state.go('dashboard.school.view_school', {}, {reload : true});
                            }else{
                                console.log(scSchool.validationFields);
                                if (scSchool.validationFields[0] == 'I' && scSchool.validationFields[1] == 't') {
                                    scNotifier.notifyFailure('Error', 'Nothing was changed in the form fields');
                                }else{
                                    angular.forEach(scSchool.validationFields, function(value, prop){
                                        $scope.validation [prop] = true;
                                    });
                                    scNotifier.notifyFailure('Error', 'There were errors in some of the fields');
                                }

                                $rootScope.loading = false;

                            }
                        },
                        function(){
                            console.log('server error');
                            console.log(data);
                            scNotifier.notifyFailure('Error', 'There was an error editing the school');
                            $rootScope.loading = false;
                        });
                } else {
                    $rootScope.loading = true;
                    // process the form for adding a school
                    scSchool.addSchool($scope.formData).then(function (status) {
                        if (status == true) {
                            scNotifier.notifySuccess('Success', 'Successfully created the school');
                            $state.go('dashboard.school.view_school', {}, {reload : true});
                            $scope.formData = {};
                            $rootScope.loading = false;
                        } else if (status == false) {
                            angular.forEach(scSchool.validationFields, function(value, prop){
                                $scope.validation [prop] = true;
                                console.log($scope.validation);
                            });
                            scNotifier.notifyFailure('Error', 'There was an error creating the school');
                            $rootScope.loading = false;
                        } else {
                            console.log(status);
                            $rootScope.loading = false;

                            scNotifier.notifyFailure('Error', 'Please check the fields again');
                            $rootScope.loading = false;
                        }
                    });
                }

            };



            $rootScope.deleteSchoolModal = function (size, id, element) {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/deleteModal.html',
                    controller: ModalInstanceCtrl,
                    size: size,
                    resolve: {
                        deleteSchoolObject: function () {
                            return id;
                        },
                        clickedElement: function(){
                            return $(element.target).parents('tr');
                        }
                    }
                });

            };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

            var ModalInstanceCtrl = function ($scope, $modalInstance, deleteSchoolObject, clickedElement) {

                $scope.deleteObject = scSchool.schoolLookup[deleteSchoolObject];
                $scope.deleteObject.objectType = 'School';

                $scope.ok = function () {
                    $modalInstance.close('close');
//            $modalInstance.close($scope.selected.item);
//            $log.info('Modal ok\'ed at: ' + new Date());
                    scSchool.deleteSchool(deleteSchoolObject).then(
                        function(status){
                            if (status) {
                                scNotifier.notifySuccess('Success', 'School deleted successfully');
                                $state.go('dashboard.school.view_school', {} , {reload : true});
                            }else{
                                scNotifier.notifyFailure('Error', 'There was an error deleting the school');
                            }
                        },
                        function(){
                            scNotifier.notifyFailure('Error', 'There was an error deleting the school');
                        })
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
//            $log.info('Modal cancelled at: ' + new Date());
                };
            };

            $scope.searchModal = function () {

                $modal.open({
                    templateUrl: 'partials/miscellaneous/searchSchoolModal.html',
                    controller: searchModalInstanceCtrl,
                    size: 'lg'
                });

            };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

            var searchModalInstanceCtrl = function ($scope, $modalInstance) {

                $scope.schoolSortOrder = 'name';

                if ($scope.adminRight == 5){
                    $scope.schools = scSchool.schools;
                    $scope.schoolsSearch = scSchool.schools;
                    $scope.totalItems =  scSchool.schools.length;
                }else if ($scope.adminRight  == 4){
                    $scope.schools = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch =  scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems =   scRegion.regionLookup[$scope.currentUser.level_id].totalSchools;
                }else if ($scope.adminRight == 3){
                    $scope.schools = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch =  scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems =   scDistrict.districtLookup[$scope.currentUser.level_id].totalSchools;
                }else if ($scope.adminRight == 2){
                    $scope.schools = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch =  scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.totalItems =   scCircuit.circuitLookup[$scope.currentUser.level_id].totalSchools;
                }else{
                    $scope.schools = [
                        $scope.schoolLookup[$scope.currentUser.level_id]
                    ];
                }

                $scope.districtLookup = scDistrict.districtLookup;

                $scope.circuitLookup = scCircuit.circuitLookup;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };

            };



        }]);