/**
 * Created by Kaygee on 18/05/2015.
 */
//SRC - Enrollment Controller
schoolMonitor.controller('scViewSchoolEnrollmentController',
    ['$scope', '$stateParams', 'scSchool', 'School', 'scNotifier','$timeout', '$rootScope',
        function ($scope, $stateParams, scSchool, School, scNotifier, $timeout, $rootScope) {

            $scope.page = 'Enrollment';
            $scope.past_verb = 'Enrolled';
            $scope.comment_variable = 'cs_students_enrolment_comments';
            $scope.deleteSectionNormal = 'normal_enrolment';
            $scope.deleteSectionSpecial = 'special_enrolment';
            $scope.model = {
                disable_reload : true //this allow for deleting multiple before refreshing automatically
            };


            var dataloader;
            function initEnrollmentVars() {
                dataloader = 0; //This variable prevents the data prepping from happening until both are loaded
                $scope.enrollmentData = [];
                $scope.enrollmentData.location = {};/*an object whose keys are concatenation of the year,week,term and dc to track location of submitting data*/
                $scope.enrollmentData.totals = {};/*an object whose keys are concatenation of the year,week,term and dc to track totals of data*/
                $scope.schoolNumberOfDaysInSession = {};
                $scope.classHolder = []; //keeps track of the classes
                $scope.previousIndex = 0;
            }

            function prepEnrollmentData() {
                for (var index = 0; index < scSchool.loadedEnrollmentInfoFromServer.length; index ++){
                    var item = scSchool.loadedEnrollmentInfoFromServer[index];

                    if(item.data_collector_type === 'circuit_supervisor'){
                        continue;
                    }


                    var concatenate = item.year + item.term + item.week_number + item.data_collector_type;
                    if ($scope.enrollmentData.totals[concatenate]) {
                        $scope.enrollmentData.totals[concatenate].normal += parseInt(item.total_enrolment);
                        $scope.enrollmentData.totals[concatenate].boys += parseInt(item.num_males);
                        $scope.enrollmentData.totals[concatenate].girls += parseInt(item.num_females);
                    } else {
                        $scope.enrollmentData.totals[concatenate] = {
                            normal: parseInt(item.total_enrolment),
                            special: 0,
                            boys : parseInt(item.num_males),
                            special_boys : 0,
                            girls : parseInt(item.num_females),
                            special_girls : 0,
                            week_number :  item.week_number,
                            term : item.term,
                            year : item.year
                        }
                    }
                    var tempObject = {
                        index: index, //This is used to know which index was clicked so we pass it into the chart function
                        selected: false, //This is used to know which index was clicked so we highlight that row
                        class: item.level,
                        boys: item.num_males,
                        girls: item.num_females,
                        streams: item.num_of_streams,
                        total: parseInt(item.num_males) + parseInt(item.num_females),
                        term: item.term,
                        week: item.week_number,
                        year: item.year,
                        special_boys: 0,
                        special_girls: 0,
                        data_collector_type: item.data_collector_type,
                        date_created: moment(item.created_at).utc().valueOf(),
                        normal_id: item.id
                    };
                    for (var subIndex = 0; subIndex < scSchool.loadedSpecialEnrollmentInfoFromServer.length; subIndex ++) {
                        var special_item = scSchool.loadedSpecialEnrollmentInfoFromServer[subIndex];
                        if (special_item.level == item.level &&
                            special_item.term == item.term &&
                            special_item.week_number == item.week_number &&
                            special_item.year == item.year &&
                            special_item.data_collector_type == item.data_collector_type) {
                            tempObject.special_boys = special_item.num_males;
                            tempObject.special_girls = special_item.num_females;
                            tempObject.special_id = special_item.id;
                            $scope.enrollmentData.totals[concatenate].special += parseInt(special_item.total_enrolment);
                            $scope.enrollmentData.totals[concatenate].special_boys += parseInt(special_item.num_males);
                            $scope.enrollmentData.totals[concatenate].special_girls += parseInt(special_item.num_females);
                        }
                    }

                    if ($scope.classHolder.indexOf(item.level) < 0) {
                        $scope.classHolder.push(item.level);
                    }

                    $scope.enrollmentData.push(tempObject);

                    //Assign the total number of days for the year, term, week and dc type
                    $scope.schoolNumberOfDaysInSession[concatenate] = item.school_in_session || item.number_of_week_days || '-';
                    $scope.enrollmentData.location[concatenate] = {
                        venue: null,
                        longitude: item.long,
                        latitude: item.lat
                    };
                    $scope.filter_term = item.term;
                    $scope.filter_week = item.week_number;
                    $scope.filter_year = item.year;
                    $scope.filter_data_collector_type = item.data_collector_type;
                }
            }



            initEnrollmentVars();

            function run() {
                initEnrollmentVars();
                prepEnrollmentData();
                $scope.loadingData = false;
                /*the variable to hide the gif is here because its is called here regardless of the start point of loadin data*/
                $scope.rowSelected = false;
            }

            //Fired from the below when data is deleted successfully
            $scope.$on('dataLoaded', function(event, data){
                //this should be greater than 1. that's when both special and normal have loaded
                if (data.loader > 3) {
                    run();
                }
            });

            if (scSchool.loadedEnrollmentInfoFromServer.length && scSchool.loadedAttendanceInfoFromServer) {
                $timeout(function(){
                    $scope.loadingEverything = true;
                });
                run();
            }

            $scope.loadingEverything = false;//ths checks and hides the row that allows loading data

            $scope.fetchWeeklyData = function loadSchoolDataFromServer(year, term, week) {
                $timeout(function () {
                    $scope.loadingData = true; //this variable checks to display the loading gif whiles loading data
                });
                var dataParams = {school_id: $stateParams.id};
                dataloader = 0; // reset the variable that checks that both normal and special are loaded
                if (week || term || year) {
                    angular.extend(dataParams, {
                        data_collector_type: "head_teacher",
                        week_number: week,
                        term: term,
                        year: year
                    });
                }else {
                    $scope.loadingEverything = true; //ths checks and hides the row that allows loading data
                }


                /*if not, load it from server and keep track of it in the variable */
                School.allSpecialEnrollmentData(dataParams).$promise
                    .then(function (data) {
                        scSchool.loadedSpecialEnrollmentInfoFromServer = data[0].special_school_enrolment;
                        $scope.$emit('dataLoaded', {loader: ++dataloader});
                    });

                /*if not load it from server and keep track of it in the variable */
                School.allEnrollmentData(dataParams).$promise
                    .then(function (data) {
                        scSchool.loadedEnrollmentInfoFromServer = data[0].school_enrolment;
                        $scope.$emit('dataLoaded', {loader: ++dataloader});
                    });
                /*Load special attendance data also and keep it in the service*/
                School.allSpecialAttendanceData(dataParams).$promise
                    .then(function (data) {
                        scSchool.loadedSpecialAttendanceInfoFromServer = data[0].special_students_attendance;
                        $scope.$emit('dataLoaded', {loader: ++dataloader});
                    });
                /* Load attendance data also and keep it in the service */
                School.allAttendanceData(dataParams).$promise
                    .then(function (data) {
                        scSchool.loadedAttendanceInfoFromServer = data[0].school_attendance;
                        $scope.$emit('dataLoaded', {loader: ++dataloader});
                    });
            };

            $scope.deleteData = function (dataCategory, line_id) {
                $scope.loadingData = true;
                scSchool.deleteSubmittedData(dataCategory, line_id)
                    .then(function (status) {
                        var concatenater = $scope.filter_year + $scope.filter_term+ $scope.filter_week + $scope.filter_data_collector_type;

                        $scope.rowSelected = false; //this clears the highlight on the row and hides the chart side
                        if (dataCategory == 'special_enrolment') {//this check hides the special data if that was deleted
                            $scope.deletable.special_boys = '-'; //and updates the special total
                            $scope.deletable.special_girls = '-';
                            delete $scope.deletable.special_id;
                            $scope.enrollmentData.totals[concatenater].special -= ($scope.deletable.special_boys + $scope.deletable.special_girls)
                        }else{ //the reverse for the normal
                            $scope.deletable.isDeleted = true;
                            $scope.enrollmentData.totals[concatenater].normal -=  ($scope.deletable.boys + $scope.deletable.girls)
                        }
                        if (status && !$scope.model.disable_reload) { //if the data deleted successfully and the user has unchecked the checkbox
                            $scope.fetchWeeklyData($scope.filter_year ,$scope.filter_term, $scope.filter_week);
                        }else{
                            $scope.loadingData = false;
                        }
                    })
            };

            // this variable is used to indicate which type of charge is visible
            $scope.pie_or_line = 'Pie';

            // This variable tells if a row has been clicked so that it
            // shows the chart column
            $scope.rowSelected = false;

            $scope.showLineChart = function(){

                $scope.pie_or_line = 'Line';


                /*to get the termly enrollment report,
                 1. we load all that class that was clicked,
                 2. then we create a giant object that contains the year + term as keys. inside this new object will be the loaded classes
                 object with their data sorted by year and term,
                 3. sorted according to week in descending order so the latest will be the first index and that indicates how many
                 students have been enrolled as at the last week.
                 4. Finally when the data in the loaded class is exhausted, pop out all the latest enrollments sorted into the objects,
                 5. assign a key to be used as a chart label, and
                 6. sort according to date created and send them to amCharts.*/

                //initialize an array to hold the clicked class data
                $scope.enrollmentClickedClassData = [];

                //loop through all the enrollment data
                angular.forEach($scope.enrollmentData, function(enrolInfo){
//                    previous index is a variable used to know the row that was clicked's index
                    if ($scope.previousIndex !== undefined) {
                        //load only the class that was clicked's rows
                        if ($scope.enrollmentData[ $scope.previousIndex ].class == enrolInfo.class) {
                            $scope.enrollmentClickedClassData.push(enrolInfo);
                        }}});

                //This is the giant object that has the enrollment data broken into as year_term as keys
                $scope.enrollmentYearTermObject = {};

                //loop through the loaded classes' data
                angular.forEach($scope.enrollmentClickedClassData, function(item, index){
                    //use the year and term to create a key for the object. eg. 2014/2015_first_term

                    //if it exists, update the array,
                    if ($scope.enrollmentYearTermObject[ item.year+'_'+item.term ]) {
                        $scope.enrollmentYearTermObject[ item.year+'_'+item.term ].push(item);

                        //if not, create it and update the array with the item created
                    }else{
                        $scope.enrollmentYearTermObject[ item.year+'_'+item.term ] = [];
                        $scope.enrollmentYearTermObject[ item.year+'_'+item.term ].push(item);
                    }
                });

                //This is the ultimate array to be sent to the line chart
                $scope.enrollmentLineProcessedData = [];

                //loop through the Year_Term object and sort their arrays by week in descending order
                angular.forEach($scope.enrollmentYearTermObject, function(array_as_value, key){
                    array_as_value.sort(function(a, b){
                        return b.week - a.week
                    });
                    array_as_value[0].chartLabel = key;
                    $scope.enrollmentLineProcessedData.push( array_as_value[0]);
                });

                //sort the processed data by created date so we can be sure in is arranged well
                $scope.enrollmentClickedClassData.sort(function(a, b){
                    return a.date_created - b.date_created
                });

                $scope.changeLineChart($scope.enrollmentLineProcessedData, $scope.enrollmentData[ $scope.previousIndex ].class + ' Termly Enrollment Trend'  ,'enrollmentLineChart');
            };

            $scope.showPieChart = function(){
                $scope.pie_or_line = 'Pie';
                $scope.changePieChart($scope.tempArray, 'Enrollment Chart', 'enrollmentPieChart');
            };

            $scope.showBarChart = function(){
                $scope.pie_or_line = 'Bar';
                $scope.changeBarChart($scope.tempArray, 'Enrollment Chart', 'enrollmentBarChart');
            };


            $scope.renderChart = function(index){
                //This is used to show the chart area until it is clicked
                $scope.rowSelected = true;

                //This is a temporary variable that formats the selected row to a
                // format that the pie chart can render
                $scope.tempArray= [
                    {"field" : "Boys",
                        "value" : $scope.enrollmentData[index].boys
                    },
                    {"field" : "Girls",
                        "value" : $scope.enrollmentData[index].girls
                    }
                ];

                //The variable to hold the data that will be deleted
                $scope.deletable = $scope.enrollmentData[index];

                //Check if previous index has been initialised and change the scopes selected row
                if ($scope.previousIndex) {
                    $scope.enrollmentData[$scope.previousIndex].selected = false;

                }

                //Keep a reference to the previous index so we can change its selected state to false
                $scope.previousIndex = index;

                $scope.enrollmentData[index].selected = true;


                //Dynamically change the chart type that is showing
                var chartType = 'show'+$scope.pie_or_line+'Chart';

                //Show the chart type chosen via the scope object
                $scope[chartType]();
            };


            $scope.trending = false;


            $scope.showTrends = function () {
                $scope.trending = !$scope.trending;


                for (var i = 0; i < $scope.schoolWeeksTrendHolder.length; i++) {
                    var weekObj = $scope.schoolWeeksTrendHolder[i];

                    for (var j = 0; j < $scope.enrollmentData.length; j++) {

                        var enrolItem = $scope.enrollmentData[j];

                        if (weekObj.week === enrolItem.week && weekObj.termObj.value === enrolItem.term && weekObj.year === enrolItem.year) {

                            weekObj[enrolItem.class] = {
                                boys  : enrolItem.boys,
                                girls : enrolItem.girls
                            }
                        }
                    }
                }
            };


            /*this function send the totals calculated to the server to update the weekly table*/
            $scope.updateSchoolWeeklyTotal = function () {
                var holder = [];

                $scope.updatingTotals = true;

                angular.forEach($scope.enrollmentData.totals, function (value, key) {
                    holder.push({
                        normal_enrolment_total_boys : value.boys,
                        normal_enrolment_total_girls : value.girls,
                        normal_enrolment_total_students : value.normal,
                        // normal_attendance_total_boys : value.boys,
                        // normal_attendance_total_girls : value.boys,
                        // normal_attendance_total_students : value.boys,
                        special_enrolment_total_boys : value.special_boys,
                        special_enrolment_total_girls : value.special_girls,
                        special_enrolment_total_students : value.special,
                        // special_attendance_total_boys : value.boys,
                        // special_attendance_total_girls : value.boys,
                        // special_attendance_total_students : value.boys,
                        year : value.year,
                        term : value.term,
                        week_number : value.week_number,
                        country_id : $scope.selected_school.country_id,
                        region_id : $scope.selected_school.region_id,
                        district_id : $scope.selected_school.district_id,
                        circuit_id : $scope.selected_school.circuit_id,
                        school_id : $scope.selected_school.id
                    })
                });


                scSchool.updateWeeklyTableForEnrollment(holder)
                    .success(function (successData) {
                        if (successData.status == 200) {
                            scNotifier.notifySuccess("Recalculation", "Enrolment totals have been updated.")
                        }
                    })
                    .error(function (errData) {
                        scNotifier.notifyFailure("Recalculation", "Enrolment totals failed to update")
                    })
                    .finally(function () {
                        $scope.updatingTotals = false;
                    });


            };




        }
    ]);
