/**
 * Created by Kaygee on 21/05/2015.
 */
//SRC - School Community Involvement
schoolMonitor.controller('scViewSchoolCommunityInvolvementController',
    ['$scope', '$stateParams', 'School', '$modal', '$rootScope',  'allSchoolCommunityInvolvementData','allSchoolMeetingsHeldData',
        'allSchoolGeneralSituationData', 'scGlobalService',

        function ($scope, $stateParams, School, $modal, $rootScope, allSchoolCommunityInvolvementData, allSchoolMeetingsHeldData,
                  allSchoolGeneralSituationData, scGlobalService) {

            $scope.schoolTerms = [];
            $scope.schoolYears = [];
            $scope.schoolWeeks = [];

            function fetchCommunityData(allSchoolCommunityInvolvementData) {
                $scope.communityInvolvementItems = [];
                $scope.communityInvolvementComment = {};
                $scope.communityInvolvementUpdated = {};
                $scope.communityLocation = {};

                angular.forEach(allSchoolCommunityInvolvementData, function (communityObject, index) {

                    var communityHolder = [];
//                var format_term = $scope.academicTermDisplay(communityObject.term);
                    communityHolder.push({
                        name: "PTA Involvement",
                        term: communityObject.term,
                        status: $scope.formatGoodPoorFair[communityObject.pta_involvement],
                        year: communityObject.year,
                        data_collector_type: communityObject.data_collector_type
                    });
                    communityHolder.push({
                        name: "Parents Notified of Student Progress",
                        term: communityObject.term,
                        status: $scope.formatGoodPoorFair[communityObject.parents_notified_of_student_progress],
                        year: communityObject.year,
                        data_collector_type: communityObject.data_collector_type
                    });
                    communityHolder.push({
                        name: "Community Sensitization on School Attendance",
                        term: communityObject.term,
                        status: $scope.formatGoodPoorFair[communityObject.community_sensitization_for_school_attendance],
                        year: communityObject.year,
                        data_collector_type: communityObject.data_collector_type
                    });
                    communityHolder.push({
                        name: "School Management Committees",
                        term: communityObject.term,
                        status: $scope.formatGoodPoorFair[communityObject.school_management_committees],
                        year: communityObject.year,
                        data_collector_type: communityObject.data_collector_type
                    });


                    var concatenate = communityObject.year + communityObject.term + communityObject.data_collector_type;

                    $scope.communityLocation[concatenate] = {
                        venue: null,
                        longitude: communityObject.long,
                        latitude: communityObject.lat,
                        id: communityObject.id
                    };


                    $scope.communityInvolvementComment[concatenate] = {
                        ht_comment: communityObject.comment,
                        cs_comments: communityObject.cs_comments
                    };

                    $scope.communityInvolvementUpdated[concatenate] = communityObject.updated_at;

                    $scope.communityInvolvementItems = $scope.communityInvolvementItems.concat(communityHolder);


                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[communityObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = communityObject.year;

                    //save the current week in the loop to a variable
//                var currentWeek = sanitationObject.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    //These assign the scope variable to the latest term, week and year and data collector
                    $scope.filter_term = currentTerm.value;

                    $scope.filter_year = currentYear.toString();

                    $scope.filter_data_collector_type = communityObject.data_collector_type;


                });
            }


            function fetchMeetingData(allSchoolMeetingsHeldData) {
                $scope.schoolMeetings = [];
                $scope.schoolMeetingsComment = {};
                $scope.schoolMeetingsUpdated = {};
                $scope.schoolMeetingsCSVisits = {};
                $scope.schoolMeetingsInsets = {};
                $scope.meetingLocation = {};


                angular.forEach(allSchoolMeetingsHeldData[0].school_meetings, function (meetingObject, index) {

                    var meetingsHolder = [];

//                number_of_circuit_supervisor_visit
//                number_of_inset

                    meetingsHolder.push({
                        name: "Staff Meeting",
                        frequency: meetingObject.number_of_staff_meeting,
                        males_present: meetingObject.num_males_present_at_staff_meeting,
                        females_present: meetingObject.num_females_present_at_staff_meeting,
                        week: meetingObject.week_number,
                        term: meetingObject.term,
                        year: meetingObject.year,
                        data_collector_type: meetingObject.data_collector_type,
                        comment: meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "SPAM",
                        frequency: meetingObject.number_of_spam_meeting,
                        males_present: meetingObject.num_males_present_at_spam_meeting,
                        females_present: meetingObject.num_females_present_at_spam_meeting,
                        week: meetingObject.week_number,
                        term: meetingObject.term,
                        year: meetingObject.year,
                        data_collector_type: meetingObject.data_collector_type,
                        comment: meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "PTA",
                        frequency: meetingObject.number_of_pta_meeting,
                        males_present: meetingObject.num_males_present_at_pta_meeting,
                        females_present: meetingObject.num_females_present_at_pta_meeting,
                        week: meetingObject.week_number,
                        term: meetingObject.term,
                        year: meetingObject.year,
                        data_collector_type: meetingObject.data_collector_type,
                        comment: meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "SMC",
                        frequency: meetingObject.number_of_smc_meeting,
                        males_present: meetingObject.num_males_present_at_smc_meeting,
                        females_present: meetingObject.num_females_present_at_smc_meeting,
                        week: meetingObject.week_number,
                        term: meetingObject.term,
                        year: meetingObject.year,
                        data_collector_type: meetingObject.data_collector_type,
                        comment: meetingObject.comment
                    });

                    $scope.schoolMeetings = $scope.schoolMeetings.concat(meetingsHolder);

                    var concatenate = meetingObject.year + meetingObject.term + meetingObject.data_collector_type;

                    $scope.meetingLocation[concatenate] = {
                        venue: null,
                        longitude: meetingObject.long,
                        latitude: meetingObject.lat,
                        id: meetingObject.id
                    };


                    $scope.schoolMeetingsComment[concatenate] = {
                        ht_comment: meetingObject.comment,
                        cs_comments: scGlobalService.appendIfNotNull(meetingObject.cs_comments)
                    };

                    $scope.schoolMeetingsUpdated[concatenate] = meetingObject.updated_at;

                    $scope.schoolMeetingsCSVisits[concatenate] = meetingObject.number_of_circuit_supervisor_visit;
                    $scope.schoolMeetingsInsets[concatenate] = meetingObject.number_of_inset;

                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[meetingObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = meetingObject.year;


                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }

                    //These assign the scope variable to the latest term, week and year
                    $scope.filter_term = currentTerm.value;

                    $scope.filter_year = currentYear.toString();

                    $scope.filter_data_collector_type = meetingObject.data_collector_type;

                });
            }


            function fetchSchoolGeneralSituationData(allSchoolGeneralSituationData) {
                $scope.generalIssueItems = [];
                /*This is used to track what to show on the html*/
                $scope.generalIssueItemsComment = {};
                $scope.generalIssueItemsUpdated = {};

                $scope.issueLocation = {};

                if (allSchoolGeneralSituationData.length) {
                    $scope.generalIssueItemsObject = {
                        'Inclusive': [],
                        'Effective Teaching and Learning': [],
                        'Healthy': [],
                        'Safe and Protective': [],
                        'Friendliness to Boys and Girls': [],
                        'Community Involvement': []
                    };

                    $scope.generalIssueItemsObject['Effective Teaching and Learning'].push(allSchoolGeneralSituationData[0]);
                    $scope.generalIssueItemsObject['Healthy'].push(allSchoolGeneralSituationData[1]);
                    $scope.generalIssueItemsObject['Effective Teaching and Learning'].push(allSchoolGeneralSituationData[2]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[3]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[4]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[5]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[6]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[7]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[8]);
                    $scope.generalIssueItemsObject['Inclusive'].push(allSchoolGeneralSituationData[9]);
                    $scope.generalIssueItemsObject['Inclusive'].push(allSchoolGeneralSituationData[10]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[11]);
                    $scope.generalIssueItemsObject['Healthy'].push(allSchoolGeneralSituationData[12]);
                    $scope.generalIssueItemsObject['Safe and Protective'].push(allSchoolGeneralSituationData[13]);
                }


                angular.forEach(allSchoolGeneralSituationData, function (issueObject, index) {

                    /*This is used to track what to show on the html*/
                    $scope.generalIssueItems.push(issueObject);


                    /*save the current term in the loop to a variable*/
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[issueObject.term];

                    //save the current year in the loop to a variable
                    var currentYear = issueObject.year;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                        $scope.schoolTerms.push(currentTerm);
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.schoolYears.indexOf(currentYear) < 0) {
                        $scope.schoolYears.push(currentYear);
                    }


                    var concatenate = issueObject.year + issueObject.term + issueObject.data_collector_type;


                    $scope.generalIssueItemsUpdated[concatenate] = issueObject.updated_at;

                    $scope.issueLocation[concatenate] = {
                        venue: null,
                        longitude: issueObject.long,
                        latitude: issueObject.lat,
                        id: issueObject.id
                    };

                    $scope.generalIssueItemsComment[concatenate] = {
                        cs_comments: scGlobalService.appendIfNotNull(allSchoolGeneralSituationData[0].cs_comments)
                    };

                    //These assign the scope variable to the latest term, week and year
                    $scope.filter_term = currentTerm.value;

                    $scope.filter_year = currentYear.toString();

                    $scope.filter_data_collector_type = issueObject.data_collector_type;


                });
            }




            fetchCommunityData(allSchoolCommunityInvolvementData);
            $scope.fetchCommunityData = function(){
                $rootScope.loading = true;
                School.allSchoolCommunityInvolvement({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolCommunityInvolvementData) {
                        fetchCommunityData(allSchoolCommunityInvolvementData);
                        $rootScope.loading = false;
                    })
            };


            fetchMeetingData(allSchoolMeetingsHeldData);
            $scope.fetchMeetingData = function(){
                $rootScope.loading = true;
                School.allSchoolMeetingsHeld({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolMeetingsHeldData) {
                        fetchMeetingData(allSchoolMeetingsHeldData);
                        $rootScope.loading = false;
                    })
            };


            fetchSchoolGeneralSituationData(allSchoolGeneralSituationData);
            $scope.fetchSchoolGeneralSituationData = function(){
                $rootScope.loading = true;
                School.allSchoolGeneralSituation({school_id : $stateParams.id}).$promise
                    .then(function (allSchoolGeneralSituationData) {
                        fetchSchoolGeneralSituationData(allSchoolGeneralSituationData);
                        $rootScope.loading = false;
                    })
            };

        }
    ]);

