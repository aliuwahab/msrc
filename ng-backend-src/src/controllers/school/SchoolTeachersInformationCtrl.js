/**
 * Created by Kaygee on 18/05/2015.
 */
//SRC - School Teacher Information
schoolMonitor.controller('scViewSchoolTeacherInformationController',
    ['$rootScope', '$scope', '$stateParams', '$modal', 'School', '$timeout','scSchool','scNotifier',
        function ($rootScope, $scope, $stateParams, $modal, School, $timeout, scSchool, scNotifier) {

            var dataloader = 0, teacherIndex = 0;
            var teacherAttendanceTracker = [], teacherAttendanceTermlyTracker= [], teacherAttendanceTermlyWeekTracker =[], teacherAttendanceTotals = {}, teacherAttendanceTermlyTotals = {};


            $scope.show_with_comments = false;



            var teachersInSchool;
            var isShowingHistory = false; //this variable makes the histories show on and off

            $scope.transferredTeachersInSchool = []; //this array holds transferred teachers.
            // it is updated when the loaded teachers array doesn't have an id from the loaded data

            function updateTeachersInSchoolWithTransferredOnes(reset) {
                if (!reset) {
                    teachersInSchool = angular.copy($scope.teachers_in_school);
                }else{
                    angular.forEach($scope.transferredTeachersInSchool, function (teacher, index) {
                        teacher.isTransferred = true;
                        teachersInSchool.push(teacher);
                    });
                }
                $scope.prepTeachers();
                isShowingHistory = !isShowingHistory;
            }

            $scope.showTeacherHistory = function () {
                console.log('isShowingHistory :', isShowingHistory);
                /*if isShowing is false, it will update teachers with transferred ones, else it will
                * reset to default teachers presently in school*/
                updateTeachersInSchoolWithTransferredOnes(!isShowingHistory);
            };

            $scope.lookupTeacherInfoData = {};

            $scope.prepTeachers = function(){
                $scope.loadingData = true;

                $scope.teacherInfoData =[];

                $scope.schoolNumberOfDaysInSession = {};


                //These variables only keep track of all weeks, term and years in the punctuality and class management objects
                var allWeeks =[],  allTerms = [], allYears = [], allDataCollectors = [];
                teacherAttendanceTracker = [];
                teacherAttendanceTotals = {};
                teacherAttendanceTermlyTracker = [];
                teacherAttendanceTermlyWeekTracker = [];
                teacherAttendanceTermlyTotals = {};

                for (var index = 0; index < teachersInSchool.length; index++) {
                    var item = teachersInSchool[index], temp_lev_taught = '-';

                    if (item.class_taught == null || item.class_taught == 'null') item.class_taught = '';
                    if (angular.isDefined(item.level_taught)) temp_lev_taught = item.level_taught.toUpperCase();
                    if (item.class_subject_taught == null || item.class_subject_taught == 'null') item.class_subject_taught = '';
                    var tempObject = {
                        id: item.id,
                        staff_number: item.staff_number,
                        first_name: item.first_name,
                        last_name: item.last_name,
                        gender: item.gender.toUpperCase(),
                        level_taught: $scope.removeTrailingComma(temp_lev_taught),
                        subject: $scope.removeTrailingComma($.trim(item.class_subject_taught + item.class_taught)),
                        category: item.category,
                        rank: item.rank,
                        school_id: item.school_id,
                        circuit_id: item.circuit_id,
                        district_id: item.district_id,
                        highest_academic_qualification: item.highest_academic_qualification,
                        highest_professional_qualification: item.highest_professional_qualification,
                        years_in_the_school: item.years_in_the_school,
                        teacher_status: item.teacher_status,
                        showStatus : true,
                        punctualityHolder: [],
                        classManagementHolder: [],
                        unitsCoveredHolder: []
                    };

                    var longitude = 0;
                    var latitude = 0;

                    for (var tp = 0; tp < scSchool.allSchoolTeachersPunctuality_LessonPlan.length; tp ++) {

                        var teacherPunctualityObject = scSchool.allSchoolTeachersPunctuality_LessonPlan[tp];

                        if (item.id === teacherPunctualityObject.teacher_id) {
                            teacherPunctualityObject.week_number = parseInt(teacherPunctualityObject.week_number);//parse it to int to enforce consistency in searching and filtering
                            tempObject.punctualityHolder.push({
                                id: teacherPunctualityObject.id,
                                lesson_plan: teacherPunctualityObject.lesson_plans || 'Not Prepared',
                                days_present: teacherPunctualityObject.days_present,
                                days_punctual: teacherPunctualityObject.days_punctual,
                                days_absent_with_permission: teacherPunctualityObject.days_absent_with_permission,
                                days_absent_without_permission: teacherPunctualityObject.days_absent_without_permission,
                                longitude: teacherPunctualityObject.long,
                                latitude: teacherPunctualityObject.lat,
                                year: teacherPunctualityObject.year,
                                term: teacherPunctualityObject.term,
                                week: teacherPunctualityObject.week_number,
                                data_collector_type: teacherPunctualityObject.data_collector_type,
                                num_of_exercises_given: teacherPunctualityObject.num_of_exercises_given,
                                num_of_exercises_marked: teacherPunctualityObject.num_of_exercises_marked,
                                comment : teacherPunctualityObject.comment,
                                cs_comments : teacherPunctualityObject.cs_comments,
                                days_in_session : teacherPunctualityObject.days_in_session
                            });

                            /*keep track that the punctuality obj has a comment*/
                            if ((teacherPunctualityObject.cs_comments && teacherPunctualityObject.cs_comments.length) || (teacherPunctualityObject.comment && teacherPunctualityObject.comment.length)) {
                                tempObject.has_punctuality_comment = true;
                            }

                            /*start the total teacher attendance here*/

                            /*the concat variable includes the teacher ID so we only add the teachers attendance once, to prevent duplicates for that week, term and year*/
                            var concatWeekly = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number +'-'+ teacherPunctualityObject.teacher_id;
                            /*The concat period is only the period WITHOUT the teachers ID*/
                            var concatPeriod = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number;

                            /*Begin Weekly Aggregations*/
                            if (teacherAttendanceTracker.indexOf(concatWeekly) < 0) {

                                if (angular.isDefined(teacherAttendanceTotals[concatPeriod])) {
                                    teacherAttendanceTotals[concatPeriod].totalAttendance += teacherPunctualityObject.days_present;
                                    teacherAttendanceTotals[concatPeriod].actualTeacherTotal++;

                                    teacherAttendanceTotals[concatPeriod].averageAttendance =
                                        (teacherAttendanceTotals[concatPeriod].totalAttendance / (teacherPunctualityObject.days_in_session * teacherAttendanceTotals[concatPeriod].expectedTeacherTotal)) * 100;
                                    /*
                                    * actualAtt
                                    * ----------  x 100
                                    * expectedAtt (total days in session x total num of teacher)*/

                                }else{
                                    teacherAttendanceTotals[concatPeriod] = {
                                        totalAttendance :  teacherPunctualityObject.days_present,
                                        actualTeacherTotal : 1,
                                        expectedTeacherTotal : teachersInSchool.length,
                                        days_in_session : teacherPunctualityObject.days_in_session,
                                        year: teacherPunctualityObject.year,
                                        term: teacherPunctualityObject.term,
                                        week_number: teacherPunctualityObject.week_number,
                                        averageAttendance : (teacherPunctualityObject.days_present/(teacherPunctualityObject.days_in_session * teachersInSchool.length)) * 100
                                    };

                                    teacherAttendanceTracker.push(concatWeekly);
                                }

                            }

                            /*Begin Termly Aggregation*/

                            /*The concat period is only the period, year and term, WITHOUT the teachers ID*/
                            var concatTermPeriod = teacherPunctualityObject.year + teacherPunctualityObject.term;


                            if (teacherAttendanceTermlyTracker.indexOf(concatWeekly) < 0) {
                                if (angular.isDefined(teacherAttendanceTermlyTotals[concatTermPeriod])) {
                                    teacherAttendanceTermlyTotals[concatTermPeriod].totalAttendance += teacherPunctualityObject.days_present;

                                    // teacherAttendanceTotals[concatTermPeriod].actualTeacherTotal++;

                                    /*use this to track the number of days school was in session*/
                                    if (teacherAttendanceTermlyWeekTracker.indexOf(concatPeriod) < 0) {
                                        teacherAttendanceTermlyTotals[concatTermPeriod].totalDaysInSession += teacherPunctualityObject.days_in_session;
                                        teacherAttendanceTermlyWeekTracker.push(concatPeriod);
                                    }

                                    teacherAttendanceTermlyTotals[concatTermPeriod].averageAttendance =
                                        (teacherAttendanceTermlyTotals[concatTermPeriod].totalAttendance / (teacherAttendanceTermlyTotals[concatTermPeriod].totalDaysInSession * teacherAttendanceTermlyTotals[concatTermPeriod].expectedTeacherTotal)) * 100;
                                    /*
                                    * actualAtt
                                    * ----------  x 100
                                    * expectedAtt (total days in session x total num of teacher)*/

                                }else{
                                    teacherAttendanceTermlyTotals[concatTermPeriod] = {
                                        totalAttendance :  teacherPunctualityObject.days_present,
                                        // actualTeacherTotal : 1,
                                        expectedTeacherTotal : teachersInSchool.length,
                                        totalDaysInSession : 0,
                                        year: teacherPunctualityObject.year,
                                        term: teacherPunctualityObject.term,
                                        averageAttendance : (teacherPunctualityObject.days_present/(teacherPunctualityObject.days_in_session * teachersInSchool.length)) * 100
                                    };

                                    /*use this to track the number of days school was in session*/
                                    if (teacherAttendanceTermlyWeekTracker.indexOf(concatPeriod) < 0) {
                                        teacherAttendanceTermlyTotals[concatTermPeriod].totalDaysInSession += teacherPunctualityObject.days_in_session;
                                        teacherAttendanceTermlyWeekTracker.push(concatPeriod);
                                    }

                                    teacherAttendanceTermlyTracker.push(concatWeekly);
                                }

                            }


                            allWeeks.push(teacherPunctualityObject.week_number);

                            allTerms.push(teacherPunctualityObject.term);

                            allYears.push(teacherPunctualityObject.year);

                            allDataCollectors.push(teacherPunctualityObject.data_collector_type);

                            $scope.filter_term = teacherPunctualityObject.term;
                            $scope.filter_week = teacherPunctualityObject.week_number;
                            $scope.filter_year = teacherPunctualityObject.year;
                            $scope.filter_data_collector_type = teacherPunctualityObject.data_collector_type;

                            var concatenate = teacherPunctualityObject.year + teacherPunctualityObject.term +
                                teacherPunctualityObject.week_number;

                            $scope.schoolNumberOfDaysInSession[concatenate] = teacherPunctualityObject.days_in_session;

                            tempObject.punctualityHolder.sort(function (a, b) {
                                return a.week - b.week
                            });

                        } else {
                            if ($scope.teachersInSchoolIDHolder.indexOf(teacherPunctualityObject.teacher_id) < 0){
                                if ($scope.lookupTeacherInfoData.hasOwnProperty(teacherPunctualityObject.teacher_id)) {
                                }else{

                                    if (teacherPunctualityObject && teacherPunctualityObject.teacher) {
                                        teacherPunctualityObject.teacher.isTransferred = true;

                                        $scope.transferredTeachersInSchool.push(teacherPunctualityObject.teacher);

                                        $scope.lookupTeacherInfoData[teacherPunctualityObject.teacher_id] = teacherPunctualityObject.teacher;

                                        $scope.lookupTeacherInfoData[teacherPunctualityObject.teacher_id].isTransferred = true;
                                    }
                                }
                            }
                        }
                    }

                    $scope.teacherWeeklyTotals = angular.copy(teacherAttendanceTotals);
                    $scope.teacherAttTrend = angular.copy(teacherAttendanceTermlyTotals);


                    for( var cm = 0; cm < scSchool.allSchoolTeachersClassManagement.length; cm++){
                        var teacherClassManagementObject = scSchool.allSchoolTeachersClassManagement[cm];
                        if (item.id === teacherClassManagementObject.teacher_id) {
                            teacherClassManagementObject.week_number = parseInt(teacherClassManagementObject.week_number);//parse it to int to enforce consistency in searching and filtering

                            tempObject.classManagementHolder.push({
                                id: teacherClassManagementObject.id,
                                class_control: teacherClassManagementObject.class_control,
                                children_behaviour: teacherClassManagementObject.children_behaviour,
                                children_participation: teacherClassManagementObject.children_participation,
                                teacher_discipline: teacherClassManagementObject.teacher_discipline,
                                year: teacherClassManagementObject.year,
                                term: teacherClassManagementObject.term,
                                week: teacherClassManagementObject.week_number,
                                data_collector_type: teacherClassManagementObject.data_collector_type,
                                comment : teacherClassManagementObject.comment,
                                cs_comments : teacherClassManagementObject.cs_comments

                            });

                            /*keep track that the class management obj has a comment*/
                            if ((teacherClassManagementObject.cs_comments && teacherClassManagementObject.cs_comments.length) || teacherClassManagementObject.comment && teacherClassManagementObject.comment.length) {
                                tempObject.has_teacher_class_management_comment = true;
                            }

                            allWeeks.push(teacherClassManagementObject.week_number);

                            allTerms.push(teacherClassManagementObject.term);

                            allYears.push(teacherClassManagementObject.year);

                            allDataCollectors.push(teacherClassManagementObject.data_collector_type);

                            tempObject.classManagementHolder.sort(function (a, b) {
                                return a.week - b.week
                            });
                        }
                    }

                    for ( var uc = 0; uc < scSchool.allSchoolTeachersUnitsCoveredData.length; uc ++){

                        var teacherUnitsCoveredObject = scSchool.allSchoolTeachersUnitsCoveredData[uc];
                        if (item.id === teacherUnitsCoveredObject.teacher_id) {
                            teacherUnitsCoveredObject.week_number = parseInt(teacherUnitsCoveredObject.week_number);//parse it to int to enforce consistency in searching and filtering

                            tempObject.unitsCoveredHolder.push({
                                id: teacherUnitsCoveredObject.id,
                                ghanaian_language_units_covered: teacherUnitsCoveredObject.ghanaian_language_units_covered,
                                english_language_units_covered: teacherUnitsCoveredObject.english_language_units_covered,
                                mathematics_units_covered: teacherUnitsCoveredObject.mathematics_units_covered,
                                year: teacherUnitsCoveredObject.year,
                                term: teacherUnitsCoveredObject.term,
                                week: teacherUnitsCoveredObject.week_number,
                                data_collector_type: teacherUnitsCoveredObject.data_collector_type,
                                comment: teacherUnitsCoveredObject.comment,
                                cs_comments : teacherUnitsCoveredObject.cs_comments
                            });

                            /*keep track that the units covered obj has a comment*/
                            if ((teacherUnitsCoveredObject.cs_comments && teacherUnitsCoveredObject.cs_comments.length) || teacherUnitsCoveredObject.comment && teacherUnitsCoveredObject.comment.length) {
                                tempObject.has_units_covered_comment = true;
                            }

                            allWeeks.push(teacherUnitsCoveredObject.week_number);

                            allTerms.push(teacherUnitsCoveredObject.term);

                            allYears.push(teacherUnitsCoveredObject.year);

                            allDataCollectors.push(teacherUnitsCoveredObject.data_collector_type);

                            tempObject.unitsCoveredHolder.sort(function (a, b) {
                                return a.week - b.week
                            });
                        }
                    }



                    //This index variable helps to know which teacher was clicked on
                    tempObject.index = index;
                    $scope.lookupTeacherInfoData[index] = tempObject; /*this lookup object uses the id to lookup the clicked teacher's info*/

                    $scope.teacherInfoData.push(tempObject);
                    $scope.teacherInfoData.latitude = latitude;
                    $scope.teacherInfoData.longitude = longitude;


                }

                $timeout(function () {
                    $scope.loadingData = false;
                });

            };


            $scope.filteringTeachers = function(param){
                angular.forEach($scope.teacherInfoData, function (teacher, index) {


                    if (teacher.teacher_status !== param){
                        teacher.showStatus = false;
                    }else if (angular.isUndefined(param) || param === '' || param.length < 1){
                        console.log(teacher.teacher_status, param,  param === '', param.length < 1);

                        $scope.teacherInfoData[index].showStatus = true;
                    }
                });
                $timeout(function () {
                    $scope.loadingData = false;
                });

            };

            $scope.$watch('loadingTeachers', function (newVal, oldVal) {
                teachersInSchool = angular.copy($scope.teachers_in_school);

                $scope.prepTeachers();
            });

            if (angular.isDefined($scope.teachers_in_school) && $scope.teachers_in_school.length){
                teachersInSchool = angular.copy($scope.teachers_in_school);

                $scope.prepTeachers();
            }

            $scope.$on('teacherInfoLoaded', function (evt, data) {
                if(data > 2){
                    teachersInSchool = angular.copy($scope.teachers_in_school);

                    $scope.prepTeachers();
                }
                if (data === 999) {
                    $scope.teacherClickedChange();
                }
            });

            $scope.loadingEverything = scSchool.teacherLoadEverything;

            $scope.fetchWeeklyData = function(year, term, week, section_to_reload) {
                $scope.loadingData = true; //this variable checks to display the loading gif whiles loading data
                var dataParams = {school_id: $stateParams.id};
                dataloader = 0; // reset the variable that checks that punctuality, class management and units covered are loaded
                if (week || term || year) {
                    angular.extend(dataParams, {
                        week_number: week,
                        term: term,
                        year: year
                    });
                } else {
                    $scope.loadingEverything = true; //ths checks and hides the row that allows loading all data
                    scSchool.teacherLoadEverything = true;
                }

                /*The section_to_reload is from when the CS adds a comment to the submission, we need to reload that section*/
                if (section_to_reload) {
                    if (section_to_reload === 'punctuality_and_lessons') {
                        School.allSchoolTeachersPunctuality_LessonPlan(dataParams).$promise
                            .then(function (punctuality_lesson_plans) {
                                scSchool.allSchoolTeachersPunctuality_LessonPlan = punctuality_lesson_plans;
                                $scope.prepTeachers();
                                $scope.viewTeacher(teacherIndex);
                            });
                    }
                    if (section_to_reload === 'teachers_class_management') {
                        School.allSchoolTeachersClassManagement(dataParams).$promise
                            .then(function (class_management) {
                                scSchool.allSchoolTeachersClassManagement = class_management;
                                $scope.prepTeachers();
                                $scope.viewTeacher(teacherIndex);

                            });
                    }
                    if (section_to_reload === 'teacher_units_covered') {
                        School.allSchoolTeachersUnitsCovered(dataParams).$promise
                            .then(function (units_covered) {
                                scSchool.allSchoolTeachersUnitsCoveredData = units_covered;
                                $scope.prepTeachers();
                                $scope.viewTeacher(teacherIndex);

                            });
                    }


                }else{
                    /*else is for loading everything*/
                    School.allSchoolTeachersPunctuality_LessonPlan(dataParams).$promise
                        .then(function (punctuality_lesson_plans) {
                            scSchool.allSchoolTeachersPunctuality_LessonPlan = punctuality_lesson_plans;
                            $scope.$broadcast('teacherInfoLoaded', ++dataloader);
                        });
                    School.allSchoolTeachersClassManagement(dataParams).$promise
                        .then(function (class_management) {
                            scSchool.allSchoolTeachersClassManagement = class_management;
                            $scope.$broadcast('teacherInfoLoaded', ++dataloader);
                        });
                    School.allSchoolTeachersUnitsCovered(dataParams).$promise
                        .then(function (units_covered) {
                            scSchool.allSchoolTeachersUnitsCoveredData = units_covered;
                            $scope.$broadcast('teacherInfoLoaded', ++dataloader);
                        });
                }

            };

            $scope.viewTeacher= function(index){
                // keep track of the index for they comment reloading
                teacherIndex = index;
                $scope.clickedTeacher = $scope.lookupTeacherInfoData[index];
                $scope.clickedTeacher.school = $scope.selected_school;
                $scope.teacherClicked = true;
                $scope.rowSelected = false;
            };

            $scope.teacherClickedChange = function () {
                $scope.teacherClicked = !$scope.teacherClicked;
            };

            $scope.$on('reloadSubmissions', function (evt, dataToLoad) {
                $scope.fetchWeeklyData($scope.filter_year, $scope.filter_term, $scope.filter_week, dataToLoad);
            });




            $scope.trendToShow =  {
                trend_days_punctual : true,
                trend_lesson_plan_vetted : false,
                trend_exercises : false,
                trend_class_management : false,
                trend_units_covered : false
            };

            $scope.changeTrendToShow = function (prop) {
                angular.forEach($scope.trendToShow, function (val, key) {
                    $scope.trendToShow[key] = false;
                });
                $timeout(function () {
                    $scope.trendToShow[prop] = true;
                },0);
            };

            // this variable is used to indicate which type of chart is visible
            $scope.pie_or_line = 'Pie';

            $scope.showLineChart = function(){

                $scope.pie_or_line = 'Line';
                $scope.teacherInfoLineData = [];

                angular.forEach($scope.teacherInfoData, function(teacherData){
                    if ( $scope.previousIndex !== undefined) {

                        if ($scope.teacherInfoData[ $scope.previousIndex ].id == teacherData.id) {
                            $scope.teacherInfoLineData = teacherData.punctualityHolder;
                        }

                    }

                });

                $scope.teacherInfoLineData.sort(function(a, b){
                    if (parseInt(a.week) > parseInt(b.week)) {
                        return 1
                    }else if (parseInt(a.week) < parseInt(b.week)) {
                        return -1
                    }else{
                        return 0
                    }
                });

                if ($scope.clickedTeacher) {
                    $scope.changeLineChart($scope.teacherInfoLineData,
                        $scope.clickedTeacher.first_name + ' '+ $scope.clickedTeacher.last_name + '\'s Attendance Trend'  ,'teacherLineChart');
                }
            };

            $scope.showPieChart = function(){
                $scope.pie_or_line = 'Pie';
                if ($scope.clickedTeacher) {
                    $scope.changePieChart($scope.tempArray, $scope.clickedTeacher.first_name + ' '+ $scope.clickedTeacher.last_name + '\'s Punctuality Chart', 'teacherPieChart');
                }
            };

            $scope.showBarChart = function(){
                $scope.pie_or_line = 'Pie';
                if ($scope.clickedTeacher) {
                    $scope.changeBarChart($scope.tempArray, $scope.clickedTeacher.first_name + ' ' + $scope.clickedTeacher.last_name + '\'s Punctuality Chart', 'teacherBarChart');
                }
            };

            //Show teacher charts
            $scope.showChart = function(index){

                if ( $scope.clickedTeacher.punctualityHolder.length > 0) {


                    $scope.rowSelected = true;

                    //This is a temporary variable that formats the selected row to a
                    // format that the pie chart can render
                    $scope.tempArray= [
                        {"field" : "Punctual",
                            "value" : $scope.clickedTeacher.punctualityHolder[0].days_punctual
                        },
                        {"field" : "Absent",
                            "value" : $scope.clickedTeacher.punctualityHolder[0].days_absent_with_permission + $scope.clickedTeacher.punctualityHolder[0].days_absent_without_permission
                        }
                    ];

                    //Keep a reference to the previous index so we can change its selected state to false
                    $scope.previousIndex = index;

                    //Dynamically change the chart type that is showing
                    var chartType = 'show'+$scope.pie_or_line+'Chart';

                    //Show the chart type chosen via the scope object
                    $scope[chartType]();

                }
            };



            $scope.getTotalOfTwo = function (fig1, fig2) {
                return (parseFloat(fig1) + parseFloat(fig2))
            };

            $scope.deletingData = false;

            $scope.$on('deletingData', function(){
                $scope.deletingData = true;
            });

            $scope.$on('teacherAttendanceClassSubmittedDataDeleted', function (evt, sentInfo) {
                for( var i = 0; i < $scope.teacherInfoData.length; i++ ){
                    if ($scope.teacherInfoData[i].index === sentInfo.teacher_index) {
                        if (sentInfo.data_type === 'class_management') {
                            angular.forEach($scope.teacherInfoData[i].classManagementHolder, function (value, key) {
                                if (value.id === sentInfo.deleted_id) {
                                    $timeout(function () {
                                        value.deleted = true;
                                    }, 100);
                                }
                            });
                        }else if (sentInfo.data_type === 'attendance') {
                            angular.forEach($scope.teacherInfoData[i].punctualityHolder, function (value, key) {
                                if (value.id === sentInfo.deleted_id) {
                                    $timeout(function () {
                                        value.deleted = true;
                                    }, 100);
                                }
                            });
                        }
                    }
                }
                $scope.deletingData = false;
            });




            /*this function send the totals calculated to the server to update the weekly table*/
            $scope.updateSchoolWeeklyTotal = function () {
                var holder = [];

                $scope.updatingTotals = true;

                angular.forEach(teacherAttendanceTotals, function (value, key) {
                    /*calculate the weeks percentage*/
                    /*first get the expected total attendance = total teachers in school x number of days school was in session*/
                    /*then use the actual total attendance, divided by the expected total number of teachers and multiply by 100 */
                    var aveAttendance = (value.totalAttendance/(value.days_in_session * value.expectedTeacherTotal)) * 100;

                    holder.push({
                        average_teacher_attendance : Math.round(aveAttendance),
                        current_number_of_teachers : value.teachersTotal,
                        year : value.year,
                        term : value.term,
                        week_number : value.week_number,
                        country_id : $scope.selected_school.country_id,
                        region_id : $scope.selected_school.region_id,
                        district_id : $scope.selected_school.district_id,
                        circuit_id : $scope.selected_school.circuit_id,
                        school_id : $scope.selected_school.id
                    })
                });


                scSchool.updateWeeklyTableForTeacherAttendance(holder)
                    .success(function (successData) {
                        if (successData.status === 200) {
                            scNotifier.notifySuccess("Recalculation", "Teacher Attendance totals have been updated.")
                        }
                    })
                    .error(function (errData) {
                        scNotifier.notifyFailure("Recalculation", "Teacher Attendance totals failed to update")
                    })
                    .finally(function () {
                        $scope.updatingTotals = false;
                    });


            };




            /*Show Export Button*/
            $scope.beginExport = function (tableID) {
                var table = $('#'+tableID);
                // $('.'+tableID).hide();

                instance = table.tableExport({
                    headers: true,
                    // footers: true,
                    formats: ['xls', 'csv'],
                    filename: tableID + "-wk " + $scope.filter_week + "-" + $scope.filter_term + "-" + $scope.filter_year,
                    bootstrap: true,
                    position: 'top',
                    // ignoreRows: null,
                    // ignoreCols: null,
                    // ignoreCSS: '.tableexport-ignore',
                    // emptyCSS: '.tableexport-empty',
                    trimWhitespace: false
                });


                $timeout(function () {
                    instance.reset();
                }, 0);
            };


            $scope.trending = false;

        }
    ]);


