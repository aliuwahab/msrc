/**
 * Created by Kaygee on 18/05/2015.
 */
//School Report Card Controller (SRC)

schoolMonitor.controller('scViewSchoolStatisticsController',
    ['$scope', '$stateParams', 'School', 'scReportGenerator', 'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', 'scGlobalService', '$timeout',

        function ($scope, $stateParams, School, scReportGenerator, scNotifier,  scRegion, scDistrict, scCircuit, scSchool, scGlobalService, $timeout) {


            $scope.loadSchoolStats = function () {
                if (!$scope.filter_week || !$scope.filter_term || !$scope.filter_year) {
                    scNotifier.notifyInfo('Term Parameters', 'Please select all 3 parameters to proceed');
                    return;
                }
                var dataParams = {
                    data_collector_type: "head_teacher",
                    term: $scope.filter_term,
                    week_number: $scope.filter_week,
                    year: $scope.filter_year,
                    school_id: $stateParams.id
                };
                scSchool.tracker.dataParams = angular.copy(dataParams);
                scReportGenerator.loadStats(dataParams);
                $scope.loading = true;
                $scope.isNotVirgin = true;
            };

            if (scSchool.tracker.dataParams) {
                $scope.filter_week = angular.copy(scSchool.tracker.dataParams.week_number);
                $scope.filter_term = angular.copy(scSchool.tracker.dataParams.term);
                $scope.filter_year = angular.copy(scSchool.tracker.dataParams.year);
            }




            var performanceBarChartHolder = {};

            function runPupilPerformance(allSchoolPupilPerformanceData) {
                var schoolPerformanceTotals = {
                    english: {
                        class_score: {}
                    },
                    math: {
                        class_score: {}
                    },
                    gh_lang: {
                        class_score: {}
                    },
                    levels_available: {}
                };
                angular.forEach(allSchoolPupilPerformanceData, function (item, index) {

                    /*
                    * average_english_score:"50"
                        average_ghanaian_language_score:"50"
                        average_maths_score:"50"
                        data_collector_type:"head_teacher"
                        district_id:97
                        level:"KG1"
                        num_pupil_who_score_above_average_in_english:24
                        num_pupil_who_score_above_average_in_ghanaian_language:20
                        num_pupil_who_score_above_average_in_maths:24
                        term:"third_term"
                        year:"2015/2016"
                        */
                    //find the average score for the district, by subject
                    //so for each item
                    //add the averages of each subject, class by class
                    //add the class in one property under the subject

                    schoolPerformanceTotals.levels_available[item.level] = true;

                    if (isFinite(Number(item.average_english_score))) {
                        if (angular.isDefined(schoolPerformanceTotals.english.class_score[item.level])) {
                            schoolPerformanceTotals.english.class_score[item.level].average_score += Number(item.average_english_score);
                            schoolPerformanceTotals.english.class_score[item.level].above_average += Number(item.num_pupil_who_score_above_average_in_english);
                        } else {
                            schoolPerformanceTotals.english.class_score[item.level] = {
                                average_score: Number(item.average_english_score),
                                above_average: Number(item.num_pupil_who_score_above_average_in_english)
                            }
                        }
                    }


                    if (isFinite(Number(item.average_maths_score))) {
                        if (angular.isDefined(schoolPerformanceTotals.math.class_score[item.level])) {
                            schoolPerformanceTotals.math.class_score[item.level].average_score += Number(item.average_maths_score);
                            schoolPerformanceTotals.math.class_score[item.level].above_average += Number(item.num_pupil_who_score_above_average_in_maths);
                        } else {
                            schoolPerformanceTotals.math.class_score[item.level] = {
                                average_score: Number(item.average_maths_score),
                                above_average: Number(item.num_pupil_who_score_above_average_in_maths)
                            }
                        }
                    }


                    if (isFinite(Number(item.average_ghanaian_language_score))) {
                        if (angular.isDefined(schoolPerformanceTotals.gh_lang.class_score[item.level])) {
                            schoolPerformanceTotals.gh_lang.class_score[item.level].average_score += Number(item.average_ghanaian_language_score);
                            schoolPerformanceTotals.gh_lang.class_score[item.level].above_average += Number(item.num_pupil_who_score_above_average_in_ghanaian_language);
                        } else {
                            schoolPerformanceTotals.gh_lang.class_score[item.level] = {
                                average_score: Number(item.average_ghanaian_language_score),
                                above_average: Number(item.num_pupil_who_score_above_average_in_ghanaian_language)
                            }
                        }
                    }

                    performanceBarChartHolder[item.level] = {
                        gh_lang : {
                            average_score : schoolPerformanceTotals.gh_lang.class_score[item.level].average_score,
                            above_average : schoolPerformanceTotals.gh_lang.class_score[item.level].above_average
                        },
                        english : {
                            average_score : schoolPerformanceTotals.english.class_score[item.level].average_score,
                            above_average : schoolPerformanceTotals.english.class_score[item.level].above_average
                        },
                        math : {
                            average_score : schoolPerformanceTotals.math.class_score[item.level].average_score,
                            above_average : schoolPerformanceTotals.math.class_score[item.level].above_average
                        }
                    }
                });

                var performanceBarChart = {
                    label : "Pupil Performance By Class",
                    labels : [],
                    datasets : []
                };
                var langHolder = {
                    gh_lang : [],
                    gh_lang_line : [],
                    english : [],
                    english_line : [],
                    math : [],
                    math_line: []
                };

                /*get the class key*/
                /*get all the language datasets*/
                /*get the above average data for the line*/
                /*get one object with dataset title, class by class and language score per class*/

                /*get the class key*/
                angular.forEach(performanceBarChartHolder, function (perfData, classKey) {
                    performanceBarChart.labels.push(classKey);
                    /*get all the language datasets*/
                    langHolder.gh_lang.push(perfData.gh_lang.average_score);
                    langHolder.gh_lang_line.push(perfData.gh_lang.above_average);
                    langHolder.english.push(perfData.english.average_score);
                    langHolder.english_line.push(perfData.english.above_average);
                    langHolder.math.push(perfData.math.average_score);
                    langHolder.math_line.push(perfData.math.above_average);
                });

                performanceBarChart.datasets.push({
                    label: 'Ghanaian Language',
                    backgroundColor: pattern.draw('triangle-inverted', 'rgba(231, 235, 8, 0.75)'),
                    data:langHolder.gh_lang
                });

                performanceBarChart.datasets.push({
                    label: 'English',
                    backgroundColor: pattern.draw('diagonal-right-left', 'rgba(16, 204, 13, 0.75)'),
                    data:langHolder.english
                });
                performanceBarChart.datasets.push({
                    label: 'Maths',
                    backgroundColor: pattern.draw('dot-dash', 'rgba(75, 192, 192, 0.75)'),
                    data: langHolder.math
                });
                /*$scope.performanceBarChart.datasets.push({
                    label: 'Above Average in Ghanaian Language',
                    backgroundColor: 'rgba(255, 99, 132, 0)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    data:langHolder.gh_lang_line,
                    // Changes this dataset to become a line
                    type: 'line',
                    fill: false
                });
                $scope.performanceBarChart.datasets.push({
                    label: 'Above Average in English',
                    backgroundColor: 'rgba(54, 162, 235, 0)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    data:langHolder.english_line,
                    // Changes this dataset to become a line
                    type: 'line',
                    fill: false
                });
                $scope.performanceBarChart.datasets.push({
                    label: 'Above Average in Maths',
                    backgroundColor: 'rgba(255, 206, 86, 0.75)',
                    borderColor: 'rgba(255, 206, 86, 0.75)',
                    data:langHolder.math_line,
                    // Changes this dataset to become a line
                    type: 'line',
                    fill: false
                });*/

                scGlobalService.drawStatisticsBarStackedChart(performanceBarChart.labels,
                    performanceBarChart.label, performanceBarChart.datasets,
                    'performanceBarChart', "horizontalBarChart");



                $scope.schoolPerformanceTotals = angular.copy(schoolPerformanceTotals);
                $scope.schoolPerformanceTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }
            if (scSchool.tracker.allSchoolPupilPerformanceData) {
                runPupilPerformance(scSchool.tracker.allSchoolPupilPerformanceData);
            }
            $scope.$on('pupilPerformanceDataLoaded', function (event, allSchoolPupilPerformanceData) {
                scSchool.tracker.allSchoolPupilPerformanceData = allSchoolPupilPerformanceData;
                runPupilPerformance(allSchoolPupilPerformanceData);
            });







            function runEnrolmentData(allSchoolPupilEnrollmentData) {
                $scope.enrollmentBarChart = {
                    label : "Pupil Enrollment By Class",
                    labels : [],
                    chartData : []
                };

                var schoolEnrollmentTotals = {
                    total_male: 0,
                    total_female: 0,
                    total_pupils: 0,
                    total_by_class: {}
                };

                angular.forEach(allSchoolPupilEnrollmentData, function (item, index) {
                    /* normal: parseInt(item.total_enrolment),
                        special: 0,
                        boys : parseInt(item.num_males),
                        special_boys : 0,
                        girls : parseInt(item.num_females),
                        special_girls : 0,
                        week_number :  item.week_number,
                        term : item.term,
                        year : item.year
                     */

                    schoolEnrollmentTotals.total_male += Number(item.num_males);
                    schoolEnrollmentTotals.total_female += Number(item.num_females);
                    schoolEnrollmentTotals.total_pupils += Number(item.total_enrolment);

                    if (angular.isDefined(schoolEnrollmentTotals.total_by_class[item.level])) {
                        schoolEnrollmentTotals.total_by_class[item.level].total_male += Number(item.num_males);
                        schoolEnrollmentTotals.total_by_class[item.level].total_female += Number(item.num_females);
                        schoolEnrollmentTotals.total_by_class[item.level].total_pupils += Number(item.total_enrolment);
                    } else {
                        schoolEnrollmentTotals.total_by_class[item.level] = {
                            total_male: Number(item.num_males),
                            total_female: Number(item.num_females),
                            total_pupils: Number(item.total_enrolment)
                        }
                    }
                });

                angular.forEach(schoolEnrollmentTotals.total_by_class, function(eachClass, prop){
                    $scope.enrollmentBarChart.labels.push(prop);
                    $scope.enrollmentBarChart.chartData.push(eachClass.total_pupils);
                });

                scGlobalService.drawStatisticsBarChart($scope.enrollmentBarChart.labels,
                    $scope.enrollmentBarChart.label, $scope.enrollmentBarChart.chartData,
                    'enrollmentBarChart', "barChart");

                $scope.schoolEnrollmentTotals = angular.copy(schoolEnrollmentTotals);
                $scope.schoolEnrollmentTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }
            if (scSchool.tracker.allSchoolPupilEnrollmentData) {
                runEnrolmentData(scSchool.tracker.allSchoolPupilEnrollmentData)
            }
            $scope.$on('pupilEnrollmentDataLoaded', function (event, allSchoolPupilEnrollmentData) {
                scSchool.tracker.allSchoolPupilEnrollmentData = allSchoolPupilEnrollmentData;
                runEnrolmentData(allSchoolPupilEnrollmentData)
            });


            function runSpecialEnrolmentData(allSchoolSpecialEnrollmentData) {
                var schoolSpecialEnrollmentTotals = {
                    total_male: 0,
                    total_female: 0,
                    total_pupils: 0,
                    total_by_class: {}
                };

                angular.forEach(allSchoolSpecialEnrollmentData, function (item, index) {
                    /*
                        level:"KG1"
                        num_females:0
                        num_males:3
                        num_of_streams:1
                        school_in_session:1
                        term:"second_term"
                        total_enrolment:3
                    */

                    schoolSpecialEnrollmentTotals.total_male += Number(item.num_males);
                    schoolSpecialEnrollmentTotals.total_female += Number(item.num_females);
                    schoolSpecialEnrollmentTotals.total_pupils += Number(item.total_enrolment);

                    if (angular.isDefined(schoolSpecialEnrollmentTotals.total_by_class[item.level])) {
                        schoolSpecialEnrollmentTotals.total_by_class[item.level].total_male += Number(item.num_males);
                        schoolSpecialEnrollmentTotals.total_by_class[item.level].total_female += Number(item.num_females);
                        schoolSpecialEnrollmentTotals.total_by_class[item.level].total_pupils += Number(item.total_enrolment);
                    } else {
                        schoolSpecialEnrollmentTotals.total_by_class[item.level] = {
                            total_male: Number(item.num_males),
                            total_female: Number(item.num_females),
                            total_pupils: Number(item.total_enrolment)
                        }
                    }
                });

                $scope.schoolSpecialEnrollmentTotals = angular.copy(schoolSpecialEnrollmentTotals);
                $scope.schoolSpecialEnrollmentTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }
            if (scSchool.tracker.specialPupilEnrollmentDataLoaded) {
                runSpecialEnrolmentData(scSchool.tracker.specialPupilEnrollmentDataLoaded)
            }
            $scope.$on('specialPupilEnrollmentDataLoaded', function (event, specialPupilEnrollmentDataLoaded) {
                scSchool.tracker.specialPupilEnrollmentDataLoaded = specialPupilEnrollmentDataLoaded;
                runSpecialEnrolmentData(specialPupilEnrollmentDataLoaded)
            });



            function runPupilAttendanceData(allPupilAttendanceDataLoaded) {

               var schoolAttendanceTotals = {
                    total_attendance_for_term: 0,
                    total_school_session: 0
                };

                var classTotalHolder = {};
                var daysInSessionTotalHolder = {};

               /*  /!*
                    level:"KG1"
                    num_females:93
                    num_males:84
                    number_of_week_days:4
                    total:177

                    loop over all the attendances
                    add the attendances per class,
                    add the number of days in session for that week
                    then
                    multiply total enrolment by the number of days school was in session

                    divide the totalAttendance by expected attendance and multiply by 100
                *!/

                angular.forEach(allPupilAttendanceDataLoaded, function (attendanceItem, index) {
                    var classConcat = attendanceItem.level + attendanceItem.week_number + attendanceItem.term + attendanceItem.year;
                    var daysInSesConcat = attendanceItem.week_number + attendanceItem.term + attendanceItem.year;
                    classTotalHolder[classConcat] = attendanceItem.total;
                    daysInSessionTotalHolder[daysInSesConcat] = attendanceItem.number_of_week_days
                });

                // just for a week, the loop above prevents duplicate addition
                angular.forEach(classTotalHolder, function (classItem, index) {
                    schoolAttendanceTotals.total_attendance_for_term += classItem
                });

                angular.forEach(daysInSessionTotalHolder, function (weekItem, index) {
                    schoolAttendanceTotals.total_school_session += weekItem
                });*/


                angular.forEach(allPupilAttendanceDataLoaded, function (attendanceItem, index) {
                    var classConcat = attendanceItem.level + attendanceItem.term + attendanceItem.year;
                    var daysInSesConcat = attendanceItem.week_number + attendanceItem.term + attendanceItem.year;

                    if (classTotalHolder[classConcat]) {
                        classTotalHolder[classConcat] += attendanceItem.total;
                    }else{
                        classTotalHolder[classConcat] = attendanceItem.total;
                    }

                    if (!daysInSessionTotalHolder[daysInSesConcat]) {
                        daysInSessionTotalHolder[daysInSesConcat] = attendanceItem.number_of_week_days;
                    }
                });

                // console.log(daysInSessionTotalHolder, classTotalHolder);
                // just for a week, the loop above prevents duplicate addition
                angular.forEach(classTotalHolder, function (classItem, index) {
                    schoolAttendanceTotals.total_attendance_for_term += classItem
                });

                angular.forEach(daysInSessionTotalHolder, function (weekItem, index) {
                    schoolAttendanceTotals.total_school_session += weekItem
                });

                $scope.schoolAttendanceTotals = angular.copy(schoolAttendanceTotals);
                $scope.schoolAttendanceTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.allPupilAttendanceDataLoaded) {
                runPupilAttendanceData(scSchool.tracker.allPupilAttendanceDataLoaded)
            }
            $scope.$on('allPupilAttendanceDataLoaded', function (event, allPupilAttendanceDataLoaded) {
                scSchool.tracker.allPupilAttendanceDataLoaded = allPupilAttendanceDataLoaded;
                runPupilAttendanceData(allPupilAttendanceDataLoaded)
            });




            var teacherAttendancePieChart = {
                label : "Teacher Attendance Rate",
                labels : ["Present", "Absent"],
                data : []
            };
            function runTeacherPunctuality(allSchoolTeacherPupilPunctualityLessonPlansData) {
                var schoolTeacherAttendanceTotals = {
                    expectedAttendance: 0,
                    actualAttendance: 0,
                    attendanceRatio: 0,
                    total_above_50: 0,
                    num_of_exercises_given: 0,
                    num_of_exercises_marked: 0,
                    exercises_marked_average: 0,
                    total_by_teachers: {}
                };
                angular.forEach(allSchoolTeacherPupilPunctualityLessonPlansData, function (item, index) {

                    if (!isFinite(Number(item.num_of_exercises_given))) item.num_of_exercises_given = 0;
                    if (!isFinite(Number(item.num_of_exercises_marked))) item.num_of_exercises_marked = 0;
                    if (!isFinite(Number(item.days_present))) item.days_present = 0;
                    if (!isFinite(Number(item.days_in_session))) item.days_in_session = 5;


                    schoolTeacherAttendanceTotals.actualAttendance += (item.days_present);
                    schoolTeacherAttendanceTotals.expectedAttendance += (item.days_in_session);
                    if (isFinite(schoolTeacherAttendanceTotals.actualAttendance / schoolTeacherAttendanceTotals.expectedAttendance)) {
                        schoolTeacherAttendanceTotals.attendanceRatio = Math.round(100 * (schoolTeacherAttendanceTotals.actualAttendance / schoolTeacherAttendanceTotals.expectedAttendance));
                        teacherAttendancePieChart.data = [];
                        teacherAttendancePieChart.data.push(schoolTeacherAttendanceTotals.attendanceRatio);
                        teacherAttendancePieChart.data.push(100 - schoolTeacherAttendanceTotals.attendanceRatio);
                    }

                    schoolTeacherAttendanceTotals.num_of_exercises_given += (Number(item.num_of_exercises_given));
                    schoolTeacherAttendanceTotals.num_of_exercises_marked += (Number(item.num_of_exercises_marked));
                    if (isFinite(schoolTeacherAttendanceTotals.num_of_exercises_marked / schoolTeacherAttendanceTotals.num_of_exercises_given)) {
                        schoolTeacherAttendanceTotals.exercises_marked_average +=
                            Math.round(100 * (schoolTeacherAttendanceTotals.num_of_exercises_marked / schoolTeacherAttendanceTotals.num_of_exercises_given));
                    }


                    if (angular.isDefined(schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id])) {

                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].actualAttendance += (item.days_present);
                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].expectedAttendance += (item.days_in_session);
                        if (isFinite(schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].actualAttendance / schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].expectedAttendance)) {
                            schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].attendanceRatio =
                                Math.round(100 * (schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].actualAttendance / schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].expectedAttendance));
                        }

                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_given += (Number(item.num_of_exercises_given));
                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_marked += (Number(item.num_of_exercises_marked));

                        if (isFinite(schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_marked / schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_given)) {
                            schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].exercises_marked_average +=
                                Math.round(100 * (schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_marked / schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_given))
                        }

                    } else {
                        var attendanceRatio = 0;
                        var exercisesMarkedAverage = 0;
                        if (isFinite(item.days_present / item.days_in_session)) attendanceRatio = item.days_present / item.days_in_session;
                        if (isFinite(item.num_of_exercises_marked / item.num_of_exercises_given)) exercisesMarkedAverage = item.num_of_exercises_marked / item.num_of_exercises_given;
                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id] = {
                            expectedAttendance: Number(item.days_in_session),
                            actualAttendance: Number(item.days_present),
                            attendanceRatio: Math.round(100 * attendanceRatio),
                            num_of_exercises_given: Number(item.num_of_exercises_given),
                            num_of_exercises_marked: Number(item.num_of_exercises_marked),
                            exercises_marked_average: Math.round(100 * (exercisesMarkedAverage))
                        };
                        if (item.teacher) {
                            schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].name_of_teacher = (item.teacher.first_name || '') + " " + (item.teacher.last_name || '');
                        }
                    }
                });

                scGlobalService.drawPieStatisticsChart(teacherAttendancePieChart.labels, teacherAttendancePieChart.label, teacherAttendancePieChart.data, 'teacherAttendance', 'pieChartOne');

                var teacherAttendanceAboveFiftyPieChart = {
                    label: "Teacher Attendance Above 50%",
                    labels: ["Present", "Absent"],
                    data : []
                };

                /*Find the total number of schools with attendance over 50%*/
                angular.forEach(schoolTeacherAttendanceTotals.total_by_teachers, function (attendanceBySchool, index) {
                    if (attendanceBySchool.attendanceRatio > 50) {
                        schoolTeacherAttendanceTotals.total_above_50++;
                        var percentage = Math.round((schoolTeacherAttendanceTotals.total_above_50 / $scope.teachers_in_school.length) * 100);
                        teacherAttendanceAboveFiftyPieChart.data = [(percentage), (100 - percentage)]
                    }

                    if (attendanceBySchool.exercises_marked_average > schoolTeacherAttendanceTotals.exercises_marked_average) {
                        schoolTeacherAttendanceTotals.adequately_marked_schools++;
                    }

                });

                scGlobalService.drawPieStatisticsChart(teacherAttendanceAboveFiftyPieChart.labels, teacherAttendanceAboveFiftyPieChart.label, teacherAttendanceAboveFiftyPieChart.data, 'AboveFiftyPieChart', 'percentOverFiftyPie');



                $scope.schoolTeacherAttendanceTotals = angular.copy(schoolTeacherAttendanceTotals);
                $scope.schoolTeacherAttendanceTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }

            $scope.$on('teacherPunctualityLessonPlansDataLoaded', function (event, allSchoolTeacherPupilPunctualityLessonPlansData) {
                scSchool.tracker.allSchoolTeacherPupilPunctualityLessonPlansData = allSchoolTeacherPupilPunctualityLessonPlansData;
                runTeacherPunctuality(allSchoolTeacherPupilPunctualityLessonPlansData);
            });


            function runTeacherUnitsCovered(allSchoolTeachersUnitsCoveredData) {
                var schoolUnitCoveredTotals = {
                    gh_lang: {
                        total : 0,
                        teachers : {}
                    },
                    english: {
                        total : 0,
                        teachers : {}
                    },
                    math: {
                        total : 0,
                        teachers : {}
                    }
                };
                angular.forEach(allSchoolTeachersUnitsCoveredData, function (item, index) {

                    if (isFinite(Number(item.ghanaian_language_units_covered))) {
                        schoolUnitCoveredTotals.gh_lang.total += Number(item.ghanaian_language_units_covered);

                        if (angular.isDefined(schoolUnitCoveredTotals.gh_lang.teachers[item.teacher_id])) {
                            schoolUnitCoveredTotals.gh_lang.teachers[item.teacher_id].gh_lang = item.ghanaian_language_units_covered
                        } else {
                            schoolUnitCoveredTotals.gh_lang.teachers[item.teacher_id] = {
                                gh_lang : item.ghanaian_language_units_covered
                            }
                        }
                    }

                    if (isFinite(Number(item.english_language_units_covered))) {
                        schoolUnitCoveredTotals.english.total += Number(item.english_language_units_covered);

                        if (angular.isDefined(schoolUnitCoveredTotals.english.teachers[item.teacher_id])) {
                            schoolUnitCoveredTotals.english.teachers[item.teacher_id].english = item.english_language_units_covered
                        } else {
                            schoolUnitCoveredTotals.english.teachers[item.teacher_id] = {
                                english : item.english_language_units_covered
                            }
                        }
                    }

                    if (isFinite(Number(item.mathematics_units_covered))) {
                        schoolUnitCoveredTotals.math.total += Number(item.mathematics_units_covered);

                        if (angular.isDefined(schoolUnitCoveredTotals.math.teachers[item.teacher_id])) {
                            schoolUnitCoveredTotals.math.teachers[item.teacher_id].math = item.mathematics_units_covered
                        } else {
                            schoolUnitCoveredTotals.math.teachers[item.teacher_id] = {
                                math : item.mathematics_units_covered
                            }
                        }
                    }
                });

                $scope.schoolUnitCoveredTotals = angular.copy(schoolUnitCoveredTotals);
                $scope.schoolUnitCoveredTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.allSchoolTeachersUnitsCoveredData) {
                runTeacherUnitsCovered(scSchool.tracker.allSchoolTeachersUnitsCoveredData);
            }
            $scope.$on('teacherUnitsCoveredDataLoaded', function (event, allSchoolTeachersUnitsCoveredData) {
                scSchool.tracker.allSchoolTeachersUnitsCoveredData = allSchoolTeachersUnitsCoveredData;
                runTeacherUnitsCovered(allSchoolTeachersUnitsCoveredData);
            });



            function runRecordBooks(allSchoolRecordBooksData) {
                var schoolRecordBooks = {
                    admission_register: {
                        name: "Admission Register",
                        total: 0
                    },
                    class_registers: {
                        name: "Class Registers",
                        total: 0
                    },
                    teacher_attendance_register: {
                        name: "Teacher Attendance\nRegister",
                        total: 0
                    },
                    visitors: {
                        name: "Visitors\' Book",
                        total: 0
                    },
                    log: {
                        name: "Log Book",
                        total: 0
                    },
                    sba: {
                        name: "SBA",
                        total: 0
                    },
                    movement: {
                        name: "Movement Book",
                        total: 0
                    },
                    spip: {
                        name: "SPIP",
                        total: 0
                    },
                    inventory_books: {
                        name: "Inventory Books",
                        total: 0
                    },
                    cummulative_records_books: {
                        name: "Cumulative Record\nBooks",
                        total: 0
                    },
                    continous_assessment: {
                        name: "Continuous\nAssessment / SMB",
                        total: 0
                    },
                    books_available: []
                };
                angular.forEach(allSchoolRecordBooksData, function (item, index) {

                    angular.forEach(schoolRecordBooks, function (itemValue, itemProp) {

                        if (item[itemProp] === 'yes') {
                            schoolRecordBooks[itemProp].total++;
                            schoolRecordBooks.books_available.push(schoolRecordBooks[itemProp].name)
                        }
                    })
                });
                $scope.schoolRecordBooks = angular.copy(schoolRecordBooks);

                $scope.books = [];
                angular.forEach(allSchoolRecordBooksData, function(item,index){

                    var booksHolder =[];



                    booksHolder.push({
                        name: "Admission Register",
                        term :   item.term,
                        status : scGlobalService.makeFirstLetterCapital(item.admission_register),
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Class Registers",
                        term :  item.term,
                        status : scGlobalService.makeFirstLetterCapital(item.class_registers),
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Teacher Attendance\nRegister",
                        status: scGlobalService.makeFirstLetterCapital(item.teacher_attendance_register),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Visitors\' Book",
                        status: scGlobalService.makeFirstLetterCapital(item.visitors),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Log Book",
                        status: scGlobalService.makeFirstLetterCapital(item.log),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "SBA",
                        status: scGlobalService.makeFirstLetterCapital(item.sba),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Movement Book",
                        status: scGlobalService.makeFirstLetterCapital(item.movement),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "SPIP",
                        status: scGlobalService.makeFirstLetterCapital(item.spip),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Inventory Books",
                        status: scGlobalService.makeFirstLetterCapital(item.inventory_books),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Cumulative Record\nBooks",
                        status: scGlobalService.makeFirstLetterCapital(item.cummulative_records_books),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });

                    booksHolder.push({
                        name: "Continuous\nAssessment / SMB",
                        status: scGlobalService.makeFirstLetterCapital(item.continous_assessment || 'no'),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    $scope.books = $scope.books.concat(booksHolder);

                });

                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.allSchoolRecordBooksData) {
                runRecordBooks(scSchool.tracker.allSchoolRecordBooksData)
            }

            $scope.$on('recordBooksDataLoaded', function (event, allSchoolRecordBooksData) {
                scSchool.tracker.allSchoolRecordBooksData = allSchoolRecordBooksData;
                runRecordBooks(allSchoolRecordBooksData);
            });

            function runGrantCapitation(districtGrantCapitationPaymentsData) {
                var schoolGrantStuff = {
                    totalAmount: 0,
                    total_first: 0,
                    total_second: 0,
                    total_third: 0
                };

                angular.forEach(districtGrantCapitationPaymentsData, function (item, index) {
                    if (angular.isDefined(item.first_tranche_amount) && item.first_tranche_amount > 0) {
                        schoolGrantStuff.totalAmount += parseInt(item.first_tranche_amount);
                        schoolGrantStuff.total_first += parseInt(item.first_tranche_amount);
                    }
                    if (angular.isDefined(item.second_tranche_amount) && item.second_tranche_amount > 0) {
                        schoolGrantStuff.totalAmount += parseInt(item.second_tranche_amount);
                        schoolGrantStuff.total_second += parseInt(item.second_tranche_amount);
                    }
                    if (angular.isDefined(item.third_tranche_amount) && item.third_tranche_amount > 0) {
                        schoolGrantStuff.totalAmount += parseInt(item.third_tranche_amount);
                        schoolGrantStuff.total_third += parseInt(item.third_tranche_amount);
                    }


                    /*
                        grant.first_tranche_date
                        grant.first_tranche_amount
                        grant.second_tranche_date
                        grant.second_tranche_amount
                        grant.third_tranche_date
                        grant.third_tranche_amount
                    */
                });


                $scope.schoolGrantStuff = angular.copy(schoolGrantStuff);
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.districtGrantCapitationPaymentsData) {
                runGrantCapitation(scSchool.tracker.districtGrantCapitationPaymentsData)
            }

            $scope.$on('grantCapitationPaymentsDataLoaded', function (event, districtGrantCapitationPaymentsData) {
                scSchool.tracker.districtGrantCapitationPaymentsData = districtGrantCapitationPaymentsData;
                runGrantCapitation(districtGrantCapitationPaymentsData);
            });

            function runSupport(districtSupportData) {
                var schoolSupportStuff = {
                    donor_support: {
                        name: "Donor Support",
                        cash: 0,
                        kind: ''
                    },
                    community_support: {
                        name: "Community Support",
                        cash: 0,
                        kind: ''
                    },
                    district_support: {
                        name: "School Support",
                        cash: 0,
                        kind: ''
                    },
                    pta_support: {
                        name: "PTA Support",
                        cash: 0,
                        kind: ''
                    },
                    other_support: {
                        name: "Other",
                        cash: 0,
                        kind: ''
                    },
                    total_cash: 0,
                    received_support: false
                };
                /*
                name: "Donor Support",
                in_cash : item.donor_support_in_cash,
                in_kind : item.donor_support_in_kind,

                name: "Community Support",
                in_cash : item.community_support_in_cash,
                in_kind : item.community_support_in_kind,

                name: "District Support",
                in_cash : item.district_support_in_cash,
                in_kind : item.district_support_in_kind,

                name: "PTA Support",
                in_cash : item.pta_support_in_cash,
                in_kind : item.pta_support_in_kind,
                comment : item.comment


                name: "Other",
                in_cash : item.other_support_in_cash,
                in_kind : item.other_support_in_kind,

            });*/
                angular.forEach(districtSupportData, function (item, index) {

                    angular.forEach(schoolSupportStuff, function (itemVal, itemProp) {

                        schoolSupportStuff.total_cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                        if(!schoolSupportStuff.received_support) schoolSupportStuff.received_support = angular.isDefined(item[itemProp + '_in_kind']);
                        schoolSupportStuff[itemProp].cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                        schoolSupportStuff[itemProp].kind = angular.isDefined(item[itemProp + '_in_kind']);
                    });

                });
                $scope.schoolSupportStuff = angular.copy(schoolSupportStuff);
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.districtSupportData) {
                runSupport(scSchool.tracker.districtSupportData);
            }

            $scope.$on('supportTypesDataLoaded', function (event, districtSupportData) {
                scSchool.tracker.districtSupportData = districtSupportData;
                runSupport(districtSupportData);
            });

            function runFurniture(districtSchoolFurnitureItems) {
                var schoolFurniture = {
                    total_adequates: []
                };

                var adequacyElems = ['pupils_furniture', 'teacher_tables', 'teacher_chairs', 'classrooms_cupboard'];

                angular.forEach(districtSchoolFurnitureItems, function (item, index) {
                    for (var i = 0; i < adequacyElems.length; i++) {
                        var adequateProp = adequacyElems[i];

                        if (item[adequateProp] === 'adequate') {
                            schoolFurniture.total_adequates.push(adequateProp);
                        }

                    }

                });
                $scope.schoolFurniture = angular.copy(schoolFurniture);

                $scope.furnitureItems = [];
                angular.forEach(districtSchoolFurnitureItems, function(furnitureObject , index){

                    var furnitureHolder =[];

                    furnitureHolder.push({
                        name: "Pupils Furniture",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.pupils_furniture],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Teacher Tables",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.teacher_tables],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Teacher Chairs",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.teacher_chairs],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Classrooms Cupboard",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.classrooms_cupboard],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });

                    $scope.furnitureItems = $scope.furnitureItems.concat(furnitureHolder);

                });

            }

            if (scSchool.tracker.districtSchoolFurnitureItems) {
                runFurniture(scSchool.tracker.districtSchoolFurnitureItems);
            }
            $scope.$on('furnitureDataLoaded', function (event, districtSchoolFurnitureItems) {
                scSchool.tracker.districtSchoolFurnitureItems = districtSchoolFurnitureItems;
                runFurniture(districtSchoolFurnitureItems);
            });

            function runStructure(districtSchoolStructureItems) {
                var schoolStructure = {
                    total_goods: []
                };
                var goodElems = ['walls', 'doors', 'windows', 'floors', 'blackboard', 'illumination'];

                angular.forEach(districtSchoolStructureItems, function (item, index) {
                    for (var i = 0; i < goodElems.length; i++) {
                        var statusProp = goodElems[i];

                        if (item[statusProp] === 'good') {
                            schoolStructure.total_goods.push(statusProp);
                        }
                    }
                });

                $scope.schoolStructure = angular.copy(schoolStructure);

                $scope.structureItems = [];
                angular.forEach(districtSchoolStructureItems, function(structureObject , index){

                    var structureHolder =[];

                    structureHolder.push({
                        name: "Walls",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.walls],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Doors",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.doors],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Windows",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.windows],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Floors",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.floors],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Blackboard",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.blackboard],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    try{
                        structureHolder.push({
                            name: "Illumination",
                            term :   structureObject.term,
                            status : $scope.formatGoodPoorFair[structureObject.illumination],
                            year : structureObject.year,
                            comment : structureObject.comment,
                            data_collector_type : structureObject.data_collector_type
                        });
                    }catch(e){
                        // console.log(e);
                        structureHolder.push({
                            name: "Illumination",
                            term :   structureObject.term,
                            status : ' ',
                            year : structureObject.year,
                            comment : structureObject.comment,
                            data_collector_type : structureObject.data_collector_type
                        });
                    }


                    $scope.structureItems = $scope.structureItems.concat(structureHolder);


                })
            }

            if (scSchool.tracker.districtSchoolStructureItems) {
                runStructure(scSchool.tracker.districtSchoolStructureItems)
            }
            $scope.$on('structureDataLoaded', function (event, districtSchoolStructureItems) {
                scSchool.tracker.districtSchoolStructureItems = districtSchoolStructureItems;
                runStructure(districtSchoolStructureItems);
            });

            function runSanitation(districtSanitationItems) {
                var schoolSanitation = {
                    total_availables: []
                };
                /*
                *   sanitationHolder.push({
               name: "Toilet",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.toilet],
               year :sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Urinal",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.urinal],
               year :sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Water",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.water],
               year :sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Dust Bins",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.dust_bins],
               year : sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Veronica Buckets",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
               year :  sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
                * */

                var availabilityItems = ['toilet', 'urinal', 'water', 'dust_bins', 'veronica_buckets'];

                angular.forEach(districtSanitationItems, function (item, index) {

                    for (var i = 0; i < availabilityItems.length; i++) {
                        var statusProp = availabilityItems[i];

                        if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                            schoolSanitation.total_availables.push(statusProp)
                        }
                    }
                });

                $scope.schoolSanitation = angular.copy(schoolSanitation);

                $scope.sanitationItems = [];
                angular.forEach(districtSanitationItems, function(sanitationObject, index){

                    //An empty object that gets initialized every time the loop runs.
                    //It is used to hold the items in object being looped over for the purpose of display
                    var sanitationHolder =[];

                    //reformatting the way the objects are coming from the server tp be used in the table for display
                    sanitationHolder.push({
                        name: "Toilet",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.toilet],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Urinal",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.urinal],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Water",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.water],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Dust Bins",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.dust_bins],
                        year : sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Veronica Buckets",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
                        year :  sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });


                    //Merge the contents of the temp object to the scope variable that will display in the view
                    $scope.sanitationItems = $scope.sanitationItems.concat(sanitationHolder);
                });

            }

            if (scSchool.tracker.districtSanitationItems) {
                runSanitation(scSchool.tracker.districtSanitationItems)
            }
            $scope.$on('sanitationDataLoaded', function (event, districtSanitationItems) {
                scSchool.tracker.districtSanitationItems = districtSanitationItems;
                runSanitation(districtSanitationItems);
            });




            function runRecreation(districtRecreationItems) {
                var schoolRecreation = {
                    total_availables: []
                };
                /*
                *   recreationHolder.push({
               name: "Football",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.football],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Volleyball",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.volleyball],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Netball",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.netball],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Playing Field",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.playing_field],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Sports Wear",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.sports_wear],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           try{
               recreationHolder.push({
                   name: "Seesaw",
                   term :   recreationObject.term,
                   status : $scope.formatAvailability[recreationObject.seesaw],
                   year : recreationObject.year,
                   comment : recreationObject.comment,
                   data_collector_type : recreationObject.data_collector_type
               })
               recreationHolder.push({
                   name: "Merry-Go-Round",
                   term :   recreationObject.term,
                   status : $scope.formatAvailability[recreationObject.merry_go_round],
                   year : recreationObject.year,
                   comment : recreationObject.comment,
                   data_collector_type : recreationObject.data_collector_type
               });
           recreationHolder.push({
               name: "First Aid Box",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.first_aid_box],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });

                * */

                var availabilityItems = ['football', 'volleyball', 'netball', 'playing_field', 'sports_wear', 'seesaw', 'merry_go_round', 'first_aid_box'];

                angular.forEach(districtRecreationItems, function (item, index) {


                    for (var i = 0; i < availabilityItems.length; i++) {
                        var statusProp = availabilityItems[i];

                        if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                            schoolRecreation.total_availables.push(statusProp);
                        }
                    }
                });

                $scope.schoolRecreation = angular.copy(schoolRecreation);

                $scope.recreationItems = [];
                angular.forEach(districtRecreationItems, function(recreationObject,index){

                    var recreationHolder =[];
                    recreationHolder.push({
                        name: "Football",
                        term :   recreationObject.term,
                        formatted_term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.football],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Volleyball",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.volleyball],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Netball",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.netball],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Playing Field",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.playing_field],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Sports Wear",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.sports_wear],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    try{
                        recreationHolder.push({
                            name: "Seesaw",
                            term :   recreationObject.term,
                            status : $scope.formatAvailability[recreationObject.seesaw],
                            year : recreationObject.year,
                            comment : recreationObject.comment,
                            data_collector_type : recreationObject.data_collector_type
                        })
                    }catch(e){

                    }

                    try{
                        recreationHolder.push({
                            name: "Merry-Go-Round",
                            term :   recreationObject.term,
                            status : $scope.formatAvailability[recreationObject.merry_go_round],
                            year : recreationObject.year,
                            comment : recreationObject.comment,
                            data_collector_type : recreationObject.data_collector_type
                        });
                    }catch(e){

                    }


                    recreationHolder.push({
                        name: "First Aid Box",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.first_aid_box],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });

                    $scope.recreationItems = $scope.recreationItems.concat(recreationHolder);

                });

            }

            $scope.$on('recreationDataLoaded', function (event, districtRecreationItems) {
                scSchool.tracker.districtRecreationItems = districtRecreationItems;
                runRecreation(districtRecreationItems);
            });

            function runSecurity(districtSecurityItems) {
                var schoolSecurity = {
                    total_availables: []
                };
                var availabilityItems = ['security_man', 'lights', 'gated', 'walled'];

                angular.forEach(districtSecurityItems, function (item, index) {

                    for (var i = 0; i < availabilityItems.length; i++) {
                        var statusProp = availabilityItems[i];

                        if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                            schoolSecurity.total_availables.push(statusProp);
                        }
                    }
                });

                $scope.schoolSecurity = angular.copy(schoolSecurity);

                $scope.securityItems = [];
                angular.forEach(districtSecurityItems, function(securityObject , index){

                    var securityHolder =[];
//                var format_term = $scope.academicTermDisplay(securityObject.term);
                    securityHolder.push({
                        name: "Walled / Fenced",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.walled],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });

                    securityHolder.push({
                        name: "Gated",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.gated],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });
                    securityHolder.push({
                        name: "Lights",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.lights],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });
                    securityHolder.push({
                        name: "Security Man",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.security_man],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });

                    $scope.securityItems = $scope.securityItems.concat(securityHolder);

                });
            }

            $scope.$on('securityDataLoaded', function (event, districtSecurityItems) {
                runSecurity(districtSecurityItems);
            });

            function runMeetings(districtSchoolMeetingItems) {
                var schoolMeeting = {
                    total_frequency: 0
                };
                /*
                *
           meetingsHolder.push({
               name: "Staff Meeting",
               frequency: meetingObject.number_of_staff_meeting,
               males_present: meetingObject.num_males_present_at_staff_meeting,
               females_present: meetingObject.num_females_present_at_staff_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

           meetingsHolder.push({
               name: "SPAM",
               frequency: meetingObject.number_of_spam_meeting,
               males_present: meetingObject.num_males_present_at_spam_meeting,
               females_present: meetingObject.num_females_present_at_spam_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

           meetingsHolder.push({
               name: "PTA",
               frequency: meetingObject.number_of_pta_meeting,
               males_present: meetingObject.num_males_present_at_pta_meeting,
               females_present: meetingObject.num_females_present_at_pta_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

           meetingsHolder.push({
               name: "SMC",
               frequency: meetingObject.number_of_smc_meeting,
               males_present: meetingObject.num_males_present_at_smc_meeting,
               females_present: meetingObject.num_females_present_at_smc_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

                * */
                var frequencyItems = ['number_of_smc_meeting', 'number_of_pta_meeting', 'number_of_spam_meeting', 'number_of_staff_meeting'];

                angular.forEach(districtSchoolMeetingItems, function (item, index) {

                    for (var i = 0; i < frequencyItems.length; i++) {
                        var freqProp = frequencyItems[i];

                        if (isFinite(Number(item[freqProp])) && Number(item[freqProp]) > 0) {
                            schoolMeeting.total_frequency += Number(item[freqProp]);
                        }
                    }
                });

                $scope.schoolMeeting = angular.copy(schoolMeeting);
            }

            if (scSchool.tracker.districtSchoolMeetingItems) {
                runMeetings(scSchool.tracker.districtSchoolMeetingItems)
            }
            $scope.$on('meetingDataLoaded', function (event, districtSchoolMeetingItems) {
                scSchool.tracker.districtSchoolMeetingItems = districtSchoolMeetingItems;
                runMeetings(districtSchoolMeetingItems);
            });

            function runCommunity(districtCommunityItems) {
                var schoolCommInvolvement = {
                    total_goods: []
                };

                /*
                *       var format_term = $scope.academicTermDisplay(communityObject.term);
            communityHolder.push({
                name: "PTA Involvement",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.pta_involvement],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });
            communityHolder.push({
                name: "Parents Notified of Student Progress",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.parents_notified_of_student_progress],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });
            communityHolder.push({
                name: "Community Sensitization on School Attendance",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.community_sensitization_for_school_attendance],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });
            communityHolder.push({
                name: "School Management Committees",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.school_management_committees],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });

                * */
                var goodElems = ['school_management_committees', 'community_sensitization_for_school_attendance', 'parents_notified_of_student_progress', 'pta_involvement'];

                angular.forEach(districtCommunityItems, function (item, index) {
                    for (var i = 0; i < goodElems.length; i++) {
                        var statusProp = goodElems[i];

                        if (item[statusProp] === 'good') {
                            schoolCommInvolvement.total_goods.push(statusProp);
                        }
                    }
                });

                $scope.schoolCommInvolvement = angular.copy(schoolCommInvolvement);
            }

            if (scSchool.tracker.districtCommunityItems) {
                runCommunity(scSchool.tracker.districtCommunityItems)
            }
            $scope.$on('communityDataLoaded', function (event, districtCommunityItems) {
                scSchool.tracker.districtCommunityItems = districtCommunityItems;
                runCommunity(districtCommunityItems);
            });





            $scope.$watchGroup(['teachers_in_school', 'schoolEnrollmentTotals.total_pupils'], function(newVal, oldVal) {
                if (angular.isDefined(newVal[0]) && newVal[0].length > 0) {

                    if (angular.isDefined(newVal[1]) && newVal[1] > 0) {

                        $scope.pupilTeacherRatio = Math.round((newVal[1]) / (newVal[0].length))
                    }
                }
            });

            $scope.$watchGroup(['schoolAttendanceTotals', 'schoolEnrollmentTotals.total_pupils'], function(newVal, oldVal) {

                if ($scope.schoolAttendanceTotals && $scope.schoolEnrollmentTotals && $scope.schoolEnrollmentTotals.total_pupils) {

                    /*  multiply total enrolment by the number of days school was in session

                      divide the totalAttendance by expected attendance and multiply by 100*/
                    var expectedAttendance = $scope.schoolEnrollmentTotals.total_pupils * $scope.schoolAttendanceTotals.total_school_session;

                    $scope.schoolAttendanceTotals.attendance_ratio = Math.round(($scope.schoolAttendanceTotals.total_attendance_for_term / expectedAttendance) * 100) ;
                    // console.log('watch end: ', $scope.schoolAttendanceTotals);

                }


            });


            $scope.downloadPDF = function (){
                var pdfDocVariables;
                // pdfDocVariables = localStorage.getItem('pdfDocVariables');

                if (!pdfDocVariables) {
                    pdfDocVariables = {
                        title: 'mSRC School Report',
                        entity_name: $scope.selected_school.name + " (School)",
                        entity : $scope.selected_school,
                        upper_level_name: $scope.selected_school.districtName + " District",
                        term_in_scope: $scope.one_school_terms[$scope.filter_term].label,
                        // top_box_left : 'Number of Schools : ' + + $scope.selected_district.totalSchools,
                        top_box_left: '' + $scope.schoolEnrollmentTotals.total_pupils,
                        top_box_middle: '',
                        top_box_up_middle_up: '' +$scope.schoolEnrollmentTotals.total_male,
                        top_box_up_middle_down:  '' + $scope.schoolEnrollmentTotals.total_female,
                        top_box_right: 'Number of Teachers : ' + $scope.teachers_in_school.length,
                        top_box_down_left:  $scope.schoolAttendanceTotals.attendance_ratio + "%",
                        top_box_down_middle_up: '' + $scope.schoolSpecialEnrollmentTotals.total_male || 0,
                        top_box_down_middle_down:  '' + $scope.schoolSpecialEnrollmentTotals.total_female || 0,
                        percentage_trained_teachers: $scope.teachers_in_school.trained + '/' + $scope.teachers_in_school.length,
                        gh_lang_performance_array: $scope.schoolPerformanceTotals.gh_lang.class_score,
                        english_lang_performance_array: $scope.schoolPerformanceTotals.english.class_score,
                        maths_lang_performance_array: $scope.schoolPerformanceTotals.math.class_score,
                        teacher_pupil_ratio: ( $scope.pupilTeacherRatio + "  :  1") || '-',
                        teacher_attendance_ratio: ($scope.schoolTeacherAttendanceTotals.attendanceRatio + '%') || 0,
                        teacher_unit_covered_gh_lang: ($scope.schoolUnitCoveredTotals.gh_lang.total + '') || 0,
                        teacher_unit_covered_english: ($scope.schoolUnitCoveredTotals.english.total + '') || 0,
                        teacher_unit_covered_maths: ($scope.schoolUnitCoveredTotals.math.total + '') || 0,
                        punctuality_title: 'Teachers with',
                        teacher_punctuality: ($scope.schoolTeacherAttendanceTotals.total_above_50 + '/' + $scope.teachers_in_school.length) || 0,
                        adequate_marked_title: "Exercises",
                        adequately_marked_exercises: ($scope.schoolTeacherAttendanceTotals.num_of_exercises_marked || '0') + '/' + ($scope.schoolTeacherAttendanceTotals.num_of_exercises_given || '0'),
                        schools_with_or_number_of_title: "Number of",
                        schools_that_or_amount_of_title: "Amount of",
                        schools_that_received_or_any_type_of_support: "Any type of",

                        record_books: ($scope.schoolRecordBooks.books_available.length + '/11' ),
                        books : $scope.books,

                        grant_received: ("GHS " + $scope.schoolGrantStuff.totalAmount + '') || 0,
                        support_received: ("GHS " + $scope.schoolSupportStuff.total_cash + '') || 0,

                        adequate_furniture: ($scope.schoolFurniture.total_adequates.length + '/4') || 0,
                        furnitureItems : $scope.furnitureItems,

                        meetings_title: 'Number of ',
                        community_title: "    Active Community Participation",

                        good_structure: ($scope.schoolStructure.total_goods.length + '/6') || 0,
                        structureItems : $scope.structureItems,

                        sanitation_facilities: ($scope.schoolSanitation.total_availables.length + '/5') || 0,
                        sanitationItems : $scope.sanitationItems,

                        recreational_facilities: ($scope.schoolRecreation.total_availables.length + '/8') || 0,
                        recreationItems: $scope.recreationItems,

                        security_facilities: ($scope.schoolSecurity.total_availables.length + '/4') || 0,
                        securityItems : $scope.securityItems,

                        meetings: ($scope.schoolMeeting.total_frequency + '') || 0,
                        community_participation: ($scope.schoolCommInvolvement.total_goods.length + '/4') || 0,
                        pdf_export_name: $scope.selected_school.name + '_' + $scope.filter_term + '_school_report.pdf'
                    };
                }

                // localStorage.setItem('pdfDocVariables', pdfDocVariables);

                scReportGenerator.downloadPDF(pdfDocVariables)
            };
        }
    ]);