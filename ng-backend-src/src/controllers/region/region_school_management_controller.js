/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedRegionSchoolsRecordBooksController',  [
    '$rootScope', '$scope', '$state', '$stateParams', '$modal', 'school_years',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout','School',
    function($rootScope, $scope, $state, $stateParams, $modal, school_years,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout, School) {

        $scope.loadingRecordPerformanceData = false;

        function incrementYesNo(schoolObj, prop){
            if (prop != '' || prop != null) {
                schoolObj[prop]++;
                return schoolObj;
            }
            return schoolObj
        }

        $scope.fetchRecordPerformanceData = function(){
            if (!($scope.record_year && $scope.record_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }

            $scope.loadingRecordPerformanceData = true;
            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.record_year,
                term: $scope.record_term,
                region_id: $stateParams.id
            };
            $scope.recordPerformanceData = [];

            School.allRecordPerformanceData(data_to_post).$promise
                .then(
                function (fufilledData) {
                    fufilledData.forEach(function (record, index) {
                        var schoolObj = {
                            name : scSchool.schoolLookup[record.school_id].name,
                            school_id : record.school_id,
                            region_id : record.region_id,
                            yes : 0,
                            no : 0,
                            data : record
                        };
                        schoolObj = incrementYesNo(schoolObj, record.admission_register);
                        schoolObj = incrementYesNo(schoolObj, record.class_registers);
                        schoolObj = incrementYesNo(schoolObj, record.cummulative_records_books);
                        schoolObj = incrementYesNo(schoolObj, record.inventory_books);
                        schoolObj = incrementYesNo(schoolObj, record.log);
                        schoolObj = incrementYesNo(schoolObj, record.movement);
                        schoolObj = incrementYesNo(schoolObj, record.sba);
                        schoolObj = incrementYesNo(schoolObj, record.spip);
                        schoolObj = incrementYesNo(schoolObj, record.visitors);
                        schoolObj = incrementYesNo(schoolObj, record.teacher_attendance_register);
                        schoolObj = incrementYesNo(schoolObj, record.continous_assessment);
                        $scope.recordPerformanceData.push(schoolObj);
                    });
                    $scope.loadingRecordPerformanceData = false;
                },
                function (rejectedData) {
                    $scope.loadingRecordPerformanceData = false;

                }
            )
        };

    }]);

schoolMonitor.controller('scViewSelectedRegionSchoolsSupportTypesController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingSupportTypesData = false;

        function addCashAmount(schoolObj, amount){
            if (!(amount == '' || amount == null || amount == undefined)) {
                if (typeof parseInt(amount) == 'number' ) {
                    schoolObj.cash = parseInt(schoolObj.cash) + parseInt(amount);
                }
            }
            return schoolObj
        }
        function checkForKindDonation(schoolObj, donation){
            if (!(donation == '' || donation == null || donation == undefined || donation.toLowerCase()  == 'no' ||
                donation.toLowerCase()  == 'non' || donation.toLowerCase()  == 'none'  || donation.length < 3)) {
                schoolObj.kind = "yes";
                return schoolObj;
            }
            return schoolObj
        }
        $scope.fetchSupportTypesData = function(){
            if (!($scope.support_year && $scope.support_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingSupportTypesData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.support_year,
                term: $scope.support_term,
                region_id: $stateParams.id
            };
            $scope.supportTypesData = [];
            /*
             * school_support_type: Array[2]
             region_id: 1
             comment: ""

             country_id: 1
             created_at: "2015-02-03 14:26:20"
             data_collector_id: 5
             data_collector_type: "head_teacher"
             region_id: 1
             id: 2
             lat: "0"
             long: "0"
             questions_category_reference_code: "ST"
             region_id: 1
             school_id: 2
             school_reference_code: "u3BoEVL9q5s2gk5h"
             term: "first_term"
             updated_at: "2015-02-03 14:26:20"
             year: "2014/2015"
             */


            School.allSupportTypes(data_to_post).$promise
                .then(
                function (fufilledData) {
                    fufilledData[0].school_support_type.forEach(function (support, index) {
                        var schoolObj = {
                            name : scSchool.schoolLookup[support.school_id].name,
                            school_id : support.school_id,
                            region_id : support.region_id,
                            cash : 0,
                            kind : "no",
                            data : support
                        };

                        schoolObj = addCashAmount(schoolObj, $.trim(support.community_support_in_cash));
                        schoolObj = addCashAmount(schoolObj, $.trim(support.district_support_in_cash));
                        schoolObj = addCashAmount(schoolObj, $.trim(support.donor_support_in_cash));
                        schoolObj = addCashAmount(schoolObj, $.trim(support.other_support_in_cash));
                        schoolObj = addCashAmount(schoolObj, $.trim(support.pta_support_in_cash));

                        schoolObj = checkForKindDonation(schoolObj, $.trim(support.community_support_in_kind));
                        schoolObj = checkForKindDonation(schoolObj, $.trim(support.district_support_in_kind));
                        schoolObj = checkForKindDonation(schoolObj, $.trim(support.donor_support_in_kind));
                        schoolObj = checkForKindDonation(schoolObj, $.trim(support.other_support_in_kind));
                        schoolObj = checkForKindDonation(schoolObj, $.trim(support.pta_support_in_kind));

                        $scope.supportTypesData.push(schoolObj);
                    });
                    $scope.loadingSupportTypesData = false;

                },
                function (rejectedData) {
                    $scope.loadingSupportTypesData = false;

                }
            )
        };
    }]);

schoolMonitor.controller('scViewSelectedRegionSchoolsGrantCapitationPaymentsController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingGrantCapitationData = false;

        function addGrantCashAmount(schoolObj, grant_type, amount){
            if (!(amount == '' || amount == null || amount.length < 1)) {
                schoolObj [grant_type] =parseInt(schoolObj[ grant_type ]) + parseInt(amount);
                return schoolObj;
            }
            return schoolObj
        }

        $scope.fetchSchoolGrantCapitationData = function(){
            if (!($scope.grant_year && $scope.grant_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingGrantCapitationData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.grant_year,
                term: $scope.grant_term,
                region_id: $stateParams.id
            };
            $scope.grantCapitationData = [];
            /*school_grant: [{id: 1, school_reference_code: "R40rFdm3yKfpx7BJ", questions_category_reference_code: "SG",�},�]
             0: {id: 1, school_reference_code: "R40rFdm3yKfpx7BJ", questions_category_reference_code: "SG",�}
             region_id: 1
             comment: "null"
             country_id: 1
             created_at: "2015-01-28 14:01:31"
             data_collector_id: 2
             data_collector_type: "head_teacher"
             region_id: 1
             id: 1
             lat: "5.6453223713191"
             long: "-0.15156506471074"
             questions_category_reference_code: "SG"
             region_id: 1
             school_id: 1
             school_reference_code: "R40rFdm3yKfpx7BJ"
             term: "first_term"
             school_grant_type: "capitation"
             first_tranche_amount: ""
             first_tranche_date: "1/Jan/2014"
             second_tranche_amount: "0"
             second_tranche_date: "Not submitted yet"
             third_tranche_amount: "0"
             third_tranche_date: "Not submitted yet, hence no date"
             updated_at: "2015-01-28 14:01:31"
             year: "2014/2015"*/
            School.allSchoolGrantCapitationPayments(data_to_post).$promise
                .then(function (fufilledData) {
                    fufilledData[0].school_grant.forEach(function (grant, index) {
                        var school = scSchool.schoolLookup[grant.school_id];
                        if (school != undefined) {
                            school = school.name
                        }else{
                            school = "Not available (deleted)"
                        }
                        var schoolObj = {
                            name : school,
                            school_id : grant.school_id,
                            region_id : grant.region_id,
                            capitation : 0,
                            school_grant : 0,
                            data : grant
                        };
                        schoolObj= addGrantCashAmount(schoolObj, $.trim(grant.school_grant_type), grant.first_tranche_amount);
                        schoolObj = addGrantCashAmount(schoolObj, $.trim(grant.school_grant_type), grant.second_tranche_amount);
                        schoolObj = addGrantCashAmount(schoolObj, $.trim(grant.school_grant_type), grant.third_tranche_amount);

                        $scope.grantCapitationData.push(schoolObj);
                    });
                    $scope.loadingGrantCapitationData = false;

                },
                function (rejectedData) {
                    $scope.loadingGrantCapitationData = false;

                }
            )
        };
    }]);