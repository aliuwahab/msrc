/**
 * Created by Kaygee on 23/04/2015.
 */


schoolMonitor.controller('scViewSelectedRegionTeachersController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'District', 'DataCollectorHolder', 'Report', 'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', '$interval',
    function($rootScope, $scope, $state, $stateParams, District, DataCollectorHolder, Report,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval) {

        function  prepRegionTeachersInfo(){
            $scope.regionTeachers = $scope.teacherSummaryData.teachersInfo;

            $scope.selected_region = scRegion.regionLookup[$stateParams.id];

          for (var i = 0; i < $scope.regionTeachers.length; i++){
              var teacher = $scope.regionTeachers[i];
                if ( $scope.selected_region.allSchools[teacher.school_id] != undefined) {
                    $scope.selected_region.allSchools[teacher.school_id].totalTeachers ++;

                    /*assign a name property for filtering*/
                    teacher.full_name = teacher.first_name + teacher.last_name;

                    if (teacher.gender == 'male') {
                        $scope.selected_region.allSchools[teacher.school_id].totalMaleTeachers ++;
                    }else{
                        $scope.selected_region.allSchools[teacher.school_id].totalFemaleTeachers ++;
                    }
                }
            }

            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'list' : true,
                'summary' : false
            };

            $scope.compare_view_to_show = {
                'chart' : true,
                'table' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;

        }

        /*if the teachers are still being loaded, navigate to the parent page*/
        if ($scope.loadingTeachers) {
            $state.go('dashboard.view_selected_region');
        }

        //This function changes between displaying the list of teachers and the grouped table
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };

        //This function changes between displaying the table or charts for comparing regions
        $scope.changeCompareView = function(view_to_show){
            angular.forEach($scope.compare_view_to_show, function(val, prop){
                //set all view to false
                $scope.compare_view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.compare_view_to_show[view_to_show] = true;
        };

        $scope.$on('runChildrenStatesSummary', function () {
            prepRegionTeachersInfo()
        });

        if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scRegion.regions && !_.isEmpty(scRegion.regionLookup)) {
            prepRegionTeachersInfo()
        }

        angular.forEach($scope.teacherSummaryData.teachersInfo, function (item, key) {
            if (item.class_taught == null || item.class_taught == 'null') item.class_taught = '';
            if (item.class_subject_taught == null ||item.class_subject_taught == 'null') item.class_subject_taught = '';

            item.subject = ($.trim(item.class_subject_taught +  item.class_taught)).split(',')[0];

            if (item.gender == 'male') {
                $scope.teacherSummaryData.regionInfo.male_total ++;
            }else $scope.teacherSummaryData.regionInfo.female_total++;

            $scope.teacherSummaryData.regionInfo.teachers_total ++;
        });

        //This variable is for a single district rating's ng-show
        $scope.regionAverage = true;
        //This variable is for a comparing regional  district rating's ng-show
        $scope.compareNationalRegionAverages = false;

        //Shuffle between a single district rating and region districts
        $scope.compareAverages =function (){
            $scope.regionAverage = false;
            $scope.compareNationalRegionAverages = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scRegion.regions, 'name',
                    'Regions in Ghana ', 'countrysRegionRatingBarChart', 'ht_average');
                chart.validateNow();
            }, 100, 1)
        };

        $scope.singleAverage = function(){
            $scope.regionAverage = true;
            $scope.compareNationalRegionAverages = false;
        };

        function processTeacherAttendanceAverage(loadedWeeklyData){

            $scope.teacherAttendanceSummaryData = {
                attendanceAverageInfo : loadedWeeklyData,
                regionInfo : {}
            };

            var region_totals = {
                average_teacher_attendance : {
                    head_teacher : 0,
                    circuit_supervisor : 0
                },
                average_teacher_attendance_holder : {
                    head_teacher : 0,
                    circuit_supervisor : 0
                },
            };

            angular.forEach(loadedWeeklyData, function (value, key) {

                region_totals.average_teacher_attendance_holder[value.data_collector_type] =
                    parseFloat(region_totals.average_teacher_attendance_holder[value.data_collector_type]) + parseFloat(value.average_teacher_attendance);

                region_totals.average_teacher_attendance[value.data_collector_type] =  parseFloat(region_totals.average_teacher_attendance_holder[value.data_collector_type]) / parseInt($scope.teacherSummaryData.teachersInfo.length);

                scSchool.schoolLookup[value.school_id].average_teacher_attendance[value.data_collector_type] = value.average_teacher_attendance;
            });

            $scope.teacherAttendanceSummaryData.regionInfo = region_totals;
        }


        $scope.fetchAttendanceAverageSummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;
            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                week_number : $scope.summary_week,
                region_id : $stateParams.id
            };
            Report.totalsWeekly(data_to_post).$promise
                .then(
                /*success function*/
                function(loadedWeeklyData){
                    processTeacherAttendanceAverage(loadedWeeklyData);
                },

                /*error function*/
                function(){

                })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
        };

    }]);