/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedRegionSchoolsStatsWeeklyController',  [
    '$rootScope', '$scope', '$state', '$stateParams','District', '$modal', 'DataCollectorHolder', 'Report',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval',
    function($rootScope, $scope, $state, $stateParams, District, $modal, DataCollectorHolder, Report,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval) {

        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };

        function processEnrollmentSummary(loadedWeeklyData){

            $scope.enrollmentSummaryData = {
                schoolInfo : loadedWeeklyData,
                regionInfo : {}
            };

            var region_totals = {
                head_teacher : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                },
                circuit_supervisor : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                }
            };

            angular.forEach(loadedWeeklyData, function (value, key) {
                region_totals[value.data_collector_type].boys =  parseInt(region_totals[value.data_collector_type].boys) + parseInt(value.normal_enrolment_total_boys);
                region_totals[value.data_collector_type].special_boys =  parseInt(region_totals[value.data_collector_type].special_boys) + parseInt(value.special_enrolment_total_boys);
                region_totals[value.data_collector_type].boys_total =  parseInt(region_totals[value.data_collector_type].special_boys) + parseInt(region_totals[value.data_collector_type].boys);

                region_totals[value.data_collector_type].girls =  parseInt(region_totals[value.data_collector_type].girls) + parseInt(value.normal_enrolment_total_girls);
                region_totals[value.data_collector_type].special_girls =  parseInt(region_totals[value.data_collector_type].special_girls) + parseInt(value.special_enrolment_total_girls);
                region_totals[value.data_collector_type].girls_total =  parseInt(region_totals[value.data_collector_type].special_girls) + parseInt(region_totals[value.data_collector_type].girls);

                region_totals[value.data_collector_type].all_total =   parseInt(region_totals[value.data_collector_type].boys_total) + parseInt(region_totals[value.data_collector_type].girls_total);
                region_totals[value.data_collector_type].teachers_total =   parseInt(region_totals[value.data_collector_type].teachers_total) + parseInt(value.current_number_of_teachers);

            });

            $scope.enrollmentSummaryData.regionInfo = region_totals;


        }

        function processAttendanceSummary(loadedWeeklyData){

            $scope.attendanceSummaryData = {
                schoolInfo : loadedWeeklyData,
                regionInfo : {}
            };

            var region_totals = {
                head_teacher : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                },
                circuit_supervisor : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                }
            };

            angular.forEach(loadedWeeklyData, function (value, key) {
                region_totals[value.data_collector_type].boys =  parseInt(region_totals[value.data_collector_type].boys) + parseInt(value.normal_attendance_total_boys);
                region_totals[value.data_collector_type].special_boys =  parseInt(region_totals[value.data_collector_type].special_boys) + parseInt(value.special_attendance_total_boys);
                region_totals[value.data_collector_type].boys_total =  parseInt(region_totals[value.data_collector_type].special_boys) + parseInt(region_totals[value.data_collector_type].boys);

                region_totals[value.data_collector_type].girls =  parseInt(region_totals[value.data_collector_type].girls) + parseInt(value.normal_attendance_total_girls);
                region_totals[value.data_collector_type].special_girls =  parseInt(region_totals[value.data_collector_type].special_girls) + parseInt(value.special_attendance_total_girls);
                region_totals[value.data_collector_type].girls_total =  parseInt(region_totals[value.data_collector_type].special_girls) + parseInt(region_totals[value.data_collector_type].girls);

                region_totals[value.data_collector_type].all_total =   parseInt(region_totals[value.data_collector_type].boys_total) + parseInt(region_totals[value.data_collector_type].girls_total);
                region_totals[value.data_collector_type].teachers_total =   parseInt(region_totals[value.data_collector_type].teachers_total) + parseInt(value.current_number_of_teachers);
            });

            $scope.attendanceSummaryData.regionInfo = region_totals;


        }

        $scope.fetchSummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;
            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                week_number : $scope.summary_week,
                region_id : $stateParams.id
            };
            Report.totalsWeekly(data_to_post).$promise
                .then(
                /*success function*/
                function(loadedWeeklyData){
                    $('.showing_weekly_year').text($scope.summary_year);
                    $('.showing_weekly_term').text( $scope.one_school_terms[$scope.summary_term].label );
                    $('.showing_weekly_week').text($scope.summary_week);
                    processEnrollmentSummary(loadedWeeklyData);
                    processAttendanceSummary(loadedWeeklyData)
                },

                /*error function*/
                function(){

                })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
        };

        function prepRegionSchoolStats() {
            //This variable toggle to display the loading gif if the summary tab
            $scope.loadingSummary= false;
            //A variable to check if data has been loaded before, so we change the instruction on the summary tab
            $scope.initialSummaryInfoCount = 0;

            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'enrollment' : true,
                'attendance' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepRegionSchoolStats()
        });

        if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scRegion.regions && !_.isEmpty(scRegion.regionLookup)) {
            prepRegionSchoolStats()
        }

    }]);