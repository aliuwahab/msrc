/**
 * Created by Kaygee on 23/04/2015.
 */
schoolMonitor.controller('scViewSelectedRegionRatingController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'District', '$modal', 'DataCollectorHolder',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, District, $modal, DataCollectorHolder,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {


        //Shuffle between a single regions rating and country's' regions
        $scope.compareRatings =function (){
            $scope.view_to_show = {
                'chart' : true,
                'table' : false
            };
            $scope.regionRating = false;
            $scope.compareNationalRegionRating = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scRegion.regions, 'name',
                    'Regions in Ghana', 'countrysRegionRatingBarChart');
                chart.validateNow();
            }, 100, 1)
        };

        //Shuffle between a single region rating and country's regions
        $scope.singleRating = function(){
            $scope.regionRating = true;
            $scope.compareNationalRegionRating = false;
        };

        //An object to easily display between table and graph views under comparing regions
        $scope.view_to_show = {
            'chart' : true,
            'table' : false
        };
        //This function changes between displaying the table or charts for comparing regions
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                $scope.view_to_show[prop] = false;
            });
            $scope.view_to_show[view_to_show] = true
        };

        function prepRegionRating() {
            $scope.selected_region = scRegion.regionLookup[$stateParams.id];

            //This variable is for a single district rating's ng-show
            $scope.regionRating = true;
            //This variable is for a comparing regional  district rating's ng-show
            $scope.compareNationalRegionRating = false;


            //Prepare the district rating bar chart
            $scope.tempArray =[{
                field : 'Inclusiveness',
                value : $scope.selected_region.inclusiveness,
                remaining : 10 - $scope.selected_region.inclusiveness
            },{
                field : 'Effective Teaching and Learning',
                value : $scope.selected_region.effective_teaching_learning,
                remaining : 10 - $scope.selected_region.effective_teaching_learning
            },{
                field : 'Healthy',
                value : $scope.selected_region.healthy,
                remaining : 10 - $scope.selected_region.healthy
            },{
                field : 'Gender Friendliness',
                value : $scope.selected_region.gender_friendly,
                remaining : 10 - $scope.selected_region.gender_friendly
            },{
                field : 'Safety and Protection',
                value : $scope.selected_region.safe_protective,
                remaining : 10 - $scope.selected_region.safe_protective
            },{
                field : 'Community Involvement',
                value : $scope.selected_region.community_involvement,
                remaining : 10 - $scope.selected_region.community_involvement
            }];

            $timeout(function () {
                $scope.changeBarChart($scope.tempArray, $scope.selected_region.name + ' Region Rating', 'regionRatingBarChart', 'rating');
            }, 1000, 3);


            // Assign the country's regions to a scope variable
            $scope.regionChartData = scRegion.regions;
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepRegionRating()
        });

        if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scRegion.regions && !_.isEmpty(scRegion.regionLookup)) {
            prepRegionRating()
        }

    }]);