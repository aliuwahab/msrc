/**
 * Created by Kaygee on 14/07/2014.
 */
schoolMonitor.controller('scViewSelectedRegionController',['$rootScope', '$scope', '$state' ,
    '$stateParams', 'scRegion', '$modal', '$log','SMSService', 'scNotifier',
    'scDistrict','scCircuit', 'scSchool', '$timeout', 'RecentSubmissionService', 'Timelines', 'DataCollectorHolder',
    function ($rootScope, $scope, $state, $stateParams, scRegion, $modal, $log,
              SMSService, scNotifier,  scDistrict, scCircuit, scSchool, $timeout, RecentSubmissionService, Timelines, DataCollectorHolder) {

        $scope.$parent.changePageTitle('View', 'Region', 'View');

        function prepRegion() {
            $timeout(function () {
                //Assign the selected region to the scope variable
                $scope.selected_region = scRegion.regionLookup[$stateParams.id];

                //This helps the DataCollectors Tab to show only the filtered data collectors
                $scope.siftedDataCollectors = {
                    all : scRegion.regionLookup[$stateParams.id].circuit_supervisors_holder.concat(scRegion.regionLookup[$stateParams.id].head_teachers_holder),
                    supervisor : scRegion.regionLookup[$stateParams.id].circuit_supervisors_holder,
                    headteacher : scRegion.regionLookup[$stateParams.id].head_teachers_holder
                };

                $scope.showDataCollectors = function(type){
                    //Remove all the multiple checked boxes
                    $('input:checked').attr('checked', false);

                    //reset the datacollectors array
                    $scope.dataCollectors_to_send_messages_to = [];
                    //change the datacollectors being displayed by type
                    if ($scope.siftedDataCollectors) {
                        $scope.regionDataCollectors = $scope.siftedDataCollectors[type];
                    }
                };

                $scope.regionDataCollectors =  $scope.siftedDataCollectors['all'];

                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;
            }, 10);



            $scope.$broadcast('runChildrenStatesSummary');
        }

        function checkRegionsAreLoadAndPrepRegions() {
            if (!(_.isEmpty(scRegion.regionLookup)) && angular.isDefined(scRegion.regionLookup[$stateParams.id])) {
                prepRegion();
            } else {
                $timeout(function () {
                    checkRegionsAreLoadAndPrepRegions();
                }, 2500)
            }
        }

        $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            if (data.counter == 6) {
                checkRegionsAreLoadAndPrepRegions();
            }
        });

        /*starts from here*/
        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scRegion.regions.length && scRegion.regionLookup) {
            checkRegionsAreLoadAndPrepRegions();
        }

        $scope.average_teacher_attendance = 0;

        //Get the extension of the region's position, ie, 1st, 2nd, 3rd, 4th
        //$scope.regionPositionExtension = $scope.positionExtension($scope.selected_region.position);

        //This variable shows the checkbox to select multiple data collectors to send to
        $scope.selectMultiple = false;

        //This variable holds data collectors to receive a message
        $scope.dataCollectors_to_send_messages_to = [];

        //This function checks whether a data collector is already added
        $scope.addRemoveCollector = function(id_of_checked){
            var index_of_personnel = $scope.dataCollectors_to_send_messages_to.indexOf(id_of_checked);
            if (index_of_personnel < 0) {
                $scope.dataCollectors_to_send_messages_to.push(id_of_checked);
            }else{
//                array.splice(index,howmany,item_to_add_1,.....,item_to_add_X)
                $scope.dataCollectors_to_send_messages_to.splice(index_of_personnel, 1);
            }
        };



        RecentSubmissionService.getAllRecentSubmissions('region', $stateParams.id)
            .then(function (regionRecentSubmissions) {
                //This is the recently submitted data from the region
                $scope.regionRecentSubmissions = regionRecentSubmissions;


                //This controls the pagination of the recent submissions
                $scope.totalItems = regionRecentSubmissions.length;
                $scope.currentPage = 1;
                $scope.maxSize = 3;
                $scope.nextPage = 0;

                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };

                //array.slice(start,end)
                /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
                 * It does not include the element at that index*/

                $scope.pageChanged = function() {
                    $scope.nextPage = ($scope.maxSize  * $scope.currentPage) - $scope.maxSize;
                    $scope.regionRecentSubmissions = regionRecentSubmissions.slice($scope.nextPage);
                };
            });


        Timelines.getAvailableTimeline({category : 'normal_students_enrolment', region_id : $stateParams.id})
            .then(function (availableTimelines) {

                $scope.regionTerms = [];
                $scope.regionYears = [];
                $scope.regionWeeks = [];
                for (var i = 0; i < availableTimelines.data.length; i++) {
                    var item = availableTimelines.data[i];
                    //save the current term in the loop to a variable
                    //one school term is an object that formats the format returned
                    // from the server to a user friendlier one
                    var currentTerm = $scope.one_school_terms[item.term];

                    //save the current year in the loop to a variable
                    var currentYear = item.year;

                    //save the current week in the loop to a variable
                    var currentWeek = item.week_number;

                    // check if the current term isn't already added to the scope variable that displays the available terms
                    //in the submitted data
                    if ($scope.regionTerms.indexOf(currentTerm) < 0) {
                        $scope.regionTerms.push(currentTerm);

                        $scope.regionTerms.sort(function(a, b){
                            return a.number.charAt(0) - b.number.charAt(0)
                        });
                    }

                    // check if the current year isn't already added to the scope variable that displays the available years
                    //in the submitted data
                    if ($scope.regionYears.indexOf(currentYear) < 0) {
                        $scope.regionYears.push(currentYear);
                    }

                    // check if the current week isn't already added to the scope variable that displays the available weeks
                    //in the submitted data
                    if ($scope.regionWeeks.indexOf(currentWeek) < 0) {
                        $scope.regionWeeks.push(currentWeek);

                        $scope.regionWeeks.sort(function(a, b){
                            return a - b
                        });
                    }

                    $scope.summary_term =  item.term;

                    $scope.summary_week = item.week_number;

                    $scope.summary_year =  item.year;

                    $scope.summary_data_collector_type = item.data_collector_type || "head_teacher";

                }

            });


        $scope.loadingTeachers = true;
        scSchool.allSchoolTeachersData({region_id : $stateParams.id})
            .success(function (allSchoolTeachersData) {
                $timeout(function () {
                    $scope.teacherSummaryData = {
                        teachersInfo : allSchoolTeachersData[0].school_teachers,
                        regionInfo : {
                            teachers_total : 0,
                            male_total : 0,
                            female_total : 0,
                            averageTeacherAttendance : $scope.average_teacher_attendance,
                            trained : 0,
                            atpost : 0,
                            studyleave : 0,
                            otherleave : 0,
                            retired : 0,
                            deceased : 0,
                            percentage_trained : 0
                        }
                    };

                    var schoolTeacherHolder = {
                        teachersInSchoolHolder : []
                    };

                    /*calculate totals with a variable. using the school directly, increases it on each page load*/
                    var schoolTeacherTotalTracker = {};

                    angular.forEach($scope.teacherSummaryData.teachersInfo, function (teacher, key) {
                        if (teacher.class_taught == null || teacher.class_taught == 'null') teacher.class_taught = '';
                        if (teacher.class_subject_taught == null ||teacher.class_subject_taught == 'null') teacher.class_subject_taught = '';

                        teacher.subject = ($.trim(teacher.class_subject_taught +  teacher.class_taught)).split(',')[0];

                        /*whole region totals*/
                        if (teacher.gender == 'male') {
                            $scope.teacherSummaryData.regionInfo.male_total ++;
                        }else $scope.teacherSummaryData.regionInfo.female_total++;

                        /*assign a name property for filtering*/
                        teacher.full_name = teacher.first_name + teacher.last_name;

                        $scope.teacherSummaryData.regionInfo.teachers_total ++;
                        $scope.teacherSummaryData.regionInfo[teacher.teacher_status]++;


                        /*Calculate for professional teachers*/
                        // Professional teachers are those with professional qualifications

                        if (angular.isUndefined(teacher.highest_professional_qualification)  || teacher.highest_professional_qualification === '' || teacher.highest_professional_qualification.toLowerCase() === 'others' || teacher.highest_professional_qualification.toLowerCase() === 'other') {

                        }else{
                            $scope.teacherSummaryData.regionInfo.trained ++;
                            if (isFinite(Number($scope.teacherSummaryData.regionInfo.trained / $scope.teacherSummaryData.regionInfo.teachers_total))) {
                                $scope.teacherSummaryData.regionInfo.percentage_trained = parseInt(($scope.teacherSummaryData.regionInfo.trained / $scope.teacherSummaryData.regionInfo.teachers_total) * 100);
                            }
                        }


                        /*per school totals*/
                        if (schoolTeacherTotalTracker[teacher.school_id] !== undefined) {
                            schoolTeacherTotalTracker[teacher.school_id].totalTeachers ++;
                            schoolTeacherTotalTracker[teacher.school_id].totalMaleTeachers ++;
                            schoolTeacherTotalTracker[teacher.school_id].totalFemaleTeachers ++;

                            if ($scope.selected_region.allSchools[teacher.school_id] && $scope.selected_region.allSchools[teacher.school_id] !== undefined) {
                                if (teacher.gender === 'male') {
                                    $scope.selected_region.allSchools[teacher.school_id].totalMaleTeachers ++;
                                }else{
                                    $scope.selected_region.allSchools[teacher.school_id].totalFemaleTeachers ++;
                                }
                            }
                        }else{
                            schoolTeacherTotalTracker[teacher.school_id] ={
                                totalTeachers : 1,
                                school_id : teacher.school_id
                            };

                            if (teacher.gender === 'male') {
                                schoolTeacherTotalTracker[teacher.school_id].totalMaleTeachers = 1;
                            }else{
                                schoolTeacherTotalTracker[teacher.school_id].totalFemaleTeachers = 1;
                            }
                        }

                        if (schoolTeacherHolder[teacher.school_id] !== undefined) {
                            schoolTeacherHolder[teacher.school_id].teachersInSchoolHolder.push(teacher);
                        }else{
                            schoolTeacherHolder[teacher.school_id] = {
                                teachersInSchoolHolder : []
                            };
                            schoolTeacherHolder[teacher.school_id].teachersInSchoolHolder.push(teacher);
                        }

                    });

                    $scope.loadingTeachers = false;
                }, 1);

            });

    }]);