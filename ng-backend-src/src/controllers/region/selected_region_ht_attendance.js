/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedRegionHTAttendanceController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'District', '$modal', 'DataCollectorHolder',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, District, $modal, DataCollectorHolder,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {


        //Shuffle between a single region rating and all region
        $scope.compareAverages =function (){
            $scope.regionAverage = false;
            $scope.compareNationalRegionAverages = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scRegion.regions, 'name',
                    'Regions in Ghana ' , 'nationalRegionAverageBarChart', 'ht_average');
                chart.validateNow();
            }, 100, 1)
        };


        //Shuffle between a single region rating and all regions
        $scope.singleAverage = function(){
            $scope.regionAverage = true;
            $scope.compareNationalRegionAverages = false;
        };

        //An object to easily display between table and graph views under comparing regions
        $scope.view_to_show = {
            'chart' : true,
            'table' : false
        };

        //This function changes between displaying the table or charts for comparing regions
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                $scope.view_to_show[prop] = false;
            });
            $scope.view_to_show[view_to_show] = true;
            if ( $scope.compareNationalRegionAverages) {
                $scope.compareAverages();
            }
        };



        function prepRegionHTTeachersInfo() {
            $scope.selected_region = scRegion.regionLookup[$stateParams.id];

            //This variable is for a single region rating's ng-show
            $scope.regionAverage = true;
            //This variable is for a comparing regional  region rating's ng-show
            $scope.compareRegionDistrictAverages = false;

            //Format the data to send to amCharts to 'field' and 'value'
            $scope.tempArray = [];

            //loop through schools of this region and assign the teacher attendance average
            // into objects and shove it into the temp array for display
            angular.forEach($scope.selected_region.allSchoolsHolder, function(school, index){
                var remainder = $scope.selected_region.headteacherAttendanceAverage - school.current_teacher_attendance_by_headteacher;
                var obj = {
                    field: school.name,
                    value: school.current_teacher_attendance_by_headteacher,
//                remaining : Math.abs(remainder)
                    remaining : remainder
                };

                $scope.tempArray.push(obj);
            });

            $timeout(function () {
                $scope.changeBarChart($scope.tempArray, $scope.selected_region.name + ' Teacher Attendance Average by Head Teachers',
                    'regionHtAttendanceBarChart', 'ht_average', $scope.selected_region.headteacherAttendanceAverage);
            }, 200, 4);


            // Assign the regions to a scope variable
            $scope.regionChartData = scRegion.regions;

        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepRegionHTTeachersInfo()
        });

        if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scRegion.regions && !_.isEmpty(scRegion.regionLookup)) {
            prepRegionHTTeachersInfo()
        }

    }]);