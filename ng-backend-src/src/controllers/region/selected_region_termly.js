//Created by Kaygee on 17/06/14.
schoolMonitor.controller('scViewSelectedRegion' +
    'SchoolsStatsTermlyController',  [
    '$rootScope', '$scope', '$state', '$stateParams','District', '$modal', 'DataCollectorHolder', 'Report',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval',
    function($rootScope, $scope, $state, $stateParams, District, $modal, DataCollectorHolder, Report,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval) {

        function loadTermlyData(termlyRegionData) {
            $scope.termlyRegionData = {
                schoolInfo : termlyRegionData,
                regionInfo : {}
            };
            var region_totals = {
                average_english_score : 0,
                average_english_score_holder : 0,

                average_ghanaian_language_score: 0,
                average_ghanaian_language_score_holder: 0,

                average_maths_score: 0,
                average_maths_score_holder: 0,

                scoring_english_above_average: 0,
                scoring_ghanaian_language_above_average: 0,
                scoring_maths_above_average: 0,

                num_english_books: 0,
                num_ghanaian_language_books: 0,
                num_maths_books: 0

            };

            angular.forEach(termlyRegionData, function (value, key) {
                region_totals.average_english_score_holder = parseFloat(region_totals.average_english_score_holder) + parseFloat(value.average_english_score);
                region_totals.average_english_score =  parseFloat(region_totals.average_english_score_holder) / $scope.selected_region.totalSchools;

                region_totals.average_ghanaian_language_score_holder = parseFloat(region_totals.average_ghanaian_language_score_holder) + parseFloat(value.average_ghanaian_language_score);
                region_totals.average_ghanaian_language_score =  parseFloat(region_totals.average_ghanaian_language_score_holder)  / $scope.selected_region.totalSchools;

                region_totals.average_maths_score_holder = parseFloat(region_totals.average_maths_score_holder) + parseFloat(value.average_maths_score);
                region_totals.average_maths_score =  parseFloat(region_totals.average_maths_score_holder) / $scope.selected_region.totalSchools;


                region_totals.scoring_english_above_average =  parseInt(region_totals.scoring_english_above_average) + parseInt(value.english_above_average);

                region_totals.scoring_ghanaian_language_above_average =  parseInt(region_totals.scoring_ghanaian_language_above_average) + parseInt(value.ghanaian_language_above_average);

                region_totals.scoring_maths_above_average =  parseInt(region_totals.scoring_maths_above_average) + parseInt(value.maths_above_average);


                region_totals.num_english_books =  parseInt(region_totals.num_english_books) + parseInt(value.num_english_books);

                region_totals.num_ghanaian_language_books =  parseInt(region_totals.num_ghanaian_language_books) + parseInt(value.num_ghanaian_language_books);

                region_totals.num_maths_books =  parseInt(region_totals.num_maths_books) + parseInt(region_totals.num_maths_books);


            });

            $scope.termlyRegionData.regionInfo = region_totals
        }

        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };


        $scope.fetchTermlySummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;
            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                region_id : $stateParams.id
            };
            scRegion.fetchTermlyData(data_to_post)
                .then(
                /*success function*/
                function(loadedWeeklyData){
                    loadTermlyData(loadedWeeklyData);
                },

                /*error function*/
                function(){

                })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
        };

        function prepRegionSchoolTermlyStats() {
            //This variable toggle to display the loading gif if the summary tab
            $scope.loadingSummary= false;
            //A variable to check if data has been loaded before, so we change the instruction on the summary tab
            $scope.initialSummaryInfoCount = 0;

            //Assign the selected region to the scope variable
            $scope.selected_region = scRegion.regionLookup[$stateParams.id];

            //reset the allSchools attendance and enrollment arrays so that it prevent duplicates when switching between pages
            angular.forEach($scope.selected_region.allSchools, function(school, index){
                school.termlyDataHolder = [];
            });

            $scope.schoolYears_Termly = [];
            $scope.schoolTerms_Termly = [];

            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'display_average' : true,
                'display_textbooks' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepRegionSchoolTermlyStats()
        });

        if (DataCollectorHolder.dataCollectors.length && !_.isEmpty(DataCollectorHolder.lookupDataCollector) && scRegion.regions && !_.isEmpty(scRegion.regionLookup)) {
            prepRegionSchoolTermlyStats()
        }

    }]);
