/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewRegionController',['$rootScope', '$scope', '$state' ,'$stateParams',
    'Region', 'scRegion', 'scNotifier','$modal', '$log', 'scDistrict','scCircuit', 'scSchool','$timeout','DataCollectorHolder',
    function ($rootScope, $scope, $state, $stateParams, Region, scRegion, scNotifier,
              $modal, $log, scDistrict, scCircuit, scSchool, $timeout, DataCollectorHolder) {

        $scope.$parent.changePageTitle('View', 'Region', 'View');

        function assignAdmin() {
            $timeout(function () {
                if ($scope.adminRight > 4) {
                    $scope.regions = scRegion.regions;
                } else {
                    $scope.regions = [
                        scRegion.regionLookup[$scope.currentUser.level_id]
                    ];
                }
            }, 10)
        }

        $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            if (data.counter == 6) {
                assignAdmin();
            }
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scRegion.regions.length&& scRegion.regionLookup) {
            assignAdmin();
        }


        //initialise form Object
        $scope.formData = {};

        //Make the first letter of the action passed via stateParams a capital one
        $scope.paramAction = function () {
            return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
        };

//        if the action is to edit a Region do this
        if ($stateParams.action == 'edit') {
//            var data = scRegion.retrieveOneRegion($stateParams.id);
            scRegion.retrieveOneRegion($stateParams.id).then(
                function(data){
                    if (data) {
                        $scope.formData = {
                            id: scRegion.regionRetrieved.id,
                            region_name: scRegion.regionRetrieved.name,
                            description: scRegion.regionRetrieved.description,
                            region_email: scRegion.regionRetrieved.email,
                            region_phone_number: scRegion.regionRetrieved.phone_number,
                            country_id: scRegion.regionRetrieved.country_id
                        };
                    }
                },
                function(){
                    scNotifier.notifyFailure('Error', 'There was an error in trying to edit the region')
                });

            //Else if the action is to add, set the form object to empty
        } else if ($stateParams.action == 'add') {
            $scope.formData = {};
        }

        // process the form on click submit
        $scope.processForm = function () {
            $rootScope.loading = true;
            if ($stateParams.action == 'edit') {
                scRegion.editRegion($scope.formData).then(
                    function(data){
                        if (data) {
                            scNotifier.notifySuccess('Success', 'Region edited successfully ');
                            $state.go('dashboard.view_region',{}, {reload : true});
                        }else{
                            scNotifier.notifyFailure('Error', 'There was an error editing the region');
                        }
                    },
                    function(){
                        scNotifier.notifyFailure('Error', 'There was an error editing the region');
                    });
            } else {
                // process the form
                scRegion.addRegion($scope.formData).then(function (status) {
                    if (status == true) {
                        $scope.formData = {};
                        scNotifier.notifySuccess('Region', 'Successfully created the region');
                        $state.go('dashboard.view_region',{}, {reload : true});
                    } else if (status == false) {
                        scNotifier.notifyFailure('Region', 'There was an error creating the region');
                    } else {
                        scNotifier.notifyFailure('Region', 'Please check the fields again');
                    }
                    $rootScope.loading = false;
                });


            }
        };



        $scope.deleteModal = function (id) {

            var modalInstance = $modal.open({
                templateUrl: 'partials/miscellaneous/deleteModal.html',
                controller: ModalInstanceCtrl,
                size: 'sm',
                resolve: {
                    deleteRegionId: function () {
                        return id;}
//                    },
//                    clickedElement: function () {
////                    return $(element.target).parent('tr');
//                        return $(element.target).parents('tr');
                }
            })
        };


        var ModalInstanceCtrl = function ($scope, $modalInstance, deleteRegionId, scRegion, $state) {
            $scope.deleteObject = scRegion.regionLookup[deleteRegionId];
            $scope.deleteObject.objectType = 'Region';

            $scope.ok = function () {
                scRegion.deleteRegion(deleteRegionId).then(
                    function (status) {
                        if (status) {
                            scNotifier.notifySuccess('Region', 'Region deleted successfully');
                            $state.go('dashboard.view_region',{}, {reload : true});
                        } else {
                            scNotifier.notifyFailure('Region', 'There was an error in deleting the region');
                        }
                    });
                $modalInstance.close('close');
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };

    }]);