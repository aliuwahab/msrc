/**
 * Created by Kaygee on 21/01/2015.
 */
//Created by Kaygee on 17/06/14.

schoolMonitor.controller('scReportController',['$rootScope', '$scope','$state','$stateParams','School','coat_of_arms',
    'msrc_datauri_logo', '$interval',
    function ($rootScope, $scope, $state, $stateParams, School, coat_of_arms, msrc_datauri_logo, $interval) {

        $scope.changePageTitle('EMIS', 'Report', 'EMIS Report');

        $scope.reportParams = Storage.reportParams;

        $scope.chartObjects = {};

        $scope.drawReportBarChart = function (chartData, divId){
            var chart;

//            "type": "serial",
//                "theme": "patterns",

            var categoryField = "class";
            var categoryAxisTitle = 'Class';
            var valueAxisTitle = 'Number of Pupils';
            var graph1Title = 'Boys';
            var graph1LabelText = '';
            var graph1ValueField = "boys";
            var graph2Title = "Girls";
            var graph2LabelText = "";
            var graph2ValueField = "girls";

            if (divId == 'teacherAttendanceChartDiv') {
                categoryField = 'type';
                categoryAxisTitle = 'Average (%)';
                valueAxisTitle = 'Average Attendance';
                graph1Title = "School's Average (HT)";
                graph1LabelText = "School's Average (HT) : ";
                graph1ValueField = "ht_average";
                graph2Title = "School's Average (CS)";
                graph2LabelText = "School's Average (CS) : ";
                graph2ValueField = "cs_average";

            }

            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = categoryField;
            chart.startDuration = 1;
            chart.plotAreaBorderColor = "#DADADA";
            chart.plotAreaBorderAlpha = 1;
            // this single line makes the chart a bar chart
            chart.rotate = false;
//            chart.theme ='patterns';
            // AXES
            // Category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.gridPosition = "start";
            categoryAxis.gridAlpha = 0.1;
            categoryAxis.axisAlpha = 0;
            categoryAxis.title = categoryAxisTitle;

            // Value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.axisAlpha = 0;
            valueAxis.gridAlpha = 0.1;
            valueAxis.position = "bottom";
            valueAxis.title = valueAxisTitle;
            chart.addValueAxis(valueAxis);

            // GRAPHS
            // first graph
            var graph1 = new AmCharts.AmGraph();
            graph1.type = "column";
            graph1.title = graph1Title;
            graph1.valueField = graph1ValueField;
            graph1.labelText = graph1LabelText +  "[[value]]";
            graph1.balloonText = graph1Title + " : [[value]]";
            graph1.lineAlpha = 0;
//            graph1.pattern = {"url":"/scripts/js/plugins/amCharts/patterns/black/pattern1.png", "width":4, "height":4};
            graph1.fillColors = "#0D8ECF";
            graph1.fillAlphas = 1;
            chart.addGraph(graph1);

            if (divId == 'teacherAttendanceChartDiv') {
                // first graph
                var graph3 = new AmCharts.AmGraph();
                graph3.type = "column";
                graph3.title = 'District Average by HT';
                graph3.valueField = 'ht_district_average';
                graph3.labelText = "District's Average (HT): [[value]]";
                graph3.balloonText = "District's Average (HT) : [[value]]";
                graph3.lineAlpha = 0;
//                graph3.pattern = {"url":"/scripts/js/plugins/amCharts/patterns/black/pattern3.png", "width":4, "height":4};
                graph3.fillColors = "#074768";
                graph3.fillAlphas = 1;
                chart.addGraph(graph3);
            }

            // second graph
            var graph2 = new AmCharts.AmGraph();
            graph2.type = "column";
            graph2.title = graph2Title;
            graph2.valueField = graph2ValueField;
            graph2.labelText =  graph2LabelText + "[[value]]";
            graph2.balloonText = graph2Title + " : [[value]]";
            graph2.lineAlpha = 0;
//            graph2.pattern = {"url":"/scripts/js/plugins/amCharts/patterns/black/pattern2.png", "width":4, "height":4};
            graph2.fillColors = "#FF6600";
            graph2.fillAlphas = 1;
            chart.addGraph(graph2);

            if (divId === 'teacherAttendanceChartDiv') {
                var graph4 = new AmCharts.AmGraph();
                graph4.type = "column";
                graph4.title = 'District Average by CS';
                graph4.valueField = 'cs_district_average';
                graph4.labelText = "District's Average (CS) : [[value]]";
                graph4.balloonText =  "District's Average (CS) : [[value]]";
                graph4.lineAlpha = 0;
//                graph4.pattern = {"url":"/scripts/js/plugins/amCharts/patterns/black/pattern4.png", "width":4, "height":4};
                graph4.fillColors = "#9f4000";
                graph4.fillAlphas = 1;
                chart.addGraph(graph4);
            }

            // LEGEND
            var legend = new AmCharts.AmLegend();
            chart.addLegend(legend);

            chart.creditsPosition = "top-right";

            //EXPORTING
//            chart.exportConfig = {
//                menuItems: []
//            };
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(event){
                        return false;
                    },
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };


            $scope.chartObjects[divId] = chart;

            // WRITE
            chart.write(divId);

        };


        if ($scope.reportParams.student_enrollment) {
            $scope.student_enrollment_loading = true;
            $scope.student_enrollment_chart = false;
            $scope.student_enrollment_loading_done = false;

            School.allEnrollmentData({
                school_id :  $scope.reportParams.school.id,
                week_number : $scope.reportParams.week_number,
                term : $scope.reportParams.term,
                year : $scope.reportParams.academic_year
            }).$promise.then(
                function(enrollmentData){
                    $scope.enrollmentBarChartData = [];
                    angular.forEach(enrollmentData[0].school_enrolment, function(enrollment, index){
                        var tempObject = {
                            "girls": enrollment.num_females,
                            "boys": enrollment.num_males,
                            "class":enrollment.level
                        };
                        $scope.enrollmentBarChartData.push(tempObject);

                    });
                    if ($scope.enrollmentBarChartData.length) {
                        $interval($scope.drawReportBarChart($scope.enrollmentBarChartData, 'enrollmentChartDiv'), 1000, 5);
                        $scope.student_enrollment_loading = false;
                        $scope.student_enrollment_chart = true;
//                        $scope.chartObjects['enrollmentChartDiv'].validateNow();
                    }else{
                        $scope.student_enrollment_loading = false;
                        $scope.student_enrollment_loading_done = true;
                    }
                },
                function(){
                    console.log('error');
                }
            )

        }
        $scope.selected_school = $scope.reportParams.school;

        if ($scope.reportParams.student_attendance) {
            $scope.student_attendance_loading = true;
            $scope.student_attendance_chart = false;
            $scope.student_attendance_loading_done = false;

            School.allAttendanceData({
                school_id :  $scope.reportParams.school.id,
                week_number : $scope.reportParams.week_number,
                term : $scope.reportParams.term,
                year : $scope.reportParams.academic_year
            }).$promise.then(
                function(attendanceData){
                    $scope.attendanceBarChartData = [];
                    angular.forEach(attendanceData[0].school_attendance, function(attendance, index){
                        var tempObject = {
                            "girls": attendance.num_females,
                            "boys": attendance.num_males,
                            "class":attendance.level
                        };
                        $scope.attendanceBarChartData.push(tempObject);

                    });

                    if ($scope.attendanceBarChartData.length) {
                        $interval ($scope.drawReportBarChart($scope.attendanceBarChartData, 'attendanceChartDiv'), 1000, 5);
                        $scope.student_attendance_loading = false;
                        $scope.student_attendance_chart = true;

                    }else{
                        $scope.student_attendance_loading = false;
                        $scope.student_attendance_loading_done = true;
                    }

                },
                function(){
                    console.log('error');
                }
            )

        }

        if ($scope.reportParams.teacher_punctuality) {
            $scope.teacher_punctuality_loading = true;
            $scope.teacherAttendanceChartData = [];

            var tempObject = {
                "cs_average": $scope.selected_school.current_teacher_attendance_by_circuit_supervisor,
                "ht_average": $scope.selected_school.current_teacher_attendance_by_headteacher,
                "ht_district_average": $scope.districtLookup[$scope.selected_school.district_id].headteacherAttendanceAverage,
                "cs_district_average": $scope.districtLookup[$scope.selected_school.district_id].supervisorAttendanceAverage,
                "type" : "Teacher Attendance"
            };
            $scope.teacherAttendanceChartData.push(tempObject);

            $interval ($scope.drawReportBarChart($scope.teacherAttendanceChartData, 'teacherAttendanceChartDiv'), 1000, 2);
//            $scope.drawReportBarChart($scope.teacherAttendanceChartData, 'teacherAttendanceChartDiv');

            $scope.teacher_punctuality_loading = false;

        }

//        "student_enrollment": true,
//            "student_attendance": true,
//            "teacher_punctuality": true,
//            "records_performance": true,
//            "school_grounds": true,
//            "community_involvement": true

        $scope.createPdf = function(){
            $rootScope.loading = true;

            var date = new Date();

            var docDefinition = {};

            // a string or [width, height]
            docDefinition.pageSize = 'A4';

            // by default we use portrait, you can change it to landscape if you wish
//            docDefinition.pageOrientation = 'landscape';

            // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
            docDefinition.pageMargins = [ 40, 60, 40, 60 ];

            docDefinition.content = [];
            docDefinition.content.push();
            docDefinition.content.push({
                columns: [
                    {
                        image: coat_of_arms,
                        width: 60
                    },
                    {
                        // auto-sized columns have their widths based on their content
                        width: '*',
                        text: 'GHANA EDUCATION SERVICE',
                        alignment : 'center',
                        fontSize : 20
                    },
                    {
                        image: msrc_datauri_logo,
                        width: 60
                    }
                ]
            });

            docDefinition.content.push( {text : 'EMIS Report', fontSize : 19, alignment : 'center' });

            docDefinition.content.push( {text : $scope.selected_school.name , alignment : 'center', fontSize : 18, underline : true} );

            docDefinition.content.push( { text: 'Generated on ' + date.toLocaleDateString(), fontSize: 13, alignment: 'center', italic : true });

            docDefinition.content.push( {table: {
                // headers are automatically repeated if the table spans over multiple pages
                // you can declare how many rows should be treated as headers
                headerRows: 1,
//                widths: [ '*', 'auto', 100, '*' ],
                widths: [ '*','*','*','*'],
                body: [
                    [
                        { text: 'GES Code', bold: true },
                        { text: 'Year', bold: true },
                        { text: 'Term', bold: true },
                        { text: 'Week Number', bold: true }
                    ],
                    [
                        $scope.selected_school.ges_code,
                        $scope.reportParams.academic_year,
                        $scope.reportParams.academic_term,
                        $scope.reportParams.week_number
                    ]
                ]
            }
            });
//            docDefinition.content.push(
//                {
//                table: {
//                    widths: ['*'],
//                        body: [[" "], [" "]]
//                },
//                layout: {
//                    hLineWidth: function(i, node) {
//                        return (i === 0 || i === node.table.body.length) ? 0 : 2;
//                    },
//                    vLineWidth: function(i, node) {
//                        return 0;
//                    }
//                }
//            });
            var chartCount = 0;
            if ($scope.reportParams.student_enrollment) {

                var enrollment_imgData;
                $scope.chartObjects['enrollmentChartDiv'].AmExport.output(
                    {format:"jpg", output: 'datastring'}, function(data) {
                        enrollment_imgData = data;
                        console.log(data);
                        docDefinition.content.push( {text : 'Student Enrollment' , alignment : 'left', fontSize : 15, underline : true, margin : 20} );
                        docDefinition.content.push({image: enrollment_imgData, width : 525 });
                        chartCount++;
//                        docDefinition.content.push(  {image : 'msrc_logo.png'}  );
//                        docDefinition.content.push(  {image : 'http://schoolmonitor.dev/img/white_logo_150.png'}  );

                    });
            }

            if ($scope.reportParams.student_attendance) {

                var attendance_imgData;
                $scope.chartObjects['attendanceChartDiv'].AmExport.output(
                    {format:"jpg", output: 'datastring'}, function(data) {
                        attendance_imgData = data;
                        docDefinition.content.push( {text : 'Student Attendance' , alignment : 'left', fontSize : 15, underline : true, margin: 20} );
                        var temp = {image: attendance_imgData, width : 525 };
                        chartCount++;

                        if (chartCount > 1) {
                            temp.pageBreak =  'after';
                            docDefinition.content.push(temp);
                        }
                    });
            }

            if ($scope.reportParams.teacher_punctuality) {

                var teacher_punctuality_imgData;
                $scope.chartObjects['teacherAttendanceChartDiv'].AmExport.output(
                    {format:"jpg", output: 'datastring'}, function(data) {
                        teacher_punctuality_imgData = data;
                        docDefinition.content.push( {text : 'Teacher Attendance' , alignment : 'left', fontSize : 15, underline : true, margin: 20} );
                        docDefinition.content.push({image: teacher_punctuality_imgData, width : 525 });
                    });
            }

            // open the PDF in a new window
            /*   pdfMake.createPdf(docDefinition).open();

             pdfMake.createPdf(docDefinition).download();
             */

            pdfMake.createPdf(docDefinition).download( 'EMIS Report - '  + $scope.selected_school.name + ' - ' + date.setUTCDate(2) );

            $rootScope.loading = false;
        };




        var chart1;
        var chart2;

        makeCharts("patterns", "#FFFFFF");

        // Theme can only be applied when creating chart instance - this means
        // that if you need to change theme at run time, you have to create whole
        // chart object once again.

        function makeCharts(theme, bgColor, bgImage){

            if(chart1){
                chart1.clear();
            }
            if(chart2){
                chart2.clear();
            }

            // background
            if(document.body){
                document.body.style.backgroundColor = bgColor;
                document.body.style.backgroundImage = "url(" + bgImage + ")";
            }

            // column chart
            chart1 = AmCharts.makeChart("patternChart", {
                type: "serial",
                theme:theme,
                dataProvider: [{
                    "year": 2005,
                    "income": 23.5,
                    "expenses": 18.1
                }, {
                    "year": 2006,
                    "income": 26.2,
                    "expenses": 22.8
                }, {
                    "year": 2007,
                    "income": 30.1,
                    "expenses": 23.9
                }, {
                    "year": 2008,
                    "income": 29.5,
                    "expenses": 25.1
                }, {
                    "year": 2009,
                    "income": 24.6,
                    "expenses": 25
                }],
                categoryField: "year",
                startDuration: 1,

                categoryAxis: {
                    gridPosition: "start"
                },
                valueAxes: [{
                    title: "Million USD"
                }],
                graphs: [{
                    type: "column",
                    title: "Income",
                    valueField: "income",
                    lineAlpha: 0,
                    fillAlphas: 0.8,
                    balloonText: "[[title]] in [[category]]:<b>[[value]]</b>"
                }, {
                    type: "line",
                    title: "Expenses",
                    valueField: "expenses",
                    lineThickness: 2,
                    fillAlphas: 0,
                    bullet: "round",
                    balloonText: "[[title]] in [[category]]:<b>[[value]]</b>"
                }],
                legend: {
                    useGraphSettings: true
                }

            });

            // pie chart
            chart2 = AmCharts.makeChart("patternChart2", {
                type: "pie",
                theme: theme,
                dataProvider: [{
                    "country": "Czech Republic",
                    "litres": 156.9
                }, {
                    "country": "Ireland",
                    "litres": 131.1
                }, {
                    "country": "Germany",
                    "litres": 115.8
                }, {
                    "country": "Australia",
                    "litres": 109.9
                }, {
                    "country": "Austria",
                    "litres": 108.3
                }, {
                    "country": "UK",
                    "litres": 65
                }, {
                    "country": "Belgium",
                    "litres": 50
                }],
                titleField: "country",
                valueField: "litres",
                balloonText: "[[title]]<br><b>[[value]]</b> ([[percents]]%)",
                legend: {
                    align: "center",
                    markerType: "circle"
                }
            });

        }



    }]);


