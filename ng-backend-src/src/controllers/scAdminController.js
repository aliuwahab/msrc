/**
 * Created by KayGee on 6/16/14.
 */


schoolMonitor.controller('scAdminController', ['$rootScope', '$scope', 'scUser','scNotifier', '$state', '$modal','scRegion','scDistrict',
    function($rootScope,$scope, scUser, scNotifier, $state, $modal, scRegion, scDistrict) {

//This is an associative object that is used to apply classes to the region, district, circuit and school
        //dropdown. The numbers mean how many dropdowns are showing at a time
        $scope.level = {
            0 : 'col-sm-12',
            1 : 'col-sm-12',
            2 : 'col-sm-6',
            3 : 'col-sm-4',
            4 : 'col-sm-3'
        };
        //this is a scope variable that changes based on the chosen level so we dynamically generate the css to apply
        $scope.chosenLevel = 0;

        /*Check the user's level and hide the models that don't apply to him*/
        if ($scope.currentUser.level == 'Regional') {
            $scope.regions = [ scRegion.regionLookup[$scope.currentUser.level_id]];
            $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
            $scope.circuits = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
            $scope.schools = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
        }
        else if ($scope.currentUser.level == 'District') {
            $scope.regions = [ scRegion.regionLookup[scDistrict.districtLookup[ $scope.currentUser.level_id].region_id]];
            $scope.districts = [scDistrict.districtLookup[ $scope.currentUser.level_id]];
            $scope.circuits = scDistrict.districtLookup[ $scope.currentUser.level_id].allCircuitsHolder;
            $scope.schools = scDistrict.districtLookup[ $scope.currentUser.level_id].allSchoolsHolder;
        }
        // else if ($scope.currentUser.level == 'circuit') {
        //    
        // }
        // else if ($scope.currentUser.level == 'school') {
        //   
        // }


        // create a blank object to hold our form information
        $scope.formData = {};

        // process the form
        $scope.processForm = function() {
            $rootScope.loading = true;
            $scope.validation = {};
            scUser.createUser($scope.formData).then(
                //success callback
                function (value) {
                    // We use array because the server responseType is a json object
                    if (value[0] == 's' && value[1] == 'u') {
                        //$scope.formData = {};
                        scUser.reset();
                        $rootScope.$broadcast('updateAdmin');
                        scNotifier.notifySuccess('Success', 'Admin user successfully created');
                        $state.go('dashboard.settings.dashboard_admin');
                        // $scope.successMessage = 'Admin user successfully created' ;
                    }else if(value){
                        $rootScope.loading = false;
                        angular.forEach(value, function(value, prop){
                            $scope.validation [prop] = true;
                        })
                    }
                    else{
                        scNotifier.notifyFailure('Error', 'There was an error creating the user');
                        // $scope.errorMessageFromServer = value ;
                        $rootScope.loading = false
                    }

                },
                // error callback
                function (httpResponse){
                    scNotifier.notifyFailure('Error', 'There was an error creating the user');
                    //$scope.errorMessageFromServer = 'There was an error in creating the user' ;
                    $rootScope.loading = false
                });
        };

        $scope.deleteModal = function (userObject) {

            $modal.open({
                templateUrl: 'partials/miscellaneous/deleteModal.html',
                controller: deleteUserModalCtrl,
                size: 'sm',
                resolve : {
                    deleteObject:  function(){
                        return userObject
                    }
                }

            });
        };
// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        var deleteUserModalCtrl = function ($scope, $modalInstance, deleteObject) {

            $scope.deleteObject = deleteObject;
            $scope.deleteObject.name = deleteObject.firstname + ' ' + deleteObject.lastname;
            $scope.deleteObject.objectType = 'User';

            $scope.ok = function () {
                $rootScope.loading = true;
                $modalInstance.close('close');

                scUser.deleteUser($scope.deleteObject.id ).then(
                    function(status){
                        if (status) {
                            scUser.reset();
                            $rootScope.$broadcast('updateAdmin');
                            scNotifier.notifySuccess('Success', 'User deleted successfully');
                            $rootScope.loading = false;
                            $state.go('dashboard.settings.dashboard_admin');
                        }else{
                            scNotifier.notifyFailure('Error', 'There was an error deleting the user');
                            $rootScope.loading = false;
                        }
                    },
                    function(){
                        scNotifier.notifyFailure('Error', 'There was an error deleting the user');
                        $rootScope.loading = false;
                    })
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };


        $scope.hideOffCanvas = function(){
            $('.sidebar-offcanvas').offcanvas();
            console.log('hideOffCanvas')
        };


        $scope.clearAllCachedData = function() {

            $modal.open({
                templateUrl: 'partials/miscellaneous/clearCacheModal.html',
                controller: clearCacheModalCtrl,
                size: 'sm'
            });
        };
// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        var clearCacheModalCtrl = function ($scope, $modalInstance, CacheFactory) {

            $scope.ok = function () {
                $rootScope.loading = true;

                CacheFactory.disableAll();
                CacheFactory.clearAll();
                CacheFactory.destroyAll();


                scNotifier.notifySuccess('Success', 'Cache cleared successfully. All data will be refreshed on the next page reload.');
                $rootScope.loading = false;

                $modalInstance.close('close');

                window.location.href = '/admin';
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

        }

    }]);


schoolMonitor.controller('scAdminControllerEditUserProfile', ['$scope', 'scUser', '$rootScope', 'scNotifier','$state', '$stateParams','scDistrict','scCircuit','scSchool',
    function($scope, scUser, $rootScope, scNotifier, $state, $stateParams, scDistrict, scCircuit, scSchool){

        angular.forEach(scUser.allUsers, function(user, index){
            if (user.id == $stateParams.id) {
                $scope.userToEdit = user
            }
        });
        
        $scope.model = {
            isEditing : true
        };

        $scope.formData = {
            id:  $scope.userToEdit.id,
            first_name:  $scope.userToEdit.firstname,
            other_names:  $scope.userToEdit.lastname,
            email:  $scope.userToEdit.email,
            user_phone_number:  $scope.userToEdit.phone_number,
            gender :  $scope.userToEdit.gender,
            username :  $scope.userToEdit.username,
            // password :  '',
            admin_role:  $scope.userToEdit.role,
            country_id : 1
        };

        $scope.level = {
            0 : 'col-sm-12',
            1 : 'col-sm-12',
            2 : 'col-sm-6',
            3 : 'col-sm-4',
            4 : 'col-sm-3'
        };

        if ($scope.userToEdit.level === 'country'|| $scope.userToEdit.level === 'national' ) {
            $scope.formData.user_type = 'national'
        }
        else if ($scope.userToEdit.level === 'regional') {
            $scope.formData.user_type = 'regional';
            $scope.formData.regional_id = $scope.userToEdit.level_id;
            $scope.chosenLevel = 1;
        }
        else if ($scope.userToEdit.level === 'district') {
            $scope.formData.user_type = 'district';
            $scope.formData.district_id = $scope.userToEdit.level_id;

            $scope.formData.regional_id = scDistrict.districtLookup[ $scope.userToEdit.level_id ].region_id;
            $scope.chosenLevel = 2;
        }
        else if ($scope.userToEdit.level === 'circuit') {
            $scope.formData.user_type = 'circuit';
            $scope.formData.circuit_id = $scope.userToEdit.level_id;

            $scope.formData.regional_id = scCircuit.circuitLookup[ $scope.userToEdit.level_id ].region_id;

            $scope.formData.district_id = scCircuit.circuitLookup[ $scope.userToEdit.level_id ].district_id;

            $scope.chosenLevel = 3;
        }
        else if ($scope.userToEdit.level === 'school') {
            $scope.formData.user_type = 'school';
            $scope.formData.school_id = $scope.userToEdit.level_id;

            $scope.formData.regional_id = scSchool.schoolLookup[ $scope.userToEdit.level_id ].region_id;

            $scope.formData.district_id = scSchool.schoolLookup[ $scope.userToEdit.level_id ].district_id;

            $scope.formData.circuit_id = scSchool.schoolLookup[ $scope.userToEdit.level_id ].circuit_id;

            $scope.chosenLevel = 4;
        }


        $scope.processForm = function() {
            $rootScope.loading = true;
            $scope.validation = {};

            // delete $scope.formData.username;

            scUser.editUser($scope.formData).then(
                //success callback
                function (value) {
                    // We use array because the server responseType is a json object
                    if (value[0] == 's' && value[1] == 'u') {
                        //$scope.formData = {};
                        scUser.reset();
                        $rootScope.$broadcast('updateAdmin');
                        scNotifier.notifySuccess('Success', 'Admin user successfully edited');
                        $rootScope.loading = false;
                        $state.go('dashboard.settings.dashboard_admin');
                        // $scope.successMessage = 'Admin user successfully created' ;
                    }else if(value){
                        $rootScope.loading = false;
                        angular.forEach(value, function(value, prop){
                            $scope.validation [prop] = true;
                        })
                    }
                    else{
                        scNotifier.notifyFailure('Error', 'There was an error editing the user');
                        // $scope.errorMessageFromServer = value ;
                        $rootScope.loading = false
                    }

                },
                // error callback
                function (httpResponse) {
                    scNotifier.notifyFailure('Error', 'There was an error creating the user');
                    //$scope.errorMessageFromServer = 'There was an error in creating the user' ;
                    $rootScope.loading = false
                });
        }

    }]);

schoolMonitor.controller('scAdminControllerUserProfile', ['$scope' , function($scope){
    $scope.adminUser = $scope.currentUser;

    $scope.allowEdit = true;
    
    $scope.editable = function () {
        $scope.allowEdit = !$scope.allowEdit;
    }

}]);


schoolMonitor.controller('scAdminControllerDashboardUsers',
    ['$rootScope','$scope','scUser','$state','$timeout', 'scRegion', 'scDistrict','scCircuit','scSchool',
        function($rootScope, $scope, scUser, $state, $timeout, scRegion, scDistrict, scCircuit, scSchool){
            $scope.adminUser = $scope.currentUser;

            $scope.loadingAdmins = true;
            function getAllUsers(){
                scUser.getAllUsers().then(function (data) {

                    $scope.allUsers = data.users;

                    angular.forEach($scope.allUsers, function(user, index){
                        if (user.level === "national") {
                            user.geography = 'Ghana';
                            user.showRight = 5;
                        }else if (user.level === "regional" ) {
                            user.geography = scRegion.regionLookup[user.level_id] ? scRegion.regionLookup[user.level_id].name : "";
                            user.showRight = 4;
                        }else if (user.level === "district" ) {
                            user.geography = scDistrict.districtLookup[user.level_id] ? scDistrict.districtLookup[user.level_id].name : "";
                            user.showRight = 3;
                        }else if (user.level === "circuit" ) {
                            user.geography = scCircuit.circuitLookup[user.level_id] ? scCircuit.circuitLookup[user.level_id].name : "";
                            user.showRight = 2;
                        }else if (user.level === "school" ) {
                            user.geography = scSchool.schoolLookup[user.level_id] ? scSchool.schoolLookup[user.level_id].name : "";
                            user.showRight = 1;
                        }else{
                            user.showRight = 5;
                            user.geography = "Unknown";

                            /*do something here*/
                        }

                    });

                    $scope.loadingAdmins = false;

                });
            }

            getAllUsers();

            $scope.userAdminLevel = function(user){
                //Check if the logged in user is higher than the current admin in the loop
                //return FALSE to not hide that admin in the loop
                if ($scope.$parent.adminRight >= user.showRight ) {
                    //Start a switch case if the logged in admin is higher or equal to the admin in the loop
                    switch (user.showRight){
                        //if the admin in the loop is a national
                        case 5 :
                            //return FALSE since other nationals can view themselves
                            return false;
                            break;

                        //if the admin in the loop is a regional one,
                        case 4 :
                            //return FALSE if the logged in user is a national one
                            if ($scope.$parent.adminRight == 5) {
                                return false;
                                //if the logged in user is a regional
                            }else if ($scope.$parent.adminRight == 4) {
                                //return TRUE if their ids are not the same. TRUE will hide that user
                                return ($scope.currentUser.level_id != user.level_id)
                            }
                            break;

                        //if the admin in the loop is a district one,
                        case 3 :
                            //return FALSE if the logged in user is a national one
                            if ($scope.$parent.adminRight == 5) {
                                return false;
                                //if the logged in user is a regional
                            }else if ($scope.$parent.adminRight == 4) {
                                //return TRUE if their region ids are not the same. TRUE will hide that user
                                return ($scope.districtLookup[user.level_id].region_id != $scope.currentUser.level_id);
                                //if the logged in user is a district
                            }else if ($scope.$parent.adminRight == 3) {
                                //return TRUE if their ids are not the same. TRUE will hide that user
                                return ($scope.currentUser.level_id != user.level_id)
                            }
                            break;

                        //if the admin in the loop is a circuit one,
                        case 2 :
                            //return FALSE if the logged in user is a national one
                            if ($scope.$parent.adminRight == 5) {
                                return false;
                                //if the logged in user is a regional
                            }else if ($scope.$parent.adminRight == 4) {
                                //return TRUE if their region ids are not the same. TRUE will hide that user
                                return ($scope.circuitLookup[user.level_id].region_id != $scope.currentUser.level_id);
                                //if the logged in user is a district one
                            }else if ($scope.$parent.adminRight == 3) {
                                //return TRUE if their region ids are not the same. TRUE will hide that user
                                return ($scope.circuitLookup[user.level_id].district_id != $scope.currentUser.level_id);
                                //if the logged in user is a circuit one
                            }else if ($scope.$parent.adminRight == 3) {
                                //return TRUE if their ids are not the same. TRUE will hide that user
                                return ($scope.currentUser.level_id != user.level_id)
                            }
                            break;

                        //if the admin in the loop is a school one,
                        case 1 :
                            //return FALSE if the logged in user is a national one
                            if ($scope.$parent.adminRight == 5) {
                                return false;
                                //if the logged in user is a regional
                            }else if ($scope.$parent.adminRight == 4) {
                                //return TRUE if their region ids are not the same. TRUE will hide that user
                                return ($scope.schoolLookup[user.level_id].region_id != $scope.currentUser.level_id);
                                //if the logged in user is a district one
                            }else if ($scope.$parent.adminRight == 3) {
                                //return TRUE if their region ids are not the same. TRUE will hide that user
                                return ($scope.schoolLookup[user.level_id].district_id != $scope.currentUser.level_id);
                                //if the logged in user is a circuit one
                            }else if ($scope.$parent.adminRight == 2) {
                                //return TRUE if their ids are not the same. TRUE will hide that user
                                return ($scope.schoolLookup[user.level_id].circuit_id != $scope.currentUser.level_id);
                                //if the logged in user is a school one
                            }else if ($scope.$parent.adminRight == 1) {
                                //return TRUE if their ids are not the same. TRUE will hide that user
                                return ($scope.currentUser.level_id != user.level_id)
                            }
                            break;
                        default : return false;
                            break

                    }
                }else{
                    return true
                }
            };

            $scope.$on('updateAdmin', function(evt, data){
                getAllUsers();
                $rootScope.loading = false;
                $state.reload(true);
            });


        }]);


schoolMonitor.controller('scAdminControllerShowUserLogs', [
    '$scope', 'UserLogs', '$rootScope', 'scNotifier','$state', '$stateParams','createdLogs','editedLogs', 'deletedLogs',
    function($scope, UserLogs, $rootScope, scNotifier, $state, $stateParams, createdLogs, editedLogs, deletedLogs){

        var createdLogData = createdLogs;
        var editedLogData = editedLogs;
        var deletedLogData = deletedLogs;

        $scope.changeShowingTo = function (showWhat) {
            if (showWhat == 'created') {
                $scope.logsToShow = createdLogData;
                $scope.showing = 'created';
            }
            if (showWhat == 'edited') {
                $scope.logsToShow = editedLogData;
                $scope.showing = 'edited';
            }
            if (showWhat == 'deleted') {
                $scope.logsToShow = deletedLogData;
                $scope.showing = 'deleted';
            }
        };

        $scope.clearCacheAndRefresh = function () {
            $scope.loadingLogs = true;
            UserLogs.clearCache()
                .then(function (newData) {
                    createdLogData = newData.created;
                    editedLogData = newData.edited;
                    deletedLogData = newData.deleted;
                    $scope.changeShowingTo('created');
                    $scope.loadingLogs = false;
                })
        }

    }]);



schoolMonitor.controller('scAdminControllerLoginAdminUser', [
    '$scope', 'scUser', '$rootScope', 'scGlobalService','$state', '$stateParams',
    function($scope, scUser, $rootScope, scGlobalService, $state, $stateParams){

        //Use Jquery to replace the name on the dashboard so the
        //curly braces don't show on slow internet
        $('.hide_until_angular-loads').css('display', 'block');

        $scope.loginForm = {};
        $scope.loginAdminUser = function () {
            scUser.loginUser($scope.loginForm);
        };

        /*this is a logic to bypass the cs login and use the admin login with the same url. After clicking several times*/
        var count = 0;
        $scope.changeCSLogin = function () {
            count++;
            if (count > 5) {
                $rootScope.csLoginCheck = !$rootScope.csLoginCheck;
                count = 0;
            }
        };

    }]);
