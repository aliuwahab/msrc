/**
 * Created by Kaygee on 29/10/2015.
 */


schoolMonitor.controller('scCircuitSupervisorController',['$rootScope', '$scope', '$state','scCircuitSupervisor',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','SMSService', '$modal','DataCollector', 'DataCollectorHolder', '$timeout',
    function($rootScope, $scope, $state, scCircuitSupervisor,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, SMSService, $modal, DataCollector, DataCollectorHolder, $timeout) {

        $scope.$parent.changePageTitle('Data Collectors', 'Circuit Supervisors', 'Data Collectors');

        $scope.$on("updateDataCollector", function(){
            loadDataCollectors()
        });

        function loadDataCollectors(){

            $scope.regions = scRegion.regions;

            $scope.districts = scDistrict.districts;

            $scope.circuits = scCircuit.circuits;

            if ($scope.adminRight  == 5){
                $scope.circuit_supervisors = scCircuitSupervisor.supervisors;
            }else if ($scope.adminRight == 4){
                $scope.circuit_supervisors = scRegion.regionLookup[$scope.currentUser.level_id].circuit_supervisors_holder;
            } else if ($scope.adminRight  == 3){
                $scope.circuit_supervisors = scDistrict.districtLookup[$scope.currentUser.level_id].circuit_supervisors_holder;
            }

            $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;

        }

        if (scCircuitSupervisor.supervisors.length) {
            loadDataCollectors();
        }

        $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            if (data.counter == 6) {
                $timeout(function () {
                    loadDataCollectors();
                }, 10)
            }
        });

        $scope.$on('supervisorsUpdated', function (evt, data) {
            loadDataCollectors();
        });

        //Begin modal for sending credentials
        $scope.resendCredentialModal = function (id) {

            $modal.open({
                templateUrl: 'partials/miscellaneous/confirmCredential.html',
                controller: sendCredentialModalCtrl,
                size: 'sm',
                resolve : {

                    selected_data_collectors: function(){
                        return $scope.lookupDataCollector[id]
                    }
                }
            });
        };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        function sendCredentialModalCtrl ($scope, $modalInstance, selected_data_collectors) {

            $scope.datacollector = selected_data_collectors;

            $scope.send = function() {
                $modalInstance.dismiss('cancel');
                $rootScope.loading = true;

                DataCollector.resendLoginCredentials(
                    {
                        id :  $scope.datacollector.   id,
                        firstName :  $scope.datacollector.first_name,
                        identityCode :  $scope.datacollector.identity_code,
                        number :  $scope.datacollector.phone_number
                    },
                    //Success callback
                    function (value) {
                        if (value[0] == 's' && value[1] == 'u') {
                            scNotifier.notifySuccess('Notification', 'Credentials have been resent to ' + selected_data_collectors.name )
                        } else {
                            scNotifier.notifyFailure('Notification', 'There was a problem sending credentials')
                        }

                        $rootScope.loading = false;
                    },
                    //Error callback
                    function () {
                        scNotifier.notifyFailure('Notification', 'There was a problem sending credentials');
                        $rootScope.loading = false;
                    });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

        //Begin modal for deleting head teacher
        $scope.deleteModal = function (id) {

//            $scope.messageToDataCollector = $scope.lookupDataCollector[id];

            $modal.open({
                templateUrl: 'partials/miscellaneous/deleteModal.html',
                controller: deleteCircuitSupervisorModalCtrl,
                size: 'sm',
                resolve : {

                    selected_data_collector: function(){
                        return $scope.lookupDataCollector[id]
                    }
                }
            });
        };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        function deleteCircuitSupervisorModalCtrl ($scope, $modalInstance, selected_data_collector, scCircuitSupervisor) {

//            $scope.datacollector = selected_data_collector;

            $scope.deleteObject =selected_data_collector;
            $scope.deleteObject.objectType = 'Data Collector';

            $scope.ok = function() {
                $modalInstance.dismiss('cancel');
                $rootScope.loading = true;
                scCircuitSupervisor.deleteSupervisor(selected_data_collector.id)
                    //Success callback
                    .then(function (value) {
                        if (value) {
                            scNotifier.notifySuccess('Notification', 'Data Collector deleted');
//                            $state.go('dashboard.dashboard.data_collectors_circuit_supervisors', {reload : true})
                            $rootScope.$broadcast('supervisorsUpdated');
                        } else {
                            scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector')
                        }
                        $rootScope.loading = false;
                    },
                    //Error callback
                    function () {
                        scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector');
                        $rootScope.loading = false;
                    });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }


    }])


    .controller('scAllCircuitSupervisorsController',['$rootScope','$scope','$state','scCircuitSupervisor',
        'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$modal','SMSService','DataCollector','DataCollectorHolder', '$timeout',
        function($rootScope, $scope, $state, scCircuitSupervisor,
                 scNotifier, scRegion, scDistrict, scCircuit, scSchool, $modal, SMSService, DataCollector, DataCollectorHolder, $timeout) {

            $scope.$parent.changePageTitle('Data Collectors', 'Circuit Spupervisors', 'Data Collectors');

            $scope.loadingSupervisors = true;

            function loadDataCollectors(){
                $scope.loadingSupervisors = false;

                // $scope.regions = scRegion.regions;
                //
                // $scope.districts = scDistrict.districts;
                //
                // $scope.circuits = scCircuit.circuits;
                //
                $scope.schoolLookup = scSchool.schoolLookup;

                if ($scope.adminRight  === 5){
                    $scope.circuit_supervisors = scCircuitSupervisor.supervisors;
                    $scope.filename_export = "_country";
                }else if ($scope.adminRight === 4){
                    $scope.circuit_supervisors = scRegion.regionLookup[$scope.currentUser.level_id].circuit_supervisors_holder;
                    $scope.filename_export = scRegion.regionLookup[$scope.currentUser.level_id].name + "_region";

                } else if ($scope.adminRight  === 3){
                    $scope.circuit_supervisors = scDistrict.districtLookup[$scope.currentUser.level_id].circuit_supervisors_holder;
                    $scope.filename_export = scDistrict.districtLookup[$scope.currentUser.level_id].name + "_district";

                }else if ($scope.adminRight  === 2){
                    $scope.circuit_supervisors = scCircuit.circuitLookup[$scope.currentUser.level_id].circuit_supervisors_holder;
                    $scope.filename_export = scCircuit.circuitLookup[$scope.currentUser.level_id].name + "_circuit";

                }

                $scope.supervisorDetails = {
                    male : 0,
                    female : 0,
                    trained : 0
                };

                angular.forEach($scope.circuit_supervisors, function (supervisor, index) {
                    if (supervisor.gender === 'male') {
                        $scope.supervisorDetails.male ++;
                    }else $scope.supervisorDetails.female ++;

                    // if (angular.isUndefined(teacher.highest_professional_qualification)
                    //     || teacher.highest_professional_qualification === ''
                    //     || teacher.highest_professional_qualification.toLowerCase() === 'others'
                    //     || teacher.highest_professional_qualification.toLowerCase() === 'other') {
                    //
                    // }else{
                    //     $scope.supervisorDetails.trained ++;
                    // }



                });

                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;


            }

            $scope.$on("supervisorsUpdated", function(){
                loadDataCollectors()
            });

            if (scCircuitSupervisor.supervisors.length) {
                loadDataCollectors();
            }

            $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
                $timeout(function () {
                    if (data.counter == 6) {
                        loadDataCollectors();
                    }
                }, 10)

            });


        }]);

