/**
 * Created by kaygee on 2/07/14.
 */

schoolMonitor.controller('scStakeholderController',['$scope', 'DataCollector',
    function($scope, DataCollector) {

        $scope.$parent.content_header = 'Data Collectors';

        $scope.$parent.content_header_small = 'Overview';

        $scope.$parent.selected_page = 'Data Collectors';

        $scope.lookupDataCollector = $rootScope.lookupDataCollector;

    }]);

schoolMonitor.controller('scStakeholderCommunityController',['$rootScope','$scope',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','SMSService','$timeout',
    function($rootScope, $scope, scNotifier, scRegion, scDistrict, scCircuit, scSchool, SMSService, $timeout) {

        $scope.$parent.changePageTitle('Community', 'SMS', 'Community');




        $scope.processSMS = function(){
            // Without this dummy info, the sms array doesn't show
//        Dates array holds all the dates and
            $scope.dates = [{formatted : 'July 20, 1989'}];
            // Without this dummy info, the sms array doesn't show
            $scope.community_smses = [{kofi: 'boy'}];
//           remove  dummy data before populating array
            if ($scope.dates[0].formatted == 'July 20, 1989') {
                $scope.dates.pop();
            }
            //  remove  dummy data before populating array
            if (  $scope.community_smses[0].kofi == 'boy') {
                $scope.community_smses.pop();
            }

            //DatesHolder and comm_SMS_holder are temporary arrays that
            // check if a date or an sms already exists in an array
            $scope.datesHolder = [];
            $scope.comm_SMS_holder = [];


            //For each sms in all the smses loaded, if the created date is already in the temporal datesHolder array,
            //don't create a new date heading
            angular.forEach($scope.comm_SMS_holder, function(message){
                if ( $scope.datesHolder.indexOf($scope.momentFormatDate(message.created_at, 'LL')) == -1 ) {
                    $scope.dates.push({
                        created :  message.created_at,
                        formatted : $scope.momentFormatDate(message.created_at, 'LL')
                    });

                    $scope.datesHolder.push( $scope.momentFormatDate(message.created_at, 'LL') );
                    $scope.dates.sort(function(a, b){
                        return  moment  (b.created_at).utc().valueOf()  -   moment(a.created_at).utc().valueOf();
                    })
                }
                message.formattedDate = $scope.momentFormatDate(message.created_at, 'LL');
                message.school = $scope.schoolLookup [message.school_id];
                $scope.community_smses.push(message);
            });
        };

        if ($scope.adminRight == 5) {
            $rootScope.loading = true;
            SMSService.getSMSes('country', 1)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    $scope.comm_SMS_holder = sms_data;
                    $scope.processSMS();
                    $rootScope.loading = false;
                });
        }
        else if ($scope.adminRight == 4) {
            $rootScope.loading = true;
            SMSService.getSMSes('region', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    $scope.comm_SMS_holder = sms_data;
                    $scope.processSMS();
                    $rootScope.loading = false;
                });
        }
        else if ($scope.adminRight == 3) {
            $rootScope.loading = true;
            SMSService.getSMSes('district', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    $scope.comm_SMS_holder = sms_data;
                    $scope.processSMS();
                    $rootScope.loading = false;
                });
        }
        else if ($scope.adminRight == 2) {
            $rootScope.loading = true;
            SMSService.getSMSes('circuit', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    $scope.comm_SMS_holder = sms_data;
                    $scope.processSMS();
                    $rootScope.loading = false;
                });
        }
        else if ($scope.adminRight == 1) {
            $rootScope.loading = true;
            SMSService.getSMSes('school', $scope.currentUser.level_id)
                .then(function(sms_data){
                    sms_data.sort(function(a,b){
                        return moment( b.created_at).utc().valueOf() - moment( a.created_at).utc().valueOf()
                    });
                    $scope.comm_SMS_holder = sms_data;
                    $scope.processSMS();
                    $rootScope.loading = false;
                });
        }

        $scope.schoolLookup = scSchool.schoolLookup;

        $scope.regions = scRegion.regions;

        $scope.districts = scDistrict.districts;

        $scope.circuits = scCircuit.circuits;

        $scope.$on('updateLoadedModelsFromRefresh',function(){
            $timeout(function () {
                $scope.regions = scRegion.regions;

                $scope.districts = scDistrict.districts;

                $scope.circuits = scCircuit.circuits;

                $scope.schoolLookup = scSchool.schoolLookup;

                $scope.processSMS();


            }, 10)

        });


    }]);
