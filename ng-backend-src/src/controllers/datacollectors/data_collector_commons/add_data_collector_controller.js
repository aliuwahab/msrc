/**
 * Created by Kaygee on 29/10/2015.
 */


schoolMonitor.controller('scAddDataCollectorController',
    ['$rootScope', '$scope', 'DataCollector', '$state', '$stateParams',
        'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','scHeadTeacher','scCircuitSupervisor','$timeout',
        function ($rootScope, $scope,DataCollector, $state, $stateParams,
                  scNotifier, scRegion, scDistrict, scCircuit, scSchool, scHeadTeacher, scCircuitSupervisor, $timeout) {

            // create a blank object to hold our form information
            $scope.formData = {
                type : $stateParams.type
            };

            $timeout(function () {
                $scope.formData.type = $stateParams.type
            }, 1000);

            function assignAdminRights(){
                if ($scope.adminRight > 4){
                    $scope.regions = scRegion.regions;

                    $scope.districts = scDistrict.districts;

                    $scope.circuits = scCircuit.circuits;

                    $scope.head_teachers = scHeadTeacher.headteachers;

                    $scope.supervisors = scCircuitSupervisor.supervisors;

                }
                else if ($scope.adminRight == 4){
                    $scope.regions = new Array( scRegion.regionLookup[$scope.currentUser.level_id] );

                    $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;

                    $scope.circuits =  scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;

                    $scope.head_teachers =  scRegion.regionLookup[$scope.currentUser.level_id].head_teachers_holder;

                    $scope.supervisors =  scRegion.regionLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                }
                else if ($scope.adminRight  == 3){
                    $scope.regions = new Array( scRegion.regionLookup[scDistrict.districtLookup[$scope.currentUser.level_id].region_id] );

                    $scope.districts = new Array ( scDistrict.districtLookup[$scope.currentUser.level_id] );

                    $scope.circuits = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;

                    $scope.head_teachers = scDistrict.districtLookup[$scope.currentUser.level_id].head_teachers_holder;

                    $scope.supervisors =  scDistrict.districtLookup[$scope.currentUser.level_id].circuit_supervisors_holder;
                }
                $scope.lookupDataCollector = $rootScope.lookupDataCollector;
                $scope.dataCollectorsArray = $rootScope.dataCollectorsArray;

            }

            if (scHeadTeacher.headteachers.length && scCircuitSupervisor.supervisors.length ) {
                assignAdminRights();
            }

            $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
                if (data.counter == 6) {
                    $timeout(function () {
                        assignAdminRights();
                    }, 10)
                }

            });



            //Param Action is the edit or add data collection action passed to the url
            $scope.paramAction = function () {
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };


            $scope.dataCollectorType = function () {
                return $scope.formatDataCollectorType[$stateParams.type];
            };

            $scope.$parent.changePageTitle('Data Collectors',  $scope.paramAction() ,  'Data Collectors');

            $scope.checkPhoneNumber = function($event){
                var number = $event.target.value;
                if (number.charAt(0) == 0 && number.length == 10) {
                    $scope.phoneNumberExists = false;
                    $scope.formData.phone_number = '+233' + number.substring(1);
                    angular.forEach($scope.dataCollectorsArray, function(person, index){
                        if (person.phone_number == $scope.formData.phone_number) {
                            $scope.phoneNumberExists = true
                        }
                    })
                }else{
                    $scope.formData.phone_number = '' ;
//            $scope.formData.phone_number.$error
                }
            };

            if ($stateParams.action == 'edit') {
                DataCollector.retrieveOneDataCollector({id: $stateParams.id},
                    function (data, responseHeaders) {
                        $scope.formData = {
                            id: data.id,
                            first_name : data.first_name,
                            last_name : data.last_name,
                            gender : data.gender,
                            identity_code : data.identity_code,
                            type : data.type,
                            email : data.email,
                            phone_number : data.phone_number,
                            country_id: data.country_id,
                            region : data.region_id,
                            district : data.district_id,
                            circuit : data.circuit_id
                        };
                        $('#identity_code_display').val(data.identity_code);
                    },
                    function (httpResponse) {

                    })
            } else if ($stateParams.action == 'add') {
                $scope.formData = {};
            }

            // process the form
            $scope.processForm = function () {
                $rootScope.loading = true;
                $scope.validation = {};
                if ($stateParams.action == 'edit') {
                    var editQueryModel;
                    if ($stateParams.type == 'head_teacher' ) {
                        editQueryModel = scHeadTeacher.editHeadTeacher($scope.formData);
                    }else if($stateParams.type == 'circuit_supervisor'){
                        editQueryModel =  scCircuitSupervisor.editSupervisor($scope.formData)
                    }
                    //success callback
                    editQueryModel.then(function (value, responseHeaders) {
                            if ($.trim(value) == 'success') {
                                $scope.formData = {};
                                var string_to_go_to = 'dashboard.data_collectors_' + $stateParams.type +'s';
                                scNotifier.notifySuccess("Success", "Data collector successfully edited.");
                                $state.go(string_to_go_to, {} , {reload : true});
                            }else {
                                $rootScope.loading = false;
                                angular.forEach(value, function(value, prop){
                                    $scope.validation [prop] = true;
                                });
                                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");
                            }
                        },
                        //error callback
                        function (httpResponse) {
                            $rootScope.loading = false;
                            scNotifier.notifyFailure("Error", "An error occurred on the server. Please try again or  contact the system administrator.");
//                            scNotifier.notifyFailure("Error","There was an error in editing the data collector");
//                            $scope.errorMessageFromServer = 'There was an error in editing the data collector';
                        });
                } else {
                    var addQueryModel;
                    if ($scope.formData.type == 'head_teacher' ) {
                        addQueryModel = scHeadTeacher.addDataCollector($scope.formData);
                    }else if($scope.formData.type == "circuit_supervisor"){
                        addQueryModel =  scCircuitSupervisor.addDataCollector($scope.formData)
                    }
                    addQueryModel.then(function (value) {
                            if (value == 'success') {
                                //$scope.formData = {};
                                //var string_to_go_to = 'dashboard.data_collectors_' + $stateParams.type +'s';
                                var string_to_go_to = 'dashboard.data_collectors_' + $scope.formData.type +'s';
                                scNotifier.notifySuccess("Success", "Data collector successfully added.");
                                $state.go(string_to_go_to, {} , {reload : true});
                            }else{
                                angular.forEach(value, function(value, prop){
                                    $scope.validation [prop] = true;
                                });
                                $rootScope.loading = false;
                                scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

                            }
                        },
                        //error callback
                        function (httpResponse){
                            $rootScope.loading = false;
                            scNotifier.notifyInfo("Error", "An error occurred on the server. Please try again or  contact the system administrator.");

                        });
                }};


            $scope.generateIdentityCode = function(){
                //TODO get code from server and populate the ID code field
                $('#identity_code_display').val('fkjaww38j3uGHww');
            };

        }]);