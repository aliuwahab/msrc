/**
 * Created by Kaygee on 29/10/2015.
 */


schoolMonitor.controller('scViewDataCollectorController',
    ['$rootScope','$scope','$state', 'scNotifier', 'scRegion', 'scDistrict',
        'scCircuit', 'scSchool', 'SMSService', 'RecentSubmissionService','DataCollector','scHeadTeacher','scCircuitSupervisor','$timeout', 'DataCollectorHolder',
        '$stateParams',
        function($rootScope, $scope, $state, scNotifier, scRegion, scDistrict,
                 scCircuit, scSchool, SMSService, RecentSubmissionService, DataCollector, scHeadTeacher, scCircuitSupervisor, $timeout, DataCollectorHolder,
                 $stateParams){

            $scope.loadingSMS = false;

            $scope.fetchAndUpdateDCMessages = function() {
                $scope.loadingSMS = true;
                DataCollector.retrieveDataCollectorSMSMessages({recipient_number : $scope.selected_data_collector.phone_number, sender_db_id: $scope.selected_data_collector.id, recipient_db_id : $scope.selected_data_collector.id}).$promise
                    .then(function (successData) {
                        if (successData.status == '200') {
                            $scope.data_collector_messages = successData.data.sms_messages;

                            $scope.data_collector_messages.forEach(function (message, index) {
                                message.sender = ''; message.recipient = '';
                                if (DataCollectorHolder.lookupDataCollector[message.sender_db_id]) {
                                    message.sender =  DataCollectorHolder.lookupDataCollector[message.sender_db_id].name;
                                }
                                if ( DataCollectorHolder.lookupDataCollector[message.recipient_db_id]) {
                                    message.recipient =  DataCollectorHolder.lookupDataCollector[message.recipient_db_id].name;
                                }
                                
                                if (message.from == 'admin') {
                                    message.from = "Admin";
                                    message.sender = "";
                                } else if (message.from == 'circuit_supervisor') {
                                    message.from = 'Circuit Supervisor';
                                } else {
                                    message.from = 'Head Teacher';
                                }
                            })
                        }
                        $scope.loadingSMS = false;
                    })
            };


            function loadDataCollectors(){
                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;
                $scope.selected_data_collector = $scope.lookupDataCollector[$stateParams.id];

                $scope.messageForm = {
                    recipient : $scope.selected_data_collector,
                    message : ''
                };
                $scope.fetchAndUpdateDCMessages();
            }

            function checkDCsAreLoadAndPrepDC() {
                if (!(_.isEmpty(DataCollectorHolder.lookupDataCollector)) && angular.isDefined(DataCollectorHolder.lookupDataCollector[$stateParams.id])) {
                    loadDataCollectors();
                } else {
                    $timeout(function () {
                        checkDCsAreLoadAndPrepDC();
                    }, 2500)
                }
            }

            if (scHeadTeacher.headteachers.length && scCircuitSupervisor.supervisors.length && (!_.isEmpty(DataCollectorHolder.lookupDataCollector))) {
                checkDCsAreLoadAndPrepDC();
            }

            $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
                $timeout(function () {
                    if (data.counter == 6) {
                        checkDCsAreLoadAndPrepDC()
                    }
                }, 2000)

            });

            RecentSubmissionService.getAllRecentSubmissions('data_collector', $stateParams.id)
                .then(function (recent_submissions) {
                    prepRecentSubmissions(recent_submissions);
                });



            function prepRecentSubmissions(recent_submissions) {
                $scope.recentDataCollectorSubmissions = recent_submissions;

                $scope.lastSubmitted = 'No submissions';
                if ($scope.recentDataCollectorSubmissions.length) {
                    $scope.recentDataCollectorSubmissions.sort(function (a, b) {
                        return moment(b.created_at).utc().valueOf() - moment(a.created_at).utc().valueOf()
                    });
                    $scope.lastSubmitted = $scope.momentDateFromNow($scope.recentDataCollectorSubmissions[0].created_at);
                    $scope.totalItems = $scope.recentDataCollectorSubmissions.length;
                } else {
                    $scope.totalItems = 0;
                }

                $scope.currentPage = 1;
                $scope.maxSize = 5;
                $scope.nextPage = 0;
                //            array.slice(start,end)
                /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
                 * It does not include the element at that index*/
                $scope.pageChanged = function () {
                    $scope.nextPage = (parseInt($scope.maxSize) * parseInt($scope.currentPage)) - parseInt($scope.maxSize);
                    $scope.recentDataCollectorSubmissions = recent_submissions.slice($scope.nextPage, ($scope.nextPage + 5));
                };
            }

            $scope.sendingSms = false;

            $scope.sendSms = function () {
                if ($scope.messageForm.message.length < 5) {
                    scNotifier.notifyInfo("Please provide a message with at least 5 characters");
                    return;
                }
                $scope.sendingSms = true;

                SMSService.sendSmsFromDashboard($scope.messageForm)
                    .success(function (successData) {
                        if(successData.status == '200'){
                            scNotifier.notifySuccess("Message sent successfully");
                            $scope.messageForm.message = '';
                            $scope.fetchAndUpdateDCMessages();
                        }
                    })
                    .error(function () {
                        scNotifier.notifyFailure("Oops! Something went wrong. Please check internet connection");
                    }).finally(function () {
                    $scope.sendingSms = false;
                });
            };



        }]);