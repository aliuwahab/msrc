/**
 * Created by Kaygee on 29/10/2015.
 */

schoolMonitor
    .controller('scHeadTeacherController',['$rootScope','$scope','$state','scHeadTeacher',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$modal','SMSService','DataCollector','DataCollectorHolder', '$timeout',
    function($rootScope, $scope, $state, scHeadTeacher,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $modal, SMSService, DataCollector, DataCollectorHolder, $timeout) {

        $scope.$parent.changePageTitle('Data Collectors', 'Head Teachers', 'Data Collectors');

        function loadDataCollectors(){

            $scope.regions = scRegion.regions;

            $scope.districts = scDistrict.districts;

            $scope.circuits = scCircuit.circuits;

            if ($scope.adminRight  == 5){
                $scope.head_teachers = scHeadTeacher.headteachers;
            }else if ($scope.adminRight == 4){
                $scope.head_teachers = scRegion.regionLookup[$scope.currentUser.level_id].head_teachers_holder;
            } else if ($scope.adminRight  == 3){
                $scope.head_teachers = scDistrict.districtLookup[$scope.currentUser.level_id].head_teachers_holder;
            }else if ($scope.adminRight  == 2){
                $scope.head_teachers = scCircuit.circuitLookup[$scope.currentUser.level_id].head_teachers_holder;
            }

            $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;

        }

        $scope.$on("headteachersUpdated", function(){
            loadDataCollectors()
        });

        if (scHeadTeacher.headteachers.length) {
            loadDataCollectors();
        }

        $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            $timeout(function () {
                if (data.counter == 6) {
                    loadDataCollectors();
                }
            }, 10)

        });

        //Begin modal for sending message
        $scope.messageModal = function (id, quantity) {

//            $scope.messageToDataCollector = $scope.lookupDataCollector[id];

            $modal.open({
                templateUrl: 'partials/miscellaneous/sendNotificationModal.html',
                controller: sendMessageModalCtrl,
                size: 'sm',
                resolve : {

                    selected_data_collectors: function(){
                        if (typeof (parseInt(id)) == 'number' ) {
                            //Lookup the object of the datacollector by id
                            //and put it in an array so that its the same for the multiple one too
                            return [
                                $scope.lookupDataCollector[id]
                            ];
                        }else if(id == 'multiple'){
                            return $scope.dataCollectors_to_send_messages_to;
                        }
                    },
                    quantity : function() {
                        return  quantity
                    }
                }
            });
        };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        function sendMessageModalCtrl ($scope, $modalInstance,SMSService,  selected_data_collectors, quantity) {

            //initialise the sendMessage object variable
            $scope.messageForm ={
                scope : null,
                id_of_scope : null,
                recipients : []
            };

            $scope.selected_data_collectors = [];

            if (quantity == 'one' || quantity == 'multiple') {
                $scope.messageForm.recipients = selected_data_collectors;
                $scope.selected_data_collectors = selected_data_collectors;

            }else if(quantity == 'all'){
                $scope.messageForm.scope = 'country';
                $scope.messageForm.id_of_scope = 1;
            }


            $scope.send = function() {
                $modalInstance.dismiss('cancel');
                $rootScope.loading = true;
                SMSService.sendMessageToDataCollectors($scope.messageForm)
                    //Success callback
                    .success(function (value) {
                        if ((value[0] == 's' && value[1] == 'u') || $.trim(JSON.stringify(value)) == '') {
                            scNotifier.notifySuccess('Notification', 'Messages have been sent successfully')
                        } else {
                            scNotifier.notifyFailure('Notification', 'There was a problem sending message')
                        }

                        $rootScope.loading = false;
                    })
                    //Error callback
                    .error(function () {
                        scNotifier.notifyFailure('Notification', 'There was a problem sending message');
                        $rootScope.loading = false;
                    });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }

        //Begin modal for sending credentials
        $scope.resendCredentialModal = function (id) {

//            $scope.messageToDataCollector = $scope.lookupDataCollector[id];

            $modal.open({
                templateUrl: 'partials/miscellaneous/confirmCredential.html',
                controller: sendCredentialModalCtrl,
                size: 'sm',
                resolve : {

                    selected_data_collectors: function(){
                        return $scope.lookupDataCollector[id]
                    }
                }
            });
        };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        function sendCredentialModalCtrl ($scope, $modalInstance, selected_data_collectors) {

            $scope.datacollector = selected_data_collectors;

            $scope.send = function() {
                $modalInstance.dismiss('cancel');
                $rootScope.loading = true;
                DataCollector.resendLoginCredentials(
                    {
                        id :  $scope.datacollector.id,
                        firstName :  $scope.datacollector.first_name,
                        identityCode :  $scope.datacollector.identity_code,
                        number :  $scope.datacollector.phone_number
                    },
                    //Success callback
                    function (value) {
                        if (value[0] == 's' && value[1] == 'u') {
                            scNotifier.notifySuccess('Notification', 'Credentials have been resent to ' + selected_data_collectors.name )
                        } else {
                            scNotifier.notifyFailure('Notification', 'There was a problem sending credentials')
                        }

                        $rootScope.loading = false;
                    },
                    //Error callback
                    function () {
                        scNotifier.notifyFailure('Notification', 'There was a problem sending credentials');
                        $rootScope.loading = false;
                    });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }


        //Begin modal for deleting head teacher
        $scope.deleteModal = function (id) {

//            $scope.messageToDataCollector = $scope.lookupDataCollector[id];

            $modal.open({
                templateUrl: 'partials/miscellaneous/deleteModal.html',
                controller: deleteHeadTeacherModalCtrl,
                size: 'sm',
                resolve : {

                    selected_data_collector: function(){
                        return $scope.lookupDataCollector[id]
                    }
                }
            });
        };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

        function deleteHeadTeacherModalCtrl ($scope, $modalInstance, selected_data_collector, scHeadTeacher) {

//            $scope.datacollector = selected_data_collector;

            $scope.deleteObject = selected_data_collector;
            $scope.deleteObject.objectType = 'Data Collector';

            $scope.ok = function() {
                $modalInstance.dismiss('cancel');
                $rootScope.loading = true;
                scHeadTeacher.deleteHeadTeacher(selected_data_collector.id)
                    //Success callback
                    .then(function (value) {
                        if (value) {
                            scNotifier.notifySuccess('Notification', 'Data Collector deleted');
//                            $state.go('dashboard.dashboard.data_collectors_circuit_supervisors', {reload : true})
                            $rootScope.$broadcast('headteachersUpdated');
                        } else {
                            scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector')
                        }
                        $rootScope.loading = false;
                    },
                    //Error callback
                    function () {
                        scNotifier.notifyFailure('Notification', 'There was a problem deleting the data collector');
                        $rootScope.loading = false;
                    });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }


    }])

    .controller('scAllHeadTeachersController',['$rootScope','$scope','$state','scHeadTeacher',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$modal','SMSService','DataCollector','DataCollectorHolder', '$timeout',
    function($rootScope, $scope, $state, scHeadTeacher,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $modal, SMSService, DataCollector, DataCollectorHolder, $timeout) {

        $scope.$parent.changePageTitle('Data Collectors', 'Head Teachers', 'Data Collectors');

        $scope.loadingTeachers = true;

        function loadDataCollectors(){
            $scope.loadingTeachers = false;

            // $scope.regions = scRegion.regions;
            //
            // $scope.districts = scDistrict.districts;
            //
            // $scope.circuits = scCircuit.circuits;
            //
            $scope.schoolLookup = scSchool.schoolLookup;

            if ($scope.adminRight  === 5){
                $scope.head_teachers = scHeadTeacher.headteachers;
                $scope.filename_export = "_country";
            }else if ($scope.adminRight === 4){
                $scope.head_teachers = scRegion.regionLookup[$scope.currentUser.level_id].head_teachers_holder;
                $scope.filename_export = scRegion.regionLookup[$scope.currentUser.level_id].name + "_region";

            } else if ($scope.adminRight  === 3){
                $scope.head_teachers = scDistrict.districtLookup[$scope.currentUser.level_id].head_teachers_holder;
                $scope.filename_export = scDistrict.districtLookup[$scope.currentUser.level_id].name + "_district";

            }else if ($scope.adminRight  === 2){
                $scope.head_teachers = scCircuit.circuitLookup[$scope.currentUser.level_id].head_teachers_holder;
                $scope.filename_export = scCircuit.circuitLookup[$scope.currentUser.level_id].name + "_circuit";
            }

            $scope.teacherDetails = {
                male : 0,
                female : 0,
                trained : 0
            };

            angular.forEach($scope.head_teachers, function (teacher, index) {
                if (teacher.gender === 'male') {
                    $scope.teacherDetails.male ++;
                }else $scope.teacherDetails.female ++;

                // if (angular.isUndefined(teacher.highest_professional_qualification)
                //     || teacher.highest_professional_qualification === ''
                //     || teacher.highest_professional_qualification.toLowerCase() === 'others'
                //     || teacher.highest_professional_qualification.toLowerCase() === 'other') {
                //
                // }else{
                //     $scope.teacherDetails.trained ++;
                // }



            });

            $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;


        }

        $scope.$on("headteachersUpdated", function(){
            loadDataCollectors()
        });

        if (scHeadTeacher.headteachers.length) {
            loadDataCollectors();
        }

        $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            $timeout(function () {
                if (data.counter == 6) {
                    loadDataCollectors();
                }
            }, 10)

        });


    }]);
