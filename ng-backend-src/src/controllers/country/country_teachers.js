/**
 * Created by Kaygee on 23/04/2015.
 */


schoolMonitor.controller('scViewSelectedCountryTeacherController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'District', 'DataCollectorHolder', 'Report', 'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', '$interval',
    function($rootScope, $scope, $state, $stateParams, District, DataCollectorHolder, Report,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval) {

        function  prepCountryTeachersInfo(){
            $scope.countryTeachers = $scope.teacherSummaryData.teachersInfo;

            // $scope.selected_region = scRegion.regionLookup[$stateParams.id];

            /* Loop to count teacher per school */
            // for (var i = 0; i < $scope.countryTeachers.length; i++){
            //     var teacher = $scope.countryTeachers[i];
            //       if ( $scope.selected_region.allSchools[teacher.school_id] != undefined) {
            //           $scope.selected_region.allSchools[teacher.school_id].totalTeachers ++;
            //
            //           /*assign a name property for filtering*/
            //           teacher.full_name = teacher.first_name + teacher.last_name;
            //
            //           if (teacher.gender == 'male') {
            //               $scope.selected_region.allSchools[teacher.school_id].totalMaleTeachers ++;
            //           }else{
            //               $scope.selected_region.allSchools[teacher.school_id].totalFemaleTeachers ++;
            //           }
            //       }
            //   }
            angular.forEach($scope.teacherSummaryData.teachersInfo, function (item, key) {
                if (item.class_taught === null || item.class_taught === 'null') item.class_taught = '';
                if (item.class_subject_taught === null ||item.class_subject_taught === 'null') item.class_subject_taught = '';

                item.subject = ($.trim(item.class_subject_taught +  item.class_taught)).split(',')[0];

                if (item.gender === 'male') {
                    $scope.teacherSummaryData.countryInfo.male_total ++;
                }else $scope.teacherSummaryData.countryInfo.female_total++;

                $scope.teacherSummaryData.countryInfo.teachers_total ++;


                if (angular.isUndefined(item.highest_professional_qualification)
                    || item.highest_professional_qualification === ''
                    || item.highest_professional_qualification.toLowerCase() === 'others'
                    || item.highest_professional_qualification.toLowerCase() === 'other') {

                }else{
                    $scope.teacherSummaryData.countryInfo.total_trained ++;
                }
            });
            $scope.teacherSummaryData.countryInfo.percentage_trained = Math.round(100 * ($scope.teacherSummaryData.countryInfo.total_trained / $scope.teacherSummaryData.countryInfo.teachers_total)) ;


            $scope.schoolLookup = scSchool.schoolLookup;

        }

        /*if the teachers are still being loaded, navigate to the parent page*/
        if ($scope.loadingTeachers) {
            $state.go('dashboard.view_country_details');
        }

        //An object to easily display between displaying the list of teachers and the grouped table
        $scope.view_to_show = {
            'list' : true,
            'summary' : false
        };
        //This function changes between displaying the list of teachers and the grouped table
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };

        //This function changes between displaying the table or charts for comparing regions
        $scope.changeCompareView = function(view_to_show){
            angular.forEach($scope.compare_view_to_show, function(val, prop){
                //set all view to false
                $scope.compare_view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.compare_view_to_show[view_to_show] = true;
        };

        $scope.$watch('teacherSummaryData.teachersInfo', function () {
            prepCountryTeachersInfo();
        });






    }]);