/**
 * Created by Kaygee on 23/04/2015.
 */

function incrementAvailability(schoolObj, prop){
    if (prop != '' || prop != null) {
        schoolObj[prop]++;
        return schoolObj;
    }
    return schoolObj
}

schoolMonitor.controller('scViewSelectedCountrySanitationController',  [
    '$rootScope', '$scope', '$state', '$stateParams', '$modal', 'school_years',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout','School',
    function($rootScope, $scope, $state, $stateParams, $modal, school_years,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout, School) {

        $scope.loadingSanitationData = false;

        $scope.fetchSanitationData = function(){
            if (! ($scope.sanitation_year && $scope.sanitation_term )) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingSanitationData = true;
            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.sanitation_year,
                term: $scope.sanitation_term,
                country_id: $stateParams.id
            };
            $scope.sanitationData = [];

            School.allSchoolSanitation(data_to_post).$promise
                .then(
                function (fufilledData) {
                    fufilledData.forEach(function (sanitation, index) {
                        var school = scSchool.schoolLookup[sanitation.school_id];
                        if (school != undefined) {
                            school = school.name
                        }else{
                            school = "Not available (deleted)"
                        }
                        var schoolObj = {
                            name : school,
                            school_id : sanitation.school_id,
                            district_id : sanitation.district_id,
                            "available" : 0,
                            "available not functional" : 0,
                            "not available" : 0,
                            no : 0,
                            data : sanitation
                        };
                        schoolObj = incrementAvailability(schoolObj, sanitation.dust_bins);
                        schoolObj = incrementAvailability(schoolObj, sanitation.toilet);
                        schoolObj = incrementAvailability(schoolObj, sanitation.urinal);
                        schoolObj = incrementAvailability(schoolObj, sanitation.veronica_buckets);
                        schoolObj = incrementAvailability(schoolObj, sanitation.water);

                        $scope.sanitationData.push(schoolObj);
                    });
                    $scope.loadingSanitationData = false;
                },
                function (rejectedData) {
                    $scope.loadingSanitationData = false;

                }
            )
        };

    }]);

schoolMonitor.controller('scViewSelectedCountrySecurityController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingSecurityData = false;

        $scope.fetchSecurityData = function(){
            if (!($scope.security_year && $scope.security_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingSecurityData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.security_year,
                term: $scope.security_term,
                country_id: $stateParams.id
            };
            $scope.securityData = [];

            School.allSchoolSecurity(data_to_post).$promise
                .then(
                function (fufilledData) {
                    fufilledData.forEach(function (security, index) {
                        var schoolObj = {
                            name : scSchool.schoolLookup[security.school_id].name,
                            school_id : security.school_id,
                            district_id : security.district_id,
                            "available" : 0,
                            "available not functional" : 0,
                            "not available" : 0,
                            data : security
                        };
                        schoolObj = incrementAvailability(schoolObj, $.trim(security.gated));
                        schoolObj = incrementAvailability(schoolObj, $.trim(security.lights));
                        schoolObj = incrementAvailability(schoolObj, $.trim(security.security_man));
                        schoolObj = incrementAvailability(schoolObj, $.trim(security.walled));

                        $scope.securityData.push(schoolObj);
                    });
                    $scope.loadingSecurityData = false;

                },
                function (rejectedData) {
                    $scope.loadingSecurityData = false;

                }
            )
        };
    }]);

schoolMonitor.controller('scViewSelectedCountrySportsController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingSportsData = false;

        $scope.fetchSportsData = function(){
            if (!($scope.sports_year && $scope.sports_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingSportsData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.sports_year,
                term: $scope.sports_term,
                country_id: $stateParams.id
            };
            $scope.sportsData = [];

            School.allSchoolRecreation_Equipment(data_to_post).$promise
                .then(function (fufilledData) {
                    fufilledData.forEach(function (sport, index) {
                        /*
                         * country_id: 1
                         comment: ""
                         country_id: 1
                         created_at: "2015-02-03 14:45:05"
                         data_collector_id: 5
                         data_collector_type: "head_teacher"
                         district_id: 1
                         id: 4
                         lat: "0"
                         long: "0"
                         first_aid_box: "available not functional"
                         football: "available"
                         merry_go_round: "available"
                         netball: "not available"
                         playing_field: "not available"
                         seesaw: "not available"
                         sports_wear: "available not functional"
                         volleyball: "available"
                         questions_category_reference_code: "SP"
                         region_id: 1
                         school_id: 2
                         school_reference_code: "u3BoEVL9q5s2gk5h"
                         term: "first_term"
                         updated_at: "2015-02-03 14:45:05"
                         year: "2014/2015"
                         */
                        var school = scSchool.schoolLookup[sport.school_id];
                        if (school != undefined) {
                            school = school.name
                        }else{
                            school = "Not available (deleted)"
                        }
                        var schoolObj = {
                            name : school,
                            school_id : sport.school_id,
                            district_id : sport.district_id,
                            "available" : 0,
                            "available not functional" : 0,
                            "not available" : 0,
                            data : sport
                        };
                        schoolObj= incrementAvailability(schoolObj, sport.first_aid_box);
                        schoolObj = incrementAvailability(schoolObj, sport.football);
                        schoolObj = incrementAvailability(schoolObj, sport.merry_go_round);
                        schoolObj = incrementAvailability(schoolObj, sport.netball);
                        schoolObj = incrementAvailability(schoolObj, sport.playing_field);
                        schoolObj = incrementAvailability(schoolObj, sport.seesaw);
                        schoolObj = incrementAvailability(schoolObj, sport.sports_wear);
                        schoolObj = incrementAvailability(schoolObj, sport.volleyball);

                        $scope.sportsData.push(schoolObj);
                    });
                    $scope.loadingSportsData = false;

                },
                function (rejectedData) {
                    $scope.loadingSportsData = false;

                }
            )
        };
    }]);

schoolMonitor.controller('scViewSelectedCountryStructureController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingStructureData = false;

        $scope.fetchStructureData = function(){
            if (!($scope.structure_year && $scope.structure_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingStructureData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.structure_year,
                term: $scope.structure_term,
                country_id: $stateParams.id
            };
            $scope.structureData = [];

            School.allSchoolStructure(data_to_post).$promise
                .then(
                function (fufilledData) {
                    /*
                     *
                     country_id: 1
                     comment: ""
                     country_id: 1
                     created_at: "2015-02-03 14:46:04"
                     data_collector_id: 5
                     data_collector_type: "head_teacher"
                     district_id: 1

                     id: 3
                     lat: "0"
                     long: "0"
                     questions_category_reference_code: "SP"
                     region_id: 1
                     school_id: 2
                     school_reference_code: "u3BoEVL9q5s2gk5h"
                     term: "first_term"
                     updated_at: "2015-02-03 14:46:04"
                     year: "2014/2015"*/
                    fufilledData.forEach(function (structure, index) {
                        var schoolObj = {
                            name : scSchool.schoolLookup[structure.school_id].name,
                            school_id : structure.school_id,
                            district_id : structure.district_id,
                            good : 0,
                            fair : 0,
                            poor : 0,
                            data : structure
                        };
                        schoolObj = incrementAvailability(schoolObj, $.trim(structure.doors));
                        schoolObj = incrementAvailability(schoolObj, $.trim(structure.floors));
                        schoolObj = incrementAvailability(schoolObj, $.trim(structure.blackboard));
                        schoolObj = incrementAvailability(schoolObj, $.trim(structure.walls));
                        schoolObj = incrementAvailability(schoolObj, $.trim(structure.windows));
                        $scope.structureData.push(schoolObj);
                    });
                    $scope.loadingStructureData = false;

                },
                function (rejectedData) {
                    $scope.loadingStructureData = false;
                }
            )
        };
    }]);

schoolMonitor.controller('scViewSelectedCountryFurnitureController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingFurnitureData = false;


        $scope.fetchFurnitureData = function(){
            if (!($scope.furniture_year && $scope.furniture_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingFurnitureData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.furniture_year,
                term: $scope.furniture_term,
                country_id: $stateParams.id
            };
            $scope.furnitureData = [];

            School.allSchoolFurniture(data_to_post).$promise
                .then(function (fufilledData) {
                    fufilledData.forEach(function (furniture, index) {
                        var school = scSchool.schoolLookup[furniture.school_id];
                        if (school != undefined) {
                            school = school.name
                        }else{
                            school = "Not available (deleted)"
                        }
                        var schoolObj = {
                            name : school,
                            school_id : furniture.school_id,
                            district_id : furniture.district_id,
                            "adequate" : 0,
                            "not adequate" : 0,
                            data : furniture
                        };
                        /*country_id: 1
                         comment: ""
                         country_id: 1
                         created_at: "2015-02-03 16:54:57"
                         data_collector_id: 101
                         data_collector_type: "head_teacher"
                         district_id: 1
                         id: 5
                         lat: "9.645476"
                         long: "-0.8266954"
                         questions_category_reference_code: "SP"
                         region_id: 1
                         school_id: 80
                         school_reference_code: "gQbIjwLPoTixBRgE"
                         classrooms_cupboard: "not adequate"
                         pupils_furniture: "not adequate"
                         teacher_chairs: "not adequate"
                         teacher_tables: "not adequate"
                         term: "first_term"
                         updated_at: "2015-02-03 16:54:57"
                         year: "2014/2015"*/
                        schoolObj= incrementAvailability(schoolObj, furniture.classrooms_cupboard);
                        schoolObj = incrementAvailability(schoolObj, furniture.pupils_furniture);
                        schoolObj = incrementAvailability(schoolObj, furniture.teacher_chairs);
                        schoolObj = incrementAvailability(schoolObj, furniture.teacher_tables);

                        $scope.furnitureData.push(schoolObj);
                    });
                    $scope.loadingFurnitureData = false;

                },
                function (rejectedData) {
                    $scope.loadingFurnitureData = false;

                }
            )
        };
    }]);