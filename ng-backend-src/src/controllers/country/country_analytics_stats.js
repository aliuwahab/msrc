//Created by Kaygee on 17/06/14.

schoolMonitor.controller('scViewSelectedCountryAnalyticsStatsController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'District', 'School', 'scReportGenerator','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', 'scGlobalService', 'msrc_datauri_logo',
    '$interval',
    function($rootScope, $scope, $state, $stateParams, District, School, scReportGenerator,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, scGlobalService, msrc_datauri_logo) {



        $scope.loadCountryStats = function () {
            var dataParams = {
                data_collector_type: "head_teacher",
                term: $scope.summary_term,
                year: $scope.summary_year,
                country_id: $stateParams.id
            };
            scReportGenerator.loadStats(dataParams);
            $scope.loading = true;
            $scope.isNotVirgin = true;
        };

        var performanceBarChartHolder = {};

        $scope.$on('pupilPerformanceDataLoaded', function (event, allCountryPupilPerformanceData) {
            var countryPerformanceTotals = {
                english: {
                    class_score: {}
                },
                math: {
                    class_score: {}
                },
                gh_lang: {
                    class_score: {}
                }
            };

            angular.forEach(allCountryPupilPerformanceData, function (item, index) {

                /*
                * average_english_score:"50"
                    average_ghanaian_language_score:"50"
                    average_maths_score:"50"
                    data_collector_type:"head_teacher"
                    district_id:97
                    level:"KG1"
                    num_pupil_who_score_above_average_in_english:24
                    num_pupil_who_score_above_average_in_ghanaian_language:20
                    num_pupil_who_score_above_average_in_maths:24
                    term:"third_term"
                    year:"2015/2016"
                    */
                //find the average score for the district, by subject
                //so for each item
                //add the averages of each subject, class by class
                //add the class in one property under the subject


                if (isFinite(Number(item.average_english_score))) {
                    if (angular.isDefined(countryPerformanceTotals.english.class_score[item.level])) {
                        countryPerformanceTotals.english.class_score[item.level].total_score += Number(item.average_english_score);
                        countryPerformanceTotals.english.class_score[item.level].average_score = Math.round(countryPerformanceTotals.english.class_score[item.level].total_score / $scope.selected_country.totalSchools);
                    } else {
                        countryPerformanceTotals.english.class_score[item.level] = {
                            total_score: Number(item.average_english_score),
                            average_score: Math.round(item.average_english_score / $scope.selected_country.totalSchools)
                        }
                    }
                }


                if (isFinite(Number(item.average_maths_score))) {
                    if (angular.isDefined(countryPerformanceTotals.math.class_score[item.level])) {

                        countryPerformanceTotals.math.class_score[item.level].total_score += Number(item.average_maths_score);
                        countryPerformanceTotals.math.class_score[item.level].average_score = Math.round(countryPerformanceTotals.math.class_score[item.level].total_score / $scope.selected_country.totalSchools);
                    } else {
                        countryPerformanceTotals.math.class_score[item.level] = {
                            total_score: Number(item.average_maths_score),
                            average_score: Math.round(item.average_maths_score / $scope.selected_country.totalSchools)
                        }
                    }
                }


                if (isFinite(Number(item.average_ghanaian_language_score))) {
                    if (angular.isDefined(countryPerformanceTotals.gh_lang.class_score[item.level])) {
                        countryPerformanceTotals.gh_lang.class_score[item.level].total_score += Number(item.average_ghanaian_language_score);
                        countryPerformanceTotals.gh_lang.class_score[item.level].average_score = Math.round(countryPerformanceTotals.gh_lang.class_score[item.level].total_score / $scope.selected_country.totalSchools);
                    } else {
                        countryPerformanceTotals.gh_lang.class_score[item.level] = {
                            total_score: Number(item.average_ghanaian_language_score),
                            average_score: Math.round(item.average_ghanaian_language_score / $scope.selected_country.totalSchools)
                        }
                    }
                }


                performanceBarChartHolder[item.level] = {
                    gh_lang : {
                        average_score : countryPerformanceTotals.gh_lang.class_score[item.level].average_score,
                        above_average : countryPerformanceTotals.gh_lang.class_score[item.level].above_average
                    },
                    english : {
                        average_score : countryPerformanceTotals.english.class_score[item.level].average_score,
                        above_average : countryPerformanceTotals.english.class_score[item.level].above_average
                    },
                    math : {
                        average_score : countryPerformanceTotals.math.class_score[item.level].average_score,
                        above_average : countryPerformanceTotals.math.class_score[item.level].above_average
                    }
                }

                //then add for the total in another property under the subject
                //

            });

            var performanceBarChart = {
                label : "Pupil Performance By Class",
                labels : [],
                datasets : []
            };
            var langHolder = {
                gh_lang : [],
                gh_lang_line : [],
                english : [],
                english_line : [],
                math : [],
                math_line: []
            };

            /*get the class key*/
            /*get all the language datasets*/
            /*get the above average data for the line*/
            /*get one object with dataset title, class by class and language score per class*/

            /*get the class key*/
            angular.forEach(performanceBarChartHolder, function (perfData, classKey) {
                performanceBarChart.labels.push(classKey);
                /*get all the language datasets*/
                langHolder.gh_lang.push(perfData.gh_lang.average_score);
                langHolder.gh_lang_line.push(perfData.gh_lang.above_average);
                langHolder.english.push(perfData.english.average_score);
                langHolder.english_line.push(perfData.english.above_average);
                langHolder.math.push(perfData.math.average_score);
                langHolder.math_line.push(perfData.math.above_average);
            });

            performanceBarChart.datasets.push({
                label: 'Ghanaian Language',
                backgroundColor: pattern.draw('triangle-inverted', 'rgba(231, 235, 8, 0.75)'),
                data:langHolder.gh_lang
            });

            performanceBarChart.datasets.push({
                label: 'English',
                backgroundColor: pattern.draw('diagonal-right-left', 'rgba(16, 204, 13, 0.75)'),
                data:langHolder.english
            });
            performanceBarChart.datasets.push({
                label: 'Maths',
                backgroundColor: pattern.draw('dot-dash', 'rgba(75, 192, 192, 0.75)'),
                data: langHolder.math
            });


            scGlobalService.drawStatisticsBarStackedChart(performanceBarChart.labels,
                performanceBarChart.label, performanceBarChart.datasets,
                'performanceBarChart', "horizontalBarChart");


            $scope.countryPerformanceTotals = angular.copy(countryPerformanceTotals);
            $scope.countryPerformanceTotals.loaded = true;
            $scope.loading = false;
        });

        $scope.$on('pupilEnrollmentDataLoaded', function (event, allCountryPupilEnrollmentData) {
            var countryEnrollmentTotals = {
                total_male: 0,
                total_female: 0,
                total_pupils: 0,
                total_by_class: {}
            };

            $scope.enrollmentBarChart = {
                label : "Pupil Enrollment By Class",
                labels : [],
                chartData : []
            };

            angular.forEach(allCountryPupilEnrollmentData, function (item, index) {
                /* normal: parseInt(item.total_enrolment),
                    special: 0,
                    boys : parseInt(item.num_males),
                    special_boys : 0,
                    girls : parseInt(item.num_females),
                    special_girls : 0,
                    week_number :  item.week_number,
                    term : item.term,
                    year : item.year
                 */


                countryEnrollmentTotals.total_male += Number(item.num_males);
                countryEnrollmentTotals.total_female += Number(item.num_females);
                countryEnrollmentTotals.total_pupils += Number(item.total_enrolment);

                if (angular.isDefined(countryEnrollmentTotals.total_by_class[item.level])) {
                    countryEnrollmentTotals.total_by_class[item.level].total_male += Number(item.num_males);
                    countryEnrollmentTotals.total_by_class[item.level].total_female += Number(item.num_females);
                    countryEnrollmentTotals.total_by_class[item.level].total_pupils += Number(item.total_enrolment);
                } else {
                    countryEnrollmentTotals.total_by_class[item.level] = {
                        total_male: Number(item.num_males),
                        total_female: Number(item.num_females),
                        total_pupils: Number(item.total_enrolment)
                    }
                }
            });

            angular.forEach(countryEnrollmentTotals.total_by_class, function(eachClass, prop){
                $scope.enrollmentBarChart.labels.push(prop);
                $scope.enrollmentBarChart.chartData.push(eachClass.total_pupils);
            });

            scGlobalService.drawStatisticsBarChart($scope.enrollmentBarChart.labels,
                $scope.enrollmentBarChart.label, $scope.enrollmentBarChart.chartData,
                'enrollmentBarChart', "barChart");


            $scope.countryEnrollmentTotals = angular.copy(countryEnrollmentTotals);
            $scope.countryEnrollmentTotals.loaded = true;
        });

        $scope.$on('specialPupilEnrollmentDataLoaded', function (event, allSchoolSpecialEnrollmentData) {
            var countrySpecialEnrollmentTotals = {
                total_male: 0,
                total_female: 0,
                total_pupils: 0,
                total_by_class: {}
            };

            angular.forEach(allSchoolSpecialEnrollmentData, function (item, index) {
                /*
                    level:"KG1"
                    num_females:0
                    num_males:3
                    num_of_streams:1
                    school_in_session:1
                    term:"second_term"
                    total_enrolment:3
                */

                countrySpecialEnrollmentTotals.total_male += Number(item.num_males);
                countrySpecialEnrollmentTotals.total_female += Number(item.num_females);
                countrySpecialEnrollmentTotals.total_pupils += Number(item.total_enrolment);

                if (angular.isDefined(countrySpecialEnrollmentTotals.total_by_class[item.level])) {
                    countrySpecialEnrollmentTotals.total_by_class[item.level].total_male += Number(item.num_males);
                    countrySpecialEnrollmentTotals.total_by_class[item.level].total_female += Number(item.num_females);
                    countrySpecialEnrollmentTotals.total_by_class[item.level].total_pupils += Number(item.total_enrolment);
                } else {
                    countrySpecialEnrollmentTotals.total_by_class[item.level] = {
                        total_male: Number(item.num_males),
                        total_female: Number(item.num_females),
                        total_pupils: Number(item.total_enrolment)
                    }
                }
            });

            $scope.countrySpecialEnrollmentTotals = angular.copy(countrySpecialEnrollmentTotals);
            $scope.countrySpecialEnrollmentTotals.loaded = true;
            $scope.loading = false;
            $scope.isNotVirgin = true;
        });


        function runPupilAttendanceData(allPupilAttendanceDataLoaded) {
            var countryAttendanceTotals = {
                total_attendance_for_term: 0,
                total_school_session: 0
            };

            var classTotalHolder = {};
            var daysInSessionTotalHolder = {};

            /*
                level:"KG1"
                num_females:93
                num_males:84
                number_of_week_days:4
                total:177

                loop over all the attendances
                add the attendances per class,
                add the number of days in session for that week
                then
                multiply total enrolment by the number of days school was in session

                divide the totalAttendance by expected attendance and multiply by 100
            */

            angular.forEach(allPupilAttendanceDataLoaded, function (attendanceItem, index) {
                var classConcat = attendanceItem.level + attendanceItem.term + attendanceItem.year;
                var daysInSesConcat = attendanceItem.week_number + attendanceItem.term + attendanceItem.year;

                if (classTotalHolder[classConcat]) {
                    classTotalHolder[classConcat] += attendanceItem.total;
                }else{
                    classTotalHolder[classConcat] = attendanceItem.total;
                }

                if (!daysInSessionTotalHolder[daysInSesConcat]) {
                    daysInSessionTotalHolder[daysInSesConcat] = attendanceItem.number_of_week_days;
                }
            });

            console.log(daysInSessionTotalHolder, classTotalHolder);
            // just for a week, the loop above prevents duplicate addition
            angular.forEach(classTotalHolder, function (classItem, index) {
                countryAttendanceTotals.total_attendance_for_term += classItem
            });

            angular.forEach(daysInSessionTotalHolder, function (weekItem, index) {
                countryAttendanceTotals.total_school_session += weekItem
            });



            $scope.countryAttendanceTotals = angular.copy(countryAttendanceTotals);
            $scope.countryAttendanceTotals.loaded = true;

            $scope.loading = false;
            $scope.isNotVirgin = true;
        }
        $scope.$on('allPupilAttendanceDataLoaded', function (event, allPupilAttendanceDataLoaded) {
            runPupilAttendanceData(allPupilAttendanceDataLoaded)
        });



        $scope.$on('teacherPunctualityLessonPlansDataLoaded', function (event, allCountryTeacherPupilPunctualityLessonPlansData) {
            var countryTeacherAttendanceTotals = {
                expectedAttendance: 0,
                actualAttendance: 0,
                attendanceRatio: 0,
                total_above_50: 0,
                adequately_marked_schools: 0,
                num_of_exercises_given: 0,
                num_of_exercises_marked: 0,
                country_exercises_marked_average: 0,
                total_by_teachers: {}
            };
            angular.forEach(allCountryTeacherPupilPunctualityLessonPlansData, function (item, index) {

                if (!isFinite(Number(item.num_of_exercises_given))) item.num_of_exercises_given = 0;
                if (!isFinite(Number(item.num_of_exercises_marked))) item.num_of_exercises_marked = 0;
                if (!isFinite(Number(item.days_present))) item.days_present = 0;
                if (!isFinite(Number(item.days_in_session))) item.days_in_session = 5;

                countryTeacherAttendanceTotals.actualAttendance += (item.days_present);
                countryTeacherAttendanceTotals.expectedAttendance += (item.days_in_session);
                if (isFinite(countryTeacherAttendanceTotals.actualAttendance / countryTeacherAttendanceTotals.expectedAttendance)) {
                    countryTeacherAttendanceTotals.attendanceRatio = Math.round(100 * (countryTeacherAttendanceTotals.actualAttendance / countryTeacherAttendanceTotals.expectedAttendance));
                }

                countryTeacherAttendanceTotals.num_of_exercises_given += (item.num_of_exercises_given);
                countryTeacherAttendanceTotals.num_of_exercises_marked += (item.num_of_exercises_marked);
                if (isFinite(countryTeacherAttendanceTotals.num_of_exercises_marked / countryTeacherAttendanceTotals.num_of_exercises_given)) {
                    countryTeacherAttendanceTotals.country_exercises_marked_average +=
                        Math.round(100 * (countryTeacherAttendanceTotals.num_of_exercises_marked / countryTeacherAttendanceTotals.num_of_exercises_given));
                }

                if (angular.isDefined(countryTeacherAttendanceTotals.total_by_teachers[item.school_id])) {

                    countryTeacherAttendanceTotals.total_by_teachers[item.school_id].actualAttendance += (item.days_present);
                    countryTeacherAttendanceTotals.total_by_teachers[item.school_id].expectedAttendance += (item.days_in_session);
                    if (isFinite(countryTeacherAttendanceTotals.total_by_teachers[item.school_id].actualAttendance / countryTeacherAttendanceTotals.total_by_teachers[item.school_id].expectedAttendance)) {
                        countryTeacherAttendanceTotals.total_by_teachers[item.school_id].attendanceRatio =
                            Math.round(100 * (countryTeacherAttendanceTotals.total_by_teachers[item.school_id].actualAttendance / countryTeacherAttendanceTotals.total_by_teachers[item.school_id].expectedAttendance));
                    }

                    countryTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_given += (item.num_of_exercises_given);
                    countryTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_marked += (item.num_of_exercises_marked);

                    if (isFinite(countryTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_marked / countryTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_given)) {
                        countryTeacherAttendanceTotals.total_by_teachers[item.school_id].exercises_marked_average +=
                            Math.round(100 * (countryTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_marked / countryTeacherAttendanceTotals.total_by_teachers[item.school_id].num_of_exercises_given))
                    }

                } else {
                    var attendanceRatio = 0;
                    var exercisesMarkedAverage = 0;
                    if (isFinite(item.days_present / item.days_in_session)) attendanceRatio = item.days_present / item.days_in_session;
                    if (isFinite(item.num_of_exercises_marked / item.num_of_exercises_given)) exercisesMarkedAverage = item.num_of_exercises_marked / item.num_of_exercises_given

                    countryTeacherAttendanceTotals.total_by_teachers[item.school_id] = {
                        name_of_school: scSchool.schoolLookup[item.school_id].name,
                        expectedAttendance: parseInt(item.days_in_session),
                        actualAttendance: parseInt(item.days_present),
                        attendanceRatio: Math.round(100 * attendanceRatio),
                        num_of_exercises_given: item.num_of_exercises_given,
                        num_of_exercises_marked: item.num_of_exercises_marked,
                        exercises_marked_average: Math.round(100 * (exercisesMarkedAverage))
                    }
                }
            });

            /*Find the total number of schools with attendance over 50%*/
            angular.forEach(countryTeacherAttendanceTotals.total_by_teachers, function (attendanceBySchool, index) {
                if (attendanceBySchool.attendanceRatio > 50) {
                    countryTeacherAttendanceTotals.total_above_50++;
                }

                if (attendanceBySchool.exercises_marked_average > countryTeacherAttendanceTotals.country_exercises_marked_average) {
                    countryTeacherAttendanceTotals.adequately_marked_schools++;
                }

            });


            $scope.countryTeacherAttendanceTotals = angular.copy(countryTeacherAttendanceTotals);
            $scope.countryTeacherAttendanceTotals.loaded = true;
        });

        $scope.$on('recordBooksDataLoaded', function (event, allCountryRecordBooksData) {
            var countryRecordBooks = {
                admission_register: {
                    name: "Admission Register",
                    total: 0
                },
                class_registers: {
                    name: "Class Registers",
                    total: 0
                },
                teacher_attendance_register: {
                    name: "Teacher Attendance Register",
                    total: 0
                },
                visitors: {
                    name: "Visitors\' Book",
                    total: 0
                },
                log: {
                    name: "Log Book",
                    total: 0
                },
                sba: {
                    name: "SBA",
                    total: 0
                },
                movement: {
                    name: "Movement Book",
                    total: 0
                },
                spip: {
                    name: "SPIP",
                    total: 0
                },
                inventory_books: {
                    name: "Inventory Books",
                    total: 0
                },
                cummulative_records_books: {
                    name: "Cumulative Record Books",
                    total: 0
                },
                continous_assessment: {
                    name: "Continuous Assessment / SMB",
                    total: 0
                },
                schools: {},
                schools_with_books: 0
            };
            angular.forEach(allCountryRecordBooksData, function (item, index) {
                angular.forEach(countryRecordBooks, function (itemValue, itemProp) {

                    if (item[itemProp] === 'yes') {
                        countryRecordBooks[itemProp].total++;
                        if (countryRecordBooks.schools[item.school_id]) {
                            countryRecordBooks.schools[item.school_id].books_available.push(itemProp)
                        } else {
                            countryRecordBooks.schools[item.school_id] = {
                                books_available: [itemProp],
                                school_name: scSchool.schoolLookup[item.school_id].name
                            }
                        }
                    }
                })
            });
            /*
            *        booksHolder.push({
         name: "Admission Register",
         status : item.admission_register,
         data_collector_type : item.data_collector_type

         name: "Class Registers",
         status : item.class_registers,

         name: "Teacher Attendance Register",
         status: item.teacher_attendance_register,

         name: "Visitors\' Book",
         status: item.visitors,

         name: "Log Book",
         status: item.log,

         name: "SBA",
         status: item.sba,

         name: "Movement Book",
         status: item.movement,

         name: "SPIP",
         status: item.spip,

         name: "Inventory Books",
         status: item.inventory_books,

         name: "Cumulative Record Books",
         status: item.cummulative_records_books,

         name: "Continuous Assessment / SMB",
         status: item.continous_assessment,
         */


            angular.forEach(countryRecordBooks.schools, function (school, index) {
                if (school.books_available.length > 5) {
                    countryRecordBooks.schools_with_books++;
                    countryRecordBooks.percentage_of_schools_with_books = Math.round(100 * (countryRecordBooks.schools_with_books / $scope.selected_country.totalSchools));
                }
            });

            $scope.countryRecordBooks = angular.copy(countryRecordBooks);
        });

        $scope.$on('grantCapitationPaymentsDataLoaded', function (event, countryGrantCapitationPaymentsData) {
            var countryGrantStuff = {
                totalAmount : 0,
                total_first : 0,
                total_second : 0,
                total_third : 0,
                schools_received : 0,
                percentage_received : 0,
                schools : {}
            };

            angular.forEach(countryGrantCapitationPaymentsData, function (item, index) {
                if (angular.isDefined(item.first_tranche_amount) && item.first_tranche_amount > 0) {
                    countryGrantStuff.totalAmount += parseInt(item.first_tranche_amount);
                    countryGrantStuff.total_first += parseInt(item.first_tranche_amount);
                }
                if (angular.isDefined(item.second_tranche_amount) && item.second_tranche_amount > 0) {
                    countryGrantStuff.totalAmount += parseInt(item.second_tranche_amount);
                    countryGrantStuff.total_second += parseInt(item.second_tranche_amount);
                }
                if (angular.isDefined(item.third_tranche_amount) && item.third_tranche_amount > 0) {
                    countryGrantStuff.totalAmount += parseInt(item.third_tranche_amount);
                    countryGrantStuff.total_third += parseInt(item.third_tranche_amount);
                }

                if (countryGrantStuff.schools[item.school_id]) {
                    countryGrantStuff.schools[item.school_id].first_tranche_amount += parseInt(item.first_tranche_amount);
                    countryGrantStuff.schools[item.school_id].second_tranche_amount += parseInt(item.second_tranche_amount);
                    countryGrantStuff.schools[item.school_id].third_tranche_amount += parseInt(item.third_tranche_amount);
                    countryGrantStuff.schools[item.school_id].total += parseInt(item.first_tranche_amount) + parseInt(item.second_tranche_amount) + parseInt(item.third_tranche_amount)

                }else{
                    countryGrantStuff.schools[item.school_id] = {
                        total : parseInt(item.first_tranche_amount) + parseInt(item.second_tranche_amount) + parseInt(item.third_tranche_amount),
                        first_tranche_amount : parseInt(item.first_tranche_amount),
                        second_tranche_amount : parseInt(item.second_tranche_amount),
                        third_tranche_amount : parseInt(item.third_tranche_amount)
                    }
                }
                /*
                    grant.first_tranche_date
                    grant.first_tranche_amount
                    grant.second_tranche_date
                    grant.second_tranche_amount
                    grant.third_tranche_date
                    grant.third_tranche_amount
                */
            });

            angular.forEach(countryGrantStuff.schools, function (school, index) {
                if (angular.isDefined(school.total) && school.total > 0) {
                    countryGrantStuff.schools_received ++;
                }
            });
            countryGrantStuff.percentage_received = Math.round(100 * (countryGrantStuff.schools_received / $scope.selected_country.totalSchools));

            $scope.countryGrantStuff = angular.copy(countryGrantStuff);
        });

        $scope.$on('supportTypesDataLoaded', function (event, countrySupportData) {
            var countrySupportStuff = {
                donor_support : {
                    name: "Donor Support",
                    cash : 0,
                    kind : ''
                },
                community_support: {
                    name: "Community Support",
                    cash : 0,
                    kind : ''
                },
                district_support: {
                    name: "District Support",
                    cash : 0,
                    kind : ''
                },
                pta_support: {
                    name: "PTA Support",
                    cash : 0,
                    kind : ''
                },
                other_support: {
                    name: "Other",
                    cash : 0,
                    kind : ''
                },
                total_cash : 0,
                schools : {},
                received_support_tracker : [],
                received_support : 0,
                percentage_received_support : 0
            };
            /*
            name: "Donor Support",
            in_cash : item.donor_support_in_cash,
            in_kind : item.donor_support_in_kind,

            name: "Community Support",
            in_cash : item.community_support_in_cash,
            in_kind : item.community_support_in_kind,

            name: "District Support",
            in_cash : item.district_support_in_cash,
            in_kind : item.district_support_in_kind,

            name: "PTA Support",
            in_cash : item.pta_support_in_cash,
            in_kind : item.pta_support_in_kind,
            comment : item.comment


            name: "Other",
            in_cash : item.other_support_in_cash,
            in_kind : item.other_support_in_kind,

        });*/
            angular.forEach(countrySupportData, function (item, index) {

                angular.forEach(countrySupportStuff, function (itemVal, itemProp) {

                    countrySupportStuff.total_cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                    countrySupportStuff[itemProp].cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                    countrySupportStuff[itemProp].kind  = angular.isDefined(item[itemProp + '_in_kind']);

                    if (isFinite(Number(item[itemProp + '_in_cash'])) ||  Number(item[itemProp + '_in_cash']) > 0 || angular.isDefined(item[itemProp + '_in_kind'])) {
                        if (countrySupportStuff.received_support_tracker.indexOf(item.school_id) < 0) {
                            countrySupportStuff.received_support_tracker.push(item.school_id);
                            countrySupportStuff.received_support ++;
                            countrySupportStuff.percentage_received_support = parseInt(100 * (countrySupportStuff.received_support / $scope.selected_country.totalSchools));
                        }
                    }

                    if( countrySupportStuff.schools[item.school_id]){
                        countrySupportStuff.schools[item.school_id].total += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                        countrySupportStuff.schools[item.school_id].in_kind = angular.isDefined(item[itemProp + '_in_kind']);

                    }else{
                        countrySupportStuff.schools[item.school_id] = {};
                        countrySupportStuff.schools[item.school_id].name_of_school = scSchool.schoolLookup[item.school_id].name;
                        countrySupportStuff.schools[item.school_id].total = isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                        countrySupportStuff.schools[item.school_id].in_kind = angular.isDefined(item[itemProp + '_in_kind']);
                    }
                });

            });
            $scope.countrySupportStuff = angular.copy(countrySupportStuff);
        });

        $scope.$on('furnitureDataLoaded', function (event, countrySchoolFurnitureItems) {
            var countryFurniture = {
                schools : {},
                total_schools_with_adequates : 0,
                adequate_percentage : 0
            };
            /*
            *    furnitureHolder.push({
            name: "Pupils Furniture",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.pupils_furniture],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });
        furnitureHolder.push({
            name: "Teacher Tables",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.teacher_tables],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });
        furnitureHolder.push({
            name: "Teacher Chairs",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.teacher_chairs],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });
        furnitureHolder.push({
            name: "Classrooms Cupboard",
            term :   furnitureObject.term,
            status : $scope.formatAdequecy[furnitureObject.classrooms_cupboard],
            year : furnitureObject.year,
            comment : furnitureObject.comment,
            data_collector_type : furnitureObject.data_collector_type
        });

        */

            var adequacyElems = ['pupils_furniture','teacher_tables','teacher_chairs','classrooms_cupboard'];

            angular.forEach(countrySchoolFurnitureItems, function (item, index) {
                countryFurniture.schools[item.school_id] = {
                    adequacy_count : 0
                };

                for (var i = 0; i < adequacyElems.length; i++) {
                    var adequateProp = adequacyElems[i];

                    if (item[adequateProp] === 'adequate') {
                        countryFurniture.schools[item.school_id].adequacy_count++;
                    }

                }
                if (countryFurniture.schools[item.school_id].adequacy_count >= 3) {
                    countryFurniture.total_schools_with_adequates ++;
                    countryFurniture.adequate_percentage = parseInt(100 * (countryFurniture.total_schools_with_adequates / $scope.selected_country.totalSchools));
                }

            });
            $scope.countryFurniture = angular.copy(countryFurniture);
        });

        $scope.$on('structureDataLoaded', function (event, countrySchoolStructureItems) {
            var countryStructure = {
                schools : {},
                total_schools_with_goods : 0,
                good_percentage : 0
            };
            /*structureHolder.push({
                name: "Walls",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.walls],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Doors",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.doors],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Windows",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.windows],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Floors",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.floors],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
            structureHolder.push({
                name: "Blackboard",
                term :   structureObject.term,
                status : $scope.formatGoodPoorFair[structureObject.blackboard],
                year : structureObject.year,
                comment : structureObject.comment,
                data_collector_type : structureObject.data_collector_type
            });
                structureHolder.push({
                    name: "Illumination",
                    term :   structureObject.term,
                    status : $scope.formatGoodPoorFair[structureObject.illumination],
                    year : structureObject.year,
                    comment : structureObject.comment,
                    data_collector_type : structureObject.data_collector_type
                });
*/
            var goodElems = ['walls','doors','windows','floors','blackboard','illumination'];

            angular.forEach(countrySchoolStructureItems, function (item, index) {
                countryStructure.schools[item.school_id] = {
                    good_count : 0
                };

                for (var i = 0; i < goodElems.length; i++) {
                    var statusProp = goodElems[i];

                    if (item[statusProp] === 'good') {
                        countryStructure.schools[item.school_id].good_count++;
                    }
                }
                if (countryStructure.schools[item.school_id].good_count >= 4) {
                    countryStructure.total_schools_with_goods ++;
                    countryStructure.good_percentage = parseInt(100 * (countryStructure.total_schools_with_goods / $scope.selected_country.totalSchools));
                }
            });

            $scope.countryStructure = angular.copy(countryStructure);
        });

        $scope.$on('sanitationDataLoaded', function (event, countrySanitationItems) {
            var countrySanitation = {
                schools : {},
                total_schools_with_available : 0,
                available_percentage : 0
            };
            /*
            *   sanitationHolder.push({
           name: "Toilet",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.toilet],
           year :sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Urinal",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.urinal],
           year :sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Water",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.water],
           year :sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Dust Bins",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[sanitationObject.dust_bins],
           year : sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
       sanitationHolder.push({
           name: "Veronica Buckets",
           term :   sanitationObject.term,
           status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
           year :  sanitationObject.year,
           comment : sanitationObject.comment,
           data_collector_type : sanitationObject.data_collector_type
       });
            * */

            var availabilityItems = ['toilet','urinal','water','dust_bins','veronica_buckets'];

            angular.forEach(countrySanitationItems, function (item, index) {
                countrySanitation.schools[item.school_id] = {
                    available_count : 0
                };

                for (var i = 0; i < availabilityItems.length; i++) {
                    var statusProp = availabilityItems[i];

                    if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                        countrySanitation.schools[item.school_id].available_count++;
                    }
                }
                if (countrySanitation.schools[item.school_id].available_count >= 4) {
                    countrySanitation.total_schools_with_available ++;
                    countrySanitation.available_percentage = parseInt(100 * (countrySanitation.total_schools_with_available / $scope.selected_country.totalSchools));
                }
            });
            $scope.countrySanitation = angular.copy(countrySanitation);
        });

        $scope.$on('recreationDataLoaded', function (event, countryRecreationItems) {
            var countryRecreation = {
                schools : {},
                total_schools_with_available : 0,
                available_percentage : 0
            };
            /*
            *   recreationHolder.push({
           name: "Football",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.football],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Volleyball",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.volleyball],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Netball",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.netball],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Playing Field",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.playing_field],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       recreationHolder.push({
           name: "Sports Wear",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.sports_wear],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });
       try{
           recreationHolder.push({
               name: "Seesaw",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.seesaw],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           })
           recreationHolder.push({
               name: "Merry-Go-Round",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.merry_go_round],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
       recreationHolder.push({
           name: "First Aid Box",
           term :   recreationObject.term,
           status : $scope.formatAvailability[recreationObject.first_aid_box],
           year : recreationObject.year,
           comment : recreationObject.comment,
           data_collector_type : recreationObject.data_collector_type
       });

            * */

            var availabilityItems = ['football','volleyball','netball','playing_field','sports_wear','seesaw','merry_go_round','first_aid_box'];

            angular.forEach(countryRecreationItems, function (item, index) {

                countryRecreation.schools[item.school_id] = {
                    available_count : 0
                };

                for (var i = 0; i < availabilityItems.length; i++) {
                    var statusProp = availabilityItems[i];

                    if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                        countryRecreation.schools[item.school_id].available_count++;
                    }
                }
                if (countryRecreation.schools[item.school_id].available_count > 7) {
                    countryRecreation.total_schools_with_available ++;
                    countryRecreation.available_percentage = parseInt(100 * (countryRecreation.total_schools_with_available / $scope.selected_country.totalSchools));
                }
            });
            $scope.countryRecreation = angular.copy(countryRecreation);

        });

        $scope.$on('securityDataLoaded', function (event, countrySecurityItems) {
            var countrySecurity = {
                schools : {},
                total_schools_with_available : 0,
                available_percentage : 0
            };
            /*
            *  securityHolder.push({
            name: "Walled / Fenced",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.walled],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });

        securityHolder.push({
            name: "Gated",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.gated],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });
        securityHolder.push({
            name: "Lights",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.lights],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });
        securityHolder.push({
            name: "Security Man",
            term :   securityObject.term,
            status : $scope.formatAvailability[securityObject.security_man],
            year : securityObject.year,
            comment : securityObject.comment,
            data_collector_type : securityObject.data_collector_type
        });

            * */
            var availabilityItems = ['security_man','lights','gated','walled'];

            angular.forEach(countrySecurityItems, function (item, index) {

                countrySecurity.schools[item.school_id] = {
                    available_count : 0
                };

                for (var i = 0; i < availabilityItems.length; i++) {
                    var statusProp = availabilityItems[i];

                    if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                        countrySecurity.schools[item.school_id].available_count++;
                    }
                }
                if (countrySecurity.schools[item.school_id].available_count >= 3) {
                    countrySecurity.total_schools_with_available ++;
                    countrySecurity.available_percentage = parseInt(100 * (countrySecurity.total_schools_with_available / $scope.selected_country.totalSchools));
                }
            });
            $scope.countrySecurity = angular.copy(countrySecurity);
        });

        $scope.$on('meetingDataLoaded', function (event, countrySchoolMeetingItems) {
            var countryMeeting = {
                schools : {},
                total_schools_with_frequency : 0,
                frequency_percentage : 0
            };
            /*
            *
       meetingsHolder.push({
           name: "Staff Meeting",
           frequency: meetingObject.number_of_staff_meeting,
           males_present: meetingObject.num_males_present_at_staff_meeting,
           females_present: meetingObject.num_females_present_at_staff_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

       meetingsHolder.push({
           name: "SPAM",
           frequency: meetingObject.number_of_spam_meeting,
           males_present: meetingObject.num_males_present_at_spam_meeting,
           females_present: meetingObject.num_females_present_at_spam_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

       meetingsHolder.push({
           name: "PTA",
           frequency: meetingObject.number_of_pta_meeting,
           males_present: meetingObject.num_males_present_at_pta_meeting,
           females_present: meetingObject.num_females_present_at_pta_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

       meetingsHolder.push({
           name: "SMC",
           frequency: meetingObject.number_of_smc_meeting,
           males_present: meetingObject.num_males_present_at_smc_meeting,
           females_present: meetingObject.num_females_present_at_smc_meeting,
           week :   meetingObject.week_number,
           term :   meetingObject.term,
           year : meetingObject.year,
           data_collector_type : meetingObject.data_collector_type,
           comment : meetingObject.comment
       });

            * */
            var frequencyItems = ['number_of_smc_meeting','number_of_pta_meeting','number_of_spam_meeting','number_of_staff_meeting'];

            angular.forEach(countrySchoolMeetingItems, function (item, index) {
                countryMeeting.schools[item.school_id] = {
                    frequency_count : 0
                };

                for (var i = 0; i < frequencyItems.length; i++) {
                    var freqProp = frequencyItems[i];

                    if (isFinite(Number(item[freqProp])) && Number(item[freqProp]) > 0) {
                        countryMeeting.schools[item.school_id].frequency_count++;
                    }
                }
                if (countryMeeting.schools[item.school_id].frequency_count >= 3) {
                    countryMeeting.total_schools_with_frequency ++;
                    countryMeeting.frequency_percentage = parseInt(100 * (countryMeeting.total_schools_with_frequency / $scope.selected_country.totalSchools));
                }
            });
            $scope.countryMeeting = angular.copy(countryMeeting);
        });

        $scope.$on('communityDataLoaded', function (event, countryCommunityItems) {
            var countryCommInvolvement = {
                schools : {},
                total_schools_with_goods : 0,
                good_percentage : 0
            };

            /*
            *       var format_term = $scope.academicTermDisplay(communityObject.term);
        communityHolder.push({
            name: "PTA Involvement",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.pta_involvement],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });
        communityHolder.push({
            name: "Parents Notified of Student Progress",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.parents_notified_of_student_progress],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });
        communityHolder.push({
            name: "Community Sensitization on School Attendance",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.community_sensitization_for_school_attendance],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });
        communityHolder.push({
            name: "School Management Committees",
            term :   communityObject.term,
            status : $scope.formatGoodPoorFair[communityObject.school_management_committees],
            year : communityObject.year,
            data_collector_type : communityObject.data_collector_type
        });

            * */
            var goodElems = ['school_management_committees','community_sensitization_for_school_attendance','parents_notified_of_student_progress','pta_involvement'];

            angular.forEach(countryCommunityItems, function (item, index) {
                countryCommInvolvement.schools[item.school_id] = {
                    good_count : 0
                };

                for (var i = 0; i < goodElems.length; i++) {
                    var statusProp = goodElems[i];

                    if (item[statusProp] === 'good') {
                        countryCommInvolvement.schools[item.school_id].good_count++;
                    }
                }
                if (countryCommInvolvement.schools[item.school_id].good_count >= 3) {
                    countryCommInvolvement.total_schools_with_goods ++;
                    countryCommInvolvement.good_percentage = parseInt(100 * (countryCommInvolvement.total_schools_with_goods / $scope.selected_country.totalSchools));
                }
            });
            $scope.countryCommInvolvement = angular.copy(countryCommInvolvement);

        });




        $scope.$watchGroup(['teacherSummaryData.countryInfo.teachers_total', 'countryEnrollmentTotals.total_pupils'], function(newVal, oldVal) {
            if (angular.isDefined(newVal[0]) && newVal[0] > 0) {

                if (angular.isDefined(newVal[1]) && newVal[1] > 0) {

                    $scope.pupilTeacherRatio = Math.round((newVal[1]) / (newVal[0]))
                }
            }
            // else
            // $scope.$scope.countryEnrollmentTotals.total_pupils /
        });

        $scope.$watchGroup(['countryAttendanceTotals', 'countryEnrollmentTotals.total_pupils'], function(newVal, oldVal) {

            if ($scope.countryAttendanceTotals && $scope.countryEnrollmentTotals) {

                /*  multiply total enrolment by the number of days school was in session

                  divide the totalAttendance by expected attendance and multiply by 100*/
                var expectedAttendance = $scope.countryEnrollmentTotals.total_pupils * $scope.countryAttendanceTotals.total_school_session;

                $scope.countryAttendanceTotals.attendance_ratio = Math.round(($scope.countryAttendanceTotals.total_attendance_for_term / expectedAttendance) * 100) ;
                console.log('watch end: ', $scope.countryAttendanceTotals);

            }


        });

        $scope.downloadPDF = function (){

            var pdfDocVariables = {
                title : 'mSRC Country Report',
                entity_name : $scope.selected_country.name + " (Region)",
                entity : $scope.selected_country,
                upper_level_name: "Ghana",

                term_in_scope : $scope.one_school_terms[$scope.summary_term].label,

                top_box_left: '' + $scope.countryEnrollmentTotals.total_pupils,
                top_box_middle : '',
                top_box_up_middle_up: '' +$scope.countryEnrollmentTotals.total_male,
                top_box_up_middle_down:  '' + $scope.countryEnrollmentTotals.total_female,
                top_box_right: 'Number of Teachers : ' + $scope.teacherSummaryData.countryInfo.teachers_total,

                top_box_down_left:   $scope.countryAttendanceTotals.attendance_ratio + "%",

                top_box_down_middle_up: '' + $scope.countrySpecialEnrollmentTotals.total_male || 0,
                top_box_down_middle_down:  '' + $scope.countrySpecialEnrollmentTotals.total_female || 0,

                percentage_trained_teachers: $scope.teacherSummaryData.countryInfo.percentage_trained + '',

                english_lang_performance_array : $scope.countryPerformanceTotals.english.class_score,
                gh_lang_performance_array : $scope.countryPerformanceTotals.gh_lang.class_score,
                maths_lang_performance_array : $scope.countryPerformanceTotals.math.class_score,
                teacher_pupil_ratio : ($scope.pupilTeacherRatio + "  :  1"),
                // teacher_attendance_ratio : ($scope.countryTeacherAttendanceTotals.attendanceRatio || 0) + '%',
                teacher_attendance_ratio :  '-',
                punctuality_title : 'Schools with Teacher',
                // teacher_punctuality : ($scope.countryTeacherAttendanceTotals.total_above_50 || 0)+ '%',
                teacher_punctuality : '-',
                adequate_marked_title : "Schools with Adequately",
                // adequately_marked_exercises : ($scope.countryTeacherAttendanceTotals.adequately_marked_schools || 0) + '%',
                adequately_marked_exercises : "-",
                schools_with_or_number_of_title : "Schools with",
                schools_that_or_amount_of_title : "Schools that",
                schools_that_received_or_any_type_of_support : "Schools that received",
                record_books : ($scope.countryRecordBooks.percentage_of_schools_with_books || 0) + '%',
                grant_received : ($scope.countryGrantStuff.percentage_received || 0) + "%",
                support_received : ($scope.countrySupportStuff.percentage_received_support || 0) + "%",
                adequate_furniture : ($scope.countryFurniture.adequate_percentage || 0) + '%',
                meetings_title : "Schools that Hold Meetings" || 'Number of Meetings Held',
                community_title : "Schools with Active Community\n         \tParticipation",
                good_structure : ($scope.countryStructure.good_percentage || 0) + '%',
                sanitation_facilities : ($scope.countrySanitation.available_percentage) + "%",
                recreational_facilities : ($scope.countryRecreation.available_percentage) + "%",
                security_facilities : ($scope.countrySecurity.available_percentage) + "%",
                meetings : ($scope.countryMeeting.frequency_percentage) + "%",
                community_participation : ($scope.countryCommInvolvement.good_percentage) + "%",
                pdf_export_name : $scope.selected_country.name + '_' +$scope.summary_term + '_country_report.pdf'
            };



            scReportGenerator.countryDownloadPDF(pdfDocVariables)
        };

    }]);
