/**
 * Created by Kaygee on 23/04/2015.
 */


schoolMonitor.controller('scViewSelectedCircuitTeachersController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'DataCollectorHolder', 'Report', 'School', 'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', '$interval', '$timeout',
    function($rootScope, $scope, $state, $stateParams, DataCollectorHolder, Report, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        function  prepCircuitTeachersInfo(){
            //reset the allSchools teacher attendance array so that it prevent duplicates when switching between pages
            angular.forEach($scope.selected_circuit.allSchools, function(schoolHolder, index){
                schoolHolder.teacherAttendanceAverages = [];
            });


            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'list' : true,
                'summary' : false
            };

            $scope.compare_view_to_show = {
                'chart' : true,
                'table' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;

        }

        //This function changes between displaying the list of teachers and the grouped table
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };

        //This function changes between displaying the table or charts for comparing circuits
        $scope.changeCompareView = function(view_to_show){
            angular.forEach($scope.compare_view_to_show, function(val, prop){
                //set all view to false
                $scope.compare_view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.compare_view_to_show[view_to_show] = true;
        };

        /*if teachers are still being loaded dont show this view*/
        if ($scope.loadingTeachers) {
            $state.go('dashboard.view_selected_circuit');
            return;
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepCircuitTeachersInfo()
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scCircuit.circuits.length&& scCircuit.circuitLookup) {
            prepCircuitTeachersInfo()
        }



        //This variable is for a single district rating's ng-show
        $scope.circuitAverage = true;
        //This variable is for a comparing regional  district rating's ng-show
        $scope.compareCircuitDistrictAverages = false;

        //Shuffle between a single district rating and region districts
        $scope.compareAverages =function (){
            $scope.circuitAverage = false;
            $scope.compareCircuitDistrictAverages = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scDistrict.districtLookup[$scope.selected_circuit.district_id].allCircuitsHolder, 'name',
                    'Circuits under ' + scDistrict.districtLookup[$scope.selected_circuit.district_id].name, 'circuitDistrictAverageBarChart', 'ht_average');
                chart.validateNow();
            }, 100, 1)
        };

        $scope.singleAverage = function(){
            $scope.circuitAverage = true;
            $scope.compareCircuitDistrictAverages = false;
        };

        function processTeacherAttendanceAverage(loadedWeeklyData){

            $scope.teacherAttendanceSummaryData = {
                attendanceAverageInfo : loadedWeeklyData,
                circuitInfo : {}
            };

            var circuit_totals = {
                average_teacher_attendance : {
                    head_teacher : 0,
                    circuit_supervisor : 0
                },
                average_teacher_attendance_holder : {
                    head_teacher : 0,
                    circuit_supervisor : 0
                }
            };

            angular.forEach(loadedWeeklyData, function (value, key) {

                circuit_totals.average_teacher_attendance_holder[value.data_collector_type] =
                    parseFloat(circuit_totals.average_teacher_attendance_holder[value.data_collector_type]) + parseFloat(value.average_teacher_attendance);

                circuit_totals.average_teacher_attendance[value.data_collector_type] =  parseFloat(circuit_totals.average_teacher_attendance_holder[value.data_collector_type]) / parseInt($scope.teacherSummaryData.teachersInfo.length);

                scSchool.schoolLookup[value.school_id].average_teacher_attendance[value.data_collector_type] = value.average_teacher_attendance;
            });

            $scope.teacherAttendanceSummaryData.circuitInfo = circuit_totals;
        }


        /*$scope.fetchAttendanceAverageSummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;

            console.log('attTerm + attYear : ', $scope);
            var data_to_post = {
                year : $scope.attYear,
                term : $scope.attTerm,
                // week_number : $scope.summary_week,
                circuit_id : $stateParams.id
            };

            /!*else is for loading everything*!/
            School.allSchoolTeachersPunctuality_LessonPlan(data_to_post).$promise
                .then(function (punctuality_lesson_plans) {
                    scSchool.circuitAllSchoolTeachersPunctuality_LessonPlan = punctuality_lesson_plans;
                    $scope.prepTeachers();
                    $scope.loadingSummary= false;
                });

            /!*Report.totalsWeekly(data_to_post).$promise
                .then(
                    /!*success function*!/
                    function(loadedWeeklyData){
                        processTeacherAttendanceAverage(loadedWeeklyData);
                    },

                    /!*error function*!/
                    function(){

                    })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
*!/

        };
*/
    }]);