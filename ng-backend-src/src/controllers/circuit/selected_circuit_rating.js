/**
 * Created by Kaygee on 23/04/2015.
 */
schoolMonitor.controller('scViewSelectedCircuitRatingController',  [
    '$rootScope', '$scope', '$state', '$stateParams', '$modal', 'DataCollectorHolder',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, $modal, DataCollectorHolder,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {


        //Shuffle between a single circuits rating and districts' circuits
        $scope.compareRatings =function (){
            $scope.view_to_show = {
                'chart' : true,
                'table' : false
            };
            $scope.circuitRating = false;
            $scope.compareDistrictCircuitRating = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scDistrict.districtLookup[$scope.selected_circuit.district_id].allCircuitsHolder, 'name',
                    'Circuits under ' + scDistrict.districtLookup[$scope.selected_circuit.district_id].name, 'districtsCircuitRatingBarChart');
                chart.validateNow();
            }, 100, 1)
        };

        //Shuffle between a single district rating and region districts
        $scope.singleRating = function(){
            $scope.circuitRating = true;
            $scope.compareDistrictCircuitRating = false;
        };

        //An object to easily display between table and graph views under comparing districts
        $scope.view_to_show = {
            'chart' : true,
            'table' : false
        };
        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                $scope.view_to_show[prop] = false;
            });
            $scope.view_to_show[view_to_show] = true
        };

        function prepCircuitRating() {
            $scope.selected_circuit = scCircuit.circuitLookup[$stateParams.id];

            //This variable is for a single district rating's ng-show
            $scope.circuitRating = true;
            //This variable is for a comparing regional  district rating's ng-show
            $scope.compareDistrictCircuitRating = false;


            //Prepare the district rating bar chart
            $scope.tempArray =[{
                field : 'Inclusiveness',
                value : $scope.selected_circuit.inclusiveness,
                remaining : 10 - $scope.selected_circuit.inclusiveness
            },{
                field : 'Effective Teaching and Learning',
                value : $scope.selected_circuit.effective_teaching_learning,
                remaining : 10 - $scope.selected_circuit.effective_teaching_learning
            },{
                field : 'Healthy',
                value : $scope.selected_circuit.healthy,
                remaining : 10 - $scope.selected_circuit.healthy
            },{
                field : 'Gender Friendliness',
                value : $scope.selected_circuit.gender_friendly,
                remaining : 10 - $scope.selected_circuit.gender_friendly
            },{
                field : 'Safety and Protection',
                value : $scope.selected_circuit.safe_protective,
                remaining : 10 - $scope.selected_circuit.safe_protective
            },{
                field : 'Community Involvement',
                value : $scope.selected_circuit.community_involvement,
                remaining : 10 - $scope.selected_circuit.community_involvement
            }];

            $timeout(function () {
                $scope.changeBarChart($scope.tempArray, $scope.selected_circuit.name + ' Circuit Rating', 'circuitRatingBarChart', 'rating');
            }, 1000, 3);


            // Assign the circuits' districts' circuits to a scope variable
            $scope.circuitChartData = scDistrict.districtLookup[$scope.selected_circuit.district_id].allCircuitsHolder;
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepCircuitRating()
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scCircuit.circuits.length&& scCircuit.circuitLookup) {
            prepCircuitRating()
        }

    }]);