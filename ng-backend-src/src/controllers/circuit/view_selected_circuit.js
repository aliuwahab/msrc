/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedCircuitController',
    ['$rootScope', '$scope', '$state', '$stateParams', '$modal', '$log', 'scNotifier',
        'scRegion','scDistrict','scCircuit', 'scSchool','$timeout','School','Report', 'Timelines','RecentSubmissionService','DataCollectorHolder',
        function($rootScope, $scope, $state, $stateParams, $modal, $log, scNotifier,
                 scRegion, scDistrict, scCircuit, scSchool, $timeout, School, Report, Timelines, RecentSubmissionService, DataCollectorHolder) {

            $scope.$parent.changePageTitle('View', 'Circuit', 'View');

            scSchool.circuit_tracker = {};

            function prepCircuitData(){
                $scope.selected_circuit = scCircuit.circuitLookup[$stateParams.id];

                // $scope.regions = scRegion.regions;

                // $scope.districts = scDistrict.districts;

                // $scope.circuits = scCircuit.circuits;
                //$scope.circuitLookup =  scCircuit.circuitLookup;

                // $scope.schools = scSchool.schools;
                $scope.schoolLookup= scSchool.schoolLookup;

                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;

                $scope.$broadcast('runChildrenStatesSummary');

                /*Get the circuits schools as an array of ids*/
                $scope.selected_circuit.schoolIDs = _.keys($scope.selected_circuit.allSchools);
            }

            function checkCircuitAreLoadAndPrepCircuits() {
                if (!(_.isEmpty(scCircuit.circuitLookup)) && angular.isDefined(scCircuit.circuitLookup[$stateParams.id])) {
                    prepCircuitData();
                } else {
                    $timeout(function () {
                        checkCircuitAreLoadAndPrepCircuits();
                    }, 2500)
                }
            }

            $scope.$on('updateLoadedModelsFromRefresh', function(evt, data){
                if (data.counter == 6) {
                    checkCircuitAreLoadAndPrepCircuits();
                }
            });

            /*starts from here*/

            if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scCircuit.circuits.length && scCircuit.circuitLookup) {
                checkCircuitAreLoadAndPrepCircuits();
            }



            $scope.loadingTeachers = true;

            function prepCircuitTeachers(allSchoolTeachersData) {
                if (!scSchool.schoolLookup) {
                    checkCircuitAreLoadAndPrepCircuits();
                    return;
                }
                $timeout(function () {
                    $scope.teacherSummaryData = {
                        teachersInfo: allSchoolTeachersData[0].school_teachers,
                        circuitInfo: {
                            teachers_total: 0,
                            male_total: 0,
                            female_total: 0,
                            trained : 0,
                            percentage_trained : 0,
                            atpost : 0,
                            studyleave : 0,
                            otherleave : 0,
                            retired : 0,
                            deceased : 0,
                            averageTeacherAttendance: $scope.average_teacher_attendance
                        }
                    };


                    /*calculate totals with a variable. using the school directly, increases it on each page load*/
                    var schoolTeacherTotalTracker = {};

                    for (var i = 0; i < $scope.teacherSummaryData.teachersInfo.length; i++) {
                        var teacher = $scope.teacherSummaryData.teachersInfo[i];
                        if (teacher.class_taught == null || teacher.class_taught == 'null') teacher.class_taught = '';
                        if (teacher.class_subject_taught == null || teacher.class_subject_taught == 'null') teacher.class_subject_taught = '';

                        if (angular.isUndefined(teacher.highest_professional_qualification)
                            || teacher.highest_professional_qualification === ''
                            || teacher.highest_professional_qualification.toLowerCase() === 'others'
                            || teacher.highest_professional_qualification.toLowerCase() === 'other') {

                        }else{
                            $scope.teacherSummaryData.circuitInfo.trained ++;
                        }

                        teacher.subject = ($.trim(teacher.class_subject_taught + teacher.class_taught)).split(',')[0];

                        if (teacher.gender == 'male') {
                            $scope.teacherSummaryData.circuitInfo.male_total++;
                        } else $scope.teacherSummaryData.circuitInfo.female_total++;

                        /*assign a name property for filtering*/
                        teacher.full_name = teacher.first_name + teacher.last_name;

                        $scope.teacherSummaryData.circuitInfo.teachers_total++;
                        $scope.teacherSummaryData.circuitInfo[teacher.teacher_status]++;

                        /*per school totals*/
                        if (schoolTeacherTotalTracker[teacher.school_id] != undefined) {
                            schoolTeacherTotalTracker[teacher.school_id].totalTeachers++;
                            schoolTeacherTotalTracker[teacher.school_id].teacherObjects[teacher.id] = teacher;
                        }else {
                            schoolTeacherTotalTracker[teacher.school_id] = {
                                totalTeachers: 1,
                                school_id: teacher.school_id,
                                totalMaleTeachers : 0,
                                totalFemaleTeachers : 0,
                                teachersInSchoolHolder : [teacher],
                                teacherObjects : {},
                                school_name : scSchool.schoolLookup[teacher.school_id] ? scSchool.schoolLookup[teacher.school_id].name : 'Deleted School'
                            };
                            schoolTeacherTotalTracker[teacher.school_id].teacherObjects[teacher.id] = teacher;
                        }
                        if (teacher.gender == 'male') {
                            schoolTeacherTotalTracker[teacher.school_id].totalMaleTeachers++;
                        } else {
                            schoolTeacherTotalTracker[teacher.school_id].totalFemaleTeachers++;
                        }

                        if (scSchool.schoolLookup[teacher.school_id]) {
                            scSchool.schoolLookup[teacher.school_id].teachersInSchoolHolder .push(teacher);
                        }
                    }

                    $scope.teacherSummaryData.circuitInfo.percentage_trained = Math.round(($scope.teacherSummaryData.circuitInfo.trained / $scope.teacherSummaryData.teachersInfo.length) * 100);


                    angular.forEach(schoolTeacherTotalTracker, function (value, prop) {
                        if ($scope.selected_circuit.allSchools[value.school_id] != undefined) {
                            $scope.selected_circuit.allSchools[value.school_id].totalTeachers = value.totalTeachers;
                            $scope.selected_circuit.allSchools[value.school_id].totalMaleTeachers = value.totalMaleTeachers;
                            $scope.selected_circuit.allSchools[value.school_id].totalFemaleTeachers = value.totalFemaleTeachers;
                        }
                    });

                    $scope.schoolTeacherTotalTracker = angular.copy(schoolTeacherTotalTracker);

                    $scope.loadingTeachers = false;
                }, 1);
            }

            scSchool.allSchoolTeachersData({circuit_id : $stateParams.id})
                .success(function (allSchoolTeachersData) {

                    prepCircuitTeachers(allSchoolTeachersData);

                });

            $scope.$on('teachersUpdated', function(evt, teachersObject){
                prepCircuitTeachers(teachersObject);
            });

            $scope.average_teacher_attendance = 0;
            Timelines.getAvailableTimeline({category : 'normal_students_enrolment', circuit_id : $stateParams.id})
                .then(function (availableTimelines) {
                    $scope.circuitTerms = [];
                    $scope.circuitYears = [];
                    $scope.circuitWeeks = [];
                    $scope.circuitWeeksTrendHolderTracker = [];//this only ensures we don't save one week twice
                    $scope.circuitWeeksTrendHolder = [];//this holds the weeks to be displayed in the trends


                    for(var i = 0; i < availableTimelines.data.length; i++){
                        var item = availableTimelines.data[i];
                        //save the current term in the loop to a variable
                        //one school term is an object that formats the format returned
                        // from the server to a user friendlier one
                        var currentTerm = $scope.one_school_terms[item.term];

                        //save the current year in the loop to a variable
                        var currentYear = item.year;

                        //save the current week in the loop to a variable
                        var currentWeek = item.week_number;

                        // check if the current term isn't already added to the scope variable that displays the available terms
                        //in the submitted data
                        if ($scope.circuitTerms.indexOf(currentTerm) < 0) {
                            $scope.circuitTerms.push(currentTerm);

                            $scope.circuitTerms.sort(function(a, b){
                                return a.number.charAt(0) - b.number.charAt(0)
                            });
                        }

                        // check if the current year isn't already added to the scope variable that displays the available years
                        //in the submitted data
                        if ($scope.circuitYears.indexOf(currentYear) < 0) {
                            $scope.circuitYears.push(currentYear);
                        }

                        // check if the current week isn't already added to the scope variable that displays the available weeks
                        //in the submitted data
                        if ($scope.circuitWeeks.indexOf(currentWeek) < 0) {
                            $scope.circuitWeeks.push(currentWeek);

                            $scope.circuitWeeks.sort(function(a,b){
                                return a - b
                            });
                        }
                        // check if the current week isn't already added to the scope variable
                        // for the data trending in the submitted data
                        if ($scope.circuitWeeksTrendHolderTracker.indexOf(currentYear+currentTerm.value+currentWeek) < 0) {
                            $scope.circuitWeeksTrendHolder.push({
                                week : currentWeek,
                                term : currentTerm.number,
                                termValue : currentTerm.value,
                                termObj : currentTerm,
                                year : currentYear
                            });
                            $scope.circuitWeeksTrendHolder.sort(function(a,b){
                                return a.week - b.week
                            });

                            $scope.circuitWeeksTrendHolderTracker.push(currentYear+currentTerm.value+currentWeek);
                        }



                        $scope.summary_term =  item.term;

                        $scope.summary_week = item.week_number;

                        $scope.summary_year =  item.year;

                        $scope.summary_data_collector_type = item.data_collector_type || "head_teacher";

                    }
                    delete $scope.circuitWeeksTrendHolderTracker; //free some memory of the scope

                });

            RecentSubmissionService.getAllRecentSubmissions('circuit', $stateParams.id)
                .then(function (circuitRecentSubmissions) {
                    $scope.circuit_recent_submissions = circuitRecentSubmissions;


                    $scope.totalItems = circuitRecentSubmissions.length;
                    $scope.currentPage = 1;
                    $scope.maxSize = 3;
                    $scope.nextPage = 0;

                    $scope.setPage = function (pageNo) {
                        $scope.currentPage = pageNo;
                    };

                    //array.slice(start,end)
                    /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
                     * It does not include the element at that index*/

                    $scope.pageChanged = function() {
                        $scope.nextPage = ($scope.maxSize  * $scope.currentPage) - $scope.maxSize;
                        $scope.circuit_recent_submissions = circuitRecentSubmissions.slice($scope.nextPage);
                    };
                });


            $scope.showDataCollectors = function(type){
                //Remove all the multiple checked boxes
                $('input:checked').attr('checked', false);

                //reset the datacollectors array
                $scope.dataCollectors_to_send_messages_to = [];

                if ($scope.data_collector_types_to_filter) {
                    $scope.data_collector_type = $scope.data_collector_types_to_filter[type];
                }

                //change the data collectors being displayed by type
                if ($scope.dataCollectors) {
                    $scope.circuitDataCollectors =   $scope.dataCollectors[type];
                }else{
                    $scope.circuitDataCollectors =  {};
                }
            };

            //This variable shows the checkbox to select multiple data collectors to send to
            $scope.selectMultiple = false;

            //This variable holds data collectors to receive a message
            $scope.dataCollectors_to_send_messages_to = [];

            //This function checks whether a data collector is already added
            $scope.addRemoveCollector = function(id_of_checked){
                var index_of_personnel = $scope.dataCollectors_to_send_messages_to.indexOf(id_of_checked);
                if (index_of_personnel < 0) {
                    $scope.dataCollectors_to_send_messages_to.push(id_of_checked);
                }else{
                    //array.splice(index,how many,item1,.....,itemX)
                    $scope.dataCollectors_to_send_messages_to.splice(index_of_personnel, 1);
                }

                console.log($scope.dataCollectors_to_send_messages_to);
            };


        }]);