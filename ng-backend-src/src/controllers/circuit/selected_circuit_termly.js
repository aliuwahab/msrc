//Created by Kaygee on 17/06/14.
schoolMonitor.controller('scViewSelectedCircuitSchoolsStatsTermlyController',  [
    '$rootScope', '$scope', '$state', '$stateParams', '$modal', '$log', 'Report',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','DataCollectorHolder',
    function($rootScope, $scope, $state, $stateParams, $modal, $log, Report,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, DataCollectorHolder) {

        var schoolsWithSubmission = [];
        function loadTermlyData(termlyCircuitData) {
            $scope.termlyCircuitData = {
                schoolInfo : termlyCircuitData,
                circuitInfo : {}
            };
            var circuit_totals = {
                average_english_score : 0,
                average_english_score_holder : 0,

                average_ghanaian_language_score: 0,
                average_ghanaian_language_score_holder: 0,

                average_maths_score: 0,
                average_maths_score_holder: 0,

                scoring_english_above_average: 0,
                scoring_ghanaian_language_above_average: 0,
                scoring_maths_above_average: 0,

                num_english_books: 0,
                num_ghanaian_language_books: 0,
                num_maths_books: 0

            };

            schoolsWithSubmission = [];

            angular.forEach(termlyCircuitData, function (value, key) {
                circuit_totals.average_english_score_holder = parseFloat(circuit_totals.average_english_score_holder) + parseFloat(value.average_english_score);
                circuit_totals.average_english_score =  parseFloat(circuit_totals.average_english_score_holder) / $scope.selected_circuit.totalSchools;

                circuit_totals.average_ghanaian_language_score_holder = parseFloat(circuit_totals.average_ghanaian_language_score_holder) + parseFloat(value.average_ghanaian_language_score);
                circuit_totals.average_ghanaian_language_score =  parseFloat(circuit_totals.average_ghanaian_language_score_holder)  / $scope.selected_circuit.totalSchools;

                circuit_totals.average_maths_score_holder = parseFloat(circuit_totals.average_maths_score_holder) + parseFloat(value.average_maths_score);
                circuit_totals.average_maths_score =  parseFloat(circuit_totals.average_maths_score_holder) / $scope.selected_circuit.totalSchools;


                circuit_totals.scoring_english_above_average =  parseInt(circuit_totals.scoring_english_above_average) + parseInt(value.english_above_average);

                circuit_totals.scoring_ghanaian_language_above_average =  parseInt(circuit_totals.scoring_ghanaian_language_above_average) + parseInt(value.ghanaian_language_above_average);

                circuit_totals.scoring_maths_above_average =  parseInt(circuit_totals.scoring_maths_above_average) + parseInt(value.maths_above_average);

                /*TEXTBOOKS*/

                circuit_totals.num_english_books =  parseInt(circuit_totals.num_english_books) + parseInt(value.num_english_books);

                circuit_totals.num_ghanaian_language_books =  parseInt(circuit_totals.num_ghanaian_language_books) + parseInt(value.num_ghanaian_language_books);

                circuit_totals.num_maths_books =  parseInt(circuit_totals.num_maths_books) + parseInt(circuit_totals.num_maths_books);

                value.name = scSchool.schoolLookup[value.school_id].name;

                schoolsWithSubmission.push(value.school_id + '');
            });

            $scope.termlyCircuitData.circuitInfo = circuit_totals
        }


        var alternate = true;
        $scope.showNoSubmission = function () {
            if (alternate)
                $scope.noSubmission = _.difference($scope.selected_circuit.schoolIDs, schoolsWithSubmission);
            else
                $scope.noSubmission = [];

            alternate = !alternate;

            console.log($scope.noSubmission);
        };



        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };

        $scope.clearCacheAndReload = function () {
            scCircuit.clearCache();
            $scope.fetchTermlySummaryData();
        };

        $scope.fetchTermlySummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;
            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                circuit_id : $stateParams.id
            };
            scCircuit.fetchTermlyData(data_to_post)
                .then(
                    /*success function*/
                    function(loadedWeeklyData){
                        loadTermlyData(loadedWeeklyData);
                    },

                    /*error function*/
                    function(){

                    })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
        };

        function prepCircuitSchoolTermlyStats() {
            //This variable toggle to display the loading gif if the summary tab
            $scope.loadingSummary= false;
            //A variable to check if data has been loaded before, so we change the instruction on the summary tab
            $scope.initialSummaryInfoCount = 0;

            //reset the allSchools attendance and enrollment arrays so that it prevent duplicates when switching between pages
            angular.forEach($scope.selected_circuit.allSchools, function(school, index){
                school.termlyDataHolder = [];
            });

            $scope.schoolYears_Termly = [];
            $scope.schoolTerms_Termly = [];

            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'display_average' : true,
                'display_textbooks' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;
        }




        $scope.$on('runChildrenStatesSummary', function () {
            prepCircuitSchoolTermlyStats()
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scCircuit.circuits.length&& scCircuit.circuitLookup) {
            prepCircuitSchoolTermlyStats()
        }

    }]);
