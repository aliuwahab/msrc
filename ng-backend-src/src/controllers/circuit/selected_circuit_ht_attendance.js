/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedCircuitHTAttendanceController',  [
    '$rootScope', '$scope', '$state', '$stateParams', '$modal', 'DataCollectorHolder',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, $modal, DataCollectorHolder,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {


        //Shuffle between a single circuit rating and region circuits
        $scope.compareAverages =function (){
            $scope.circuitAverage = false;
            $scope.compareDistrictsCircuitAverages = true;

            // Without the interval, the chart will not display until a browser triggered event
            $interval(function(){
                var chart = $scope.changeSerialChart(scDistrict.districtLookup[$scope.selected_circuit.district_id].allCircuitsHolder, 'name',
                    'Circuits under ' + scDistrict.districtLookup[$scope.selected_circuit.district_id].name, 'districtsCircuitAverageBarChart', 'ht_average');
                chart.validateNow();
            }, 100, 1)
        };


        //Shuffle between a single circuit rating and region circuits
        $scope.singleAverage = function(){
            $scope.circuitAverage = true;
            $scope.compareDistrictsCircuitAverages = false;
        };

        //An object to easily display between table and graph views under comparing circuits
        $scope.view_to_show = {
            'chart' : true,
            'table' : false
        };

        //This function changes between displaying the table or charts for comparing circuits
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                $scope.view_to_show[prop] = false;
            });
            $scope.view_to_show[view_to_show] = true;
            if ( $scope.compareDistrictsCircuitAverages) {
                $scope.compareAverages();
            }
        };



        function prepCircuitHTTeachersInfo() {
            $scope.selected_circuit = scCircuit.circuitLookup[$stateParams.id];

            //This variable is for a single circuit rating's ng-show
            $scope.circuitAverage = true;
            //This variable is for a comparing regional  circuit rating's ng-show
            $scope.compareRegionDistrictAverages = false;

            //Format the data to send to amCharts to 'field' and 'value'
            $scope.tempArray = [];

            //loop through schools of this circuit and assign the teacher attendance average
            // into objects and shove it into the temp array for display
            angular.forEach($scope.selected_circuit.allSchoolsHolder, function(school, index){
                var remainder = $scope.selected_circuit.headteacherAttendanceAverage - school.current_teacher_attendance_by_headteacher;
                var obj = {
                    field: school.name,
                    value: school.current_teacher_attendance_by_headteacher,
//                remaining : Math.abs(remainder)
                    remaining : remainder
                };

                $scope.tempArray.push(obj);
            });

            $timeout(function () {
                $scope.changeBarChart($scope.tempArray, $scope.selected_circuit.name + ' Teacher Attendance Average by Head Teachers',
                    'circuitHtAttendanceBarChart', 'ht_average', $scope.selected_circuit.headteacherAttendanceAverage);
            }, 200, 4);


            // Assign the regions circuits to a scope variable
            $scope.circuitChartData = scDistrict.districtLookup[$scope.selected_circuit.district_id].allCircuitsHolder;

        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepCircuitHTTeachersInfo()
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scCircuit.circuits.length&& scCircuit.circuitLookup) {
            prepCircuitHTTeachersInfo()
        }

    }]);