/**
 * Created by Kaygee on 23/04/2015.
 */
schoolMonitor.controller('scViewCircuitController',
    ['$rootScope', '$scope', '$state', '$filter', '$stateParams','Circuit', '$modal', '$log',  'scNotifier',
        'scRegion','scDistrict','scCircuit', 'scSchool' ,'$timeout','DataCollectorHolder',
        function($rootScope, $scope, $state, $filter, $stateParams, Circuit, $modal, $log, scNotifier,
                 scRegion, scDistrict, scCircuit, scSchool, $timeout, DataCollectorHolder) {


            $scope.changePageTitle('View', 'Circuit', 'View');

            $scope.formData = {};

            //this is used to sort the districts alphabetically
            $scope.circuitSortOrder = 'name';

            function assignAdmin(){
                $scope.districtLookup = scDistrict.districtLookup;

                if ($scope.adminRight == 5){
                    $scope.regions = scRegion.regions;
                    $scope.districts = scDistrict.districts;
                    $scope.circuits = scCircuit.circuits;
                    $scope.circuitsSlice = scCircuit.circuits;

                    $scope.circuitsSearch = scCircuit.circuits;
                    $scope.totalItems = scCircuit.circuits.length;

                    $scope.schools = scSchool.schools;
                    $scope.schoolsSearch = scSchool.schools;

                }
                else if ($scope.adminRight == 4) {
                    $scope.regions = new Array(scRegion.regionLookup[$scope.currentUser.level_id]);
                    $scope.districts = scRegion.regionLookup[$scope.currentUser.level_id].allDistrictsHolder;
                    $scope.circuits = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.circuitsSlice = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers = scRegion.regionLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors = scRegion.regionLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.circuitsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.totalItems = scRegion.regionLookup[$scope.currentUser.level_id].totalCircuits;

                    $scope.schools = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scRegion.regionLookup[$scope.currentUser.level_id].allSchoolsHolder;

                }
                else if ($scope.adminRight == 3) {
                    $scope.regions = new Array(scRegion.regionLookup[scDistrict.districtLookup[$scope.currentUser.level_id].region_id]);
                    $scope.districts = new Array(scDistrict.districtLookup[$scope.currentUser.level_id]);
                    $scope.circuits = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.circuitsSlice = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.head_teachers = scDistrict.districtLookup[$scope.currentUser.level_id].head_teachers_holder;
                    $scope.supervisors = scDistrict.districtLookup[$scope.currentUser.level_id].circuit_supervisors_holder;

                    $scope.circuitsSearch = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                    $scope.totalItems = scDistrict.districtLookup[$scope.currentUser.level_id].totalCircuits;

                    $scope.schools = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scDistrict.districtLookup[$scope.currentUser.level_id].allSchoolsHolder;

                }
                else {
                    $scope.circuits = new Array(scCircuit.circuitLookup[$scope.currentUser.level_id]);
                    $scope.schools = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                    $scope.schoolsSearch = scCircuit.circuitLookup[$scope.currentUser.level_id].allSchoolsHolder;
                }
            }

            $scope.$on('updateLoadedModelsFromRefresh',function(evt, data){
                if (data.counter == 6) {
                    $timeout(function () {
                        assignAdmin();
                    }, 10)
                }

            });

            if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scCircuit.circuits.length && scCircuit.circuitLookup) {
                assignAdmin();
            }

            $scope.currentPage = 1;
            $scope.maxSize = 15;
            $scope.nextPage = 0;

//            array.slice(start,end)
            /*The end parameter of array slice is optional. It stands for where the end of the slicing should be.
             * It does not include the element at that index*/

            $scope.pageChanged = function(currentPage) {
                $scope.nextPage = ($scope.maxSize  * currentPage) - $scope.maxSize;
                $scope.circuits =  ( $scope.circuitsSlice ).slice($scope.nextPage , ($scope.nextPage + 15) );
                $filter('filter')( $scope.circuits, $scope.circuitSortOrder, false);
            };

            $scope.paramAction = function () {
                return $stateParams.action.charAt(0).toUpperCase() + $stateParams.action.substring(1);
            };


            if ($stateParams.action == 'edit') {
                Circuit.retrieveOneCircuit({id: $stateParams.id},
                    function (data, responseHeaders) {
                        $scope.formData = {
                            id: data.id,
                            circuit_name : data.name,
                            description : data.description,
                            circuit_email : data.email,
                            circuit_phone_number : data.phone_number,
                            country_id: data.country_id,
                            region : data.region_id,
                            district : data.district_id,
                            circuit_location  : data.lat_long
                        };
                    },
                    function (httpResponse) {

                    })
            } else if ($stateParams.action == 'add') {
                $scope.formData = {};
            }

            // process the form
            $scope.processForm = function () {
                $rootScope.loading = true;
                if ($stateParams.action == 'edit') {
                    Circuit.editCircuit($scope.formData,
                        //success callback
                        function (value, responseHeaders) {
                            if (value) {
                                $scope.formData = {};
                                $state.go('dashboard.view_circuit', {} , {reload : true});

                            }else {
                                $scope.messageFromServer = 'There was an error in editing the circuit. Please contact system admin';
                                $rootScope.loading = false;
                            }


                        },
                        //error callback
                        function (httpResponse) {
                            $rootScope.loading = false;
                            $scope.errorMessageFromServer = 'There was an error in editing the circuit';
                        });
                } else {
                    // process the form
                    scCircuit.addCircuit($scope.formData).then(
                        function (status) {
                            if (status) {
                                scNotifier.notifySuccess('Success', 'Circuit successfully created');
                                $state.go('dashboard.view_circuit', {}, {reload: true});
                            } else {
                                scNotifier.notifyFailure('Error', 'There was an error creating the circuit');
                            }
                        },
                        function () {
                            scNotifier.notifyFailure('Error', 'There was an error creating the circuit');
                        });
                }
            };

            $scope.deleteModal = function (size, id, element) {

                var modalInstance = $modal.open({
                    templateUrl: 'partials/miscellaneous/deleteModal.html',
                    controller: ModalInstanceCtrl,
                    size: size,
                    resolve: {
                        deleteCircuitObject: function () {
                            return id;
                        },
                        clickedElement: function(){
//                    return $(element.target).parent('tr');
                            return $(element.target).parents('tr');
                        }
                    }
                });

            };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

            var ModalInstanceCtrl = function ($scope, $modalInstance, deleteCircuitObject, clickedElement) {

                $scope.deleteObject = scCircuit.circuitLookup[deleteCircuitObject];
                $scope.deleteObject.objectType = 'Circuit';

                $scope.ok = function () {
                    $modalInstance.close('close');
//            $log.info('Modal ok\'ed at: ' + new Date());
                    scCircuit.deleteCircuit(deleteCircuitObject).then(
                        function(status){
                            if (status) {
                                scNotifier.notifySuccess('Success', 'Circuit deleted successfully');
                                $state.go('dashboard.view_circuit', {} , {reload : true});
                            }else{
                                scNotifier.notifyFailure('Error', 'There was an error deleting the circuit');
                            }
                        },
                        function(){
                            scNotifier.notifyFailure('Error', 'There was an error deleting the circuit');
                        })
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
//            $log.info('Modal cancelled at: ' + new Date());
                };
            };

            $scope.searchModal = function () {

                var modalInstance = $modal.open({
                    templateUrl: 'partials/miscellaneous/searchCircuitModal.html',
                    controller: searchModalInstanceCtrl,
                    size: 'lg'
                });

            };

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

            var searchModalInstanceCtrl = function ($scope, $modalInstance) {

                if ($scope.adminRight  == 5){
                    $scope.circuits = scCircuit.circuits;
                }else if ($scope.adminRight == 4){
                    $scope.circuits = scRegion.regionLookup[$scope.currentUser.level_id].allCircuitsHolder;
                } else if ($scope.adminRight  == 3){
                    $scope.circuits = scDistrict.districtLookup[$scope.currentUser.level_id].allCircuitsHolder;
                }else{
                    $scope.circuits = [
                        scCircuit.circuitLookup[$scope.currentUser.level_id]
                    ];
                }

                $scope.circuitSortOrder = 'name';

                $scope.districtLookup = scDistrict.districtLookup;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };

            }



        }]);
