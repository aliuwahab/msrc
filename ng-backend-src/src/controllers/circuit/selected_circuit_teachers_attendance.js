/**
 * Created by Kaygee on 23/04/2015.
 */


schoolMonitor.controller('scViewSelectedDistrictTeacherAttendanceController',  [ '$rootScope', '$scope', '$state', '$stateParams',
    'DataCollectorHolder', 'Report', 'School', 'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', '$interval', '$timeout',
    function($rootScope, $scope, $state, $stateParams, DataCollectorHolder, Report, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {


        $scope.fetchAttendanceAverageSummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;

            var data_to_post = {
                year : $scope.attYear,
                term : $scope.attTerm,
                // week_number : $scope.summary_week,
                district_id : $stateParams.id
            };

            /*else is for loading everything*/
            School.allSchoolTeachersPunctuality_LessonPlan(data_to_post).$promise
                .then(function (punctuality_lesson_plans) {
                    scSchool.districtAllSchoolTeachersPunctuality_LessonPlan = punctuality_lesson_plans;
                    $scope.prepTeachers();
                    $scope.loadingSummary= false;
                });

            /*Report.totalsWeekly(data_to_post).$promise
                .then(
                    /!*success function*!/
                    function(loadedWeeklyData){
                        processTeacherAttendanceAverage(loadedWeeklyData);
                    },

                    /!*error function*!/
                    function(){

                    })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
*/

        };

        $scope.prepTeachers = function() {
            $rootScope.loading = true;
            $scope.loadingData = true;
            $scope.schoolNumberOfDaysInSession = {};

            /*
             1. In the teachers loop, put them in school objects, with their ids as the key
             $scope.schoolTeacherTotalTracker
             2. So each school, has its teachers as children
             3. then loop through the punctuality items, and do the weekly assignments to each of the teachers under the school object
             */
            for (var tp = 0; tp < scSchool.districtAllSchoolTeachersPunctuality_LessonPlan.length; tp++) {

                var teacherPunctualityObject = scSchool.districtAllSchoolTeachersPunctuality_LessonPlan[tp];


                if ($scope.schoolTeacherTotalTracker[teacherPunctualityObject.school_id]) {

                    //check if the teacher has not been deleted
                    if ($scope.schoolTeacherTotalTracker[teacherPunctualityObject.school_id].teacherObjects[teacherPunctualityObject.teacher_id]) {

                        //parse it to int to enforce consistency in searching and filtering
                        teacherPunctualityObject.week_number = parseInt(teacherPunctualityObject.week_number);

                        teacherPunctualityObject.days_absent_with_permission = parseInt(teacherPunctualityObject.days_absent_with_permission);
                        teacherPunctualityObject.days_absent_without_permission = parseInt(teacherPunctualityObject.days_absent_without_permission);
                        teacherPunctualityObject.total_days_absent = teacherPunctualityObject.days_absent_with_permission + teacherPunctualityObject.days_absent_without_permission;


                        delete teacherPunctualityObject.comment;
                        delete teacherPunctualityObject.cs_comments;
                        delete teacherPunctualityObject.questions_category_reference_code;
                        delete teacherPunctualityObject.school_reference_code;
                        delete teacherPunctualityObject.teacher;


                        var concat = teacherPunctualityObject.week_number + teacherPunctualityObject.term + teacherPunctualityObject.year;


                        $scope.schoolNumberOfDaysInSession[concat] = teacherPunctualityObject.days_in_session;

                        if ($scope.schoolTeacherTotalTracker[teacherPunctualityObject.school_id].teacherObjects[teacherPunctualityObject.teacher_id].punctualityHolder) {
                            $scope.schoolTeacherTotalTracker[teacherPunctualityObject.school_id].teacherObjects[teacherPunctualityObject.teacher_id].punctualityHolder[concat] = teacherPunctualityObject;
                        } else {
                            $scope.schoolTeacherTotalTracker[teacherPunctualityObject.school_id].teacherObjects[teacherPunctualityObject.teacher_id].punctualityHolder = {};

                            $scope.schoolTeacherTotalTracker[teacherPunctualityObject.school_id].teacherObjects[teacherPunctualityObject.teacher_id].punctualityHolder[concat] = teacherPunctualityObject;
                        }

                        // $scope.schoolTeacherTotalTracker[teacherPunctualityObject.school_id].teacherObjects[teacherPunctualityObject.id].punctualityHolder.sort(function (a, b) {
                        //     return a.week_number - b.week_number
                        // });
                    }


                }

                $timeout(function () {
                    $rootScope.loading = false;
                });

            }
        }
    }]);