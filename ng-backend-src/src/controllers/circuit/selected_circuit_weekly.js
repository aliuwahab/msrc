/**
 * Created by Kaygee on 23/04/2015.
 */

schoolMonitor.controller('scViewSelectedCircuitSchoolsStatsWeeklyController',  [
    '$rootScope', '$scope', '$state', '$stateParams','scGlobalService', '$log', 'Report', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','DataCollectorHolder','$timeout',
    function($rootScope, $scope, $state, $stateParams, scGlobalService, $log, Report, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, DataCollectorHolder, $timeout) {

        //This function changes between displaying the table or charts for comparing districts
        $scope.changeView = function(view_to_show){
            angular.forEach($scope.view_to_show, function(val, prop){
                //set all view to false
                $scope.view_to_show[prop] = false;
            });
            //set the selected view to true
            $scope.view_to_show[view_to_show] = true;
        };

        function processEnrollmentSummary(loadedWeeklyData){

            $scope.enrollmentSummaryData = {
                schoolInfo : loadedWeeklyData,
                circuitInfo : {}
            };

            var circuit_totals = {
                head_teacher : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                },
                circuit_supervisor : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                }
            };


            angular.forEach(loadedWeeklyData, function (value, key) {
                circuit_totals[value.data_collector_type].boys =  parseInt(circuit_totals[value.data_collector_type].boys) + parseInt(value.normal_enrolment_total_boys);
                circuit_totals[value.data_collector_type].special_boys =  parseInt(circuit_totals[value.data_collector_type].special_boys) + parseInt(value.special_enrolment_total_boys);
                circuit_totals[value.data_collector_type].boys_total =  parseInt(circuit_totals[value.data_collector_type].special_boys) + parseInt(circuit_totals[value.data_collector_type].boys);

                circuit_totals[value.data_collector_type].girls =  parseInt(circuit_totals[value.data_collector_type].girls) + parseInt(value.normal_enrolment_total_girls);
                circuit_totals[value.data_collector_type].special_girls =  parseInt(circuit_totals[value.data_collector_type].special_girls) + parseInt(value.special_enrolment_total_girls);
                circuit_totals[value.data_collector_type].girls_total =  parseInt(circuit_totals[value.data_collector_type].special_girls) + parseInt(circuit_totals[value.data_collector_type].girls);

                circuit_totals[value.data_collector_type].all_total =   parseInt(circuit_totals[value.data_collector_type].boys_total) + parseInt(circuit_totals[value.data_collector_type].girls_total);
                circuit_totals[value.data_collector_type].teachers_total =   parseInt(circuit_totals[value.data_collector_type].teachers_total) + parseInt(value.current_number_of_teachers);

            });

            $scope.enrollmentSummaryData.circuitInfo = circuit_totals;


        }


        var schoolsWithAttendanceWeeklySubmission = [];
        function processAttendanceSummary(loadedWeeklyData){

            $scope.attendanceSummaryData = {
                schoolInfo : loadedWeeklyData,
                circuitInfo : {}
            };

            var circuit_totals = {
                head_teacher : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                },
                circuit_supervisor : {
                    all_total : 0,
                    boys: 0,
                    special_boys: 0,
                    boys_total : 0,
                    girls: 0,
                    special_girls: 0,
                    girls_totals : 0,
                    teachers_total : 0
                }
            };

            angular.forEach(loadedWeeklyData, function (value, key) {
                circuit_totals[value.data_collector_type].boys =  parseInt(circuit_totals[value.data_collector_type].boys) + parseInt(value.normal_attendance_total_boys);
                circuit_totals[value.data_collector_type].special_boys =  parseInt(circuit_totals[value.data_collector_type].special_boys) + parseInt(value.special_attendance_total_boys);
                circuit_totals[value.data_collector_type].boys_total =  parseInt(circuit_totals[value.data_collector_type].special_boys) + parseInt(circuit_totals[value.data_collector_type].boys);

                circuit_totals[value.data_collector_type].girls =  parseInt(circuit_totals[value.data_collector_type].girls) + parseInt(value.normal_attendance_total_girls);
                circuit_totals[value.data_collector_type].special_girls =  parseInt(circuit_totals[value.data_collector_type].special_girls) + parseInt(value.special_attendance_total_girls);
                circuit_totals[value.data_collector_type].girls_total =  parseInt(circuit_totals[value.data_collector_type].special_girls) + parseInt(circuit_totals[value.data_collector_type].girls);

                circuit_totals[value.data_collector_type].all_total =   parseInt(circuit_totals[value.data_collector_type].boys_total) + parseInt(circuit_totals[value.data_collector_type].girls_total);
                circuit_totals[value.data_collector_type].teachers_total =   parseInt(circuit_totals[value.data_collector_type].teachers_total) + parseInt(value.current_number_of_teachers);

                schoolsWithAttendanceWeeklySubmission.push(value.school_id + '');

            });

            $scope.showAttendanceWeeklyDataGaps = function (test) {
                if (test) $scope.noWeeklyAttendanceSubmission = _.difference($scope.selected_circuit.schoolIDs, schoolsWithAttendanceWeeklySubmission);
                else $scope.noWeeklyAttendanceSubmission = [];
            };

            $scope.attendanceSummaryData.circuitInfo = circuit_totals;


        }


        function processTeacherAttendanceSummary(loadedWeeklyData){
            $scope.teacherAttendanceSummaryData = {
                schoolInfo : loadedWeeklyData,
                circuitInfo : {}
            };
            var circuit_totals = {
                head_teacher : {
                    males: 0,
                    females: 0,
                    teachers_total : 0,
                    teachers_total_submitted : 0
                }
            };

            angular.forEach(loadedWeeklyData, function (value, key) {
                /*we add this check because circuit supervisors used to also submit data*/
                /*check if the school is not deleted*/
                if (scSchool.schoolLookup[value.school_id] && scSchool.schoolLookup[value.school_id] != undefined) {
                    if (value.data_collector_type == 'head_teacher') {
                        circuit_totals[value.data_collector_type].teachers_total_submitted = parseInt(circuit_totals[value.data_collector_type].teachers_total) + parseInt(value.current_number_of_teachers);
                        circuit_totals[value.data_collector_type].males = scSchool.schoolLookup[value.school_id].totalMaleTeachers;
                        circuit_totals[value.data_collector_type].females = scSchool.schoolLookup[value.school_id].totalFemaleTeachers;
                        circuit_totals[value.data_collector_type].teachers_total = scSchool.schoolLookup[value.school_id].totalTeachers;
                    }
                }
            });


            $scope.teacherAttendanceSummaryData.circuitInfo = circuit_totals;


        }

        /*The variable that shows either the regular weekly or trends*/
        $scope.showTrend = false;

        /*Filter the trend table by complete and incomplete submissions*/
        $scope.filterTrendOnSubmission = function (section) {
            $scope[section +"_filterTrendViews"] = !$scope[section +"_filterTrendViews"];

            angular.forEach($scope.circuitWeeklyDataTrend, function (weekData, schoolId) {

                if (Object.keys(weekData).length >= $scope.availableWeeksInTerm.length) {
                    $scope.selected_circuit.allSchools[schoolId].hide = true;

                    var count = 0;
                    angular.forEach(weekData, function (item, index) {
                        /* sections are : item.average_teacher_attendance  && item.normal_attendance_total_students  && normal_enrolment_total_students*/
                        if ( item[section]) {
                            count++;
                        }
                    });

                    $scope.selected_circuit.allSchools[schoolId][section + '_hide'] = count === $scope.availableWeeksInTerm.length;
                }
            })

        };

        /*Filter the trend table by complete and incomplete submissions*/
        $scope.filterTrendOnEnrollment = function () {
            $scope.completeSubmissions = {};
            $scope.inCompleteSubmission = {};
            $scope.filterTrendViews = !$scope.filterTrendViews;

            if($scope.filterTrendViews){
                angular.forEach($scope.selected_district.allSchoolsHolder, function (schoolItem, index) {
                    $scope.inCompleteSubmission[schoolItem.id] = 'bg-red';
                });

                angular.forEach($scope.districtWeeklyDataTrend, function (weekData, schoolId) {
                    if (Object.keys(weekData).length < $scope.availableWeeksInTerm.length) {
                        $scope.inCompleteSubmission[schoolId] = "bg-red";
                    }else{
                        var count = 0;
                        angular.forEach(weekData, function (item, index) {
                            if (Number(item.average_teacher_attendance) > 0 && Number(item.normal_attendance_total_students) > 0 && Number(item.normal_enrolment_total_students > 0)) {
                                count++;
                            }
                        });
                        if (count < $scope.availableWeeksInTerm.length) {
                            $scope.inCompleteSubmission[schoolId] = 'bg-red';
                        }else{
                            delete $scope.inCompleteSubmission[schoolId];
                            $scope.completeSubmissions[schoolId] = true;
                        }
                    }
                });
            }
        };

        $scope.cancelFilterTrendOnSubmission = function (section) {
            $scope[section +"_filterTrendViews"] = !$scope[section +"_filterTrendViews"];
            angular.forEach($scope.selected_circuit.allSchoolsHolder, function (school, index) {
                delete school[section + '_hide'];
                delete $scope.selected_circuit.allSchools[school.id][section + '_hide'];
            })
        };


        /*prevent the scope from attaching the teacher loaded event twice*/
        var teacherEventListernerLoaded = false;

        /*This loads for just the circuit in question per week per school*/
        $scope.fetchSummaryData = function () {
            $scope.initialSummaryInfoCount ++;
            $scope.loadingSummary= true;
            $scope.showTrend= false;

            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                week_number : $scope.summary_week,
                data_collector_type : "head_teacher",
                circuit_id : $stateParams.id
            };
            Report.totalsWeekly(data_to_post).$promise
                .then(
                    /*success function*/
                    function(loadedWeeklyData){
                        $('.showing_weekly_year').text($scope.summary_year);
                        $scope.showing_weekly_year = angular.copy($scope.summary_year);

                        $('.showing_weekly_term').text( $scope.one_school_terms[$scope.summary_term].label );
                        $scope.showing_weekly_term = angular.copy($scope.summary_term);

                        $('.showing_weekly_week').text($scope.summary_week);
                        processEnrollmentSummary(loadedWeeklyData);
                        processAttendanceSummary(loadedWeeklyData);
                        processTeacherAttendanceSummary(loadedWeeklyData);

                        /*add this in the success callback so it will run once the totals have been loaded*/
                        if (!teacherEventListernerLoaded) {
                            $scope.$on('runChildrenStatesSummary', function () {
                                $timeout(function () {

                                    processTeacherAttendanceSummary(loadedWeeklyData);
                                });
                            });
                        }

                    },

                    /*error function*/
                    function(){

                    })
                .finally(function () {
                    $scope.loadingSummary= false;
                })
        };


        function prepCircuitSchoolStats() {
            //This variable toggle to display the loading gif if the summary tab
            $scope.loadingSummary= false;
            //A variable to check if data has been loaded before, so we change the instruction on the summary tab
            $scope.initialSummaryInfoCount = 0;

            //An object to easily display between table and graph views under comparing districts
            $scope.view_to_show = {
                'enrollment' : true,
                'attendance' : false
            };

            $scope.schoolLookup = scSchool.schoolLookup;
            $scope.circuitLookup = scCircuit.circuitLookup;

            /*Since we will only be submitting HeadTeacher Info, make it permanent*/
            $scope.summary_data_collector_type = 'head_teacher';
        }

        $scope.$on('runChildrenStatesSummary', function () {
            prepCircuitSchoolStats()
        });

        if (DataCollectorHolder.dataCollectors && DataCollectorHolder.lookupDataCollector && scCircuit.circuits.length&& scCircuit.circuitLookup) {
            prepCircuitSchoolStats()
        }




        /*This is the beginning of para trending data for circuit.*/

        /*on click of a button, load all the available for the totals weekly table*/

        $scope.fetchAggregatedData = function () {
            $scope.initialSummaryInfoCount ++;

            $scope.loadingSummary= true;
            $scope.showTrend= true;
            var data_to_post = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                data_collector_type : "head_teacher",
                circuit_id : $stateParams.id
            };

            // $scope.selected_circuit.allSchoolsHolder.forEach(function (school, index) {
            // });
            $scope.availableWeeksInTerm = [];
            $scope.circuitWeeklyDataTrend = {};

            scGlobalService.getEnrolmentForASchoolsTerm(data_to_post, $stateParams.id, scCircuit.circuitLookup[$stateParams.id].totalSchools)
                .then(function (returnedData) {

                    $('.showing_weekly_year').text($scope.summary_year);
                    $scope.showing_weekly_year = angular.copy($scope.summary_year);

                    $('.showing_weekly_term').text( $scope.one_school_terms[$scope.summary_term].label );
                    $scope.showing_weekly_term = angular.copy($scope.summary_term);

                    $scope.studAttendanceTotal = returnedData.enrollmentVariables;
                    $scope.availableWeeksInTerm = returnedData.availableWeeksInTerm;
                    $scope.circuitWeeklyDataTrend = returnedData.modelWeeklyDataTrend;


                    $scope.loadingSummary = false;

                });

        };









        /*
        * ENROLLMENT UPDATE SECTION
        * */

        var dataloader = 0; //This variable prevents the data prepping from happening until both enrollments are loaded
        var loadedEnrollmentInfoFromServer = [], loadedSpecialEnrollmentInfoFromServer = [];
        var enrollmentData = [];

        function initEnrollmentVars() {
            dataloader = 0; //This variable prevents the data prepping from happening until both are loaded
            enrollmentData = [];
            enrollmentData.location = {};/*an object whose keys are concatenation of the year,week,term and dc to track location of submitting data*/
            enrollmentData.totals = {};/*an object whose keys are concatenation of the year,week,term and dc to track totals of data*/
        }


        function prepEnrollmentData() {
            for (var index = 0; index < loadedEnrollmentInfoFromServer.length; index ++){
                var item = loadedEnrollmentInfoFromServer[index];

                if(item.data_collector_type === 'circuit_supervisor'){
                    continue;
                }


                var concatenate = item.year + item.term + item.week_number + item.data_collector_type;
                if (enrollmentData.totals[concatenate]) {
                    enrollmentData.totals[concatenate].normal += parseInt(item.total_enrolment);
                    enrollmentData.totals[concatenate].boys += parseInt(item.num_males);
                    enrollmentData.totals[concatenate].girls += parseInt(item.num_females);
                } else {
                    enrollmentData.totals[concatenate] = {
                        normal: parseInt(item.total_enrolment),
                        special: 0,
                        boys : parseInt(item.num_males),
                        special_boys : 0,
                        girls : parseInt(item.num_females),
                        special_girls : 0,
                        week_number :  item.week_number,
                        term : item.term,
                        year : item.year
                    }
                }
                var tempObject = {
                    index: index, //This is used to know which index was clicked so we pass it into the chart function
                    selected: false, //This is used to know which index was clicked so we highlight that row
                    class: item.level,
                    boys: item.num_males,
                    girls: item.num_females,
                    streams: item.num_of_streams,
                    total: parseInt(item.num_males) + parseInt(item.num_females),
                    term: item.term,
                    week: item.week_number,
                    year: item.year,
                    special_boys: 0,
                    special_girls: 0,
                    data_collector_type: item.data_collector_type,
                    date_created: moment(item.created_at).utc().valueOf(),
                    normal_id: item.id
                };
                for (var subIndex = 0; subIndex < loadedSpecialEnrollmentInfoFromServer.length; subIndex ++) {
                    var special_item = loadedSpecialEnrollmentInfoFromServer[subIndex];
                    if (special_item.level === item.level &&
                        special_item.term === item.term &&
                        special_item.week_number === item.week_number &&
                        special_item.year === item.year &&
                        special_item.data_collector_type === item.data_collector_type) {

                        tempObject.special_boys = special_item.num_males;
                        tempObject.special_girls = special_item.num_females;
                        tempObject.special_id = special_item.id;
                        enrollmentData.totals[concatenate].special += parseInt(special_item.total_enrolment);
                        enrollmentData.totals[concatenate].special_boys += parseInt(special_item.num_males);
                        enrollmentData.totals[concatenate].special_girls += parseInt(special_item.num_females);
                    }
                }
            }
        }

        /*this function send the totals calculated to the server to update the weekly table*/
        function updateSchoolWeeklyTotal(school_id) {
            var holder = [];

            var selected_school = $scope.schoolLookup[school_id];

            $scope.updatingTotals = true;

            angular.forEach(enrollmentData.totals, function (value, key) {
                holder.push({
                    normal_enrolment_total_boys : value.boys,
                    normal_enrolment_total_girls : value.girls,
                    normal_enrolment_total_students : value.normal,
                    special_enrolment_total_boys : value.special_boys,
                    special_enrolment_total_girls : value.special_girls,
                    special_enrolment_total_students : value.special,
                    year : value.year,
                    term : value.term,
                    week_number : value.week_number,
                    country_id : selected_school.country_id,
                    region_id : selected_school.region_id,
                    district_id : selected_school.district_id,
                    circuit_id : selected_school.circuit_id,
                    school_id : selected_school.id
                })
            });


            scSchool.updateWeeklyTableForEnrollment(holder)
                .success(function (successData) {
                    if (successData.status === 200) {
                        scNotifier.notifySuccess("Recalculation", "Enrolment totals have been updated for " + selected_school.name);
                        $scope.fetchAggregatedData();
                    }
                })
                .error(function (errData) {
                    scNotifier.notifyFailure("Recalculation", "Enrolment totals failed to update")
                })
                .finally(function () {
                    $scope.loadingEnrollmentData = false;/*the variable to hide the gif is here because its is called here regardless of the start point of loadin data*/
                    $scope.schoolBeingUpdated = 0;

                });


        }




        //Fired from the below when data is deleted successfully
        $scope.$on('enrollmentDataLoaded', function(event, data){
            //this should be greater than 1. that's when both special and normal have loaded
            if (data.loader > 1) {

                initEnrollmentVars();
                prepEnrollmentData();
                updateSchoolWeeklyTotal(data.school_id);
                $scope.loadingEnrollmentData = false;/*the variable to hide the gif is here because its is called here regardless of the start point of loadin data*/
                $scope.schoolBeingUpdated = 0;

            }
        });

        $scope.schoolBeingUpdated = 0;

        $scope.updateASchoolsEnrollmentTotals = function (schoolid) {
            $scope.loadingEnrollmentData = true;


            var dataParams = {school_id: schoolid};

            $scope.schoolBeingUpdated = schoolid;

            /*load all enrollment data from server and keep track of it in the variable */
            School.allEnrollmentData(dataParams).$promise
                .then(function (data) {
                    loadedEnrollmentInfoFromServer = data[0].school_enrolment;
                    $scope.$emit('enrollmentDataLoaded', {loader: ++dataloader, school_id : schoolid});
                });

            /*load all special enrollment from server and keep track of it in the variable */
            School.allSpecialEnrollmentData(dataParams).$promise
                .then(function (data) {
                    loadedSpecialEnrollmentInfoFromServer = data[0].special_school_enrolment;
                    $scope.$emit('enrollmentDataLoaded', {loader: ++dataloader, school_id : schoolid});
                });
        };

        /*
        * Update Multiple School Enrolments
        * */
        $scope.doMultipleEnrolmentUpdates = function () {

            var school;

            for (var i = 0; i < $scope.selected_circuit.allSchoolsHolder.length; i++) {
                (function (ind) {
                    $timeout(function () {
                        school = $scope.selected_district.allSchoolsHolder[ind];
                    });
                    $scope.updateASchoolsEnrollmentTotals(school.id)
                })(i);

            }
        };













        /*
        * ATTENDANCE UPDATE SECTION
        * */


        var attenDataloader = 0; //This variable prevents the data prepping from happening until both enrollments are loaded
        var loadedAttendanceInfoFromServer = [], loadedSpecialAttendanceInfoFromServer = [];
        var attendanceData = [];

        function initAttendanceVars() {
            attenDataloader = 0; //This variable prevents the data prepping from happening until both are loaded
            attendanceData = [];
            attendanceData.location = {};/*an object whose keys are concatenation of the year,week,term and dc to track location of submitting data*/
            attendanceData.totals = {};/*an object whose keys are concatenation of the year,week,term and dc to track totals of data*/
        }


        function prepAttendanceData() {
            for (var index = 0; index < loadedAttendanceInfoFromServer.length; index ++){
                var item = loadedAttendanceInfoFromServer[index];

                if(item.data_collector_type === 'circuit_supervisor'){
                    continue;
                }


                var concatenate = item.year + item.term + item.week_number + item.data_collector_type;
                if (attendanceData.totals[concatenate]) {
                    attendanceData.totals[concatenate].normal += parseInt(item.total);
                    attendanceData.totals[concatenate].boys += parseInt(item.num_males);
                    attendanceData.totals[concatenate].girls += parseInt(item.num_females);
                } else {
                    attendanceData.totals[concatenate] = {
                        normal: parseInt(item.total),
                        special: 0,
                        boys : parseInt(item.num_males),
                        special_boys : 0,
                        girls : parseInt(item.num_females),
                        special_girls : 0,
                        week_number :  item.week_number,
                        term : item.term,
                        year : item.year
                    }
                }
                var tempObject = {
                    index: index, //This is used to know which index was clicked so we pass it into the chart function
                    selected: false, //This is used to know which index was clicked so we highlight that row
                    class: item.level,
                    boys: item.num_males,
                    girls: item.num_females,
                    streams: item.num_of_streams,
                    total: parseInt(item.num_males) + parseInt(item.num_females),
                    term: item.term,
                    week: item.week_number,
                    year: item.year,
                    special_boys: 0,
                    special_girls: 0,
                    data_collector_type: item.data_collector_type,
                    date_created: moment(item.created_at).utc().valueOf(),
                    normal_id: item.id
                };
                for (var subIndex = 0; subIndex < loadedSpecialAttendanceInfoFromServer.length; subIndex ++) {
                    var special_item = loadedSpecialAttendanceInfoFromServer[subIndex];
                    if (special_item.level === item.level &&
                        special_item.term === item.term &&
                        special_item.week_number === item.week_number &&
                        special_item.year === item.year &&
                        special_item.data_collector_type === item.data_collector_type) {

                        tempObject.special_boys = special_item.num_males;
                        tempObject.special_girls = special_item.num_females;
                        tempObject.special_id = special_item.id;
                        attendanceData.totals[concatenate].special += parseInt(special_item.total);
                        attendanceData.totals[concatenate].special_boys += parseInt(special_item.num_males);
                        attendanceData.totals[concatenate].special_girls += parseInt(special_item.num_females);
                    }
                }
            }
        }

        /*this function send the totals calculated to the server to update the weekly table*/
        function updateSchoolAttendanceWeeklyTotal(school_id) {
            var holder = [];

            var selected_school = $scope.schoolLookup[school_id];

            $scope.updatingAttendanceTotals = true;

            angular.forEach(attendanceData.totals, function (value, key) {
                holder.push({
                    normal_attendance_total_boys : value.boys,
                    normal_attendance_total_girls : value.girls,
                    normal_attendance_total_students : value.normal,
                    special_attendance_total_boys : value.special_boys,
                    special_attendance_total_girls : value.special_girls,
                    special_attendance_total_students : value.special,
                    year : value.year,
                    term : value.term,
                    week_number : value.week_number,
                    country_id : selected_school.country_id,
                    region_id : selected_school.region_id,
                    district_id : selected_school.district_id,
                    circuit_id : selected_school.circuit_id,
                    school_id : selected_school.id
                })
            });


            scSchool.updateWeeklyTableForAttendance(holder)
                .success(function (successData) {
                    if (successData.status === 200) {
                        scNotifier.notifySuccess("Recalculation", "Attendance totals have been updated for " + selected_school.name)
                        $scope.fetchAggregatedData();
                    }
                })
                .error(function (errData) {
                    scNotifier.notifyFailure("Recalculation", "Attendance totals failed to update")
                })
                .finally(function () {
                    $scope.updatingAttendanceTotals = false;/*the variable to hide the gif is here because its is called here regardless of the start point of loadin data*/
                    $scope.attendanceSchoolBeingUpdated = 0;

                });


        }




        //Fired from the below when data is deleted successfully
        $scope.$on('attendanceDataLoaded', function(event, data){
            //this should be greater than 1. that's when both special and normal have loaded
            if (data.loader > 1) {

                initAttendanceVars();
                prepAttendanceData();
                updateSchoolAttendanceWeeklyTotal(data.school_id);
                $scope.updatingAttendanceTotals = false;/*the variable to hide the gif is here because its is called here regardless of the start point of loadin data*/
                $scope.attendanceSchoolBeingUpdated = 0;

            }
        });

        $scope.attendanceSchoolBeingUpdated = 0;

        $scope.updateASchoolsAttendanceTotals = function (schoolid) {
            $scope.updatingAttendanceTotals = true;


            var dataParams = {school_id: schoolid};

            $scope.attendanceSchoolBeingUpdated = schoolid;

            /*load all enrollment data from server and keep track of it in the variable */
            School.allAttendanceData(dataParams).$promise
                .then(function (data) {
                    loadedAttendanceInfoFromServer = data[0].school_attendance;
                    $scope.$emit('attendanceDataLoaded', {loader: ++attenDataloader, school_id : schoolid});
                });

            /*load all special enrollmeent from server and keep track of it in the variable */
            School.allSpecialAttendanceData(dataParams).$promise
                .then(function (data) {
                    loadedSpecialAttendanceInfoFromServer = data[0].special_students_attendance;
                    $scope.$emit('attendanceDataLoaded', {loader: ++attenDataloader, school_id : schoolid});

                });
        };


        /*
         * Update Multiple School Attendance
        * */
        $scope.doMultipleAttendanceUpdates = function () {

            angular.forEach($scope.selected_circuit.allSchoolsHolder, function (school) {
                $scope.attendanceSchoolBeingUpdated = school.id;
                $scope.updateASchoolsAttendanceTotals(school.id)
            });

        };








        /*
        * TEACHER ATTENDANCE SECTION
        * */

        var teacherAttendanceTracker = [], teacherAttendanceTotals = {}, allSchoolTeachersPunctuality_LessonPlan = [];


        $scope.$on('runChildrenStatesSummary', function () {
            $scope.loadingTeachers = false;
        });



        /*this function sends the teacher totals calculated to the server to update the weekly table*/
        var updateTeacherAttendanceWeeklyTotal = function (schoolid) {
            var holder = [];
            var selected_school = $scope.schoolLookup[schoolid];

            angular.forEach(teacherAttendanceTotals, function (value, key) {
                /*calculate the weeks percentage*/
                /*first get the expected total attendance = total teachers in school x number of days school was in session*/
                /*then use the actual total attendance, divided by the expected total number of teachers and multiply by 100 */
                var aveAttendance = (value.totalAttendance/(value.days_in_session * value.expectedTeacherTotal)) * 100;

                holder.push({
                    average_teacher_attendance : Math.round(aveAttendance),
                    current_number_of_teachers : value.teachersTotal,
                    year : value.year,
                    term : value.term,
                    week_number : value.week_number,
                    country_id : selected_school.country_id,
                    region_id : selected_school.region_id,
                    district_id : selected_school.district_id,
                    circuit_id : selected_school.circuit_id,
                    school_id : selected_school.id
                })
            });

            scSchool.updateWeeklyTableForTeacherAttendance(holder)
                .success(function (successData) {
                    if (successData.status === 200) {
                        scNotifier.notifySuccess("Recalculation", "Teacher Attendance totals has been updated for " + selected_school.name)
                    }
                })
                .error(function (errData) {
                    scNotifier.notifyFailure("Recalculation", "Teacher Attendance totals failed to update")
                })
                .finally(function () {
                    $scope.updatingTeacherTotals = false;
                    $scope.teacherSchoolBeingUpdated = 0;
                });


        };


        function prepTeacherAttTotals(schoolid) {
            //These variables only keep track of all weeks, term and years in the punctuality and class management objects
            teacherAttendanceTracker = [];
            teacherAttendanceTotals = {};

            for (var index = 0; index < $scope.teacherSummaryData.teachersInfo.length; index++) {
                var teacherItem = $scope.teacherSummaryData.teachersInfo[index];

                var tempObject = {
                    id: teacherItem.id,
                    punctualityHolder: []
                };


                for (var tp = 0; tp < allSchoolTeachersPunctuality_LessonPlan.length; tp ++) {
                    var teacherPunctualityObject = allSchoolTeachersPunctuality_LessonPlan[tp];

                    if (teacherItem.id === teacherPunctualityObject.teacher_id) {

                        //parse it to int to enforce consistency in searching and filtering
                        teacherPunctualityObject.week_number = parseInt(teacherPunctualityObject.week_number);

                        tempObject.punctualityHolder.push({
                            id: teacherPunctualityObject.id,
                            lesson_plan: teacherPunctualityObject.lesson_plans,
                            days_present: teacherPunctualityObject.days_present,
                            days_punctual: teacherPunctualityObject.days_punctual,
                            days_absent_with_permission: teacherPunctualityObject.days_absent_with_permission,
                            days_absent_without_permission: teacherPunctualityObject.days_absent_without_permission,
                            longitude: teacherPunctualityObject.long,
                            latitude: teacherPunctualityObject.lat,
                            year: teacherPunctualityObject.year,
                            term: teacherPunctualityObject.term,
                            week: teacherPunctualityObject.week_number,
                            data_collector_type: teacherPunctualityObject.data_collector_type,
                            num_of_exercises_given: teacherPunctualityObject.num_of_exercises_given,
                            num_of_exercises_marked: teacherPunctualityObject.num_of_exercises_marked
                        });


                        /*START THE TOTAL TEACHER ATTENDANCE HERE*/

                        /*the concat variable includes the teacher ID so we only add the teachers attendance once,
                        to prevent duplicates for that week, term and year*/
                        var concat = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number +'-'+ teacherPunctualityObject.teacher_id;
                        /*The concat period is only the period WITHOUT the teachers ID*/
                        var concatPeriod = teacherPunctualityObject.year + teacherPunctualityObject.term  + teacherPunctualityObject.week_number;

                        if (teacherAttendanceTracker.indexOf(concat) < 0) {
                            if (angular.isDefined(teacherAttendanceTotals[concatPeriod])) {
                                teacherAttendanceTotals[concatPeriod].totalAttendance += teacherPunctualityObject.days_present;
                                teacherAttendanceTotals[concatPeriod].teachersTotal++;
                            }else{
                                teacherAttendanceTotals[concatPeriod] = {
                                    totalAttendance :  teacherPunctualityObject.days_present,
                                    teachersTotal : 1,
                                    expectedTeacherTotal : $scope.selected_circuit.allSchools[schoolid].totalTeachers,
                                    days_in_session : teacherPunctualityObject.days_in_session,
                                    year: teacherPunctualityObject.year,
                                    term: teacherPunctualityObject.term,
                                    week_number: teacherPunctualityObject.week_number
                                };
                                teacherAttendanceTracker.push(concat);
                            }

                        }

                    }
                }

            }

            // console.log('teacherAttendanceTracker - ', teacherAttendanceTracker);
            // console.log('teacherAttendanceTotals - ', teacherAttendanceTotals);
            updateTeacherAttendanceWeeklyTotal(schoolid);
        }

        /*Teacher attendance update starts here*/
        $scope.updateASchoolsTeacherAttendanceTotals = function (schoolid) {

            /*Check if the teachers have been loaded*/
            /*if the teachers are still being loaded don't show the view go to the home district view*/
            if ($scope.loadingTeachers) {
                scNotifier.notifyInfo("Please wait!", "Fetching teacher list");
                return;
            }

            if (!$scope.teacherSummaryData.teachersInfo.length) {
                scNotifier.notifyInfo("Please wait!", "Teacher list not available. Refresh the page and try again.");
                return;
            }

            $scope.updatingTeacherTotals = true;
            $scope.teacherSchoolBeingUpdated = schoolid;

            var dataParams = {
                year : $scope.summary_year,
                term : $scope.summary_term,
                // week_number : $scope.summary_week,
                district_id : $stateParams.id,
                school_id : schoolid
            };

            School.allSchoolTeachersPunctuality_LessonPlan(dataParams).$promise
                .then(function (punctuality_lesson_plans) {
                    allSchoolTeachersPunctuality_LessonPlan = punctuality_lesson_plans;
                    prepTeacherAttTotals(schoolid);

                    $scope.fetchAggregatedData();
                });

        };

        /*
        * Update Multiple Teacher Attendance
        * */
        $scope.doMultipleTeacherAttendanceUpdates = function () {

            angular.forEach($scope.selected_circuit.allSchoolsHolder, function (school) {
                $timeout(function () {
                    $scope.teacherSchoolBeingUpdated = school.id;
                }, 0);
                $scope.updateASchoolsTeacherAttendanceTotals(school.id)
            });

        };




    }]);