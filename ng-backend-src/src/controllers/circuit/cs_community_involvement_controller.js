/**
 * Created by Kaygee on 23/04/2015.
 */

function incrementAvailability(schoolObj, prop){
    if (prop != '' || prop != null) {
        schoolObj[prop]++;
        return schoolObj;
    }
    return schoolObj
}


schoolMonitor.controller('scViewSelectedCircuitSchoolsMeetingsController',  [
    '$rootScope', '$scope', '$state', '$stateParams', '$modal', 'school_years',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout','School',
    function($rootScope, $scope, $state, $stateParams, $modal, school_years,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout, School) {

        $scope.loadingMeetingData = false;

        function incrementTotal(schoolObj, prop, amount){
            if (!(amount == '' || amount == null || amount == undefined || amount.length < 1 )) {
                schoolObj[prop] = parseInt(schoolObj[prop]) + parseInt(amount);
                return schoolObj;
            }
            return schoolObj
        }

        $scope.fetchMeetingData = function(){
            if (!($scope.meeting_year && $scope.meeting_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingMeetingData = true;
            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.meeting_year,
                term: $scope.meeting_term,
                circuit_id: $stateParams.id
            };
            $scope.meetingData = [];

            School.allSchoolMeetingsHeld(data_to_post).$promise
                .then(
                function (fufilledData) {
                    fufilledData[0].school_meetings.forEach(function (meeting, index) {
                        var schoolObj = {
                            name : scSchool.schoolLookup[meeting.school_id].name,
                            school_id : meeting.school_id,
                            district_id : meeting.district_id,
                            number_held : 0,
                            total_attendance : 0,
                            cs_visits : meeting.number_of_circuit_supervisor_visit,
                            insets : meeting.number_of_inset,
                            data : meeting
                        };
                        /*
                         number_of_circuit_supervisor_visit: 5
                         number_of_inset: 5
                         */
                        schoolObj = incrementTotal(schoolObj, "number_held" , meeting.number_of_pta_meeting);
                        schoolObj = incrementTotal(schoolObj, "number_held" , meeting.number_of_smc_meeting);
                        schoolObj = incrementTotal(schoolObj, "number_held" , meeting.number_of_spam_meeting);
                        schoolObj = incrementTotal(schoolObj, "number_held" , meeting.number_of_staff_meeting);

                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_females_present_at_pta_meeting);
                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_males_present_at_pta_meeting);

                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_females_present_at_smc_meeting);
                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_males_present_at_smc_meeting);

                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_females_present_at_spam_meeting);
                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_males_present_at_spam_meeting);

                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_females_present_at_staff_meeting);
                        schoolObj = incrementTotal(schoolObj, "total_attendance" , meeting.num_males_present_at_staff_meeting);

                        $scope.meetingData.push(schoolObj);
                    });
                    $scope.loadingMeetingData = false;
                },
                function (rejectedData) {
                    $scope.loadingMeetingData = false;

                }
            )
        };

    }]);

schoolMonitor.controller('scViewSelectedCircuitSchoolsCommunityInvolvementController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingSupportTypesData = false;

        $scope.fetchCommunityData = function(){
            if (!($scope.community_year && $scope.community_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingCommunityData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.community_year,
                term: $scope.community_term,
                circuit_id: $stateParams.id
            };
            $scope.communityData = [];


            School.allSchoolCommunityInvolvement(data_to_post).$promise
                .then(
                function (fufilledData) {
                    fufilledData.forEach(function (community, index) {
                        /*circuit_id: 1
                         comment: ""
                         country_id: 1
                         created_at: "2015-02-03 14:51:45"
                         data_collector_id: 5
                         data_collector_type: "head_teacher"
                         district_id: 1
                         id: 3
                         lat: "0"
                         long: "0"

                         questions_category_reference_code: "LCSR"
                         region_id: 1
                         school_id: 2
                         school_reference_code: "u3BoEVL9q5s2gk5h"
                         term: "first_term"
                         updated_at: "2015-02-03 14:51:45"
                         year: "2014/2015"*/
                        var schoolObj = {
                            name : scSchool.schoolLookup[community.school_id].name,
                            school_id : community.school_id,
                            district_id : community.district_id,
                            good : 0,
                            fair : 0,
                            poor : 0,
                            data : community
                        };
                        schoolObj = incrementAvailability(schoolObj, $.trim(community.community_sensitization_for_school_attendance));
                        schoolObj = incrementAvailability(schoolObj, $.trim(community.parents_notified_of_student_progress));
                        schoolObj = incrementAvailability(schoolObj, $.trim(community.pta_involvement));
                        schoolObj = incrementAvailability(schoolObj, $.trim(community.school_management_committees));

                        $scope.communityData.push(schoolObj);
                    });
                    $scope.loadingCommunityData = false;

                },
                function (rejectedData) {
                    $scope.loadingCommunityData = false;

                }
            )
        };
    }]);

schoolMonitor.controller('scViewSelectedCircuitSchoolsGeneralIssuesController',  [
    '$rootScope', '$scope', '$state', '$stateParams', 'school_years', 'School',
    'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','$interval','$timeout',
    function($rootScope, $scope, $state, $stateParams, school_years, School,
             scNotifier, scRegion, scDistrict, scCircuit, scSchool, $interval, $timeout) {

        $scope.loadingSituationData = false;

        $scope.fetchSchoolGeneralSituationData = function(){
            if (!($scope.situation_year && $scope.situation_term)) {
                scNotifier.notifyInfo("No Duration Specified", "Select year and term to load.");
                return;
            }
            $scope.loadingSituationData = true;

            var data_to_post = {
                data_collector_type: "head_teacher",
                year: $scope.situation_year,
                term: $scope.situation_term,
                circuit_id: $stateParams.id
            };
            $scope.situationData = {};

            School.allSchoolGeneralSituation(data_to_post).$promise
                .then(function (fufilledData) {
                    fufilledData.forEach(function (situation, index) {
                        var school = scSchool.schoolLookup[situation.school_id];
                        if (school != undefined) {
                            school = school.name
                        }else{
                            school = "Not available (deleted)"
                        }
                        if ($scope.situationData[situation.school_id]) {
                            $scope.situationData[situation.school_id].data.push(situation);
                        }else{
                            $scope.situationData[situation.school_id] = {
                                submitted : 'yes',
                                name : school,
                                school_id : situation.school_id,
                                district_id : situation.district_id,
                                term : situation.term,
                                year : situation.year,
                                data : [situation]
                            };
                        }

                    });
                    $scope.loadingSituationData = false;

                },
                function (rejectedData) {
                    $scope.loadingSituationData = false;

                }
            )
        };
    }]);