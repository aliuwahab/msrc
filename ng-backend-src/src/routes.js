/**
 * Created by Kaygee on 07/07/2014.
 */


/**
 *  Setting up configuration uiRouter
 */


schoolMonitor.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    //The redirect from the server after login has no trailing slash, hence the redirect send the route to the error page.
    // urlRouter uses the 'when' function to redirect to the main dashboard
    //$urlRouterProvider.when('', '/');
    $urlRouterProvider.when('', 'login');

    //This happens when a url does not exist.
    $urlRouterProvider.otherwise('/error');

    // This is an abstract view for resolving all data from the server. It loads all the data and all other views extend it
    $stateProvider
        .state('login', {
            url : '/login',
            abstract: false,
            controller : 'scAdminControllerLoginAdminUser'
        })

        .state('dashboard', {
            abstract: true,
            controller : 'scGlobalController',
            // Note: abstract still needs a ui-view for its children to populate.
            template: '<ui-view/>',
            resolve: {
                scUser : 'scUser',

                user : function (scUser) {
                    return scUser.authenticate();
                }

            }
        })
        .state('dashboard.home',{
            url :'/',
            templateUrl: 'partials/templates_Main/main.html',
            controller: 'scMainController'
        })

        
        .state('dashboard.settings',{
            url :'/configurations',
            templateUrl: 'partials/templates_Configuration/settings.html',
            controller: 'scAdminController'
        })
        .state('dashboard.settings.general_settings',{
            url :'/general_settings',
            templateUrl: 'partials/templates_Configuration/general_settings.html',
            controller: 'scAdminController'
        })
        .state('dashboard.settings.user_profile',{
            url :'/user_profile',
            templateUrl: 'partials/templates_Configuration/user_profile_settings.html',
            controller: 'scAdminControllerUserProfile'
        })
        .state('dashboard.settings.dashboard_admin',{
            url :'/dashboard_admin',
            templateUrl: 'partials/templates_Configuration/dashboard_admin.html',
            controller: 'scAdminControllerDashboardUsers'

        })
        .state('dashboard.settings.dashboard_admin.add_user',{
            url :'/add_user',
            templateUrl: 'partials/templates_Configuration/add_user_form.html',
            controller: 'scAdminController'
//            view : {
//                add_users : {
//                    templateUrl: 'partials/templates_Configuration/add_user_form.html',
//                    controller: 'scAdminController'
//                }
//            }
        })
        .state('dashboard.settings.dashboard_admin.edit_user',{
            url :'/edit_user/:id',
            templateUrl: 'partials/templates_Configuration/add_user_form.html',
            controller: 'scAdminControllerEditUserProfile'
//            view : {
//                add_users : {
//                    templateUrl: 'partials/templates_Configuration/add_user_form.html',
//                    controller: 'scAdminController'
//                }
//            }
        })
        .state('dashboard.settings.configure_reports',{
            url :'/configure_reports',
            templateUrl: 'partials/templates_Configuration/configure_reports_settings.html',
            controller: 'scAdminController'
        })
        .state('dashboard.settings.user_logs',{
            url :'/user_logs',
            templateUrl: 'partials/templates_Configuration/user_logs.html',
            controller: 'scAdminControllerShowUserLogs',
            resolve : {
                UserLogs : 'UserLogs',
                createdLogs : function (UserLogs) {
                    return UserLogs.getCreatedActions()
                },

                editedLogs : function (UserLogs) {
                    return UserLogs.getEditedActions()
                },

                deletedLogs : function (UserLogs) {
                    return UserLogs.getDeletedActions()
                }
            }
        })

        /*VIEW ROUTES*/
        .state('dashboard.view',{
            url :'/view',
            templateUrl: 'partials/view.html'
            //controller: 'scViewController'
        })


 .state('dashboard.view_Pupil_Aggregated_Data',{
            url :'/view/Pupil_Aggregated_Data/',
            templateUrl: 'partials/templates_View/country/Pupil_Aggregated_Data.html',
            controller: 'submitFormController'
        })


        .state('dashboard.t1viewpoll',{
            url :'/targetedinstruction/analytics/poll',
            templateUrl: 'partials/templates_Targeted_Instruction/poll.html',
            controller: ''
        })

        .state('dashboard.t1viewregion',{
            url :'/t1/region/all',
            templateUrl: 'partials/templates_View/region/t2.html',
            controller: 'MkT1ViewRegionController'
        })


    // .state('dashboard.t1viewcircuit', {
    //         url: '/t1/circuit/all',
    //         templateUrl: 'partials/templates_View/circuit/t4.html',
    //         controller: 'sct1CircuitController'
    //     })
// .state('dashboard.t1viewdistrict', {
//             url: '/t1/district/all',
//             templateUrl: 'partials/templates_View/district/t3.html',
//             controller: 'sct1DistrictController'
//         })
// .state('dashboard.t1viewschool', {
//             url: '/t1/school/all',
//             templateUrl: 'partials/templates_View/school/t5.html',
//             controller: 'sct1SchoolController'
//         })
        .state('dashboard.t1viewcircuit',{
            url :'/t1/circuit/all',
            templateUrl: 'partials/templates_View/circuit/t4.html',
            controller: 'MkT1ViewCircuitController'
        })


        .state('dashboard.t1viewdistrict',{
            url :'/t1/district/all',
            templateUrl: 'partials/templates_View/district/t3.html',
            controller: 'MkT1ViewDistrictController'
        })


        .state('dashboard.t1viewschool',{
            url :'/t1/school/all',
            templateUrl: 'partials/templates_View/school/t5.html',
            controller: 'MkT1ViewSchoolController'
        })

        .state('dashboard.t1viewschoolone', {
            url: '/view/school/attendance/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/viewschoolone.html',
            controller: 'submitAttendanceSchoolGraphFormController'
        })

        .state('dashboard.t1viewschooltwo', {
            url: '/view/school/pupilaggregated/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/viewschooltwo.html',
            controller: 'submitPupilAggregatedDataSchoolGraphFormController'
        })

        .state('dashboard.t1viewcircuitone', {
            url: '/view/circuits/attendance/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/viewcircuitone.html',
            controller: 'submitAttendanceCircuitGraphFormController'
        })

        .state('dashboard.t1viewcircuittwo', {
            url: '/view/circuits/pupilaggregated/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/viewcircuittwo.html',
            controller: 'submitPupilAggregatedDataCircuitGraphFormController'
        })

        //View Country
        .state('dashboard.view_country',{
            url :'/view/countries/all',
            templateUrl: 'partials/templates_View/country/view_country.html',
            controller: 'scViewCountryController'
        })

        .state('dashboard.view_countrytwo',{
            url :'/view/countriestwo/all',
            templateUrl: 'partials/templates_View/country/view_countrytwo.html',
            controller: 'scViewCountryController'
        })

        .state('dashboard.pupil_data',{
            url :'/targetedinstruction/pupil_aggregated_data',
            templateUrl: 'partials/templates_Targeted_Instruction/pupil_aggregated_data.html',
            controller: 'scPupilAggregatedController'
        })

        .state('dashboard.view_country_details',{
            url :'/view/:id/country/details',
            templateUrl: 'partials/templates_View/country/view_country_details.html',
            controller: 'scViewSelectedCountryController'
        })

        .state('dashboard.view_country_details.teachers',{
            url :'/country_teachers',
            templateUrl: 'partials/templates_View/country/country_teachers.html',
            controller: 'scViewSelectedCountryTeacherController'
        })

        .state('dashboard.view_country_details.country_stats',{
            url :'/country_stats',
            views : {
                "country_stats": {
                    templateUrl: 'partials/templates_View/country/country_statistics_analytics.html',
                    controller: 'scViewSelectedCountryAnalyticsStatsController'
                }
            }
        })

//scViewSelectedCountryController
        .state('dashboard.t1viewcountry.loaddata',{
            url :'/loaddata',
            views : {
                "loaddata": {
                    templateUrl: 'partials/templates_View/country/new.html',
                    controller: 'scViewSelectedCountryAnalyticsStatsController'
                }
            }
        })
        // .state('dashboard.t1viewcountry.loaddata',{
        //     url :'/loaddata',
        //     templateUrl: 'partials/templates_View/country/new.html',
        //     controller: 'scViewSelectedCountryController'
        //
        // })



        .state('dashboard.view_country_details.schools_stats_weekly',{
            url :'/weekly_stats',
            templateUrl: 'partials/templates_View/country/country_stats_weekly.html',
            controller: 'scViewSelectedCountrySchoolsStatsWeeklyController'
        })
        .state('dashboard.view_country_details.schools_stats_termly',{
            url :'/termly_stats',
            templateUrl: 'partials/templates_View/country/country_stats_termly.html',
            controller: 'scViewSelectedCountrySchoolsStatsTermlyController'
        })
        .state('dashboard.view_country_details.other_termly',{
            url :'/other_termly_sessions',
            templateUrl: 'partials/templates_View/country/other_termly_sessions_main.html'
        })

        /*start here*/
        .state('dashboard.view_country_details.other_termly.school_management',{
            url :'/school_management',
            templateUrl: 'partials/templates_View/country/country_main_school_management.html'
        })
        .state('dashboard.view_country_details.other_termly.school_grounds',{
            url :'/school_grounds',
            templateUrl: 'partials/templates_View/country/country_main_school_grounds.html'
        })
        .state('dashboard.view_country_details.other_termly.community_involvement',{
            url :'/community_involvement',
            templateUrl: 'partials/templates_View/country/country_main_community_involvement.html'
        })
        /*end here*/


        //View Region
        .state('dashboard.view_region',{
            url :'/view/regions/all',
            templateUrl: 'partials/templates_View/region/view_region.html',
            controller: 'scViewRegionController'
        })
        .state('dashboard.view_selected_region',{
            url :'/view/region/:id',
            templateUrl: 'partials/templates_View/region/view_selected_region.html',
            controller: 'scViewSelectedRegionController'
        })

        .state('dashboard.view_selected_region.rating_view',{
            url :'/rating',
            templateUrl: 'partials/templates_View/region/region_rating.html',
            controller: 'scViewSelectedRegionRatingController'
        })
        .state('dashboard.view_selected_region.schools_stats_weekly',{
            url :'/schools/weekly',
            templateUrl: 'partials/templates_View/region/region_schools_stats_weekly.html',
            controller: 'scViewSelectedRegionSchoolsStatsWeeklyController'
        })
        .state('dashboard.view_selected_region.schools_stats_termly',{
            url :'/schools/termly',
            templateUrl: 'partials/templates_View/region/region_schools_stats_termly.html',
            controller: 'scViewSelectedRegionSchoolsStatsTermlyController'
        })
        .state('dashboard.view_selected_region.other_termly',{
            url :'/other_termly_sessions',
            templateUrl: 'partials/templates_View/region/other_termly_sessions_main.html',
            controller: 'scViewSelectedCircuitOtherTermlyMainController'
        })

        /*start here*/
        .state('dashboard.view_selected_region.other_termly.school_management',{
            url :'/school_management',
            templateUrl: 'partials/templates_View/region/rg_main_school_management.html'
        })
        .state('dashboard.view_selected_region.other_termly.school_grounds',{
            url :'/school_grounds',
            templateUrl: 'partials/templates_View/region/rg_main_school_grounds.html'
        })
        .state('dashboard.view_selected_region.other_termly.community_involvement',{
            url :'/community_involvement',
            templateUrl: 'partials/templates_View/region/rg_main_community_involvement.html'
        })
        /*end here*/

        .state('dashboard.view_selected_region.teachers',{
            url :'/district_teachers',
            templateUrl: 'partials/templates_View/region/region_teachers.html',
            controller: 'scViewSelectedRegionTeachersController'
        })
        .state('dashboard.view_selected_region.cs_attendance_average',{
            url :'/cs_attendance_average',
            templateUrl: 'partials/templates_View/region/region_cs_attendance_average.html',
            controller: 'scViewSelectedRegionCSAttendanceController'
        })
        .state('dashboard.view_selected_region.ht_attendance_average',{
            url :'/ht_attendance_average',
            templateUrl: 'partials/templates_View/region/region_ht_attendance_average.html',
            controller: 'scViewSelectedRegionHTAttendanceController'
        })
        .state('dashboard.add_region',{
            url :'/view/region/modify/:action/:id',
            templateUrl: 'partials/templates_View/region/add_region.html',
            controller: 'scViewRegionController'
        })


        //View District
        .state('dashboard.view_district',{
            url :'/view/districts/all',
            templateUrl: 'partials/templates_View/district/view_district.html',
            controller: 'scViewDistrictController'
        })
        .state('dashboard.view_selected_district',{
            url :'/view/district/:id',
            templateUrl: 'partials/templates_View/district/view_selected_district.html',
            controller: 'scViewSelectedDistrictController'
        })
        .state('dashboard.view_selected_district.rating_view',{
            url :'/rating',
            templateUrl: 'partials/templates_View/district/district_rating.html',
            controller: 'scViewSelectedDistrictRatingController'
        })
        .state('dashboard.view_selected_district.schools_stats_weekly',{
            url :'/schools/weekly',
            templateUrl: 'partials/templates_View/district/district_schools_stats_weekly.html',
            controller: 'scViewSelectedDistrictSchoolsStatsWeeklyController'
        })
        .state('dashboard.view_selected_district.schools_stats_termly',{
            url :'/schools/termly',
            templateUrl: 'partials/templates_View/district/district_schools_stats_termly.html',
            controller: 'scViewSelectedDistrictSchoolsStatsTermlyController'
        })
        //        .state('dashboard.view_selected_district.cs_school',{
        //            url :'/cs_schools',
        //            templateUrl: 'partials/templates_View/district/district_cs_schools.html',
        //            controller: 'scViewSelectedDistrictCsSchoolsController'
        //        })

        .state('dashboard.view_selected_district.teachers',{
            url :'/district_teachers',
            templateUrl: 'partials/templates_View/district/district_teachers.html',
            controller: 'scViewSelectedDistrictTeachersController'
        })
        .state('dashboard.view_selected_district.other_termly',{
            url :'/other_termly_sessions',
            templateUrl: 'partials/templates_View/district/other_termly_sessions_main.html',
            controller: 'scViewSelectedCircuitOtherTermlyMainController'
        })

        /*start here*/
        .state('dashboard.view_selected_district.other_termly.school_management',{
            url :'/school_management',
            templateUrl: 'partials/templates_View/district/ds_main_school_management.html'
        })
        .state('dashboard.view_selected_district.other_termly.school_grounds',{
            url :'/school_grounds',
            templateUrl: 'partials/templates_View/district/ds_main_school_grounds.html'
        })
        .state('dashboard.view_selected_district.other_termly.community_involvement',{
            url :'/community_involvement',
            templateUrl: 'partials/templates_View/district/ds_main_community_involvement.html'
        })
        /*end here*/

        .state('dashboard.view_selected_district.cs_attendance_average',{
            url :'/cs_attendance_average',
            templateUrl: 'partials/templates_View/district/district_cs_attendance_average.html',
            controller: 'scViewSelectedDistrictCSAttendanceController'
        })
        .state('dashboard.view_selected_district.ht_attendance_average',{
            url :'/ht_attendance_average',
            templateUrl: 'partials/templates_View/district/district_ht_attendance_average.html',
            controller: 'scViewSelectedDistrictHTAttendanceController'
        })
        .state('dashboard.add_district',{
            url :'/view/district/modify/:action/:id',
            templateUrl: 'partials/templates_View/district/add_district.html',
            controller: 'scViewDistrictController'
        })

        //View Circuit
        .state('dashboard.view_circuit',{
            url :'/view/circuits/all',
            templateUrl: 'partials/templates_View/circuit/view_circuit.html',
            controller: 'scViewCircuitController'
        })
        .state('dashboard.view_selected_circuit',{
            url :'/view/circuit/:id',
            templateUrl: 'partials/templates_View/circuit/view_selected_circuit.html',
            controller: 'scViewSelectedCircuitController'
        })
        .state('dashboard.view_selected_circuit.rating_view',{
            url :'/rating',
            templateUrl: 'partials/templates_View/circuit/circuit_rating.html',
            controller: 'scViewSelectedCircuitRatingController'
        })
        .state('dashboard.view_selected_circuit.schools_stats_weekly',{
            url :'/schools/weekly',
            templateUrl: 'partials/templates_View/circuit/circuit_schools_stats_weekly.html',
            controller: 'scViewSelectedCircuitSchoolsStatsWeeklyController'
        })
        .state('dashboard.view_selected_circuit.schools_stats_termly',{
            url :'/schools/termly',
            templateUrl: 'partials/templates_View/circuit/circuit_schools_stats_termly.html',
            controller: 'scViewSelectedCircuitSchoolsStatsTermlyController'
        })
        //        .state('dashboard.view_selected_circuit.cs_school',{
        //            url :'/cs_schools',
        //            templateUrl: 'partials/templates_View/district/district_cs_schools.html',
        //            controller: 'scViewSelectedCircuitCsSchoolsController'
        //        })
        .state('dashboard.view_selected_circuit.teachers',{
            url :'/district_teachers',
            templateUrl: 'partials/templates_View/circuit/circuit_teachers.html',
            controller: 'scViewSelectedCircuitTeachersController'
        })
        .state('dashboard.view_selected_circuit.cs_attendance_average',{
            url :'/cs_attendance_average',
            templateUrl: 'partials/templates_View/circuit/circuit_cs_attendance_average.html',
            controller: 'scViewSelectedCircuitCSAttendanceController'
        })
        .state('dashboard.view_selected_circuit.ht_attendance_average',{
            url :'/ht_attendance_average',
            templateUrl: 'partials/templates_View/circuit/circuit_ht_attendance_average.html',
            controller: 'scViewSelectedCircuitHTAttendanceController'
        })
        .state('dashboard.view_selected_circuit.other_termly',{
            url :'/other_termly_sessions',
            templateUrl: 'partials/templates_View/circuit/other_termly_sessions_main.html',
            controller: 'scViewSelectedCircuitOtherTermlyMainController'
        })

        /*start here*/
        .state('dashboard.view_selected_circuit.other_termly.school_management',{
            url :'/school_management',
            templateUrl: 'partials/templates_View/circuit/cs_main_school_management.html'
        })
        .state('dashboard.view_selected_circuit.other_termly.school_grounds',{
            url :'/school_grounds',
            templateUrl: 'partials/templates_View/circuit/cs_main_school_grounds.html'
        })
        .state('dashboard.view_selected_circuit.other_termly.community_involvement',{
            url :'/community_involvement',
            templateUrl: 'partials/templates_View/circuit/cs_main_community_involvement.html'
        })
        /*end here*/

        .state('dashboard.add_circuit',{
            url :'/view/circuit/modify/:action/:id',
            templateUrl: 'partials/templates_View/circuit/add_circuit.html',
            controller: 'scViewCircuitController'
        })


        //View School\
        .state('dashboard.school',{
            abstract: true,
            template : '<ui-view/>'
        })

//////////
        
 .state('dashboard.school.view_school',{
            url :'/view/schools/all',
            templateUrl: 'partials/templates_View/school/view_school.html',
            controller: 'scViewSchoolController'
        })



// .state('dashboard.targeted_instruction.pupil_aggregated_data',{
//             url :'/targetedinstruction/pupil_aggregated_data',
//             templateUrl:'partials/templates_Targeted_Instruction/targeted_instruction.html',
//             controller:'scViewSchoolController'
//         })


// .state('dashboard.targeted_instruction.pupil_aggregated_data',{
//             url :'/view/schools/all',
//             templateUrl: 'partials/templates_View/school/view_school.html',
//             controller: 'scViewSchoolController'
//         })  

// .state('dashboard.add_data_collector',{
//             url :'/data_collectors/:type/:action/:id',
//             templateUrl: 'partials/templates_Stakeholders/add_data_collector.html',
//             controller: 'scAddDataCollectorController'
//         })






// .state('dashboard.analytics',{
//             url :'/targetedinstruction/analytics',
//             templateUrl: 'partials/templates_Targeted_Instruction/analytics.html',
//             controller: 'scViewSelectedCountryController'
//
//         })


 // .state('dashboard.t1viewcountry',{
 //             url :'/t1_analytics/country',
 //             templateUrl: 'partials/templates_Targeted_Instruction/analytics.html',
 //             controller: ''
 //
 //         })


        // .state('dashboard.t1viewcountry',{
        //     url :'/t1_analytics/country',
        //     templateUrl: 'partials/templates_Targeted_Instruction/analytics.html',
        //     controller: 'scViewSelectedCountryAnalyticsStatsController'

        // })
.state('dashboard.t1viewcountry', {
            url: '/view/country/attendance/',
            templateUrl: 'partials/templates_Targeted_Instruction/analytics.html',
            controller: 'submitPupilAggregatedDataCountryGraphFormController'
        })


        // .state('dashboard.view_countrytwo',{
        //     url :'/view/countriestwo/all',
        //     templateUrl: 'partials/templates_View/country/view_countrytwo.html',
        //     controller: 'scViewCountryController'
        // })


// .state('dashboard.school.view_school.class_observation',{
//             url :'/targetedinstruction/class_observation',
//             templateUrl: 'partials/templates_Targeted_Instruction/class_observation.html',
//             controller: 'scTargeted_InstructionController'
//
//         })

        .state('dashboard.class_observation',{
            url :'/targetedinstruction/class_observation',
            templateUrl: 'partials/templates_Targeted_Instruction/class_observation.html',
            controller: 'submitClassObservationFormController'
        })



        .state('dashboard.t1viewdistrictone', {
            url: '/view/districts/attendance/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/analyticsc.html',
            controller: 'submitPupilAggregatedDataDistrictGraphFormController'
        })

        .state('dashboard.t1viewdistricttwo', {
            url: '/view/districts/pupilaggregated/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/analyticstwod.html',
            controller: 'submitAttendanceDistrictGraphFormController'
        })






         .state('dashboard.t1viewregionone', {
            url: '/view/region/attendance/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/analyticsa.html',
            controller: 'submitPupilAggregatedDataRegionGraphFormController'
        })

        

        .state('dashboard.t1viewregiontwo', {
            url: '/view/region/pupilaggregated/:id',
            templateUrl: 'partials/templates_Targeted_Instruction/analyticstwob.html',
            controller: 'submitAttendanceRegionGraphFormController'

        })



        .state('dashboard.selected_region',{
            url :'/selected/region/:id',
            templateUrl: 'partials/templates_View/region/selected_region.html',
            controller: 'scSelectedRegionController'
        })


        // .state('dashboard.t1viewregion',{
        //     url :'/t1/region/all',
        //     templateUrl: 'partials/templates_View/region/t2.html',
        //     controller: 'scRegionController'
        // })


           .state('dashboard.school.view_school.attendance',{
            url :'/targetedinstruction/attendance',
            templateUrl: 'partials/templates_Targeted_Instruction/attendance.html',
            controller: 'scTargeted_InstructionController'

        })

////////////////
        // .state('dashboard.school.view_school',{
        //     url :'/view/schools/all',
        //     templateUrl: 'partials/templates_View/school/view_school.html',
        //     controller: 'scViewSchoolController'
        // })

        //School Report Card - Add and Edit School
        .state('dashboard.school.add_school',{
            url :'/view/school/modify/:action/:id',
            templateUrl: 'partials/templates_View/school/add_school.html',
            controller: 'scViewSchoolController'
        })
        //View Selected School
        .state('dashboard.school.view_selected_school',{
            url :'/view/school/:id/district/:district_id',
            templateUrl: 'partials/templates_View/school/view_selected_school.html',
            controller: 'scViewSelectedSchoolController',
            resolve : {
                scSchool : 'scSchool',
                districtItinerary : function(scSchool, $stateParams){
                    return  scSchool.fetchItinerary($stateParams.district_id)
                }
            }
        })

        //--------------------------School Statistics---------------------------------------//
        .state('dashboard.school.view_selected_school.statistics',{
            url :'/statistics',
            views: {
                "statistics": {
                    templateUrl: 'partials/templates_View/school/school_statistics.html',
                    controller: 'scViewSchoolStatisticsController'
                }
            }
        })

        //--------------------------School Report Card---------------------------------------//
        //School Report Card - School Report Card
        .state('dashboard.school.view_selected_school.report_card',{
            url :'/schoolreportcard',
            views: {
                "school_report_card": {
                    templateUrl: 'partials/templates_View/school/school_report_card.html',
                    controller: 'scViewSchoolReportCardController'
                }
            }
        })
        //School Report Card - School Enrollment
        .state('dashboard.school.view_selected_school.report_card.enrollment',{
            url :'/enrollment',
            views: {
                "report_card": {
                    templateUrl: 'partials/templates_View/school/src_enrollment.html',
                    controller: 'scViewSchoolEnrollmentController'
                }
            }
        })
        //School Report Card - School Attendance
        .state('dashboard.school.view_selected_school.report_card.attendance',{
            url :'/attendance',
            views: {
                "report_card": {
                    /*remember to uncomment the resolve when you uncomment the llines below...lol*/
                    //templateUrl: 'partials/templates_View/school/src_attendance.html',
                    templateUrl: 'partials/templates_View/school/src_attendance.html',
                    controller: 'scViewSchoolAttendanceController'
                }
            }
        })

        //School Report Card -Teacher Information
        .state('dashboard.school.view_selected_school.report_card.teacher_information',{
            url :'/teacher_information',
            views: {
                "report_card": {
                    templateUrl: 'partials/templates_View/school/src_teacher_information.html',
                    controller: 'scViewSchoolTeacherInformationController'
                }
            }
        })

        //School Report Card - School Records and Performance
        .state('dashboard.school.view_selected_school.report_card.school_management',{
            url :'/school_management',
            views: {
                "report_card": {
                    templateUrl: 'partials/templates_View/school/src_school_management.html',
                    controller: 'scViewSchoolManagementController'
                }
            },
            resolve: {

                School : 'School',

                allSchoolTextbookData : function(School, $stateParams){
                    return School.allSchoolTextbook({school_id : $stateParams.id}).$promise
                },

                allSchoolPupilPerformanceData : function(School, $stateParams){
                    return School.allSchoolPupilPerformance({school_id : $stateParams.id}).$promise
                },

                allRecordPerformanceData : function(School, $stateParams){
                    return School.allRecordPerformanceData({school_id : $stateParams.id}).$promise
                },

                allSupportTypesData : function(School, $stateParams){
                    return School.allSupportTypes({school_id : $stateParams.id}).$promise
                },

                allSchoolGrantCapitationPaymentsData : function(School, $stateParams){
                    return School.allSchoolGrantCapitationPayments({school_id : $stateParams.id}).$promise
                }

            }
        })

        //School Report Card - School Management / School Grounds / School Facilities
        .state('dashboard.school.view_selected_school.report_card.school_grounds',{
            url :'/school_grounds',
            views: {
                "report_card": {
                    templateUrl: 'partials/templates_View/school/src_school_grounds.html',
                    controller: 'scViewSchoolGroundsController'
                }
            },
            resolve: {

                School : 'School',

                allSchoolSanitation : function(School, $stateParams){
                    return School.allSchoolSanitation({school_id : $stateParams.id}).$promise;
                },
                allSchoolRecreation_Equipment : function(School, $stateParams){
                    return School.allSchoolRecreation_Equipment({school_id : $stateParams.id}).$promise
                },
                allSchoolSecurity : function(School, $stateParams){
                    return School.allSchoolSecurity({school_id : $stateParams.id}).$promise
                },
                allSchoolStructure : function(School, $stateParams){
                    return School.allSchoolStructure({school_id : $stateParams.id}).$promise
                },
                allSchoolFurniture : function(School, $stateParams){
                    return School.allSchoolFurniture({school_id : $stateParams.id}).$promise
                }
            }
        })

        //School Report Card - School Community Involvement
        .state('dashboard.school.view_selected_school.report_card.community_involvement',{
            url :'/community_involvement',
            views: {
                "report_card": {
                    templateUrl: 'partials/templates_View/school/src_community_involvement.html',
                    controller: 'scViewSchoolCommunityInvolvementController'
                }
            },
            resolve: {

                School : 'School',

                allSchoolCommunityInvolvementData : function(School, $stateParams){
                    return School.allSchoolCommunityInvolvement({school_id : $stateParams.id}).$promise
                },

                allSchoolMeetingsHeldData : function(School, $stateParams){
                    return School.allSchoolMeetingsHeld({school_id : $stateParams.id}).$promise
                },

                allSchoolGeneralSituationData : function(School, $stateParams){
                    return School.allSchoolGeneralSituation({school_id : $stateParams.id}).$promise
                }
            }
        })

        //--------------------------School Visit---------------------------------------//
        .state('dashboard.school.view_selected_school.school_visit',{
            url :'/schoolvisits',
            views: {
                "school_visit": {
                    templateUrl: 'partials/templates_View/school/school_visit.html',
                    controller: 'scViewSchoolVisitController'
                }
            },
            resolve : {

                School : 'School',

                allSchoolVisitsData : function (School, $stateParams){
                    return School.allSchoolVisitsData({school_id : $stateParams.id}).$promise
                }
            }
        })
        //School Visit Teacher Attendance
        .state('dashboard.school.view_selected_school.school_visit.teacher_attendance',{
            url :'/teacher_attendance',
            views: {
                "school_visit_sub_sections": {
                    templateUrl: 'partials/templates_View/school/selected_school_visit_teacher_attendance.html'
                }
            }
        })
        //School Visit Student Attendance
        .state('dashboard.school.view_selected_school.school_visit.student_attendance',{
            url :'/student_attendance',
            views: {
                "school_visit_sub_sections": {
                    templateUrl: 'partials/templates_View/school/selected_school_visit_student_attendance.html'
                }
            }
        })
        //School Visit Facilities, Safety and Sanitation
        .state('dashboard.school.view_selected_school.school_visit.facilities_safety_sanitation',{
            url :'/facilities_safety_sanitation',
            views: {
                "school_visit_sub_sections": {
                    templateUrl: 'partials/templates_View/school/selected_school_visit_facilities_safety_sanitation.html'
                }
            }
        })
        //School Visit Lesson Plans
        .state('dashboard.school.view_selected_school.school_visit.lesson_plans',{
            url :'/lesson_plans',
            views: {
                "school_visit_sub_sections": {
                    templateUrl: 'partials/templates_View/school/selected_school_visit_lesson_plans.html'
                }
            }
        })
        //School Visit Learning Materials
        .state('dashboard.school.view_selected_school.school_visit.teaching_learning_materials',{
            url :'/teaching_learning_materials',
            views: {
                "school_visit_sub_sections": {
                    templateUrl: 'partials/templates_View/school/selected_school_visit_teaching_learning_materials.html'
                }
            }
        })

        //School Visit Community Involvement
        .state('dashboard.school.view_selected_school.school_visit.community_involvement',{
            url :'/community_involvement',
            views: {
                "school_visit_sub_sections": {
                    templateUrl: 'partials/templates_View/school/selected_school_visit_community_involvement.html'
                }
            }
        })
        //School Visit Records Performance
        .state('dashboard.school.view_selected_school.school_visit.records_performance',{
            url :'/records_performance',
            views: {
                "school_visit_sub_sections": {
                    templateUrl: 'partials/templates_View/school/selected_school_visit_records_performance.html'
                }
            }
        })
        /* -------------SCHOOL REVIEW ROUTES ----------------------*/

        //School Review -Home with Sidebar template
        .state('dashboard.school.view_selected_school.review',{
            url :'/schoolreview',
            views: {
                "review": {
                    templateUrl: 'partials/templates_View/school/school_review.html',
                    controller: 'scViewSchoolReviewMainController'
                }
            },
            resolve: {
//
                scSchool : 'scSchool',
                schoolInclusiveData : function(School, $stateParams){
                    return School.schoolInclusiveData({school_id : $stateParams.id}).$promise
                },
                schoolEffectiveTeachingData : function(School, $stateParams){
                    return School.schoolEffectiveTeachingData({school_id : $stateParams.id}).$promise
                },
                schoolHealthyData : function(School, $stateParams){
                    return School.schoolHealthyData({school_id : $stateParams.id}).$promise
                },
                schoolSafeProtectiveData : function(School, $stateParams){
                    return School.schoolSafeProtectiveData({school_id : $stateParams.id}).$promise
                },
                schoolFriendlyData : function(School, $stateParams){
                    return School.schoolFriendlyData({school_id : $stateParams.id}).$promise
                },
                schoolCommunityInvolvementData : function(School, $stateParams){
                    return School.schoolCommunityInvolvementData({school_id : $stateParams.id}).$promise
                }
            }
        })

        // School Review Inclusive Route
        .state('dashboard.school.view_selected_school.review.inclusive',{
            url :'/inclusive',
            views: {
                "review_sub_view": {
                    templateUrl: 'partials/templates_View/school/selected_school_review_inclusive.html',
                    controller: 'scViewSchoolReviewInclusiveController'
                }
            }
        })

        /* -------------SCHOOL REVIEW EFFECTIVE TEACHING AND LEARNING ----------------------*/
        .state('dashboard.school.view_selected_school.review.effective_teaching_learning',{
            url :'/effective_teaching_learning',
            views: {
                "review_sub_view": {
                    templateUrl: 'partials/templates_View/school/selected_school_review_effective_teaching.html',
                    controller: 'scViewSchoolReviewEffectiveTeachingController'
                }
            }
        })

        /* -------------SCHOOL REVIEW HEALTHY SCHOOL ----------------------*/
        .state('dashboard.school.view_selected_school.review.healthy',{
            url :'/healthy',
            views: {
                "review_sub_view": {
                    templateUrl: 'partials/templates_View/school/selected_school_review_healthy_school.html',
                    controller: 'scViewSchoolReviewHealthyController'
                }
            }
        })

        /* -------------SCHOOL REVIEW SAFE AND PROTECTIVE ----------------------*/
        .state('dashboard.school.view_selected_school.review.safe_protective',{
            url :'/safe_protective',
            views: {
                "review_sub_view": {
                    templateUrl: 'partials/templates_View/school/selected_school_review_safe_protective.html',
                    controller: 'scViewSchoolReviewSafeProtectiveController'
                }
            }
        })

        /* -------------SCHOOL REVIEW FRIENDLY FOR BOYS AND GIRLS ----------------------*/
        .state('dashboard.school.view_selected_school.review.gender_friendly',{
            url :'/gender_friendly',
            views: {
                "review_sub_view": {
                    templateUrl: 'partials/templates_View/school/selected_school_review_friendly_school.html',
                    controller: 'scViewSchoolReviewFriendlyController'
                }
            }
        })

        /* -------------SCHOOL REVIEW  COMMUNITY INVOLVEMENT ----------------------*/
        .state('dashboard.school.view_selected_school.review.community_involvement',{
            url :'/community_involvement',
            views: {
                "review_sub_view": {
                    templateUrl: 'partials/templates_View/school/selected_school_review_community_involvement.html',
                    controller: 'scViewSchoolReviewCommunityInvolvementController'
                }
            }
        })
        /* -------------STARS IPA INTEGRATION ROUTES ----------------------*/
        .state('dashboard.school.view_selected_school.stars',{
            url :'/stars_integtration',
            views: {
                "review_sub_view": {
                    templateUrl: 'partials/templates_View/school/selected_school_review_community_involvement.html',
                    controller: 'scViewSchoolReviewCommunityInvolvementController'
                }
            }
        })

        /*END OF VIEW ROUTES*/

        /*DataCollectors ROUTES*/
        .state('dashboard.data_collectors',{
            url :'/data_collectors',
            templateUrl: 'partials/templates_Stakeholders/stakeholders.html',
            controller: 'scStakeholderController'
        })
        .state('dashboard.data_collectors_circuit_supervisors',{
            url :'/data_collectors/circuit_supervisors',
            templateUrl: 'partials/templates_Stakeholders/circuit_supervisors.html',
            controller: 'scCircuitSupervisorController'
        })
        .state('dashboard.data_collectors_circuit_supervisors.show_all',{
            url :'/display_all',
            templateUrl: 'partials/templates_Stakeholders/view_all_circuit_supervisors.html',
            controller: 'scAllCircuitSupervisorsController'
        })

        .state('dashboard.view_data_collector',{
            url :'/data_collector/:id',
            templateUrl: 'partials/templates_Stakeholders/selected_data_collector.html',
            controller: 'scViewDataCollectorController'
        })

        .state('dashboard.data_collectors_head_teachers',{
            url :'/data_collectors/head_teachers',
            templateUrl: 'partials/templates_Stakeholders/head_teachers.html',
            controller: 'scHeadTeacherController'
        })
        .state('dashboard.data_collectors_head_teachers.show_all',{
            url :'/display_all',
            templateUrl: 'partials/templates_Stakeholders/view_all_headteachers.html',
            controller: 'scAllHeadTeachersController'
        })
        .state('dashboard.add_data_collector',{
            url :'/data_collectors/:type/:action/:id',
            templateUrl: 'partials/templates_Stakeholders/add_data_collector.html',
            controller: 'scAddDataCollectorController'
        })

        .state('dashboard.stakeholders_community',{
            url :'/stakeholders/community',
            templateUrl: 'partials/templates_Stakeholders/community.html',
            controller: 'scStakeholderCommunityController'

        })
        


        .state('dashboard.generate_report',{
            url :'/emis_report',
            templateUrl: 'partials/templates_Report/report_main.html',
            controller: 'scReportController'
        })
        .state('dashboard.error',{
            url :'/error',
            templateUrl: 'partials/error.html'

//            controller: 'testCtrl'
        });


}]);
