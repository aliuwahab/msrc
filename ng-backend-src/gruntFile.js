module.exports = function (grunt) {

       

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-html2js');

    // Default task.
       

    grunt.registerTask('default', ['html2js','concat', 'timestamp']);
    grunt.registerTask('htmls', ['html2js:app', 'timestamp']);
    grunt.registerTask('php', ['concat:index', 'timestamp']);
    grunt.registerTask('css', ['concat:stylesheets', 'timestamp']);
    grunt.registerTask('app', ['concat:app_files', 'timestamp']);
    grunt.registerTask('scripts', ['concat:scripts', 'timestamp']);
    grunt.registerTask('load', ['app', 'htmls']);

    /*Mobile Tasks*/
    grunt.registerTask('mobile_php', ['concat:mobile_index', 'timestamp']);
    grunt.registerTask('mobile_htmls', ['html2js:mobile_app', 'timestamp']);
    grunt.registerTask('mobile_css', ['concat:mobile_stylesheets', 'timestamp']);
    grunt.registerTask('mobile_scripts', ['concat:mobile_scripts', 'timestamp']);
    grunt.registerTask('mobile_app', ['concat:mobile_app_files', 'timestamp']);
    grunt.registerTask('mobile_load', ['mobile_app', 'mobile_htmls']);
    /*End Mobile Tasks*/

    /*Download Report Tasks*/
    grunt.registerTask('download_report_app', ['concat:report_app_files', 'timestamp']);
    grunt.registerTask('download_report_htmls', ['html2js:download_report', 'timestamp']);
    grunt.registerTask('download_report_load', ['download_report_app', 'download_report_htmls']);
    /*End Download Report Tasks*/

    // Print a timestamp (useful for when watching)
    grunt.registerTask('timestamp', function() {
        grunt.log.subhead(Date());
    });

    // var karmaConfig = function(configFile, customOptions) {
    //     var options = { configFile: configFile, keepalive: true };
    //     var travisOptions = process.env.TRAVIS && { browsers: ['Firefox'], reporters: 'dots' };
    //     return grunt.util._.extend(options, customOptions, travisOptions);
    // };

    // Project configuration.
    grunt.initConfig({
        distdir: '../public/backend',
        mobile_distdir: '../public/mobile',
        home_distdir: '../public',
        pkg: grunt.file.readJSON('package.json'),
        banner:
        '/* <%= pkg.title || pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '<%= pkg.homepage ? " * " + pkg.homepage + "\\n" : "" %>' +
        ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;\n' +
        ' * Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n */\n',

        js_php_banner : '<?php header("Cache-Control: public, s-maxage=604899999900 max-age=604899999900"); header("Expires: Sun, 25-Jun-2030 19:14:07 GMT"); ' +
        '$etag = \'"\' .  md5(<%= new Date().getTime() %>) . \'"\';$etag_header =\'Etag: \' . $etag;header($etag_header);if (isset($_SERVER["HTTP_IF_NONE_MATCH"]) and $_SERVER["HTTP_IF_NONE_MATCH"]==$etag) {header("HTTP/1.1 304 Not Modified");exit();}header("Content-Type: text/javascript");header("last-modified: Sun, 25-Jun-2000 19:14:07 GMT");if (substr_count($_SERVER["HTTP_ACCEPT_ENCODING"], "gzip")) ' +
        'ob_start("ob_gzhandler"); else ob_start();?>',

        css_php_banner : '<?php header("Cache-Control: public, s-maxage=604899999900 max-age=604899999900"); header("Expires: Sun, 25-Jun-2030 19:14:07 GMT"); ' +
        '$etag = \'"\' .  md5(<%= new Date().getTime() %>) . \'"\';$etag_header =\'Etag: \' . $etag;header($etag_header);if (isset($_SERVER["HTTP_IF_NONE_MATCH"]) and $_SERVER["HTTP_IF_NONE_MATCH"]==$etag) {header("HTTP/1.1 304 Not Modified");exit();}header("Content-Type: text/css");header("last-modified: Sun, 25-Jun-2000 19:14:07 GMT");if (substr_count($_SERVER["HTTP_ACCEPT_ENCODING"], "gzip")) ' +
        'ob_start("ob_gzhandler"); else ob_start();?>',
                                                                                                                                                        
        src: {
            html: [ 'src/dashboard.html' ],
            css: [ 'stylesheets/**/*.css', 'vendors/**/*.css' ],
            assets : ['stylesheets/fonts', 'stylesheets/images' ],
            app_files : ['src/schoolApp.js', 'src/routes.js', 'src/**/*.js'],
            tpl: {
                app: [ 'partials/**/*.html' ],
                mobile_app: [ 'mobileNgMaterial/partials/**/*.tpl.html' ],
                download_report: [ 'downloadSchoolReport/partials/**/*.html' ]
            },
            specs: [ 'test/**/*.spec.js' ],
            scenarios: [ 'test/**/*.scenario.js' ],
            jquery:  'vendors/jquery/*.js',
            angular: 'vendors/angular/*.js',
            large_scripts: ['vendors/amCharts/**/*.js', 'vendors/amMaps/**/*.js', 'vendors/pdfMake/**/*.js', 'vendors/tableExport/**/*.js'],
            scripts: ['vendors/underscore/*.js', 'vendors/**/*.js', '!vendors/amCharts/**/*.js', '!vendors/amMaps/**/*.js', '!vendors/pdfMake/**/*.js', '!vendors/tableExport/**/*.js' ],


            mobile_html: ['mobileNgMaterial/html/mobile.html'],
            mobile_vendors: ['mobileNgMaterial/vendors/angular/angular.min.js','mobileNgMaterial/vendors/**/*.js'],
            mobile_css: ['mobileNgMaterial/vendors/**/*.css'],
            mobile_app_files : ['mobileNgMaterial/src/**/*.js'],

            report_app_files : ['downloadSchoolReport/**/*.js']
        },
        destinations : {
            php_views : ['../app/views']
        },
        clean: {
            options  : { force : true },
            src:  ['<%= distdir %>*']
        },
        copy: {
            assets: {

                files: [{ dest: '<%= distdir %>', src: ['scripts/js/plugins/amMaps/images/**', 'scripts/js/plugins/pdfMake/msrc_logo.png'], expand: true }]
            }
        },
        // karma: {
        //     unit: { options: karmaConfig('test/config/unit.js') },
        //     watch: { options: karmaConfig('test/config/unit.js', { singleRun:false, autoWatch: true}) }
        // },
        html2js: {
            app: {
                options: {
                    fileHeaderString: '<%= banner %>',
                    singleModule: true,
                    useStrict: true,
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: true,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: true
                    },
                    base: '../ng-backend-src'
                },
                src: ['<%= src.tpl.app %>'],
                dest: '<%= distdir %>/templates.js',
                module: 'templates.app'
            },
            mobile_app: {
                options: {
                    base: 'mobileNgMaterial/'
                },
                src: ['<%= src.tpl.mobile_app %>'],
                dest: '<%= mobile_distdir %>/templates/app.js',
                module: 'templates.app'
            },
            download_report: {
                options: {
                    base: 'downloadSchoolReport/'
                },
                src: ['<%= src.tpl.download_report %>'],
                dest: '<%= mobile_distdir %>/templates/report_app.js',
                module: 'report_templates.app'
            }
        },
        concat:{
            index: {
                src: ['src/dashboard.html'],
                dest: '<%= destinations.php_views %>/dashboard.php',
                options: {
                    force : true,
                    process: true
                }
            },
            stylesheets : {
                options: {
                    banner: "<%= banner %>"
                },
                files : {
                    '<%= distdir  %>/<%= pkg.name_lowercase %>.css' : '<%= src.css %>'
                }
            },
            app_files : {
                options: {
                    banner: "<%= banner %>"
                },
                src:['<%= src.app_files %>'],
                dest: '<%= distdir %>/<%= pkg.name_lowercase %>.js'
            },
            jquery: {
                options: {
                    banner: "<%= banner %>"
                },
                src:['<%= src.jquery %>'],
                dest: '<%= distdir %>/jquery.js'
            },
            angular: {
                options: {
                    banner: "<%= banner %>"
                },
                src:['<%= src.angular %>'],
                dest: '<%= distdir %>/angular.js'
            },
            large_scripts: {
                options: {
                    //banner: "<%= banner %>"
                },
                src:['<%= src.large_scripts %>'],
                dest: '<%= distdir %>/large_scripts.js'
            },
            scripts: {
                options: {
                    banner: "<%= banner %>"
                },
                src:['<%= src.scripts %>'],
                dest: '<%= distdir %>/scripts.js'
            },
            mobile_index: {
                src : '<%= src.mobile_html %>',
                dest: '<%= destinations.php_views %>/mobile_responsive.php',
                options: {
                    force : true,
                    process: true
                }
            },
            mobile_stylesheets : {
                options: {
                    banner: "<%= banner %>"
                },
                files : {
                    '<%= mobile_distdir %>/<%= pkg.name_lowercase %>.css' : '<%= src.mobile_css %>'
                }
            },
            mobile_scripts: {
                options: {
                    banner: "<%= banner %>"
                },
                src: '<%= src.mobile_vendors %>',
                dest: '<%= mobile_distdir %>/<%= pkg.name_lowercase %>-scripts.js'
            },
            mobile_app_files : {
                options: {
                    banner: "<%= banner %>"
                },
                src:['mobileNgMaterial/src/mobileApp.js', '<%= src.mobile_app_files %>'],
                dest: '<%= mobile_distdir %>/ng-<%= pkg.name_lowercase %>.js'
            },
            report_app_files : {
                options: {
                    banner: "<%= banner %>"
                },
                src:['downloadSchoolReport/reportApp.js', '<%= src.report_app_files %>'],
                dest: '<%= mobile_distdir %>/down_report_<%= pkg.name_lowercase %>.js'
            }
        },
        uglify: {
            options: {
                banner: "<%= banner %>"
            },
            app_files: {
                src:['<%= distdir %>/scripts.js', '<%= distdir %>/<%= pkg.name_lowercase %>.js', '<%= distdir %>/templates.js' ],
                dest: '<%= distdir %>/ng-<%= pkg.name_lowercase %>.min.js'
            },
            scripts: {
                src:['<%= distdir %>/scripts.js'],
                dest: '<%= distdir %>/scripts.min.js'
            },
            templates : {
                src:['<%= distdir %>/templates.js'],
                dest: '<%= distdir %>/templates.js'
            },
            large_scripts: {
                src:['<%= distdir %>/large_scripts.js'],
                dest: '<%= distdir %>/large_scripts.js'
            }
        },
        watch:{
            all: {
                files:['<%= src.js %>', '<%= src.specs %>', '<%= src.lessWatch %>', '<%= src.tpl.app %>', '<%= src.tpl.common %>', '<%= src.html %>'],
                tasks:['default','timestamp']
            },
            build: {
                files:['<%= src.js %>', '<%= src.specs %>', '<%= src.lessWatch %>', '<%= src.tpl.app %>', '<%= src.tpl.common %>', '<%= src.html %>'],
                tasks:['build','timestamp']
            }
        },
        jshint:{
            files:['gruntFile.js', '<%= src.js %>', '<%= src.jsTpl %>', '<%= src.specs %>', '<%= src.scenarios %>'],
            options:{
                curly:true,
                eqeqeq:true,
                immed:true,
                latedef:true,
                newcap:true,
                noarg:true,
                sub:true,
                boss:true,
                eqnull:true,
                globals:{}
            }
        }
    });

};
