/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobMeetingsController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;

            $scope.$on('meetingsLoaded', function (evt, meetingData) {

                $scope.school = meetingData.school;

                $scope.updateLoadLevel(15);

                $scope.meetingData = [];
                if (meetingData.code === 200 && meetingData.data.length > 0) {

                    var meetingObject = meetingData.data[0];

                    var meetingsHolder = [];

//                number_of_circuit_supervisor_visit
//                number_of_inset

                    meetingsHolder.push({
                        name: "Staff Meeting",
                        frequency: meetingObject.number_of_staff_meeting,
                        males_present: meetingObject.num_males_present_at_staff_meeting,
                        females_present: meetingObject.num_females_present_at_staff_meeting,
                        comment : meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "SPAM",
                        frequency: meetingObject.number_of_spam_meeting,
                        males_present: meetingObject.num_males_present_at_spam_meeting,
                        females_present: meetingObject.num_females_present_at_spam_meeting,
                        comment : meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "PTA",
                        frequency: meetingObject.number_of_pta_meeting,
                        males_present: meetingObject.num_males_present_at_pta_meeting,
                        females_present: meetingObject.num_females_present_at_pta_meeting,
                        comment : meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "SMC",
                        frequency: meetingObject.number_of_smc_meeting,
                        males_present: meetingObject.num_males_present_at_smc_meeting,
                        females_present: meetingObject.num_females_present_at_smc_meeting,
                        comment : meetingObject.comment
                    });
                    $scope.meetingData = $scope.meetingData.concat(meetingsHolder);

                    $scope.submittedDate = meetingData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
