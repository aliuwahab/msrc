/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSupportController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);
            $scope.schoool =

                $scope.$on('supportLoaded', function (evt, supportData) {
                    $scope.school = supportData.school;

                    $scope.updateLoadLevel(15);

                    $scope.supportData = [];
                    if (supportData.code === 200 && supportData.data.length) {
                        var item = supportData.data[0];

                        var supportTypeHolder =[];
//                var format_term = $scope.academicTermDisplay(item.term);
                        supportTypeHolder.push({
                            name: "Donor Support",
                            in_cash : item.donor_support_in_cash,
                            in_kind : item.donor_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "Community Support",
                            in_cash : item.community_support_in_cash,
                            in_kind : item.community_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "District Support",
                            in_cash : item.district_support_in_cash,
                            in_kind : item.district_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "PTA Support",
                            in_cash : item.pta_support_in_cash,
                            in_kind : item.pta_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "Other",
                            in_cash : item.other_support_in_cash,
                            in_kind : item.other_support_in_kind,
                            comment : item.comment
                        });
                        $scope.supportData = $scope.supportData.concat(supportTypeHolder);

                        $scope.submittedDate = supportData.data[0].updated_at;
                        $scope.updateLoadLevel(100);

                    }else{
                        $scope.completed = true;
                    }
                });


            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;
            };

            $scope.doSecondaryAction = function(event) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Secondary Action')
                        .content('Secondary actions can be used for one click actions')
                        .ariaLabel('Secondary click demo')
                        .ok('Neat!')
                        .targetEvent(event)
                );
            };

        }]);
