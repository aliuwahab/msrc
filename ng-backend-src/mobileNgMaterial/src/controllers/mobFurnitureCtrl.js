/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobFurnitureController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('furnitureLoaded', function (evt, furnitureData) {

                $scope.school = furnitureData.school;

                $scope.updateLoadLevel(15);

                $scope.furnitureData = [];
                if (furnitureData.code === 200 && furnitureData.data.length > 0) {

                    var furnitureObject = furnitureData.data[0];

                    var furnitureHolder =[];

                    furnitureHolder.push({
                        name: "Pupils Furniture",
                        status : $scope.formatAdequecy[furnitureObject.pupils_furniture],
                        comment : furnitureObject.comment
                    });
                    furnitureHolder.push({
                        name: "Teacher Tables",
                        status : $scope.formatAdequecy[furnitureObject.teacher_tables],
                        comment : furnitureObject.comment
                    });
                    furnitureHolder.push({
                        name: "Teacher Chairs",
                        status : $scope.formatAdequecy[furnitureObject.teacher_chairs],
                        comment : furnitureObject.comment
                    });
                    furnitureHolder.push({
                        name: "Classrooms Cupboard",
                        status : $scope.formatAdequecy[furnitureObject.classrooms_cupboard],
                        comment : furnitureObject.comment
                    });

                    $scope.furnitureData = $scope.furnitureData.concat(furnitureHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = furnitureData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);