/**
 * Created by Kaygee on 14/05/2015.
 */

mobileMSRC.controller('mobHomeController', ['$scope','$mdDialog', function($scope, $mdDialog){

    $scope.doSecondaryAction = function(event) {
        $mdDialog.show(
            $mdDialog.alert()
                .title('Secondary Action')
                .content('Secondary actions can be used for one click actions')
                .ariaLabel('Secondary click demo')
                .ok('Neat!')
                .targetEvent(event)
        );
    };

}]);

