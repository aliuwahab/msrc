/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobPupilPerformanceController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('pupil_performanceLoaded', function (evt, pupilPerformanceData) {
                /*
                *

                average_english_score
                :
                "59"
                average_ghanaian_language_score
                :
                "65"
                average_maths_score
                :
                "71"
                circuit_id
                :
                13
                comment
                :
                ""
                country_id
                :
                1
                created_at
                :
                "2015-06-03 12:58:24"
                data_collector_id
                :
                81
                data_collector_type
                :
                "head_teacher"
                district_id
                :
                597
                id
                :
                1833
                lat
                :
                "0"
                level
                :
                "KG1"
                long
                :
                "0"
                num_pupil_who_score_above_average_in_english
                :
                40
                num_pupil_who_score_above_average_in_ghanaian_language
                :
                45
                num_pupil_who_score_above_average_in_maths
                :
                50
                questions_category_reference_code
                :
                "PEPA"
                region_id
                :
                9
                school_id
                :
                49
                school_reference_code
                :
                "8L5b60CvggIKM7nv"
                term
                :
                "second_term"
                updated_at
                :
                "2015-06-03 12:58:24"
                year
                :
                "2014/2015"
                * */

                $scope.school = pupilPerformanceData.school;

                $scope.updateLoadLevel(15);

                $scope.pupilPerformanceData = [];
                if (pupilPerformanceData.code === 200 && pupilPerformanceData.data.length) {
                    $scope.pupilPerformanceData = pupilPerformanceData.data;
                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };
                    $scope.submittedDate = pupilPerformanceData.data[0].updated_at;
                    $scope.updateLoadLevel(100);

                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };


        }]);
