/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSecurityController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('securityLoaded', function (evt, securityData) {

                $scope.securityData = [];
                if (securityData.code === 200 && securityData.data.length > 0) {

                    var securityObject = securityData.data[0];
                    var securityHolder =[];

                    securityHolder.push({
                        name: "Walled / Fenced",
                        status : $scope.formatAvailability[securityObject.walled],
                        comment : securityObject.comment
                    });

                    securityHolder.push({
                        name: "Gated",
                        status : $scope.formatAvailability[securityObject.gated],
                        comment : securityObject.comment
                    });
                    securityHolder.push({
                        name: "Lights",
                        status : $scope.formatAvailability[securityObject.lights],
                        comment : securityObject.comment
                    });
                    securityHolder.push({
                        name: "Security Man",
                        status : $scope.formatAvailability[securityObject.security_man],
                        comment : securityObject.comment
                    });


                    $scope.securityData = $scope.securityData.concat(securityHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = securityData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
