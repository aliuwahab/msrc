/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobTeacherAttendanceController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.week_number = $location.search().week_number;
            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);
            $scope.schoool =

            $scope.$on('teacher_attendanceLoaded', function (evt, teacherData) {
                $scope.teacherAttendanceData = [];
                $scope.school_in_session = 'Unavailable';
                if (teacherData.code == '200') {
                    for(var i=0; i < teacherData.data.length; i++){
                        if ( teacherData.data[i].puntuality_and_lesson_plans.length > 0) {//if it has a punc & lessons array
                            //get the days school was in session that week
                            $scope.school_in_session = teacherData.data[i].puntuality_and_lesson_plans[0].days_in_session;
                            $scope.school_in_session = $scope.school_in_session != 1 ? $scope.school_in_session + ' days' : $scope.school_in_session + ' day' ;
                            //overwrite the property with the array content
                            teacherData.data[i].puntuality_and_lesson_plans =  teacherData.data[i].puntuality_and_lesson_plans[0];
                        }
                        $scope.teacherAttendanceData.push(teacherData.data[i])
                    }
                    if ($scope.loadLevel < 90) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.$on('class_managementLoaded', function (evt, classMangmnt) {
                $scope.classManagementData = [];
                if (classMangmnt.code == '200') {
                    for(var i=0; i < classMangmnt.data.length; i++){
                        if ( classMangmnt.data[i].classroom_management.length > 0) {//if it has a classroom management array
                            //overwrite the property with the array content
                            classMangmnt.data[i].classroom_management =  classMangmnt.data[i].classroom_management[0];
                        }
                        $scope.classManagementData.push(classMangmnt.data[i])
                    }
                    if ($scope.loadLevel < 100) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.loadLevel = 15;
            $timeout(function () {
                if (!$scope.teacherAttendanceData  && !$scope.classManagementData) {
                    $scope.loadLevel += 10;
                }
            },500, 8);

            $scope.completed = false;
            $scope.$watch('loadLevel', function (newValue, oldValue) {
                if (parseInt(newValue) == 100) $scope.completed = true
            });

            $scope.showClassManagement = false;

            $scope.doSecondaryAction = function(event) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Secondary Action')
                        .content('Secondary actions can be used for one click actions')
                        .ariaLabel('Secondary click demo')
                        .ok('Neat!')
                        .targetEvent(event)
                );
            };

            $scope.displayMoreInfo = function (teacherObj, event) {
                var teacherName = teacherObj.first_name + ' ' + teacherObj.last_name;



                $mdDialog.show({
                    parent: angular.element(document.body),
                    targetEvent: event,
                    templateUrl: 'partials/teacher_dialog_list.tpl.html',
                    locals: {
                        teacherObj: teacherObj
                    },
                    controller: DialogController
                });

                function DialogController(scope, $mdDialog, teacherObj) {
                    scope.teacherObj = teacherObj;
                    scope.closeDialog = function () {
                        $mdDialog.hide();
                    }
                }
                //$mdDialog.alert()
                //    .title('Information of ' + teacherObj.first_name + ' ' + teacherObj.last_name)
                //    .content('Secondary actions can be used for one click actions')
                //    .ariaLabel('Secondary click demo')
                //    .ok('Neat!')
                //    .targetEvent(event)
            }
        }]);
