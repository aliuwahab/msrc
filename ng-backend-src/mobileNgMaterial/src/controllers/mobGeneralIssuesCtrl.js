/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobGeneralIssuesController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;
            $scope.$on('general_issuesLoaded', function (evt, generalIssuesData) {
                console.log('general_issues : ', generalIssuesData);


                $scope.school = generalIssuesData.school;

                $scope.updateLoadLevel(15);

                $scope.generalIssuesData = [];
                if (generalIssuesData.code === 200 && generalIssuesData.data.length > 0) {

                    var allSchoolGeneralSituationData = generalIssuesData.data;

                    $scope.generalIssueItems = [];/*This is used to track what to show on the html*/

                    $scope.generalIssueItemsObject = {
                        'Inclusive' : [],
                        'Effective Teaching and Learning' : [],
                        'Healthy' : [],
                        'Safe and Protective' : [],
                        'Friendliness to Boys and Girls' : [],
                        'Community Involvement' : []
                    };

                    $scope.generalIssueItemsObject['Effective Teaching and Learning'].push(allSchoolGeneralSituationData[0]);
                    $scope.generalIssueItemsObject['Healthy'].push(allSchoolGeneralSituationData[1]);
                    $scope.generalIssueItemsObject['Effective Teaching and Learning'].push(allSchoolGeneralSituationData[2]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[3]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[4]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[5]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[6]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[7]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[8]);
                    $scope.generalIssueItemsObject['Inclusive'].push(allSchoolGeneralSituationData[9]);
                    $scope.generalIssueItemsObject['Inclusive'].push(allSchoolGeneralSituationData[10]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[11]);
                    $scope.generalIssueItemsObject['Healthy'].push(allSchoolGeneralSituationData[12]);
                    $scope.generalIssueItemsObject['Safe and Protective'].push(allSchoolGeneralSituationData[13]);


                    angular.forEach(generalIssuesData.data, function(issueObject, index) {

                        /*This is used to track what to show on the html*/
                        $scope.generalIssueItems.push(issueObject);


                    });

                    $scope.submittedDate = generalIssuesData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
