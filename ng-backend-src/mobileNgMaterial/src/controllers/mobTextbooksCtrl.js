/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobTextBooksController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);


            $scope.completed = false;
            $scope.loadLevel = 15;


            $scope.$on('textbookLoaded', function (evt, textbookData) {

                $scope.school = textbookData.school;

                $scope.updateLoadLevel(15);
                /*
                *
                circuit_id:13
                country_id:1
                created_at:"2015-06-03 12:47:10"
                data_collector_id:81
                data_collector_type:"head_teacher"
                district_id:597
                id:1876
                lat:"0"
                level:"KG1"
                long:"0"
                number_of_english_textbooks:"0"
                number_of_ghanaian_language_textbooks:"0"
                number_of_maths_textbooks:"0"
                questions_category_reference_code:"CTB"
                region_id:9
                school_id:49
                school_reference_code:"8L5b60CvggIKM7nv"
                term:"second_term"
                updated_at:"2015-06-03 12:47:10"
                week_number:0
                year:"2014/2015"
                * */
                $scope.textbookData = [];
                if (textbookData.code === 200 && textbookData.data.length > 0) {

                    $scope.textbookData = textbookData.data;

                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = textbookData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
