/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobAttendanceController',
    ['$scope','mobDataService','$stateParams','$location', '$timeout',
        function($scope, mobDataService, $stateParams, $location, $timeout){

            $scope.week_number = $location.search().week_number;
            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('normal_attendanceLoaded', function (evt, data) {
                if (data.code == '200') {
                    $scope.normalAttendanceData = data.data;
                    if (data.data.length > 0 && !$scope.school_in_session) {
                        $scope.school_in_session = data.data[0].school_in_session
                    }
                    if ($scope.loadLevel < 100) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.$on('special_attendanceLoaded', function (evt, data) {
                if (data.code == '200') {
                    $scope.specialAttendanceData = data.data;
                    $scope.selected_school = data.school;
                    if (data.data.length > 0 && !$scope.school_in_session) {
                        $scope.school_in_session = data.data[0].school_in_session
                    }
                    if ($scope.loadLevel < 100) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.getTotal = function (fig1, fig2) {
                return parseInt(fig1) +  parseInt(fig2)

            };

            $scope.loadLevel = 25;
            $timeout(function () {
                if (!$scope.normalAttendanceData  && !$scope.specialAttendanceData) {
                    $scope.loadLevel += 10;
                }
            },500, 8);

            $scope.completed = false;
            $scope.$watch('loadLevel', function (newValue, oldValue) {
                if (parseInt(newValue) == 100) $scope.completed = true
            });



            $scope.showSpecialNeed = false;



        }]);
