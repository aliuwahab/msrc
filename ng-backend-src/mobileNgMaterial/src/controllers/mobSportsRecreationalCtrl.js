/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSportsRecreationalController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('sports_recreationalLoaded', function (evt, sportsRecreationData) {

                $scope.sportsRecreationData = [];
                if (sportsRecreationData.code === 200 && sportsRecreationData.data.length > 0) {

                    $scope.school = sportsRecreationData.school;


                    var recreationObject = sportsRecreationData.data[0];
                    var recreationHolder =[];
                    recreationHolder.push({
                        name: "Football",
                        status : $scope.formatAvailability[recreationObject.football],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Volleyball",
                        status : $scope.formatAvailability[recreationObject.volleyball],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Netball",
                        status : $scope.formatAvailability[recreationObject.netball],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Playing Field",
                        status : $scope.formatAvailability[recreationObject.playing_field],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Sports Wear",
                        status : $scope.formatAvailability[recreationObject.sports_wear],
                        comment : recreationObject.comment
                    });
                    try{
                        recreationHolder.push({
                            name: "Seesaw",
                            status : $scope.formatAvailability[recreationObject.seesaw],
                            comment : recreationObject.comment
                        })
                    }catch(e){

                    }

                    try{
                        recreationHolder.push({
                            name: "Merry-Go-Round",
                            status : $scope.formatAvailability[recreationObject.merry_go_round],
                            comment : recreationObject.comment
                        });
                    }catch(e){

                    }


                    recreationHolder.push({
                        name: "First Aid Box",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.first_aid_box],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });



                    $scope.sportsRecreationData = $scope.sportsRecreationData.concat(recreationHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = sportsRecreationData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
