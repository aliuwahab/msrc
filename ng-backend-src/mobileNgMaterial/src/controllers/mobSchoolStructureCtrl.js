/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSchoolStructureController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);
            $scope.schoool =

            $scope.$on('school_structureLoaded', function (evt, structureData) {
                $scope.school = structureData.school;

                $scope.updateLoadLevel(15);

                $scope.structureData = [];
                if (structureData.code === 200 && structureData.data.length > 0) {

                    var structureObject = structureData.data[0];

                    var structureHolder =[];

                    structureHolder.push({
                        name: "Walls",
                        status : $scope.formatGoodPoorFair[structureObject.walls],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Doors",
                        status : $scope.formatGoodPoorFair[structureObject.doors],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Windows",
                        status : $scope.formatGoodPoorFair[structureObject.windows],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Floors",
                        status : $scope.formatGoodPoorFair[structureObject.floors],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Blackboard",
                        status : $scope.formatGoodPoorFair[structureObject.blackboard],
                        comment : structureObject.comment
                    });
                    try{
                        structureHolder.push({
                            name: "Illumination",
                            status : $scope.formatGoodPoorFair[structureObject.illumination],
                            comment : structureObject.comment
                        });
                    }catch(e){
                        console.log(e);
                        structureHolder.push({
                            name: "Illumination",
                            status : ' ',
                            comment : structureObject.comment
                        });
                    }

                    $scope.structureData = $scope.structureData.concat(structureHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = structureData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);