/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobGrantController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('grantLoaded', function (evt, grantData) {
                $scope.school = grantData.school;

                $scope.updateLoadLevel(15);

                $scope.grantData = [];
                if (grantData.code === 200 && grantData.data.length) {
                    $scope.grantData = grantData.data;

                    $scope.submittedDate = grantData.data[0].updated_at;
                    $scope.updateLoadLevel(100);

                }
                if (!grantData.data.length) {
                    $scope.completed = true;
                }
            });

            $scope.formatGrant = {
                school_grant : 'School Grant',
                capitation : 'Capitation'
            };

            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            $scope.doSecondaryAction = function(event) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Secondary Action')
                        .content('Secondary actions can be used for one click actions')
                        .ariaLabel('Secondary click demo')
                        .ok('Neat!')
                        .targetEvent(event)
                );
            };

        }]);
