/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobCommunityInvolvementController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;

            $scope.$on('community_involvementLoaded', function (evt, commInvolveData) {

                $scope.school = commInvolveData.school;

                $scope.updateLoadLevel(15);

                $scope.commInvolveData = [];
                if (commInvolveData.code === 200 && commInvolveData.data.length > 0) {

                    var communityObject = commInvolveData.data[0];

                    var communityHolder = [];

                    communityHolder.push({
                        name: "PTA Involvement",
                        status: $scope.formatGoodPoorFair[communityObject.pta_involvement]
                    });
                    communityHolder.push({
                        name: "Parents Notified of Student Progress",
                        status: $scope.formatGoodPoorFair[communityObject.parents_notified_of_student_progress]
                    });
                    communityHolder.push({
                        name: "Community Sensitization on School Attendance",
                        status: $scope.formatGoodPoorFair[communityObject.community_sensitization_for_school_attendance]
                    });
                    communityHolder.push({
                        name: "School Management Committees",
                        status: $scope.formatGoodPoorFair[communityObject.school_management_committees]
                    });

                    $scope.commInvolveData = $scope.commInvolveData.concat(communityHolder);

                    $scope.submittedDate = commInvolveData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
