/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobRecordBooksController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('record_booksLoaded', function (evt, recordBooksData) {
                $scope.updateLoadLevel(15);

                if (recordBooksData.school) {
                    $scope.school = recordBooksData.school;
                }

                if (recordBooksData.code === 200 && recordBooksData.data.length) {

                    var item = recordBooksData.data[0];
                    var booksHolder = [];

                    $scope.recordBooksData = [];

                    booksHolder.push({
                        name: "Admission Register",
                        status: item.admission_register
                    });
                    booksHolder.push({
                        name: "Class Registers",
                        status: item.class_registers
                    });
                    booksHolder.push({
                        name: "Teacher Attendance Register",
                        status: item.teacher_attendance_register
                    });
                    booksHolder.push({
                        name: "Visitors\' Book",
                        status: item.visitors
                    });
                    booksHolder.push({
                        name: "Log Book",
                        status: item.log
                    });
                    $scope.updateLoadLevel(35);

                    booksHolder.push({
                        name: "SBA",
                        status: item.sba
                    });
                    booksHolder.push({
                        name: "Movement Book",
                        status: item.movement
                    });
                    booksHolder.push({
                        name: "SPIP",
                        status: item.spip
                    });
                    booksHolder.push({
                        name: "Inventory Books",
                        status: item.inventory_books
                    });
                    booksHolder.push({
                        name: "Cumulative Record Books",
                        status: item.cummulative_records_books
                    });

                    booksHolder.push({
                        name: "Continuous Assessment / SMB",
                        status: item.continous_assessment
                    });


                    $scope.recordBooksData = $scope.recordBooksData.concat(booksHolder);


                    $scope.submittedDate = item.updated_at;


                    $scope.updateLoadLevel(100);
                }else{
                    $scope.completed = true;
                }
            });


            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };


        }]);
