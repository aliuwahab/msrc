/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSanitationController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;
            $scope.$on('sanitationLoaded', function (evt, sanitationData) {
                
                $scope.school = sanitationData.school;

                $scope.updateLoadLevel(15);

                $scope.sanitationData = [];
                if (sanitationData.code === 200 && sanitationData.data.length > 0) {

                        var sanitationObject = sanitationData.data[0];


                    //An empty object that gets initialized every time the loop runs.
                    //It is used to hold the items in object being looped over for the purpose of display
                    var sanitationHolder = [];
                    $scope.sanitationData = [];

                    //reformatting the way the objects are coming from the server tp be used in the table for display
                    sanitationHolder.push({
                        name: "Toilet",
                        status : $scope.formatAvailability[sanitationObject.toilet],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Urinal",
                        status : $scope.formatAvailability[sanitationObject.urinal],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Water",
                        status : $scope.formatAvailability[sanitationObject.water],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Dust Bins",
                        status : $scope.formatAvailability[sanitationObject.dust_bins],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Veronica Buckets",
                        status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
                        comment : sanitationObject.comment
                    });

                    $scope.sanitationData = $scope.sanitationData.concat(sanitationHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = sanitationData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);