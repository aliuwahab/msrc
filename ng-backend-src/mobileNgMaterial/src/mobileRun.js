/**
 * Created by Kaygee on 14/05/2015.
 */

mobileMSRC.run( ['$rootScope', '$location', '$state', '$stateParams', '$http', '$templateCache',
    function ($rootScope, $location, $state, $stateParams, $http, $templateCache) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        //Helper function to know the chosen term in a dropdown and associate the server format to readable format
        $rootScope.academicTermDisplay = function(term){
            return term === 'first_term' ? '1st Term' :
                term === 'second_term' ? '2nd Term' :
                    term === 'third_term' ? '3rd Term' : 'NA'
        };

        $rootScope.formatDataCollectorType = function(collector) {
            return collector === 'head_teacher' ? 'Head Teacher' :
                collector === 'circuit_supervisor' ? 'Circuit Supervisor' : 'NA';
        };

        //This is a associative object to format the lowercase and underscores to readable words
        $rootScope.formatAvailability = {
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };

        $rootScope.formatGoodPoorFair = {
            good : 'Good',
            fair : 'Fair',
            poor : 'Poor',
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };

        //This is a associative object to format the lowercase to sentence case words
        $rootScope.formatAdequecy = {
            'adequate' : 'Adequate',
            'not adequate' : 'Inadequate',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable',
            'not available' : 'Not Available',
            '' : 'Not Available'
        };

    }]);
