/**
 * Created by Kaygee on 14/05/2015.
 */

mobileMSRC.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    //The redirect from the server after login has no trailing slash, hence the redirect send the route to the error page.
    // urlRouter uses the 'when' function to redirect to the main dashboard
    $urlRouterProvider.when('', '/');

    //This happens when a url does not exist.
    $urlRouterProvider.otherwise(function ($inj, $loc) {
        console.log('inj',$inj);
        console.log('loc',$loc);
    });

    $stateProvider
        .state('home', {
            url : '/',
            controller: 'mobHomeController',
            templateUrl: 'partials/home.tpl.html',
            page_name : 'Home'
        })
        .state('home.enrollment_state', {
            //url : 'enrollment
            // http://localhost:81/mobileview#/enrollment?school_id=49&week_number=10&term=first_term&year=2015%2F2016&data_collector_type=head_teacher
            url : 'enrollment',
            controller: 'mobEnrollmentController',
            templateUrl: 'partials/enrollment.tpl.html',
            page_name : 'Enrollment',
            resolve: {
                normalEnrollmentData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchData(
                        'normal_enrollment',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}],
                specialEnrollmentData : ['mobDataService', '$location', function(mobDataService, $location){
                    mobDataService.fetchData(
                        'special_enrollment',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.attendance_state', {
            // http://localhost:81/mobileview#/attendance?school_id=49&week_number=10&term=first_term&year=2015%2F2016&data_collector_type=head_teacher
            url : 'attendance',
            controller: 'mobAttendanceController',
            templateUrl: 'partials/attendance.tpl.html',
            page_name : 'Attendance',
            resolve: {
                normalAttendanceData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchData(
                        'normal_attendance',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}],
                specialAttendanceData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchData(
                        'special_attendance',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.teacher_attendance_state', {
            //url : 'teacher_attendance/:school_id/:week_number/:term/:year/:data_collector_type',
            url : 'teacher_attendance',
            controller: 'mobTeacherAttendanceController',
            templateUrl: 'partials/teacher_attendance.tpl.html',
            page_name : 'Teacher Attendance',
            resolve: {
                teacherAttendanceData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTeacherData(
                        'teacher_attendance',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}],

                classManagementData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTeacherData(
                        'class_management',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.textbooks_state', {
            //url : 'textbooks/:school_id/:term/:year/:data_collector_type',
            url : 'textbooks',
            controller: 'mobTextBooksController',
            templateUrl: 'partials/text_books.tpl.html',
            page_name : 'Text Books',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'textbook',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.pupil_performance', {
            url : 'pupil_performance',
            controller: 'mobPupilPerformanceController',
            templateUrl: 'partials/pupil_performance.tpl.html',
            page_name : 'Pupil Performance',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'pupil_performance',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.record_books', {
            url : 'record_books',
            controller: 'mobRecordBooksController',
            templateUrl: 'partials/record_books.tpl.html',
            page_name : 'Record Books',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'record_books',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.support', {
            url : 'support',
            controller: 'mobSupportController',
            templateUrl: 'partials/support.tpl.html',
            page_name : 'Support',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'support',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.grant', {
            url : 'grant',
            controller: 'mobGrantController',
            templateUrl: 'partials/grant.tpl.html',
            page_name : 'Grant',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'grant',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.sanitation', {
            url : 'sanitation',
            controller: 'mobSanitationController',
            templateUrl: 'partials/sanitation.tpl.html',
            page_name : 'Sanitation',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'sanitation',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.security', {
            url : 'security',
            controller: 'mobSecurityController',
            templateUrl: 'partials/security.tpl.html',
            page_name : 'Security',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'security',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.sports_recreational', {
            url : 'sports_recreational',
            controller: 'mobSportsRecreationalController',
            templateUrl: 'partials/sports_recreational.tpl.html',
            page_name : 'Sports & Recreational',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'sports_recreational',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.school_structure', {
            url : 'school_structure',
            controller: 'mobSchoolStructureController',
            templateUrl: 'partials/school_structure.tpl.html',
            page_name : 'School Structure',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'school_structure',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
       .state('home.furniture', {
            url : 'furniture',
            controller: 'mobFurnitureController',
            templateUrl: 'partials/furniture.tpl.html',
            page_name : 'Furniture',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'furniture',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.community_involvement', {
            url : 'community_involvement',
            controller: 'mobCommunityInvolvementController',
            templateUrl: 'partials/community_involvement.tpl.html',
            page_name : 'Community Involvement',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'community_involvement',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.meetings', {
            url : 'meetings',
            controller: 'mobMeetingsController',
            templateUrl: 'partials/meetings.tpl.html',
            page_name : 'Meetings',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'meetings',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.general_issues', {
            url : 'general_issues',
            controller: 'mobGeneralIssuesController',
            templateUrl: 'partials/general_issues.tpl.html',
            page_name : 'General Issues',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'general_issues',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('error_state', {
            url : 'error',
            controller: 'mobErrorController',
            templateUrl: 'partials/error.tpl.html',
            page_name : 'Error'
        })
}]);

/*
* textbooks/:school_id/:term/:year/:data_collector_type
* pupil_performance/:school_id/:term/:year/:data_collector_type
* record_books/:school_id/:term/:year/:data_collector_type
* support/:school_id/:term/:year/:data_collector_type
* grant/:school_id/:term/:year/:data_collector_type
* sanitation/:school_id/:term/:year/:data_collector_type
* security/:school_id/:term/:year/:data_collector_type
* sports_recreational/:school_id/:term/:year/:data_collector_type
* school_structure/:school_id/:term/:year/:data_collector_type
* furniture/:school_id/:term/:year/:data_collector_type
* community_involvement/:school_id/:term/:year/:data_collector_type
* meetings/:school_id/:term/:year/:data_collector_type
* general_issues/:school_id/:term/:year/:data_collector_type
* school_visit/:school_id/:term/:year/:data_collector_type
* school_review/:school_id/:term/:year/:data_collector_type
*
*
* */