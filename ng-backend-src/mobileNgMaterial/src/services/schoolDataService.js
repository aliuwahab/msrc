/**
 * Created by Kaygee on 15/05/2015.
 */


mobileMSRC.service('mobDataService', ['$rootScope', '$location', '$state', '$stateParams', '$http', '$templateCache',
    function ($rootScope, $location, $state, $stateParams, $http, $templateCache) {

        var urlBase = '/public/data/';
        var urls = {
            normal_enrollment : urlBase+'normal/enrolment',
            special_enrollment : urlBase+'special/enrolment',
            normal_attendance : urlBase+'normal/attendance',
            special_attendance : urlBase+'special/attendance',
            teacher_attendance : urlBase+'teachers/attendance',
            class_management : urlBase+'teachers/class/management',
            textbook : urlBase+'school/textbooks',
            pupil_performance : urlBase+'school/pupils/performance',
            record_books : urlBase+'school/record/books',
            support : urlBase+'school/support',
            grant : urlBase+'school/grants',
            sanitation : urlBase+'school/sanitation',
            security : urlBase+'school/security',
            sports_recreational : urlBase+'school/recreational',
            school_structure : urlBase+'school/school_structure',
            furniture : urlBase+'school/furniture',
            community_involvement : urlBase+'school/community/relations',
            meetings : urlBase+'school/meetings/held',
            general_issues : urlBase+'school/general/situation'
        };


        this.fetchData = function (data_type, school_id, week_number, term, year, data_collector_type) {
            $http.get( urls[data_type ], {params : {
                school_id : school_id,
                week_number : week_number,
                term : term,
                year : year,
                data_collector_type : data_collector_type
            }, cache : false})
                .success(function (sucessData) {
                    if (sucessData.code == '200' && sucessData.status.toLowerCase() == 'ok') {
                        var toSend = {
                            code : 200,
                            school : sucessData.school,
                            data : sucessData.data
                        };
                        $rootScope.$broadcast(data_type+'Loaded', toSend);
                        return toSend
                    }else{
                        var errorOccurred = {
                            code : 404,
                            message : 'An error occurred. Please check internet connection'
                        };
                        $rootScope.$broadcast(data_type+'loaded', sucessData);
                        return errorOccurred;
                    }
                })
        };

        this.fetchTeacherData = function (data_type, school_id, week_number, term, year, data_collector_type) {
            $http.get(urls[data_type], {
                params: {
                    school_id: school_id,
                    week_number: week_number,
                    term: term,
                    year: year,
                    data_collector_type: data_collector_type
                }, cache: false
            })
                .success(function (successData) {
                    if (successData.length > 0) {
                        var toSend = {
                            code : 200,
                            data : successData
                        };
                        $rootScope.$broadcast(data_type+'Loaded', toSend);
                        return toSend;
                    }else{
                        var errorOccurred = {
                            code : 404,
                            message : 'An error occurred. Please check internet connection'
                        };
                        $rootScope.$broadcast(data_type+'loaded', sucessData);
                        return errorOccurred;
                    }
                });


        };

        this.fetchTermData = function (data_type, school_id, term, year, data_collector_type) {
            $http.get( urls[data_type ], {params : {
                school_id : school_id,
                term : term,
                year : year,
                data_collector_type : data_collector_type
            }, cache : false})
                .success(function (sucessData) {
                    if (sucessData.code == '200' && sucessData.status.toLowerCase() == 'ok') {
                        var toSend = {
                            code : 200,
                            school : sucessData.school,
                            data : sucessData.data
                        };
                        $rootScope.$broadcast(data_type+'Loaded', toSend);
                        return toSend
                    }else{
                        var errorOccurred = {
                            code : 404,
                            message : 'An error occurred. Please check internet connection'
                        };
                        $rootScope.$broadcast(data_type+'loaded', sucessData);
                        return errorOccurred;
                    }
                })
        };

    }]);