/**
 * Created by sophismay on 6/15/14.
 */

/**
 * Main Application
 *
 */

var schoolMonitor = angular.module('downloadReport',[
    'ui.router',
    'ngAnimate',
    'ngResource',
    'ui.bootstrap',
    'ui.utils',
    'toaster',
    'doowb.angular-pusher',
    //'flow',
    'multi-select',
    'angularFileUpload',
    'report_templates.app',
    'pasvaz.bindonce',
    'angular-loading-bar',
    'infinite-scroll',
    'angular-cache'
]);



schoolMonitor.config( ['$interpolateProvider', '$locationProvider','PusherServiceProvider','CacheFactoryProvider',
    function($interpolateProvider, $locationProvider, PusherServiceProvider, CacheFactoryProvider){
        //This prevents the confusion between blade templating and Angular
//    $interpolateProvider.startSymbol('<%');
//    $interpolateProvider.endSymbol('%>');
//    $locationProvider.html5Mode(true);
//    $locationProvider.hashPrefix();

        //Pusher token
        PusherServiceProvider.setToken('396cf3d207f8bb21224a');
                   // .setOptions({});

        angular.extend(CacheFactoryProvider.defaults, {
            maxAge:  7 * 24 * 60 * 60 * 1000,// Items added to this cache expire after 1week
            storageMode : 'localStorage',//memory| sessionStorage | localStorage
            storagePrefix : 'mc',
            cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
            deleteOnExpire: 'aggressive' // Items will be deleted from this cache when they expire
        });

        /* class selector to exclude/remove cells (<td> or <th>) or rows (<tr>) from the exported file(s). */
        TableExport.prototype.ignoreCSS = "tableexport-ignore";

        /* class selector to replace cells (<td> or <th>) with an empty string (i.e. "blank cell") in the exported file(s). */
        TableExport.prototype.emptyCSS = "tableexport-empty";


    }]);

schoolMonitor.run( ['$rootScope', '$location', '$state', '$stateParams',
    function ($rootScope, $location, $state, $stateParams) {
        //scUser.authenticate();


        //$rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){
//    $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){ ... })
//$rootScope.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams, error){ ... })

//$viewContentLoading - fired once the view begins loading, before the DOM is rendered. The '$scope' broadcasts the event.
//    $scope.$on('$viewContentLoading',function(event, viewConfig){

//$viewContentLoaded - fired once the view is loaded, after the DOM is rendered. The '$scope' of the view emits the event.
//    $scope.$on('$viewContentLoaded',function(event){ ... });

        $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
            $rootScope.loading = true;
        });

        $rootScope.$on('$viewContentLoaded',function(event){
            $rootScope.loading = false;
        });

        //This makes it possible to access UI-State and StateParams within the scope from the HTML partial view
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
            //if(!$rootScope.$state.includes('dashboard.school.view_selected_school') || !$rootScope.$state.includes('dashboard.view_selected_district')){
            //    //document.body.scrollTop = document.documentElement.scrollTop = 0;
            //}
        });


        //check if its on the test app or the real app
        /*true when on test server*/
        $rootScope.envCheck = !(window.location.host === "www.msrcghana.org"
            || window.location.host === "schoolmonitor.dev"
            || window.location.host === "localhost:81");


        //check if the page is the cs dashboard login page; true when on test server*/
        $rootScope.csLoginCheck =  window.location.host === "cs.msrcghana.org"
            || window.location.host === "fieldcs.msrcghana.org";
        /*|| window.location.host === "schoolmonitor.dev"*/

        /*Load all modules, just in case it hasn't been loaded when the page was hit*/
        // scGlobalService.loadAllModules();

        // Define a plugin to provide data labels
        Chart.plugins.register({
            afterDatasetsDraw: function(chart) {
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function(dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function(element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgb(0, 0, 0)';

                            var fontSize = 15;
                            var fontStyle = 'normal';
                            var fontFamily = 'Helvetica Neue';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 5;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });

    }]);
