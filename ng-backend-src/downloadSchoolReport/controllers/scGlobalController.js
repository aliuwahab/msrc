/**
 * Created by KayGee on 17/06/14.
 */

schoolMonitor.controller('scGlobalController', ['$rootScope','$scope','$http','$interval',
    'app_name_short', 'scUser','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','Pusher',
    '$state','scCircuitSupervisor', 'scHeadTeacher', 'scGlobalService',
    //'regions', 'districts', 'circuits', 'schools', 'allHeadteachers', 'allSupervisors','$state',

    function ($rootScope, $scope, $http, $interval,
              app_name_short,scUser, scNotifier, scRegion, scDistrict, scCircuit,
              scSchool, Pusher, $state, scCircuitSupervisor, scHeadTeacher, scGlobalService) {

        //This allows us to display the name programatically since the global scope can't reach that side of the dashboard
        $rootScope.appName = app_name_short;



        //This function makes the first letter of a word capital.
        //Its primarily used to make the admin role type a capital letter,
        //If this function runs without an argument, it means the session for the administrator timed out so
        //a redirect is done to the login page
        function makeFirstLetterCapital(word){
            if (word) {
                return word.charAt(0).toUpperCase() + word.substring(1);
            }else{
                $rootScope.logOut();
                if (angular.isArray()) {
                }

            }
        }

        if (!$rootScope.callOnce) {
            /*these are here so they run only once*/

            //Re initialize the side bar tree since it may not be showing when the login screen first shows
            $(".treeview").tree();


            //Use Jquery to replace the name on the dashboard so the
            //curly braces don't show on slow internet
            $('.hide_until_angular-loads').css('display', 'block');
            $rootScope.currentUser = scUser.currentUser;
            // $rootScope.currentUser.level = makeFirstLetterCapital( $rootScope.currentUser.level);

            $rootScope.callOnce = true;
        }


        if (scGlobalService.getLoadingCounter() >= 6) {
            scGlobalService.initModules($scope);
        }

        $rootScope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            if (data >= 6) {
                scGlobalService.initModules($scope);
            }
        });


        //This check is to hide certain feature from the general admin until approved by App Owners,
        //eg is allowing multiple submission
        // $rootScope.keepSecret = !!(window.location.host != "www.msrcghana.org" ||
        // $rootScope.currentUser.email == 'francis@techmerge.org');

//
//        <!--Key for Coloring the progress bars
//        <!--Key for Coloring the progress bars
//                     0 - 2 = red
//                     3 - 4 = warning
//                     5 - 6 = yellow
//                     7 - 8 = aqua
//                     9 - 10 = green
//                     -->


        $scope.colourProgressBar = function(value){
            return value < 1 ?  'danger' :
                value < 3 ?  'danger' :
                    value < 5 ? 'warning' :
                        value < 7 ? 'info' :
                            value < 9 ? 'primary' : 'success'
        };

        $scope.schoolColourProgressBar = function(value){
            return value < 1 ?  'danger' :
                value < 3 ?  'danger' :
                    value < 5 ? 'warning' :
                        value < 7 ? 'info' :
                            value < 9 ? 'primary' : 'success'
        };

        $scope.labelStatusColor = function(status){

            return status < 1 ? 'label-default' :
                status < 20 ? 'label-danger' :
                    status < 40 ? 'label-warning' :
                        status < 60 ? 'label-info' :
                            status < 80 ? 'label-primary' : 'label-success';
        };

        $scope.labelStatusText = function(status){

            return status < 1 ? 'no data' :
                status < 10 ? 'very bad' :
                    status < 20 ? 'bad' :
                        status < 30 ? 'very poor' :
                            status < 40 ? 'poor' :
                                status < 50 ? 'fair' :
                                    status < 60 ? 'average' :
                                        status < 70 ? 'good' :
                                            status < 80 ? 'very good' :
                                                status < 90 ? 'healthy' : 'excellent';
        };

        Pusher.subscribe('dataChannel', 'dataSubmitted', function (item) {
            //When new data is submitted, get the section type submitted and assign
            // sectionToGo variable and sectionUnderText variable

            var sectionToGo, sectionUnderText;

            if (item['0'] == 'schoolreportcard' ) {
                sectionToGo = 'dashboard.school.view_selected_school.report_card';
                sectionUnderText = 'School Report Card';
            }else if (  item['0'] == 'schoolreview') {
                sectionToGo = 'dashboard.school.view_selected_school.review';
                sectionUnderText = 'School Review';
            }else{
                sectionToGo = 'dashboard.school.view_selected_school.school_visit';
                sectionUnderText = 'School Visit';
            }


            //Check current admin login level and do pop toaster with click function
            if (scUser.currentUser.level == "National") {
                scNotifier.notifyInfoWithCallback(sectionUnderText,
                    'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                    function(){ $state.go(sectionToGo, {
                        id : item.school_id,
                        district_id : scSchool.schoolLookup[item.school_id].district_id });
                    });
            }else if (scUser.currentUser.level == "Regional" ) {
                if (scUser.currentUser.level_id == scSchool.schoolLookup[item.school_id].region_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else if (scUser.currentUser.level == "District" ) {
                if (scUser.currentUser.level_id == scSchool.schoolLookup[item.school_id].district_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else if (scUser.currentUser.level == "Circuit" ) {
                if (scUser.currentUser.level_id == scSchool.schoolLookup[item.school_id].circuit_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else if (scUser.currentUser.level == "School" ) {
                if (scUser.currentUser.level_id ==item.school_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else{
                scNotifier.notifyInfo(sectionUnderText, 'New data submitted for ' + scSchool.schoolLookup[item.school_id].name);
            }
        });

        //This function helps to change the texts on the dashboard to the selected view
        $rootScope.changePageTitle = function(content_header, content_header_small, selected_page){
            //$parent allows us to change the variable in the rootScope property
            $rootScope.content_header = content_header;

            $rootScope.content_header_small = content_header_small;

            $rootScope.selected_page = selected_page;

        };
        $rootScope.changePageTitle('Dashboard', 'Overview', 'Dashboard');

        //A function that gets called several times, in different places It here to prevent code duplication
        $rootScope.goBackInHistory = function(){
            window.history.back();
        };

        var date = new Date();
        $scope.thisYear = date.getFullYear();

        //This is a associative object to format the lowercase and underscores to readable words
        $scope.formatAvailability = {
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };
        //This is a associative object to format the lowercase  to sentence words
        $scope.formatGoodBad = {
            good : 'Good',
            poor : 'Poor',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };
        //This is a associative object to format the lowercase to sentence case words
        $scope.formatGoodPoorFair = {
            good : 'Good',
            fair : 'Fair',
            poor : 'Poor',
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };

        //This is a associative object to format the lowercase to sentence case words
        $scope.formatAdequecy = {
            'adequate' : 'Adequate',
            'not adequate' : 'Inadequate',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable',
            'not available' : 'Not Available',
            '' : 'Not Available'
        };

        //An object that us used several times to display all the terms is a school
        $scope.all_school_terms = [
            {number : '1st', label : '1st Term', value : 'first_term'},
            {number : '2nd', label : '2nd Term', value : 'second_term'},
            {number : '3rd', label : '3rd Term', value : 'third_term'}
        ];

        //This helps to quickly associate the term from the server to a term object above
        $rootScope.one_school_terms = {
            first_term : {number : '1st', label : '1st Term', value : 'first_term'},
            second_term: {number : '2nd', label : '2nd Term', value : 'second_term'},
            third_term : {number : '3rd', label : '3rd Term', value : 'third_term'}
        };

        //Helper function to know the chosen term in a dropdown and associate the server format to readable format
        $scope.academicTermDisplay = function(term){
            return term === 'first_term' ? '1st' :
                term === 'second_term' ? '2nd' :
                    term === 'third_term' ? '3rd' : 'NA'
        };

        //This strips the underscore and capitalizes the data collector type from the server
        $scope.formatDataCollectorType = {
            circuit_supervisor : 'Circuit Supervisor',
            head_teacher : 'Head Teacher'
        };
        //An object that us used  to display data collector type in a dropdown used in school review
        $scope.data_collector_types = [
            //{type : 'circuit_supervisor', label : 'Circuit Supervisor', value : 'data_collector'},
            {type : 'head_teacher', label : 'Head Teacher', value : 'data_collector'}
        ];


        //This strips the underscore and capitalizes the Grant  type from the server
        $scope.formatSchoolGrantType = {
            school_grant : 'School Grant',
            capitation : 'Capitation'
        };

        //This strips the underscore and capitalizes the rank type type from the server
        $scope.formatSuperitendentType = {
            superitendent : 'Superintendent',
            assistant_superitendent : 'Asst. Superintendent'
        };

//This helps to know the extension of a position. eg, 1 returns 'st' as in 1st
        //2 returns 'nd' as in second, etc
        $scope.positionExtension = function(key){

//            if (key == 10) {
//                return 'th'
//            }else{
//                key = parseInt(key.toString().charAt(key.length - 1));
//            }
//
//            return key === 1 ? 'st' :
//                    key === 2 ? 'nd' :
//                    key === 3 ? 'rd' : 'th';

        };

//        A helper function to format the dates returned from the server using moment js
        $scope.momentFormatDate = function(date, format){
            return moment(date).format(format);
            //moment().format('L');    // 09/11/2014
//            moment().format('l');    // 9/11/2014
//            moment().format('LL');   // September 11, 2014
//            moment().format('ll');   // Sep 11, 2014
//            moment().format('LLL');  // September 11, 2014 3:01 PM
//            moment().format('lll');  // Sep 11, 2014 3:01 PM
//            moment().format('LLLL'); // Thursday, September 11, 2014 3:01 PM
//            moment().format('llll'); // Thu, Sep 11, 2014 3:01 PM
        };

        $scope.momentDateFromNow = function(date){
            // eg. Just now, 22 days ago, etc
            return moment(date).fromNow();
        };



        //A helper function to scroll pages to the top on load
        $scope.scroll_to_top =function(){
            $("html, body").animate({ scrollTop: 0 }, 200);
        };

        $scope.uniqueArray =  function(array){
            var seen = [], result = [];
            for(var len = array.length, i = len-1; i >= 0; i--){
                if(!seen[array[i]]){
                    seen[array[i]] = true;
                    result.push(array[i]);
                }
            }
            return result;
        };

        //This function is called when a circuit/district is clicked on
        function clickBarChart(event) {
            if (event.chart.categoryField == 'region') {

                $scope.$broadcast('regionChartClicked', event.item.dataContext.id);

            }else if (event.chart.categoryField == 'district') {

                $scope.$broadcast('districtChartClicked', event.item.dataContext.id);

            }else if (event.chart.categoryField == 'circuit') {

                $scope.$broadcast('circuitChartClicked', event.item.dataContext.id);

            }

        }

        $rootScope.removeTrailingComma = function(classSubject){
            if ($.trim(classSubject.charAt(classSubject.length - 1)) === ',') {
                return classSubject.substring(0, classSubject.length - 1)
            }else return classSubject
        };

        /*Start of AmCharts*/

        $scope.changeSerialChart = function changeCharts(chartData, categoryField, chartTitle, chartDivID, chartType){

            var valueAxisTitle =  "Rating (%)";
            var titleTextTwo = 'Rating';
            var graphValueField = 'rating';

            if (chartType == 'ht_average') {
                valueAxisTitle = "Average (%)";
                titleTextTwo = 'Attendance Average';
                graphValueField = 'headteacherAttendanceAverage'
            }else if(chartType == 'cs_average'){
                valueAxisTitle = "Average (%)";
                titleTextTwo = 'Teacher Attendance Average by Head Teachers';
                graphValueField = 'supervisorAttendanceHolder'
            }

            // SERIAL CHART
            var chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = categoryField;
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 45;
            categoryAxis.title = chartTitle;
            categoryAxis.gridPosition = "start";

            //value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = valueAxisTitle;
            valueAxis.dashLength = 5;
            valueAxis.axisAlpha = 0;
            valueAxis.minimum = 0;
//            valueAxis.maximum = 6;
            valueAxis.integersOnly = true;
            valueAxis.gridCount = 10;
            valueAxis.reversed = false; // this line makes the value axis reversed
            chart.addValueAxis(valueAxis);

            // value
            chart.titles = [
                {
                    "id": "Title-1",
                    "size": 15,
                    text : chartTitle
                },
                {
                    "id": "Title-2",
                    "size": 10,
                    text : titleTextTwo
                }
            ];
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.

            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };



            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField =  graphValueField;
            graph.balloonText = "[[category]]: <b>[[value]]  %</b>";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);

            chart.creditsPosition = "top-right";

            // add click listener
            chart.addListener("clickGraphItem", clickBarChart);

            $interval( chart.write(chartDivID), 200, 2);


            return chart;



        };
        /*End of amSerialCharts*/


        /*Start of amPieCharts*/
        $scope.changePieChart = function(chartData, chartTitle, chartDivID){
            var chart;
            var legend;

            // PIE CHART
            chart = new AmCharts.AmPieChart();
            chart.dataProvider = chartData;
            chart.titleField = "field";
            chart.valueField = "value";

            //COLORS
//			chart.colors = ["#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01", "#B0DE09", "#04D215", "#0D8ECF",
// "#0D52D1", "#2A0CD0", "#8A0CCF", "#CD0D74", "#754DEB", "#DDDDDD", "#999999", "#333333", "#000000",
// "#57032A", "#CA9726", "#990000", "#4B0C25"]
            chart.colors=["#0D8ECF","#FF6600","#8A0CCF","#04D215","#CD0D74","#754DEB","#CA9726","#F8FF01"];

            //3D SETTING
            chart.depth3D = 10; //0 for flat
            chart.angle = 10; //0 when it is flat

            // LEGEND
            legend = new AmCharts.AmLegend();
            legend.align = "center";
            legend.markerType = "circle";
            chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
            chart.addLegend(legend);

            //TITLE
            chart.addTitle(chartTitle);

            //LABEL
            chart.labelRadius = 30;
            chart.labelText = "[[title]]: [[value]]";
//                chart.labelRadius = -30; // label Inside
//                chart.labelText = "[[percents]]%"; // display labels as percentage


            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // WRITE
            $interval(chart.write(chartDivID), 2000, 2);

        };

        /*End of amPieCharts*/

        // PIE CHART OTHER SETTINGS
//        chart.outlineColor = "#FFFFFF";
//        chart.outlineAlpha = 0.8;
//        chart.outlineThickness = 2;
//		chart.labelsEnabled = false;
//		chart.autoMargins = false;
//		chart.marginTop = 0;
//		chart.marginBottom = 0;
//		chart.marginLeft = 0;
//		chart.marginRight = 0;
//		chart.pullOutRadius = 0;

        //START OF LINE CHART




        $scope.changeLineChart = function (chartData, chartTitle,  divID) {
            var chart;
//            var chartData = chartData;

            var graph1Title,graph1ValueField, graph2Title,graph2ValueField,categoryField, categoryAxisTitle;

            if (divID == 'teacherLineChart') {
                graph1Title = "Present";
                graph1ValueField = "days_present";
                graph2Title = "Absent";
                graph2ValueField = "total_number_of_days_absent";
            }else{
                graph1Title = "Boys";
                graph1ValueField = "boys";
                graph2Title = "Girls";
                graph2ValueField = "girls";
            }

            if(divID == 'enrollmentLineChart'){
                categoryField = 'chartLabel';
                categoryAxisTitle = 'Term';
            }else{
                categoryField = 'week';
                categoryAxisTitle = 'Week Number';

            }

            function zoomChart() {
                chart.zoomToIndexes(1, 10);
            }
//

            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "http://cdn.amcharts.com/lib/3/images/";
            chart.dataProvider = chartData;
            chart.categoryField = categoryField;

            // listen for "dataUpdated" event (fired when chart is inited) and call zoomChart method when it happens
//            chart.addListener("dataUpdated", zoomChart);

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = false;
            categoryAxis.minPeriod = "WW";
            categoryAxis.minorGridEnabled = true;
            categoryAxis.axisColor = "#DADADA";
            categoryAxis.twoLineMode = true;
            categoryAxis.title = categoryAxisTitle;
            categoryAxis.dateFormats = [
                {
                    period: 'fff',
                    format: 'JJ:NN:SS'
                },
                {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                },
                {
                    period: 'mm',
                    format: 'JJ:NN'
                },
                {
                    period: 'hh',
                    format: 'JJ:NN'
                },
                {
                    period: 'DD',
                    format: 'DD'
                },
                {
                    period: 'WW',
                    format: 'DD'
                },
                {
                    period: 'MM',
                    format: 'MMM'
                },
                {
                    period: 'YYYY',
                    format: 'YYYY'
                }
            ];

            // first value axis (on the left)
            var valueAxis1 = new AmCharts.ValueAxis();
            valueAxis1.axisColor = "#0D8ECF";
            valueAxis1.axisThickness = 2;
            valueAxis1.gridAlpha = 0;
            valueAxis1.title = graph1Title;
            chart.addValueAxis(valueAxis1);

            // second value axis (on the right)
            var valueAxis2 = new AmCharts.ValueAxis();
            valueAxis2.position = "right"; // this line makes the axis to appear on the right
            valueAxis2.axisColor = "#FF6600";
            valueAxis2.gridAlpha = 0;
            valueAxis2.axisThickness = 2;
            valueAxis2.title = graph2Title;
            chart.addValueAxis(valueAxis2);


            // GRAPHS
            // first graph
            var graph1 = new AmCharts.AmGraph();
            graph1.valueAxis = valueAxis1; // we have to indicate which value axis should be used
            graph1.title = graph1Title;
            graph1.inside = false;
            graph1.valueField =  graph1ValueField;
            graph1.bullet = "round";
            graph1.hideBulletsCount = 30;
            graph1.bulletBorderThickness = 1;
            graph1.lineColor = "#0D8ECF";
            graph1.balloonText = graph1Title + " : [[value]]";
//            graph1.type = "column";
            chart.addGraph(graph1);

            // second graph
            var graph2 = new AmCharts.AmGraph();
            graph2.valueAxis = valueAxis2; // we have to indicate which value axis should be used
            graph2.title = graph2Title;
            graph2.valueField = graph2ValueField;
            graph2.bullet = "square";
            graph2.hideBulletsCount = 30;
            graph2.bulletBorderThickness = 1;
            graph2.lineColor = "#FF6600";
            graph2.balloonText = graph2Title + " : [[value]]";
//            graph2.type = "column";
            chart.addGraph(graph2);

            // third graph
//            var graph3 = new AmCharts.AmGraph();
//            graph3.valueAxis = valueAxis3; // we have to indicate which value axis should be used
//            graph3.valueField = "views";
//            graph3.title = "green line";
//            graph3.bullet = "triangleUp";
//            graph3.hideBulletsCount = 30;
//            graph3.bulletBorderThickness = 1;
//            chart.addGraph(graph3);

            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0.1;
            chartCursor.fullWidth = true;
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chart.addChartScrollbar(chartScrollbar);

            // LEGEND
            var legend = new AmCharts.AmLegend();
            legend.marginLeft = 110;
            legend.useGraphSettings = true;
            chart.addLegend(legend);

            chart.titles = [
                {
                    "id": "Title-1",
                    "size": 15,
                    text : chartTitle
                }
            ];

            // WRITE
            chart.write(divID);
        };


        /*-------------            Bar Chart        ------------------*/

        $scope.changeBarChart = function (chartData, chartTitle, chartDivID, type, average){

            //type determines if the graph is used i rating views
            //if its a rating graph, change the value axis title to rating, and the
//            display type to a stacked view
            var chart_display_type = null;
            var valueAxisTitle = "Days";

            if (type == 'rating') {
                chart_display_type = 'rating';
                valueAxisTitle = "Rating";
            }else if (type == 'ht_average') {
                chart_display_type = 'ht_average';
                valueAxisTitle = "Teacher Attendance Average";
            }else if (type == 'cs_average') {
                chart_display_type = 'cs_average';
                valueAxisTitle = "Teacher Attendance Average";
            }

//            average is the district average if it is a district average attendance
            var teacher_average = average || 0;


            // SERIAL CHART
            var chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "field";
            chart.startDuration = 1;
            chart.maxSelectedSeries = 25;
            chart.mouseWheelScrollEnabled = true;
            chart.pathToImages = "http://cdn.amcharts.com/lib/3/images/";

            // SCROLLBAR
            var chartScrollBar = new AmCharts.ChartScrollbar();
            chartScrollBar.graphType = 'column';
            chartScrollBar.resizeEnabled = true;
            chartScrollBar.scrollbarHeight = 21;
            chartScrollBar.scrollbarWIdth = 2;
            chartScrollBar.scrollDuration = 0;
            chartScrollBar.updateOnReleaseOnly = false;
            chart.addChartScrollbar(chartScrollBar);

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 0;
            categoryAxis.gridPosition = "start";
            categoryAxis.labelRotation = 45;



            //value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = valueAxisTitle;
            valueAxis.dashLength = 1;
            valueAxis.axisAlpha = 0;
            valueAxis.minimum = 0;
            if (chart_display_type == 'rating') {
                valueAxis.maximum = 10;
            }
            valueAxis.stackType = "regular";
            valueAxis.integersOnly = true;
            valueAxis.gridCount = 10;
            valueAxis.reversed = false; // this line makes the value axis reversed
            chart.addValueAxis(valueAxis);

            // value
            chart.titles = [
                {
                    "id": "Title-1",
                    "size": 15,
                    text : chartTitle
                }
            ];
            if (chart_display_type == 'ht_average' ||  chart_display_type == 'cs_average' ) {
                chart.titles.push({
                    "id": "Title-2",
                    "size": 12,
                    text : 'Teacher Attendance Average : ' + teacher_average + ' %'
                })
            }
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.

            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick: function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // GRAPH
            var graph = new AmCharts.AmGraph();
            if (chart_display_type) {
                graph.title = valueAxisTitle;
            }
            graph.valueField = "value";
            graph.balloonText = "[[field]]: <b>[[value]]</b>";
            graph.type = "column";
            graph.labelText = "[[value]]";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);

            if (chart_display_type) {
                // second graph
                graph = new AmCharts.AmGraph();
                graph.title = "Difference";
//                graph.labelText = "[[value]]";
                graph.valueField = "remaining";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 1;
                graph.lineColor = "#ffc299";
                graph.balloonText = "Difference : <b>[[value]]</b>";
                chart.addGraph(graph);

            }

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);

//            chart.creditsPosition = "top-right";

            if (chart_display_type) {
                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.align = "center";
//            legend.markerType = "circle";
//                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                chart.addLegend(legend);
            }



            $interval(chart.write(chartDivID), 200, 2);



        };
        /*End of  bar Charts*/

    }]);
