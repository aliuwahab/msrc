/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Factory service for User
 *
 */

schoolMonitor.factory('scUser', ['$resource','$http','$q','$rootScope','CacheFactory','scNotifier','$state','$timeout',
    function($resource, $http, $q, $rootScope, CacheFactory, scNotifier, $state, $timeout){

        var userService = {};
        var url = '/admin/user/profile';

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('userCache')) {
            var userCache = CacheFactory('userCache', {
                maxAge: 60 * 60 * 1000, // 60minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    if (key == url) {
                        $http.get(key).success(function (successData) {
                            if (successData.code == '200' && $.trim(successData.status.toLowerCase()) == 'ok') {
                                if(successData.user.type && successData.user.type  == "circuit_supervisor"){
                                    successData.user =  appendCSMissingProperties(successData.user);
                                }
                                userCache.put(key, successData);
                            }else{
                                $rootScope.logOut();
                            }
                        });
                    }
                }
            });
        }

        /*The CS that logs in has some different properties from the admin login object. This function makes sure the login objects are the same*/
        var appendCSMissingProperties = function (returnedUserObj) {
            returnedUserObj.username = returnedUserObj.first_name + " " + returnedUserObj.last_name;
            returnedUserObj.firstname = returnedUserObj.first_name;
            returnedUserObj.lastname = returnedUserObj.last_name;
            returnedUserObj.level_id =returnedUserObj.circuit_id;
            returnedUserObj.level = "circuit";
            returnedUserObj.role = 'super';/*normal*/

            return returnedUserObj;
        };

        function fetchCurrentUser(){
            var deferred = $q.defer();

            var cachedData = userCache.get(url);

            if (!cachedData ) {
                $http.get(url)
                    .success(function(successData){

                        if (successData.code == '200' && $.trim(successData.status.toLowerCase()) == 'ok') {
                            if(successData.user.type && successData.user.type  == "circuit_supervisor"){
                                successData.user =  appendCSMissingProperties(successData.user);
                            }else{
                                /*parsing the JSON because the server returns a string*/
                                successData.user =  JSON.parse(successData.user);
                            }
                            $rootScope.currentUser = userService.currentUser = successData.user;

                            userCache.put(url, successData.user);
                            deferred.resolve(true);
                        }else{
                            $state.go('login');
                            deferred.resolve(false);
                        }
                    })
                    .error(function (data) {
                        $state.go('login');
                        deferred.reject(false);
                    });
            }else{

                userService.currentUser = cachedData;
                $rootScope.currentUser = cachedData;
                deferred.resolve(true);
            }
            return deferred.promise;
        }

        userService.loginUser = function (loginForm) {
            $rootScope.submittingLogin = true;
            var loginUrl = '/login';
            if ($rootScope.csLoginCheck) {
                if(loginForm.cs_phone_number.length == 10 &&  loginForm.cs_phone_number.charAt(0) == '0' ){
                    loginForm.cs_phone_number = "+233" + loginForm.cs_phone_number.substring(1);
                }else if ( loginForm.cs_phone_number.length > 10 && loginForm.cs_phone_number.substring(0,3) == "233") {
                    loginForm.cs_phone_number = "+" + loginForm.cs_phone_number;
                }
                loginUrl = "/cs/dashboard/authentication";
            }

            $http.post(loginUrl, loginForm)
                .success(function (successData) {
                    if (successData.code == '200' && successData.status.toLowerCase() == 'ok') {
                        scNotifier.notifySuccess("Login Success", "Credentials are valid. Redirecting to the dashboard");
                        if (successData.authticated_cs) {
                            successData.authticated_cs =  appendCSMissingProperties(successData.authticated_cs);
                            $rootScope.currentUser =  userService.currentUser = successData.authticated_cs;
                        }else{
                            /*parsing the JSON because the server returns a string*/
                            $rootScope.currentUser =  userService.currentUser = JSON.parse(successData.user);
                        }
                        /*delete is used so we don't have too many variables in the scope object*/
                        delete $rootScope.submittingLogin;
                        delete $rootScope.loginError;
                        $state.go('dashboard.home');

                    }else{
                        scNotifier.notifyFailure("Login Failed", "Please check credentials, and re-enter them.");
                        $rootScope.loginError = true;
                        $timeout(function () {
                            /*delete is used so we don't have too many variables in the scope object*/
                            delete $rootScope.loginError;
                        }, 5000);
                        delete $rootScope.submittingLogin;

                    }
                })
                .error(function () {
                    scNotifier.notifyFailure("Network Error", "Please check internet connection and try again.");
                    delete $rootScope.submittingLogin;

                })

        };


        userService.authenticate =  function(){
            var defer = $q.defer();
            if ( userService.currentUser &&  userService.currentUser.id) {
                defer.resolve(true);
                return defer.promise;
            }

            fetchCurrentUser().
                then(function(success){
                    if (success) {
                        defer.resolve(true);
                    }else{
                        defer.reject(false);
                    }
                }, function (error) {
                    defer.reject(false);
                });
            return defer.promise;
        };

        userService.getAllUsers = function(){
            var deferred = $q.defer();
            var url = '/admin/users?country_id=1';

            var cachedData = userCache.get(url);
            if (!cachedData ) {
                $http.get(url)
                    .success(function(data){
                        userCache.put(url, data);
                        userService.allUsers = data.users;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                userService.allUsers = cachedData.users;
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


        userService.createUser =  function(formData){
            return $resource('/admin/user', {}, {
                create:{method:'POST', url:'/admin/user/create', params:{}}
            }).create(formData).$promise;
        };
        userService.editUser =  function(formData){
            return $resource('/admin/user', {}, {
                edit:{method:'POST', url:'/admin/user/update', params:{}}
            }).edit(formData).$promise;
        };
        userService.resetPassword =  function(formData){
            return $resource('/admin/user', {}, {
                editPassword:{method:'POST', url:'/admin/user/password/change', params:{}}
            }).editPassword(formData).$promise;
        };
        userService.deleteUser =  function(user_id){
            return $resource('/admin/user', {}, {
                edit:{method:'POST', url:'/admin/user/:id/delete', params:{id : '@id'}}
            }).edit({id : user_id}).$promise;
        };

        userService.reset = function(){
            userService.allUsers = [];
            userCache.removeAll();
        };

        $rootScope.logOut = function () {
            userCache.remove(url);
            userCache.removeAll();
            userService.currentUser =  '';
            $http.get('/logout')
                .success(function () {
                    $rootScope.currentUser = '';
                });
        };

        return userService;

    }]);


