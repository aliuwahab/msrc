/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Global Functions that will need to be called regularly from several places
 *
 */

schoolMonitor.service('scGlobalService',['$rootScope','$http','$interval',
    'app_name_short','foursquare_client_id', 'foursquare_client_secret',
    'scUser','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','Pusher',
    '$state','scCircuitSupervisor', 'scHeadTeacher', 'scReportGenerator',
    //'regions', 'districts', 'circuits', 'schools', 'allHeadteachers', 'allSupervisors','$state',

    function ($rootScope, $http, $interval,
              app_name_short, foursquare_client_id, foursquare_client_secret,
              scUser, scNotifier, scRegion, scDistrict, scCircuit, scSchool, Pusher,
              $state, scCircuitSupervisor, scHeadTeacher, scReportGenerator) {


        // Check to make sure the cache doesn't already exist
        //if (!CacheFactory.get('publicSMSCache')) {
        //    var publicSMSCache = CacheFactory('publicSMSCache', {
        //        maxAge: 24 * 60 * 60 * 60 * 1000, // 15minutes
        //        deleteOnExpire: 'aggressive',
        //        onExpire: function (key, value) {
        //            $http.get(key).success(function (data) {
        //                publicSMSCache.put(key, data);
        //            });
        //        }
        //    });
        //}


        //This counter will be incremented to know if everything has been loaded
        var modelLoadingCounter = 0;


        this.getLoadingCounter = function () {
            return modelLoadingCounter;
        };

        this.incrementLoadingCounter = function () {
            return ++modelLoadingCounter;
        };

        var that = this;
        /*----------------** Lookup ------------------------*/
        //Lookup is an object that uses the id of the region as a key to find that region,
        //without using a for loop when we want to determine which region something belongs to

        this.loadAllModules = function () {
            scRegion.getAllRegions().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});
            });

            scDistrict.getAllDistricts().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});
            });

            scCircuit.getAllCircuits().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});

            });
            scSchool.getAllSchools().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});

            });
            scCircuitSupervisor.getAllSupervisors().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});

            });
            scHeadTeacher.getAllHeadteachers().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});
            });
        };


        this.initModules = function ($scope) {
            scRegion.init();
            $scope.regionLookup = scRegion.regionLookup;
            $scope.regions = scRegion.regions;

            scDistrict.init();
            $scope.districtLookup = scDistrict.districtLookup;
            $scope.districts = scDistrict.districts;

            scCircuit.init();
            $scope.circuitLookup = scCircuit.circuitLookup;
            $scope.circuits = scCircuit.circuits;


            scCircuitSupervisor.init();

            scHeadTeacher.init();

            scSchool.init();
            $scope.schoolLookup = scSchool.schoolLookup;
            $scope.schools = scSchool.schools;
            //$rootScope.schools = scSchool.schools;

            if (scUser.currentUser.level === "National") {
                $rootScope.adminRight = 5;
                $rootScope.currentUser.geography = 'Ghana';
            }else if (scUser.currentUser.level === "Regional" ) {
                $rootScope.adminRight = 4;
                $rootScope.currentUser.geography = scRegion.regionLookup[scUser.currentUser.level_id].name;
            }else if (scUser.currentUser.level === "District" ) {
                $rootScope.adminRight = 3;
                $rootScope.currentUser.geography = scDistrict.districtLookup[scUser.currentUser.level_id].name;
            }else if (scUser.currentUser.level === "Circuit" ) {
                $rootScope.adminRight = 2;
                $rootScope.currentUser.geography = scCircuit.circuitLookup[scUser.currentUser.level_id].name;
            }else if (scUser.currentUser.level === "School" ) {
                $rootScope.adminRight = 1;
                $rootScope.currentUser.geography = scSchool.schoolLookup[scUser.currentUser.level_id].name;
            }else{
                $rootScope.adminRight = 0;
            }

            if (scUser.currentUser.role === 'super') {
                $rootScope.adminRight += 0.5;
            }
        };

        this.appendIfNotNull =  function(stringToCheck){
            if (!(stringToCheck == null || stringToCheck == '')) {
                return stringToCheck + ' '
            }else{
                return '';
            }
        };

        this.makeFirstLetterCapital = function(word){
            return word.charAt(0).toUpperCase() + word.substring(1);
        };

        this.drawStatisticsBarChart =  function drawChart(labelsArray, label, chartData, target, canvasId) {
            if (angular.isDefined(scReportGenerator.barChart[target])) {
                scReportGenerator.barChart[target].destroy();
            }

            var ctx = document.getElementById(canvasId).getContext("2d");
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labelsArray,
                    datasets: [{
                        label: label,
                        data: chartData,
                        backgroundColor: [
                            pattern.draw('square', 'rgba(255, 99, 132, 0.75)'),
                            pattern.draw('circle', 'rgba(54, 162, 235, 0.75)'),
                            pattern.draw('diamond', 'rgba(255, 206, 86, 0.75)'),
                            pattern.draw('zigzag-horizontal', 'rgba(75, 192, 192, 0.75)'),
                            pattern.draw('triangle', 'rgba(153, 102, 255, 0.75)'),
                            pattern.draw('plus', 'rgba(255, 159, 64, 0.75)'),
                            pattern.draw('cross', 'rgba(239, 7, 7, 0.75)'),
                            pattern.draw('dot', 'rgba(31, 29, 171, 0.75)'),
                            pattern.draw('disc', 'rgba(231, 235, 8, 0.75)'),
                            pattern.draw('ring', 'rgba(16, 204, 13, 0.75)'),
                            pattern.draw('weave', 'rgba(119, 9, 146, 0.75)')
                        ]
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: label
                    },
                    legend: {
                        display: false
                        // labels: {
                        //     fontColor: 'rgb(255, 99, 132)'
                        // }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            if (chart){
                chart.update();
            }


            scReportGenerator.barChart[target] = chart;
        };

        this.drawStatisticsBarStackedChart =  function drawChart(labelsArray, labelArray, chartData, target, canvasId) {

            if (angular.isDefined(scReportGenerator.barChart[target])) {
                scReportGenerator.barChart[target].destroy();
            }

            var ctx = document.getElementById(canvasId).getContext("2d");
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labelsArray,
                    datasets: chartData
                },
                options: {
                    title: {
                        display: true,
                        text: labelArray
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
            if (chart) chart.update();
            scReportGenerator.barChart[target] = chart;
        };

        this.drawPieStatisticsChart = function drawChart(labelsArray, label, chartData, target, id) {

            if (angular.isDefined(scReportGenerator.pieChart[target])) {
                scReportGenerator.pieChart[target].destroy();
            }

            var chart = new Chart(document.getElementById(id).getContext("2d"), {
                type: 'pie',
                data: {
                    labels: labelsArray,
                    datasets: [{
                        data: chartData,
                        label: label,
                        backgroundColor: [
                            pattern.draw('weave', 'rgba(75, 192, 192, 0.75)'),
                            pattern.draw('diamond', 'rgba(54, 162, 235, 0.5)'),
                            pattern.draw('zigzag-horizontal', 'rgba(255, 206, 86, 0.5)'),
                            pattern.draw('plus', 'rgba(255, 159, 64, 0.5)'),
                            pattern.draw('dot', 'rgba(31, 29, 171, 0.75)'),
                            pattern.draw('disc', 'rgba(231, 235, 8, 0.75)'),
                            pattern.draw('cross', 'rgba(239, 7, 7, 0.75)'),
                            pattern.draw('ring', 'rgba(16, 204, 13, 0.75)'),
                            pattern.draw('square', 'rgba(75, 192, 192, 0.5)'),
                            pattern.draw('circle', 'rgba(255, 99, 132, 0.5)'),
                            pattern.draw('triangle', 'rgba(153, 102, 255, 0.5)')
                        ]
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: label
                    },
                    responsive: true
                }
            });
            scReportGenerator.pieChart[target] = chart;
            if (chart){
                    chart.update();
            }

        }

    }]);
