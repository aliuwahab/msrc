/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for District resource
 *
 */

schoolMonitor.factory('scDistrict', ['$resource', 'scRegion', '$q','CacheFactory','$http','scNotifier','$rootScope','$timeout',
    function($resource, scRegion, $q, CacheFactory, $http, scNotifier, $rootScope, $timeout){

        var districtResource = $resource('/admin/ghana/districts',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/district/create', params:{}},
            createItinerary:{method:'POST', url:'/admin/district/itinerary', params:{}},
            getItinerary:{method:'GET', url:'/admin/district/itinerary/:id', params:{}},
            getAllItineraries:{method:'GET', url:'/admin/itinerary', params:{country_id: 1}, isArray:true, cache:false},
            retrieveOneDistrict:{method:'GET', url:'/admin/district/:id/edit', params:{id : '@id'}},
            editDistrict:{method:'POST', url:'/admin/district/:id/update', params:{id : '@id'}},
            deleteDistrict:{method:'POST', url:'/admin/district/:id/delete', params:{id : '@id'}}
        });

        var scDistrict = {};

        var districts;

        scDistrict.districts = [];

        scDistrict.districtLookup = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('districtCache')) {
            var districtCache = CacheFactory('districtCache', {
                //maxAge: 60 * 60 * 1000 // 1 hour
                //deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        var allDistrictsUrl = '/admin/ghana/districts', loadingDistricts = false;

        scDistrict.getAllDistricts = function(){
            if (loadingDistricts) {
                return;
            }
            loadingDistricts = true;

            var defer = $q.defer();
            var cachedData = districtCache.get(allDistrictsUrl);

            if (!cachedData) {
                scNotifier.notifyInfo('Info', 'Updating districts...');
                districtResource.query().$promise.then(function(data){
                    if (data) {
                        scDistrict.districts = data;
                        districtCache.put(allDistrictsUrl, data);
                        loadingDistricts = false;
                        defer.resolve(true);
                    }
                }, function(){
                    loadingDistricts = false;
                    defer.resolve(false)
                });
            }else{
                scDistrict.districts = cachedData;
                loadingDistricts = false;
                defer.resolve(true);
            }
            return defer.promise;
        };

        scDistrict.updateDistrictArray = function(){
            var defer = $q.defer();
            districtCache.remove(allDistrictsUrl);
            this.districts = [];

            this.getAllDistricts().then(
                function(status){
                    if (status) {
                        scDistrict.districts.sort(function (a, b) {
                            return a.name.toLowerCase() - b.name.toLowerCase();
                        });
                        $timeout(function () {
                            $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                            $rootScope.loading = false;
                            defer.resolve(true);
                        }, 100);
                    }else{
                        defer.resolve(false);
                    }
                }, function(){
                    $rootScope.loading = false;

                    defer.resolve(false);
                });
            return defer.promise;
        };

        $rootScope.reloadDistricts = function () {
            $rootScope.loading = true;
            scDistrict.updateDistrictArray();
        };


        scDistrict.init = function(){

            angular.forEach(this.districts, function(district, index){

                //Initiate the district rating
                district.rating = 0;
                scDistrict.districtLookup[district.id] = district;

                scDistrict.districtLookup[district.id].inclusiveness =  0;
                scDistrict.districtLookup[district.id].inclusivenessHolder = 0;

                scDistrict.districtLookup[district.id].safe_protective = 0;
                scDistrict.districtLookup[district.id].safe_protectiveHolder = 0;

                scDistrict.districtLookup[district.id].healthy =  0;
                scDistrict.districtLookup[district.id].healthyHolder = 0;

                scDistrict.districtLookup[district.id].gender_friendly = 0;
                scDistrict.districtLookup[district.id].gender_friendlyHolder = 0;

                scDistrict.districtLookup[district.id].effective_teaching_learning =  0;
                scDistrict.districtLookup[district.id].effective_teaching_learningHolder = 0;

                scDistrict.districtLookup[district.id].community_involvement =  0;
                scDistrict.districtLookup[district.id].community_involvementHolder = 0;

                scDistrict.districtLookup[district.id].supervisorAttendanceHolder = 0;
                scDistrict.districtLookup[district.id].supervisorAttendanceAverage = 0;

                scDistrict.districtLookup[district.id].headteacherAttendanceHolder = 0;
                scDistrict.districtLookup[district.id].headteacherAttendanceAverage =  0;


                //Get the region name into the district object
                scDistrict.districts[index].regionName = scRegion.regionLookup[district.region_id].name;

                //This is to keep track of how many circuits are in each district
                scDistrict.districtLookup[district.id].totalCircuits = 0;

                //This is to contain all circuits under this district for easy lookup
                scDistrict.districtLookup[district.id].allCircuits = {};

                //This is to contain all circuits under this district for displaying
                scDistrict.districtLookup[district.id].allCircuitsHolder = [];

                //This is to keep track of how many schools are in each district
                scDistrict.districtLookup[district.id].totalSchools = 0;

                //This is to contain all schools under this district to be looked up by id
                scDistrict.districtLookup[district.id].allSchools = {};

                //Use the school_id in the scDistrict so that we can lookup a district using only the school_Id
                scDistrict.searchSchoolDistrict = {};

                //This is to contain all schools under this district
                scDistrict.districtLookup[district.id].allSchoolsHolder = [];

                //This is to contain all itineraries of each district
                scDistrict.districtLookup[district.id].itineraries = [];

//        //This is to keep track of how many supervisors are in each district
                scDistrict.districtLookup[district.id].circuit_supervisors = 0;

                //This is to hold circuit supervisors that are in each district
                scDistrict.districtLookup[district.id].circuit_supervisors_holder = [];

//        //This is to keep track of how many headteachers are in each district
                scDistrict.districtLookup[district.id].head_teachers = 0;

                //This is to hold head teachers that are in each district
                scDistrict.districtLookup[district.id].head_teachers_holder = [];

                /*kgTodo - Calculate district rating and position*/
                //This is randomly generated positions and ratings pending actual implementation
//            scDistrict.districts[index].rating = Math.floor(Math.random() * 100);
                scDistrict.districts[index].position = Math.floor(Math.random() * 10 + 1);

                //Increment the total districts of the region by one
                scRegion.regionLookup[district.region_id].totalDistricts += 1;

                //add district to the region object and associate it by its id
                scRegion.regionLookup[district.region_id].allDistricts[district.id] = district;

                //add district to the region object's districts
                scRegion.regionLookup[district.region_id].allDistrictsHolder.push(district);

                //This is randomly generated school review ratings pending actual implementation
                //scDistrict.districts[index].inclusiveness = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].safe_protective = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].healthy = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].gender_friendly = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].effective_teaching_learning = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].community_involvement = Math.floor(Math.random() * 10);

            });

            scDistrict.districts.sort(function (a, b) {
                if (a.name > b.name)
                    return 1;
                if (a.name < b.name)
                    return -1;
                // a must be equal to b
                return 0;
            });
        };

        scDistrict.circuits= [];
        scDistrict.schools =[];

        scDistrict.addDistrict = function(formData){
            var defer = $q.defer();
            // process the form
            districtResource.create(formData,
                //success callback
                function (value) {
                    if (value[0] == 's' && value[1] == 'u') {
                        scDistrict.updateDistrictArray().then(
                            function(status){
                                if (status) {
                                    defer.resolve(true);
                                }else{
                                    defer.resolve(false);
                                }
                            },
                            function(){
                                defer.resolve(false);
                            }
                        );
                        defer.resolve(true);
                    }else {
//                    scDistrict.addDistrictStatus = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };

        scDistrict.deleteDistrict = function(district_id){

            var defer = $q.defer();
            if (scDistrict.districtLookup[district_id]) {
                districtResource.deleteDistrict({id : district_id},
                    //success callback
                    function(data){
                        if (data[0] == 's' && data[1] == 'u') {

                            scDistrict.updateDistrictArray().then(
                                function(status){
                                    if (status) {
                                        defer.resolve(true);
                                    }else{
                                        defer.resolve(false);
                                    }
                                },
                                function(){
                                    defer.resolve(false);
                                }
                            );
                        }else{
                            defer.resolve(false);
                        }
                    },
//                error callback
                    function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };




//    Retrieve a single district to view or edit
        scDistrict.retrieveOneDistrict = function(district_id){
            var defer = $q.defer();
            districtResource.retrieveOneDistrict({id: district_id},
                //After retrieving the region to edit, return it to the controller

                function (data, responseHeaders) {
                    defer.resolve(true);
                    scDistrict.districtRetrieved = data
                },

                //Error function
                function (httpResponse) {
                    defer.resolve(false);
                    return false
                });
            return defer.promise;
        };



        scDistrict.editDistrict = function(formData){
            var defer = $q.defer();
            districtResource.editDistrict(formData,
                //success callback
                function (value, responseHeaders) {
//                        the value returned is a json object, hence the wierd checking to see success
                    if (value == 'success' || (value[0] == 's' && value[1] == 'u')) {
                        //If successful, requery the new regions and assign them to the parent regions variable
                        scDistrict.updateDistrictArray();
                        defer.resolve(true);
                    } else {
                        defer.resolve(false)
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };

        scDistrict.sendNotification = function(){

        };

        scDistrict.fetchItinerary = function(district_id, reloader){

            var aDistrictsItineraryUrl = '/admin/itinerary?country_id=1&district_id='+district_id;

            if (reloader) {
                districtCache.remove(aDistrictsItineraryUrl);
            }

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsItineraryUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsItineraryUrl)
                    .success(function(data){
                        districtCache.put(aDistrictsItineraryUrl, data);
                        scDistrict.districtLookup[district_id].itineraries = data.itinerary;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                scDistrict.districtLookup[district_id].itineraries = cachedData.itinerary;
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };

        scDistrict.allTeachersData = function(district_id){
            //return $resource('/admin/ghana/districts', {}, {
            //    allDistrictTeachersData:{method:'GET', url:'/admin/school_report_card/school/teachers', params:{country_id: 1}, isArray:true, cache:false}
            //}).allDistrictTeachersData({district_id : district_id});
            var aDistrictsTeachersDataUrl = '/admin/school_report_card/school/teachers?country_id=1&district_id='+district_id;

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsTeachersDataUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsTeachersDataUrl)
                    .success(function(data){
                        districtCache.put(aDistrictsTeachersDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };

        scDistrict.fetchWeeklyData = function(district_id){
            //return $resource('/admin/ghana/districts', {}, {
            //    getWeeklyData:{method:'GET', url:'/admin/totals/weekly/data', params:{country_id: 1}, isArray:true, cache:false}
            //}).getWeeklyData({district_id : district_id});
            var aDistrictsWeeklyDataUrl = '/admin/totals/weekly/data?country_id=1&district_id='+district_id;

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsWeeklyDataUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsWeeklyDataUrl)
                    .success(function(data){
                        districtCache.put(aDistrictsWeeklyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };

        scDistrict.fetchTermlyData = function(params, reloader){
            //return $resource('/admin/ghana/districts', {}, {
            //    getTermlyData:{method:'GET', url:'/admin/totals/termly/data', params:{country_id: 1}, isArray:true, cache:false}
            //}).getTermlyData({circuit_id : circuit_id});

            var aDistrictsTermlyDataUrl = '/admin/totals/termly/data';
            var aDistrictsCachedTermlyDataUrl = '/admin/totals/termly/data?country_id=1'+JSON.stringify(params);


            if (reloader) {
                districtCache.remove(aDistrictsCachedTermlyDataUrl);
            }

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsCachedTermlyDataUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsTermlyDataUrl , {params : params })
                    .success(function(data){
                        districtCache.put(aDistrictsCachedTermlyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


        return scDistrict;



    }]);
