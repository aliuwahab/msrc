/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Region resource
 *
 */

//Usage
//$resource(url, [paramDefaults], [actions], options);

//action parameters
//action – {string} – The name of action. This name becomes the name of the method on your resource object.
//    method – {string} – HTTP request method. Valid methods are: GET, POST, PUT, DELETE, and JSONP.
//    params – {Object=} – Optional set of pre-bound parameters for this action. If any of the parameter value is a function, it will be executed every time when a param value needs to be obtained for a request (unless the param was overridden).
//url – {string} – action specific url override. The url templating is supported just like for the resource-level urls.
//    isArray – {boolean=} – If true then the returned object for this action is an array, see returns section.
//    cache – {boolean|Cache} – If true, a default $http cache will be used to cache the GET request, otherwise if a cache instance built with $cacheFactory, this cache will be used for caching.
//                                                                                                                                                                                            timeout – {number|Promise} – timeout in milliseconds, or promise that should abort the request when resolved.
//    responseType - {string} - see requestType.
//    interceptor - {Object=} - The interceptor object has two optional methods - response and responseError. Both response and responseError interceptors get called with http response object. See $http interceptors.

//options
//stripTrailingSlashes – {boolean} – If true then the trailing slashes from any calculated URL will be stripped. (Defaults to true.)

schoolMonitor.service('scViewServices',[])
    .factory('Region', function($resource){
        return $resource('/admin/ghana/regions',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/region/create', params:{}},
            retrieveOneRegion:{method:'GET', url:'/admin/region/:id/edit', params:{id : '@id'}},
            editRegion:{method:'POST', url:'/admin/region/:id/update', params:{id : '@id'}},
            deleteRegion:{method:'POST', url:'/admin/region/:id/delete', params:{id : '@id'}}
        });
    })

    .factory('District', function($resource){
        return $resource('/admin/ghana/districts',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/district/create', params:{}},
            createItinerary:{method:'POST', url:'/admin/district/itinerary', params:{}},
            getItinerary:{method:'GET', url:'/admin/district/itinerary/:id', params:{}},
            editAnItinerary:{method:'POST', url:'/admin/district/itinerary/:id/update', params:{id : '@id'}},
            deleteAnItinerary:{method:'POST', url:'/admin/district/itinerary/:id/delete', params:{id : '@id'}},
            retrieveOneDistrict:{method:'GET', url:'/admin/district/:id/edit', params:{id : '@id'}},
            editDistrict:{method:'POST', url:'/admin/district/:id/update', params:{id : '@id'}},
            deleteDistrict:{method:'POST', url:'/admin/district/:id/delete', params:{id : '@id'}}
        });
    })

    .factory('Circuit', function($resource){
        return $resource('/admin/ghana/circuits',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/circuit/create', params:{}},
            retrieveOneCircuit:{method:'GET', url:'/admin/circuit/:id/edit', params:{id : '@id'}},
            editCircuit:{method:'POST', url:'/admin/circuit/:id/update', params:{id : '@id'}},
            deleteCircuit:{method:'POST', url:'/admin/circuit/:id/delete', params:{id : '@id'}}

        });
    })

    .factory('School', function($resource){
        return $resource('/admin/ghana/schools',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/create_school', params:{}},
            retrieveOneSchool : {method : 'GET', url : '/admin/school/:id/edit', params:{id : '@id'}},

            editSchool : {method : 'POST', url : '/admin/school/:id/update', params:{id : '@id'}},

            deleteSchool:{method:'POST', url:'/admin/school/:id/delete', params:{id : '@id'}},

            //<-- START of School Report Card  Methods-->

            allEnrollmentData : {method : 'GET', url : '/admin/school_report_card/enrollment',
                params:{ country_id : 1}, isArray:true,  cache : false},

            allSpecialEnrollmentData : {method : 'GET', url : '/admin/school_report_card/special_enrollment',
                params:{ country_id : 1}, isArray:true, cache : false },

            allAttendanceData : {method : 'GET', url : '/admin/school_report_card/attendance',
                params:{ country_id : 1}, isArray:true,  cache : false},

            allSpecialAttendanceData : {method : 'GET', url : '/admin/school_report_card/special_students_attendance',
                params:{ country_id : 1}, isArray:true,  cache : false},

            allRecordPerformanceData : {method : 'GET', url : '/admin/school_report_card/records_performance',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolTeachersPunctuality_LessonPlan : {method : 'GET', url : '/admin/school_report_card/school/punctuality_lesson_plan',
                cache : false, params:{ country_id : 1}, isArray:true},

            allSchoolTeachersClassManagement : {method : 'GET', url : '/admin/school_report_card/school/teacher_class_management',
                cache : false, params:{ country_id : 1}, isArray:true},

            allSchoolTeachersUnitsCovered : {method : 'GET', url : '/admin/school_report_card/school/units/covered',
                cache : false, params:{ country_id : 1}, isArray:true},

            allSchoolSanitation : {method : 'GET', url : '/admin/school_report_card/sanitation',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolRecreation_Equipment : {method : 'GET', url : '/admin/school_report_card/recreation_equipment',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolSecurity : {method : 'GET', url : '/admin/school_report_card/security',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolStructure : {method : 'GET', url : '/admin/school_report_card/structure',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolFurniture : {method : 'GET', url : '/admin/school_report_card/furniture',
                params:{ country_id : 1}, isArray:true, cache : true} ,

            allSchoolCommunityInvolvement : {method : 'GET', url : '/admin/school_report_card/community_relations',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolMeetingsHeld : {method : 'GET', url : '/admin/school_report_card/school/meetings',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolTextbook : {method : 'GET', url : '/admin/school_report_card/school/textbooks',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolPupilPerformance : {method : 'GET', url : '/admin/school_report_card/pupils/performance',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSupportTypes : {method : 'GET', url : '/admin/school_report_card/school/support_types',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolGrantCapitationPayments : {method : 'GET', url : '/admin/school_report_card/grant_capitation_payments',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolGeneralSituation : {method : 'GET', url : '/admin/school_report_card/school/general_situation',
                params:{ country_id : 1}, isArray:true, cache : true},


            teacherAttendanceAveragePercentage : {method : 'GET', url : '/admin/average/teacher/attendance/percentage',
                params:{ country_id : 1}, isArray:false},




            //END of School Report Card  Methods


            //-- START of  School Review Methods-->
            schoolInclusiveData : {method : 'GET', url : '/admin/school_review/inclusiveness',
                params:{ country_id : 1}, isArray:true},

            schoolEffectiveTeachingData : {method : 'GET', url : '/admin/school_review/effective_teaching',
                params:{ country_id : 1}, isArray:true},

            schoolHealthyData : {method : 'GET', url : '/admin/school_review/healthy_level',
                params:{ country_id : 1}, isArray:true},

            schoolSafeProtectiveData : {method : 'GET', url : '/admin/school_review/safe_protective',
                params:{ country_id : 1}, isArray:true},

            schoolFriendlyData : {method : 'GET', url : '/admin/school_review/friendly_schools',
                params:{ country_id : 1}, isArray:true},

            schoolCommunityInvolvementData : {method : 'GET', url : '/admin/school_review/community_involvement',
                params:{ country_id : 1}, isArray:true},

            //<-- END of  School Review Methods-->


            //<-- START of  School Visit  Methods-->

            allSchoolVisitsData : {method : 'GET', url : '/admin/schools/school_visits',
                params:{ country_id : 1}, isArray:true}
        });
    })

    .factory('Report', function($resource){
        return $resource('/admin',{},{
            totalsWeekly:{method:'GET', url:'/admin/totals/weekly/data', cache : false, params:{country_id : 1 }, isArray: true},
            totalsTermly:{method:'GET', url:'/admin/totals/termly/data', cache : false, params:{country_id : 1 }, isArray: true},

            /*This route has been taken care of in the weekly above*/
            aggregatedTotals:{method:'GET', url:'/admin/retrieve/parametric/aggregate/data', params:{country_id : 1 }, isArray: false}
        });
    });