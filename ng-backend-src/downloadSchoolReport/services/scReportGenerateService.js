/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Global Functions that will need to be called regularly from several places
 *
 */

/*Don't delete. It is used in the app somewhere*/
schoolMonitor.service('scReportGenerator', ['$rootScope',
    'District', 'School','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool',
    'coat_of_arms', 'msrc_datauri_logo', 'scGeneralService', 'DataCollectorHolder',
    '$interval', function($rootScope, District, School, scNotifier, scRegion, scDistrict, scCircuit, scSchool,
                          coat_of_arms, msrc_datauri_logo, scGeneralService, DataCollectorHolder) {

        this.barChart = {};
        this.pieChart = {};

        this.loadStats = function (dataParams) {

            //we use week 7 because that is usually the middle of the term when most schools have finished enrollments"
            // dataParams.week_number = 1;
            School.allEnrollmentData(dataParams).$promise
                .then(function (pupilEnrollmentDataLoaded) {
                    $rootScope.$broadcast('pupilEnrollmentDataLoaded', pupilEnrollmentDataLoaded[0].school_enrolment)
                });

            School.allSpecialEnrollmentData(dataParams).$promise
                .then(function (specialPupilEnrollmentDataLoaded) {
                    $rootScope.$broadcast('specialPupilEnrollmentDataLoaded',
                        specialPupilEnrollmentDataLoaded[0].special_school_enrolment)
                });

            delete dataParams.week_number;

            School.allAttendanceData(dataParams).$promise
                .then(function (allPupilAttendanceDataLoaded) {
                    $rootScope.$broadcast('allPupilAttendanceDataLoaded',
                        allPupilAttendanceDataLoaded[0].school_attendance)
                });

            School.allSchoolPupilPerformance(dataParams)
                .$promise.then(function (pupilPerformanceDataLoaded) {
                $rootScope.$broadcast('pupilPerformanceDataLoaded', pupilPerformanceDataLoaded[0].pupils_performance)

            });



            School.allSchoolTeachersPunctuality_LessonPlan(dataParams).$promise
                .then(function (teacherPunctualityLessonPlansDataLoaded) {
                    $rootScope.$broadcast('teacherPunctualityLessonPlansDataLoaded', teacherPunctualityLessonPlansDataLoaded)
                });

            School.allSchoolTeachersUnitsCovered(dataParams).$promise
                .then(function (teacherUnitsCoveredDataLoaded) {
                    $rootScope.$broadcast('teacherUnitsCoveredDataLoaded', teacherUnitsCoveredDataLoaded)
                });


            School.allRecordPerformanceData(dataParams).$promise
                .then(function (recordBooksDataLoaded) {
                    $rootScope.$broadcast('recordBooksDataLoaded', recordBooksDataLoaded)
                });


            School.allSchoolGrantCapitationPayments(dataParams).$promise
                .then(function (grantCapitationPaymentsDataLoaded) {
                    $rootScope.$broadcast('grantCapitationPaymentsDataLoaded', grantCapitationPaymentsDataLoaded[0].school_grant)
                });


            School.allSupportTypes(dataParams).$promise
                .then(function (supportTypesDataLoaded) {
                    $rootScope.$broadcast('supportTypesDataLoaded', supportTypesDataLoaded[0].school_support_type)
                });


            School.allSchoolFurniture(dataParams).$promise
                .then(function(furnitureDataLoaded){
                    $rootScope.$broadcast('furnitureDataLoaded', furnitureDataLoaded)
                });


            School.allSchoolStructure(dataParams).$promise
                .then(function (structureDataLoaded) {
                    $rootScope.$broadcast('structureDataLoaded', structureDataLoaded)
                });


            School.allSchoolSanitation(dataParams).$promise
                .then(function (sanitationDataLoaded) {
                    $rootScope.$broadcast('sanitationDataLoaded', sanitationDataLoaded)
                });


            School.allSchoolRecreation_Equipment(dataParams).$promise
                .then(function (recreationDataLoaded) {
                    $rootScope.$broadcast('recreationDataLoaded', recreationDataLoaded);
                });


            School.allSchoolSecurity(dataParams).$promise
                .then(function (securityDataLoaded) {
                    $rootScope.$broadcast('securityDataLoaded', securityDataLoaded);
                });


            School.allSchoolMeetingsHeld(dataParams).$promise
                .then(function (meetingDataLoaded) {
                    $rootScope.$broadcast('meetingDataLoaded', meetingDataLoaded[0].school_meetings);
                });


            School.allSchoolCommunityInvolvement(dataParams).$promise
                .then(function (communityDataLoaded) {
                    $rootScope.$broadcast('communityDataLoaded', communityDataLoaded);
                })
        };

        function centreTextInPage (text, y, doc){
            var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
            var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
            doc.text(textOffset, y, text);
        }
        // function centreTextInPage (text, y, doc){
        //     var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
        //     var textOffset = (doc.internal.pageSize.getWidth() - textWidth) / 2;
        //     doc.text(textOffset, y, text);
        // }



        var dummy = [{
            name : "Pupil Table",
            status : 'Not Available',
            term : "2nd"},

            {
                name : "Pupil Chairs",
                status : 'Available',
                term : "2nd"
            },
            {
                name : "Doors",
                status : 'Available, not func',
                term : "2nd"
            }];

        function createTableAndPopulate(doc, loopArray, x, y) {

            // doc.rect(x, y, 15, 10);
            // doc.text("Term " , x + 2, y + 6);

            doc.rect(x, y,  41, 10);
            doc.text("Item", x + 5, y + 6);

            doc.rect(x + 41, y, 37, 10);
            doc.text("Status", x + 55, y + 6 );



            for (var i = 0; i < loopArray.length; i++) {
                var obj = loopArray[i];

                // doc.rect(x, y + ((i+1) * 10), 15, 10);
                // doc.text($rootScope.one_school_terms[obj.term].number , x + 2, y + 5 + 10*(i+1));

                doc.rect(x, y + ((i+1) * 10), 41, 10);
                doc.text(obj.name, x + 5, y + 5 + 10*(i+1));


                if (obj.status === "Available" || obj.status === "Good" || obj.status === "Yes" || obj.status === "Adequate") {
                    doc.setFillColor(1, 166, 89); //green
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10, 'FD');
                    doc.setTextColor(255, 255, 255);

                }else if (obj.status === "Available, not functional" || obj.status === "Fair" || obj.status === "Inadequate") {
                    doc.setFillColor(243, 156, 18); //yellow
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10, 'FD');
                    doc.setTextColor(255, 255, 255);

                }else if (obj.status === "Not available" || obj.status === "Poor" || obj.status === "No"){
                    doc.setFillColor(245, 105, 84); //red
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10, 'FD');
                    doc.setTextColor(255, 255, 255);

                }else{
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10);
                }

                if (obj.status) {
                    doc.text(obj.status, x + 44, y + 5+ 10*(i+1));
                }else console.log("no status obj.status ", obj);

                doc.setFillColor(255, 255, 255); //yellow
                doc.setTextColor(0, 0, 0);

            }



        }

        /*This is for the school level*/
        this.downloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            /*
            * // Filled square
                doc.rect(40, 20, 10, 10, 'F');

                // Empty red square
                doc.setDrawColor(255,0,0);
                doc.rect(60, 20, 10, 10);
            * */

            doc.addImage(msrc_datauri_logo, 'PNG', 20, 8, 24, 15);
            doc.addImage (coat_of_arms, 'PNG', 170, 8, 19, 17);
            doc.setFontSize(11);
            centreTextInPage("Republic of Ghana", 12, doc);
            centreTextInPage("Ghana Education Service", 18, doc);
            centreTextInPage(pdfDocVariables.term_in_scope + " " + pdfDocVariables.title, 24, doc);
            doc.setFontSize(6);
            doc.text("Ministry of Education", 170, 27);


            doc.setFontSize(8);
            doc.setFontType("bold");
            doc.text("School", 35, 32);
            doc.text(pdfDocVariables.entity_name, 50, 32);
            doc.setFontType("normal");
            // doc.text("Circuit", 35, 36);
            // doc.text(pdfDocVariables.entity.circuitName, 50, 36);
            // doc.text("District", 35, 40);
            // doc.text(pdfDocVariables.entity.districtName, 50, 40);
            // doc.text("Region", 35, 44);
            // doc.text(pdfDocVariables.entity.regionName, 50, 44);

            doc.text("EMIS Number", 125, 32);
            doc.text(pdfDocVariables.entity.ges_code, 150, 32);
            // doc.text("Head Teacher", 125, 36);
            // doc.text(DataCollectorHolder.lookupDataCollector[pdfDocVariables.entity.headteacher_id].name, 150, 36);
            // doc.text("Phone Number", 125, 40);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 40);
            // doc.text("Email Address", 125, 44);
            // doc.text(pdfDocVariables.entity.email, 150, 44);
            // doc.rect(15, 5, 180, 42);

            doc.setFontType("bold");
            centreTextInPage("Pupil Statistics", 60, doc);

            doc.text("Total Number Enrolled", 30, 68);
            doc.text(pdfDocVariables.top_box_left, 70, 68);

            doc.setFontType("normal");
            doc.text("Boys", 30, 72);
            doc.text(pdfDocVariables.top_box_up_middle_up, 70, 72);
            doc.text("Boys with Special Need", 30, 76);
            doc.text(pdfDocVariables.top_box_down_middle_up, 70, 76);

            doc.text("Girls", 30, 80);
            doc.text(pdfDocVariables.top_box_up_middle_down, 70, 80);
            doc.text("Girls with Special Need", 30, 84);
            doc.text(pdfDocVariables.top_box_down_middle_down, 70, 84);

            doc.setFontSize(25);
            doc.text(pdfDocVariables.top_box_down_left, 26, 95);
            doc.setFontSize(8);
            doc.text("Student Attendance Rate", 45, 95);

            doc.addImage(this.barChart['enrollmentBarChart'].toBase64Image(), 'PNG', 100, 65, 80, 56);


            centreTextInPage('Pupils Average Performance', 126, doc);
            centreTextInPage('Ghanaian Language', 130, doc);

            var classArray= ['KG1', 'KG2', "P1", "P2", "P3", "P4", "P5", "P6", "JHS1", "JHS2", "JHS3"];

            var pupilPerf_X = 18; //this should shift the table right or left
            if (_.size(pdfDocVariables.gh_lang_performance_array) <= 10) {
                pupilPerf_X = 30; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 8) {
                pupilPerf_X = 45; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 6) {
                pupilPerf_X = 57; //6/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 4) {
                pupilPerf_X = 80; //3/11 classes showing
            }

            var pupilPerf_y = 134; //this should shift the table up or down

            var ghlpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var ghl = 0; ghl < 11; ghl ++){

                if (pdfDocVariables.gh_lang_performance_array[classArray[ghl]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X+(15.5 * ghlpusher), pupilPerf_y, 15.5, 20);
                    doc.text(classArray[ghl], (pupilPerf_X + 4)+(15.5 * ghlpusher), (pupilPerf_y + 7));
                    doc.line(pupilPerf_X+(15.5 * ghlpusher), (pupilPerf_y + 10), (pupilPerf_X + 17)+(15.3 * ghlpusher), (pupilPerf_y + 10));// horizontal line
                    doc.text( (pdfDocVariables.gh_lang_performance_array[classArray[ghl]].average_score || 0) + '%', (pupilPerf_X + 4)+(15.4 * ghlpusher), (pupilPerf_y + 16));
                    ghlpusher++;
                }
            }

            centreTextInPage('English Language', (pupilPerf_y + 27), doc);

            var elpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var el = 0; el < 11; el ++){
                if (pdfDocVariables.english_lang_performance_array[classArray[el]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X+(15.5 * elpusher), (pupilPerf_y + 30), 15.5, 20);
                    doc.text(classArray[el], (pupilPerf_X + 4)+(15.5 * elpusher), (pupilPerf_y + 37));
                    doc.line(pupilPerf_X+(15.5 * elpusher), (pupilPerf_y + 40), (pupilPerf_X + 17)+(15.3 * elpusher), (pupilPerf_y + 40));// horizontal line
                    doc.text( (pdfDocVariables.english_lang_performance_array[classArray[el]].average_score || 0) + '%', (pupilPerf_X + 4)+(15.4 * elpusher), (pupilPerf_y + 46));
                    elpusher++;
                }
            }

            centreTextInPage('Mathematics', (pupilPerf_y + 57), doc);

            var mtmpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var mtm = 0; mtm < 11; mtm ++){
                if (pdfDocVariables.maths_lang_performance_array[classArray[mtm]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X+(15.5 * mtmpusher), (pupilPerf_y + 60), 15.5, 20);
                    doc.text(classArray[mtm], (pupilPerf_X + 4)+(15.5 * mtmpusher), (pupilPerf_y + 67));
                    doc.line(pupilPerf_X+(15.5 * mtmpusher), (pupilPerf_y + 70), (pupilPerf_X + 17)+(15.3 * mtmpusher), (pupilPerf_y + 70));// horizontal line
                    doc.text( (pdfDocVariables.maths_lang_performance_array[classArray[mtm]].average_score || 0) + '%',
                        (pupilPerf_X + 4)+(15.4 * mtmpusher), (pupilPerf_y + 76));
                    mtmpusher++;
                }
            }

            doc.rect(15, 55, 180, 242);


            doc.addImage(this.barChart['performanceBarChart'].toBase64Image(), 'PNG', 20, 215, 170, 80);


            doc.addPage();

            doc.setFontType("bold");
            centreTextInPage("Teacher Statistics", 20, doc);
            doc.setFontType("normal");

            /*Teacher To Pupil Ratio*/
            doc.rect(20, 25, 53, 30);
            doc.addImage(scGeneralService.report_image_uris.teacher_to_pupil_image, 'PNG', 23, 29, 17, 17);
            doc.setFontSize(13);
            doc.text("Pupil \nTeacher Ratio", 42, 34);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_pupil_ratio || 0, 42, 50);
            /*Teacher To Pupil Ratio*/

            /*Teacher Attendance Ratio*/
            var attendance_ratio = scGeneralService.report_image_uris.attendance_ratio;
            doc.rect(78, 25, 55, 30);
            doc.addImage(attendance_ratio, 'PNG', 80, 29, 17, 17);
            doc.setFontSize(14);
            doc.text("Teacher \nAttendance\nRate", 98, 34);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_attendance_ratio || 0, 112, 50);

            /*Attendance Chart*/
            doc.addImage(this.pieChart['teacherAttendance'].toBase64Image(), 'PNG', 136, 15, 60, 42);
            doc.setFontSize(9);
            doc.text("Teacher Attendance Rate", 148, 62);
            /*Teacher Attendance Ratio*/


            /*Percentage Trained Teachers*/
            doc.addImage(scGeneralService.report_image_uris.teachers_trained, 'PNG', 23, 69, 17, 17);
            doc.rect(20, 66, 53, 30);
            doc.setFontSize(13);
            doc.text("Number of", 44, 71);
            doc.setFontSize(15);
            doc.text("Trained\nTeachers", 44, 77);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.percentage_trained_teachers, 44, 92);
            /*Percentage Trained Teachers*/



            /*Adequately Marked Exercises*/
            var adequately_marked_exercises = scGeneralService.report_image_uris.adequately_marked_exercises;
            doc.addImage(adequately_marked_exercises, 'PNG', 82, 69, 17, 17);
            doc.rect(78, 66, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.adequate_marked_title, 107, 71);
            doc.setFontSize(15);
            doc.text("Marked", 107, 77);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.adequately_marked_exercises, 104, 92);
            /*Adequately Marked Exercises*/

            /*Punctuality Percentage*/
            var punctuality_percentage = scGeneralService.report_image_uris.punctuality_percentage;
            doc.rect(138, 66, 53, 30);
            doc.addImage(punctuality_percentage, 'PNG', 140, 72, 17, 17);
            doc.setFontSize(14);
            doc.text(pdfDocVariables.punctuality_title, 155, 71);
            doc.text("Punctuality\nAbove 50%", 160, 77);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_punctuality, 164, 92);
            /*Punctuality Percentage*/


            /*Number of GH Language Units Covered*/
            // doc.addImage(scGeneralService.report_image_uris.teachers_trained, 'PNG', 23, 100, 17, 17);
            doc.setFontSize(30);
            doc.setFontType("bold");
            doc.text("GH", 23, 115);
            doc.rect(20, 100, 53, 30);
            doc.setFontSize(12);
            doc.setFontType("normal");
            doc.text("Number of\nGhanaian Lng.\nUnits Covered", 42, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_unit_covered_gh_lang, 45, 126);
            /*Number of GH Language Units Covered*/



            /*Number of English Units Covered*/
            // adequately_marked_exercises = scGeneralService.report_image_uris.adequately_marked_exercises;
            // doc.addImage(adequately_marked_exercises, 'PNG', 82, 100, 17, 17);
            doc.setFontSize(30);
            doc.setFontType("bold");
            doc.text("EL", 83, 115);
            doc.rect(78, 100, 53, 30);
            doc.setFontSize(12);
            doc.setFontType("normal");
            doc.text("Number of \nEnglish Lng.\nUnits Covered", 100, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_unit_covered_english, 106, 126);
            /*Number of English Language Units Covered*/


            /*Number of Mathematics Units Covered*/
            // punctuality_percentage = scGeneralService.report_image_uris.punctuality_percentage;
            // doc.addImage(punctuality_percentage, 'PNG', 140, 100, 17, 17);
            doc.setFontSize(30);
            doc.setFontType("bold");
            doc.text("MA", 141, 115);
            doc.rect(138, 100, 53, 30);
            doc.setFontSize(12);
            doc.setFontType("normal");
            doc.text("Number of\nMathematics\nUnits Covered", 160, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_unit_covered_maths, 165, 126);
            /*Number of Mathematics Units Covered*/



            doc.rect(15, 15, 180, 125); /*Main teacher rectangle*/



            doc.setFontType("bold");
            doc.setFontSize(8);
            centreTextInPage("School Status and Statistics", 145, doc);
            doc.setFontType("normal");

            /*Meetings Held*/
            var hold_meetings = scGeneralService.report_image_uris.hold_meetings;
            doc.rect(20, 147, 53, 30);
            doc.addImage(hold_meetings, 'PNG', 23, 150, 17, 17);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.meetings_title, 41, 152);
            doc.setFontSize(15);
            doc.text(" Meetings\n Held", 41, 158);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.meetings, 45, 173);
            /*Meetings Held*/


            /*School Support*/
            var school_support = scGeneralService.report_image_uris.school_support;
            doc.rect(78, 147, 53, 30);
            doc.addImage(school_support, 'PNG', 82, 153, 19, 19);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_received_or_any_type_of_support, 104, 152);
            doc.setFontSize(15);
            doc.text("support\n(kind/cash)", 104, 158);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.support_received, 104, 173);
            /*School Support*/


            /*Grant Received*/
            var received_grant = scGeneralService.report_image_uris.received_grant;
            doc.rect(138, 147, 53, 30);
            doc.addImage(received_grant, 'PNG', 141, 152, 19, 19);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_or_amount_of_title, 162, 152);
            doc.setFontSize(15);
            doc.text("Grants\nReceived", 164, 158);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.grant_received, 164, 173);
            /*Grant Received*/


            // doc.line(20, 246, 191, 246);// horizontal line
            // doc.line(20, 248, 191, 248);// horizontal line

            doc.rect(15, 140, 180, 40); /*Main Termly vectors rectangle*/



            doc.addImage(scGeneralService.report_image_uris.adequate_furniture, 'PNG', 21, 190, 19, 19);
            doc.text("Furniture", 42, 202);
            doc.setFontSize(8);
            doc.text("Were the following items adequate?", 42, 206);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.furnitureItems, 20, 208);


            doc.addImage(scGeneralService.report_image_uris.security_facilities, 'PNG', 111, 189, 17, 17);
            doc.setFontSize(15);
            doc.text("Security", 130, 201);
            doc.setFontSize(8);
            doc.text("Were the following items available?", 130, 205);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.securityItems, 110, 208);




            doc.addPage();

            doc.addImage(scGeneralService.report_image_uris.good_structure, 'PNG', 21, 10, 17, 17);
            doc.setFontSize(15);
            doc.text("School Structure", 40, 21);
            doc.setFontSize(8);
            doc.text("Were the following items in a good state?", 40, 25);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.structureItems, 20, 29);



            doc.addImage(scGeneralService.report_image_uris.sanitation_facilities, 'PNG', 111, 10, 17, 17);
            doc.setFontSize(15);
            doc.text("Sanitation", 130, 21);
            doc.setFontSize(8);
            doc.text("Were the following items available?", 130, 25);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.sanitationItems, 110, 29);



            doc.addImage(scGeneralService.report_image_uris.recreational_facilities, 'PNG', 21, 128, 16, 16);
            doc.setFontSize(13);
            doc.text("Sports/Recreational Facilities", 39, 137);
            doc.setFontSize(8);
            doc.text("Were the following items available?", 39, 141);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.recreationItems, 20, 146);



            doc.addImage(scGeneralService.report_image_uris.with_record_books, 'PNG', 111, 128, 16, 16);
            doc.setFontSize(15);
            doc.text("Record Books", 130, 137);
            doc.setFontSize(8);
            doc.text("Were the following books available?", 130, 141);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.books, 110, 146);







            doc.save(pdfDocVariables.pdf_export_name)



            //
            //
            //
            // doc.line(20, 53, 191, 53);// horizontal line
            // doc.line(20, 55, 191, 55);// horizontal line
            //
            // var active_community = scGeneralService.report_image_uris.active_community;
            // doc.addImage(active_community, 'PNG', 114, 65, 19, 19);
            // doc.rect(110, 58, 80, 30);
            // doc.setFontSize(15);
            // doc.text(pdfDocVariables.community_title, 113, 63);
            // doc.setFontSize(26);
            // doc.text(pdfDocVariables.community_participation, 150, 80);
            // // doc.save('a4.pdf');




        };


        function commonModelPDF(doc, pdfDocVariables) {
            doc.addImage(msrc_datauri_logo, 'PNG', 20, 8, 24, 15);
            doc.addImage (coat_of_arms, 'PNG', 170, 8, 19, 17);
            doc.setFontSize(11);
            centreTextInPage("Republic of Ghana", 12, doc);
            centreTextInPage("Ghana Education Service", 18, doc);
            centreTextInPage(pdfDocVariables.term_in_scope + " " + pdfDocVariables.title, 24, doc);
            doc.setFontSize(6);
            doc.text("Ministry of Education", 170, 27);

            doc.setFontType("bold");
            centreTextInPage("Pupil Statistics", 60, doc);

            doc.text("Total Number Enrolled", 30, 68);
            doc.text(pdfDocVariables.top_box_left, 70, 68);

            doc.setFontType("normal");
            doc.text("Boys", 30, 72);
            doc.text(pdfDocVariables.top_box_up_middle_up, 70, 72);
            doc.text("Boys with Special Need", 30, 76);
            doc.text(pdfDocVariables.top_box_down_middle_up, 70, 76);

            doc.text("Girls", 30, 80);
            doc.text(pdfDocVariables.top_box_up_middle_down, 70, 80);
            doc.text("Girls with Special Need", 30, 84);
            doc.text(pdfDocVariables.top_box_down_middle_down, 70, 84);

            doc.setFontSize(25);
            doc.text(pdfDocVariables.top_box_down_left, 26, 95);
            doc.setFontSize(8);
            doc.text("Student Attendance Rate", 45, 95);

            doc.addImage(this.barChart['enrollmentBarChart'].toBase64Image(), 'PNG', 100, 65, 80, 56);


            centreTextInPage('Pupils Average Performance', 126, doc);
            centreTextInPage('Ghanaian Language', 130, doc);

            var classArray = ['KG1', 'KG2', "P1", "P2", "P3", "P4", "P5", "P6", "JHS1", "JHS2", "JHS3"];

            //this should shift the table right or left
            var pupilPerf_X = 18; //if all 11 classes are showing
            if (_.size(pdfDocVariables.gh_lang_performance_array) <= 10) {
                pupilPerf_X = 30; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 8) {
                pupilPerf_X = 45; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 6) {
                pupilPerf_X = 57; //6/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 4) {
                pupilPerf_X = 80; //3/11 classes showing
            }

            var pupilPerf_y = 134; //this should shift the table up or down

            var ghlpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var ghl = 0; ghl < 11; ghl++) {

                if (pdfDocVariables.gh_lang_performance_array[classArray[ghl]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X + (15.5 * ghlpusher), pupilPerf_y, 15.5, 20);
                    doc.text(classArray[ghl], (pupilPerf_X + 4) + (15.5 * ghlpusher), (pupilPerf_y + 7));
                    doc.line(pupilPerf_X + (15.5 * ghlpusher), (pupilPerf_y + 10), (pupilPerf_X + 17) + (15.3 * ghlpusher), (pupilPerf_y + 10));// horizontal line
                    doc.text((pdfDocVariables.gh_lang_performance_array[classArray[ghl]].average_score || 0) + '%', (pupilPerf_X + 4) + (15.4 * ghlpusher), (pupilPerf_y + 16));
                    ghlpusher++;
                }
            }

            centreTextInPage('English Language', (pupilPerf_y + 27), doc);

            var elpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var el = 0; el < 11; el++) {
                if (pdfDocVariables.english_lang_performance_array[classArray[el]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X + (15.5 * elpusher), (pupilPerf_y + 30), 15.5, 20);
                    doc.text(classArray[el], (pupilPerf_X + 4) + (15.5 * elpusher), (pupilPerf_y + 37));
                    doc.line(pupilPerf_X + (15.5 * elpusher), (pupilPerf_y + 40), (pupilPerf_X + 17) + (15.3 * elpusher), (pupilPerf_y + 40));// horizontal line
                    doc.text((pdfDocVariables.english_lang_performance_array[classArray[el]].average_score || 0) + '%', (pupilPerf_X + 4) + (15.4 * elpusher), (pupilPerf_y + 46));
                    elpusher++;
                }
            }

            centreTextInPage('Mathematics', (pupilPerf_y + 57), doc);

            var mtmpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var mtm = 0; mtm < 11; mtm++) {
                if (pdfDocVariables.maths_lang_performance_array[classArray[mtm]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X + (15.5 * mtmpusher), (pupilPerf_y + 60), 15.5, 20);
                    doc.text(classArray[mtm], (pupilPerf_X + 4) + (15.5 * mtmpusher), (pupilPerf_y + 67));
                    doc.line(pupilPerf_X + (15.5 * mtmpusher), (pupilPerf_y + 70), (pupilPerf_X + 17) + (15.3 * mtmpusher), (pupilPerf_y + 70));// horizontal line
                    doc.text((pdfDocVariables.maths_lang_performance_array[classArray[mtm]].average_score || 0) + '%',
                        (pupilPerf_X + 4) + (15.4 * mtmpusher), (pupilPerf_y + 76));
                    mtmpusher++;
                }
            }

            doc.rect(15, 55, 180, 242);


            doc.addImage(this.barChart['performanceBarChart'].toBase64Image(), 'PNG', 20, 215, 170, 80);

            doc.addPage();

            var teacher_to_pupil_image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABQ7SURBVHic7Z17dBRVnse/t7ob8uBhAHmTLiQgFCSBpDuiCEkQ2fGBuoy6jswJ+JjVUVcdHXFcZfF1xvE5HN3d0YNzHOKwuo7r8YXKgCQBw0C6E0OAJiEkdDo8EgLkJST06+4f/aCqUtXp7lR1d0h9zsk5Xbeqfvemf7/+3Xt/91e3CKUUfAghJH12zv2EkFsA5AKYBHU4CaCSUvqlo7bqfSpuSISwnOlaAG8CyFOkdZc+FQCeJPzvPX12zmSGYTYDKIhxY0q9Xu8qR23ViWgFsJxpLzTlR0oFE/hECCFxUj4AFDAMs5kQQgYgQ1N+5OQFDSB9ds79iI/yAxT426ARQ/SBD/4+P8ioJNS8/guDLpclc9WouNJODz71kcvT1YssURs2KiHftvsrJcRccnDXrBAcM7zPufwTaiofAHJZMvf1Xxh04mK16tOQhm8AgtG+msoPUYdaMw4NGZj+L9G4lNH3f4kP03qnIhVaXximiBwNZdA8wBAnbA9wqbFzT2XNk8+9qjt3vkfRsU5qSvLBN19+2rNkYW6W1Pl41SvHkPUAaigBAM6d75n75HOvimc3ca9XjrA9wKXWd6uhhHBkx6teOYasB9DwwfcAJ8Gbh1fa6UG1YwGVdnoQAL+Ok2rWF4qBejjxLEkccYtVvZHC9wCV/BNPfeTy+BWkCoFQsLhYrfo0pAl6AErpl4SQmwPHXb3IeuADV/DCy1JJ9at36g0Br1BeT2v+/W8u3bkLiNZL9LmPUvpllLI0oiToARy1Ve8DKJW7sOMcnf/gBy62voU2AsAAlS9Fqb8NGjGE7wFo+uycVaFyAiiQes9Gl3fb74adV1r5Xq931UCzggaCUpHOAHKrkeKxgdL1RopgGuiorTpBCFkaKiWs142Me993/QDg2gHWrWhKmEZ09IkD+BWxEbx1eeNc0weEYk3guL6F9lG+3WYdSDZPzElNST6o1pw8NSVZPLuJe71yhBUHcCfjYQCqzQjiwZsvP+3xf2GKEgjJJlq9cpBwPS+bmTsHHmIBkCp1Pt4egOVMgn9ksGcEiccKSv0/oTKCQmLfX3mIgjykSCs0EoaIVgObbJZilsvNB8i90VSWkZ093u0ymCmlJoZhcimlJgAghFi9Xm8lIcSqN7gsR/btOxWNfI3IiXg52NM9/BHdSGcegHnh3kMKC/XG1u5nAMM6AAZCCPhdD6V0BSFkBQC4XQYXy5leapow8hVaUuKOtH0akRGxATQ37+6ZfqXpDqqDFRfHA4fkrmczc+ewbmyihJjDrMIA4EW2pWsFm5m72r6/Ula2EoQbs4+UwTIGiWo18GidtZZhyFIQlFCgmVDm11LXGTlzETykKgLlB6GEmOEhVUbOXBRNGzXCI+qMoMYDlgoAS+XOs5m5cwjIewCS+OVEp7NfbsxoSps6PSVtCpsOAO3H7Y6zzY3nTjsaWOrxsLzLkwjoe2xmrkVtTxAros0I6s9TRZsRpEpKGCks1PvcvkD5dPTk9J1Zy1fmMXo9y79+4qx5EybOmgev291T8/fPyjpPOJYACEwrk4gbm0hh4TWXwphA5Yygg5btn0R0nyoGYGztfkbk9unsghutEzK4/FD3MXp98vwb78xvPWKz1JZ+Y4LfCCghZt8gEi8p3dZY99WXfEZQRnb2eADr+GWjJ6fvnJDBhT0OmJDBmUdPTt8pKl7nl62hIIp7ALfLYIZvJA/A1+dnLV8Z8ZO7WctX5v3w4Tt23pjA4Je9RZmWJgaJlBGkCIHgToBx6TPsjF6fHKkcRq9PHpc+wx5KtsbAUdwD+CN8weMx066QXDsIhzHTrkhtO3pYIHtgrZNn08ef73574+YxAPDC04/8dPPy/JDG1t+ofLDEARQ3APGvNDDViwbxvWp5gMee+X3ptrJ/FASO1z7/hrurq3vP3bffvFCN+hKJIftkEAA4na4LP1/zWGWDvblAdEr/8lvvmQCobgTxzghSfAxACLHyj9uP2x3RyhLfK5Y9ENo7us4uWVFU12BvvkbmEv3Lb71n+p9Pv96jVJ2JiOIewL+qF+wgzzY3nps4K+x1IwFnmxvPiWUPsHkAgIajDvvK1Y8Rl9stipqRDoCOwMXvRdYTRNvHD8qMoEgQ/0pPOxpYr9vdE6kcr9vdc9rRwIaSHQ2l5ZZ9t/zykdEut9vIL2cYnd18+5rOjKuXWgDwM2sU9QSDNiMoXPxr/sfAiwWMnpxeNv/GO0NGAcVUf/NJWecJB/8el97gmtpfrsD5jpOy/9D7H35a/tafNpkBCCbf+uFJNXl33DfNkJScBgDHDlT+o2FPSR4A/sOW7tee/211f7MDpUi4jKBw8StIELLtPOFY0nrEZglXRusRm8W/HsDnpYEkijyx7tXSt/60aRFEyk9NG1d+9apfzw4oHwCmzsu9esZVBXsh8gQvvv7fEcczEh1VZgFNE0a+wrZ0reCtB5Da0m9MJw8fKPMvBkl+kf7FoArRYhAIpRb7xFGvRNMWl8vtvP2exy31jU0F4nOXT59Vyl13S59yAKDUG5ZrlIsHDNk4AADQkhI3m5m7Gh5U4eJyMOk84cj/4cN3+lsOFncVvVSP1dGsBHZ0drXfeNeDjo7O7kWiU87p5iWW9Oy8Aqn7mmsqyhsrdi6EqAv4j6ceingsk+ioFgew7688ZOTMDxBQQU4A9XjYU4117KnGOv7lE2TE9FKQB5r2WyPOBTjadMxxW9GjXpfLlS04QUj7vOtvc4xNnyE2CgDyyn/uiQesNy/Pv+QCQ6ruD9BksxRDR3MIpWH3/wEIpRboaE6TzVIc6b0/7Knaf/PdD41wuVysQCajazKtXN05Nn1GttR9oZR/qUYFFZ8FSFYSTArFOvBmBzK4AESVFMpypmn515iLy3ZbrgYwnH/OP9KfakhKHiN1b6yUH+89gsRjlpgYQAC10sJZzrQYwKMAboNEt5Zy2Zjduf+8OpfR6Yb3uRmx/eWbl92paiDIsv2TkLLFBhDTtQC/YrdAgTV9ljMlAbgbPsVLunQAGMfOLJ277NZ88GYVfGLt9hMtI2jQLQaxnGkagIcA/ArAWNkLCTl7hXnxoWlZ0iN9YGj2+WIGjQGwnGkJLrp52e3QGL3+8LRMU2t69kITo9dLjvSByJUf6fMD4cYBiselRCRXTNHp8wO6P6ENgOVMyfC5+X9DCDcPwJM0crQlY2Fh0lhjxnwAs0LJ1X75F4naAFjOVAjgZQByy6nqQ0j7mClszcxFy2YmjRwdluI05QuJygD8o+5vIZpqxQqRmw97kUlTfl8iNgCWM80G8Dlir/yI3Dwft/NCV93O76pO2+sXI0rlqxXbH2gfPlAiMgCWM00A8A0AyWCKGhBCWtOmsLWRuPkAbueFrsM//L2qrbFuAfpufDWkf/kBwjYAljOlAPgawHR++cSZc0uvzL+hQOF28ZkA+bUCSXyK31bV1lgrpXggjsoflBlBLGfSAfgYgCAZYtT4STtVVn5EuJ0Xumw7vi4tL36HtjXWFgAYLb6GENL+4u8eqYzXL39QZgSxnOm/4Au+BBmeOtJy1V3/mkMIiXiLcqVxOy9015dvqzzVULsAEkoHAEJIx43XL6le/9RDuSNSU0bGuIkJQWvbmdbCW9fwvenJfrsAljOthUj5OsOwQ+Y77uXirfyLiq+bD9ACqWsIIR03LFtc/fzah3NHpKZIXjNU2F1R7YCwO7WGNACWM/0LgD/wywjDHM+7496xOr0hqid+yt5/I+T5/Pt/268Mn+K3V55qqA2l+M6fXXftj8+vfThn5IhUyWuGGh99tqVXVCRvAP65/iYIFlFIR86tv7wwLGXEFFVa2A/hKv6fli768YWnH9EUz2PHrr3VBw7Vizf4lDYAmbm+c97y25pGjB0vGZI91VhbWVv6zXhCiIu77pZuuaSLaDltr//x4PdfsqDSigfQCWDDnq0f/UZTvJAdu/ZWP/rM7ydBuCK6F8DWPgYgM9enM64qsI5NnyEZ9u1sPV57aMfXHIBkCsD2/ZeNi+/5jXL/AYBDO75OA6VpEqc6AWwAsMFus3ac7zi5XtGKBymtbWdad1dUOz76bEuv/5fPV34PgNV2m9UjMIAQc/2yqZmmAqmKnD3nz+zb8r+jAAQzfb0ezxVyDQv08eKxQH99v9cr2DsIECle7j61dgEbBISKnzxrt1nrAF4gSHauf/mkXXJzfer1uq3/90Ez9XrnK9LkyGBDKV5Dkh4Az8L3wwEgjAS+DUDwcxmeOsIy/5a7ZVf7ar77tNzV2xPREz9KIaf8lMsmiTN/tG3ofeyFz+0L0rH1QKi5/n2yc33Hvr3lHcJHtzQSi5MArLy/rXabtU+kUM9ypp8jwrl+Z+vx2qOWXTkqNFpx4r2LeaLDAHgDwhFip3+uL7kjl9SgD76+RWMQwkD0sKRxwcKaEWPHS47ieYO+yfzyK/Lyf1SxjRoqwgD4jl/QfqxJ9mL/oE8w4k+bYiyblmWOX1qYxoBgAPyNX9B9umUOpbTPYOHYfutu8aDPkJTyY+bPbh/oy6M04ggD4HsAZwMFlNJxZ5qO1IgvbLTsEsT/CcMcN9++xhjvFUGNgcHYbVYXfHH/IM37Kn7qeykVJI9MmZvbYEhKiVlqmIY6BJQq2GJaqhu4bHJ6I/+4tf7AKJXbphEDAgawA/10AxlXFRrBi6q5enuyz3ecsceikRrqwQBAON1AStpY1pCUvI9XRBr2lMhPGTQGBfx+vd/ZQPr8hYKAz9njTfO8Xk98t7rUGBB8A+h3NjCZm58LQs7g4kVjT9iqFdm8USM+BFcD7Tari+VMnwMIvhOweV/FT+PYmcGLGUY3LG2K8UD7MXswHuCo3pM8dV7km3iHk/unoT7i5wL67Qa0weClhdgA+u0GtMHgpYXAACRnAzUV3YHPbueF7kMlW8pcvb0s/5r2401XqtlIDfWQygr+BLxxQHdbC+e+0NtRv/v7facaa7NAaZ8kEEqpRORQYzAgZQCBoNAYwNcNlH/4n+cByGX/nAXwAHzdh8Ygo8/DoVLdAACpjWzOwrfv33S7zbpDhbZpxAC5J4ME3YCIswD+COBtu83apUqrNGKGnAHsANAMYBqvbMgpXmZjyxRQ2CmBHUAJ9M7ippqa9nDkGbOy0uAeVgSgkFCwIGABQGF55yPZeFP28XD/42F/hW/O/wX6UTzLmQSClA70iB8kUTPZM8KtbXspwcfDnN619fVVbVIXzJyZc7lzGPMaobgLopdpx0BeyK13FdsqNlIDONt8tMa24ysdAHBLV3jGTJseco/bWBkAm5k7h7ixKYpX3rdQkKImm2Ubv9DIma8noMUAJsZTHqHUQvVYLX4Lu6q7hYfCtuMrncflnOtxOecGDCHeGDlzETykKgrlA8BEArqV5cz3BQpYznwfAd2KyJWvuDxKiBkeUmXkzEX88rhtFOlxOedKfY4XbGbuHAIieLcBABh0sOfP1jUtmomUq2Yw6QCwt8HrKD9Mz5XVeVmXx9+P+yAAfdfI5TX5Dui7EO1RHGd5SQT0PTYz1xLwBAm9U2isIIWFetaNTZQIlE9N05mdG1bp85IMgi8RKxboJqxYAPS60PP4ZneZ9aiX/4obPYH308DnBJSXRNzYRAoLr6ElJe64dQGJhLG1+xmR26cvrNRZ312jz08yQPZFUUkGJL+7Rp//wkqdFcJnEEdDuFdRQsmjhJj9g9z4jQEShYzs7PHwjfaDmKYzO2/K1oU9DrgpW2c2TWd2yp1PUHnrMrKzxw95A3C7DGbwpnoGHewbVunzIpWzYZU+z6CDXVyewPIMbpfBPOQNQPxG8vw5jD2UW5UjyYDk/CsZu7g8keVRSk1D3gAYhhGkMy3KIFHtfgYAi2b1vTeR5TEMkzvkDUDsAQJTqWiQujeR5WkewMfAXtkxuEnRDIAKB1p7G7yOaEVJ3ZvQ8ijsQ94A/KtwQcqP0HPRyio/3PfeRJZHiWYAAFDCPyg75GV7XZHveNLrQk9ZnZcVlye4vBLNAPTOYgDBPXRdHrCPb3ZXRCrm8c3uClHcPdHl9ULvLB7yBtBUU9NOCT7ml1mPepds2ecJ+33HW/Z5LP54uySJKI8SfNxUU9M+5A0AAIY5vWsBtPCKyPrPPKYH/+IuC+Vue13oefAv7rL1n3lMEK7Stfv/ElVei/9/1lYDAaC+vqrNyJmL/GvtgS+KWI968wtfcfa33CrOlm4HsMz/eTuAtASTRylIUSDbSDMAP002yzaWM/8KvjX34Pfi8oDdftDDbj8IAMGn5OT24G0HsMxus1YBAMuZlkGotHjLcwPkQX6WkdYF8LDbLH+mYG6AbyPqSBEoyyfPWgXfrzesJE+V5XVSMDfYbZY/8ws1AxjiaAbAw5dz5/0WMi+e6oc0ANtZzhTcQtf/WeCy4yhvNIH3W36OIaCNAYL4s203YmA5dwGlSQ3aEkGeHqAbjZzZERgHaAYAX549MTDFECo/2py7NPgUFficaPIIAS2eOTMnq76+qk3rAgA4hzGvQZhqPdAcvjSIlJVg8ib6/2dtDGDMykrzP2ETJEFz+BSVRyjuMmZlpQ15A/A/WxdMB0/gHD6l5SXBPaxIMwCgkH+QyDl8KsgrVNIATvIPLpz7qVVB2apBqHAAlcg5fErLIxSskgZg5R+0H7dHnbkSU4jQABI5h09xeURFAzhxqFr8ntpERRD0GT+KyMXR+0Xq3gSXN1o1A+hua7n2TNORagXla6iAkgawFb530wUgB7d/MUkzgsRGMQPwv5NuNXhvEKOUTjiw7fPsqi/+uqvl8AHLYBkYDiUUDQXbbdY6ljM9C+AtXjHpbmtZXNf2ndxtAPruAKIRG9SIA2wA8AS0dwkOChQ3ALvNSu026x8BLIBwTKAkEWfFakijWiTQ/5LiRQBuArAewFcQBYuipALAkwrI0QDw/43f1OjMfigqAAAAAElFTkSuQmCC";
            doc.rect(20, 20, 53, 30);
            doc.addImage(teacher_to_pupil_image, 'PNG', 23, 26, 17, 17);
            doc.setFontSize(14);
            doc.text("Pupil to \nTeacher Ratio", 42, 28);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_pupil_ratio || 0, 42, 45);

            var attendance_ratio = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z15fFTV+f/fdyazJJN1QkgICYQkJAIim0arXzewSF2rUL+2IrWKQkGUnwtQQamFWgW/KKhYFyyv2i+KX0ALiAUBFS27ZRUNSWSHBLInk2TW8/tjZi53Zu5MZkI2kM/rdV9zz5lz75y557nPec5zPuc5khCC8xWSJOkBM5Dk+fQeiYDJc8T4Hf550YDGc0ieQxPk03u0BC6gEjgJrAeWCCGKW3ivVoMUrgBIkpQMdAOi8H0Q/g9GmdaFOPSKT4Pi0xAkz4C7YZWNbWrJn+4kaABuF0Js7MhKqAqAJEn5wC+B64FcIAP3m3IRrYsDQoh+HVkBHwHwqNT5wMOAtqMq1VJoNBr0ej06nQ6DwYBerw9I63Q69Hp9QNqbFyrd3KHT6YiKisJqtdLY2OhzOBwOnE4nq1at4vPPP1dWO0UIUd5Rz8xfAOYCTwUrrNFoSEtLw2QyIYTAe63yXJnWaDRER0cTHR2N0WgMOI8kL1RDeBtYq+38MltfX0+PHj2w2WzerF5CiMMdVZ8o74kkSdHAZOWXWVlZTJw4kX79+pGZmUlGRgY6na7dK3khITY2lszMTEpKSrxZHWrHRCnOByvTU6dOZdq0aRcbvA3gcDg6ugoyNIrzOOUXDzzwwMXGbyPY7XZl0hasXHtAo5aZlJTUlJmZ2d51+cmgMwlAlFpmly5dnO1dkZ8S/ATA2pr3liRJAroDArAIIapDlVfVABqNRjX/IloHbaEBJEm6XpKkZcBp4BhwHKiUJGmHJEl/8Bj5AQgmAJ1/PHUew88IPCcNIElSb0mSPga+BEYCXZRfA5cDLwD/kSTpCv/rVQVAez4MqM9jtIYGkCQpVpKkV4HvcHttZZh0errFxiH5TltcAnwlSVK2MlPVBrioAdoOLpcLl8vlTQohhD1UeTVIkmQC1gDXAmgkiVtyL2Hc4AJG5OQT5enBiysrWPjtFt7bvZMaaxO43fkLgRHee6kKgNuOuIi2gNPpY19H7BCQJCkG+BRP4/fvmsaHd/+Gvl26BpTNNScz7+e38cCAIVz9tzex2G0AN0uSNEwIsQGCdAEX0XbQ6XSYTLLzTxfMOFODovGvB3hgwBC2PThRtfGVuKxrNx4YMESZJXcDFwWgA5CSkqJMJkdw6WLgBoBbcy9h0W2jiI4Kz1k3pFt3ZVI2FC8KQAegJQIgSdIjwK8AeiQksuSuX6OJoKsus9Qrk7IRclEAOgCRCoAkSd2Al73pozXVjPhgEUsP7MUpXCGuPIsTdbXK5E7viaoAXDQC2xYt0AAv4zdXs+X4Ue5dsYRrFr/JgfLTzd5gd+lJ76lAIQCqo4BIIYTg+PHjFBUVYbPZfObpdTodmZmZ/n/6Jw2/Z2EOVVaSpOuB3wT7ftuJYwx+ZwGvDL+N3w+5SrXMnrJTfHPssDe5XQhR4020WABsNhsrV65k8eLF7Ni+HUtDQ9CyUVFR/Pa3v2X+/Pmq3wsh+Oijj9i/fz/PPffcBT8LGa4GkCQpCnjdm37st3om3Gdgb6GTl9+1sn2Pe0hpdTqY8NknOF0uHr3i6oD7LNjxb2VyiTLRIgE4ceIEI4YP59CRI6rfa4BMvZ5so5ETNhsHm5pYtGgRd955J0OHDvUpe/z4ce699152794NwP33309eXl5LqnXeoEsXpbc2ZBcwCbgU4IrLtMx/1j1izM/WMGqEjrc/tDF1TpOoqRMSwGNrVxGl0TJ+yJXyDcobLCzZv9ubdABLlT8QsQA4HA7uuO02ufFviItjUEwMOUYjOQYD2QYDWQYDeoUdcXtREaurqykpKfERgJKSEm699VaOHz8OQEFBQUDj79mzh/Lycmw2G3a7HavVis1mk9Pec5vNhsvlCqCpqVHXhBA4HA6Zp+d0OuW08vDz2gFu+yguLo7ExESSkpIwm8307t07QLBDIRwNIEmSGZjpTb80xej3PYz7tZ5f/lwnPTy9oWnVBodRIJjw2SdEaTSMHeR2+7+9aztNZ+cePhBClCnvE7EncN26dRQWFdFdr+cf2dncEBcXtKwX/202s7q6mkOHDsl5VVVV3HXXXXLjS5LEc88953OdxWLhvvvu4+jRowEN0ZHQarX06NGD3NxccnNz/d/oZpGc7NPmwTTANCABwJwoERuj3iapXST++VeTccpLTbUvv2uNFwge+XQFA1K7MTCtGwt3bvEWFcBL/tdHrAHWrl0LwKSuXcNqfIAsgwGAIx6t4XA4GD16ND/++KNcZsqUKdxwww0+15lMJvbv348QAovFQl1dHXV1ddTW1lJfX09tba2cV1dXh8vlkg1PvV5PVFSUfO7N9zJ3w4EkSTJ72Gg0ykd6evo52SlGo8/bnKjyu+nAo56kqKwWUsHd9Qz9WRR/GG/gpmui/MrD3GnG+KwMzZlJf2xMEoioZ79ax135/ZTDv9VCiO/8fytiAaipcRuQtyQG1DsosvR6AI4dOwbA008/zVdffSV/f8cddzB9+vSg10uSRGxsLLGxsXTr1i3SKnc66D3PwwOLSpE/4VmHMXWI2XV9RrR25vYKNm5pYuMWB3cM0/HG80Yy0nxH8RNH61OKD7vWvrrYevPakoNsPn7Exdmh/otqdYlYAOrr3R4lpUJyCsGPVis5RqOqYyHZ88YdOXKEN998k3feeUf+7qabbmLx4sWcKwfFbrfjdDpVqerN2QDKT+XhdDpVbYD4+HjZBvBrzLDgtxinyu/+1wEPAvRO1Ik/XGHWGrQSN2bEsKyojin/LmflBjtfbHXw0hQj43+jR9ljvzLDOPStD21bG5vEVXVWq/ehfi2E2KxWlxYLgEPxJ6794Qe21NdzS0ICS3JySPCjE3gfYUVFBVOnTpXzr7nmGj744IOgD9FisTB69GhOnz7tY+wpD7vdjt1u93+o7QaTycTIkSNZuHBh2Nco1gSAQgAkSTIAb+PW6rx5Y6pk0J5t3VG94xje08SMLeW8s7+GCTMbWfYvO4v+Ek1WhvwC6X42SMvGLT4TjapvP7TACPSSGZQCcF1cHFvq61lTU8MVBw7wz9xc+kSfneRyKcp636hBgwaxfPlyoqODT4aZTCbZCFT29cpDaQcIIQL6/aioqHO2AbyHd6FKZmYmubm55OTkkJOTQ0xMTFj388JPAJScvRlAPsDYSxO4Jj3w2cTrNcREaUg2ajHpJDZucdD/lno+ei2GX1zv/l9l5aK/4pK9Qog1weoSsQYweAw6u6JR/9y9O4VNTXxSVUVRUxP/9cMPbMzPZ4DnwexvbPS5R2ZmJsuWLSM2NrbZ3xs1alSkVez0UNMAkiRdCkwFSIuJcs7+WRdVUs620iZe21PF/Ou7cndOLPevK2XjsQbunmARaxebpC5JGr4rcioXmwRY/kpE3PF6LVilAGgliQ9zcrgxPh6ASoeDnxcW8l1jI4VNTdxRVCSXjY2NZdmyZaSmpkb60xcM/ChhjZIkaYH3cK+WZsENXbXx+sCmaXIKHtlQRv8uBh7sm4DZqGXl7d0Z0yeeJivSLQ82OJ95uUl5ySH8HD/+aLEGsPn1uQZJ4p+5udxYWMi3FgtnHA6GFRailyTKFSTI2bNn069f+Ativ/nmm2ZtAO+5mhEYzBj0NwLVDMFwjMCkpCTy8vK46667wv5Pft1eHO71mFcA3JUTK27vZVLtg/+0rYKD1TY23J2BxlNCK8GC67uy+4yVveVW7T/X+wjXTCFESIq/UgDCmgL0Gmx2FaMrTqvls7w8rv3+ewqbmiizB9LdBgwYEM7PAG4j8Omnn+bo0aPU19f706k6DHFxcbINEBcXF1ZXpkT37j7kjKuAmwFidRrHK9d1VX0pd5Q1sWB3Fff0juPqbr62gUEr8ffhaVy19ChNTrldvhJCvN9cXZQ/JuucUEagVwNYg3jmUqKiWJefz93FxXxrcQ9x47Ra6jyNl5OT01ydZJhMJrZskT1ZNDQ0BDUC6+vrcblcQZ0/SgdRuKTnYEZgQkJC2P9BDV26dMFgMGC1WgHu8Oa/fG1KVGpMYN2sHtUfHaXhL9eoex2P1TuUjW8Hfh9OXSLWAF4BqAnxNvbQ69napw+vnz5Nd52ObIOByw8cICEhAbM55OxnSMTExBATE3Pe2w+SJNGtWzcOHz4s592YEeP4bZ941bd/1vYKfqiy8caNXUk3BRaptroYt8HHxf8/Qojvw6lLxALglf7KZtRxlCQx2dNQ4zx/tH///iGu+GkhPT1dFoCYKMn516Gpqo2/rbSJV3dV8YueJh7sq655Jm86zUmLbGcdAWaFW4+IBSArKwuADyoq+K/YWAbFxBAlSdiFoNRu55Tdzhm7nTqXiyqHgxVVVayvrUWr1fLCCy+EW68LGvX19Rw+fNiJJwrLX65J0faIC2z/yiYno9eeItGg5a9D1bXeiuJ6lh6sU2Y9JoQITs7wQ8QCMHLkSKZPn84Oi4WCAwcAiNFoaHS5COaLS01J4X9eeYXBgweHW68LGtOmTePkyZNagGvTo8XDlyYEPHuXgAc+L+V4vYMPf9GNriq2QVmDk0lf+dDBVgkhVkZSF43aeSgjMCkpiW+//ZZ77rlHLteg0vjR0dEMHz6cOXPmsGvPHn75y18G3uwniLVr17J48WIAjFGS669DU1Wf9os7K/n8aAOjL4nnzmz1Ucb4jWVUNsldcQPwWKT1iVgDAGRkZPDee+8xdepUSkpKOHPmDOXl5SQkJNCjRw+ysrLIyspq0UTJhYyqqiomTpwop2dd1UWTnRA4rbzhWAN/3lFBXqKeV65T51K+d6CGfx3xmUic3ZJYQy0SAC/y8/PJz8+P9LKfLCZPnkxpaSkAV6UZmXBZ4JT6iXoHD3xeikEr8b8j0ojVBXoED9XamfqNT2CxncDcltRJVQCE22V2kRveili+fDnLly8HwKCVxFvDUiWN3xO2uwSj156ivNHJGzd25dJkQ8B9XAIe2VBGvV32wzQA9wkhWhR4SNUGcHUw/8pLOrlQUFpayuTJZwOwPVeQLOUlBnaPz2wuZ2tpE/+dFxd0yPfG3mq+OekzufaEEOJgS+sWTAPIQ5RQsNlsrFmzhq1bt3KwsJCiwkJsVit6rRa9TkeUwYA+JoZe2dlMnDiRK6+8MuT9jh49ypQpU1i9ejUrVqxg+PDhLf1fnQoTJkygqso97V+QauTxQUkBZT4uqef1PdX0TtTz+g3qiz0PVtt4bquP6l8lhHjrXOrWYhtgzpw5vPnGG5ypqGi27K7du/nss8/Ytm0b2dnZqmXefvttpk+fTmNjI0lJSVx66aWRVKfTYtGiRaxbtw6A6CiJRTelofV70sXVdsZvLMMYot93Chi7vowmhzzeKgMeOtf6tUgApj/zDPMXLJDTMRoN2QaDTAtXUsRP2Gw8cPgwRxsbVQXA5XIxbdo0H0bNwoULSU9Pb/m/6iQ4dOgQzzzzjJx+4eou5Cb6Wv2NDsGv/3WKWpuL12/oSn+Vfh/glV1V7Cjzmep9UAhx5lzrGLEAFBcXM3/BAiTgHrOZ57t3J9+X5eqDPKORR7t2ZcqxYz6+b3Czg8eMGcPKlWd9F2PHjuX222+X05s2beLMmTM+6wFsNltAWkkLCzUVHIz753A4sNvtcp4/xUyn02E2mzGbzSQnJ5OSksKDDz4YlB3scrl4+OGHsXgmxIZlxjCuf6DV/8zmcvZXWPlV7zge6qfe739XaeP5bRVKw3xhKJZPJAg2GxhUGD744AMAxnTpwuJevcL6kb4eATmiWEkkhGD8+PE+jd+3b19efPEsfc07HXzs2DF5tq+joNFo6NGjB3l5eRgMBjIyMkLOKr766qts3boVgASDhreGpga8YV8cb+CtfdXkJup440b1ft/hEvxuXand4RJeSfuBEPGcI4WqBgjF0D169CgAD0SwGCLT4xBSCsBTTz3Fhx9+KKd79erFsmXLfDjzJpOJbdu2AW6B8a4F8B41NTXU1dVRU1ODzWaT+X/KqWD/iODhrHzWaDTy9K/3iI+Pl2dCm8N3333H7Nmz5fSr13Wle6yvr7/W5mLcxjKiNBL/GN6NOJV+H+AvOytd+yqs3sa34x7yNaoWbgFUBSAUadLLCk7xK1NitXKgsZHbEhMDJD3DIwBe4Zk9ezZvvXXWeM3NzWXNmjUh+33vkqy4uDh/QkWngs1mY+zYsTLv766cWO7NC1xA8/Q3ZzhW5+APl5sZkKIuWLvPWHlxZ6Uy6zkhxH9as76qAqDV+tupZ6FGCwe4/ocfOGGz8YuEBP6RnY1ZISDRHo1y4sQJ5s+f76PmL7nkEj799NMWzfFHagMIIYKuBXQ6nWHbAGazOSgT+M9//jP79u0DIDVGqzqk+/Swhb9/X0s/s55pl6vzI2xOwZh1pVaXwCsdm4A5ET+kZhBMAIJe0OBZBu4vAPcnJ/PiqVN8VlPDPSUlrMvPl40Kb8/tcDh8VgD16dOHzz77LOjaOovFwq233upjBFqtVpkH2FHQaDT07duXL7/80qfL2rZtG6+++qqcfnNoKmaj77OsbHIy8YsytBK8NSwVfZB3bdb2Coqqbd7GrwHuFyLMcCARQNUIDCUAXqvX3+/454wMdjc08K+aGjbU1jLr5ElmelS6U4U/2KtXL1atWhVyYaXJZGLatGkcP37cp+/3twFqa2uxWq2q/b4yT6/Xh7UCybvziNIGSExMJC8vj7y8PHJzc/3X92GxWHj44Ydl3uLv+ibwi56BWwE8vukMZQ1OnhycxJCu6qOnHWVNzNtVpbT6JwohjjZb8RZAVQPodLqgXYDaugBwS8+HOTlctn8/R202nj9xglyDgfuSk9lU50NYID09nVWrVpGWltZsBUeMGNFsmc6A6dOny4tds+J1zPmvQMFeVlTHsqI68hL1zChQXxTc6BA88HmpwyXktvlACPG/bVVv5esQVhcg08JVhmQJWi1/69ULCfda5NE//kjevn3cXXx2d7Tk5GRWrVolM4suBGzdupVFixYBoJHg3WGpAd68sgYnj286g8aj+o1BVP/MreX8WGP3Nv5RYEIbVj1yIzAULRxgaHw8j6em8mqZm6RY1OTjvWLu3LkRTSGvXLmSM2fO+DiAlIEivOlzNQL9j+aMwJSUFCZPnoxGo2HSpEly+d/3T1Rd0jXxCzd549EBiVyVpq76vznZyOt7q72q3wWMaS7c+7kiYhsg2MIQJeb16IFOkpjrmftWYuDAgWFXzmKx8Nprr3HixAm5v+8oZ5DJZCI3N5ekpCSSk5PJz8/HYDAwd+5cvv/eTcBNN0Ux86pA1f5BYR2fHraQnaDj+auCGLx2F2M3lLmEkNvhZSHEV6qFWxHB/ADN2gCWEA0hAXMyMxlkMrG2pobeBgMzTpzAaDRGvC5AucWaEIKGhgZqamqCGoFqhp/SQRSpERgdHS0HifDHoUOHePHFF2Vjbd51KQEOncomJ1P+fQYJ+OvQVGKCPNpnNpdzpNbuvXgn7oWibY6IBcA7/q0MY+OjX5vN/Nps5v8q3c6M/Pz8c9raTZIkTCYTJpOpU0wWPf7448JqtUoAt2SZVLl70/5dTnmjk3GXJnCtStcAsOlEI+/slzkQdcC9LYki3hJE7AfwBjj6wa9vV4NDCD6prmasZxLonnvuaVktOyGWLl3Kxo0bJYCYKIlXrgt0+Hx5vIH3f6ilZ5yO2Verq36rU/Dol6eFOPv8ZwghSlQLtwEiFoDhw4cza9YsXisrY3N9PZdFRxOj0VDrdFLmcFBmt1PhcFDnclHvdMo+gBEjRjBp0qQ2+yPtifLycp588kkHnuc3oyAZf15/k1Mw6avTSLitfrU5foA531ZSVG3zPvsy4B3Vgm2EiI3AQYMG8cc//pG5c+fyrcUir/8Lhry8PJ544gnuu+++CyYE7ZNPPimqq6ujAPonG5g0IHCa98WdlRRX2/l9/0Su766u+gurbMz91sfhs6A1J3rCQcSTQeCeyRs9ejTz5s3zoYXHx8fTs2dPevbsSd++fRk2bBgX2vZza9asYfny5RK4H9hrN3Qlyo/d+V2ljXn/qSInIbjqdwmY8MVpl90llKrhR9XCbYiIp4O9SEtLY86cVp+b6NSora3l0UcfteMJ5JCTqKPO7sJid2HyqHiBe8zvFIK3hwW3+uftqmLzqUb/B90qsZsjQcSewJ8yZs2axenTp2UKUHG1ndtXniDrb4f42sPUfWd/DdtKm3jk0sSAdfxe7Dpj9TJ8/BH27iGtBVVa+EUBCITD4WDJkiWqS6Lr7S5+9elJSmrsPLulnCSjlucK1Kd5LXYX96895XK43PF9/SaV2l0AIh4F+MPpdHLo0CEKCwuDhovPzs7m2muvPa+NwK+++oqamhotQGKciWcfHoXRoOOpeX8XjVabVGNzcfMnx6m1ufifa1NIMqo/w6e/Kaekxu3wMZvNrsmTJ2sUIXLPHwE4fPgw7733Hv94/31On2menFpQUMCaNWsCplHPFyxfvlzu+1e+OpWrB7jnM/YUHpbeXrEecC/rArdPf2CKIaALWFJYx98OuB0+kiTx7rvvaiy+o6gO7QJkAWjO375z505+VlDAvHnzAho/TqtlQEwMv0xK4v+lpnKP2YxOq2X79u2sX7++VSvfXnA6naxYscIJ0L93D7nxAQbkZwWU/7iknttXnmBfxdlNQfdVWJn4ZZnc7z/99NMMHz7cn1Xc7m+HqgYItb+9xWLh7jvvpK6hgViNhompqQyKiSHbYKCXwUAXlSHkQ4cO8V55eQAt/HzB119/TX19vRHg3puv8fmub68M1WsaHIL7/lXK3vt6UmNzce9np2hyuPv9O+64g2effRbAn2jaoV2ArA1CCcDy5cuprKmhf3Q0a/Pz6RZG1OybEhLOawFYvXp1DZ7Q7SOu9p3NvHZwHx65+ya+3vU9V/TLZem6zVhtbjd+UbWNw7V2nvr6DD/WuPMGDhzIu+++K9tDfpNMncMGCBWOzct1fyw1NazGB+Ryp06dakEVOx67du1qxCMAvboHklcXPvOwfP7I3Tdx15NzOVPlDtP+4PoytpxyDxG7devGRx995EMo9XvZOocAhNIAdR5619URxMbzCkCpCj8gFJxOJwsWLKCuro7GxsaAw0sCEULIu4Uoo4H77yjiJY54bRxJkjCbzaSnp9O9e/egAZ+Lior0AIlxMa54U3RIL9lVl+Xx2tSHuHfaKwBy46empvLxxx8HzGI2+obR7fwC4LVaIwnZmOYRgLKysmZKBv7WF198walTpygvL6eysrLVgkUajUZiY2NJSUlh8ODBFBQUqJazWq1UVVUlAvTu0S28UHp6XzsoNzeXTz75RJUG1+Q7q9r5bYBg6wJCwbt/UKQCEB8f77N0TAhBVVUV5eXl8j5CRqMxYBWP0WgM6s7WaDTExMSE7esoLi5GCLe/vslmlzZs38fQKy5V9Wm4XIKXFn/C82/9n5w3ZMgQ14oVKzR+28TI8BOAzj8K8HLfIhEA76CysbGR2tpa4j1BpSOFV2WbzeZ221ls//79sgG4r+goN0+YTe8e3fjbHydw1WVn63CqvIrfPvc6G7fvl/NuueUW1+LFizWhwsn7CUC7e8oiFoBgtPBQUO4XUFZWFpEAOJ1OmpqaaGxspKGhQT4PxwbwDybdnA2gRlfbsWNHOR4ByM/LxdLQQNHRk9wy6S988c5MBuRl8a/Nu3nguddFeXWdBKDRaJyzZs3iscce0zbn/fSzAdqd8KgUALlzDdXPtkgAFOelpaX07t07rOsqKyvJycnxD6/e6jAajQwcOFDVUbVr1y65+mlpXYmLiyWlSzJ79uzn57+fxV03Ftje+2Sj3svoSUpKqv7444/jL7/88rBC8fsJQLtve6IUAPkph6MBQrGC/WFU9MenTze/z60XZrPZxwj0P87FBvBuQtW3b18uu+yyoCHtiouL5eFOTIzbRuvevRv19RaKin9k0Scb5QuHDRtW+f7775sj0XB+XUCHagB5sV0oAWhuXYAaDJKEVpJwChHxrl8DBw6MiEremrDb7VRWVqYAGDxRxl0uF8XFhyj58bBcTqvVOl566SXn+PHjI46E3Zm6gIg0QFME/PwaDzdQkqTzKmD0kSNHEEJEAUTHRFNWdoYD3xdisZwNxdurVy/HkiVLovr3798iMkdnEoCwNIB3Nq8qDFq4Fwc9ai4rK4u4MDebBPfy7xdeeIHa2lqampp8jMCGhgZ5BU+4RqDNZpO3hAVfIzA9PZ0lS3z2VfaZuaytrWPHzl0+399///28/PLLUSZT4CLQcOHXBXR+GyApyR3irLlw8V7YhOAPir2BI4HdbqekpMTHBqipqTmnLeIkSfKxAfr160dBQYFqCDvl2F05QxofH89rr73GyJEjW1wPLzqlDRBqFODd72dBWRmJnqlfmRZut/vQwqscDlbX1HDSZiMxMZE//elPEVXOZDLx/vu+u544HA4qKip8jEA1Q/BcN6IEd8DradOmyQEttFoto0aNYubMmfTo0eOc7w+dSwDC0gC33347OTk5lJSU8EiYs3tXXnkl8+fPb5XQLlFRUaSmprbbriEzZsygX79+2O12Bg8eTG5ubqve32+I26FdQNg2wI4dO1i4cCEvvfSSPDnkX6ZPnz4MGzaMm266iWuuuea8poNFsiNYpPDjA3SOLiCUAIB7KDh58mQmTJjA6dOnA9YFpKamntcN3p7wcxN3qADIP96cAHih1+vJyMggI0OdFXMRzcNvD8F2FwBVWni4AnAR546OJsmqTga1NAhDRUUFNpvNZ6+9cDdq/qnCTwN0DkJIuKSLw4cP8/e//53Nmzfz/XffUVFVFVBGo9GQnZ3NE088wZgxY865whca/ASg5R6lFiJiQgi43Zdjx45l5cqVIZ0yMRoNTS4XxcXFTJw4kX79+jFkyJBzrvSFhM4kAGFrgN/cey+fb9gAQFedjr5Go0wLz/YcvQwGUnU6KhwOfn7wILssFvbt23dRp64OjQAABZ9JREFUAPzgJwDBmSNtBFUN0Bwr+PMNGzBpNPyxe3ceS02VKV9qSI6K4uEuXZhgsfgEi74IN/yMwM6vAVasWAHA+K5deSqMQI/g3ksY4LhnTiASbNiwgZqaGhoaGgKOUOFivbF/I2EFjx8/PuL6nSv8/ACd3wYoL3fvWfPrCDaB7uYRgEhp4ZWVlTz00EPyb7YFkpKSKCgoiHgL+NZCp9QAoYaBXlq4IYLJlpauCzCbzRw+fBi73S5PALU2IygtLa1DvZad0gYIZdm3hBae6KFgR0oL90Kn05GWlhZWbOHzDX4CoJEkySiEaD4EWytB1UsTijPvnb6MRAC8JauqquT4AecCLymkORsgEkJIYmJgoKf2gJ8AgLsb6BABkFs0lPfO23gtWRcghOD06dNhzx1UVVVx2223yUagxWKhsbGxTcLFpqamUlLSbuH5ZKgIwLkTGSKAakuHEoBzXRdQWloatgAkJiYybty4NmcFX3nllfTp0yfs/9OaUJkLaJcIoV5ErAFaQgvXKoysSAxBSZIuePdxRwuA8jWJqAuIRAPEtAI960KFyqKXdp2KbRcN4PTEQHdBxFvCrlixgurqahobG2UbwPvZmkZgeno606ZNi6hurQGr1eqf1WFdgIxw9guojWCZ9o9WKy7cbNpIdgqpqqri+eefp7S0FEszIWlbiqysLOLj42W2c3vDnxbeFhtDhULEGsDrMQsnXLwXq6vdm15EuigkKSmJPXv2AO4ZyGBGoNIQ9Mb3D2UEekPOx8bGBt3+rb3gJwDt+vZDEAEIth8uIEe42FRXx++6dCHOT1tYhaDC4aDB5aLW6WRFVRUvekLDTJjQ8u1voqOjyczMvOBiD/t1AZ1DAEK5Ru+8805mzJjB8qoqNtTV0UuvJ0ajodrp5JTdrqoZNBoNTz/1FHfeeWerVv5CgJ8GaHcunqqeDOUKzsrKYtmyZfTu3Ztqh4NdDQ38u76e7xobAxrfbDYzatQoNm3axMyZM1u35hcIamtrlckO1QAymlt6dfPNNzN06FCWLl0aMlz8wIEDW2WFzoWM//zHZyvgQE5dG6PFjE2dTsfo0aNbsy4/OTQ1NbFmzRpl1p72rkPEXcBFtB4effRRDh48qMxaGaxsW+GiAHQQZs2axYcffqjM2gq02RaxwXBRADoA3nWVCuwBbm1vJxAEiRZ+UQDaDkuXLmXq1KnKrL3AMCFEZUfURykA7c5H+6lh/fr1jBs3TvmCFQM3CSEqOqpOFwWgnbB//35Gjx6tJNxWALcIIZrfbaMNoRQAmRZ7IXLvOhKnTp1i5MiRMp8SN+XrDiFEUQdWCwgiAD179uyAqlyYsFgsjBw5khMnTnizBO5t4Td3YLVkqHYBfqHLLqKFcDqdjBkzhr179yqzpwgh/i/YNe0NpQDIIlpYWNgmxMufGp588knWrl2rzFoohHi5o+qjBqUAfO092bt3LwMGDODNN9+kuLjYf8LiIsLAK6+8wrvvvqvMWg081kHVCQpJOeaXJGkZoBr8TqfTYTAY5D0BDQYDOp1OTnv3CQw37f+d/xHqWpPJRFJSUoct5xJCYLFYqK+vx2KxoNFoiIqKQqPRUFJSwsKFC12rV69WvlzfAtcLIdqG1nQO8BeAWOBZYDJwbqs32gFarZaEhAQSEhJITEwkMTGRhIQEWTi8AuuNVqIUXm/0Em9DBjvq6uqwWCzyZ319PQ0NDZE4yw7gdvREti6unSCp/RFJknoDTwGXAnmA+hbYF9Ec1gOjhBA1HV2RYFAVgIBCkpQIxAEGxaHDrSX0Qc4j+d6IexhqUn56NFKsEOJ82m7UDvwAfAZMF0J06ohbYQlAR0OSJA1uoUgAEoEkz6fy3PuZgDvYkgG3YCk/lecawALUez6V5/55DYAVd+Pa/A5l3mngByFEuzN7Wor/D76G2Hou4LZgAAAAAElFTkSuQmCC";
            doc.rect(78, 20, 55, 30);
            doc.addImage(attendance_ratio, 'PNG', 80, 26, 17, 17);
            doc.setFontSize(14);
            doc.text("Teacher \nAttendance\nRatio", 98, 28);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_attendance_ratio || 0, 112, 45);


            var punctuality_percentage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAEbAAABGwBr11x4QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z15dFRVtri/W1MgFGHIJDQsqEJGedgiIAEMyBAFpbuBhwoCPhuBx/gTaMT4lEFcAiKgyCCTPgbBJ0voJ70cABXCPDTd7fIBMlQRgUBmhiKBpKru749biam6pyp1qyoT+P2TlXNv3Tp19r777DPtLcmyzL2G1WypC7QC2gCtgRZAfSAGqOvzF+AmcMvn73XgAvAzcAY4a3PYb1Xer6gcpJquAFazJRroDvQGHkMReOMK+roMFIU4CnwPHLQ57AUV9F2VQo1TAKvZYkQR+BMoQu8CmKqoOkXAMRRl+AFFIYqrqC4hUWMUwGq2dAZGAc8DcVVcHX/kAJ8BG20O+/GqrkwwVGsFsJotTYERwEigbTjPkiSoXRvM0TLmOlAnWsYcrVxzFMDtAgnHbXAUSBQWQgSa5TSwCdhsc9gvhf20CqJaKoDVbOkAvAEMAXTBfk6SoEkjGWszNy2ay1iaybRo5sbaTCY+VkaSgnuOLEN2roQtXeJCug57usSFixK2dB2Xr0palcMNfAG8bXPYf9T0yUqgWimAx8y/AQwEghJXs6Yy3Tq5SOrkJqmTm9gGFft7cvMlDp/QcfiEjkMn9KRfClKrQAZ2oihCtekeqoUCWM2WrsAc4Mny7tXroFtnNwNTnHTv4qZRYtXW/2qmxMFjOnbuMnDouA6XO6iPfQvMsTnsRyq2duVTpQpgNVvigXeBFynnjW/Xys2gAS4GprhIiKt6pRWRlSOxc5eeHV/pOXW23J5LBjYAr9oc9uyKr52YKlEAq9miA8YA84EG/u4zmWDoQCejhrpoaQ3u1aounLPp2LhNz7adBoqKAt6aD6QCa20Oe6X/yEpXAKvZ0hFYhTJ+FxJdG4YPdvLyC85q+7YHS1aOxLpPDWzZbqCgMOCtx4DxNof9ZOXUTKHSFMBqtkjATGAeYBDdU9cs8+KzLl4a5qRBvZoteF/yb0h8stXAhs/13HL47e2cwJvAQpvDXikNUCkK4OnrNwJP+btn0AAXqVOKiWt4bwnel5w8ifnLjOz4Sh/otm+AUZXhG1S4AljNlmRgK37m51taZN6aWcRjHWtWHx8uR0/qmLXQxDm7X2uQAQyzOexpFVmPClUAq9mSimLyVepeuxZMGVPM6GFODMIO4d7H6YT1Ww0sW2uk8I7wFhfwps1hn19RdagQBbCaLXoUR2+M6PqDzWVWLLxLS8u9be6D5ZxdYuLMKM5f9GsN1qI4iK5If3fEFcBqttQCtgCDRNcHDXAxb2YR0bUj+rVBkX9DIitbIjNHIisbMnOUBk+Mk0mIL/krV4kDWlAIby40BfINdgDDbQ672FaESEQVwGq21AP+F+jpe61WFMydUcTQP0Rcib1wOuHI33WkHdFz5ZpEZrYi9KxcqbzxeCkmEyTEKsqQGC/zuwdkkru66Pqou8K7q21f6pm9yMSdu8LL+4A/2hz2G5H6vogpgNVsSUSZ4nzY91pivMzH7xfRtmXFOHoFBbD3sJ5de/XsPaTj5q2g5+c1EVNXplc3Nym9XPRKchEdXSFfw+lzOv78ionMbOHv+BfwpM1hz4zEd0VEATxv/j4Ewm/RXGbDsrs0fiCyZjUnT2JPmp5d+3QcOq4P+u2OFCYTdOvsIqWnm77JrogPXzOuSbw4JYoLYr/gX0DPSFiCsBXA0+d/g8DsP9LezbqlRRHtU//xk473Vho5elKHu5qMHHU6eKyjm79MKOaR9pGrVP4NiZenmvjHT8J1hX3AU+H6BGEpgMfb34bA4evV3cWK+UXUrhVG7cpw8ZLEopVGvv4u4ASKEIPBQMP4hsTFxxGbEEdcQiyx8bHEJigbi3KzcsjNziUnK5fcrBxysnPIy87D6XRq/q7+fVzMmFBM86aRUfrCOzAx1cTeg8LfvQMYGs7oIFwFWINgqNeru4u17xWh1y4rFXn5Eh+sM7B1h4Fg5CFJEm07tCW5bzJdenQhsfEDxNSPQQp2N4gHWZa5ef0mmRnXOHbgGGl70jj942mCaS+DQVnLmDLaScMI7E9wuWDMX/wqwVqbwz421GeHrACeSZ53fMsfae9m88q7Yb/5hXdg/RYDqzcauV3Ovluj0UjHrh15vG8yPfr0IC6hYrYM5mTlcOC7A+zfk8bJIycpLg68/7NONIwbVczo4c6ItMeICVH+uoPXQ50sCkkBPNO73+Mzw9eiuczna++G3ef/9Ws9C5cb/XnBgPKmJ/dL5on+vUnqmUQdc52wvlMrtx23ObzvMD98/T1pu9MCWobEeJmZk4r5U//whsD5NySeHSN0DF1A71CmjTUrgGdh55/4zO0nxst8sT48b9/lgreWGNm0LfBgu0uPLkx4dSIPtnkw5O8CKCwoRJIkaoX5ep4/c56V767g2IFjAe8bOdTJrGnFYXWNGdckhoyOEr0cGcDvtS4gaVIAz5LuV/is6tWKgi8+vhvWOD//hsSk10wc/rv/nTStHmrNhBnj6dStc8jfU8Inyz/hs4+3AvDvo/6dMa+E3I2WcuLQcVYuWsXZ//vZ7z1Jj7pZviC8kdHpczqG/DlKNFn0DTBAy1KyVgV4DWUXjxcL3whvhu/sBR1jppu4lCE2+Y2aNGLs1LH0faafZmdOxJVfrjAs5XncnnGkJEls+XYrTZs3DfvZsiyz52+7WbN0DVcvXxXe07SxzNrFRbRqEfoLs+1LPTPfFp6HSbU57AuCfU7QW649O3nm+ZYPGuAKS/i79ukZPDpKKPyoWlFMTp3Clm+30m9gSkSED3B43+FS4YMitKP7j0bk2ZIk0W9gClu+3crk1ClE1YpS3XMpQ2Lw6Ch27Qu9Lxj6BxeDBgjbfZ5HVkERlAJ49vCtwmcnz4PNZebNDH0K7sP1Bsa/aqJA4OXHJ8azYstKnnvpOYxGY8jfIcLlUjecqCwcjEYjz730HCu2rCQ+MV51vaAAxr9q4sP1oS8uzJtZxIPNVRbcAKzyyKxcgrUAY/DZw1e7FqxYeDfkVb3pc0wsXW0UHrJo16Ed67avp037NqE9vBrRpn0b1m1fT7sO7VTXZBmWrjYyfU5oRxujaysyEPiwXfCzFO9LuQrg8fpV/f6UMcUhr+d/uN7gd9mz38AUln+6gtj42JCeXR2JjY9l+acr6DcwRXh9x1f6kC1BS4vMlDHC+Yj5HtkFJBgL8C4+W7dbWmRGD9M+TQpKn//+GrVJlySJcdP/k9mLZ2OKqqrDvhWHKcrE7MWzGTf9P4W+zPtrjCH7BKOHOUUvYwMU2QUkoAJ4Tuy86Fv+1syikNbFz17QMW22SWX2a9WuxTsr5jNy3EjtD61hjBw3kndWzFfNPcgyTJtt4uyFoP3yUgwGRSYCXvTI0C/lfdscfE7sDBrgCmkDZ/4NiTHT1Q6fJEnMXjyHx/s+rvmZNZXH+z7O7MVzVJagoADGTDeRf0P7aOexjm7RqEBCkaFf/CqA56Cm11m9umaZ1Cna4x+4XDDpNfE4f+y0cfeV8Et4vO/jjJ02TlV+KUOZEAtlUJI6pZi6ZlVX8KRHlkICWYA3fAtefDa0jQ9vLTEKZ/j6DUy5L8y+P0aOGyl0DA//XcdbS7QPfeMaKgdrBKhkWYJQATzn8weWLYuuDS+F4Pj99Wu9cG6/XYd2pL6Tqvl59xqp76QKh4ibthn469fancKXhjlFQ/OBHpmq8GcB3sCn7x8+WPtxrcI7sHC5WpPjE+OZv2rBPenta8UUZWL+qgXCyaKFy/2eF/BLg3oywwerXlQJP1ZApQCesCxDvCppgpdf0P72r99iUK1aRdWKYv6qBffUOD9cYuNjmb9qgWraODNbYv0W7cOtl19wYlK/W0M8svVCZAFG+JYPHaj9lG5evsTqjeq3f+zUcffEDF+kadO+DWOnqp3C1RuN5OVrGxUkxMkMHah6YXUoslUV+qLyykYN1e6SfrDOoNrJ06hJIwaPGKz5WfcLg0cMplGTRl5ltwuUttSKH5mpZOulAJ7hglc0rnat3JqDM1y8JLF1h7rSY6eOjfjCzr2E0Whk7FT1voStOwxcDD4WEQAtrW7atVLJra3vkNDXAozy/YSfJceALFppVG3gbPVQa/o+00/zs+43+j7Tj1YPtfYqczqVNtWKH9l5ybhUATwROJ8ve1Gvg4Ep2hTgHz/phFu3J8wYH7H1/HsZSZKYMGO8qvzr7/T+NoT6ZWCKC736I897ZA14W4Du+ETg7NbZrdn5e0+gqV16dInINq77hU7dOtOlhzqCjqhtA5EQJ9Ots6obiEORNeCtAE/43jkwRdvQLydP4uhJb5WTJIkJr07U9JzfgAmvTlRZzKMndeTkabOifmRYKuuy0urte1f3Ltqcvz1petVxreR+yWHv3r0febDNgyT3S/Yqc7uVNtaCHxmWyloHpSHXvWxOs6ay5iCMu/apO5wn+qv06jeCRNR2ojYORKNEmWbqY2pdPDIvtQDd8Qm53q2TNuevoAAOHffWTqPRSFLPJE3P+Y1fSeqZpBo2HzquF+6hDIRAliY8fkCJAqhULamTNvO/97D6iHbHrh0r/cTOvUQdcx06dvXe4FtUpLS1FvzIsjf8qgCPlb0iSdoVYNdedaUe75ssuPM3tCBqQ1FbByKpk1sUKf0x+FUBvGYemjSSNUXddjph7yG199+jTw9NFf0NNT369FCNBvYe0gV1UrqE2AYyTRqp5NkaQOdJsOR1zs/aTNvbf+Tv6rAsbTu0rbBTuvcTcQlxtO3gnSvj5i2JIwGO0IkQyLSx1Wypq0PJruVFC/Vhg4CkHVGbpOTfzH/EELWlqM0D4UemrXQoqdW8sDTTpgBXrqk7GNFMVlVz6+YtNq/ZzNZ1W1TXPlu/lc//+38oLCeic1UgaktRmwfCj0zb6PDp/wFaaOwCROf4Exs/oOkZFc0Xm79gcPIgPnpvFTlZOarr2ZnZLHtnGUN6DmbXl99WQQ39I2rLQLETRPiRaWsDSlJFL6waLUCWT2UMBgMx9WP83F35rFm6ho2rNgR1780bN5k3Yx6FBXf44/N/rOCaBUdM/RgMBoNXzCLfNi8PPzJtoUPJqFmKJEF8rEYFyPWuTMP4htVm5W/TRxuDFn4JsiyzaNa77N65q4JqpQ1JkmgY39CrzLfNy8NP0qz6On5NnwooqdW0yC7/hjoCZ1x89fD+83Ly2PjRRuE1azOZwU87Gfy006/FW/nuSu76CdlZ2fi2aVERmg6QlKTN8yHGgJJDtxRzdHjmHygNv1bVfPzhxyqnrnYteGNqMcMGlR1IF7N1h4G3l3rvws3OzGbbxm2MGKvaSlfpiNo0K1vStFPbHC1TUOAlr7oqC6B15rYk4HJZ4hKqx47ftN37VGVq4SsMG+TkjanqU097/ra7QuqmFVGbito+EALZxujwsQB1NFsAdVl12PJ988ZN8nLyvMqszWSh8EsYNkjdHVz55UqF1E8rojYVtX0gBLIVWACNAZBFWlgduoD0CxdVZb9vX/4Kp+89hQWF5GbnRqpaISNqU80WQC3bGO1nkWsIbnfk4hO7g8wGWRPRATfLFjg0rjUnCvYM5gomWiqbJs2aqMr++VP506e+99RvWJ/4B8oNtFHhiNpU1PaBEMj2pg64VbbkdoE2s5IgaJtqYTLjY1X9pi1dfF6hhK07DNjSvX9/dTnFJGpTUdsHQiDbW2oLcFvbQ0VamJNV9QoA8MzQZ1Rlby81CpWgZBjoS7uH1Sd3qwJRm2q2AGrZ3jTgYwEcmi1A9ewCALr16s6Gld6zgIV34L/mG1m/xVDq8P3zJ73qzQflFPOz//FcpdS1PERtKmr7QAhke8uAjwUoLFTi1QQ7G9ignozJhNdsYE521StA1tUs5k6f6/e6LV3Clh74zF3q/Ncx1zVHumoh4dumJhOaJoFkWZGtDzd1wHXfG7M1zjMn+Kwd5GXnBRVXv6LIzMhk0oiJZFwKfQz/7H88V22WtGVZJi/be07Dt83LIztXEsVkvK4DLviWisxhIHxNkdPp5Ob1m37urliuXr7qEX5GSJ+vG1OXWe/NYsrrUyJcs9C5ef2mKnuJVvPvR6YXdIAqtPWFdG3TA4mCymRmXNP0jEiQcSmDySMnCYM0N2nWhHnL3qZHnx7oBfHa6zesT58Bfdj01WZS/vCk6npVImpLUZsHwo9MfzYAZ3xL7RotwO8EOQKOHTimOuVakVz55QqTR04i62qW6lrT5k35cPNy4hLieOKpJ7iRf4OLFy5yJf0y5pi6tGnfhoRGCZVWV62I8hCI2jwQfmR6Rgec9S31k6rML8ld1VOsaXsqNOexF1d+ucKkEROFwm9mbcbyT1d4bVCt16AeD3d6mAFDnia5X3K1Fj6I21LU5oHwI9OzOpvDfgsl20QpNo1dQNdH3cTU9dbI0z+eFm69ijSX0y8z6YWJZF9Tr4xYHrTw4ebl1WJxKlRysnI4/eNpr7KYujJdH9U2PS2QaYbNYb9VUurlB1y+KpGrIS6NwQC9unlXSJZlDnx3QFMltfKL/RdF+Jlq4VtbWVm26UMaxjUUfLLmcOC7A6oRVa9u2lLY5uZLXL6qkufP8OvBEK9sCbIMh09oswIpvdQmaX8FdgPptnQmj5gktDItWrfgw03LaRDbQPDJmoWoDUVtHYjDJ3SiIeBR+FUBvhd9SAu9klyq0GQnj5zktta55SCwn7czecQk4fx4y7YtWbbpQ+o1qBfx761sbjtuc/LISa8yk0lpay34keX38KsCHAS8dvYdOqHt4EF0tJJLtyzFxcUc3ndY03PKw37OxpSRk1WbPQBaP9SaDzYuo179mi98UFLb+OYm7NZZe9JqgSyLUGSuKIDNYS8AvMYa6ZckrmZqGw2k9FQ7Jj98rTIuIXP18lUmj5xMfm6+6lqbf2vD+xs+IKZe9dmOHi6ithO1cSCuZkqkqyOMHfPI3CtCiOrbDh7T1g30TXah8/lI2u40zp85r+k5/tixZTvX866rytt2aMsHG5ZRN6au4FM1k/NnzpO227v/1+mUNtaCHxmWyrrs1R9879q5S1uAwriGsiqXgCzLrHx3habn+OPsKdWUBe0fac/7//3BPReHYOW7K1Te/2Md3ZqjtfuRYamsyyrAQcDLpT50XEeWxn1nf5mg3ll77MAxThw6ruk5Inr37+P1/7917MCSj5fec8I/cei4cPZP1LaByMqROHRcZQFy8PT/UEYBbA57MfBZ2Ttdbti5S5sz+Eh7N/37qM3UykWrwl4hfHrI0/zXwjfo2jOJqbOmseTjJUTX0egRVXNkWWblolWq8v59XDzSXlv/v3OXHsF2xs88sgbUkUJVx2j8ZfcKxIwJxaqJirP/93PYe+z1Bj39B/XnvbXvMWTEEGqHmrOuGrPnb7tVqWcNBqVNteJHdl4y9lIAm8N+HPCadzx1Vsc5mzZnsHlT8f77NUvXlJty/X6muLiYNUvXqMqHDXLSXB3pKyDnbDpOnVXJ7bRHxqWIJLvJt2DjNu1W4P+97MTXOl+9fJXtm7drftb9wvbN21VL2XWilbbUih+ZqWQrUoDNgFfPsW2nQbMz2LCBzLhR6rd9zdLVnPlJtQJ933PmpzOsWbpaVT5uVDENNcRrAsX527ZT5f27UWTrhUoBbA77JeCLsmVFRbDuU+0x60cPd6o2Lty9c5fU8a9Vi63j1YXc7FxSx7+mOomcGC8zerj2t3/dpwbViW3gC49svfDXub8NeEluy3aD5nx2tWvBzElqK5CdmU3q+Ncouht64ul7haK7RaSOf024ojlzUrEoL3BA8m9IbNmuelllFJmqECqAzWH/EdhZtqygED7Zqt0K/Km/i5FD1Vp86sdTzH9dlZL4vmP+6/M59eMpVfnIoU7+1F97roZPthoQhDna6ZGpikDuvUpjNnyu1xytGmDWtGKSBBsYdu/cxabVKr/kvmHT6k3CKCRJj7qZNU37aCknT2LD50LnT/j2QwAF8AwXvKIl3XJIzF+mPXOFXg/LFxTRtLHamVmzZDX79+zX/Myazv49+1mzRO30NW0ss3xBEYJ9q+Uyf5mRWw7VC/qt79CvLMHkDvaS2o6v9KqcAMHQoJ7M2sVFqqVMWZaZO33OfaUE+/fsZ+70OaqZ0ehoWLu4SHN+RlByCQgmfmRCzR0MYHPYjwCqCEuzFpo0hSotoVULN0vmFqlOHd0pvMPrE1Pvi+5g0+pNvD4xlTs+GSElCZbMLaJVC+1H0Z1ORSYCNnhk6JdgXuVXAa8F+HN2ifUhOIQAKT1dvDJW3b/JsszqxR8xd/rce3J0UHS3iLnT57J68UfCNZFXxhaT0jOEjNHA+q0GztlVpj8fRXYBKVcBbA57NqBK8rtsrVH0pUExebTTbzay3Tt3MemFiffUPEFudi6TXpjoN+zcoAEuJo8OwaSivIzL1gr9slSP7AISbGe+Fp8dQ4V3YOLMKNGQIygWzyli6rhi4SHUUz+e4uXBo++JGcMzP53h5cGjhUM9SYKp44pZPCc0i1dQqMhAkF/4GIrMykUKdonWarZ0RNlJ6mX7Bw1whfwDAHbt0zNttkmYBSOqVhRjp45j8IjBNS7hZHFxMds3b2fN0tXCWIPR0UqfH6rZB5g+xyRy/JzAYzaH/aTgIyqCVgAAq9nyGqCavVn4RhFD/xD6Dzl7QceY6SYuZYi7lEZNGjF26lj6PtOv2kQg9Ycsy+z5227WLF0jPKMIylBv7eLQHL4Stn2pZ+bbQscv1eawLwj2OVoVQAK+Ap4qW14rCr74+C5tW4b+g/JvSEx6zcThAHHwWz3UmgkzxlfbHIQnDh1n5aJVqvX8siQ96mb5gtCGeiWcPqdjyJ+jEBiWb4ABNoc96IdrUgAAq9kSD/wTnyQTifEyX6y/S2ONhxbL4nLBW0uMbNoWeITRpUcXJrw6sdqkozt/5jwr310h3MZVlpFDncyaVhzSJE8JGdckhoyOEkULzwB+H4zjVxbNCgBgNVuSUXaWev2UFs1lPl97NyztBvjr13oWLjcGDIkuSRLJ/ZJ5on9vknomVfq+wNuO2xzed5gfvv6etN1pAbe7JcbLzJxUHNLcflnyb0g8OyZKdNDTBfS2Oeyaj2KFpAAAVrMlFXjHt/yR9m42r7yreRXLl8I7sH6LgdUbjao09L4YjUY6du3I432T6dGnR4WlqsnJyuHAdwfYvyeNk0dOlru7qU60sp4/ergzIu0xYkKUv/zBr9sc9pBW1kJWAACr2bIGGONb3qu7i7XvhTaf7UtevsQH6wxs3WEIavZRkiTadmhLct9kuvToQmLjB4ipH6PZeZRlmZvXb5KZcY1jB46RtieN0z+eDmpjq8EAwwc7mTLaqXkzhwiXC8b8xcTeg8IGXWtz2NU554MkXAXQA9uAQb7XenV3sWJ+UdiaX8LFSxKLVhqFmcnLw2Aw0DC+IXHxccQmxBGXoMQQLAm/mpuVQ252LjlZueRm5ZCTnUNedp4qLEsw9O/jYsaEYs17+PxReAcmpvoV/g5gqM1hD7lvCUsBAKxmSy0U77On77VH2rtZtzQ8j9eXf/yk472VRo6e1KnyFFcVOp1yaOMvE4o1b90ORP4NiZenmvyZ/X3AUzaHXT0NpIGwFQDAarbU81ToYd9rLZrLbFgW3uhARE6exJ40Pbv26Th0XJ21tKIxmZSDmik93fRNdmk+sVMeGdckXpwidPgA/gX0tDnsN8L9nogoAIDVbElE2T+gUoLEeJmP3y8Ka54gEAUFSjrVXXv17D2kzmEYKWLqyvTq5iall4teSdpP6QbL6XM6/vyKyd8o6F/AkzaHPTMS3xUxBYBSS/C/CLqDWlEwd0Z4M4bB4HQqiSzTjui5ck0iM1siK1siK1ed2sYfJpMShy8hXiYxXuZ3D8gkd3XR9VFtkTlCYduXemYvMokmeUCxsn+MxJtfQkQVAEp9gi0IHENQ1g7mzSyiKg715N9QlCEzRyIr+9d4+4lxMgnxJX/liPoswVJQCG8uFM7tl7ADGB5un+9LxBUASkcHqxAMEQEebC6zYuFdWlqqLppodeKcXWLizCjO+4/OthYYH463748KUYASPJNF8/CZMQRly/iUMcWMHuascLNaXXE6lc0cy9YaRUu6oMzwvRnqJE8wVKgCQOm08VZ81g5KaGmReWtmkSquwL3O0ZM6Zi00BdpUkwEMC2V6VwsVrgBQuoC0EZ9VxLIMGuAidUpxxIdT1Y2cPGVndTmnrr8BRmld2AmFSlEAKF1KnonSJQiNfl2zzIvPunhpmLNKHLGKJP+GxCdbDWz4XC/aul2CE3gTWKhlSTccKk0BSvDsLFoF+I3FHl1bmUt/+QUnCRqzYlQ3snIk1n1qYMt24YmdshxDcfSC2skTKSpdAQCsZosOZYQwH/AbzdFkgqEDnYwa6qKltWb5COdsOjZu07Ntp/CgZlnyUTbdrrU57JX+I6tEAUrw+AbvAi8CAafv2rVyM2iAi4EprmprFbJyJHbu0rPjK70oOIMvMsqZi1cro6/3R5UqQAlWs6UrygmWcgP163XQrbObgSlOundx0yixaut/NVPi4DEdO3cZOHRcJ4rJI+JbYE55hzYqg2qhACVYzZbOwBvAQMqxCCU0ayrTrZOLpE5ukjq5iY3A+nsgcvMlDp/QcfiEjkMn9KIgjP6QUU5cvx3orF5lU60UoASr2dIBRRGGEPzZBSQJmjSSsTZz06K5jKWZTItmbqzNZOJj5aATYZXkTbKlS1xI12FPl7hwUcKWruPyVWHunUC4UQJuvO3viHZVUi0VoASr2dIUGAGMBNqG8yxJgtq1lRTq5jpKIuWSXLqOAiWpouO2klqtJHNamJxGicmzWRSZo7pQrRWgLJ7uYRTwPFD12anF5KDEWtxYncx8IGqMApRgNVuMQHfgCaA3ynyC8IREJVCEMn7/HiX86sGyQRhrAjVOAXyxmi3RKArRG3gMaI2fdYcIkIGSaeMoitAPlkTdrqnUeAUQrZX8/gAAAC9JREFUYTVb6gKtgDYoCtECqA/EAHV9/oKSPfWWz9/rKDkVf0bJrHbWk1/pnuL/A6t7Ru6t8SvPAAAAAElFTkSuQmCC";
            doc.rect(138, 20, 53, 30);
            doc.addImage(punctuality_percentage, 'PNG', 140, 26, 17, 17);
            doc.setFontSize(14);
            doc.text(pdfDocVariables.punctuality_title, 140, 25);
            doc.text("Punctuality\nAbove 50%", 160, 31);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_punctuality, 164, 46);


            doc.line(20, 53, 191, 53);// horizontal line
            doc.line(20, 55, 191, 55);// horizontal line


            var adequately_marked_exercises = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADngAAA54BEWVixwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAArvSURBVHja7Z17UFT3FcdFRBCJYpRH6iOACZgo5oEao+CgQR5NDanTGG07VSsZrZMWOmnBpGlKJpNXH+JoFZ1uJpM0NU1boZUEa6JuYoqmNRYivuWhBEPNSiFGJSB4es8iHXbvj+V39/7ub+/uPWfm+9fufew5n733nt/v/M4dAgBD9EqxaEXrFbUqApJTZxU9pShChI+N0hBBAGyggA+oD80MgYjgBytqo0D7JwQiAAijAHNpvxkhIAAsDoFhANhsNjh48KClVFhYyAvByIAHAB1iNSstLdVyJRhJAFgXANQHZoCAADAYgKCgIFNDQAAYDEBcXBzk5OSYFgLpAJw5cwby8/O51Nzc7PcAxMfHw86dO00LgXQA7HY7932yuro6IACoqKhwQpCdne3p977vCwgIAEkAmBUCAkAiAGaEgACQDIAGCMIDEoDa2lrIzc3lUkNDQ0AC0AdBVlaWJwjsMiCgNNBHAPRBkJmZ6VMICAAfAmAGCAgAHwPQB8HChQt9AgEBYAIAOCHYZwQE0gHo7OyElpYWoWpvb/d7AHwFganTQF4tX748IADwBQQEgMEAhIaGQlJSkiYlJiYONosoDAICwHf1AHpVRACYDIBt27bJBKBeURABYCIA6uvrB7t0i1aY3wHQ0dEBjY2NQuVwOEyTCubl5cmEwP8AsIJh0cvGjRuhpKREiAoKCggAKxv6kgAgAAgAAoAAIAAIAJ12vQug6wLAlRMAXxwA+PKwkpqcBei+RAAEHACdnwE4dgA0Pg1wejXAsUcAjmQPrNpFACe+B1D3Y4DmTQCXDinAXCMA/AeA6wBXTwP85zUl4Gs8B5tXR78JcO4FgDa7cpW4TACYEwAl8G17AU6uEBP0Aa8Q3wD4dL1yC7lIAJgGALyHn15rbOBVV4WHAFpeBei5QgD4DICOeoCGp+QG3l3HlwBc/KvhzwkEgLt9/kclADm+DX5/4RUIMwsCwGAAejoBml40T+BdrgaPAlyuJQAMA6DLAXDmh+YMfv+HxNZKAkA4ADhoc3yZuYPfX+c3K88F3QSAEPvqXG8u7i/B7w8BAaDTcHj25ErTBTfx1pEQM3a4i/bZZqm/K+h2YE0A8BLasE5XoHpqsuDt36bAzx6bDJVbUoQBMDpimOr37iqdwX4mEPBgaE0Azm/RHagn8xJczunF/ES5APRlBzpTROsB8N89uoN0+u15EBTkek7BQ4Pg3O50uQD0jRPoGCyyFgA9V5V/zVLdQSr6fgLTaT9dES8fABSOGBIAHHbhDd0B6vp3lvPBjHVeY0aFwJV/LeTaT/OedMidH63SsGB11e/9d0WqvvfmL+9yHTb2cu7AOgBcaxOS8u1Yf4/HcurSp6dy7efE39J0lW3/4ge3ue4TJ5AIAE8PfpuFPKRlzx3nMTB3JETA9U98AADOInoxlWwNADrP96ZNOoOPD3lDhw6+MOPvW2fIBwCF9QQEAMNabEL+/eh0nuDkpEb5BgCEXGNlkTUAOLVKd/Bx4GdibJjqXFhXBEwRT+5M87i/C+8vgPzv3KrS8JChqv09vCBG9b0BMwMsLyMA+o/3Nwn597+zOYXpqJcKEpkQrH10krw0sL+wxpAA6F/g8ZYQAPBf6H4eUWOGQ+fhTOZnI0cEQ1vVA/IBwExHw8BQ4ANQl687+C375jPz8yeWxzk/t78yi+nEXz8xRT4AKCw5JwAw928VUt6F4/ys8+h/n5+eeJO6n//XRkC38uwgHQBcd0AAKHbpY93Bx5x+8sRw1Tmk3TvG5XuvPDuN6UgcONJyvIzZY+G+5EgXHXxjtrbzxsUnBABO/OzWDcBe20ymg15/frrL9zoOZcK4SPUQ8byUm+XXFeAKJAIAx/6363bm0uxbVMePvCkErh5Sj/ljbQCzM/mf5kiuH1xEADgN74U6HHlx/wMQOlydlz++jJ3ind87H0KGqR8WV+SOl38V4FyQGtgAnC3W5cSSwilM53zyl7kDbrMsR33FQIhw4EcqALgq2fIA6Cz1vnNyhOrYs6aN9rjNR3+YzXTos2tvkwsALmuzPACnHvPagVWvswP5u+Jpg247e3qkarvYcaHOQSNpAGB/AssDUF/otQPxvu1+3IjwYPjyo8ELPrBYg3Xev39hujwAcL2D5QFoetkr531xIAPCw4JVx81bPIFr+2vVWTA+Wj1xNGPqaHkAcBaLBjYAXk4DY1UP67j/3H4/9z6e/xF79PAfr90nBwBsV2N5ABzlXjnv3jtGqY6JQ71a9uHYvwDCQtUp5COZscYHH9vV0DiAYu37NTvv8FtzmA7Z9OSdmve1SrlluO8HJ5Wa3k03FgDsWUQA4MLPk5qdt2bJRNXxRoR6N7V7ZMdcpnMLV8YbCwA2riIAbiwBO/YtbsdhSfcoxmwcFoJiCZc3unuK+nZy82j+8nGvhN3LCIC+TOBX3I579blkae3Yt/58qnEAYAs7AqDvOeBDbsfNuTtSGgA4ymjM/X8NlYSploPVPjSo446Vp8p8GYNTu7fOEA8A9jEkANys8ZlBHVfw3TjpAHw9LUo8ANjMkgBwLwx5z6PTvvo4E8ZGhkgHAMvHT1WkiQs+NrPExpYEACMb8LA2YPvL7LH7ik0pzvRPhGrLUpnl4wPVFngl7GhKK4O0PwwumDVWdYyECeFca/y0CFf1siaY2g9kiOknqPHfb73l4YwS8bp31M0eUDiWL/r+jL1+WL/nNz+ZIm3+39oAXD6icty6VQnM4drP9s43JE1Lvl1dPh4/foRz6ZnX+8W2ttQgQntGgNO2WKjhvn+8VBs1SIMFJazfVL7hHi/3mdPb25gA4DRcQ3/8207nlZWwmz1g9y+jAMBqYlbGkT7Ty/Jx7G2sw6zZJQwniWoXOfNw931PiAnTvJpHb4exPtX8ea62fWFvY51m2UaRTbVvMtOyZ9YYX7z56XvpzLWGKx/WUD6OBa/Y4JoA8M6Ki4uZa/1FtHrj0ZKsWGb5+OcfcJSPY2/jLjGvtLUkAD09PTBp0iTmtK+smr2Bqo6fe/z2wZd/cxZ8EgADWGVlpZCFnHqFRaLu53BLVKizFR17qHdlb4NrgWZJABYvXqzaZ0zUGOiqflAqAFgmzpomxsEpda6/zpD3D1oSgLKyMsjIyHB5FXtRUVFvdnAjRZQhXCiC4xA4EvngvCh4d9vMAdrDbxH6jgB6COwbGa6rcwY+NjbW+Vr2/48TcEwfixI2f8Dew+yHvaW9vY0NNHpjiGLd3d3sYWMB7WW8fqEktrXFYhaDjQDgmUUU0GaO+71A2NEU29pKMgKAt54Ai0rw1sBRXqZZCBiuYsKOppKNANA8iHC196qA1cYaSs5VEzh4e8EWdtjH0IdGAOi9MmDmgCuQcBka/otxQSquSsal6Thci00qsFMJtqvBnkXYuAq7l5nECACLGwFAABAABAABQAAQAAQAAUAAEAAEAAFAABAABAABQAAQAASAnwFQVVUFMTExXDp69Khq+8LCQq5tU1NTmcdPTk7mPr5soW8CHgC73c69Nr+6ulq1/erVq7m2TUpKYh4fHS27xwCv0DcEAAFAABAABAABEKgAOBwOKC8v51J7e7tq+5qaGq5t9+xhV+ru2rWL+/iyhb6hNJCMACAjAMgIADICgIwAICMACAACgAAgAAgA/wLAZrM5T5ykX+hLvwOAJEUEAAHgewCCFbVRMKQLfR7scwBuQLCBAiJdJUJiJwiAaEXrFbVSYAxX6w1fR4uI3f8A6GJNyL+CJlsAAAAASUVORK5CYII=";
            doc.addImage(adequately_marked_exercises, 'PNG', 23, 66, 17, 17);
            doc.rect(20, 58, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.adequate_marked_title, 22, 63);
            doc.setFontSize(15);
            doc.text("Marked\nExercises", 44, 69);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.adequately_marked_exercises, 47, 84);


            var with_record_books = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAB8VAAAfFQEIJ2vyAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAihQTFRF//////8A89Vc/6oA/8wz/9Ur48Yc5swa68QU89Vc7cgS89Vc89Vc8cYc8skb9L8g9cIf7sQa78cY8MEX89Vc89Vc8sMb8sQa8sYa7sIc78Mb8cUd8cYc7sQa78Ud8MIb89Vc8MQa89Vc8cUb8sMb78Qc8MUc8cMa8cQc78Ub78Ua8MQa89Vc89Vc89Vc8MQb8MUb8cMa78Qb78Qb8MMc8MQc8cQa8cUa89Vc8MQb89Vc8MQa8MUb8cQb78Qc8MUb8MQa8MQc8tNX78Ub89Vc78Qb78Qa89Vc89Vc8MQb8MUb8MQb8cQb78Ub8MQc8MQb8MUb8MQa89Vc89Vc89Vc8MQb8MQb89Vc8MQb8MQb8MQb89Vc8tJO89BL78Qc8MQb8MQb8MQc89Vc89Vc8MQb8cQb78Qb8MQb8MQb89Vc8MQb8MQc8MQb89Vc8MQb8MQb8MQb8MQb8MQb8MQb1tC719G82NK92NK+2NO+2NO/2dO+2dS/2tO/2tTA3NbD3dfD3dfE3djD3tjF3tnG39rH4NrI4dvI4dvJ4t3K493L497M5N7M5ODN5eDO5uHP5+LQ5+LR6OTS6ubV6ufW6+bW7OfX7OjX7ejX7enY7enZ7unZ7una7urZ7+va8MQb8Ovc8Ozc8ezc8e3d8e3e8e7e8u3e8u7f89Vc89py892B8+/f8/Dg9N6G9OKd9OSk9O/h9PDg9PDh9evH9e7S9e7W9fDh9fHh9fHiSvpmDQAAAHR0Uk5TAAECAwUGCQoNDQ4ODxITGBkeICEkJSYnKC4vNTY8PkNERUVLTFJTWVpgYWNjZGVnaWpwcXd4fn+DhYWHjI6UlZucoKKio6SkpamqrLKzubrAwcPExcfIyM7P0NDS1NXX3d7h4uTl5uvs7fDx9fb4+fv8/f6tMyeMAAACcklEQVR42u3aZ1MTURSA4QPYe++99967Ym/YY8PeBWsiKiJ2EILE3rCAgbVLbPl75iZZZSaJy+4qrzPe99OZcz/kmcnu7MzOiuh0Op2OzmOjDc3/AsCw0ygasI4GGJ1owDQasCcdBhj9aMASGpDbEgYYo2nAehpgdKYBM2jA3nQYYAygAUtpQG4rGGCMoQGHtzlr4Z8COM2jARqgAf89YPvWxLIaEpD9PLFlNGAsDegCA/YJDMikAcNpQHsYsEtgwDwaMJQGtIUBOwQGzKEBg2HAqdYwYLPAgFk0YCAMONEMBmwUGDCdBvSFAceawIA1AgOm0IDeMCCnEQxYLTBgEg3oCQOOZMCAlQIDJtCA7jDgYFpywIevNvvsELA8xUuqj2GbfXEIGEcDusKA/QIDFtGAEZaAYGW8anPzzhxeqXXIHaCDBaDqivdnV9+qzcvLvmu1aqi8FN1euOsGsFssAH5vnQJqUxIZbquhOL4t+O4CMN8KUFwXUKY2RZHhlhqux7dnQy4Aw6wAz879+v38KrV5muctqFHDk7zo1lfu5i9oZwUIh+7fiffwW2zz6bE53FProJuLcKcIexvOpQFDYMDpNjBgi8CA2TRgkDWg1p9/JrHzN2MPhsDFkmoXgJMtrAEBb/JK1eEjc3AI2CTWgBspAIXqsNwcHAJm1gPwIAUg+jgK+uKDQ0D/egDCL8pKE/NXxA5rAhUuLsLjTYW9C9YKDJhKA/rAgJzGMCBLYMDk3wNev7fZG7uAXuQXFBHA0QwYsEpgwEQa0AMGHEqDASsEBoynAd1gwAH4U67sxTRgJA3omAzgabgWiE6n0+n+xX4Aj4wEZA3pk5QAAAAASUVORK5CYII=";
            doc.addImage(with_record_books, 'PNG', 82, 63, 19, 19);

            doc.rect(78, 58, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 99, 63);
            doc.setFontSize(15);
            doc.text("Record\nBooks", 105, 69);
            doc.setFontSize(20);

            doc.text(pdfDocVariables.record_books, 106.5, 84);


            var received_grant = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzA3NEQyQzRFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3NEQyQzVFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1RjcyNjY3NURFQjExMUU3QTgyOEYwMUNCNDExQ0RFRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1RjcyNjY3NkRFQjExMUU3QTgyOEYwMUNCNDExQ0RFRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtwUdIEAABMUSURBVHja7F0JWBRH2n4Hhku5EQZB5BA5FAVBEQ80mhiN0Y3ZRLMmJmvMxhwmJms2ye7mWjf+mmP1T0w2Me6/ia6rcd1cqybqRiWKB2pEQA5BBOU+5L7P+etrUKenRxhgZpjp6fd56mm6pumuru/t+r6qr+ormVKphDlCJpMtYYffsTTJDF+/nqWdLK2TmSMBmPC92eEqS3KYNz6zMNMXf0gSPodHzJUAjpLsOdhbSHVg3pCawW5MWBmECU8Fi/odP4/aJ8iTWgAzh0QAiQASJAKY1xiAOzuMkkRvJkYgE7gdO0xnaQ5Ld7EUQdmS6EVKACZwatUmqAichG8jiVrEBGBC9+8WNgl9NktukmhFTAAmcJduQd/4ygddp5cmVaLo7HVAT74VW1cbBMz1ho2jlfkRgAmcmvCpKl951EAMWJshcrQ0tuusfId/exZ5x0v1Xg/nPkjH3L/GQBHhKm4CMIGTkTZeReCxLA3p7/0c3W0xZroCYTMUGBOrwPFdOfhuU5pOylp1pc4gwie0N3cg7csccROAyX44O3zJ0syBfOHBMe6csMeyNCLEGTI92f15x0sMWj/58WVQdiohs5CJtgXY1VfhW1jK4D/etUvgMzwRONENcivDDHFUpNcYtHI6WjpQnVMHl0BH0RIgSJuLPAMcuK+bhB46TYEhOjaOtEFnhxJlF6sE+fc/Nhnevrpppr/9xxkUXqvk5ZVcqBQ1Aeo1ZTq42XTrcU9O6G7eQwa9oMl/v4zG8ma+veFshz/99SHY2OqGkDZ2Vtj4x728vAufZcL/Li/YulgP+P7GOBR8WD0jZtFIbE5ehGc+mYLYX/kbhfDpyycCqGP+kiidCZ9w3yPRsLTki6m5qhUn1yVztoAYCfCjekZucpXejLi+gir94vZsHFh5Cp3tneoGLBY/MVWnz/PwcsLM+WMF+dd+KsH+5SdQc7VedCogjmwdlixvDrLk1qGisHHQvvw69uyiM+WoyKxBKdO/1PXThCdfmYOxkT46f/5bHy1BUkIuKsv5wi5Pq8Z3S49hxFQPuIU4YdgYZ3ix3g8ZxSZLAKVSWcO+pHPU8qvmp7Hu1oylAQb/2uNePc99bb01txOnj8ILa+/VSzkU3s54b9tjeHLBp1Cfxd3R2smVjxJhqMIW014Lx4hpHiarAjTaAWk6GGy5fO46tjyXgCPbsrXr4x8rxdWjxb0KP3yyHz74coVAV+sS0+8OxdpPHoKVtWWP1zWUNuPnjzNM2gbQaAeknygd0DB7/O5cvPPgUSR8ew11lS2aRqA0DLr0TrrHX5yNnXEvYpin/icaL/nNNOyOfwk+/sN6vK4yq5YRocmkCXCayMzTw0xoeWlV/b7h0R3Z6GhX9qR7hFkdmq+nr3DuAxHY/uNqvPr+/ZBbWRqsYsjG+ObcK3jxzwvgG+h++9fpUJouAZiea2OHY7pUA2RE6gIPPTkNJ/LX48PdT2DyHaMHpX4cnOzw9B/m4lDGm9h17LdwdOm/cWzMU8IEaiAtvv8E0NXQcESMP5xchxhNJUVODYC9o60oCSAwBLNYV6ytpaNfNxuMoWJTgNESgKmBVHYoVs0j4WfRpIt+oLmhXZK2ibUAhCOC3kA/1MDVlCpcz2+QpG2CBBDYAan98L+f/OqqJGkTJYDADshLrUJ9VavWN0g8WIjDX1yWJG2KBGB2QBG1+urddRoU0gYX44qxZdVpnXjNJAIYU3ewFzWQcaoMGx44io3LjqO1WdhrcBlmLyohevm48Lu8dpawcbYWDQEOazseQCN9u9cm4d3FcchMKNd4zYx5Y7D0qemiIgANEasi8F4fWA3Rzs9nCtPCf6IeIEs3O/Jk0ZderYfCz16th1CCg1szb3ujwFBPbNr5OLZ9cFRUBFj48ES4eThg++fHoAyTc+sHtIXRE4DZAfUymSwBXVPDeWpA4RfIu7a8h64ejd2//enSAY2aGStoIsq0OSFwjnLE9wWporMBtFYDLp52whe0kGH9/z3Cjd0PZMxcMgKNzBDMYD2BTjWPV8gUD1jK+W7dTtYDiJ45WpK0iROAZgjVqmY01rYhN4U/XdrOwQoBE4TrQk8dviRJ2pQJwOwAGsiPU8/XNCxMC0PUceJHiQCm3gJotgM0jAfQ+j91JMRlcapAgmkTQGAHZP9cIVjl6x/hxqkCVdRWNSL1fJ4kbVMmAFMD1MHPV81rb+sUDPiQERg61cOs7ID62mZsen0fVk74GPFvJaGuoEGULcBt1IAGO2Cm0A44KWI7YPfWE9j67n9RUVSHy/vzcWZjmmgJoGGamHZ2wIWEXDTWt4iSAEf2pvDOC06WcauIxUgAmiDCs+YKLtWgpoy/QFPh7wC3EUPBVxcdOHs8W5QEmLVgHO/cm8ZDbCzFRwBmB5SxQ4qwFSjVqhUQqx3wq5XTsWLNnXAd7oBR93gjes1Y0aoAzWpAQ3dQ03iAWAlAS9JfeXcR/pb0HGaui4ST71BRE0ArvwDFElAPo5KdUYLSwmqp76cCUwwTF88SWXM3gz9WlzahKKsWXkG3lmfZu1jDb5wLcpP5w8WnjmQOSqFLCqq5sQhKaYn5yEgugJu7A8KifDA2aiQ7jkTIeG9Y28glAvRiBzTKZLJTZPuo5tNkUVUCdKkBhYAAJ5ka8OthSZWuUVvdhPVrvsJ3O84KfrteUovMi4X4elsCd+4+3JFzWd9xb5jBymeqwaKFi0e19AucZi2AofbJOnYgDQvC/0ej8DWhvLgWTy/6DH944p+oq2mSCNAXO+DSadb3VYvYMXrSMFjb8btDFWV13Fenb/z5+T146hdbUFbU9yhiFBhqYcR6pCcVSAS4Dc6zxGvbaeXPlfMVfP1mZYHgGOGwMDmH9An64ndtiR+wzfDyo9vR0tzW67Xk6PrphzR8+OxeZH2XxwWUFLMRSHZAJ7MDaGLfg+q9gaDJ7gI7gKaHq4LGzvUF6mWQztcEit4RtMgX7mOduZAuTRUtuJ5ejcLT5cg9XCS4/sqlEmz+0/d4+Z1FPT5z785z+P2KHV0nX3eFjpn22nhRtwCau4Ma3cOeBi3UG09/yRl+6ghaNBK//PcsbnMqCt9i52YDV2a0Uv6sd6Nw9+bJGOIhnK/4xf8e5eID9YSvPj/FJ86BArRpGQvZlAkgMARzkirRVMdvMkewL83JwzATQeMPpeP4wXRBfvjjozH9jXBYDb19g0ukWLgtFtb2VoLmfcNL3/T43KJ8fuCM9qYOtFS3ipsATA3ksAPv06A5grQoRJvegD6QeCpHkOcc4ICIlVoFP+VUxOSXhcO4qYl5WtkC5mQE3rYV0OQeDotVGKQwqefzBXlRz4bA0vpWNbfUtiFFLUgVhaC72XNZ4MOpBlVQ7+ZSSqFEgP7aAWNmGIYAaYnCWUce4fxlW4eePc3FHVTFwWcT+P8z3kXDvfP1UmZT3zKG3MOdqkQuyalDZVEjXL1urQFw9rDjbAFyHesLxXlVgkCOQxV2sHPtGrFW3bXzekYNMr+5xrv2xu8rzi/kAj4C/N9Tf86TCKDBDqhk3cEk9mekaj7FA1i4egzfDoj11CsB2tp6trpJsISdsw5i4vOhCP6lL3dORPj5oww8EjdvQPc3SxXAhP8LUvHq+Wf3CZvLsJn6VQPefm6CcHEUq69JLSbhrHeiOIHvXXacS/Q35amCxgbU4TfaQ2oBNAj/3ywJ1kEXZFRzcQUdXG/tFkcjgjQySBNJ9QGKEjoyYBhyMvlGaFlyFXxn3eqFeE12x4P/uRMVGV1Cdgt1FmwEVZYijIfoH6SQWgBthN+lGrp8A6ognwD5BvSJ0IgRgrzzn1zi4vmqggRORKCkLnya1EmRPtUREu4tEUAb4d/ApZOGHw949DnhTje0vUvSVu18DxTn98z7whm9FCc4IFhqAbQWPiFdIwH0awdQEElN0UOTv7iME28no62HUHU0k3ff8ni01gsHfJ76/d16K7NcDMKPCvbBhcsFvOVfxdm1qC5r4rqANw2pcS7cTKG+BJnqK557cz7OHd8sWIpGXrpCppb64gwiTJkdjEmxgeZNgJ6EHzPGD8ujhuLj1uFIzS0SqIGY+31v3cdCxm0wdW5/vt7KSsL646YHsO7FrzQ28bTfj7bwD/LApp3L9Vq3cg2VTfv2zWBpIro2b9TnxsvkNqPI4LTy9wzr17f2R/jWcguM93UVECBdjQBd3UFPvRKAsGzVTJQV13KrdfoLmh729x9W6T2glVylomkK7fMsbcAAdunsB2gEZC2RgRXhdUaCTX0VPve1OAvNGU2OoTADDQuvWbcQru72+Gjt92io69uKJApo8faWpfDyddV7OS26K5oqmCZYfGhg4auClPVGVpb3ZV3QWviEEQ4dkFvyB2LKr9WjQm2hJK0YopVDhsDyF2bhQOobmL8kUqvradOJ97f/Gv84vLrHvQB0ql5pDxpW2evY368ZkdonL99MbYV/Ax+frEJiVgHEApoiHhTmxW0SMW6SL7eFnKYtY2YHvoUitc0ll+y7E/Ze/G9Z1R9xswVgwo9gx1eN7N3n9FX4hHE+LhATWlvauXUE//rbSby+chcemPwet55A1zbAw+rGoExuBafIe+Accz8sbXs3Qq68t1iQF/DKnl7/r7O5AdUJ36Im8QCU7T1PeOhN+ARfJ4gaWalFWDzlL3jzoyVYoqP9CeXd1j4PHvNXQXHfSwO6sX3wFK2ucwy/C6X/2YjSvR8MSPgEHwclgn0UyMwvFS0JaJXz26v3IDzaF8HjBj48TDUapZ7pGrvUoC/V0/O0FT7BkvXzH412Rey4AIFBKCa0tXbg1eU7uKMuWgDBfmdWrl4GfaHbPa8vwr8BL0c5Hp9IaYzpf+0dSpQ0KHHwUiNOpfFnBtMUsR/2nMd9y6IH3g00VvRV+GKD3FKGEY4WeCLaHlPG+gl+v6iDWUJGXbvmLHyeUc7S3GChMa5pDqKoCCDhFlxshBNZrmWXSwQwF1S1CEU1KsRTIoA5gBzLhzLrBfnjo3110guQYAK9gNNpVwW/h0f7SQQwNAprOhCX24rjKblMQB2DWpaXHt0G91ec+hWDQCJAP7ErsRIZV0uMoiw0EKTuBJJsAD2ioqHNaISvK0gE6APchloh1M/TJMraXNUqEUAfeDjSFbMnBBq9r0F9BbJkA+gI3k6WWBZhx9Lg+xqa2ztRVG+BrxPLkXGNr5quHi1GRWYN3IKdpBZArLCVWyDAGXhhhhv8hwtXPRWduS6pAHMA+UwWRArnBlTn1kkEMBc4a/AV1Bc3SQQwF1Rr8BU4ePc+wVsyAkWAVmYM7k8sFuS7BDhIBBAzbvUCKpBbrGbwyYDhWiyHlwjQRxiTL6An+N/lJYg2JhFABzAmX8BthWpryYWn0waSEdgHmIIvwNrBCvM+iYHjyKESAXQNY/cFDAt1wvytU+ERrv2iUkkF9BHkC4hzsTcqG4B0/eTfhWF4lFvf1YUk0r5hsHwBlUz95NTbYNvRDDQ28z19FIyyP8KXVIAJwZWpn4mKTqy5J1jwG4WVU/Zzd3SdEMB2RGiP5xJ0B3L+RAbxw9HR3gAVmbWDRwD3Ob+hKBPdAxCyrnMJeoOPm3CRSGVW/+YF6sQGcJm+BENGT0LthUNwnDAXNgp/SUp6RH6FcIq400j7wSMAgYTuPu9pSTp6Rk41BFFQKPqZa7Dj4BJAguF6AYKv388eVkPkEgEMAWP0BUxcFdLv/5UI0EcYmy9g9EIfjLzDc3AJ0FZVjLJ9m1Gb9F84RtwNj4WrYeUyXHTCNzZfgHuYC2JeHtg+wzrpBpLwK479E201ZdyRzsUIY/EFWMgtEPlMCBZ8Ma3HregM1gLQl69+7v3YhgHfl+L+3xheMBYMli+AtpSjzSWGhThxG1C4BDrq5L46IQA1+/Tlq57rRN+mNGFxmC2sLY2HBfrwBbTSKmDWtd+XVoPzmcI4xvM+nQKvaP1sdqETFUA6323mMlg5eXBHOtcFjiRmY8upKtQ1G+/MG12ACD7SSYZnpjghfJQwYFZ5apXenq2TFoAMPmryddHsqyMpuxAb65rwTKw7FA7WoiaCBdN388cNQ/KVIjUCVOvvmaZQMXmllXjvUD7yapQQO5zkwkWd2izwGAgBBE/sbGkw6Etr87yqukZs2H8JGRXi9mDXtAtbOWc9Rjen2kwRVHbCtwZ9aW2f19Lajr/sS8H+rHY0tIrPLuhk3Z4fLl7X0N931tszaY0zBbPnxQtuzkuHhY0dbIePgkxurdcvv/LkHm7coLNZ4OE60d06CSa4ZeRfx5lrjbB3ckWbkl6iHTYmHFOQegGFrJXfmViDC5eFm0SHrxit1Sqf3nDhNruX0cYMSiNMd7BEfZ/TRlo+cSTaMILhGyMrWDyVq7tstJPI15Kw9EsA2kin1EgKdZnU3g0CqBCBQooXSELTaapTrWCFEbQEh1nyUxe+ShlptcN6MlMk4ekkbZF1V+xNdG/WdA9ubRunz9EX2iaENtK7yNIOVpYD2vwTKyPNiowl+0glic/9qD+Qxb2TpXX/L8AAK23SCjk4axoAAAAASUVORK5CYII=";
            doc.addImage(received_grant, 'PNG', 141, 63, 19, 19);

            doc.rect(138, 58, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_or_amount_of_title, 162, 63);
            doc.setFontSize(15);
            doc.text("Received\nGrants", 164, 69);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.grant_received, 164, 84);


            doc.line(20, 91, 191, 91);// horizontal line
            doc.line(20, 93, 191, 93);// horizontal line

            var school_support = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAejAAAHowBNXh8qQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA59SURBVHja7Z17WFPnHcftzbW7dN3WzRdlU6ttVUgK+ri2thWsgrWInW3dUwSn1W5t0a66ro9b3Trt3XWtruvNboBgFQFJoCAQLpKEJIT7xQACllsAEUQoUIRwOfu99LDRkEA4Sc45Oef94/uEy8nJOfl+3vO+v997m0VR1Cw+KDcl8juglaDnQJ+BikBdoFqQGhQDOgraBrplhue+DbQZ9CZIAWoHDYB6QfmgT0DPgJaDZvPlO2FDfDD+dtBpkAlE2ahu0BHQrdOc+ybQi6DOGZx7EBQO+iEBwPnmLwJdnoE55moCPWrhvNfRJb7GjnMbQW4EAOcC8KkdBk1UCuhJ0CrQ26BGB533HQKAcwHodZBRzpKRAOBcAFp5DkAlAcC5AGTzHAA5AcC5ACwD9fPU/K9A8wkAzodgB2iUZ+aP4EYlCQPZg+B5ngGwkySC2IfgCE/MjyeZQO5SwU0cm4/DUncCAHcQPMMxAC+JyXw+AnAj3fnDhfll+PMJANxDEMwRAKvEZj5fAbgeZGDZ/Gwxms9LAGgIXiF1v7gBCGIZgCcJAPwC4D6WAVhJAOCRIg7vvysj9mOKLX34xr4HXwjecCsBgGPt23L/LaFb/eW7g/2HQBTLugYKIwBwqD1b/XdxYLyZ/NYSADjS7uD1e7kGYE+I/68IABxp7w7f23aH+JdxCMC5LVu23EAA4FihwY+sjP7ktfL4sHcoNnTs8B+PPrfN35M0AvkVCipZDAMPkTCQL6V/x6MIqoGXjxwMbfr4rb0UG3r3wG/LQkPW78FVEAGAQz2/dd0dUA8PctgGuLpz56YfEAA4iwL8DnEfBax/mgDA1eM/ZP0jXAMQum3dUgIAVxcza9Z1u0P8/gBPgop9OwNNL+16jGJLL4SsLwYAtpM2AH+igDKWO4SWkCiAXwA0sAyAHwGAR1XAS7s2jbJZBex7euPA9s2rjx08OOv6qa7R19f3xkBfr2c2+nrnBq7x/mCzv/RnBAABNQI3+68s2rRGerel69u0drl0o493EZhP/V/LjxIABBQGPhVwPzb2WqDv8uQAH+9DAb7LQwJ8vd6Av6UF+nqbvm0+yMc7jwAgkETQ80HrqMCHV1CTTJ5CAWuW/44A4OBU8N9eDPqIrTTwuI4fPUD9esN9MzG/C54M20kj0DkRQCgXcwP27XqiyDbzvZI2+XnPJVGA8wA4xgUAurPHDwWs8QoAk1+Huv0svF6iTW8GfQENvoObfLz8SBjofADyOJodpDO/Fn9/6fdIHoD9mUFfcwTAsFjWB+QzAHdzPEOYjAkU+WohHxIAuFsyNoIH6wX1gLYTANg1/zczXM+XDSWRpWKdb/ytoFM8XicQQxlEAHCO+feC6ni+UOS44nAVRQBwXJj3J9CQi5g/Lryq+WMEAPvMdwNlupjx5orEG1AQAGZufgC9UwclAOG9BPwJALYZP5ve6oUSoPBWM98nAEyd1SsWqPnj+hL0EAFgsvlPg/oEbv7ExaXfA90segDo2D5aJMabq8rV1htyxuJODo/t9WknqMLMaKrkXCxVrpZTBu0XVKUueex3HkKAw9vX8Y5logGA3qXrz46M7fWpUVRJdhxVpT9L1RamWxUGg6dPgxKQp+ABgJu8ge7EccgXl684OVbKq/PTpjR+XOVqGZ+rBLz6+AbBAkCHeGcc8WXlpX0+9mi3xfSJwsDwvF2AB5k8JzgA4Ka+C0pzxJeE63JbS3wNHIfrf1zy8xWfu1ID8V1cVQoCADxsCpRjd6kHAyt0SVMajsEoU8mogoxoaBeccPUoIczlAYCb+IkjkjvFUOprCqyXetz4K4ZGIG4MCixUfNNlAaBb+wp7v4RSZbz1Ep+XAi3700LPF+xxVQD+Yu/Nl+fIrZp/PieByhVeief1tnQzMX8N3aJleNNR0MpPtFLqU6ki4Zd6c+E0+Z0uAQBc6BzQJcY3C6XaWmMPt+jz0k5QIk0d63EehdcA0CN4spibH2nVfBz34/+L1Pxxvcp3APbbc4M4hLNY32sSKX2K6M0f7ztYyUsA4MJ+BOpmenNFWTGW07fQECTGf0vFXCWJpgPgHaY3VZB+iqotUFhu6RPDLSmIVwDABSGmkzRxxg7H85MafLnJY9EAMduiLnLRhTwVAP9iejOWwj2c0hVxa99W7bboR+GKm8bEFgBwIQtAg0xuojAjenIHDlQFhenRxGBrafG0D7rrsn5f1qF6UjaUK/3HsFby+bBOkjmikxjg9QpolNYV+m+ZoJNDWsl7Izrpy8NazxAq12uBIwH4jOnNWBrAwdORO5xJnxLRVZOxP6VLvSHVpPW+CGZSjtCQTlKFoRjKlayjKjxmMwIA10OgLqYdPJYSPcT0b1Sd+YqhT7O6cFgn7XGU6VOoD54kicM6z40zBWA90yFc5n36NXjARvpJ0Rtfmf5qtkm7Ip8F061IqhnSSB+yFQBGj/9SVbyFkE/c8X6p4silfs0DedwZP0kplObbq6BaSvteZpLurc5PNWv4pQlhAAfzDKji721QJ3fwyPxxdZr0khXWAPBh1PLPnNzyL1WeEXXp79esKuah+bQ8s6wB8E8mN3tek2AGgELUpR+rPXVpKh268Q2A0c7U+TJrABiYNP5qzFK+Bm2S6Bt+Rvm8utYEt9LuzEVak8azm2vjh7QePT2Zi3WtiXOLm+WoYRIAdP0/4IjQrzjrNAEAAIAvmqJlwl/8VcVCVb9qSaVJ4/EVG4YPwGd1pS9UtSa6FcI1DE64HosAzGdyoxVmY/nxsG2S758EwGQloCstCXPPt33hrrmSuiAbnhSansw7c/vO3VXYn72kHMyrHshZ2jCYs6xtSOvZi4V/xn/D/8PH4GN7su7Ud2cs1lxJWZDdluyeA+csh/N3TPnZclRvCQA/Jjd6wazTh/T22QgAt7L4BGC0Qrd5/S/21r8rA3CEycQOUv8LB4Czjuj5wwNBCACuCYB25nP64iYBIJJx/dOqSe5eyVcAjHJUZQmAbHtDwAv6FGL+eOP41MJ0+LJ7eQhAX81JlGgJAAWTJNDEHkD8RCDm030B4fPqzkeg1osxSGmUIQN88aMcmj4Kpb7iyxiUbYhELWVhlquAJKbdwMVZsa4wT591AOCLpsYFMLRdjEaqxjNIQwNx1YmGd+HPaIxHGvyZGMSJ12INABkxznkAWFL5cdRVEYUM1SeRBpfOuliUXR+LlA1xSI1BaTqD9MZ4VNgkQ2VYYz/D3/D/8DH4WPwe/F54rGsq4Vzl4ejqdJ9rDYDTxDh2AeBQFgGIIsaJG4DXiHHiBiCQGOdIANzrXQ0AN2Kc41QecUcVXwEwhKNiawNCmol5Vvo9UsOoqoy/Uq3KbVSn+jGqJ2cd9bXmAXhdO/Z7a/Y26kLGATjuP2PH18k8DIYI1Mg7848jI0QUmdYASCBmm43sTTtCtSu3UCatt00DMUw6b6pD9QTVnOxlhHj8WlM8yqs9hdQQi3dwZTp8dmfNKZQD4aMerqnfYl8ADcABYjo9uzn12FipHtLdw3jsXb9qSW5r4tw+OjkzYpShcojblbXRSF0VhYrprNyoA80eBbNbqk6gEjBcXReDlDh/AJ89PG1nkD2DQoSmkrT34fH+oKOmazW3J7tXTJG1u9YsQ7XwpChojEe6hjiUUx+HVOMJnounkRKbiYV//l/CCI7Bx0Kp1sF78+E8tXTptiVTWG8NgJsYzQsQ1CyeQ9SgdoWjx+j1d6YtrOJ1d/AECA6LdybPe9SgboWzBmr2X07+ebErALA4l/vtWzmo8z912GPfenXg2daaOK+H1wDQEGSJDYBL2VtZGZ8/oF5W4AoAPCW2R/+QTsrazJzLKe6lXAKAG47TAYD3AegQCwAdqsdZnaUzqPEo4cp8CA2LysOR3pY1gt4Sg/n5Kf+mTDovtqdqDbUmuH3FkukjEDaWVUUhZVkYarLaF2Bl9682wa/akfEKJ3P1OtMWJV2MQYWNcagS4v9Go2xshNCIPUbDeYx1p1FRzSmkqohESkMEygezO2zqDJpi7z9hN/6UwZwA0HduaYGlTB6ouywcNcFjurI8AhWdP450lVFIXRGFVIbjKAekw8aej0AlcAweAVSLj4f3DdjVGzjF/gD5QgbgqnojJwAMqj0qeNUdPM1egILNC/RpfDkBwKSVNLkEADQE4UIF4Jr2Xq7m7Pe4EgB4h7BSIQLQq/Hh6Ang2eIyAExYPfSK0ADoUgdwAsCA2qOas/EB4SiD6Z5BD9u3bQz/1KZ8ihMAerOWst4ILA9DPZWRY2MEquzZNm6vkACoydjPCQCtiYtKWDL+UkUkUtXFoInLxDTYu3Hks0zWE+LryB87Rv0w1Uh9zFxF5QmkNEQiNcT6uTiuh3geVwt4GtfXMzB4BNSL8wFwHn1lFFLWfjMaqKQpHrUy6gyyEYKVoEYBQDB6TXN/Ias5gByPCzZk9vB4wraGOFRbH4NK62JRcX0sMjTGolqc8YP/dRptHwFk24gghruIKlw/Hbz/IzbX9Gs/+4s2XncHM9hNFC8wnQwyuSgExmGtNIadDOCyCt6PB7BzO3kv0OOgENAOV1G7KkgCBrU5GYCutiT3dsEC4Ooa0nmuApMGndUF3JEyv5onYwLrCQBWNKyTBoFZJkeb36VYWM+bUcGyOe8TAKZ8Ekh86L16HGF+N5R8PgwJx0vU5LXI5+xivHu4qJQjuWM4V3LWLvO1knQq7567jIk/XWyUuQW3yNEHYAKenjXAkul4karMFhnabUxyn2f39vGifBpoJavBTOUMwsTRsS1adNK11s5ZEesxG2L4XwIQe+D1MDySP21OQNFgVkqz3E3b8s0aQnhuYY+FxaVGaGMv47qcPjYPdKYlwe3tFtmcnU2Jbqsb4m93c+j28aKXeqnbiE7yLJgbD684cdRCNxhb4fcieJWPaKWhlO6eeQ793IOzrq+XL7jNGOv+4/qIBTc7+r7+CwrY4DGTSNpzAAAAAElFTkSuQmCC";


            doc.addImage(school_support, 'PNG', 23, 104, 17, 17);
            doc.rect(20, 96, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_received_or_any_type_of_support, 25, 101);
            doc.setFontSize(15);
            doc.text("   support\n(kind/cash)", 41, 107);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.support_received, 45, 122);

            var adequate_furniture = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAN2AAADdgF91YLMAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAWJQTFRF////AP///wAA////gICAgICPVVVjXlFrTb/y/00ai4CXioCVnWJYe1pjnGNa5r2Ua2t5g4OSc22AhoaUhISSXVNrW1JpAMR7gF5imWZZ/1EaSrnthYWQ/04aS7rt/04YXFRqSrjsXVVr/1AZdG2Acm6AmmVYXFRqXVNrW1RqdnGChYSRdnKCS7ns/04ZXVNrXFRqf1ximWNZ572US7ns/1AZmGRYS7nt/08ZhoSRhoSRmmRZeHSEeXSEhoOQeHWFAMN6XVRrfl1immRZ/08ZXFVqS7nthoSR/04ZS7nshYSR/08ZTLnsh4SR/08ZfHeHe3iGS7ns/08ZflximWRZ5r6ThoSQXFRqflxihoSRmWRZ5r6UhoSRf3yKXFRqg4GOAMN6S7nsXFRqb8Pid3KDeXWEflxigX+NgoCNhYOQhoSRj2FclmNamWRZzt3G1eDE5r6U/08Z/245/8CM/8eS/+u3xyVjjwAAAGB0Uk5TAAEBAQIQEhMUFBYYGh8fHyMjJSY2Nzg8PDw8RUVFVVVbXV1dYmRlgIGJlpeYo6Olpqenp7Cws7W1t7y/wMHEyMnJycnJytDQ0NbW1tjY2N/g4+Pm5ubp7vDw8PDz9P7+Pm8aaAAAAo1JREFUeNrtmvlzEjEUx+PWo9UCHtUe3hdqPSktHvXAgvctS609FKqi0qKs2v/fbFdmd0yybPLemFLf57cuefl+ZrJ50wQYIwiCIAiCkDJWLo+hTnhy6rUn8PCwcnzZ88qI8QPXPCnPHdno4WJx2P8UUWDK0xEo1mpFXIGjinzFEtQ4uAKX+Gw/fq0JKIbjCzzhs/1cAwk42YLb0sAtZB1MgcHpljbTg3hL4BjkcwMH7SXMtozIGmzDc++WfZo+X+90nhbMBAoGjejucijQXOo8dc0EXINWfCUq8LLztGXIn/Ljk6/qAvcPStf/bFTgFopA/9W6lKdbZAInogIXUAQm6zoCB6ICpzEEjijyFUuwIyqwF0PgIg/78D1xE2JvQoH3DEPgERf4lrwNs3uhwJIVgcuhwAtmYwnOhAI3mYWXkB0LBcaZhW3I9ocCp5iFRsS2hwK72b9pxZl8tW2Jaj7D8xfaFlnIsHzbKnlWtStQtS9gfQmsv4TWt2FAg6M+WgEbkQQhjwQ2uADCwQQmAD6aQQXAh1OoAPR4DhaAXlDABYBXNAgCPv7/akmupJKMIwES6CWBkZkGgJkRsAAonxuABRpAel+gAsuvgAVyMIEcWCA1B8mfS8H7QCpnvAqVXAqnESVtQclrSIAESIAESIAESKAXBUwggc0lQLuABEiABDa5QHpldXUlrRq959OXzx936eXH1/ydl573b7jnFQb7Fv1fE8zu1MmPrxHyJoI79gn58OvBr4rO6wjE1wh5btyXTFsXg8kea+R3qRHyYr9m2xbM1XyrIdClRsgjAesCpeDvknR037NgshsaAl1qhLzR9SelUfnwQw/8uW4P6WzD+Jr4PIIgCIIg/jd+A+MYLb+J8y4eAAAAAElFTkSuQmCC";
            doc.addImage(adequate_furniture, 'PNG', 82, 104, 19, 19);
            doc.rect(78, 96, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 102, 101);
            doc.setFontSize(15);
            //Schools with Adequate Furniture
            doc.text("Adequate\nFurniture", 103, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.adequate_furniture, 106.5, 122);


            var good_structure = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAN2AAADdgF91YLMAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAotQTFRF/////wAA/4AA/1Ur/0Ag/0kS5r6U/1Uc/04d/00a/1IZ/1Eb/1EZ/1EX/04c/1Eb/04aXlVq/00X/04a/08Z5AAe/04a/08Z/04Z/04Z/04Y/1Aa/08Z/08Z/08Y/08Z/1AZ/08Z/08Z/08Y/08ZXVRr/1AZ/08Z/1AZ/08Z/08Z/1AZ/08Y/08a/04Z/08Y/08Z5r6U/08Z/08Z/08Z/08Z4wAe/08Z/08ZS7nsTrPjULDfUa7bUqzYVKjTVKnVVabQVqXPV6PMWKLKWaDHW53CXFRqZoijZ4WfaYKaaYOca3+WbH2TbXuQbnqOcHWHcnKDeWRue2FqfF9mfGBnfV1kfV5lflxiflxjfl1jf1xif11jf3R7gF1hgF1jgF5jgV5jgjxUg2FkhWJlhWNlhmRmh2Rmh2RniGVmiGZniWZniWdnimdnimhoi2hojGlpjWppjmtqjmxqj2xqkGxqkW5rkm9sknJvk3BslHFslnNtlnVvl2RZl3NumHRvmHVumWRZmXZvnHhwnXpxnnpyn3tyoH1zo390o4V7qIR2qYR3r4p5so17s498tpF9t5uIuJN9uZR+vZeAvqOMv5qBwZuCxZ+ExhIux6CFyaOGyqSHzKWIzaeIzqeIz6iJz6mJ0KmK0quK06uK1K2L1K6M2MCe2bKO2cil3LSP3LWP3raQ37eR37iR4AEg4LiR4bqS4wAe4wEe4wIf4wIg4wMg4ys547uS47yT48yl5DA85LyT5LyU5Xpq5Xtq5Xtr5Xxq5X9t5Ypz5Zh85b2T5b6U5r6U6RId6WdT6mRQ6mVQ6mpW62BM62FN62JP62VR62ZR62hT7GFN7GJN8t2v9jUb9zka+ua0/08Z/+u3Qc3uHwAAADl0Uk5TAAECBggOEBIaHh8mKSwuOTs8QkVRVFhnaG9/gIeXnq6wtbi8xMnKzM3W2Nrb3N3l6fDx8vP5+/v+kdRjUAAABSlJREFUeNrtm+tfVEUYx9e0tMwSC83ykuVaaWpWWmQlVm4pmpfM9bIl4iVAC/ASUtlai0a5QYLiDVNSkCxl8RaUeaPxWlqhsH+Oz5yze87ZMzNnZ/bADn44vzfMPjszv+85M/M8sy9wuRw5urOV4nanSAVwezxuqQAekAPQRmhMP8kAbaNkA4yWDTBAMsCT3eQCvHCf5FMwQPIxTOICdFIA6UsgfxNKP4byE5H8VCy7GCW5HDs3IgegawMs2bFjiUz/1COh0JFUif4TQqAJqXL95REo/sePSyNQ/U+flkWg+p9B6IwcAs1fEoHBXwpBjH+7EvR5oo1DNxuw/1mk6SwmaLjJM/bx3lb+vZ5PyF+I4LmeFgBDuf0bYvwR+rOBm2AI2/9hfv9LyKR/+AmYN8a7x3L7X0aErnETPNuDATDYjr8IwSC6f9+XbfkLELz0AM2/+zM2/QUIRtxFAXjMtr8AwUDS//7x9v35CV681+zfbTin/xVkqb85CYaZAR5pH39+gv6iOZjiXx3052dl5fuD1QkQmDLyUHH/ygKvpoJKcYIhQjmY8K/d4I3Rhlphgn4COVj1v6577FnuNWn5Hv3b/7gIDBl5sLB/JvZcXFJR09JSU1GyGH/KFCYYxJuDCf9a5fkLG8MRNRYq76BWkCCakePlYMIf4fVfVNYK1od8vkPwp7VsEd4HSJAgkpEfFfWvxM9bpjy7z+PxKY0yHKsUJRjIkYNV/xvGk47PX2Gr4ot/niuNVrwKBcZe/3MQ4IwcJwdT/Kvx/ousvwYQbsQ7sVqUYFi8HEzxR0FwKgmbAcIlEA4iUYL+rtGi/sgPThUkQAWE/UiU4CnXGItvb9H8UT441cD+90QUAaiBcD6iEdyysHja9ZAFwUUYfuyqud5lgVNLWPOPArRAOMvc999jMMVFtsPIB9VUcA+9tDaFQifOoTgAPisAdO5EKNREnXycsRgxAC40nTxPRmOXQElErCUAnT/ZdCFhALqMm9Agyia0VOIAxmNoEHkMOwrAmIh0kYmowwCMqVgTmYoTBvj1+0D2x5t+OKVHDge/XvHJ5q2/U4qRpthiVF9anJNTXFpvNSsLYNdK9Yaz+kfN7yM1su4nshxHnz+2HFflqSPyqtizsgB2anesD/epke1aJPMg34WkamF0xMIq1qwsgF9y9VveauV91S3TI+v/4LmS1efpI/Lq6bMyAbZAl/emvv7G9PehsRVHvoPG3GmT3pyBR2/juZSWwqd5GenpGfOgUUqflQnwJfR4Jw30LjS+wZH10HgbR2ZC41uea3kxfMzAIzKgUUyflQXwF+T5+a/hrm9B1zUQ+Q1e74JXcWQKRD7l+WGSA4F0PCIdGjnUWZkAR6HDHNwz7RVoLYXIz/B3thKZCK1sFP+nGcJEyog03KLOagmgKwqgK5snw8SuDXVWB6BzA/BvQgsAO5tQOgDHEjSXB3LNmTA3UN6crD1Q5/dS5a+zDdBcYOz6Fc5iq4yRgNLpcy9DfuUdFBlDRdRZ2bVg9zq952f7lUq61mBwAEfKvUyV4+/3GgiK9tJntXUlC8A0syanmTR5VvQNdfidEO8/wh8I8E5MCoB+zGIV2XJdBIClOx+APDDkoYoHwDMHfyIi00o8AK45+FMxmViVyEZVX0Dzg0h7IzvxUpep8wKYCidRXHkA4s7RqQGcPeAAOKega+8B7mJkAWCrGHGXYwsAW+WY+0JiBZCUG5ED4AC0I0CK2yNR7hSXVH/8/5rSAaQvgSNHsnUbfogttl2Xh5oAAAAASUVORK5CYII=";
            doc.addImage(good_structure, 'PNG', 141, 104, 19, 19);
            doc.rect(138, 96, 53, 30);
            // +8, +5, +11, +26
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 162, 101);
            doc.setFontSize(13);
            doc.text("Good School\nStructures", 162, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.good_structure, 166.5, 122);

            // doc.addPage();

            doc.line(20, 129, 191, 129);// horizontal line
            doc.line(20, 131, 191, 131);// horizontal line


            var sanitation_facilities = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAqwSURBVHic7Z3tj1TVHce/v/swswMLyMMiCzOubLBCtNaUALuupjbRxLQ2YglNSRGtL/gDTNs0Teo7jZqYRl/4qjWxDdp20tSGNtpADa0FXDak2kSsLURghwWWfZqdnYd7597z64uVhV3m3lmW+zB3z/kkGzOem/v7ce9nzj3n3DPnEDMjLE6ePGnW6vWnSVAfCNsAbAGghRYwIQghStVabZlt2ShNleG67qxiAJ+BMMBMRyulsbf2799fDysXCkuA/v7++1yBtwDcH0qARYLrCoyNjWOqXPY65GMN2tN79uz6dxjxQxHg2LET3wbxHwGYgZ98kVIsljA2Pu5VXGfCk3u/v/svQccNXIATJ06sdlz+FMDtgZ5YAi5dHka1WvMqvuzWjXv27XtyNMiYgT+P6654HermL4iO1augEXkV364bzutBxwxUgHw+rxNoZ5DnlAndMJBKp/wO2ZnP5/UgYwYqwB133HEPgCVBnlM20n4CEJZYQtwbZDwjyJOZZtu9cOufEGCCyJj1X5BBBBOAASKTpmNL3yWcy/LlK2DbzsxnBteZucyCpwTzlGHoXwXwSVDxAhWgY+3qtQC+FuQ5ZaO9vQZDn/W9MAHc9uUfCFgTZDz1DZQcJYDkKAEkRwkgOUoAyVECSI4SQHICHQeYD2fPDeLIP47hysj832m0taXx9fvvQ1/vthAzk5PIBfjze4cxchM3HwDK5QoO/e3v6N7Yhc51a0PKTE4ifQQwM1zHaX6gB0KIALNRABELQET41mOPoHPdWui6Pu+/pUuX4KEHe7Bh/boo05WCyB8Bd23aiLs2bYw6rMID1QuQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5AS9RMyFmzlYmxxH27/+CePSeZhD56CVJgJOJx7YMMGlCXC1Arc0AVGdvSMYr1wDt3sL3Ds3o/7NJ8Crrq18NmcLuQbQTV3jZgS6adRgodAF4Ox8jm37+BiW/vV30KxqYPFbFWdsBO7IELjBKmecaYe17znUv/E4AGBsbBzlcsXzXBrxxp6enrNB5Rb4rmGDhcIwgA6/Y9rfexuZgSOBxm11hFVF/dz/GkoAAPVHd6P2wx/j8uVh2LbXPpF05YHe7YEulBhGG+AXfoWpM59Kd/MBQEtnYKzp9Cw3D+XBJz/0ufkAg32v7YLyCvqEAF4B8FGjAqrbWHbw1yGETAb6yjWgzFLP8iW/fBFkW40LCR8NFc6/EnROgQuQy2ZdAE8B+HxumTF0Ftqk5+6Yix8i6O0rPIuN4igyhdONij4nFk/t3r27WQvxpgmlG5jLZk9jes/gVzG9GTIAwLh4PoxwiYIy/rvqZQpnrv8oQHjVtqr39/b2NjTjVgltpdBcNlsD8KPBQuF1AL0AtlkTY7uwrmu1YGgM1gRIZ2aNmUkwiJmJGQSEt6N53FC7hfHqjeslE8ApXavXxkcugZBn8ABc83hf39ZQvzWh7R7eiAO/zb8LxhORBWxBmBlnzw96l4P+9POfPhfZ7qtqJFBylACSowSQHCWA5CgBJEcJEDkUdwKziHTHEI35VwLakauf7+zK/UTXNe8B8kVCcbI0s9+REAKmeW3HFNd1L37xxbmZIV7S+MyNZwiPSMcB5jI4OHgIRI/ElkBEFApD8LrODBzu693xaMQpzRD3I2Ao5vihw8yeN/9LYr0GcQsQ6OyWVsR1/be602K+BrEKQESLvgYQ7C+AQLzXQNUAISNUDeDLoheg+SRPV14B0un0KQAeU2AWB47vVrlkWZZ1KrJkGhCrAB0dHVMAPogzh7CxLduzjMAfPPzww1MRpnMDcT8CwETvxp1DmFi2twAAYv+3xy+A4xzEIp0CZFm23xgAO45xMMp8GhG7AF1dXRcBnIg7jzCwLe/mDQEnHnpo68UI02lI7AIAALdAVRgGtRav/oEWEcDU9bexyHoDQgifBiBZQhhvR5qQBy0hQGdn53kwvxF3HkFSrVRn3gDOhSDeCHu273xpCQEAQNO0FwAU484jKEpTZa+ioq5rL0SZix8tI8CGDRtGGXgp7jyCwLYs1OuNf+NHwEvbt28fjTglT1pGAADQgNewCIaHfb79FwDxWpS5NKOlBMhms1UwPx93HreCcF1UKo3XPCDg+d7e3pZaEKGlBACAXC73JoB34s5joUxMTHoVvdPbu+PNKHOZDy0nAAC4jvMsgIG487hZajUL5Uqj1T14wLaqz0ae0DyIdU6gH4ODg+tBNABgfdy5zAdmxqVLw43e/g2xcLb19fW15OSXlqwBACCXyw2BeSeAWty5zIfJYqnRza+BtZ2tevOBFhYAAHK53ACYnwHg91I9dmy7jslSae7/dpj4mQce2NbSj7KWfQRcz/mhoUdIiN8DWBl3LnNxHBfDw8NzJ3+Oa8Tf6+npORxXXvMlEQIAQKFQ2ATgIAOb487lKq4rMDw8DMeZNe3rPwTxnbBW9Aialn4EXE82mz1t23YPgPfjzgWYftlz5cqVWTefQe+nU0ZPUm4+kCABAKC7u7uYy2YfJ+BlxNguEEJgZGQU9fpMCg6IXh4qnHt869atiXqfkZhHwFwuXLjwFZf5RQJ2RRnXqlkYHRu/NtuX8Aeh0c8e3L79v1HmERSJFeAqhUJhh2XXD6VS5rKwYxUnipgsTc/htGy7ZKbNRx/s6ekPO26YJF4AADh2vH+iLdO2YsWydqTS6cDPb9t1jI1PwLYsWJaN4uQkKpVq8Qd7dt8WeLCISbwAH354slM3nJmBFk3TsCSTQSbThnRbGkQL+z2+YEalUsFUaQrFyRIqlQoqldqsH3qwq6/fu/e7sc/ruxUiXR8gDHTd3XL9ZyEEpsplTJXL0IjQ1jYtgq7r03+GDl27se3LzHDqdTiOi5ploVyuoDxVwfDoqOcCzzwdWwkQJ0S8xasOE8yoVKuoVG98AzstgwEww3GcxtO3CJ43HwA04i1I+A9bEi8AwJsXsuyK67pNf7dnmqZvOYFaZlBqoSRqHKARDGxpftTCME3/7weL8GJHReIFACjUm5BK+dQCpASIlf7+/uUIeb5Ak8fA+gMHDiwPM37YJFoAIUToz+Cm7QBKJbodkGwBSA+9Ck41EYCnewKJJdECgDmCGqBZRynZPYFEC0Ah9gCuYpqm/2hiwhuCiRYAHP7FJyLohu5dHkEOYZJYAU6dOpUCoTuKWKbh/RhgoDufz6eiyCMMEivAaKm0CRGNZPqOBQCGbWNTFHmEQWIF0N3oql7T8P+Cs5bcnkBiBQCiu+jNegIaaUqA6NEi6341GwwSHP6AVFgkV4AIB2B0XYPWYA7BtVTCfR8RJkkVgAi4O8qAqZRPO4BwN1ptK5B5kkgBjh49mWPAexfmEGjSDlj6m3w+F1UuQZJIATTNjbzKbdYO0OvJ7AkkUoAoewBXSTWbHJLQnkAiBRAx/D7QNJsN9iWzJ5BIAaJ4CTQXw9Ch+U4xT2ZPIJECxHWx/doBYc5NDJPECXD8+PFVAHfEEdtMmZ6rlRDQkc/nV0WZTxAkTgCi+KraTCbzmV+5bSevJ/B/kaOnefVwGsIAAAAASUVORK5CYII=";

            doc.addImage(sanitation_facilities, 'PNG', 23, 142, 19, 19);
            doc.rect(20, 134, 53, 30);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 41, 140);
            doc.text("Sanitation\nFacilities", 45, 146);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.sanitation_facilities, 49, 161);

            var recreational_facilities = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABpASURBVHja7V0HmBVF1gUm55xzBoYZHGBIMmQBEcSE6Vd3WVkVRFFWV8HsuIAKJmRBRRQRBAUVluAikgVEUDGsoqigAoZ1jauw7lp/naLfo7u6ul/3e/1e1xuZ7zsffDPzerrrnq66devce1sRQlq1NNCvYoqeFCMoRlNMpJhO8QTFaoqdFPso/k3xH4pvKD6j2EPxGsVmisUUkykupuirXLNNixurFmLwKsXQ8yk+oSBBwk8UmyimUgynyDhOAHcM3pbiUoqFFAeCaHAreI9iJkU/iojjBAie0btTzKP43GWDm+FLiocpBlFEHidA4EaPpriQ4lW7xsjNziC9ezSQ0RecRu665Uoy78FbydLH7iJ/f3oG2bJyDnlj/QKyd8dzDLs3LCRbV80la5fOJMufnE66d6lzggxfKUtF8XEC2Dd8PsXtFF9YGeyszDRy1vD+ZMaUa8nLqx4lX733Ijn82ct+46Wlf9X9jZzCMlJYVkPSs/NIbHwiad26tVUi/JdiCUXv4wTwbXh47osofjEb1Pi4WDJiSB9yb/ME8tq6JwMythEG9G7U/M3omFhSU9+V1HTsxlBV14UUlrclGdn5JC4hySoh3qA47TgB9IZvUrZmhoOHAe7TsxN55J4bAn7DrWDT8od195BXXOElAI+qDl1IdkEJiY6Ns0KELRQ9fvMEoF8pFLMpfjVby2+//jK2Vgfb6DyG9O+hmwWqVbOAEYoq2pGk1AwrswKWhqrfJAEwFVrZwk0Yc37IDe8B/AkdIYvKfRLAg8raTiQrr4hERceYPSMCUc1weH8TBKBfeQrzLTlRD02b6BoBgOGDmzT3A2NamQV4FJRWsxnE5FnfoujSYglAv1or0bpvRAMQGRlF8ksq2b/q729c9pCrBNixZp5uKs+lOwK7BACaBgwl1e3r6TNGmu0YEH6OaVEEUEK1643Yn5KeRSo7dGaOFP+zQ++84CoBgDNO6cfNAtG2Z4GGHn3J4FNHMvQdPJwUFJWazQZvU1S3CALQr4uUGLruQTElwmHyDFJxZa3m59lZ6a4bH8BWs02bNtq4QEGpLQL0HXyqlwAedO89gKSmZRiR4FuKoWFLAJycUUwz2tJh/1xd36gZJDhY6t9DJE8GAgBnjxioXbKionX3b4T6rk0646tR16krvV6UiAT/o5gUdgSgX4kUy0XGRxSttKZOOFCIsql/95KLTpeGAG9ufIpERGhnAez7rRDgRLr2mxEAaBpwMklMSjaaDZ6hSAgLAtCvLKOgTmZuoelAJSanan5/+u1XSUMA4Pwzh+gc1+o637PAScPO8EkAYODQ00lOXoERCXZhbKUmAP0qUY5HNTffJiKChU19DRS/TVq58D6pCPDOlsXU6BHaWSC/2PSZ6hp7WTK+GlXtOhgFkfZijKUkAP0qVVQ2nMccS8radvRpfKynrbiHdiP65wu/O3eY5h4j6CxQZTILNPYaaJsAQOfuTSSK+hkCEuxzkgROvvn79Ot9AouGWVkn4ReoP5uYECed8YE925aQKG4fn5VXHND6b+4XpIhI8BFFoRQEUNZ83bQfn5jMTsysbpPyS6o0n+9c31ZKAgDQGGhngUjDZ+0zaLjfBAAGDD2NpKVnikgA/WKOqwRQvH2dw5dAWWvFOVIDDqL6GuedMVhaAmBpiomOsuTg9hsywpbBBw07k/TqP4Sc0NiT+gJ1JL+oxGgWALZRRLlCAGWfv1z05ts1PpCcqg2I3HbdpdISABgz6kztLBARySKZ/HMNPOUMwze7e9MAUtfQlZRVtSXZuQUkITHJjtDEg/vcIsA00ZpvZ9pXIyYuXnOtxXMmS02Afa8tJ7ExWictI6dAEP8/mXTp0Zu0q2sgxaUVJD0zm8RY0wzYwVkhJQD9ukDk7Vt1+ERozYVaodmTmQDAlX88R2cM6ACS0zLZy9CmTYSjhkY4urS4gGSmp/I/+85fXYG/Bzs/8vt8K1s9I5S3O4ELsESQH/ZtlJ4An76xgiQnJTiuLo6LjSH1tdVk5IhB5KZrLiVPPjSV7Fy3mHy3fzs58vlrZNuaBdQH0W0Rd1PEBZUAypGu7lTPSpDHDIXlNZrrVVcUS298D6bceLnfhs7KSCO9ujWQiy84g9x925/I8oUzyJ4dfyM/H9zJDG2GB++6QXTNR4NNgNH8H83IyQ/I+ACiaeprnjqkd9gQgJeNiabt8tJCcvLAJnLVmAvJ7HtuJuuXzyUH313v08i+8H8jTxH9zd8HhQCKkkcj5oiOjfdLHcMjJSNb8xB/HndhWBhfJBw957STmIZxwexmsnPtE+TbjzYGbGgjfPPxVlLbtlKUvlYXDAJoZVx0u1JU2S5g4wOQVauv/eh9N4YFAXjpeHlJAflx3ybd7x35fFfQSPD2y8+RpMQEUZAozjECKAJOzR9JpW+tE8YHIrjQKjJ3ZDf+umdn6d5+I+IeObgjaAQA5s+eIloK/uwIARTp9gFeDFHZvpMjxsfWkb/5UOj+AwVyFNT3XFVeRP69f7P49w9sDyoBgIF9db7I17CdEwSYrUuQKKpwzPjQA6qvXVqcJ73xkV/IjwlyD80+c+TQzqAS4M0tz5KoKJ3YtDkgAigZO5qkjaSUdCbgtLvPx1YP3j4cPqz5EQbKWKR8yU6Anl3rNffcrrqU/PTJZvPPhWAWmDD2d/x4Il6THQgBdvLx7pLqDobn+TjSxakeDkYQ20d4l4/w+QJi4Q9MvkZa40Okwt/zwofusPTZYM8C/9y7meTm6E4O7/eLAEqiJqd+KWFvM9S7EHBCwwcZF1Py2D/ECEsSdO2kVS7Xt68kP3+6xdrnD2wL+izw+Mw7+PE8YiYgMSPAIt4wmAGcDntizce0z5+CyUiC55+Yprv/Zx6dausawd4RACd2O4G/z8dsEUDJz//FKSMjto/wLiJ8CPJgu4Stntrbh7FlJwFEKur761Rf49d1jhx6NagEWPHUTFHGUTs7BLjdH0NDxoVBgpgD5/k40sWpntWDHZlJsGTunbrnxYzg7/WC6Q8cPrSLtK0q02UiWyKAUpbFtDIHMnaQtAHdPqTbcIycEnDKSAKs8R1rtZI1+AKBXXdrUEkw485JoiSTPCsEuFAjdcpIJaPOG04m3zCWJWqGIldPFhJ8//EGJgKddff1upfAKcl6sJYDnBOkpegSTcZZIYCmINP4S84jb21aRD7bvTKkgx9KEnz57hqyecUjZM69N5Jrxl5Ahg3qxSJ7fCaQByd26+jo3z9y8JVQxQU2mhJAKcWmGfAXl8xkBPh6z9qQT71OkwAyrhcWP0Duu2MC0/T169WF5OVk2vZ17Hr+lreIDi8JH+xcyZNYtwzwBJinWecaapnxAcM4t2QkwH0igwcl4f4yaSy58OyhbL1OSUp0bOsaVL0CIoaOEWEXnc16my4DPAE0RRjvmDiGGR/roJseuBEJ7mm+mtX1u+HqUeTMYf1JbU25Tq7tRFBK9H2Ukgvqc4MILGbgx1Ey3QUcpssKrvPw9EmmywBfflWjS9v+wmOMAPtf/5vr2zARCRwVXEZEsMxlHE6hrk9BWTXTOULwUlHbSZe3GNLUdbo8wKCMEHAaMUNQIx/991X2fWZwSprDB7ZqPvsJtR03blgGckUEuFT9gAOaunqn/y/+8XcpAjFOkABH2UhcScvMJTmFpaxARYUFNTOfuQTAnwgH4UqXju34e79cRICF6l+aNH6UlwD/+uAlaR4G076VaRv1+nByCa0+6vvhEMvfnAVv7kKsNnehR2N9WBDgpj+N5sdog4gAGtHHc09M8xJAJok21nxdKlpyqm7adkqtxFf64v/2igX3Sk+AbavnioJCOV4CKFp/7y+kpyV7jf/25kVSPQwcPt74wTC2EWLjtPq7xob20hMAkUzBdneEmgAauffg/t29BHB7B8AD3r42NbsopARAwWh+Fnhu3t1SEwBCVf4gi+I6NQHmq3+INcNDgI92Pi/Vw2Crp75XTPuhJACbBeITHTkVDJXx+TJ3CuaqCaBps7LokcleAux/fbk0D4MgD7/PDyQlzf9MJt3bRJ6eMyWcjA9sVWzPmiFpfujZ/wOfvLFCqho9vLcfLIfPbi6DLWWQ+8ZnqmEPAXryp38e47NDoDdXSvNQCO9qM5PiXDG+pyK4v9pACYzvAaq7sNZqGq9WTYCDb62ShgCI7fMKZbcIAKAYhvp+2leX+VYHu2R8FLPi4xiK6lu7Axh56kANAWSo1esBDnZ8FWQIJYor29vOD3DL+FBsp2bm8D+D7VlTRe83rx13kYYAn0sSBhYpcs06eIQKCVz9Hmgf3Tg59WV83Cu2zNzPUeWFddT0fvPOm6+Qdgbgj3SNchRCOgtU1eoGfe79N0lnfAAvDPc7K1op7VS930TnLRl9AIg5+AcMNLbv2CzAlbitKC0UZgm7aXyD7SvK0rNeuscyXCl7NbuA3XLsAnDyxp/qyWB8ADORG51O7BjfYLZ6S5f+9dTDf9EQQJY4AGRcfC1CWQjACl2npGmXp6K8oB6i2TU+q8ZaXSdqY6ct8bps/jQNATD1yliXD+f5MhFAMLjkwanXSmN8AFFT7jN4+VkLde83PSLQY2cBy6QgAASc2k6epVIRAEBcQlM8Kz+bSctlMD5Q0b6B/9z2VkrbMkMCvP/Ks1IQgD/OVLeakWYWqKnXGcZJGXsgxheV46N4uRVf+Gn5/OkaAiD+7rbxodvnH7qitpN0BBCVvMXp5dfvv+S68Q2c1Y0gwGdGJ4Ee/Lh/k6sEQNIGL+CU0fiisveeQ6uighxyUp9u5IrR5zDfAJ3Kre6wnDC+wTbwpVZKRSnvNx+fcYuOAN98sM5VAiBjR6NYjk+UlgCCadYU6WkprOII0u8QhFtGZ2CIcNQni0i2DdT4R4WtupJya0CA19TfnDXteh0B3FYFI12L7zMoo/ERmOKPiQMpF4uE1P5NjY4YH0CrO+5aq0CAzepvntChmsybcauGAJ+6HAxCrp6bMrBAjO90ooq/xhf1ZFDK/bdaLPpDPRrryIJZzYwAbvfuQaKm2zIwf4zfoW0Fk9RDVYXKYtgRjB11FnurC/Ky/SIATiD9vc+0rFxROzrWq9bwDzZ1byCL5kx2Te2CfTSfpeuGDMyu8XEe8OGr5i8OKqSgUgoqpqByCnIOcZrIdyZzaveDMvbc9e4BAS62wryhA09kjZTdaNIkiwzMSeObASFkVFbBjiwjPcVW70U7h1bIEAIB+tpJlMR2BL10Q6VnFxVnkCEGECzj8xh38dm6LbCoNY0VCNrXnywUhYJlUdExpiXQ0UsX7VSDWZOHL8uibjqNXL2WbnwA1+MdSX+UUCjuKRjLMk/zJ01379zCMlJWU0+y8otNiYC1Ge1UnYwWIvVLkMwoBDRuSNdqqcY3OgjDLGC3WiscZ+6ev2cNQBRp8CbNPjsjizlaAOLbmXTbFSnuYuktA4eOmoFkEa1edD/p1rmDX54x0rWQsdMSje8RwyAuoJkFsu016kjPyuPve6U6MWSq5s2Ki/cSQE0E7L+joo2JgI6aaKpoZ9uIkGivbubRM9TkQVkWeMmmRIhPDLh9jWzG92D8JedyyzCdBWqtzwJ8NhPFBDUBhvPOHg4OeBIA6JOLiJLZjIA1C9OWmZYAFcf4I14eEIHy1bhQmQPFGUyjaNRQTp4Wum18T4OqhHitE4dSvdbuv1FUV6GjmgAZ/MOhFrCIAJ61B8WhswtKWAt1wzcyJpq1VvtUpSrCvndQ3+6mBkSuna8ijJCIIT/f7DrQ7QcSOJHF+EYhcTjjVnZERfpDoK+w/vP1Ad7jO4KICIDDDk2VcMoulIGPMCFCUmI8S+o45aQTTQ2G9Cq7FbiQn49kFrPrQj4GPVw4Gx+AQBdjqVFGZflWRmHXwD3D06ICETN5x0pEgKMFGBqF0wy6aBv1ATAD6u0jrSqQaCNStDFzmBIhOdWylFw243swcfzvtbEZzAI+urfwGUwoByQiQD+dH1Al9gPM/iAGDnEEK5XFEeNHJo2T6VTI0sVMYlrTOCWNafjCzfgAEnX4/AgzfSSepXVrXcHLShEBIii+5HsCGs8C5uFYRKsEU4+3uxbi38HKoMFMghkFuXpmRICGD7ubcDG+Uc0fGBh6P4tHwPvN6gQ+zEfcjAhgtU9wEieROu/0QSFLmsDMghkGByxmRICMC0es8G9kN75HIpeemmypi5sgIXSuGQEG8Q8PFYm/s4DoBKp54mWuFJZAuhYMadc/kc34HmAc+SWbd9BFyasUfcwIEKlsEY6tl8lpAc0C6KKtvh46arqZPYuMHSRthLPxAQhNUctBq5TK9nX8u9tKtfCp/PpSQrdQ/s4CfAt1tFN1e/Bw3AphJnT7RsaHmENW43sw9aZxhrMAooQC52+0FQIUKy1GNHFnIwIYOR+iRAQELr79cL00AwixCVQ6fOEpRDJlKo5phG/2riM5WRlCvSS25IJGknFWW8Zo+gTjRNCIAJ7wsJUIFLx/WadTPlQqU3EsM6BjC9/TGTaJitad/U+10zOotz40XGZIAN758LaF57YgJw/oKe1AQrevvldo+MKBAN99tEGnL0TtJEHTqGK7bePe0MwCdEuIPbOxQ6g/meJLklx16XnSDiSSNoKV0hWKIto+HNol/vQN1HUMN/MFRCFiPgQ5++6J0g4iMnbU9wr1brgQAL5McWGuGQH6+Ns6dguvRDHbEfBLAX9kvP65WdIOInYF6nuFdDtcCADg5TIw/q5Aegf3EIVPzWYBzzkBQsH8Z2UqOScSpmhyD+i6Gk4EQIyjrDifH3M0/u4ZaPv4JbroYGmV+a6AGr+EK0eCoIXMA4hETf451Z1NZQdiFqnJOtXP7IDax6tKyf9He1Qcb0oAAIKSYLZaCwaQqKm+Z4hXwsH4OPwSqKsOUaQGTACFBM382wHFsBkBUjO0O4A/nH+q9AOJLF1Nwaz7bgwLAky7bbxo7T/Xkm0tEiBaqSilcQghwDQiQHySdgdw1y1XSj+QSNFW3zPStWS/59fXLWDSO874q63Y1TIBFBJ04UPEiA0YiUf5SBTy3mUfTOTnh6w/oENnGg11OhUUcjzKHCeAQgJdIinedJGEnA+tvrd1ifQEAEn5sq8y3+91V1wkmvqvs2VTmwSIUapL6iRJagIUltfo1MEf71omPQH4RFQkvMjUMEuNJ2c1i6Teu3GkHzQCKCSopviWJ0FO4bGzAl6GVFNZ4i05J1NTBZE3zWfgIEtXtvtEboSg8AR0HBW27Wn3AwoJhiqtx4ROYRqXhjSkfw9vtZG9O551vaa+GfiEVKRoy3R/IGRaik62dthXwMdRAigkmKRLDYNTWNWBqYj4uLq65Mye7UtDpgu0C74mD/LzkaLtdpUUAEfUgpg/on3n+G1Hfz+okOAZUaImn4d+963jdYWn3t36DPlOInEICCmqxsWnu7nly/xzz1pWv0lwb5MCsmGABEjAYYMvbd3Sx+7UEcDTlFKGvsQ2euwwhxYnh5+EsKE2jG+QR/loIPYLmAAKCdB46AOzYhI7184XEsBbkHrXctf8AjvG12x/42LJhDHnkwNvrgr6tG/w5q+liHKdAAoJCik+Eg1UQX6WqfE9eI9uwUK9JJhV4ISkGiIXZDnBwTXMMkqIYxHDg2+vDorDZ3DO/w5FiiO2c+IiCglKRCRIpR6rqPys0ZKANnWh2CraKb/qyXIyI0JyUgLra+xUjyVs9VJTkoyMX+SY3Zy6kGom2KOr6RMVSW6+ZrQlEngcRKx7MjVb8NTZgSqKl7prCJ+cyJ41kKNkBHkMCkyuderNDwoBFBLkKHXodQ8wfHATeXXNPMtE+HDn8+SHjzdKYXwNEejSgOIM8G8MawCnJrPsHTuVwiHtQiUQQYSPOXxOrPlBJ4BCgiiK+4UZwWVFLKffKgmwLKBtzY8OhGSdqrrtzXuo7cTy81ubEAFCmCk3Xm6aZ4Alb8Oyh0i7qlLRNX4NdKsXcgKoiDBSqUalzdOPj2PHw1ZJ4CHCfrr18ndGcNr42gSYTuw8RJCJcyxUnpXBzu3ViTE/UcMfeGsVufbyC9gyKfjc4UCCPK4TQHV28KZoUCDA4BtUWAFqD9nZMQTT+HwmFLJ0Dabwo80uczLJvc1Xk310e/fikgdJZ+OSeF/5G96VigAKCeIoHheWmKOeNYQY6o7lVvH+9qWssaVZQ4tQGZ/vGYBETTMi5GZnsJnQ4Oe7/TnYkZYAKiKgV+3PhmvlTeNsk0DtMMLzVgeU3DC+ngiZLF3LYoAJYo7r7B7phg0BFBKcQLHXaBAa6mvI7GkTWRlaf8nwwSvPsmVihKCuYLCNj2xpVBlB6jwIcFQf0ZYdlPkw/mo7Sp6wJYBqSQDT/2U0IEgkRSkUO9tGDxBBG9S3m9D4kKs7VW38qLEbWaCoQmVwD/C30rPySWRUjJnhD1kVcLYYAqiIkEoxhe9dyEfY4COseXpGYMaPiGT1cvlMJjhu8OLxxmJ/j2APAKOy/+N79GcwMH4Pv88bmgeqquBI3GxXoGzvZluRbrdYAqiIkEfxVz7/gC9M3a9XZ9J8/WVkw/OzAza+04A4NjOngETHxFlZ63eFwsMPGwKoiFBBsVB5O0z7FqCCx9g/jCSL50xm/kKojV9SXUfyispZBjS6mPl42z0p2siy6i3VmMt0MyoidFR62/9qxXvOykzTVflw2vhYz9GuFhU4UEy7tXXP/itFTV0k5VjLeFMqIuRTXKlkKf9q98we0zEKJaFUOjKZ8Mai0QR6DMAzx5Evikoj1xGpbNn5JWwaR1QvmW7fsI7HJSab9kwwwesUoyhipR5jmW+OI0MBxXil3+2vfhgkFPiYYg5Fr7AZ13C5UY4MRRRXU2xzmQyorLqI4o8U5WE5luF404KZ4RSKa5Qj0618Q2wH8b3im4B89Z6S62E9fuH+ACbEyFU6oo2heIDiRYp/UHyqOGY/UPyinLh9QfE+xQ7l95YoZJqu+CDDUGC5JRicx/8DEyLaClD4WBsAAAAASUVORK5CYII=";
            doc.addImage(recreational_facilities, 'PNG', 80, 143, 19, 19);
            doc.rect(78, 134, 53, 30);
            // +8, +5, +11, +26
            doc.setFontSize(15);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 100, 141);
            doc.text("Recreational\n  Facilities", 100, 146);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.recreational_facilities, 108, 161);

            var security_facilities = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADnwAAA58BwyKAIwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA5CSURBVHja7V17cFTVGQ+GBGQsIhaKGMsrCGE3u5u9N2+QyEskVMZBsJQBq9ZOyVQUYUAoWGpbWrBOp44IRUV8VJHhIfgAhBleQsXyfskjhFBehopgQEQIbL+TfIHN3e++sjd3z549f/xmGHY395zv+91zvvO9TlIoFEoSDV6/0gTgBQwF/B4wGzAfsBywCbAXcBxwFlABKAXsAGwAfAJ4H/AK4BnAzwAZ7G+KKCsRlJ0KKAI8D1gKOAioAoQcxlVAORJkIqAAkCIJ4L7CbwIEAeMBKwHfNYCyrYI9ezVgCiBPEqBhFe8DvAg4FUOFm+EIYBrbfiQBnFF6a8DTgO0OKegaoBJwDLAH8AVgN9oAJ9Em+MGhZ+0CPAu4QxLAvuIz0Wi7Ug/BnwF8BpiD5LkP0AHQgm0fFp/fDI3IQWgIvowG5Ff1GA8j1KuAuyUBzAWvAj7AN9WqgI8C3gCMBNzlwhg7Ax7DZx62aUQuAmRLAkQKtQDfMKuCZMbfE4BOHIw9DfAU4N82yMAMx0DCEwCE0AbwrkWhHcDj150c2yzt8XSyzSKRZwFuTzgC4FGuBHDOREiXAfMA+XF4XFXQoVRlwWYZBUi2uerMQZ8Hc3Slxg0BYLBZgM0mQvkeMBPQTgBHVUecy0WTOTNPZJbFvzlX89sR3BMA3/qpJm/EBcDfeD861XP+rQDTTY6Zl5g9YeFvaZ1fy7kmAE5+lckbwGyBNiL62zWySAcsM5HFh0a2AdoP4d/fwC0BYHCFGHjRmyzbx/qIrnhCLv0A+wzkwmRWpPNbrX9kE5cEgIGNNXDmXMItoUmiKT9MPo0BkwxkdJXaEohtZDNXBGAWLTpK9Ni9P5785S4QIQ9QZiAvFgNpFPZ9rUG5hRsCwGCaojdPbzLMxfsjqfgIud2KstGT24La1RKN5fDPtnNBABhIc8BaA5/4b6WyTWX4mMGRcT3gNgxo1Qk8xZwAGLnT84CdAORIBdtyjX+tI8t9BEH2xpQA7MiCrlpqwIeYi1Qq1rZMu2B+gSU3ecwIgClZ63QGxuL4raVCo4qVbLVAgNJYEkDP2mekaC4VGbV8b8Hop2EmUkwIAA+eoDMgljjZVCrQkgxTAGNwq6xEC/8i+kkuE14/Cv91nQDw0Ad1EjfYsn+LVK5lOQ5zIP3shKsEwKxcKhuX5dWlScXakuU7DhBgm2sEgIe11fHts2VLlUq1Lc9BDhBglCsEwJDuJp1s24d4F3bXnJzbPT51pMev/tPrV5d5fep/qsnsU45m+pXPPT7lA/j3TK8/+HB6bm5zlwjAZPpLrErai9nEbBvdAvgcsBEdQGswlWwl2lgsYvgvTHpt5BYBntRh4CRele7xeFJBqaO8AXWdzYoh5rlckRkIDk9KSrpJ1BXIbvJjJSGoN7mcWFJSI3jbf24zc5dGQNmZGVAHJDoBluokanIXzvX5ctMya5ZPpyt/Vno8+S0TjgAw8cE6AinibULdfEqujfKxc9X7rq/av15p8TeHfD61S8IQAMOUJwlBzOVuv/epgzGhVDfLJtOvsiqffmAbRPgqFAXmGlCKa4xEwyqgbzx+pXuiEGAWIYDTgJYcvvmXdBRW4QkoJUVFRY2t/r309PQmQIQxsDroReXOeILBdKEJgGFJyts3nKdJ+P3+O/WX/eB7XboU1jsBpXpV8CuLdUjwZSAQaCEyAdZQhhBv1r6OwXfN6wtOcuoZsDVMo0gAx8SlQhIACziohggdeJoAO6dTivEE1PGOO210SODzZfUWkQBvEZOdztPga/bpyKSJTJ8yr6FWG53tYCv7TBgCoL//MlGnx1VhJhhoowllHGvfvqjBQtHVNgFpGAaHikSAP8eDxw+W5C0Rb78/+GiDHzfZ6SBCPuoyIQiA3THOEATwcaV8b/ZdhMu2dMiQIclubD2En+AirA7NRCDAbwjlf8phGPVJ4u13zUZBZ5HmRKAO4jTk3EKvhFz7xUZYtaMlQD/+JqUuIiz/Qhe3n+KI5/uVFzlU/riwMY4zI8A9VLcrHllNnP2r3Fj+NQ4irR0wnzPlZxB5G4VGBPgLQYDHuVzWfNXNoeqko8VAwHUDSAF1HWcEyCf0+YIRAXYS1ak/5jHWT1TIbo0BCbXl3Qc5I0BbggC7SQKwMz7x5Y08vv1ohdcdqy+4loNt6BSHNsBOQq9pFAEej5dUL+bo4ZQAX3FIgBmEXn9FEWAh72d/SYB6EaAXodeFdQiAnSrOOVVtIgnAFQFY7eZ5IhOqcTgBqOPfLEmA+CcA6pdqStU9nADU8W+gJIAwBCgh9PuncAKsIo5/zSQBhCFAR6qAN5wAZU6VG0sCcFuCVkH5LGoNQG3FzCpJAOEIsInI70hmH3QilodXuCzzCgbTPT51mKfmgijtmMu9AXWqq4gskr0ApJgCY3woIyurXRxUIXdkH/QlPhjDVeAnU+lqsx8/L1jBqpQ4IcAfqCivXvyfmxMAvGl9TYo9eMdZrzfo44AAI4mxlbAPXiA+6MqD8jHkeiyOlX+9uJRVKceYAIVUB9IkvMdGewRM5WLp9yvPxb3yryerBJ/goOOYdlxLk4gr2Y5xZLis0A56fEFu6JM+BaHlfQu5xEcwttH5uRQJXuNAntoWs7uSiBSwMo4I8L/wsfUMqqHjxT1CJwfyjXIYoxpQtdvAFg7keVDbtJt3AtQp9nxAzeZe+bXoEVQjOqRzIM8DVghwWBJAWAJYWgFKJQGEJcAhKwQ4JAkgLAFKrRDgoCSAsAQ4bIUAByQBJAESggA7+3cPLYFz+9yi/NCR4oQgQBlFgN2a/zyfCARY3LsgFAj72/cqamgh/J/gBNC61Xew//yY8Fr9RGQCrO9XGAoGIt212VlqqKy4u5AEwORQbd7HEvbBSwQBCkQmwGxY8vV89vPuzReVABl6waCniA9GiEyAaT1ydQkwo0eeqAR4QC8cPJD4YKrIBFgNW4AvQBNgZd8CUQkwjpjvfXpLw9uiG4HPd49cBdj/iWoEwvPnEHpOr73p85pTlxHHCwFYxC4v64aS+sAp4LjAx0Ci5yMzCFP0jgcVieAH6K/eUNIvcrKF9gPg5Z0Rqf+1H1L3/nWUBBCDAHilr3Y8q8MJQB0FSyQBhCHAg4R+Z4QToJjKF5MEEIYArxH67RVOgGZEq3VWUpwiMgFGgNJr//ajeWISADu/nSB0m6ptEPEpbzeCNDQB1vQrDPlZs2fAe73yRSVAltHqHv5Fov2p8lfRw8H7B3QPHXLI/88pASYb3TNo5iveIfMB4p4AVEldB702ceVEY8E2kgDxSQDW4o+4fPqAUZ/A2TxFBiUBopYf1fntH0YEGED8oJskQNwSgOoR2N+sW/hLYcvGszyVMt0fJwQ4MbBunAGx12XZ9SV7KPiVZCsXRrBCwphfiYaXO9eZxILe+dwrfy6ZcKIuinVdJeAZSzeGcNQQeiYVs8/JUkK5AZVLZEe++bWt5Ce7qHwvMYZvWUwgrgjQza8yJ8YVAcrDKwOBQHsXCfAGlf5l+c4grvoC0f2A4qs3gO9Gb14XlH8H0Umdxf7bxSUB2CUQmWCMGlwLy/Wb76bykQDUhV/zLd8ZxCtYoyi2j7KbOrFP/35OsdfrDy4E0k7wKMpPXVZ+O6IJBENO3BNAwhIBqBqPDYaXb0jBCaP8h3W2oZ6SAOIr/zbiHkOG102v35ECFIIAVMbPaUBLSQDxld+TSOtnGG7l91KI8a38JkTjJ1s3vUpBxjcB3iSUf5E1AJcEEF/5k3Ws/ol2/k602ab3A94FfMhuoACsZAUHWIa0nt076K3pqb8FO5LuqnGUVBeiPAK4SSqzXrIfqrPv77SbyR3NIJ52wFU6SCrUttzzdLqnnw7P9XODAHscIMA7Uqm2Xb0VhBwv1Td1L5rBnHGAAMOkYm3V9+1xWo7RDOi0BQVfxbtpLqF1egETEw7gFpIilWtJ1s3QtqJk/Fw0fzuaQZ0yUf5HPF89F2du3o0NtYVGM7DjFlaAzYBWUpFRJXfs0pHtBuYIiiUBjlrc51l/2s5Sobbl24lo7Hi9uIMVfTjxHCe7Tp4nulHXgl38kCcVa1m22QZb7HYn+zg62XueKbkVLvshHRflI1LBpnIdTeT01YI512518nnRDPRL7fVoYRbrMoMtYb7TkxBE8S0Aiw3kxmR6s9PPddIRVBn2WTK7ft5gMuW115dLXF/yjxjI6y12xW9DPNvJurPviO9MNJhUFd5mmZrAir8Z8EeDJZ/JaAqLuzTUGKIZ/DbNYH/Q+V6xidOI2RLFCaj8QSZv/Uk3OrREMwFt3V6VwXfb6LSgCQfLaL07QY53H5vIgsmqtRvjiWYia822ACJ8PA5dw3oTZ0vhDBGdRzCnNMDfTQpcqjDO71qYPJoJaS+dnmvxdwpgn8kbcBGNyM4CKL4Lk40J8UOYM5Hj9viiDVBMR4cQ6yySZuO3KaxUGXDOQjBpMS/3F9iUTwHey3zVZI7fsKZNsUqOibWQWgNetSCkEF5t8zs7+W4xmE8GWvWlFubDMnpej/V2x4vgghjcsJpH8AWuIO04GHs3tG222Rj/Kl5c47y9Qfegx+uaDWEyp9LbgF8Durqk8BLAAp3sHKPtjG0JKk8y53Up7YpbQ31Kwk9jUir7/QTAYIAfXa3JFp7dFGMaCtbbMat8HuAzwNf1GM9lbNjQlUdZ825ItUEF7HGwZv97VGQ5Zijvw/sSzjrcjWQbbg1teZZxPFnVXjSwDnLcEOIwjjEjXuQar2frLPSRr9JpiOAWvsV6iAm87e0JUxmEPgWWKz8e8xCP2zQireIKZuIsAYxlCrdiU0gCxC7K5sGAy1j0Ki5GH/smzLMrQ4OxAs/t2zHhgr3R7wNexjsVWffU9IYKx8Ya/wdkRPdTmKgdlwAAAABJRU5ErkJggg==";
            doc.addImage(security_facilities, 'PNG', 140, 143, 19, 19);
            doc.rect(138, 134, 53, 30);
            // +8, +5, +11, +26
            doc.setFontSize(14);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 160, 141);
            doc.text("Security\nFacilities", 164, 146);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.security_facilities, 166, 161);


            doc.line(20, 167, 191, 167);// horizontal line
            doc.line(20, 169, 191, 169);// horizontal line

            var hold_meetings = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzA3NEQyQzhFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3NEQyQzlFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozMDc0RDJDNkUzRTcxMUU3QTgyOEYwMUNCNDExQ0RFRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozMDc0RDJDN0UzRTcxMUU3QTgyOEYwMUNCNDExQ0RFRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PsGrDmUAADbzSURBVHja7H0JeFzHcWZ19zvmnsGNwUkcPEVSPExSJ62LsiTLkiytHa0de21ZdpJNNnHs3WxiJ+vYOTf7eXej7LdyEkeWb3sT25J8yLJsnbZMiRQt3pQIXgCIGxhgznd2b/WbAQkQwOAakJDFp+8J4GDmvTddf1f9VV1dRepfGISLcXDKvJ/rDvz09qYjL/2hZqavBiAqZ8qxnuYN33xj483/NFbZkFCsHBAh5nZRQgDkdRmelALYFoBpACTHGIwMqJBOqtDbrYJja5BJKzA8QPF9ivzkBaconBw45xCKuFBR5YAQNsQbbQiELKiqtSEcdcAfBNC0/P1dN38KDm/Wg1wMADiqDoy7sOXn3/5U4+u7/wyF7hOEFv4qgLkOGP7w4Y4NN/3XE1fs/CEOPIJghkGVgh4XuhT42CiF0WEUdJfPO7NpFHZKAdtWwHVIHiXjYKFQEHTRMZH3RyDAuQckRICquRAIOngiGOIm1DUZUFFtQSTmgj+Qf78EA37PywCYcLiKBvHOQ1tbD7/w2zVnX3/QUbTp5YoDp1iGOL7plgf2X33fo8w2J890Rc3/zKQAZ7IGpzv80NMZgME+P1gGCtulHigksCjNC62UhxDEm+mcE0/YeVA4UFZhQLwpC83tOahBYERiwnsGx54IorcmAFxFh8aOPddve+4rP8Z5FJCaoOjDCKkNbHP3ro9s62m+8iATTl7wZg6g86QGHUeD0HUqBKkxHw4w8wTOlNILez6g4C4Bx5EaiaN5sFAzpKFtTRpaVhloSgqawQEQYlkCQFk6aBEg+MVbjrz4AP4Lhe8DqfaJmFlFCk9bC73t0POf7Gvd/CEYTlB443AQXj8YgaG+AA4mCl3JD7Y8L/n0QeBJALLCMJqGDh1HfHD8cAVEy3PQviYFqzemoLbBOWeylhkQlgwA8msy7oBqm+Vc8i6czYqdBoPFQGEzAQG1LNPAHR2pF9//RpCcOVGNKl/3BlhRuUfSlvMhTQ/V8hLOJAOw9xcBOLC3AolkCq7cPgrta000G8sKCMqSakgUKAo/4AoK5dAPV5efhGOZajhlxcEkIRwvOk7RgAsJGBM0hUAulRnhfQdiVEHCoPvenBSboobSmRQ0g66TZXhGEQhpd93mYb5mo6lK4rgMgLCEACB4cQc0NxuwcbJH9AyEfBS2aMPQZiWgKxuCYR4DQ/jlvIcQTUNcH4bD2SboInXfA1Ulc/cHlzPL8sii9z3c7tOhiGOW+42k3R+tPslWXWF7HEcC4dcNACbVYNvYrzbfRZ7d+pxbA+3BBNhowuVMD6EWXK8n0aSPgSvyKkBF7ZlzGfRlBuCdK8QdXznTvlsYtqQF5E0PAsSxYzucByPm//zj3/2L9fHyDTf9/ufuTr/60qh64x0JqG92PPfRvfguJIt8+I+WBgBMh02J167Z2fvj9xnIhGuC/JwkvYiL9KpAmgDinRx/l69rwoJGv1H7bKbxG64p3Dc7AFzHkYQV6tvborWNDXpiYGhk/4nOPSmiDPmFG07vfiHoDg1w2tBsQCB00eMIS6YBFCR5Pf66Ewb1Q2vUBkrItBGYia8xFHVjyIGDInzScGiGEVd70056zoXDBa+qrwtXNTQyc3SEnjl40Dicyf0cv7MI6Jq/trlJ37Bta93ZU6dDg1//whBs3znANm23pJoUUhuQNzEApAfQE2zsMPyxXl96KG6DPrfPIW86QGp+ZptgIR/US/U8NoeMuEgxW8lc/JGo3tLeEhWOY548dDidTKYsRVGooue/kg2QOtVxKtnb0zfcsmpleVmdWtmz9/lQ4mxnB7v+VkcLBC5KiHlJA0Em88F7Bn90+337Pv81yzDKRWE9YKbDh6TxGX3lt/+ld80f0TFDl2GDEjkjtC2ivMPHSOhi0Eo0dmJdU/yGnGme6urt72SUKejxEJgmDC1QyLbj2k21VSuqIqHNLx87+V1/LEZXVobXIAde8gjX0gFAqnxNh+wbHXDb8X/8gwftn/9vU6hFfAaUEzLBz+R23nbytHJUV7m/RPEIjo/CPtgSeKXGxyKm7UDWsi+C7c+rcMbY3N7vctT86DAozFP/nC+d6FWKGormTfLSmAC56CLZ2+5ng/4Xf1b9Qizygxsbaj7WmhtYZ5Hpb6kLB/ay5mdOj/iOaszylc4ZBYrj6v603/y4LtzylpXt9b9968aPw0WYXcsyPIFyOTKSfu7HXcM/0ChhpQeAvKTUdk8/EYNf/bKG6j6SS0PyB1b7Q7+vDD9MXDnwZMrst1XN/WGm5WGexjew0tIfCYLOlPt0OpvLRrbEN1xz7bUfh7fw0ZuAF75/eOzzEUaBlpZSoorPZQAe/0Yl7H+5Fnx+qQm4Tzih3QMVP9irNf5Eg6lujo6vPau0fePwYPglnbqBJWG7FAI6I2Em3CC8xQ/XMnXdsUBzzBICYFz4T3yjBl4/WAmazsdJj7c6mnTcPcmyp9g0d5R/70jr+3nOFb8GYZ83l0koofAJPP71Wjh7ugxnPr+QiQniGpVW72rHcr3Az3muSMAxHKjPdW9BbBogLgvlzQWAc2r/6zUo/BhoUxZvhGXamY3XbbzvXetX3jM6mgLunA9yuMjKR8dScNv2q2/ccN2m+yzTygBchsGbAwCS8GXT0wtf4GE7BvpcI3R72wer79r2xTsf+HxN/bXvgeTwMBipnHemR8eg6eYPwe2/+dl45V1bvsi2t79ffsb7rBCXgbDEx8K9AC83D6fxT5+ohK5T5aj2PXYnXO6A4+aYpuhKc/VGurH5XvPK5v/Qj1rCVnxw1Qf/FvRYDbzx1KPeZda/99OwftdHQa6HDeSyoNy+5a/1uorV/MCZ7zpnhw+7pm2CwvyEUeWyuJYLAEgh1/LpJ6Jw/HAl6D5HWE4OX+FqRbheWV13M7mi8R5eV77NVZH2mTakHQeSjg0VaDJW7nw/wLGHPPbXtPUO75JJZKVp2wvQMHd7+wN0S8uH9LMje8Shru85r/c844ykzgoJOZX58N70suguJQA0HWD3c0HY91I1UC1JGQsqbbXXs/VNd4v22l085CsXMhfOwROFL9f5Uq4DacGhzDIhu/tPYW3N2bzgd/8ZhG55GNCQQIq7+TVB/AxHIfPGih2kuWqHsnPtiHqi/yf8UOfj9unBV3nOSgqFSq2gLSe+kE6nobunF1G6PPApM9yrqiqhoryshADQfABvHFLh5z+poYEg1W7e+Glord4pqiPrHS833/EEODkQQ8DEh0mgFmhEDSDj3+NS4yLvFSRQA1j4HjruIUjzb7v4Phdcn1oOm1bcTzc23a/3jx1yekZfg18cfdjqHz1NdDW0XACg4cSoqqyY5OVcykNSqEDAX0INIBl/YpDAz74fF7brKm1VW8V1q/+jjHsTmfYDM69l2/gwQ7YB1B+EbORq8I/8yKMQvOoGIKoGQ9kUWPge33SDxwtaQf5aHV0P9eXrldbq6+GxVz5hH+/5OehqdDmMuuu6kM3mlhUANJmD6CuFFyDVmkxWePqJKkiNBQlVTHZFw7vkokVe+EU+WgDAiEyfxqMipoI/HABfOAgidcp7bRhNhCPmEAeSLmTOAjvib2bvu+6rvp1XfIy4Iicc1wK4xGEkAucSXJbLOduIKPPQb2j3nw3B6TfKhKpZasBXLdpqdwk5++d4iUEnbxqGT74MjQUuSXOnPWCN/23OY433dSgJ0Ns3/YWvqXKb/aN9f24n0v0X2yRYRqpgGcPA0AT6fD5YJgoABE5OZZbVyLkBQKqRMx0MXnqm2iOAFrp5bTXv4LFgLZnH0uog2nkp7NpqH1C0JJSp4I4hAEwThlxn/hNOZs4YNjjrGu5Sa2Mb6Y/2fdo62v0zoSohQgkrxSCmh7vgzMv/Cr5INZ6VkOw/Aa6RhmTvG2DnkpAZPANlbVvh6g8/DDbyn0RiFJUlKZVCye9SW2BiiCSBBEHp9/sXAQAJZymcF5+uwp+63CNHkanRdY13OvMg4PIywzLNycjBQMc+aC5XvUyNaNABw0zCsNQkCx035Ad2NLCC3X/dV/273/g/1rOHHnJMO0c0ZVE5BbnUILzy6O9C8sxBoHJLm8xxwO+g+EMQqm0HX7QGylu2QkXbNu/9uk+HeLwGSpXGKOS+SZzBfp9vybSEMqfZ/8rzQeg5E8XZ7wqX20pVZIVoqtw5H/UvycYwqnkHf4nX+IBbKH6mgJ0ZAJbpgxERXFRYUpoEz3W8fs3va02VO+gPX/203TV8WOioDQozCZ/XplwUzcHOoobSKAM3MwIHn/hbSHcfBS143o3i+B38FY1wze88ikMzeWExm81CZ2d3yTSAVOE+vw/aW1suEQBkqDcxROCVF6vQA8inNDluTllVdxMP6UFizF39yyX+UYHuXu9REH2n0T9VPXIf1JEcDp2A4fAVwBZrPKWUpUlorNjBPnTjd9jPDv61/erJ/+dSommaykRQbcyGlfYX0qPokVgwiOcAmiUJzGH8OSR/4mtfWLUVqvY9AT17HgfNH7lAKC7oofIpwpdHJByG9Ves/TUKBEm//pUXYpDL+LzlXfQrmKb6yNr6d3GXz0vRSf9+xDLBDbpQVabmN86iZ0GFiSZgAMb8a4CWisRb6FGoLEpv3/zffZtb3m8R8Cu6pqp+tfI4o8H7Ow4QW/DCUkNeYctJK++fQwH32yasrGhGjvLrH31Wivr8PWcUOPyrivGdLcLhJmusXM/ryrcQZ37ERA6wjASe7D4JlTLgIxMD5N5K5oNeS4EUenGzak5S+N8c1oiImw82mUgOx93QfN0B9I3xGvoMHFG+L+vawMZ3MksCVojsydnP8W91W975awMAWnT2v/pSGRI/ZXz7NeHcYusabuMaMpN5LtTJeZa1s6Csvh36zDXgWllwzQz0WFcArLrH+9ts5AmFajOXG6AjgWRzYwySG8jYgQSE9BokKmbDWdo2QAnEQEVV78r4BLJ+x855AatQ/Woob7ry11wDyNl/pkOFE8di47Nfxm9Z0BeDVfE7UBPMW1nL98tQ76iMTzd/GOyeT+XjOisegDSSLlsWiCjCAYTKQD8z8kv7h7/6NNvYfB9c0XA3rwi1eDiUgagSrRxLvdFtZEGrWws3fOJ7kBo4CYmugxCsbIJITTv4y+pwSAK/5gCQgpCzXxZhyKd2gbBdg7XH3y4qI23EXoDPLuWEgzuCM71+0z1w9NhXvfus3XQ37B7p8lSvWgQA0p81k8aI3T10BHpHOthLx/5ZWRl/O9u44j7eXHk99yFS7fxMn5ug0buV6cJy354QnsslASg3IxgSTPi7H908eVavvPotxAHGbX/nici52Z+3Fa70/d2ZDLVUybMMPvfCwTYKyQbfpv9UUOu2x8L5HGIKesY4azOqowkIO4ZtOvtOPk73n/mBUl++VtnQdI9YW38nLws1nluJLAgyH3Ik3jNKlW4gSQyi1qlElV6v6dCqB2ClPH0BaNH9ENd0eKscyrSzf98vo2AjMyvMfhSsw2LBOLRV74Jp4v4CBzfWN3osXR5qcxlVi91wCAXOkfBVt+dnlfx92J2bO2mNZs6e1wjI4nQ1JGFjdQ+/bncOfo69eOxhZXX8RtQK/443VlwDmsLQIxAka/aTZK7X7E+8vrKiJvOHV2/+cKvup3EEQJmizhpNS6czy05w0oPRdc0LPZcOANLtGRmkcOZEFFT13JQUjlT/tdfxSKDiwqVeObOUtNmXfP7IZ/i7t3/NW4AoYo9HCiFf7hSKQOFMHJ5DGFju7iBpY1iQqWEWojKcskx3ckbW2dPxb+y104+xpsotNBZs5EOpDpHM9rlZcyw7lh6N3Lprw523Vz4w1wGSK3xDwyPLDgByES4WjZQYAAr+s+NI0Cu1NqEyB7pslK5ruNuZTrCaAuLg8X+Fs8NHNSFy6HOrM225kX72CJ+86idtfwIBQIsFgWQI1ubAk7lu/F0pwhNQK9CQt7n21MA+HKU9Xl1ARtC8o8bQlLCrkHntC1BwTOrr48sOAOPRTcM0p4sheyFkVVXmAQApACMHcOxgxAPC+dlvqdWxNtFYec0U9Y9zkWXMDNribyMBy9KcPUJ0NTJTko6culLY9gQgSesvQUGLxsTxQR0HPUX0G+eSDiZXQmXqGCx+Pci2bThzpmsZqPz5aAcOsVgU4rXV8wCAtIUnjqpeNa58QaaCH80NtrruFjeg+eDC0C/OfvLame85A2Md4NeizJYMjxQJOhAYQ3fPFBy0ghxlplBSagAo5gGgBshZgyDPEq3yzfXQNA1aW5ov7WzHMaV0adLMJmuAjqMhrxTbeDUuZBrUpwbI2vq74cLIn5z9Odtw93R8STDqU1ye0zPmqRyBlaRI1CmNAEgLFyoLXyjtul6EkBYfAdROVsa1bPtir7XbtgWnTpdQA8wzXCGXgiPhCNTV1SwhAKQwZAVOWYRxkvrnJmup2cJrYuunuHhoX8jh009YPSNHibStlpPKpY0s8QTrzmACCGQQABmc9dWFeZzBLyhfK8YBpJdBk8YpYvOsUOhF3dunoGZc0dxUMumLBQCGMVpijTIFAEyWX1W9CpwTCjASzm2ttfpqS9ZuM6xJV6Co7pFxP4qs3FsmlEZKz1lD1izExSiQvjbV5/1b/i5fo7OoQCdjpLjcRK9c3Ixbx3Ggq/vs4lm7KzN0KzzbfMk9CG+4yQQAyLSh0x2BQvlVPlHQViTQNoWBqAzo4e6nrM6h/Ui2zqVgIUs/NZdQqwPnvQAvCjcesClyqGPZ1y9FMTXJpmuqq0rkty+PANOxk5194KLYFA0Ub+BlnTpZeHli/pjchaEwRYv4Y5KokQnTWC6qIPP/Ghdiok9OrIxhsiJmTiIrjByuVjm/SbwWVWwINVAGecFMZQHkq+jFJ+ESJH1K8hUK/XrtKLf6ewlwBHVNLaoCqf7HRgkM9vm8urfnEcCFwgJ2yNc8uV4j+li247hj2UFk55McTebVPioSVMHrVKLAK9A1dwpx+AqmQiVT8PfiPhATwrq8UbBEHECW3+0768V98iXWR4c1sAx1YtVtFL9Q/Zpf8WvBiTmJsnKjYjoDquXIqNxEl4zTmtjqYkKS2zzqcPaHKPXoEPcqhDKoU3XvbzPKX6Y447XJcq8V/CbCAAz1M4/LeR6AbLQg6+1PfIcQjuPXqh2fWjNRA0j33TRsyzJsk0yg7kRhKqmJrocixY3kjG9GYauEnmvRoeElmqRJKBbpkHsPaqIb5D0uy64kdg2gr8svexpQz/73dvvggvxxubtbD/sjRJb2njA7JX3TLTtBXG8FhxS0hUsDvjJREWoXszRJWDFNz4AWzTdbaAtERbidBvSYvNdlCS4WAEzA8KCOXh8CQPbYyaRUuCDShETPtsO+FWKy/L0AkEgZXeDw83ugOHdoWbBeBH1xMoMGkK/Kmd+k6JOWfiVc5GvjWmEmDSBCvjgrC9XLe12WYAkCAaahyD5KFFHAIJtG+0+njL8eCcTEND65lbNs9GvP/8nlNq2OrhYyXiBmBoC0/fWqhgRwgp+N769HExCkRQAgXUVNoaQqskre67IEFw0AgZqfQWJIpV4ASK79T2m7goKOBVZP9/lAxjw5US1IckZqYxvELB5AObL9SrkbaMJn5e/SMyj3PAFRLCAGJF62/jIRLCERHBnUUAMkUSLuFP+aKpRpIX9ETKPSs8lcYoJkBFUVHUnaumJNkmTwJ44zPUyZlxl0XrsLiOBrcdkppFigVMYiqiNXELzXQiKql49pjnQKNUBftzolwCKQyiks4IR9LRf2bJCJGcywUmI8dIdvJUFfuSgLthVz5uXsbkTZ6QVbP7Fhn3yt0TMNxT0BUR5qZ0G9TAh+WQss9pCkPzGIAHAcbarJ9WIAPipjABOFQjx7DzRtnEH66AWBhCtsWh5qRAJYNduidQt6AGTSxci531aos3kCCICgr5qUhRoRTZd5QCkOx0ESKJssXhCHRybvuH6thvu0msmsnniLGkbWtEnBA5ALRrQGCaDKoBgBlNu+mlHI0gMIIvqeSgx4p/xd/l3GB4puDZNEUK5BINkklz2B0hBBy2SynSq70AXEWc/RA4hyn8LMc6uA+WIDAdsdpZadMAhhhTksaG1sfTGJ5D0A5vn73t4MFOYjg93AVBXuqqjx3tCK4JCZuk6RlUEPSHgvWAARxMtKuiH3S7/l4wiEMrk3y+XptFCA0inrN4gNYSm0QhszTm0sL2+Wu25dwUHGClNjQz0DWStBiGfMBdFUP1QVJ4AiH1mEtGNDMBCBZ3Dm702NQaAsAq8aadgZiEFWuJ7gxTmGMD0RBCSbSAR9Xir/PPZhM4WoqopfOpcK9fb0eCB8KzJJRgkYyYQW1Jnfr9GAMu1oa0rAOtr1rJqE5//uUw98s13JbvIWb/BPn3i24+HH07lswKcFhAwAhQOSALaKIptFpWrPWBY8+Ppr8MmGVng6MYQznYOD6uCLowPQYRnw0EgPyN5CRXcHyf1+ZaFWFvZX2KksgnD22oGFfgHKnfW+r5WpgUY2ejz8pX/sIG9VR0Km3iGx+9OPro79gefszTTdGGMi0T+S+Lu//9Nv/ZayZ5OJykIoitg3fPU+ghNQjiuSMYdWhFeIgFZOimgAeQMFATRgmfCHJ46Ahij0owKRBaGeyY7BU+mEtyagzJITILUI3qsCSWcTjGYGx4norP4u4mDI5IcsVww4QmZUv7VJJPWGmkjyJRSYcYOonF9cbSVd261cFiw51qhyf6P82J98A1b/1VAvGVCQAKL/v0YoTGZ3Fifxbn52qxP5hgBvO5g614RHyR/wXkg615CO3t0IU98cpC/bEYgX+sxPi8tBpCmHUsTr4oGACLSx1GZXLvsXVn7fnu240ylnzj8k1/6eMmqDjADOZVT5ND3xFlIK2PtEbdmG+daTUQgELot7OpMwQwIPWnQ3QqzqsGtU8gmDnQINNvC+m2vCZgOXlRHjZZtni8t4Wy/5BRxhfL/+PB9YUvlgNLRVVZSQx+ovH4s5xMwAEMQNKXYsKozwRCGphMMpEds/klL6VI36WNo466UFaMrMeX2iBLu3C/eQewT8I5leTfaYvBwRLokJ4NPxAGQIaoIGzh4INLywKtu3PegYPtn+3U9sSAlt2LCpSW1TzX3jxY+qbbU72JbW9/O2mpu5ruogS8eJaZX3ZGLI+ZxESKTdd7lJjp39KX/15NfPnhp4hRMh81EuF41epAYg9b/x8RVAqT6dJkDV7zoBzdhRk7jtGqX7ziTRMymu9R8zyvYe7Aq8pALX830B3BwKQ3jbtDe1vMe9svkDglH/xGmfG00Cl5VCCx1EpfCr/H5IRoIwKdpYKANDzj0D/q9zcB996sAn7d7EcTQBctuXf9nUY30Tx4NQPrYCoYgDMklwmvGkQjA1aQReyYV/+ktlw5OeNbeFUIWro/B9HmhkTFhTAuPbtK3e0b9QW6pvJpWhtkkL/xcccu9xjWnBKISATUwQcfJkUXIGB//uyjpCp/reIGcGD5CgXnlZ6qWa+0IWNuQKlFe7MjUIZigpShhOOBcFjuf5GTqNTofCNm3GFNmTb2aXPG/PHZfnjDFzP60pv2qcyslJ7do2mOl8vSAv2IdQQzfRdL0t4JeP0gEABz0Y4hR/seegLCYu3s3BWYdp3ny+OpP84aD6OZvIvjY1mnv+TeNannGRuSyxJTEBDoV4owWlbLogplEOYvIr3qKOANe1bDGDjphsijjPXZZXiQ+517OiGjlAMGQX0sFKYl5luJbJKBLOXl5oIesVZKJ5S0/luj7eSeUi7XfdlAliVgSQWcq7LtACTioYR7ynuvRdC/PRSjI+T0g+DxvoktzJH0QAVMdtWQAaHFudmhe4IG/dBoVlJHv3Z7Og5izQbAsymRxYCAjDp0MuIDeCEMNnOGctMZ2DOCUoUZLYPQdiuUKYCEVNJUL2ZyfjaUkOUFNWE0JvRrZv9l1MMHBZhUGAIX/D5wpTWQWRgg95sIHj5DiCpuXQMrmEko/dlAYBngaQXkAg6MDYiAqLq71AOFCUJ7fb9xxI12TGIDCSBAWZPJX+vtQEDAGg6zBWEYXOQGTMzIiThFLBXV7Up/M6kS3qmxKOgs/GiLm2TnNur1D4dj+VLQvEeO1RMAUZTjjk1V5HeXrI1XYDoQoKYsl7ErkIOh/Y1XWqjc/lbg8zWI0sWq6T6aimTAcnStIh+/sd+lyfoz6DszXLPPAu4rk8P1zhUFZhKagGAAFgQWIo4G0YWKDw8UEzQWLVXekz/7L2yNGtcIEJkKaBoosXtDMQSaagzuWrhqLlf3xoLC0ysTBh430HplMACy2YX5hdRLhwhZb7z20+/lsagQCH8ez18zcLEmiuVMSWNmF99KxlP3bA0P/CINogaoPA0oCASI86VUdzN24K2P8jyKBeQD5Gcs6JKlS4izGxukl33jvqOIcO5tzP9nP95yqByGLiP6j1bQhHkATKTiBVdSYsPM9SNgTPRsFsvTqQ+1K9BrtcFW2KgjiWuf6FOn2icEpQ2PJvuhapzoxeu+27P6bhwRFwixQ0chYIACl8yh2x1Zf9H+v84g8pkIAtiJe7ymHyKV+zvdcJNOlwzzVB46tBYdaiCs4uhTlwBE+vVLMf2xGyv+RnpF4+lwybXPhs8t+OdxKIKWT9jqD1SAPN3YqvLdwz4i5erMKEULRgT+oajYXafxwwM0jM+I6g8aWoQtfNdQuvfJvDFAgPJeBtj/8UAmNpNBHM8//JFMIqxELUPhUu2eLPfh4Feu9UrjHzc8n34mCvuypoPhoAKy65QymnPgIt2arkPnhlwP0sflvdFXMFjeyCTsLbQs4/xWlup+QsC7M7LkBtvSG7v1DvHxXVJqoEZ+YAzsyMFQdZ3eo3Hw4z2uosAEKOpkJ4OAFrX3i5ED4g08Qs5n9htPmZdjX3YLMO77YX8lz4mTKVrEOT9jdIQkqWhIoaJVdFzW3r/e6fSW0zX9XGve6ZRN8adL4QIWb7wsCJE6q6zvBcbA8AkRj3VAJ3yfwGmeTiirWrSoUtziKspARBzYlOiPUPgutFJC/cpjA/DSCXsgPEibfo/AFnEdrblimIqripitpXS8GVKALntmnOb2uUBPkCx0xqDB8lkRbN/g+oHK15yp6AL+Cg92dK2VMvJOhHnlPXlIF5Al2u8lcp/KbFmkjJDZjteObA24xKpvHa56eZ7BB125DpVy9mGTqfzg5QqfDrSpFGJs2SRngkqvD17qI1iXwusVMBHp5XplPe/uegvEo2PygEGCQBXNGeRS+ALwCN6ZJQZJIHwkzBpQUMdslaj8tOhVCSZnD5VGb5bCUYLjn2GbyWOy+SKjV+U2vW6wV1LgVfvlgdt9AltBAMZO4PQeiIS3cv1kuSonJUFdIVMS9mMPXv8x4cJcPpGVNAYjFiIwUmPubSQ7IsYgmERpHtp1IueZ2WAAAJl+xB05Qh8wEApS40rMiOa9WCBkAAhGMCzUAazcCcL4aDoiVc5ZDJIbeY6SEzijOxCKTKpwLAix/k1e/cnwuEkhOsf8whBxb1XPjhnAtDo5wdwYEqyWokfht30KEvLHbSSF0y7CqvnOtnMzf1TyFSZkBtgzVu7ukEZAC0rUl7nZ3nPtBqltPOUYe8uphOadTlMNxYB7ZPn6K4CynlmfkmgMio5IDNnlsMAOTgjDjkFQRTHyrukpSoxXHShx3lZfRMbLLwa8gY8ciIw/bK682dbaPQG1vSEAzDeNznPABk69aWVQZEy415eAPEpkpuCJTddDGIll+oKlSoHkimMDHKRU7Mk2nK9aZBpr3iLiDxdCL5HCTqbk5YyfYRIMCVMaadynA4u9BJIz82KuihDFV7aT61f24fY4oL6zalJtZxohPirQChCED72iTYcwOnw6nZUm2uvzF68j2mQxaqEmVzebj/8DehIindQGXKl0UAmPMxAVKHsZBCdzX23K86ORALXEyzUTPtrOh6d0U1qUGaVJI9hThhjBuahu5rpIN19gKVisUprFPPXnlNQ+JdpmBzCwY5KNOa+gzEGx2Y0KBj8shIEKzZkAJNn1NQyAZqrFET125hPc34WAuKmMpgSIiloM3og4r0MLgXbhLJN8+dFweQzewrVLPxVnHiN/wkDe4CnssDJrrY15HTm1exkWttwowSEADgCuVbtMEbGuig5iwwECCr87Qqg9HtSu9dnNG5+e6yw8SaDWNe72chZgCAJAY1DS6iJOUhZladIlgCfIM+jUOYpIAvYKbJL1OtpuS2MzCpMj3jn2dlMLlkmhNK2lS0RLU6tqBlDgnMKE15SyVJNNtElGhNXsiVR2ZW6Tlg88N14ctRUEUOKrQcjJBgL8zFwknPLhg2oH1dFrX7FJ4zebbJSNym7aMwh+ACE0I9a4XecFXNqWBpcOcLaFmoEAehWknBmB6G0UA5XpNPjg0IrwGCO5+dwPilaMpSRnpp9HgNggsWEMORk7MKnyur+kd6Tf8pBfjiaxQSrwK7OO3EDpdpFrK3+Zsnjoo5SHMQ0lw45UQPynDs7OrfIrD2ylGIlgnP45sRAJ5eR1XettZELSC7JRZ9Oka4MpTVzg7S0OlKJZ03vvOMSurEhDLUrt2ROIwFoggId8q75HIwmd8kIZYhrE47fKxcM0FD8zTvCYwDVaWmYRCCnWM5ZZjSEnkBrlBO5UKHmUYgQrMLWguIURSNqrmn8TqK4NoshIiAL2jBldtTMM32PDod8/W6h2zclpht8x6aa5rJkdQhUf1CnZbMq7R5eGue/Yc0xEQWDtesRgKoFrYFkEmPs5DVQOa46l6r9sc+P3j3mJ8vQEBFYlqrZWCvqPuRZYBBStQTXiWu3pGN7BvUo521aEBdPl8AyLJ6CTirlh3pyfhOKmQWzSQJ/er1CSir5NOF+lnkw380nc1A/Rd3oPOkDsmEr1iiCHUEO0XK9gejJKqk3Q0WCRbSGuYSRqbQyHqhd2ULPL7xbiRImszdAccwz7sA+D96tOfrkDaHgJI5d3NmRKhDpt6TLIv2VBpj16V5VKVzDClyYBAhY3C8vOqLTw40/RPPOlT2rCiJGyhrbZtgdYRrX6qjyU25nL8G5twOhoDmopdeJl7+trHujxLDZITRIm6gtP2+gA233dsPmk9Mp6HpTP6v1zlk6zUjXgeiYgPNhDrSD8M/TLf8L81nZeezcKc4JgRCDjxy7UcgGaxAd8+ZxmiKBe0i9pQICu7p7tovmwH1qDqf+pKoZ30+M/t0ouHh1DBPMwol7VOEals/dMq395hS9XgIzcBczZN8X5Bl4CXS8M/HuvT9KuHFg0C2SdDvH4HyKj7TQt/Md/a4wBoDmtvH8PeiTb10hfuHRtXu7mDkO7przInZChzTIEk5B2vXfSelRngxAYkF5ixThpjKusphUvmonyS9e84FOTrPQKce/M5QUuvTGPdBqQ9UkX7Xiu3PVf2b43cHyBzdQYpCTIeUIx2p2PMBYceK5rjIYF6kLAebrxrzIoBFop0zj7oMylx7yzCOZPG4gJyoGUc/aJb/M2jpVH49qUipF8JQDxqQDdPHnk82P0THZ/h4nh6ZEj1buM3lbuDQWMUTI2H2HEXdK4qFzmXaGo6b48v1vmrH/4HkHH2pcoPR2VJGhtnAG/7YQwpPeWanuFlS8L8kvK7EHkqPiKwEd/HZjxP4bdcNQVmVKLbMP/tF6pocuGLzkKdOiqk1KrTeQa3jTCz2lxXQDQ4yTkdWkeH5BAYvt43nc9uImYKaUN/AflHzV6NDBitsAZ1k9yf6zYvZWy49AnOMi/0k/pmy0MAotbJTnsv1Wg2j4G0HqkkXnIpW/OXgoNKN9nVJy9MrjhM6OFb+VVFuPBe0BsH2qu6Q888l8s8lE1Nidh9YlfDYsZHo9xXuhmaJ+lGobUzB+i0ZlFvxZ5iLO4RaYAw6T4QhNRZErcBnDL7bTmjPcPVXt9T177hl7LV70+DzCCEt1P4aZ+Jh1bS/79/8JyeOqd3+KreW5FN+yAzactGr5yrlvq4+7fgb7fFP/TvtV/8blZU2fm0oPJc8g8SAg9GmbxzoKntcda0QLPHmcwlOO+HSl8Itf/y7dS//m5rubnCQ55LCE40/l4beVTIaPPR/k1s+y1OOrrBZfGtCHXj77YOgB2A2AMz+FaXvGI4K2HHDAKoSXky3S83ujFrixUzzL6oCGWhgfVCvDkFcH4U6PYG/D0IL64EBvez5n3ZWPKkIN6RwkZXndAamsIXMZGJ+awHTgdPn2uHnOqu+e8pf+dM2dhZdqUGo0xL4XKPeMzbjcyECur81tPJzIm3L6rUXZWOIyrh2uls9+aLT/MUVWj/UKXhqw/kxw5/1Sh/EfSPie9aaz/X10X6FieJ+v2UQT2M3t9mzCX9uGsC7KF5o/VYTzp4egkOvVnsuxUxjzV2WtEhCb7sSGtqvhVjDeiBKPvvEMdKQPPlL2HfoRDKbMawQhYDiuCk805amhqbjNKgdDHmKxabkEEHMjGG7wXguvnU7hFuvBsUXztt9x4ZU9wFIduy3s12WgZxEBbg4tSck43FNE9KuP1Gx6Q5oxOcKVjTJYo6e9jXHBqC/40VI7UW/1DIZ+Irs3ZCqv6YhBdffOlqM+M0fAIV6PrDzthE42xmAsUTI21kyJeiEVCYcZL/57+9fee2O1ejeTjWh9RvfAbGbxjbt/9Yz1zz51DP7ddMxBJ4QgvO74iaMjkwGobLT3CITDw2XG/e86/Zdv3PvNTdUyvXwC47atW+HFTe7Da+v3v+hv3/k249Sx9IJXXoQWLZtr96wrvmPf+vd92+YoTto0/b7yGd3dP3e7z30rU9me7s5UxQ6reqnzIYb7+hH3zo/aecWNp/rqg3KIIADt+vufpSsPZ1XYJqWtenK9as/fvX6P5lO+ONHPBJt/dz9t/1NMBJlpmW7tuPy84G2yV4A4eAQmRG0iEicbDgZKi/X/+Y9u/4KhV8xM1dg6qdv2vLn669Y3W7KnrFLfeCkynHI/e5dN//O9rqaq4u99fa2xnfdf/vb787abnZaRSIFfs1Nfei2O3MV/tw1wPnAAsCKlTbeqBeef7IRdP94xfc8ALjIrYqXe00mhs70w8DRThg5NQginQOBfo8/HoGq9jqIr2uCikggEgz5aSKT4nRi2VZyvox8YZDc/GrgwhUA51wEQ0Ee9VG1pzcBLx44A4dO90PPUNq7U1VZANY3V8HVVzRC24paqCiLItH2olLakoNAoXZDRbDcybnQcXQUuk6nYLDf8KqiMh+FeNwHza0RWNEeg83x6GaXki9PuYZpUFi1fgC2XZ+Zj/DnD4BxPrDt+iwMDw7Cwb3VwufHkRImus9WMODz+08Nrnv2v30dTu/tACdtTJq4Hq9F7RWpi0HlNeuCdYKtTVDyKuqSYcOntGo5WcL43K7ogvxlAzvBF2YCvJx5kymM+Tm58ZN/+734T149Dr2jmfze6/GWB4WsofKQDrs2tkKGZN+u+/UjhIikbPiJ79SghNvDuJBVE11vN1ZNsKz50I8H4r863QOdvWmvfC6ZtBaCD6BSWNkYhlzcbigLhSotOzOKlIYxwnzEtlWorhtDzTy8EJeZ1L8wuIAoBuLGzAE89rVq2ttdFddD7Vfa6jvWmcqtZWmrzbFdYLrqlXSbdgDw7wJPEtLNbpXv7tpa39a5prqhszECiTIfOIMJBJqTT6BLm6fVf9vzTsuwLbzeHEyWkDPXoFS+W2sRXL/RdbQ7TAO2pHOWqqsKKDM0Y5a5CQbe168x4fezw4pqPU2Z+RMurGOoRWQpA58shLMwbY9SR6FLNz+oBKprtZqrGpWGO2qhcic3SExu0lOVmSPztsO93Cnhd/p63f6fdTpnfzyQ6zvgaOoouecD3RBv5POd/QsHgHwoBQVspskHvv2vj27qGPmgBIVsCzOfzDDp33vtSpx8ubjhygD8YmcjvNwWhKRGwPSrEBjOnvL9v1fe6aJRhpkBgBpCyBU7F7lHHTp916HQ77QtdpXjiJA3w8m8zbOnvZhCHFXlrzLF+D6h1nNIR84UditLMCjFw91eRz0LbZjpU/RItVb9tial/rY4rbk5KAJ18iqu1yhn7rOWyl6fMmqIH05ro796dlfzTWPxplG6AOEvCgCe16Fq0DY4sPZDX/vuj4NDySZbW1zgTHHyJmAsqsNwVIXX1pXDK63hE+kf7rtBN2yVEjJxv760EzYKNkcIK0NzvZW7+u2Ore6ybVI5nty82O0c4xVvpFpGGpNSNeclxqwfIvpfQrj34m3wSxMfXFAFxxZ2RmGKXq6UrW1Q63Y1KvXviIroWiKjffgfX0SRUyTGsmCnve/G+vcebqt6jFoLz1ldFAA890rToLW/b/2DX3vsJ6HhZHyxIPAsDKpihkBQ8OyKBzIHqfno85D+Qo67WXZu1knPQCknIvgx21JvcRzSml+3FLBUFQTHawrI6yuKGFI19zlCM48IYR3C1/0FniMJhb0y0Hp/C2u+twzKtqhCYV6/hRLklUrhK8gi9twQf//hVVXf1MzFXXPRAPBAoGvQ0tu/5cGvf+9HkeFkjaWVLoQuQSBLy++Oiie+oqX+k8KFtwSKNt51nNiXjQy7TtqSi1020gODrLOgw6DuH72bu3af5Ae2cNIbQms+sk3Z+jnHSyV2S9bkrCB8eGVn3UeOrKl8ZLHCn18coMjhMy04Fa/Z9y+/+e53pCojZzSrdOX4HUYgTQVsTtO71gj97RaIHAo8C8J3m5lTrpP9Li9FzVCv7gWV/RehituBj+G/DaQGTlgN1a9laz5huTZyIqd0wnc9tW/tuaHug6USfskAMA6Ck7U1+7/4gXfvGo2XHdDN0sVRvDUB1ATrbWUXp8SRZWk519/BxXIoFi1XW9UbKVXKcLZna9WqHX7wx3gJC5lLXsQUPrbnxvi7D6+s/GqphF9SAJwDQVXN8S984N5dnasaXvTL1K4SyUgq0ipQmlRCZPORIHfoKrJMqoVzTuqEIHEZKotq4cZSPpZiI3cI0t4XdjXehoTvR5pV2p5XJQ92+ywLeqLlA//4vntv23fNxn/xSe+NL35EZODZb/J6xsHP0cW3XOFbDnWDpSlA50V1BPFWvFRHaynVtTUUfrpW/+Xzd6y4/mRj2W7VKn3DsyVZ7dBR6BlFy37p7tsffPKWq/6zrNKlOot7+HyRyXzajEIJVRkjy6VbgIx36YxRL57IlUUzYC8+gsLuavY9+tRtrbf2RIMndGtput0t2XKXggxYsRz4/s07P/+V9911x2gk0KUvwiTIj2lS8DLEB0Sj+fDssji8gBEhAbmy7OFgEdyEObKOiGMe2FbxiWd3tX04pyjpxU6eSwKAPJLRhUNesO+KNU/+w4ffu+Pw5lXf9DkWEjq+IADohGq6LF4OyAEAQssFAPLZkKeVIQiEnymqWOBFpH3PVSovP3drw859b2v4XxTtHlviNskXJevBhzN/pLK894vvvft937nv1o8YYV+/z7DmneyJDxtSZPNSR/gdhweXS8sIue/SsnhUdjBRKQvM1/VjtgANuHN8U9nfPPnOthu6V1S+IsFALoKXo1ysQVJtx6sB9LO3bXnk2Iqmp2977hef2fza6x+RSze2OrfHYA4PoEsUkI3/OIdl0zsiv3sJqCJ78QENz929k6bSgZE67ZlD2+o+dbIu9jKTjNK6eN1tL2rPHc8koDboj5V1ffneOx/84gfuuaW7sepl6Tkos9g56QWg8CnDc9pKYpcaBFTafqIQPjsAZERPznArKDr3Xl/90afuWHnLiXjsZSn4UnhMy1IDTNIGjuNlux5YufJnx5sbn9928OgHb/j5K/+lui+xxmYquMqMvSxxoAsFdCenjSwDd5BI9k6pQ7WZwOmFcnGGW0EYOnpl7OGja6sfGgv7h1R09zT70vS0Vi7ZgKHwdJz5DlGc57ZteeTAmrZvXbXvwANX7Tn0B5VDY+0Ocn1HUSbPdC/8SpFxU7moBmIZdY1jlJKZ2qrkBe+CHYDEiTWhR46sr/n74bJQl4KMX7MubTNz5VIPHBXcMwtpPZh9cuf1/+elLZse3XTs+Huu3fvab8W7h3YQHCTJEYRXp40QP9pZtLXU8YobLx8EoIsqg0GFWof5TVsyfC3rHZhhOHlsZeyRN1ZVfmU0GugaNwHL4VCWzQziLjDThazuSz//ti1f2rth7aMrT3fu2rHvwIfbTvW8I5jKlVGmyK0m0t92l1v3MOp5qQQUTgKqiwxece2RWu0Xp1ZWfrmrKfqdZCiQkjF9GdpdToeyzMbR83uZaYJNmNi/atVPDq5s/0ltYiS+8sixm6860fmgSJ8hYFoV3PZ7bWnkoF9KMMhC1rLVHTetWkVxqVXDXjsU11/obq14YqgqethCMyaDO+oymfFTzFMp8gGW+nAYQz7AwI8uE+k7q9Ceng30eM/nnL7BrWYqG5cOhOxD4G2mKJjipQCFKCRdCjnDJVBlCDioJ9SKsoNkdf0/2LU1j9H6Jke2xZFqXga8iFje7W1J9fPLHwATI25eQioCgnEb1GQqRgb61ou+wRtF39B2d3B4lciazY7l6OMl4sV4syrIh2wnBo/IxOtOuEm+WUOhlKqMxMlGWHLvo8o49avdrLzsBNRUvkrjVc9ATc2vnLLyPkdR8wBx7GUv9EkAuH7PCLxZDync5Kises7R9uKsTCU1PZ1ppsOj65yRxConlVkrhoZjqu2uHuNCy4JTg3INcFnYRHbHLuxtkRBhlEieKb1Tywe0P0oVA2nmGbuyos8XCnRCZfkRVhY9ZETCp3k4nHJVzYNIpMwHChHLyyWZx/H/BRgA4E/tO3/9oP8AAAAASUVORK5CYII=";
            // +8, +5, +11, +26 114
            doc.addImage(hold_meetings, 'PNG', 28, 181, 19, 19);
            doc.rect(20, 172, 80, 30);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.meetings_title, 28, 178);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.meetings, 60, 194);

            var active_community = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAuQSURBVHja7d17VBTXHQdwPaY9SWuaNG1P2ticpua0p6f1FDQPNGJiYhpFAUHi6YlaWU2MJjFGhQV8svjgobKLiqhAxV1UzLLIQ9QAK+D7iShh8RWBVqNtT1KtSIKwM7f3zoIizAV27+zuzO7vj+9RmL3jnPl95s6d8e5MP4RQP69LxaaBXJl2M1+mvUVC/k5+5437wvuKj1B/XPQSHNQlJWQZAPD0mFNeESm+ELIMAHhSNk8fhDLDo1B6uBZlqkKE35UmT6MCwMuEz5DPCm1wW7IOAKDAZKiGo4zwOzioU3bhIqt6AKASPvNomzvCugCA0gCE13UppC0FyzZQAeBlom3IugCAgpL5/jOUQiKUG11KBYCXUduRdbZfQaCDumGectXgoed+1QvUQu6OqKMCwMuo7cg6y3Sx+HOt7Z9vJT8DAKUByFlwmwoAL6O2K4qLEG1TljwLACgKwHxEBzAf0drx++OPibYzJx8CAF4BILFZHIC2AQB4A4ADibR2jQAAAAAAAAAAAAAAAAAAAABIngrN6Md2TPX7CQDwMgBbP3zpR9tn+KfhNOPw21X+p7bNGDUMAHgJAFzwTFx49GhGfZPxgd+zAMDDAbQf/dbuAHBm+n8CADrHaByAzLrBwp8KBLAq0GeQZqLvWk3Q0CJNsM8KzeQ/PaOfPurPosUXMlJL2uE2U/DnczTBvnr89/HeCaBU+xkuQlN7MZrIz0oCsDLQ93e4gLdxUKd8vWXqq2NoALJm+Kfgz6R2aYPjs9S7AJh1U0ULWbz6E6UA0Ez0MXUvpC9KCns5hwYgM/y1HWJtcNpWBw951msA4J1fLlqUkuQjcgSgnz5iUJZqVMj2cP+X8K4XZiCTo12smAmhw76kAdjyt9cOUgAgTZDPO94DwKxtEC9K8tdyA6BX+Ufj4rV1KuRhfEn3C1y0O2KFjA8ddo8GIG3qiPN0AL4h3tQDNIr3AOv+IycA22a8/halmJ/3AADRAKROGf4PANAjgLW35QQAX8+nUop5zxEAG6f4NQGAngB8sbZJTgCyZowsoBUTF63JXgAb3vNDAKBHAGtaFATgewAgNYADSRza9j4Sze4FGEiSaMgyWjt+3ypauwYWAHH40g0ASN8DID5X7aJE3WACMHEoAgDSDwJdCEDdCAAAAAB4GOOA+3WGIehSznMAwMsAWC36AKvFcBMH2ZK9H13e9XMA4A0AanYOxkVvflj8B8kHAF4AgKs1LBApPgmPrhmfAgAeA6Bff5Qb9TFvjDrEG9UnUK56Mdr64Q/aag06CgDUWpvtg3arX+RMkQa8w2r43MhcZFo4FAA4D8C1xMlPNSS8O/qfa0JelBQAl6ve3G0nGNX5bRZ9Cg2A9WhKIC56U5d2rSgv4hUAID2AhoSwOQ2JYd/hIJLGhLC9DbqQp9kB5Cx8nrYjrNUZu6kA9sflU9odAADSAsj/+M0YXHS+o/gPkhC2nR1AbkQAFcDptEoaAK449iKl3b+F9V43PmGtzQ7j6gzzWy36VwGA4wCqlgYWdyu+LXeF0zcTAFNUCBXAiY01VAB7l7VQ2t0hVw/49HGlS5s0JgDC7eAk2/37oljE71mMeFOUVwC4EBtcRQGAyLjAeQCOrb/RAwDazrtjrcsuEW1Xpw9lAtB9kgjiC5Z6fg+wLPAKDYDYOEAyANwRXbNDACyGFtF2tfoMSQF0ZN9q3BtEeyyAU0sm3HQPgMNaZDcAU9Q96sDRYihwCoCOMQI5LXgggGMxAXeVBKDFLQAEBGvwv+95AA5Hj+OUAyAv2soIoN5hACTFK6UAUN/bdn4++429xtlvILFsmjIcpU7x65Yt04YjWhvbxFA/0RyNCUDKAbAnBjECOMMEgGTPElYAZ3rbzorIsUU4yBU5HxvkTQB0hcwAyFw/FgBGdVEfABS4CoBlxUTvAdD+0gfEHAYAnEm9FQC4CQAq1c6RBADDFQEyRc4CAO4CcFA7SBIAhbGOA8hZ+DwAcBcAMg4w684yAyC3jB0DUNuXbQQATgSAzMnzmAHsj3fs6DeqP/JcABUVj7VZdozBhVChmqw/yBaAUfND5vsBjgAwqRvIRBjPBHBZP4izZFd3ntbVZjHEyxKArRf4q6sB4MHftL5un+IAWC36QtFCHtWtlCUA4VVwyfmuAsDlqnfZc+pUGADjALzzvxMtyrn0L2QKQHiVCy7mBRcAuIRPOwPlCqCOGUB11tPUQlZnVMsWgHBZuO43fKn2uhMBXEN5MYPt3S5nATikHodOLhqPzi0PQpa4EHQ1fhK9+FIAsFZtvSprAEJPsOaXfOm6eskBGCNuoLz5v3Jkm5wB4OTiCeir3goueQ9wZvMt2QMgyYuZK1zblyVLA2DnXPLWsAWObo+UAMhR/6Um2L7CSwbgVFqTIgCYoubY1r8Y8QcSHAdAnheQ9YHtIRHpqjnuBnA0epz9R72kAE6mcooC0JH8JYgvXmWbF9gbAGOE7YgnD4bo/JQQNwOoxLm0MsTx4kvTA2yy3TcXS4UWcafTRcPvXyXeZm8ctQ13OiNPMgAPH+Zg6xUKMcgiDUaxwjYppHC5bfmuz6iPiGEBgIuXxwqgalkQW/FxLiUFP8kG4Gw6kuQ/X/qWAukB9JKd85wDQD1uK1vXH4DqE8JYAXzfty+G9ASgKhMAOHQKGKdhAeDwoO/RXAMA7uoBIsfOYgFweXUoM4D6pLCjAMBNAMwRAX90ePCHL/vq2Y9+Ei0AcBMA4TQQMbbBEQDHF42XoviIfFUcALgTQOTYjY6N/gOlAPBf8uIpAOBGAOaF43+PC2q1F0D18iCndf8AwIUA2geDejcAuH0jPvRnAEAGAMwL3h6METS7FsCkqJ62CQC4EABJuXrsdJcBSJhUebaXaWv2ATgHAKRIReQ7WS4AUH953Xu9PqPRvvkAtu/tI656G+LOpCPuWBriy9cDADtTOHPkk8WfjnEmgFtfrQ4d0pdtsR9ANxC4ZzieBgDsSJZq9OPkG75Fc99yBoCqq0mTft3XbWEH8GC62N8RX5ECAOwAQGKcPRqZF77DDKA+cRK5Y5h5XTv5CXu2RTIAD04PlRsAgB0ASAwzbb1BeYRjAI7HjEdp04bXOLIt0gIgPcH5bYg36wCAHQA6op85Cpk+ehOVzP8LKu8FwPnYYIxmDHmBVMfTQU5KA8BiHMgCwDZzaIsUAEzeBuARDDg7Z72OCnHPUIZPEST75r2Nj/QRaM27L4s9HkYiAMIXQwzNbAjwqeAQ26mAK9OleTMA6kumKM8UkhrAVeZe4GwGEwBk1i0DAO4DUMkKwHohiw1AabIKALgJQJvFsIoZAA7LYBCVrP0tAHATgFaLwU8KANzhVEcB1DHtZADABgCnPy7gLWYAJzY7NgA06xIBgHsB9OMshuVuuhxsIc/+AQBuBoAuGH7M2gs4AoAzJ6cy72QAIAEAWy8wx8UAvkWlSc8BAJkAaB8LFLgIAI/KdBMk2ckAQDIAwq1h3BPUOBsAV6pNkGoHAwApAZBczHpB5LUu0gHYuwKhLM3jAECuAEhqdv4UF7VMcgDkW7rkqVsAQOYAhBgHCHcJa/UcM4CSpEee0QsAFAHAFr58/TkyJ5BMB7MbAHldS1Fct7d4AQAlASjTnhSKeTBFmBhKZguT2UC9AiCFpz14EQAoEMAj0dlAVG5A3JFUnE2Ir+gyYzh/KQDwXAB9CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIC+AeABAAAAAAAAAHgrAA4AAAAA4LUAggGAtwOwAgAAoFQAS3gA4N0A7gMAVgC+bUoG0CxzADwAcC6A/8kbwKctAMCZAPYs/lbmAO4CAOcC+JfMAXyjAACtigXA7Vl0Q+YAbgIAZ/YAeYsaZQ1gx9xGAOBcAFdlDSB77hUFALivYAAxF2UOoBYAOBdAjcwBVAMAZwIwRZ+TOYBTCgDQIiWA/wPnX3R8b+9OXAAAAABJRU5ErkJggg==";
            doc.addImage(active_community, 'PNG', 114, 179, 19, 19);
            doc.rect(110, 172, 80, 30);
            // +8, +5, +11, +26
            doc.setFontSize(15);
            doc.text(pdfDocVariables.community_title, 113, 177);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.community_participation, 150, 194);
            // doc.save('a4.pdf');


            doc.save(pdfDocVariables.pdf_export_name)
        }

        /*This is for the circuit level*/
        this.circuitDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            // doc.text("Circuit", 35, 32);
            // doc.text(pdfDocVariables.entity_name, 50, 32);
            doc.text("Circuit", 35, 36);
            doc.text(pdfDocVariables.entity_name, 50, 36);
            doc.setFontType("normal");
            doc.text("District", 35, 40);
            doc.text(pdfDocVariables.entity.districtName, 50, 40);
            doc.text("Region", 35, 44);
            doc.text(pdfDocVariables.entity.regionName, 50, 44);

            // doc.text("Circuit Supervisor", 125, 32);
            // doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 32);
            doc.text("Circuit Supervisor", 125, 36);
            doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 36);
            doc.text("Phone Number", 125, 40);
            doc.text(pdfDocVariables.entity.circuitSupervisor.phone_number, 150, 40);
            doc.text("Email Address", 125, 44);
            doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        };


        /*This is for the district level*/
        this.districtDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            // doc.text("District", 35, 32);
            // doc.text(pdfDocVariables.entity_name, 50, 32);
            doc.text("District", 35, 36);
            doc.text(pdfDocVariables.entity_name, 50, 36);
            doc.setFontType("normal");
            doc.text("Region", 35, 40);
            doc.text(pdfDocVariables.entity.regionName, 50, 40);
            doc.text("Country", 35, 44);
            doc.text("Ghana", 50, 44);

            // doc.text("Circuit Supervisor", 125, 32);
            // doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 32);
            doc.text("Phone Number", 125, 36);
            doc.text(pdfDocVariables.entity.phone_number, 150, 36);
            doc.text("Email Address", 125, 40);
            doc.text(pdfDocVariables.entity.email, 150, 40);
            // doc.text("Email Address", 125, 44);
            // doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        };

        /*This is for the region level*/
        this.regionDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            // doc.text("District", 35, 32);
            // doc.text(pdfDocVariables.entity_name, 50, 32);
            // doc.text("District", 35, 36);
            // doc.text(pdfDocVariables.entity_name, 50, 36);
            doc.text("Region", 35, 40);
            doc.setFontType("normal");
            doc.text(pdfDocVariables.entity.name, 50, 40);
            doc.text("Country", 35, 44);
            doc.text("Ghana", 50, 44);

            // doc.text("Circuit Supervisor", 125, 32);
            // doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 32);
            // doc.text("Phone Number", 125, 36);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 36);
            doc.text("Phone Number", 125, 40);
            doc.text(pdfDocVariables.entity.phone_number, 150, 40);
            doc.text("Email Address", 125, 44);
            doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        };


        /*This is for the region level*/
        this.countryDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            doc.text("Country", 35, 32);
            doc.text("Ghana", 50, 32);
            // doc.setFontType("normal");
            // doc.text("District", 35, 36);
            // doc.text(pdfDocVariables.entity_name, 50, 36);
            // doc.text("Region", 35, 40);
            // doc.text(pdfDocVariables.entity.name, 50, 40);
            // doc.text("Country", 35, 44);
            // doc.text("Ghana", 50, 44);

            // doc.text("", 125, 32);
            doc.text("West Africa", 150, 32);
            // doc.text("Phone Number", 125, 36);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 36);
            // doc.text("Phone Number", 125, 40);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 40);
            // doc.text("Email Address", 125, 44);
            // doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        }
    }]);
