/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.factory('scHeadTeacher', ['$resource' , '$rootScope', '$q', 'CacheFactory','$http','scNotifier',
    'scRegion','scDistrict','scCircuit', 'scSchool','DataCollectorHolder','$timeout',
    function($resource, $rootScope,$q, CacheFactory, $http, scNotifier,
             scRegion, scDistrict, scCircuit, scSchool, DataCollectorHolder, $timeout){

        var scHeadTeacher = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('headteacherCache')) {
            var headteacherCache = CacheFactory('headteacherCache', {
                maxAge: 15 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        scHeadTeacher.headteachers = [];

        var url = '/admin/ghana/data_collectors/head_teacher', loadingSchools = false;

        scHeadTeacher.getAllHeadteachers = function(){
            if (loadingSchools) {
                return;
            }
            loadingSchools = true;
            var deferred = $q.defer();

            var cachedData = headteacherCache.get(url);

            if (cachedData == undefined ) {
                scNotifier.notifyInfo('Info', 'Updating Head Teachers...');

                $http.get(url)
                    .success(function(data){
                        headteacherCache.put(url, data);
                        scHeadTeacher.headteachers = data;
                        loadingSchools = false;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        loadingSchools = false;
                        deferred.resolve(false);
                    })
            }else{
                scHeadTeacher.headteachers = cachedData;
                loadingSchools = false;
                deferred.resolve(cachedData);
            }
            return deferred.promise;

        };

        scHeadTeacher.updateHeadTeacherArray = function(){
            var defer = $q.defer();
            headteacherCache.remove(url);
            scHeadTeacher.headteachers = [];
            this.getAllHeadteachers().then(function () {
                sortHeadteachers();
                $timeout(function () {
                    $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                    $rootScope.loading = false;
                    defer.resolve(true);
                }, 100);

            });
            return defer.promise;
        };

        $rootScope.reloadHeadTeachers = function () {
            $rootScope.loading = true;
            scHeadTeacher.updateHeadTeacherArray();
        };

        function sortHeadteachers() {
            scHeadTeacher.headteachers.sort(function (a, b) {
                if (a.first_name > b.first_name) {
                    return 1;
                } else if (a.first_name < b.first_name) {
                    return -1
                } else {
                    return 0
                }
            });
        }

        scHeadTeacher.init = function(){

            scHeadTeacher.headteacherLookup = {};

            angular.forEach(this.headteachers, function(headteacher, index){

                //assign the property name to each supervisor
                //This formats the names of the teachers to their full name
                headteacher.name = headteacher.first_name + ' ' + headteacher.last_name;
                headteacher.fullName = headteacher.first_name + ' ' + headteacher.last_name;

                //Increment Circuit circuit supervisor count
                if (scCircuit.circuitLookup[headteacher.circuit_id] !== undefined) {
                    scCircuit.circuitLookup[headteacher.circuit_id].head_teachers += 1;
                    scCircuit.circuitLookup[headteacher.circuit_id].head_teachers_holder.push(headteacher);
                }else{
                    console.log('The circuit with id of ' + headteacher.circuit_id + ' does not exist.');
                }

                //Increment District circuit supervisor count and push him into array
                if ( scDistrict.districtLookup[headteacher.district_id] !== undefined) {
                    scDistrict.districtLookup[headteacher.district_id].head_teachers += 1;
                    scDistrict.districtLookup[headteacher.district_id].head_teachers_holder.push(headteacher);
                }else{
                    console.log('The district with id of ' + headteacher.district_id + ' does not exist.');
                }

                //Increment Region circuit supervisor count and push him into array
                if (scRegion.regionLookup[headteacher.region_id] !== undefined) {
                    scRegion.regionLookup[headteacher.region_id].head_teachers += 1;
                    scRegion.regionLookup[headteacher.region_id].head_teachers_holder.push(headteacher);

                }else{
                    console.log('The region with id of ' + headteacher.region_id + ' does not exist.');
                }

                //Assign the data collector to a spot in the global data collector variable
                // via his id for easy retrieving
                DataCollectorHolder.dataCollectors.push(headteacher);
                DataCollectorHolder.lookupDataCollector[headteacher.id] = headteacher;
                scHeadTeacher.headteacherLookup[headteacher.id] = headteacher;

                //$rootScope.lookupDataCollector[headteacher.id].school_name = school.name;
                //$scope.lookupDataCollector[supervisor.id] = supervisor;
                //scSchool.schoolLookup[school.id].supervisor_name = supervisor.name;


            });

            sortHeadteachers();
        };


        scHeadTeacher.addDataCollector = function(formData){
            var defer = $q.defer();
            // process the form
            $http.post('/admin/data_collectors', formData).
            //success callback
            success(function (value) {
                if (value[0] == 's' && value[1] == 'u') {
                    scHeadTeacher.updateHeadTeacherArray()
                        .then(function (data) {
                            if (data) defer.resolve('success');
                        });
                }else {
                    defer.resolve(value);
                }
            })
            //error callback
                .error(function (httpResponse) {
                    defer.resolve(false);
                });


            return defer.promise;
        };


        scHeadTeacher.deleteHeadTeacher = function(headteacherID){

            var defer = $q.defer();
            if (scHeadTeacher.headteacherLookup[headteacherID]) {
                var deleteUrl = '/admin/data_collector/'+ headteacherID +'/delete';
                $http.post(deleteUrl, {})
                //success callback
                    .success(function(data){
                        if (data[0] == 's' && data[1] == 'u') {
                            scHeadTeacher.updateHeadTeacherArray()
                                .then(function (data) {
                                    if (data) defer.resolve(true);
                                });
                        }else  defer.resolve(false);
                    })
                    //                error callback
                    .error(function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };


        //After posting the form for editing, this gets called
        scHeadTeacher.editHeadTeacher = function(formData){
            var defer = $q.defer();
            var editUrl = '/admin/data_collector/' + formData.id +'/update';
            $http.post(editUrl, formData)
            //success callback
                .success(function (value) {
                    if (value) {
                        //If successful, requery the new head teachers and assign them to the model service variable
                        scHeadTeacher.updateHeadTeacherArray()
                            .then(function (data) {
                                if (data) {
                                    defer.resolve('success');
                                }
                            });
                    } else {
                        defer.resolve(false)
                    }
                })
                //error callback
                .error(function () {
                    defer.reject(false);
                });
            return defer.promise;
        };


        scHeadTeacher.sendNotification = function(){

        };


        return scHeadTeacher;



    }]);

