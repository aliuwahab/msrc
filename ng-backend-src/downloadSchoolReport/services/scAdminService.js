/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Administrative and Configurations resource
 *
 */
//Usage
//$resource(url, [paramDefaults], [actions], options);

//action parameters
//action – {string} – The name of action. This name becomes the name of the method on your resource object.
//    method – {string} – HTTP request method. Valid methods are: GET, POST, PUT, DELETE, and JSONP.
//    params – {Object=} – Optional set of pre-bound parameters for this action. If any of the parameter value is a function, it will be executed every time when a param value needs to be obtained for a request (unless the param was overridden).
//url – {string} – action specific url override. The url templating is supported just like for the resource-level urls.
//    isArray – {boolean=} – If true then the returned object for this action is an array, see returns section.
//    cache – {boolean|Cache} – If true, a default $http cache will be used to cache the GET request, otherwise if a cache instance built with $cacheFactory, this cache will be used for caching.
//                                                                                                                                                                                            timeout – {number|Promise} – timeout in milliseconds, or promise that should abort the request when resolved.
//    responseType - {string} - see requestType.
//    interceptor - {Object=} - The interceptor object has two optional methods - response and responseError. Both response and responseError interceptors get called with http response object. See $http interceptors.

//options
//stripTrailingSlashes – {boolean} – If true then the trailing slashes from any calculated URL will be stripped. (Defaults to true.)


schoolMonitor.service('scAdminServices',[])
    .factory('SMSService',['$http','$q','CacheFactory','$rootScope', function($http, $q, CacheFactory , $rootScope){

        var SMSS = {};

        SMSS.sendMessageToDataCollectors = function (postParams) {
            return $http.post('/admin/sendMessageAsNotification', postParams)
        };

        SMSS.sendSmsFromDashboard = function (messageToSendObject) {
            var dataToSend = {
                from : $rootScope.currentUser.type || "admin",
                sender_db_id : $rootScope.currentUser.id,
                recipient_type : messageToSendObject.recipient.type,
                recipient_db_id : messageToSendObject.recipient.id,
                message : messageToSendObject.message,
                recipient_number : messageToSendObject.recipient.phone_number,
                // recipient_number : "+233277482171",
                region_id : messageToSendObject.recipient.region_id,
                district_id : messageToSendObject.recipient.district_id,
                circuit_id : messageToSendObject.recipient.circuit_id,
                school_id : messageToSendObject.recipient.school_id || 0
            };
            return $http.post('/admin/send/sms/message/from/dashboard', dataToSend)
        };

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('publicSMSCache')) {
            var publicSMSCache = CacheFactory('publicSMSCache', {
                maxAge: 24 * 60 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        publicSMSCache.put(key, data);
                    });
                }
            });
        }

        SMSS.getSMSes = function (param_name, id) {

            var deferred = $q.defer();

            var url = '/admin/general_public/sms?'+param_name+'_id='+id;

            var cachedData = publicSMSCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        publicSMSCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };



        return SMSS;

    }])
    .factory('Email', ['$resource', function($resource){
        return $resource('/admin/general_public/sms',{country_id : 1},{
            query:{isArray:true,cache:true},
            sendAnEmail:{method: 'POST', url:'/admin/send/Email'},
            removeFileUploaded:{method: 'POST', url:'/admin/remove/file/uploaded'}
        });
    }])
    .factory('RecentSubmissionService',['$http','$q','CacheFactory', function($http, $q, CacheFactory ){

        var RSS = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('recentSubmissionCache')) {
            var recentSubmissionCache = CacheFactory('recentSubmissionCache', {
                maxAge: 15 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        recentSubmissionCache.put(key, data);
                    });
                }
            });
        }

        RSS.getAllRecentSubmissions = function (param_name, id) {

            var deferred = $q.defer();

            var url = '/admin/recent/data/submission?'+param_name+'_id='+id;

            var cachedData = recentSubmissionCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        recentSubmissionCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        return RSS;

    }])
    .factory('Timelines',['$http','$q','CacheFactory', function($http, $q, CacheFactory ){

        var submissionTimeline = {};

        var recentTimelineCache;
        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('recentTimelineCache')) {
            recentTimelineCache = CacheFactory('recentTimelineCache', {
                maxAge: 15 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        recentTimelineCache.put(key, data);
                    });
                }
            });
        }

        submissionTimeline.getAvailableTimeline = function (params, id) {

            var deferred = $q.defer();

            var urlCache = '/admin/school_report_card/submissions/timelines' + JSON.stringify(params);
            var url = '/admin/school_report_card/submissions/timelines';

            var cachedData = recentTimelineCache.get(urlCache);
            if (!cachedData) {
                $http.get(url, {params : params})
                    .success(function (data) {
                        recentTimelineCache.put(urlCache, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        return submissionTimeline;

    }])
    .factory('UserLogs',['$http','$q','CacheFactory', function($http, $q, CacheFactory ){

        var userLog = {};

        var userLogCache;
        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('userLogCache')) {
            userLogCache = CacheFactory('userLogCache', {
                maxAge: 15 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        userLogCache.put(key, data);
                    });
                }
            });
        }

        userLog.getCreatedActions = function () {

            var deferred = $q.defer();

            var url = '/admin/created/actions';

            var cachedData = userLogCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        userLogCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        userLog.getEditedActions = function () {

            var deferred = $q.defer();

            var url = '/admin/edited/actions';

            var cachedData = userLogCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        userLogCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };


        userLog.getDeletedActions = function () {

            var deferred = $q.defer();

            var url = '/admin/deleted/actions';

            var cachedData = userLogCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        userLogCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        userLog.clearCache = function () {
            var defer = $q.defer();
            userLogCache.removeAll();
            var temp = {};
            userLog.getCreatedActions()
                .then(function (createdData) {
                    temp.created = createdData;

                    userLog.getEditedActions()
                        .then(function (editedData) {
                            temp.edited = editedData;

                            userLog.getDeletedActions()
                                .then(function (deletedData) {
                                    temp.deleted = deletedData;

                                    defer.resolve(temp);
                                })
                        })
                });
            return defer.promise;
        };

        return userLog;

    }]);
