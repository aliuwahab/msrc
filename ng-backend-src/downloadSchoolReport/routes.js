/**
 * Created by Kaygee on 07/07/2014.
 */


/**
 *  Setting up configuration uiRouter
 */


schoolMonitor.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    //The redirect from the server after login has no trailing slash, hence the redirect send the route to the error page.
    // urlRouter uses the 'when' function to redirect to the main dashboard
    //$urlRouterProvider.when('', '/');
    $urlRouterProvider.when('', 'dashboard');

    //This happens when a url does not exist.
    $urlRouterProvider.otherwise('/');

    // This is an abstract view for resolving all data from the server. It loads all the data and all other views extend it
    $stateProvider
        .state('dashboard', {
            abstract: true,
            controller : 'scGlobalController',
            // Note: abstract still needs a ui-view for its children to populate.
            template: '<ui-view/>'
        })
        .state('school', {
            url :'/',
            templateUrl: 'partials/view_selected_school.html',
            controller: 'scViewSelectedSchoolController',
            resolve : {
                scSchool : 'scSchool',
                selected_school : ['scSchool', '$location', function (scSchool, $location) {

                    return scSchool.retrieveOneSchool($location.search().school_id)
                }]

            }
        })
        // .state('dashboard.school.statistics', {
        //     url :'statistics',
        //     templateUrl: 'partials/school_statistics.html',
        //     controller: 'scViewSelectedSchoolController'
        //     // resolve : {
        //     //     scSchool : 'scSchool',
        //     //     selected_school : ['scSchool', '$location', function (scSchool, $location) {
        //     //
        //     //         return scSchool.retrieveOneSchool($location.search().school_id)
        //     //     }]
        //     //
        //     // }
        // })
    ;



}]);
