angular.module('report_templates.app', ['partials/school_statistics.html', 'partials/view_selected_school.html']);

angular.module("partials/school_statistics.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/school_statistics.html",
    "<div class=\"box-no-shadow box-danger\" ng-controller=\"scViewSchoolStatisticsController\">\n" +
    "    <div class=\"box-header\">\n" +
    "        <h3 class=\"box-title page-header\">Data Analysis\n" +
    "            <div class=\"box-tools pull-right\" ><!--ng-show=\"isNotVirgin\"-->\n" +
    "                <button class=\"btn btn-default btn-sm hidden\"><i class=\"fa fa-refresh\"></i></button>\n" +
    "                <button class=\"btn btn-primary btn-sm\" ng-click=\"downloadPDF()\"><i class=\"fa fa-download\"></i> &nbsp; Export PDF</button>\n" +
    "            </div>\n" +
    "        </h3>\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"box-body\">\n" +
    "        <div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <!--<p class=\"col-sm-12\">Select a period to see summary</p>-->\n" +
    "                    <div class=\"form-group col-xs-12 col-sm-6 col-md-3\">\n" +
    "                        <label for=\"academic_year\">Academic Year</label>\n" +
    "                        <select name=\"academic_year\" id=\"academic_year\" class=\"form-control\"\n" +
    "                                ng-model=\"filter_year\" ng-options=\"year for year in schoolYears\">\n" +
    "                            <option value=\"\">Select year</option>\n" +
    "\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm-6 col-md-3\">\n" +
    "                        <label for=\"term\">Term</label>\n" +
    "                        <select name=\"term\" id=\"term\" class=\"form-control\"\n" +
    "                                ng-model=\"filter_term\"\n" +
    "                                ng-options=\"term.value as term.label for term in schoolTerms\">\n" +
    "                            <option value=\"\">Select term</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-md-3 col-xs-3\">\n" +
    "                        <label for=\"week\">Week</label>\n" +
    "                        <select name=\"week\" id=\"week\" class=\"form-control\"\n" +
    "                                ng-model=\"filter_week\" ng-options=\"week as week for week in schoolWeeks\" >\n" +
    "                            <option value=\"\">Select week</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm-6 col-md-3\">\n" +
    "                        <label for=\"actions\">Actions</label><br>\n" +
    "                        <div class=\"row\" style=\"padding-left: 0 !important;\">\n" +
    "                            <div class=\"btn-group col-xs-12\">\n" +
    "\n" +
    "                                <button data-ng-disabled=\"loading\" id=\"actions\" data-ng-click=\"loadSchoolStats()\" class=\"btn btn-info col-md-12 col-xs-12\" style=\"padding-left: 0 !important;\">\n" +
    "                                    Load<span ng-if=\"loading\">ing</span> Data\n" +
    "                                </button>\n" +
    "                                <!--<button class=\"btn btn-primary dropdown-toggle col-md-4 col-xs-4\" data-ng-disabled=\"loading\" data-toggle=\"dropdown\">-->\n" +
    "                                <!--<span class=\"caret\"></span> <span class=\"sr-only\">Other Load Options</span>-->\n" +
    "                                <!--</button>-->\n" +
    "                                <!--<ul class=\"dropdown-menu dropdown-menu-right\">-->\n" +
    "                                <!--<li data-ng-click=\"loadDistrictStats()\"><a href=\"\">Load Set Period</a></li>-->\n" +
    "                                <!--<li class=\"divider\"></li>-->\n" +
    "                                <!--<li data-ng-click=\"fetchAggregatedData()\"><a href=\"\">Show Data Trend</a></li>-->\n" +
    "                                <!--<li class=\"divider\" ng-show=\"showTrend\"></li>-->\n" +
    "                                <!--<li data-ng-click=\"doMultipleEnrolmentUpdates()\" ng-show=\"showTrend\"><a href=\"\">Update Enrollment Totals</a></li>-->\n" +
    "                                <!--</ul>-->\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <h3 class=\"box-title page-header\" ng-show=\"isNotVirgin\">Pupil Analytics</h3>\n" +
    "\n" +
    "            <div class=\"row\" ng-show=\"isNotVirgin\">\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-warning\" >\n" +
    "                        <h3>{{ schoolEnrollmentTotals.total_pupils || 0}} </h3>\n" +
    "                        <p>Total Enrollment<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                    <br>\n" +
    "                    <div class=\"callout callout-success\">\n" +
    "                        <h3>{{ (schoolTeacherAttendanceTotals.total_above_50 * 3)|| 0 }}%</h3>\n" +
    "                        <p>Student Attendance Rate<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-danger\">\n" +
    "                        <h3>{{  schoolEnrollmentTotals.total_male }} <small class=\"smaller\">BOYS</small> | {{ schoolEnrollmentTotals.total_female}} <small class=\"smaller\">GIRLS</small></h3>\n" +
    "                        <p>Boys | Girls Enrolled<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                    <br>\n" +
    "                    <div class=\"callout callout-info\">\n" +
    "                        <h3>{{  schoolSpecialEnrollmentTotals.total_male }} <small class=\"smaller\">BOYS</small> | {{ schoolSpecialEnrollmentTotals.total_female}} <small class=\"smaller\">GIRLS</small></h3>\n" +
    "                        <p>Special Need Students Enrolled</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-6\">\n" +
    "                    <!--<span ng-if=\"enrollmentBarChartData.length\" target=\"enrollmentBarChart\" chart-js-bar=\"enrollmentBarChartData\" labels=\"enrollmentBarChart.labels\" label=\"{{enrollmentBarChart.label}}\"></span>-->\n" +
    "                    <div class=\"chart-container\" style=\"position: relative; width:100%\">\n" +
    "                        <canvas id=\"barChart\" width=\"400\" height=\"300\" style=\"width: 100%; height: 300px\"></canvas>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "            <!--<div class=\"row\">-->\n" +
    "            <!--<article id=\"attendanceChartDiv\" style=\"width: 100%; height: 400px; background-color: #fefefe\"></article>-->\n" +
    "            <!--</div>-->\n" +
    "\n" +
    "            <h3 class=\"box-title page-header\" ng-show=\"isNotVirgin\">Pupil Performance</h3>\n" +
    "\n" +
    "\n" +
    "\n" +
    "            <!--Average Performance of Pupils by subject-->\n" +
    "            <!--Average Performance of Pupils by class-->\n" +
    "            <!--Pupil Attendance Ratio-->\n" +
    "            <!--Class to Textbook Ratio-->\n" +
    "\n" +
    "            <!--<div class=\"row\">-->\n" +
    "            <!--<div class=\"col-sm-12 col-xs-12\">-->\n" +
    "            <!--<div class=\"alert alert-warning\">-->\n" +
    "\n" +
    "            <!--<h3><i class=\"\"></i> Pupil Attendance Ratio</h3>-->\n" +
    "\n" +
    "            <!--</div>-->\n" +
    "            <!--</div>-->\n" +
    "            <!--<div class=\"col-sm-6 col-xs-12\">-->\n" +
    "            <!--<div class=\"alert alert-warning\">-->\n" +
    "\n" +
    "            <!--<h3>Class to Textbook Ratio</h3>-->\n" +
    "\n" +
    "            <!--</div>-->\n" +
    "            <!--</div>-->\n" +
    "            <!--</div>-->\n" +
    "            <div class=\"row\">\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6\" ng-show=\"isNotVirgin\">\n" +
    "                    <div class=\"alert alert-grey\">\n" +
    "                        <h3>Performance of Pupils by Class</h3>\n" +
    "                        <div class=\"table-responsive\">\n" +
    "                            <table class=\"margin-bottom-0 table table-condensed table-bordered\">\n" +
    "                                <thead>\n" +
    "                                <tr>\n" +
    "                                    <td>Class</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\"  colspan=\"2\">Ghanaian Lang</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\" colspan=\"2\">English</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\" colspan=\"2\">Mathematics</td>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                    <td></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">Ave. Score</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">Scoring above Ave. Score</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">Ave. Score</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">Scoring above Ave. Score</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">Ave. Score</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">Scoring above Ave. Score</td>\n" +
    "                                </tr>\n" +
    "                                </thead>\n" +
    "                                <tbody>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['KG1']\">\n" +
    "                                    <td>KG1</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['KG1'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['KG1'].above_average }}</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['KG1'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['KG1'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['KG1'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['KG1'].above_average }} <sup>%</sup></td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['KG2']\">\n" +
    "                                    <td>KG2</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['KG2'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['KG2'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['KG2'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['KG2'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['KG2'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['KG2'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['P1']\">\n" +
    "                                    <td>P1</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P1'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P1'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P1'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P1'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P1'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P1'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['P2']\">\n" +
    "                                    <td>P2</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P2'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P2'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P2'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P2'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P2'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P2'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['P3']\">\n" +
    "                                    <td>P3</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P3'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P3'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P3'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P3'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P3'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P3'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['P4']\">\n" +
    "                                    <td>P4</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P4'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P4'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P4'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P4'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P4'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P4'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['P5']\">\n" +
    "                                    <td>P5</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P5'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P5'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P5'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P5'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P5'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P5'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['P6']\">\n" +
    "                                    <td>P6</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P6'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['P6'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P6'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['P6'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P6'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['P6'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['JHS1']\">\n" +
    "                                    <td>JHS1</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['JHS1'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['JHS1'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['JHS1'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['JHS1'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['JHS1'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['JHS1'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['JHS2']\">\n" +
    "                                    <td>JHS2</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['JHS2'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['JHS2'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['JHS2'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['JHS2'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['JHS2'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['JHS2'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                <tr ng-show=\"schoolPerformanceTotals.levels_available['JHS3']\">\n" +
    "                                    <td>JHS3</td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['JHS3'].average_score }}<sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.gh_lang.class_score['JHS3'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['JHS3'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.english.class_score['JHS3'].above_average }} </td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['JHS3'].average_score }} <sup>%</sup></td>\n" +
    "                                    <td class=\"horizontal_vertical_center\">{{ schoolPerformanceTotals.math.class_score['JHS3'].above_average }} </td>\n" +
    "                                </tr>\n" +
    "                                </tbody>\n" +
    "                            </table>\n" +
    "                        </div>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-12 col-sm-6\">\n" +
    "                    <!--<span ng-if=\"performanceBarChart.datasets.length\" target=\"performanceBarChart\" chart-js-horizontal-bar=\"performanceBarChart.datasets\" labels=\"performanceBarChart.labels\" label=\"{{performanceBarChart.label}}\"></span>-->\n" +
    "                    <div class=\"chart-container\" style=\"position: relative; width:100%\">\n" +
    "                        <canvas id=\"horizontalBarChart\" width=\"400\" height=\"350\" style=\"width: 100%; height: 350px\"></canvas>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <h3 class=\"box-title page-header\" ng-show=\"isNotVirgin\">Teacher Analytics</h3>\n" +
    "            <!--Teacher Attendance Ratio-->\n" +
    "            <!--Pupil Teacher Ratio-->\n" +
    "            <!--Percentage of Trained Teacher-->\n" +
    "            <div class=\"row\" ng-show=\"isNotVirgin\">\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-warning\" >\n" +
    "                        <h3>{{ teachers_in_school.trained || 0}} </h3>\n" +
    "                        <p>Number of Trained Teachers<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                    <br>\n" +
    "                    <div class=\"callout callout-info\">\n" +
    "                        <h3>{{ (schoolTeacherAttendanceTotals.attendanceRatio | currency : '' : 0) || 0 }} <sup ng-show=\"schoolTeacherAttendanceTotals.attendanceRatio\">%</sup></h3>\n" +
    "                        <p>Teacher Attendance Rate<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\" style=\"margin-top: 50px\">\n" +
    "                    <div class=\"chart-container\" style=\"position: relative; width:100%\">\n" +
    "                        <canvas id=\"pieChartOne\" width=\"400\" height=\"300\" style=\"width: 100%; height: 300px\"></canvas>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-danger\">\n" +
    "                        <h3 ng-show=\"pupilTeacherRatio\">{{ pupilTeacherRatio }} : 1</h3>\n" +
    "                        <h3 ng-hide=\"pupilTeacherRatio\">-</h3>\n" +
    "                        <p>Pupil Teacher Ratio<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                    <br>\n" +
    "                    <div class=\"callout callout-success\">\n" +
    "                        <h3>{{ schoolTeacherAttendanceTotals.total_above_50 || 0 }}&nbsp;<small ng-show=\"schoolTeacherAttendanceTotals.total_above_50\">/&nbsp;{{ teachers_in_school.length || 0 }}</small></h3>\n" +
    "                        <p>Teachers with Punctuality above 50%</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3 \"  style=\"margin-top: 50px\">\n" +
    "                    <div class=\"chart-container\" style=\"position: relative; width:100%\">\n" +
    "                        <canvas id=\"percentOverFiftyPie\" width=\"400\" height=\"300\" style=\"width: 100%; height: 300px\"></canvas>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"isNotVirgin\">\n" +
    "                <div class=\"col-xs-12 col-sm-4\">\n" +
    "                    <div class=\"callout callout-warning\" >\n" +
    "                        <h3>{{ schoolUnitCoveredTotals.gh_lang.total || 0}} </h3>\n" +
    "                        <p>Units Covered in Ghanaian Language</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-12 col-sm-4\">\n" +
    "                    <div class=\"callout callout-danger\" >\n" +
    "                        <h3>{{ schoolUnitCoveredTotals.english.total || 0}} </h3>\n" +
    "                        <p>Units Covered in English Language</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-12 col-sm-4\">\n" +
    "                    <div class=\"callout callout-success\" >\n" +
    "                        <h3>{{ schoolUnitCoveredTotals.math.total || 0}} </h3>\n" +
    "                        <p>Units Covered in Mathematics</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <h3 class=\"box-title page-header\" ng-show=\"isNotVirgin\">School Analytics</h3>\n" +
    "            <div class=\"row\" ng-show=\"isNotVirgin\" >\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-info\" >\n" +
    "                        <h3>{{ schoolTeacherAttendanceTotals.num_of_exercises_marked || 0 }} <small ng-show=\"schoolTeacherAttendanceTotals.num_of_exercises_marked\">/ {{ schoolTeacherAttendanceTotals.num_of_exercises_given}}</small></h3>\n" +
    "                        <p>Exercises Marked <br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-danger\">\n" +
    "                        <h3><small ng-show=\"schoolGrantStuff.totalAmount\">GHS</small> {{ schoolGrantStuff.totalAmount || 0 }} </h3>\n" +
    "                        <p>Grant Received<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-warning \">\n" +
    "                        <h3>\n" +
    "                            <small ng-show=\"schoolSupportStuff.total_cash\">GHS</small> {{ schoolSupportStuff.total_cash || '' }}\n" +
    "\n" +
    "                            <small ng-show=\"schoolSupportStuff.total_cash && schoolSupportStuff.received_support\">and some in kind</small>\n" +
    "\n" +
    "                            <small ng-show=\"!schoolSupportStuff.total_cash && schoolSupportStuff.received_support\">No Cash | Received Items</small>\n" +
    "\n" +
    "                            <small ng-show=\"!schoolSupportStuff.total_cash && !schoolSupportStuff.received_support\">No Support Received</small></h3>\n" +
    "                        <p>Support Received (cash, kind or both)</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-3\">\n" +
    "                    <div class=\"callout callout-success \">\n" +
    "                        <h3>{{ schoolMeeting.total_frequency || 0}}</h3>\n" +
    "                        <p>Meetings Held<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\" ng-show=\"isNotVirgin\">\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-6\">\n" +
    "                    <div class=\"callout callout-info\">\n" +
    "                        <h3>{{ schoolFurniture.total_adequates.length || 0}} <small ng-show=\"schoolFurniture.total_adequates.length\">/ 4</small></h3>\n" +
    "                        <p>Adequate Furniture Available</p>\n" +
    "                        <div class=\"box-body no-padding\">\n" +
    "                            <table  class=\"table table-striped table-bordered table-hover\" id=\"furnitureTable\">\n" +
    "                                <thead>\n" +
    "                                <tr class=\"bg-primary\">\n" +
    "                                    <th style=\"text-align: center\">Term</th>\n" +
    "                                    <th style=\"text-align: center\">Furniture</th>\n" +
    "                                    <th style=\"text-align: center\">Status</th>\n" +
    "                                    <th style=\"text-align: center\" class=\"hidden\">Comment</th>\n" +
    "                                </tr>\n" +
    "                                </thead>\n" +
    "                                <tbody class=\"\">\n" +
    "\n" +
    "                                <tr ng-repeat=\"furniture in furnitureItems |   filter: {term : filter_term, year : filter_year, data_collector_type : filter_data_collector_type}\">\n" +
    "                                    <td align=\"center\">{{one_school_terms[furniture.term].number}}</td>\n" +
    "                                    <td align=\"center \">{{furniture.name}}</td>\n" +
    "                                    <td align=\"center\" ng-class=\"{'bg-green' : furniture.status == 'Adequate', 'bg-yellow' : furniture.status == 'Inadequate', 'bg-red' : furniture.status == 'Not Available'} \">\n" +
    "                                        <i class=\"fa\" ng-class=\"{true : 'fa-smile-o', false : 'fa-frown-o'}[furniture.status == 'Adequate']\"></i>\n" +
    "                                        &nbsp;{{furniture.status}}</td>\n" +
    "                                    <td class=\"hidden\">{{ furniture.comment }}</td>\n" +
    "                                </tr>\n" +
    "                                </tbody>\n" +
    "                            </table>\n" +
    "\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-6\">\n" +
    "                    <div class=\"callout callout-warning \">\n" +
    "                        <h3>{{ schoolSecurity.total_availables.length || 0}} <small ng-show=\"schoolSecurity.total_availables.length\">/ 4</small></h3>\n" +
    "                        <p>Security Facilities Available</p>\n" +
    "                        <div class=\"box-body no-padding\">\n" +
    "                            <div class=\"\">\n" +
    "                                <table class=\"table table-striped table-bordered table-hover\" id=\"securityTable\">\n" +
    "                                    <thead>\n" +
    "                                    <tr  class=\"bg-primary\">\n" +
    "                                        <th style=\"text-align: center\">Term</th>\n" +
    "                                        <th style=\"text-align: center\">Security</th>\n" +
    "                                        <th style=\"text-align: center\">Status</th>\n" +
    "                                        <th style=\"text-align: center\" class=\"hidden\">Comments</th>\n" +
    "                                    </tr>\n" +
    "                                    </thead>\n" +
    "                                    <tbody>\n" +
    "                                    <tr ng-repeat=\"security in securityItems |  filter: {term : filter_term, year : filter_year, data_collector_type : filter_data_collector_type}\">\n" +
    "                                        <!--<tr ng-repeat=\"sanitation in sanitationItems\">-->\n" +
    "                                        <td align=\"center\">{{ one_school_terms[security.term].number}}</td>\n" +
    "                                        <td align=\"center\">{{security.name}}</td>\n" +
    "                                        <!--<td align=\"center\"><span check-mark status=\"{{book.status}}\"></span></td>-->\n" +
    "                                        <td align=\"center\" ng-class=\"{true : 'bg-green', false : 'bg-red'}[security.status == 'Available' ]\">\n" +
    "                                            <i class=\"fa \" ng-class=\"{true : 'fa-check', false : 'fa-times'}[security.status == 'Available']\"></i>\n" +
    "                                            &nbsp;{{security.status}}</td>\n" +
    "                                        <td class=\"hidden\">{{ security.comment}}</td>\n" +
    "                                    </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"isNotVirgin\">\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-6\">\n" +
    "                    <div class=\"callout callout-danger\">\n" +
    "                        <h3>{{ schoolStructure.total_goods.length || 0}} <small ng-show=\"schoolStructure.total_goods.length\">/ 6</small></h3>\n" +
    "                        <p>School Structure Status</p>\n" +
    "\n" +
    "                        <div class=\"box-body no-padding\">\n" +
    "                            <div class=\"\">\n" +
    "                                <table class=\"table table-striped table-bordered table-hover\" id=\"structureTable\">\n" +
    "                                    <thead>\n" +
    "                                    <tr  class=\"bg-primary\">\n" +
    "                                        <th style=\"text-align: center\">Term</th>\n" +
    "                                        <th style=\"text-align: center\">Structure</th>\n" +
    "                                        <th style=\"text-align: center\">Status</th>\n" +
    "                                        <th style=\"text-align: center\" class=\"hidden\">Comments</th>\n" +
    "                                    </tr>\n" +
    "                                    </thead>\n" +
    "                                    <tbody>\n" +
    "                                    <tr  ng-repeat=\"structure in structureItems |   filter: {term : filter_term, year : filter_year, data_collector_type : filter_data_collector_type}\">\n" +
    "                                        <!--<tr ng-repeat=\"sanitation in sanitationItems\">-->\n" +
    "                                        <td align=\"center\">{{ one_school_terms[structure.term].number}}</td>\n" +
    "                                        <td align=\"center\">{{structure.name}}</td>\n" +
    "                                        <!--<td align=\"center\"><span check-mark status=\"{{book.status}}\"></span></td>-->\n" +
    "                                        <td align=\"center\" ng-class=\"{\n" +
    "                                         'bg-green' : structure.status == 'Good',\n" +
    "                                         'bg-yellow' : structure.status == 'Fair',\n" +
    "                                          'bg-red' : structure.status == 'Poor'}\">\n" +
    "\n" +
    "                                            <i class=\"fa\" ng-show=\"structure.status\" ng-class=\"{true : 'fa-smile-o', false : 'fa-frown-o'}[structure.status == 'Good']\"></i>\n" +
    "                                            &nbsp;{{structure.status}}</td>\n" +
    "                                        <td class=\"hidden\">{{ structure.comment}}</td>\n" +
    "                                    </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-6\">\n" +
    "                    <div class=\"callout callout-success \">\n" +
    "                        <h3>{{ schoolSanitation.total_availables.length || 0}} <small ng-show=\"schoolSanitation.total_availables.length\">/ 5</small></h3>\n" +
    "                        <p>Sanitation Facilities Available</p>\n" +
    "\n" +
    "                        <div class=\"box-body no-padding\">\n" +
    "                            <div class=\"\">\n" +
    "                                <!--This is the Table Export Button-->\n" +
    "                                <!--<button class=\"btn btn-warning\"  export-table=\"sanitationTable\" file-name=\"{{selected_school.name + ' School Sanitation' +  ' ' + filter_term + ' ' + filter_year}}\">Begin Export</button>-->\n" +
    "                                <table class=\"table table-striped table-bordered table-hover\" id=\"sanitationTable\">\n" +
    "                                    <thead>\n" +
    "                                    <tr class=\"bg-primary\">\n" +
    "                                        <th style=\"text-align: center\">Term</th>\n" +
    "                                        <th style=\"text-align: center\">Item</th>\n" +
    "                                        <th style=\"text-align: center\">Status</th>\n" +
    "                                        <th style=\"text-align: center\" class=\"hidden\">Comments</th>\n" +
    "                                    </tr>\n" +
    "                                    </thead>\n" +
    "                                    <tbody>\n" +
    "                                    <tr ng-repeat=\"sanitation in sanitationItems | filter: {term : filter_term, year : filter_year, data_collector_type : filter_data_collector_type}\">\n" +
    "                                        <!--<tr ng-repeat=\"sanitation in sanitationItems\">-->\n" +
    "                                        <td align=\"center\">{{one_school_terms[sanitation.term].number}}</td>\n" +
    "                                        <td align=\"center\">{{sanitation.name}}</td>\n" +
    "                                        <td align=\"center\"\n" +
    "                                            ng-class=\"{\n" +
    "                                'bg-green' : sanitation.status == 'Available' || sanitation.status == 'Available, functional' ,\n" +
    "                                'bg-yellow' : sanitation.status == 'Available, not functional',\n" +
    "                                'bg-red' : sanitation.status == 'Not available'\n" +
    "                                }\">\n" +
    "                                            <!--<i class=\"fa \" ng-class=\"{true : 'fa-check', false : 'fa-times'}[sanitation.status == 'Available']\"></i>-->\n" +
    "                                            <i class=\"fa \" ng-class=\"{\n" +
    "                                'fa-check' : sanitation.status == 'Available' || sanitation.status == 'Available, functional' ,\n" +
    "                                'fa-exclamation-circle' : sanitation.status == 'Available, not functional',\n" +
    "                                'fa-times' : sanitation.status == 'Not available'\n" +
    "                                }\"></i>\n" +
    "                                            &nbsp;{{sanitation.status}}</td>\n" +
    "                                        <td class=\"hidden\">{{sanitation.comment}}</td>\n" +
    "                                    </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\" ng-show=\"isNotVirgin\">\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-6\">\n" +
    "                    <div class=\"callout callout-warning\">\n" +
    "                        <h3>{{ schoolRecreation.total_availables.length || 0}}<small ng-show=\"schoolRecreation.total_availables.length\">/ 8</small></h3>\n" +
    "                        <p>Recreational Facilities Available</p>\n" +
    "                        <div class=\"box-body no-padding\">\n" +
    "                            <div class=\"\">\n" +
    "                                <table  class=\"table table-striped table-bordered table-hover\" id=\"sportsTable\">\n" +
    "                                    <thead>\n" +
    "                                    <tr  class=\"bg-primary\">\n" +
    "                                        <th style=\"text-align: center\">Term</th>\n" +
    "                                        <th style=\"text-align: center\">Sports and Recreation</th>\n" +
    "                                        <th style=\"text-align: center\">Status</th>\n" +
    "                                        <th style=\"text-align: center\" class=\"hidden\">Comments</th>\n" +
    "                                    </tr>\n" +
    "                                    </thead>\n" +
    "                                    <tbody>\n" +
    "\n" +
    "                                    <tr ng-repeat=\"recreation in recreationItems |  filter: {term : filter_term, year : filter_year, data_collector_type : filter_data_collector_type}\">\n" +
    "                                        <!--<tr ng-repeat=\"sanitation in sanitationItems\">-->\n" +
    "                                        <td align=\"center\">{{ one_school_terms[recreation.term].number}}</td>\n" +
    "                                        <td align=\"center\">{{recreation.name}}</td>\n" +
    "                                        <td align=\"center\" ng-class=\"{true : 'bg-green', false : 'bg-red'}[recreation.status == 'Available' || recreation.status == 'Adequate' ]\">\n" +
    "                                            <i class=\"fa \" ng-class=\"{true : 'fa-check', false : 'fa-times'}[recreation.status == 'Available' || recreation.status == 'Adequate']\"></i>\n" +
    "                                            &nbsp;{{recreation.status}}</td>\n" +
    "                                        <td class=\"hidden\">{{ recreation.comment }}</td>\n" +
    "                                    </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-6\">\n" +
    "                    <div class=\"callout callout-info\">\n" +
    "                        <h3>{{ schoolRecordBooks.books_available.length || 0 }} <small ng-show=\"schoolRecordBooks.books_available.length\">/ 11</small></h3>\n" +
    "                        <p>Number of Record Books</p>\n" +
    "\n" +
    "                        <div class=\"box-body no-padding\">\n" +
    "                            <div class=\"\">\n" +
    "                                <table class=\"table table-striped table-bordered table-hover\" id=\"recordsTable\">\n" +
    "                                    <thead>\n" +
    "                                    <tr class=\"bg-primary\">\n" +
    "                                        <th style=\"text-align: center\">Term</th>\n" +
    "                                        <th style=\"text-align: center\">Book Name</th>\n" +
    "                                        <th style=\"text-align: center\">Status</th>\n" +
    "                                    </tr>\n" +
    "                                    </thead>\n" +
    "                                    <tbody>\n" +
    "                                    <tr  ng-repeat=\"book in books |  filter: {term : filter_term, year : filter_year, data_collector_type :  filter_data_collector_type}\">\n" +
    "                                        <td align=\"center\">{{ one_school_terms[book.term].number}}</td>\n" +
    "                                        <td align=\"center\">{{book.name}}</td>\n" +
    "                                        <td align=\"center\" ng-class=\"{true : 'bg-green', false : 'bg-red'}[book.status == 'Yes' ]\">\n" +
    "                                            <i class=\"fa \" ng-class=\"{true : 'fa-check', false : 'fa-times'}[book.status == 'Yes']\"></i> &nbsp; {{book.status}}</td>\n" +
    "                                    </tr>\n" +
    "                                    </tbody>\n" +
    "                                </table>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-xs-12 col-sm-6 col-md-4 hidden\">\n" +
    "                    <div class=\"callout callout-info \">\n" +
    "                        <h3>{{ schoolCommInvolvement.total_goods.length || 0}} <small ng-show=\"schoolCommInvolvement.total_goods.length\">/ 4</small></h3>\n" +
    "                        <p>Active Community Participation<br>&nbsp;</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"row\" ng-if=\"!isNotVirgin\" style=\"margin: 80px 0\">\n" +
    "                <h3 class=\"horizontal_vertical_center text-muted\">Tap to load statistics for {{ selected_school.name}} School</h3>\n" +
    "            </div>\n" +
    "\n" +
    "            <!--<div style=\"width:75%;\">-->\n" +
    "            <!--<canvas id=\"myChart\" width=\"300\" height=\"300\"></canvas>-->\n" +
    "            <!--</div>-->\n" +
    "\n" +
    "        </div>\n" +
    "    </div><!-- /.box-body -->\n" +
    "    <!-- Loading (remove the following to stop the loading)-->\n" +
    "    <div class=\"overlay\" ng-show=\"loading\"></div>\n" +
    "    <div class=\"loading-img\" ng-show=\"loading\"  style=\"padding: 50px\"></div>\n" +
    "    <!-- end loading -->\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("partials/view_selected_school.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/view_selected_school.html",
    "<h4 class=\"page-header\">\n" +
    "    {{selected_school.name}}\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-10\">\n" +
    "            <small> {{ selected_school.description }}</small>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-2\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <!--<button class=\"btn btn-primary\" type=\"button\" data-toggle=\"modal\" data-target=\"#schoolSettingsModal\">Settings</button>-->\n" +
    "                <button data-toggle=\"dropdown\" class=\"btn btn-primary dropdown-toggle\" type=\"button\">\n" +
    "                    Actions <span class=\"caret\"></span>\n" +
    "                    <span class=\"sr-only\">School Settings Dropdown</span>\n" +
    "                </button>\n" +
    "                <ul role=\"menu\" class=\"dropdown-menu dropdown-menu-right\">\n" +
    "                    <!--<li><a class=\"pointer\" send-email=\"\" href=\"\">Send Email</a></li>-->\n" +
    "                    <li><a class=\"pointer\" report-selection-modal=\"selected_school\"\n" +
    "                           data-school-weeks=\"schoolWeeks\"\n" +
    "                           data-school-terms=\"schoolTerms\"\n" +
    "                           data-school-years=\"schoolYears\">Generate School Report Card</a></li>\n" +
    "\n" +
    "                    <li><a ui-sref=\"dashboard.school.add_school({action : 'edit', id : selected_school.id})\">Edit School Details</a></li>\n" +
    "                    <li><a class=\"pointer\" href=\"\" ng-click=\"reloadSchools()\">Reload School Information</a></li>\n" +
    "                    <li><a class=\"pointer\" href=\"\" ng-click=\"emptyCache()\">Reset Loaded Data</a></li>\n" +
    "                    <li class=\"divider\"></li>\n" +
    "                    <li><a href=\"\" years=\"schoolYears\" update-school=\"selected_school\">Update All School Data</a></li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <!--<button class=\"pull-right btn btn-primary\" ><i class=\"fa fa-plus\"></i>District Settings</button>-->\n" +
    "</h4>\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "\n" +
    "    <div class=\"col-sm-6 col-xs-12\">\n" +
    "        <!-- small box -->\n" +
    "        <div class=\"small-box bg-blue\">\n" +
    "            <div class=\"inner\">\n" +
    "                <h3 ng-show=\"teachers_in_school\">\n" +
    "                    {{ teachers_in_school.atpost }}\n" +
    "                    <sup style=\"font-size: 20px\"> / {{teachers_in_school.length}}</sup>\n" +
    "                </h3>\n" +
    "                <h3 ng-hide=\"teachers_in_school\">\n" +
    "                    0\n" +
    "                </h3>\n" +
    "                <p>Teacher<span ng-hide=\"teachers_in_school.length == 1\">s At Post</span>\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"icon\">\n" +
    "                <i class=\"fa fa-spinner fa-spin\" ng-class=\"{true : 'fa fa-spinner fa-spin', false : 'ion ion-android-social-user'}[loadingTeachers]\"></i>\n" +
    "                <!--<i class=\"ion ion-android-social-user\" ng-hide=\"loadingTeachers\"></i>-->\n" +
    "            </div>\n" +
    "            <a class=\"small-box-footer\"  data-toggle=\"tab\" data-target=\"#reportCard\" anchor-scroll=\"reportCard\" ui-sref=\"dashboard.school.view_selected_school.report_card.teacher_information\">\n" +
    "                More info <i class=\"fa fa-arrow-circle-right\"></i>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div><!-- ./col -->\n" +
    "\n" +
    "    <div class=\"col-sm-6 col-xs-12\">\n" +
    "        <!-- small box -->\n" +
    "        <div class=\"small-box bg-teal\">\n" +
    "            <div class=\"inner\">\n" +
    "                <h4>\n" +
    "                    {{ teachers_in_school.studyleave || ''}} &nbsp; <span ng-show=\"teachers_in_school.studyleave\">on Study Leave</span>\n" +
    "                </h4>\n" +
    "                <h4 >\n" +
    "                    {{ teachers_in_school.otherleave || '' }} &nbsp; <span ng-show=\"teachers_in_school.otherleave\">on Other Leave</span>\n" +
    "                </h4>\n" +
    "                <h4 ng-show=\"teachers_in_school.retired> 0\">\n" +
    "                    {{ teachers_in_school.retired}} &nbsp; on Retirement\n" +
    "                    <!--<sup style=\"font-size: 20px\">%</sup>-->\n" +
    "                </h4>\n" +
    "                <h4 ng-show=\"teachers_in_school.deceased > 0\">\n" +
    "                    {{ teachers_in_school.deceased}} &nbsp; Deceased\n" +
    "                </h4>\n" +
    "            </div>\n" +
    "            <div class=\"icon\">\n" +
    "                <i class=\"ion ion-android-star\"></i>\n" +
    "            </div>\n" +
    "            <a class=\"small-box-footer\"  data-toggle=\"tab\" data-target=\"#reportCard\" anchor-scroll=\"reportCard\" ui-sref=\"dashboard.school.view_selected_school.report_card.teacher_information\">\n" +
    "                More info <i class=\"fa fa-arrow-circle-right\"></i>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div><!-- ./col -->\n" +
    "    <!--<div class=\"col-lg-3 col-xs-6\">-->\n" +
    "    <!--&lt;!&ndash; small box &ndash;&gt;-->\n" +
    "    <!--<div class=\"small-box bg-purple\">-->\n" +
    "    <!--<div class=\"inner\">-->\n" +
    "    <!--<h3 ng-show=\"selected_school.current_teacher_attendance_by_circuit_supervisor > 0\">-->\n" +
    "    <!--{{selected_school.current_teacher_attendance_by_circuit_supervisor | number : 2 }}%-->\n" +
    "    <!--</h3>-->\n" +
    "    <!--<h4 ng-hide=\"selected_school.current_teacher_attendance_by_circuit_supervisor > 0\">-->\n" +
    "    <!--No data submitted for-->\n" +
    "    <!--</h4>-->\n" +
    "    <!--<p>-->\n" +
    "    <!--Average Teacher Attendance (CS)-->\n" +
    "    <!--</p>-->\n" +
    "    <!--</div>-->\n" +
    "    <!--<div class=\"icon hidden\">-->\n" +
    "    <!--<i class=\"ion ion-ios7-alarm-outline\"></i>-->\n" +
    "    <!--</div>-->\n" +
    "    <!--<a class=\"small-box-footer\" data-toggle=\"tab\" data-target=\"#reportCard\" anchor-scroll=\"reportCard\" ui-sref=\"dashboard.school.view_selected_school.report_card.teacher_information\">-->\n" +
    "    <!--More info <i class=\"fa fa-arrow-circle-right\"></i>-->\n" +
    "    <!--</a>-->\n" +
    "    <!--</div>-->\n" +
    "    <!--</div>&lt;!&ndash; ./col &ndash;&gt;-->\n" +
    "    <div class=\"col-lg-6 col-xs-6 hidden\">\n" +
    "        <!-- small box -->\n" +
    "        <div class=\"small-box bg-maroon\">\n" +
    "            <div class=\"inner\">\n" +
    "                <h3 ng-show=\"selected_school.current_teacher_attendance_by_headteacher\">\n" +
    "                    {{selected_school.current_teacher_attendance_by_headteacher | number}}%\n" +
    "                    <!--<sup style=\"font-size: 20px\">{{ positionExtension( selected_school.position ) }}</sup>-->\n" +
    "                </h3>\n" +
    "                <h4 ng-hide=\"selected_school.current_teacher_attendance_by_headteacher\">\n" +
    "                    No data submitted for\n" +
    "                </h4>\n" +
    "                <p>\n" +
    "                    Average Teacher Attendance (HT)\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"icon hidden\">\n" +
    "                <i class=\"ion ion-ios7-pricetag-outline\"></i>\n" +
    "            </div>\n" +
    "            <a class=\"small-box-footer\" data-toggle=\"tab\" data-target=\"#reportCard\" anchor-scroll=\"reportCard\" ui-sref=\"dashboard.school.view_selected_school.report_card.teacher_information\">\n" +
    "                More info <i class=\"fa fa-arrow-circle-right\"></i>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div><!-- ./col -->\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "<h4 class=\"page-header\">\n" +
    "    School Details\n" +
    "    <!--<small>This view shows a snapshot of the details of the school. To see the details click the <code>View More</code> button.</small>-->\n" +
    "</h4>\n" +
    "<div class=\"row\">\n" +
    "    <div class=\"col-sm-12 col-xs-12\">\n" +
    "        <!--<b style=\"border-bottom-style: none\" class=\"page-header\">Circuit Details</b>-->\n" +
    "        <div class=\"box box-success\">\n" +
    "            <div class=\"box-header\">\n" +
    "                <h4 class=\"box-title\">{{selected_school.name}}</h4>\n" +
    "            </div>\n" +
    "            <div class=\"box-body\">\n" +
    "                <div class=\"\">\n" +
    "                    <table class=\"table table-bordered table-responsive\">\n" +
    "                        <tbody>\n" +
    "                        <!--<tr>-->\n" +
    "                            <!--<td>Name of Circuit Supervisor</td>-->\n" +
    "                            <!--<td ng-show=\"lookupDataCollector[selected_school.supervisor_id]\"><a ui-sref=\"dashboard.view_data_collector({id : selected_school.supervisor_id})\">{{ lookupDataCollector[selected_school.supervisor_id].name }} <em>( {{ lookupDataCollector[selected_school.supervisor_id].gender}} )</em></a></td>-->\n" +
    "                            <!--<td ng-hide=\"lookupDataCollector[selected_school.supervisor_id]\">Not Available</td>-->\n" +
    "                        <!--</tr>-->\n" +
    "\n" +
    "                        <!--<tr>-->\n" +
    "                            <!--<td>Name of Head Teacher</td>-->\n" +
    "                            <!--<td ng-show=\"lookupDataCollector[selected_school.headteacher_id]\"><a ui-sref=\"dashboard.view_data_collector({id : selected_school.headteacher_id })\"> {{ lookupDataCollector[selected_school.headteacher_id].name }} <em>( {{ lookupDataCollector[selected_school.headteacher_id].gender}} )</em></a></td>-->\n" +
    "                            <!--<td ng-hide=\"lookupDataCollector[selected_school.headteacher_id]\">Not Available</td>-->\n" +
    "                        <!--</tr>-->\n" +
    "                        <tr>\n" +
    "                            <td>School GES Code</td>\n" +
    "                            <td>{{selected_school.ges_code}}</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>School SMS Code</td>\n" +
    "                            <td>{{selected_school.sms_code}}</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>School Email</td>\n" +
    "                            <td>{{selected_school.email || 'Not provided' }}</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>School Phone Number</td>\n" +
    "                            <td>{{selected_school.phone_number}}</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>School Address</td>\n" +
    "                            <td>{{selected_school.address}}</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>Ghanaian Language Taught</td>\n" +
    "                            <td>{{selected_school.ghanaian_language}}</td>\n" +
    "                        </tr>\n" +
    "                        <tr>\n" +
    "                            <td>Shift System</td>\n" +
    "                            <td ng-show=\"selected_school.shift == 0\">No</td>\n" +
    "                            <td ng-hide=\"selected_school.shift == 0\">Yes</td>\n" +
    "                        </tr>\n" +
    "                        <!--<tr>-->\n" +
    "                            <!--<td>Last Updated</td>-->\n" +
    "                            <!--<td tooltip=\"{{selected_school.updated_at}}\">{{ momentFormatDate(selected_school.updated_at, 'llll')}}</td>-->\n" +
    "                        <!--</tr>-->\n" +
    "                        <!--<tr>-->\n" +
    "                            <!--<td>Circuit</td>-->\n" +
    "                            <!--<td><a   ng-show=\"adminRight > 1\" ui-sref=\"dashboard.view_selected_circuit({id : selected_school.circuit_id})\">  {{ selected_school.circuitName}}</a>-->\n" +
    "                                <!--<span ng-show=\"adminRight < 2\" > {{   selected_school.circuitName   }}</span>-->\n" +
    "                            <!--</td>-->\n" +
    "                        <!--</tr>-->\n" +
    "                        <!--<tr>-->\n" +
    "                            <!--<td>District</td>-->\n" +
    "                            <!--<td>-->\n" +
    "                                <!--<a ui-sref=\"dashboard.view_selected_district({id : selected_school.district_id})\"-->\n" +
    "                                   <!--ng-show=\"adminRight > 2\">  {{ selected_school.districtName}}</a>-->\n" +
    "                                <!--<span ng-show=\"adminRight < 3\" >{{   selected_school.districtName   }}</span>-->\n" +
    "                            <!--</td>-->\n" +
    "                        <!--</tr>-->\n" +
    "                        <!--<tr>-->\n" +
    "                            <!--<td>Region</td>-->\n" +
    "                            <!--<td><a   ng-show=\"adminRight > 3\" ui-sref=\"dashboard.view_selected_region({id : selected_school.region_id})\">{{selected_school.regionName }}</a>-->\n" +
    "                                <!--<span ng-show=\"adminRight < 4\" >{{   selected_school.regionName   }}</span>-->\n" +
    "                            <!--</td>-->\n" +
    "                        <!--</tr>-->\n" +
    "                        </tbody>\n" +
    "                    </table>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <!--<div class=\"col-md-7 col-xs-12\">\n" +
    "        <div class=\"box box-success\">\n" +
    "            <div class=\"box-header\">\n" +
    "                <h4 class=\"box-title\">Images from the school</h4>\n" +
    "            </div>\n" +
    "            <div class=\"box-body\">\n" +
    "                <div style=\"height: 420px; max-height: 420px; overflow: hidden\" ng-if=\"image.slides.length\">\n" +
    "\n" +
    "                    &lt;!&ndash;Carousel&ndash;&gt;\n" +
    "                    <div id=\"main-slide\" class=\"carousel slide\" data-ride=\"carousel\" style=\"height: 418px\">\n" +
    "\n" +
    "                        &lt;!&ndash; Indicators &ndash;&gt;\n" +
    "                        <ol class=\"carousel-indicators\">\n" +
    "                            <li data-target=\"#main-slide\" data-slide-to=\"{{image_indicator}}\" ng-class=\"{'active' : $index == 0}\"\n" +
    "                                class=\"pointer\" ng-repeat=\"image_indicator in image.indicators\"></li>\n" +
    "                        </ol>&lt;!&ndash;/ Indicators end&ndash;&gt;\n" +
    "\n" +
    "                        &lt;!&ndash; Carousel inner &ndash;&gt;\n" +
    "                        <div class=\"carousel-inner\">\n" +
    "                            <div class=\"item\" ng-repeat=\"image in image.slides\" ng-class=\"{'active' : $index == 0}\">\n" +
    "                                <img class=\"img-responsive\"\n" +
    "                                     pretty-image=\"\"\n" +
    "                                     rel=\"prettyPhoto\"\n" +
    "                                     data-ng-href=\"{{image.image_url }}\"\n" +
    "                                     ng-src=\"{{image.image_url }}\"\n" +
    "                                     title=\"{{image.image_title + '   |   ' + image.image_description}}\"\n" +
    "                                     alt=\"{{image.image_title + '   |   ' + image.image_description}}\"\n" +
    "                                     style=\"margin:auto;\" width=\"610px\" height=\"370px\">\n" +
    "                                <div class=\"carousel-caption\">\n" +
    "                                    <h4>Title : {{image.image_title || 'Untitled' }}</h4>\n" +
    "                                    <h5>Description : {{image.image_description || 'None provided' }}</h5>\n" +
    "                                </div>\n" +
    "                            </div>&lt;!&ndash;/ Carousel item end &ndash;&gt;\n" +
    "                        </div>&lt;!&ndash; Carousel inner end&ndash;&gt;\n" +
    "\n" +
    "                        &lt;!&ndash; Controls &ndash;&gt;\n" +
    "                        <a class=\"left carousel-control\" href=\"\" data-target=\"#main-slide\" data-slide=\"prev\">\n" +
    "                            <span><i class=\"fa fa-angle-left\"></i></span>\n" +
    "                        </a>\n" +
    "                        <a class=\"right carousel-control\" href=\"\" data-target=\"#main-slide\" data-slide=\"next\">\n" +
    "                            <span><i class=\"fa fa-angle-right\"></i></span>\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                    &lt;!&ndash;Carousel End&ndash;&gt;\n" +
    "\n" +
    "                    &lt;!&ndash;<carousel interval=\"myInterval\">&ndash;&gt;\n" +
    "                    &lt;!&ndash;<slide ng-repeat=\"slide in slides\">&ndash;&gt;\n" +
    "                    &lt;!&ndash;<img ng-src=\"{{slide.image_url}}\" alt=\"{{ slide.image_title }}\" style=\"margin:auto;\" width=\"610\" height=\"370\">&ndash;&gt;\n" +
    "                    &lt;!&ndash;<div class=\"carousel-caption\">&ndash;&gt;\n" +
    "                    &lt;!&ndash;<h4>Title: {{ slide.image_title || 'Untitled image' }}</h4>&ndash;&gt;\n" +
    "                    &lt;!&ndash;<p>Description: {{slide.image_description || 'No description provided'}}</p>&ndash;&gt;\n" +
    "                    &lt;!&ndash;</div>&ndash;&gt;\n" +
    "                    &lt;!&ndash;</slide>&ndash;&gt;\n" +
    "                    &lt;!&ndash;</carousel>&ndash;&gt;\n" +
    "                </div>\n" +
    "                <div ng-hide=\"image.slides.length\" style=\"height: 390px\">\n" +
    "                    <img src=\"/img/blue_logo_150.png\" width=\"150px\" class=\"center-block\" style=\"margin-top: 10%\">\n" +
    "                    <p class=\"text-center\" style=\"margin-top: 10%; color: darkgrey; font-size: 23px\">\n" +
    "                        <i class=\"fa fa-warning\"></i> No images uploaded.</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"overlay\" ng-show=\"loadingImages\" ></div>\n" +
    "            <div class=\"loading-img\"  style=\"padding: 50px\" ng-show=\"loadingImages\"></div>\n" +
    "        </div>\n" +
    "    </div>-->\n" +
    "</div>\n" +
    "\n" +
    "<h4 class=\"page-header\">\n" +
    "    {{selected_school.name}}\n" +
    "    <small>{{ selected_school.description }}</small>\n" +
    "</h4>\n" +
    "\n" +
    "<div class=\"row\">\n" +
    "    <div class=\"col-sm-12\">\n" +
    "        <!-- Custom Tabs -->\n" +
    "        <div class=\"nav-tabs-custom\">\n" +
    "            <ul class=\"nav nav-tabs nav-justified\">\n" +
    "                <li class=\"active\" style=\"text-align: center\">\n" +
    "                    <a data-toggle=\"tab\" data-target=\"#statistics\"><h3>Statistics</h3></a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "            <div class=\"tab-content\">\n" +
    "                <div id=\"statistics\" class=\"tab-pane active\">\n" +
    "                    <div ng-include=\"'partials/school_statistics.html'\"></div>\n" +
    "                </div><!-- /.tab-pane -->\n" +
    "            </div><!-- /.tab-content -->\n" +
    "        </div><!-- nav-tabs-custom -->\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);
