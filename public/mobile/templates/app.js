angular.module('templates.app', ['partials/attendance.tpl.html', 'partials/community_involvement.tpl.html', 'partials/enrollment.tpl.html', 'partials/error.tpl.html', 'partials/furniture.tpl.html', 'partials/general_issues.tpl.html', 'partials/grant.tpl.html', 'partials/home.tpl.html', 'partials/meetings.tpl.html', 'partials/pupil_performance.tpl.html', 'partials/record_books.tpl.html', 'partials/sanitation.tpl.html', 'partials/school_structure.tpl.html', 'partials/security.tpl.html', 'partials/sports_recreational.tpl.html', 'partials/support.tpl.html', 'partials/teacher_attendance.tpl.html', 'partials/teacher_dialog_list.tpl.html', 'partials/text_books.tpl.html']);

angular.module("partials/attendance.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/attendance.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "        <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div ng-show=\"completed\">\n" +
    "        <h2 ng-bind=\" 'Attendance data for ' + selected_school.name \"></h2>\n" +
    "        <h3 ng-bind=\"'Week : ' + week_number\"> </h3>\n" +
    "        <h3 ng-bind=\"'Term : ' + term\"></h3>\n" +
    "        <h3 ng-bind=\"'Year : ' + year\"></h3>\n" +
    "        <h3 ng-bind=\"'Days In Session : ' + school_in_session || 'unavailable' \"></h3>\n" +
    "        <h3 ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "        <md-button class=\"md-raised\" ng-click=\"showSpecialNeed = !showSpecialNeed\">\n" +
    "                <span ng-if=\"!(showSpecialNeed)\">Show Special Need Attendance</span>\n" +
    "                <span ng-if=\"showSpecialNeed\">Show Attendance </span>\n" +
    "        </md-button>\n" +
    "        <!--md-swipe-right=\"showSpecialNeed = !showSpecialNeed\"-->\n" +
    "        <!--md-swipe-left=\"showSpecialNeed = !showSpecialNeed\"-->\n" +
    "        <table class=\"rwd-table\"  >\n" +
    "\n" +
    "                <tr>\n" +
    "                        <th>Class</th>\n" +
    "                        <th ng-if=\"!(showSpecialNeed)\">Boys</th>\n" +
    "                        <th ng-if=\"showSpecialNeed\">Spec. Need Boys</th>\n" +
    "                        <th ng-if=\"!(showSpecialNeed)\">Girls</th>\n" +
    "                        <th ng-if=\"showSpecialNeed\">Spec. Need Girls</th>\n" +
    "                        <th>Total</th>\n" +
    "                </tr>\n" +
    "                <tr ng-hide=\"showSpecialNeed\" ng-repeat=\"data in normalAttendanceData\">\n" +
    "                        <td data-th=\"Class\">{{ data.level }}</td>\n" +
    "                        <td data-th=\"Boys\">{{ data.num_males }}</td>\n" +
    "                        <td data-th=\"Girls\">{{ data.num_females }}</td>\n" +
    "                        <td data-th=\"Total\">{{ getTotal(data.num_males, data.num_females) }}</td>\n" +
    "                </tr>\n" +
    "                <tr ng-show=\"showSpecialNeed\" ng-repeat=\"data in specialAttendanceData\">\n" +
    "                        <td data-th=\"Class\">{{ data.level }}</td>\n" +
    "                        <td data-th=\"Spec. Need Boys\">{{ data.num_males }}</td>\n" +
    "                        <td data-th=\"Spec. Need Girls\">{{ data.num_females }}</td>\n" +
    "                        <td data-th=\"Total\">{{ getTotal(data.num_males, data.num_females) }}</td>\n" +
    "                </tr>\n" +
    "\n" +
    "        </table>\n" +
    "</div>\n" +
    "");
}]);

angular.module("partials/community_involvement.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/community_involvement.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Community Involvement</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"commInvolveData.length\">\n" +
    "        <tr>\n" +
    "            <th>Item</th>\n" +
    "            <th style=\"text-align: center\">Status</th>\n" +
    "            <!--<th style=\"text-align: center\">Comment</th>-->\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in commInvolveData\">\n" +
    "            <td data-th=\"Class\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.status}}</td>\n" +
    "            <!--<td style=\"text-align: center\" data-th=\"status\">     {{ data.comment}}</td>-->\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "    <div ng-hide=\"commInvolveData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/enrollment.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/enrollment.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h2 ng-bind=\" 'Enrollment data for ' + selected_school.name \"></h2>\n" +
    "    <h3 ng-bind=\"'Week : ' + week_number\"> </h3>\n" +
    "    <h3 ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3 ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3 ng-bind=\"'Days In Session : ' + school_in_session || 'unavailable' \"></h3>\n" +
    "    <h3 ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <md-button class=\"md-raised\" ng-click=\"showSpecialNeed = !showSpecialNeed\">\n" +
    "        <span ng-if=\"!(showSpecialNeed)\">Show Special Need Enrollment</span>\n" +
    "        <span ng-if=\"showSpecialNeed\">Show Enrollment</span>\n" +
    "    </md-button>\n" +
    "    <!--md-swipe-right=\"showSpecialNeed = !showSpecialNeed\" md-swipe-left=\"showSpecialNeed = !showSpecialNeed\"-->\n" +
    "    <table class=\"rwd-table\" >\n" +
    "        <tr>\n" +
    "            <th>Class</th>\n" +
    "            <th ng-if=\"!(showSpecialNeed)\">Boys</th>\n" +
    "            <th ng-if=\"showSpecialNeed\">Spec. Need Boys</th>\n" +
    "            <th ng-if=\"!(showSpecialNeed)\">Girls</th>\n" +
    "            <th ng-if=\"showSpecialNeed\">Spec. Need Girls</th>\n" +
    "            <th>Total</th>\n" +
    "        </tr>\n" +
    "        <tr ng-hide=\"showSpecialNeed\" ng-repeat=\"data in normalEnrollmentData\">\n" +
    "            <td data-th=\"Class\">{{ data.level }}</td>\n" +
    "            <td data-th=\"Boys\">{{ data.num_males }}</td>\n" +
    "            <td data-th=\"Girls\">{{ data.num_females }}</td>\n" +
    "            <td data-th=\"Total\">{{ getTotal(data.num_males, data.num_females) }}</td>\n" +
    "        </tr>\n" +
    "        <tr ng-show=\"showSpecialNeed\" ng-repeat=\"data in specialEnrollmentData\">\n" +
    "            <td data-th=\"Class\">{{ data.level }}</td>\n" +
    "            <td data-th=\"Spec. Need Boys\">{{ data.num_males }}</td>\n" +
    "            <td data-th=\"Spec. Need Girls\">{{ data.num_females }}</td>\n" +
    "            <td data-th=\"Total\">{{ getTotal(data.num_males, data.num_females) }}</td>\n" +
    "        </tr>\n" +
    "\n" +
    "    </table>\n" +
    "\n" +
    "</div>");
}]);

angular.module("partials/error.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/error.tpl.html",
    "\n" +
    "<md-content layout=\"column\" flex=\"flex\" >\n" +
    "  <h1 flex>Data Not Found</h1>\n" +
    "</md-content>\n" +
    "\n" +
    "");
}]);

angular.module("partials/furniture.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/furniture.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Furniture</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"furnitureData.length\">\n" +
    "        <tr>\n" +
    "            <th>Item</th>\n" +
    "            <th style=\"text-align: center\">Status</th>\n" +
    "            <th style=\"text-align: center\">Comment</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in furnitureData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.status}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.comment}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "    <div ng-hide=\"furnitureData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/general_issues.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/general_issues.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>General Issues</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"submittedDate\">\n" +
    "        <tr>\n" +
    "            <th style=\"\">Issue</th>\n" +
    "            <th style=\"text-align: center\">Response</th>\n" +
    "            <th style=\"text-align: left\">Comment</th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"(key, value) in generalIssueItemsObject\">\n" +
    "            <td  style=\"text-align: left; color: rgba(85,85,85,1); font-size: 18px; background-color: rgba(218, 239, 255, 0.58) !important;\" colspan=\"1\"><b><i>{{ key }}</i></b></td>\n" +
    "            <td>\n" +
    "                <table>\n" +
    "                    <tr ng-repeat=\"issue in value\">\n" +
    "                        <td align=\"left\">{{ issue.situation }}</td>\n" +
    "                        <td style=\"text-align: center\">{{ issue.status }}</td>\n" +
    "                        <td style=\"text-align: center\"> {{issue.comment}}</td>\n" +
    "                    </tr>\n" +
    "                </table>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"2\" style=\"text-align: center\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"submittedDate\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>");
}]);

angular.module("partials/grant.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/grant.tpl.html",
    "\n" +
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Grant</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"grantData.length\">\n" +
    "        <tr>\n" +
    "            <th>Grant Type</th>\n" +
    "            <th style=\"text-align: center\" colspan=\"2\">1st Tranche</th>\n" +
    "            <th style=\"text-align: center\" colspan=\"2\">2nd Tranche</th>\n" +
    "            <th style=\"text-align: center\" colspan=\"2\">3rd Tranche</th>\n" +
    "        </tr>\n" +
    "        <tr style=\"color: white; background-color: #2a2a2a\">\n" +
    "            <th></th>\n" +
    "            <th style=\"text-align: center\">Date</th>\n" +
    "            <th style=\"text-align: center\">Amount</th>\n" +
    "            <th style=\"text-align: center\">Date</th>\n" +
    "            <th style=\"text-align: center\">Amount</th>\n" +
    "            <th style=\"text-align: center\">Date</th>\n" +
    "            <th style=\"text-align: center\">Amount</th>\n" +
    "\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in grantData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">{{ formatGrant[data.school_grant_type] }}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"first_tranche_amount\">{{ data.first_tranche_amount}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"first_tranche_date\">{{ data.first_tranche_date}}</td>\n" +
    "\n" +
    "            <td style=\"text-align: center\" data-th=\"second_tranche_amount\">{{ data.second_tranche_amount}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"second_tranche_amount\">{{ data.second_tranche_date}}</td>\n" +
    "\n" +
    "            <td style=\"text-align: center\" data-th=\"second_tranche_amount\">{{ data.third_tranche_amount}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"second_tranche_amount\">{{ data.third_tranche_date}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"grantData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/home.tpl.html",
    "<div ui-view=\"\">\n" +
    "\n" +
    "    <div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\">\n" +
    "        <h2>Select week, term and year to see submitted data.</h2>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "");
}]);

angular.module("partials/meetings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/meetings.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>School Meetings</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"meetingData.length\">\n" +
    "        <tr>\n" +
    "            <th>Meeting</th>\n" +
    "            <th style=\"text-align: center\">Frequency</th>\n" +
    "            <th style=\"text-align: center\">Males Present</th>\n" +
    "            <th style=\"text-align: center\">Females Present</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in meetingData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Name\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.frequency}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.males_present}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.females_present}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "    <div ng-hide=\"meetingData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/pupil_performance.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/pupil_performance.tpl.html",
    "\n" +
    "\n" +
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Pupil Performance</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"pupilPerformanceData.length\">\n" +
    "        <tr>\n" +
    "            <th>Class</th>\n" +
    "            <th style=\"text-align: center\" colspan=\"2\">Ghanaian Language</th>\n" +
    "            <th style=\"text-align: center\" colspan=\"2\">English</th>\n" +
    "            <th style=\"text-align: center\" colspan=\"2\">Mathematics</th>\n" +
    "            <th style=\"text-align: center\" colspan=\"2\">Science</th>\n" +
    "            <th style=\"text-align: center\">Literacy</th>\n" +
    "        </tr>\n" +
    "        <tr style=\"color: white; background-color: #2a2a2a\">\n" +
    "            <th></th>\n" +
    "            <th style=\"text-align: center\">Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Scoring Above Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Scoring Above Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Scoring Above Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Scoring Above Average Sco.</th>\n" +
    "            <th style=\"text-align: center\">Number That Can Read</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in pupilPerformanceData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.level}}</td>\n" +
    "\n" +
    "            <td style=\"text-align: center\" data-th=\"average_ghanaian_language_score\">     {{ data.average_ghanaian_language_score}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"num_pupil_who_score_above_average_in_ghanaian_language\">     {{ data.num_pupil_who_score_above_average_in_ghanaian_language}}</td>\n" +
    "\n" +
    "            <td style=\"text-align: center\" data-th=\"average_english_score\">     {{ data.average_english_score}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"num_pupil_who_score_above_average_in_english\">     {{ data.num_pupil_who_score_above_average_in_english}}</td>\n" +
    "\n" +
    "            <td style=\"text-align: center\" data-th=\"average_maths_score\">     {{ data.average_maths_score}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"num_pupil_who_score_above_average_in_maths\">     {{ data.num_pupil_who_score_above_average_in_maths}}</td>\n" +
    "\n" +
    "            <td style=\"text-align: center\" data-th=\"science_average_score\">     {{ data.science_average_score}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"number_scoring_above_science_score\">     {{ data.number_scoring_above_science_score}}</td>\n" +
    "\n" +
    "            <td style=\"text-align: center\" data-th=\"num_pupil_who_can_read_and_write\">     {{ data.num_pupil_who_can_read_and_write}}</td>\n" +
    "\n" +
    "        </tr>\n" +
    "\n" +
    "        <tr>\n" +
    "            <td colspan=\"2\">Submitted on</td>\n" +
    "            <td colspan=\"4\">{{ submittedDate | date :  \"dd.MM.y\" }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"pupilPerformanceData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/record_books.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/record_books.tpl.html",
    "\n" +
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Record Books</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"recordBooksData.length\">\n" +
    "        <tr>\n" +
    "\n" +
    "            <th>Book</th>\n" +
    "            <th style=\"text-align: center\">Availability</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in recordBooksData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.status}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"recordBooksData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/sanitation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/sanitation.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Sanitation</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"sanitationData.length\">\n" +
    "        <tr>\n" +
    "            <th>Item</th>\n" +
    "            <th style=\"text-align: center\">Status</th>\n" +
    "            <th style=\"text-align: center\">Comment</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in sanitationData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.status}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.comment}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "\n" +
    "    <div ng-hide=\"sanitationData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>");
}]);

angular.module("partials/school_structure.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/school_structure.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>School Structure</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"structureData.length\">\n" +
    "        <tr>\n" +
    "            <th>Item</th>\n" +
    "            <th style=\"text-align: center\">Status</th>\n" +
    "            <th style=\"text-align: center\">Comment</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in structureData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.status}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.comment}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"structureData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/security.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/security.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Security</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"securityData.length\">\n" +
    "        <tr>\n" +
    "            <th>Item</th>\n" +
    "            <th style=\"text-align: center\">Status</th>\n" +
    "            <th style=\"text-align: center\">Comment</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in securityData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.status}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.comment}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"securityData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/sports_recreational.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/sports_recreational.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Sports and Recreational Items</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"sportsRecreationData.length\">\n" +
    "        <tr>\n" +
    "            <th>Item</th>\n" +
    "            <th style=\"text-align: center\">Status</th>\n" +
    "            <th style=\"text-align: center\">Comment</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in sportsRecreationData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.status}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"status\">     {{ data.comment}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"sportsRecreationData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/support.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/support.tpl.html",
    "\n" +
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Support</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "    <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "    <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"supportData.length\">\n" +
    "        <tr>\n" +
    "\n" +
    "            <th>Support Type</th>\n" +
    "            <th style=\"text-align: center\">Cash</th>\n" +
    "            <th style=\"text-align: center\">Kind</th>\n" +
    "            <th style=\"text-align: center\">Comment</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in supportData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Name\">{{ data.name}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"Cash\">{{ data.in_cash}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"Kind\">{{ data.in_kind}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"Comment\">{{ data.comment}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"supportData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("partials/teacher_attendance.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/teacher_attendance.tpl.html",
    "<!--<pre>{{ teacherAttendanceData | json }}</pre>-->\n" +
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Teacher Attendance Information </h2>\n" +
    "    <h3  ng-bind=\"'Week : ' + week_number\"> </h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Days In Session : ' + school_in_session \"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <md-button class=\"md-raised\" ng-click=\"showClassManagement = !showClassManagement\">\n" +
    "        <span ng-if=\"!(showClassManagement)\">Show Teacher Class Management</span>\n" +
    "        <span ng-if=\"showClassManagement\">Show Teacher Attendance</span>\n" +
    "    </md-button>\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" >\n" +
    "        <tr>\n" +
    "            <th>Full Name</th>\n" +
    "            <th>Staff Number</th>\n" +
    "            <th ng-if=\"!(showClassManagement)\">Days Present</th>\n" +
    "            <th ng-if=\"!(showClassManagement)\">Days Punctual</th>\n" +
    "            <th ng-if=\"!(showClassManagement)\">Absent With Permission</th>\n" +
    "            <th ng-if=\"!(showClassManagement)\">Absent Without Permission</th>\n" +
    "            <th ng-if=\"!(showClassManagement)\">Lesson Plans</th>\n" +
    "\n" +
    "            <th ng-if=\"showClassManagement\">Class Control</th>\n" +
    "            <th ng-if=\"showClassManagement\">Children Behaviour</th>\n" +
    "            <th ng-if=\"showClassManagement\">Children Participation</th>\n" +
    "        </tr>\n" +
    "        <tr ng-hide=\"showClassManagement\" ng-repeat=\"data in teacherAttendanceData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Full Name\">     {{ data.first_name + ' ' + data.last_name }}</td>\n" +
    "            <td data-th=\"Staff No.\">     {{ data.staff_number }}</td>\n" +
    "            <td data-th=\"Days Present\">     {{ data.puntuality_and_lesson_plans.days_present }}</td>\n" +
    "            <td data-th=\"Days Punctual\">     {{ data.puntuality_and_lesson_plans.days_punctual }}</td>\n" +
    "            <td data-th=\"Absent With Permission\">     {{ data.puntuality_and_lesson_plans.days_absent_with_permission }}</td>\n" +
    "            <td data-th=\"Absent Without Permission\">     {{ data.puntuality_and_lesson_plans.days_absent_without_permission }}</td>\n" +
    "            <td data-th=\"Lesson Plans\">    {{ data.puntuality_and_lesson_plans.lesson_plans || 'Not Prepared' }}</td>\n" +
    "        </tr>\n" +
    "\n" +
    "        <tr ng-show=\"showClassManagement\" ng-repeat=\"data in classManagementData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Full Name\">     {{ data.first_name + ' ' + data.last_name }}</td>\n" +
    "            <td data-th=\"Staff No.\">     {{ data.staff_number }}</td>\n" +
    "            <td data-th=\"Class Control\">     {{ data.classroom_management.class_control }}</td>\n" +
    "            <td data-th=\"Children Behaviour\">     {{ data.classroom_management.children_behaviour }}</td>\n" +
    "            <td data-th=\"Children Participation\">     {{ data.classroom_management.children_participation }}</td>\n" +
    "\n" +
    "        </tr>\n" +
    "\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("partials/teacher_dialog_list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/teacher_dialog_list.tpl.html",
    "<md-dialog aria-label=\"Information of  {{teacherObj.first_name + ' ' + teacherObj.last_name}}  \">\n" +
    "        <md-dialog-content>\n" +
    "                <md-subheader class=\"md-sticky-no-effect\">Information of  <b>{{teacherObj.first_name + ' ' + teacherObj.last_name}}</b>  </md-subheader>\n" +
    "                <md-list>\n" +
    "                        <md-list-item>\n" +
    "<!--                                created_at: \"2015-02-12 20:46:30\"-->\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\" flex=\"33\">Gender  :   </span>\n" +
    "                                        <span flex=\"33\">    <b>{{teacherObj.gender   | uppercase }}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Category    :   </span>\n" +
    "                                        <span flex=\"33\"><b>    {{teacherObj.category}}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Highest Academic Qualification  :  </span>\n" +
    "                                        <span flex=\"33\"><b>     {{teacherObj.highest_academic_qualification}}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Highest Professional Qualification   :   </span>\n" +
    "                                        <span flex=\"33\"><b>    {{teacherObj.highest_professional_qualification}}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Class Taught   :   </span>\n" +
    "                                        <span flex=\"33\"><b>     {{teacherObj.class_taught  |  uppercase  }}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Level Taught   :   </span>\n" +
    "                                        <span flex=\"33\"><b>     {{teacherObj.level_taught   | uppercase  }}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Rank   :    </span>\n" +
    "                                        <span flex=\"33\">    <b>      {{teacherObj.rank}}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Class Subject Taught    :   </span>\n" +
    "                                        <span flex=\"33\">      <b>{{teacherObj.class_subject_taught}}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                        <md-list-item>\n" +
    "                                <p>\n" +
    "                                        <span flex=\"33\">Years Spent in the School     :   </span>\n" +
    "                                        <span flex=\"33\">     <b>{{teacherObj.years_in_the_school}}</b></span>\n" +
    "                                </p>\n" +
    "                        </md-list-item>\n" +
    "                </md-list>\n" +
    "        </md-dialog-content>\n" +
    "\n" +
    "        <div class=\"md-actions\">\n" +
    "                <md-button ng-click=\"closeDialog()\" class=\"md-primary\">\n" +
    "                        Close\n" +
    "                </md-button>\n" +
    "        </div>\n" +
    "</md-dialog>");
}]);

angular.module("partials/text_books.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("partials/text_books.tpl.html",
    "<div layout=\"row\" style=\"height: 300px; min-height: 200px\" layout-align=\"center center\" ng-hide=\"completed\">\n" +
    "    <md-progress-circular md-mode=\"determinate\" ng-value=\"loadLevel\" md-diameter=\"100\"></md-progress-circular>\n" +
    "</div>\n" +
    "<div ng-show=\"completed\">\n" +
    "    <h1 ng-bind=\"\"></h1>\n" +
    "    <h2>Textbooks</h2>\n" +
    "    <h3>School : {{ school.name }}</h3>\n" +
    "    <h3  ng-bind=\"'Term : ' + term\"></h3>\n" +
    "    <h3  ng-bind=\"'Year : ' + year\"></h3>\n" +
    "    <h3  ng-bind=\"'Submitted by : ' + data_collector_type\"></h3>\n" +
    "    <!--<md-button class=\"md-raised\" ng-click=\"showTrend = !showTrend\">-->\n" +
    "        <!--<span ng-if=\"!(showTrend)\">Show Term</span>-->\n" +
    "        <!--<span ng-if=\"showTrend\">Show Trend</span>-->\n" +
    "    <!--</md-button>-->\n" +
    "    <!--md-swipe-right=\"showClassManagement = !showClassManagement\" md-swipe-left=\"showClassManagement = !showClassManagement\"-->\n" +
    "    <table class=\"rwd-table\" ng-show=\"textbookData.length\">\n" +
    "        <tr>\n" +
    "\n" +
    "            <th>Class</th>\n" +
    "            <th style=\"text-align: center\">Ghanaian Language</th>\n" +
    "            <th style=\"text-align: center\">English</th>\n" +
    "            <th style=\"text-align: center\">Mathematics</th>\n" +
    "        </tr>\n" +
    "        <tr  ng-repeat=\"data in textbookData\" ng-click=\"displayMoreInfo(data, $event)\">\n" +
    "            <td data-th=\"Class\">     {{ data.level}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"number_of_ghanaian_language_textbooks\">     {{ data.number_of_ghanaian_language_textbooks}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"number_of_english_textbooks\">     {{ data.number_of_english_textbooks}}</td>\n" +
    "            <td style=\"text-align: center\" data-th=\"number_of_maths_textbooks\">     {{ data.number_of_maths_textbooks}}</td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td colspan=\"1\">Submitted on</td>\n" +
    "            <td colspan=\"3\">{{ submittedDate | date : 'fullDate' }}</td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "    <div ng-hide=\"textbookData.length\">\n" +
    "        <md-toolbar class=\"md-warn\">\n" +
    "            <div class=\"md-toolbar-tools\">\n" +
    "                <h2 class=\"md-flex\">No data submitted under {{term}} {{year}}</h2>\n" +
    "            </div>\n" +
    "        </md-toolbar>\n" +
    "\n" +
    "        <md-content flex layout-padding>\n" +
    "            <p>Sorry, we couldn't find any data submitted for the selected parameters.</p>\n" +
    "        </md-content>\n" +
    "    </div>\n" +
    "</div>");
}]);
