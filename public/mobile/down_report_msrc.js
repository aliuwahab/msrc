/* mSRC - v0.0.1 - 2018-11-07
 * http://msrcghana.org
 * Copyright (c) 2018 TechMerge Ltd;
 * Licensed MIT
 */
/**
 * Created by sophismay on 6/15/14.
 */

/**
 * Main Application
 *
 */

var schoolMonitor = angular.module('downloadReport',[
    'ui.router',
    'ngAnimate',
    'ngResource',
    'ui.bootstrap',
    'ui.utils',
    'toaster',
    'doowb.angular-pusher',
    //'flow',
    'multi-select',
    'angularFileUpload',
    'report_templates.app',
    'pasvaz.bindonce',
    'angular-loading-bar',
    'infinite-scroll',
    'angular-cache'
]);



schoolMonitor.config( ['$interpolateProvider', '$locationProvider','PusherServiceProvider','CacheFactoryProvider',
    function($interpolateProvider, $locationProvider, PusherServiceProvider, CacheFactoryProvider){
        //This prevents the confusion between blade templating and Angular
//    $interpolateProvider.startSymbol('<%');
//    $interpolateProvider.endSymbol('%>');
//    $locationProvider.html5Mode(true);
//    $locationProvider.hashPrefix();

        //Pusher token
        PusherServiceProvider.setToken('396cf3d207f8bb21224a');
                   // .setOptions({});

        angular.extend(CacheFactoryProvider.defaults, {
            maxAge:  7 * 24 * 60 * 60 * 1000,// Items added to this cache expire after 1week
            storageMode : 'localStorage',//memory| sessionStorage | localStorage
            storagePrefix : 'mc',
            cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
            deleteOnExpire: 'aggressive' // Items will be deleted from this cache when they expire
        });

        /* class selector to exclude/remove cells (<td> or <th>) or rows (<tr>) from the exported file(s). */
        TableExport.prototype.ignoreCSS = "tableexport-ignore";

        /* class selector to replace cells (<td> or <th>) with an empty string (i.e. "blank cell") in the exported file(s). */
        TableExport.prototype.emptyCSS = "tableexport-empty";


    }]);

schoolMonitor.run( ['$rootScope', '$location', '$state', '$stateParams',
    function ($rootScope, $location, $state, $stateParams) {
        //scUser.authenticate();


        //$rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){
//    $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){ ... })
//$rootScope.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams, error){ ... })

//$viewContentLoading - fired once the view begins loading, before the DOM is rendered. The '$scope' broadcasts the event.
//    $scope.$on('$viewContentLoading',function(event, viewConfig){

//$viewContentLoaded - fired once the view is loaded, after the DOM is rendered. The '$scope' of the view emits the event.
//    $scope.$on('$viewContentLoaded',function(event){ ... });

        $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
            $rootScope.loading = true;
        });

        $rootScope.$on('$viewContentLoaded',function(event){
            $rootScope.loading = false;
        });

        //This makes it possible to access UI-State and StateParams within the scope from the HTML partial view
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
            //if(!$rootScope.$state.includes('dashboard.school.view_selected_school') || !$rootScope.$state.includes('dashboard.view_selected_district')){
            //    //document.body.scrollTop = document.documentElement.scrollTop = 0;
            //}
        });


        //check if its on the test app or the real app
        /*true when on test server*/
        $rootScope.envCheck = !(window.location.host === "www.msrcghana.org"
            || window.location.host === "schoolmonitor.dev"
            || window.location.host === "localhost:81");


        //check if the page is the cs dashboard login page; true when on test server*/
        $rootScope.csLoginCheck =  window.location.host === "cs.msrcghana.org"
            || window.location.host === "fieldcs.msrcghana.org";
        /*|| window.location.host === "schoolmonitor.dev"*/

        /*Load all modules, just in case it hasn't been loaded when the page was hit*/
        // scGlobalService.loadAllModules();

        // Define a plugin to provide data labels
        Chart.plugins.register({
            afterDatasetsDraw: function(chart) {
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function(dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function(element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgb(0, 0, 0)';

                            var fontSize = 15;
                            var fontStyle = 'normal';
                            var fontFamily = 'Helvetica Neue';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 5;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });

    }]);

/**
 * Created by Kaygee on 18/05/2015.
 */
//School Report Card Controller (SRC)

schoolMonitor.controller('scViewSchoolStatisticsController',
    ['$scope', '$stateParams', 'School', 'scReportGenerator', 'scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool', 'scGlobalService',

        function ($scope, $stateParams, School, scReportGenerator, scNotifier,  scRegion, scDistrict, scCircuit, scSchool, scGlobalService) {

            $scope.loadSchoolStats = function () {
                if (!$scope.filter_week || !$scope.filter_term || !$scope.filter_year) {
                    scNotifier.notifyInfo('Term Parameters', 'Please select all 3 parameters to proceed');
                    return;
                }
                var dataParams = {
                    data_collector_type: "head_teacher",
                    term: $scope.filter_term,
                    week_number: $scope.filter_week,
                    year: $scope.filter_year,
                    school_id: $scope.selected_school.id
                };
                scSchool.tracker.dataParams = angular.copy(dataParams);
                scReportGenerator.loadStats(dataParams);
                $scope.loading = true;
                $scope.isNotVirgin = true;
            };

            if (scSchool.tracker.dataParams) {
                $scope.filter_week = angular.copy(scSchool.tracker.dataParams.week_number);
                $scope.filter_term = angular.copy(scSchool.tracker.dataParams.term);
                $scope.filter_year = angular.copy(scSchool.tracker.dataParams.year);
            }

            var performanceBarChartHolder = {};

            function runPupilPerformance(allSchoolPupilPerformanceData) {
                var schoolPerformanceTotals = {
                    english: {
                        class_score: {}
                    },
                    math: {
                        class_score: {}
                    },
                    gh_lang: {
                        class_score: {}
                    },
                    levels_available: {}
                };
                angular.forEach(allSchoolPupilPerformanceData, function (item, index) {

                    /*
                    * average_english_score:"50"
                        average_ghanaian_language_score:"50"
                        average_maths_score:"50"
                        data_collector_type:"head_teacher"
                        district_id:97
                        level:"KG1"
                        num_pupil_who_score_above_average_in_english:24
                        num_pupil_who_score_above_average_in_ghanaian_language:20
                        num_pupil_who_score_above_average_in_maths:24
                        term:"third_term"
                        year:"2015/2016"
                        */
                    //find the average score for the district, by subject
                    //so for each item
                    //add the averages of each subject, class by class
                    //add the class in one property under the subject

                    schoolPerformanceTotals.levels_available[item.level] = true;

                    if (isFinite(Number(item.average_english_score))) {
                        if (angular.isDefined(schoolPerformanceTotals.english.class_score[item.level])) {
                            schoolPerformanceTotals.english.class_score[item.level].average_score += Number(item.average_english_score);
                            schoolPerformanceTotals.english.class_score[item.level].above_average += Number(item.num_pupil_who_score_above_average_in_english);
                        } else {
                            schoolPerformanceTotals.english.class_score[item.level] = {
                                average_score: Number(item.average_english_score),
                                above_average: Number(item.num_pupil_who_score_above_average_in_english)
                            }
                        }
                    }


                    if (isFinite(Number(item.average_maths_score))) {
                        if (angular.isDefined(schoolPerformanceTotals.math.class_score[item.level])) {
                            schoolPerformanceTotals.math.class_score[item.level].average_score += Number(item.average_maths_score);
                            schoolPerformanceTotals.math.class_score[item.level].above_average += Number(item.num_pupil_who_score_above_average_in_maths);
                        } else {
                            schoolPerformanceTotals.math.class_score[item.level] = {
                                average_score: Number(item.average_maths_score),
                                above_average: Number(item.num_pupil_who_score_above_average_in_maths)
                            }
                        }
                    }


                    if (isFinite(Number(item.average_ghanaian_language_score))) {
                        if (angular.isDefined(schoolPerformanceTotals.gh_lang.class_score[item.level])) {
                            schoolPerformanceTotals.gh_lang.class_score[item.level].average_score += Number(item.average_ghanaian_language_score);
                            schoolPerformanceTotals.gh_lang.class_score[item.level].above_average += Number(item.num_pupil_who_score_above_average_in_ghanaian_language);
                        } else {
                            schoolPerformanceTotals.gh_lang.class_score[item.level] = {
                                average_score: Number(item.average_ghanaian_language_score),
                                above_average: Number(item.num_pupil_who_score_above_average_in_ghanaian_language)
                            }
                        }
                    }

                    performanceBarChartHolder[item.level] = {
                        gh_lang : {
                            average_score : schoolPerformanceTotals.gh_lang.class_score[item.level].average_score,
                            above_average : schoolPerformanceTotals.gh_lang.class_score[item.level].above_average
                        },
                        english : {
                            average_score : schoolPerformanceTotals.english.class_score[item.level].average_score,
                            above_average : schoolPerformanceTotals.english.class_score[item.level].above_average
                        },
                        math : {
                            average_score : schoolPerformanceTotals.math.class_score[item.level].average_score,
                            above_average : schoolPerformanceTotals.math.class_score[item.level].above_average
                        }
                    }

                    //then add for the total in another property under the subject
                    //

                });

                var performanceBarChart = {
                    label : "Pupil Performance By Class",
                    labels : [],
                    datasets : []
                };
                var langHolder = {
                    gh_lang : [],
                    gh_lang_line : [],
                    english : [],
                    english_line : [],
                    math : [],
                    math_line: []
                };

                /*get the class key*/
                /*get all the language datasets*/
                /*get the above average data for the line*/
                /*get one object with dataset title, class by class and language score per class*/

                /*get the class key*/
                angular.forEach(performanceBarChartHolder, function (perfData, classKey) {
                    performanceBarChart.labels.push(classKey);
                    /*get all the language datasets*/
                    langHolder.gh_lang.push(perfData.gh_lang.average_score);
                    langHolder.gh_lang_line.push(perfData.gh_lang.above_average);
                    langHolder.english.push(perfData.english.average_score);
                    langHolder.english_line.push(perfData.english.above_average);
                    langHolder.math.push(perfData.math.average_score);
                    langHolder.math_line.push(perfData.math.above_average);
                });

                performanceBarChart.datasets.push({
                    label: 'Ghanaian Language',
                    backgroundColor: pattern.draw('triangle-inverted', 'rgba(231, 235, 8, 0.75)'),
                    data:langHolder.gh_lang
                });

                performanceBarChart.datasets.push({
                    label: 'English',
                    backgroundColor: pattern.draw('diagonal-right-left', 'rgba(16, 204, 13, 0.75)'),
                    data:langHolder.english
                });
                performanceBarChart.datasets.push({
                    label: 'Maths',
                    backgroundColor: pattern.draw('dot-dash', 'rgba(75, 192, 192, 0.75)'),
                    data: langHolder.math
                });


                scGlobalService.drawStatisticsBarStackedChart(performanceBarChart.labels,
                    performanceBarChart.label, performanceBarChart.datasets,
                    'performanceBarChart', "horizontalBarChart");

                $scope.schoolPerformanceTotals = angular.copy(schoolPerformanceTotals);
                $scope.schoolPerformanceTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }
            if (scSchool.tracker.allSchoolPupilPerformanceData) {
                runPupilPerformance(scSchool.tracker.allSchoolPupilPerformanceData);
            }
            $scope.$on('pupilPerformanceDataLoaded', function (event, allSchoolPupilPerformanceData) {
                scSchool.tracker.allSchoolPupilPerformanceData = allSchoolPupilPerformanceData;
                console.log('allSchoolPupilPerformanceData',allSchoolPupilPerformanceData);

                runPupilPerformance(allSchoolPupilPerformanceData);
            });





            function runEnrolmentData(allSchoolPupilEnrollmentData) {

                $scope.enrollmentBarChart = {
                    label : "Pupil Enrollment By Class",
                    labels : [],
                    chartData : []
                };

                var schoolEnrollmentTotals = {
                    total_male: 0,
                    total_female: 0,
                    total_pupils: 0,
                    total_by_class: {}
                };

                angular.forEach(allSchoolPupilEnrollmentData, function (item, index) {
                    /* normal: parseInt(item.total_enrolment),
                        special: 0,
                        boys : parseInt(item.num_males),
                        special_boys : 0,
                        girls : parseInt(item.num_females),
                        special_girls : 0,
                        week_number :  item.week_number,
                        term : item.term,
                        year : item.year
                     */


                    schoolEnrollmentTotals.total_male += Number(item.num_males);
                    schoolEnrollmentTotals.total_female += Number(item.num_females);
                    schoolEnrollmentTotals.total_pupils += Number(item.total_enrolment);

                    if (angular.isDefined(schoolEnrollmentTotals.total_by_class[item.level])) {
                        schoolEnrollmentTotals.total_by_class[item.level].total_male += Number(item.num_males);
                        schoolEnrollmentTotals.total_by_class[item.level].total_female += Number(item.num_females);
                        schoolEnrollmentTotals.total_by_class[item.level].total_pupils += Number(item.total_enrolment);
                    } else {
                        schoolEnrollmentTotals.total_by_class[item.level] = {
                            total_male: Number(item.num_males),
                            total_female: Number(item.num_females),
                            total_pupils: Number(item.total_enrolment)
                        }
                    }
                });

                angular.forEach(schoolEnrollmentTotals.total_by_class, function(eachClass, prop){
                    $scope.enrollmentBarChart.labels.push(prop);
                    $scope.enrollmentBarChart.chartData.push(eachClass.total_pupils);
                });

                scGlobalService.drawStatisticsBarChart($scope.enrollmentBarChart.labels,
                    $scope.enrollmentBarChart.label, $scope.enrollmentBarChart.chartData,
                    'enrollmentBarChart', "barChart");


                $scope.schoolEnrollmentTotals = angular.copy(schoolEnrollmentTotals);
                $scope.schoolEnrollmentTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }
            if (scSchool.tracker.allSchoolPupilEnrollmentData) {
                runEnrolmentData(scSchool.tracker.allSchoolPupilEnrollmentData)
            }
            $scope.$on('pupilEnrollmentDataLoaded', function (event, allSchoolPupilEnrollmentData) {
                scSchool.tracker.allSchoolPupilEnrollmentData = allSchoolPupilEnrollmentData;
                runEnrolmentData(allSchoolPupilEnrollmentData)
            });


            function runSpecialEnrolmentData(allSchoolSpecialEnrollmentData) {
                var schoolSpecialEnrollmentTotals = {
                    total_male: 0,
                    total_female: 0,
                    total_pupils: 0,
                    total_by_class: {}
                };

                angular.forEach(allSchoolSpecialEnrollmentData, function (item, index) {
                    /*
                        level:"KG1"
                        num_females:0
                        num_males:3
                        num_of_streams:1
                        school_in_session:1
                        term:"second_term"
                        total_enrolment:3
                    */

                    schoolSpecialEnrollmentTotals.total_male += Number(item.num_males);
                    schoolSpecialEnrollmentTotals.total_female += Number(item.num_females);
                    schoolSpecialEnrollmentTotals.total_pupils += Number(item.total_enrolment);

                    if (angular.isDefined(schoolSpecialEnrollmentTotals.total_by_class[item.level])) {
                        schoolSpecialEnrollmentTotals.total_by_class[item.level].total_male += Number(item.num_males);
                        schoolSpecialEnrollmentTotals.total_by_class[item.level].total_female += Number(item.num_females);
                        schoolSpecialEnrollmentTotals.total_by_class[item.level].total_pupils += Number(item.total_enrolment);
                    } else {
                        schoolSpecialEnrollmentTotals.total_by_class[item.level] = {
                            total_male: Number(item.num_males),
                            total_female: Number(item.num_females),
                            total_pupils: Number(item.total_enrolment)
                        }
                    }
                });

                $scope.schoolSpecialEnrollmentTotals = angular.copy(schoolSpecialEnrollmentTotals);
                $scope.schoolSpecialEnrollmentTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }
            if (scSchool.tracker.specialPupilEnrollmentDataLoaded) {
                runSpecialEnrolmentData(scSchool.tracker.specialPupilEnrollmentDataLoaded)
            }
            $scope.$on('specialPupilEnrollmentDataLoaded', function (event, specialPupilEnrollmentDataLoaded) {
                scSchool.tracker.specialPupilEnrollmentDataLoaded = specialPupilEnrollmentDataLoaded;
                runSpecialEnrolmentData(specialPupilEnrollmentDataLoaded)
            });



            function runPupilAttendanceData(allPupilAttendanceDataLoaded) {
                // console.log("allPupilAttendanceDataLoaded", allPupilAttendanceDataLoaded);
                var schoolAttendanceTotals = {
                    total_school_session: 0,
                    total_attendance_for_term: 0
                };

                var tempHolder = {
                    weeks : {}
                };

                angular.forEach(allPupilAttendanceDataLoaded, function (item, index) {
                    /*
                        level:"KG1"
                        num_females:93
                        num_males:84
                        number_of_week_days:4
                        total:177
                    */
                    var concat = item.level + item.week_number + item.term + item.year;

                    // tempHolder.weeks[item.level] =

                });

                $scope.schoolAttendanceTotals = angular.copy(schoolAttendanceTotals);
                $scope.schoolAttendanceTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.allPupilAttendanceDataLoaded) {
                runPupilAttendanceData(scSchool.tracker.allPupilAttendanceDataLoaded)
            }
            $scope.$on('allPupilAttendanceDataLoaded', function (event, allPupilAttendanceDataLoaded) {
                scSchool.tracker.allPupilAttendanceDataLoaded = allPupilAttendanceDataLoaded;
                runPupilAttendanceData(allPupilAttendanceDataLoaded)
            });




            var teacherAttendancePieChart = {
                label : "Teacher Attendance Rate",
                labels : ["Present", "Absent"],
                data : []
            };
            function runTeacherPunctuality(allSchoolTeacherPupilPunctualityLessonPlansData) {
                var schoolTeacherAttendanceTotals = {
                    expectedAttendance: 0,
                    actualAttendance: 0,
                    attendanceRatio: 0,
                    total_above_50: 0,
                    num_of_exercises_given: 0,
                    num_of_exercises_marked: 0,
                    exercises_marked_average: 0,
                    total_by_teachers: {}
                };
                angular.forEach(allSchoolTeacherPupilPunctualityLessonPlansData, function (item, index) {

                    if (!isFinite(Number(item.num_of_exercises_given))) item.num_of_exercises_given = 0;
                    if (!isFinite(Number(item.num_of_exercises_marked))) item.num_of_exercises_marked = 0;
                    if (!isFinite(Number(item.days_present))) item.days_present = 0;
                    if (!isFinite(Number(item.days_in_session))) item.days_in_session = 5;


                    schoolTeacherAttendanceTotals.actualAttendance += Number(item.days_present);
                    schoolTeacherAttendanceTotals.expectedAttendance += Number(item.days_in_session);
                    if (isFinite(schoolTeacherAttendanceTotals.actualAttendance / schoolTeacherAttendanceTotals.expectedAttendance)) {
                        schoolTeacherAttendanceTotals.attendanceRatio = Math.round(100 * (schoolTeacherAttendanceTotals.actualAttendance / Number(schoolTeacherAttendanceTotals.expectedAttendance)));
                        teacherAttendancePieChart.data = [];
                        teacherAttendancePieChart.data.push(schoolTeacherAttendanceTotals.attendanceRatio);
                        teacherAttendancePieChart.data.push(100 - Number(schoolTeacherAttendanceTotals.attendanceRatio));
                    }

                    schoolTeacherAttendanceTotals.num_of_exercises_given += Number(item.num_of_exercises_given);
                    schoolTeacherAttendanceTotals.num_of_exercises_marked += Number(item.num_of_exercises_marked);
                    if (isFinite(schoolTeacherAttendanceTotals.num_of_exercises_marked / Number(schoolTeacherAttendanceTotals.num_of_exercises_given))) {
                        schoolTeacherAttendanceTotals.exercises_marked_average +=
                            Number(Math.round(100 * (schoolTeacherAttendanceTotals.num_of_exercises_marked / schoolTeacherAttendanceTotals.num_of_exercises_given)));
                    }


                    if (angular.isDefined(schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id])) {

                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].actualAttendance += Number(item.days_present);
                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].expectedAttendance += Number(item.days_in_session);
                        if (isFinite(Number(schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].actualAttendance) / schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].expectedAttendance)) {
                            schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].attendanceRatio =
                                Number(Math.round(100 * (schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].actualAttendance / schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].expectedAttendance)));
                        }

                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_given += Number(item.num_of_exercises_given);
                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_marked += Number(item.num_of_exercises_marked);

                        if (isFinite(Number(schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_marked) / Number(schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_given))) {
                            schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].exercises_marked_average +=
                                Number(Math.round(100 * (schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_marked / schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].num_of_exercises_given)))
                        }

                    } else {
                        var attendanceRatio = 0;
                        var exercisesMarkedAverage = 0;
                        if (isFinite(item.days_present / item.days_in_session)) attendanceRatio = Number(item.days_present) / Number(item.days_in_session);
                        if (isFinite(item.num_of_exercises_marked / item.num_of_exercises_given)) exercisesMarkedAverage = Number(item.num_of_exercises_marked) / Number(item.num_of_exercises_given);
                        schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id] = {
                            expectedAttendance: parseInt(item.days_in_session),
                            actualAttendance: parseInt(item.days_present),
                            attendanceRatio: Math.round(100 * attendanceRatio),
                            num_of_exercises_given: item.num_of_exercises_given,
                            num_of_exercises_marked: item.num_of_exercises_marked,
                            exercises_marked_average: Math.round(100 * (exercisesMarkedAverage))
                        };
                        if (item.teacher) {
                            schoolTeacherAttendanceTotals.total_by_teachers[item.teacher_id].name_of_teacher = (item.teacher.first_name || '') + " " + (item.teacher.last_name || '');
                        }
                    }
                });

                scGlobalService.drawPieStatisticsChart(teacherAttendancePieChart.labels, teacherAttendancePieChart.label, teacherAttendancePieChart.data, 'teacherAttendance', 'pieChartOne');

                var teacherAttendanceAboveFiftyPieChart = {
                    label: "Teacher Attendance Above 50%",
                    labels: ["Present", "Absent"],
                    data : []
                };

                /*Find the total number of schools with attendance over 50%*/
                angular.forEach(schoolTeacherAttendanceTotals.total_by_teachers, function (attendanceBySchool, index) {
                    if (attendanceBySchool.attendanceRatio > 50) {
                        schoolTeacherAttendanceTotals.total_above_50++;
                        var percentage = (schoolTeacherAttendanceTotals.total_above_50 / $scope.teachers_in_school.length) * 100;
                        teacherAttendanceAboveFiftyPieChart.data = [(percentage), (100 - percentage)]
                    }

                    if (attendanceBySchool.exercises_marked_average > schoolTeacherAttendanceTotals.exercises_marked_average) {
                        schoolTeacherAttendanceTotals.adequately_marked_schools++;
                    }

                });

                scGlobalService.drawPieStatisticsChart(teacherAttendanceAboveFiftyPieChart.labels, teacherAttendanceAboveFiftyPieChart.label, teacherAttendanceAboveFiftyPieChart.data, 'AboveFiftyPieChart', 'percentOverFiftyPie');



                $scope.schoolTeacherAttendanceTotals = angular.copy(schoolTeacherAttendanceTotals);
                $scope.schoolTeacherAttendanceTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }

            $scope.$on('teacherPunctualityLessonPlansDataLoaded', function (event, allSchoolTeacherPupilPunctualityLessonPlansData) {
                scSchool.tracker.allSchoolTeacherPupilPunctualityLessonPlansData = allSchoolTeacherPupilPunctualityLessonPlansData
                runTeacherPunctuality(allSchoolTeacherPupilPunctualityLessonPlansData);
            });


            function runTeacherUnitsCovered(allSchoolTeachersUnitsCoveredData) {
                var schoolUnitCoveredTotals = {
                    gh_lang: {
                        total : 0,
                        teachers : {}
                    },
                    english: {
                        total : 0,
                        teachers : {}
                    },
                    math: {
                        total : 0,
                        teachers : {}
                    }
                };
                angular.forEach(allSchoolTeachersUnitsCoveredData, function (item, index) {

                    if (isFinite(Number(item.ghanaian_language_units_covered))) {
                        schoolUnitCoveredTotals.gh_lang.total += Number(item.ghanaian_language_units_covered);

                        if (angular.isDefined(schoolUnitCoveredTotals.gh_lang.teachers[item.teacher_id])) {
                            schoolUnitCoveredTotals.gh_lang.teachers[item.teacher_id].gh_lang = item.ghanaian_language_units_covered
                        } else {
                            schoolUnitCoveredTotals.gh_lang.teachers[item.teacher_id] = {
                                gh_lang : item.ghanaian_language_units_covered
                            }
                        }
                    }

                    if (isFinite(Number(item.english_language_units_covered))) {
                        schoolUnitCoveredTotals.english.total += Number(item.english_language_units_covered);

                        if (angular.isDefined(schoolUnitCoveredTotals.english.teachers[item.teacher_id])) {
                            schoolUnitCoveredTotals.english.teachers[item.teacher_id].english = item.english_language_units_covered
                        } else {
                            schoolUnitCoveredTotals.english.teachers[item.teacher_id] = {
                                english : item.english_language_units_covered
                            }
                        }
                    }

                    if (isFinite(Number(item.mathematics_units_covered))) {
                        schoolUnitCoveredTotals.math.total += Number(item.mathematics_units_covered);

                        if (angular.isDefined(schoolUnitCoveredTotals.math.teachers[item.teacher_id])) {
                            schoolUnitCoveredTotals.math.teachers[item.teacher_id].math = item.mathematics_units_covered
                        } else {
                            schoolUnitCoveredTotals.math.teachers[item.teacher_id] = {
                                math : item.mathematics_units_covered
                            }
                        }
                    }
                });

                $scope.schoolUnitCoveredTotals = angular.copy(schoolUnitCoveredTotals);
                $scope.schoolUnitCoveredTotals.loaded = true;
                $scope.loading = false;
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.allSchoolTeachersUnitsCoveredData) {
                runTeacherUnitsCovered(scSchool.tracker.allSchoolTeachersUnitsCoveredData);
            }
            $scope.$on('teacherUnitsCoveredDataLoaded', function (event, allSchoolTeachersUnitsCoveredData) {
                scSchool.tracker.allSchoolTeachersUnitsCoveredData = allSchoolTeachersUnitsCoveredData;
                runTeacherUnitsCovered(allSchoolTeachersUnitsCoveredData);
            });



            function runRecordBooks(allSchoolRecordBooksData) {
                var schoolRecordBooks = {
                    admission_register: {
                        name: "Admission Register",
                        total: 0
                    },
                    class_registers: {
                        name: "Class Registers",
                        total: 0
                    },
                    teacher_attendance_register: {
                        name: "Teacher Attendance Register",
                        total: 0
                    },
                    visitors: {
                        name: "Visitors\' Book",
                        total: 0
                    },
                    log: {
                        name: "Log Book",
                        total: 0
                    },
                    sba: {
                        name: "SBA",
                        total: 0
                    },
                    movement: {
                        name: "Movement Book",
                        total: 0
                    },
                    spip: {
                        name: "SPIP",
                        total: 0
                    },
                    inventory_books: {
                        name: "Inventory Books",
                        total: 0
                    },
                    cummulative_records_books: {
                        name: "Cumulative Record Books",
                        total: 0
                    },
                    continous_assessment: {
                        name: "Continuous Assessment / SMB",
                        total: 0
                    },
                    books_available: []
                };
                angular.forEach(allSchoolRecordBooksData, function (item, index) {

                    angular.forEach(schoolRecordBooks, function (itemValue, itemProp) {

                        if (item[itemProp] === 'yes') {
                            schoolRecordBooks[itemProp].total++;
                            schoolRecordBooks.books_available.push(schoolRecordBooks[itemProp].name)
                        }
                    })
                });
                $scope.schoolRecordBooks = angular.copy(schoolRecordBooks);

                $scope.books = [];
                angular.forEach(allSchoolRecordBooksData, function(item,index){

                    var booksHolder =[];


                    booksHolder.push({
                        name: "Admission Register",
                        term :   item.term,
                        status : scGlobalService.makeFirstLetterCapital(item.admission_register),
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Class Registers",
                        term :  item.term,
                        status : scGlobalService.makeFirstLetterCapital(item.class_registers),
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Teacher Attendance Register",
                        status: scGlobalService.makeFirstLetterCapital(item.teacher_attendance_register),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Visitors\' Book",
                        status: scGlobalService.makeFirstLetterCapital(item.visitors),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Log Book",
                        status: scGlobalService.makeFirstLetterCapital(item.log),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "SBA",
                        status: scGlobalService.makeFirstLetterCapital(item.sba),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Movement Book",
                        status: scGlobalService.makeFirstLetterCapital(item.movement),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "SPIP",
                        status: scGlobalService.makeFirstLetterCapital(item.spip),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Inventory Books",
                        status: scGlobalService.makeFirstLetterCapital(item.inventory_books),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    booksHolder.push({
                        name: "Cumulative Record Books",
                        status: scGlobalService.makeFirstLetterCapital(item.cummulative_records_books),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });

                    booksHolder.push({
                        name: "Continuous Assessment / SMB",
                        status: scGlobalService.makeFirstLetterCapital(item.continous_assessment || 'no'),
                        term : item.term,
                        year : item.year,
                        data_collector_type : item.data_collector_type
                    });
                    $scope.books = $scope.books.concat(booksHolder);

                });

                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.allSchoolRecordBooksData) {
                runRecordBooks(scSchool.tracker.allSchoolRecordBooksData)
            }

            $scope.$on('recordBooksDataLoaded', function (event, allSchoolRecordBooksData) {
                scSchool.tracker.allSchoolRecordBooksData = allSchoolRecordBooksData;
                runRecordBooks(allSchoolRecordBooksData);
            });

            function runGrantCapitation(districtGrantCapitationPaymentsData) {
                var schoolGrantStuff = {
                    totalAmount: 0,
                    total_first: 0,
                    total_second: 0,
                    total_third: 0
                };

                angular.forEach(districtGrantCapitationPaymentsData, function (item, index) {
                    if (angular.isDefined(item.first_tranche_amount) && item.first_tranche_amount > 0) {
                        schoolGrantStuff.totalAmount += parseInt(item.first_tranche_amount);
                        schoolGrantStuff.total_first += parseInt(item.first_tranche_amount);
                    }
                    if (angular.isDefined(item.second_tranche_amount) && item.second_tranche_amount > 0) {
                        schoolGrantStuff.totalAmount += parseInt(item.second_tranche_amount);
                        schoolGrantStuff.total_second += parseInt(item.second_tranche_amount);
                    }
                    if (angular.isDefined(item.third_tranche_amount) && item.third_tranche_amount > 0) {
                        schoolGrantStuff.totalAmount += parseInt(item.third_tranche_amount);
                        schoolGrantStuff.total_third += parseInt(item.third_tranche_amount);
                    }


                    /*
                        grant.first_tranche_date
                        grant.first_tranche_amount
                        grant.second_tranche_date
                        grant.second_tranche_amount
                        grant.third_tranche_date
                        grant.third_tranche_amount
                    */
                });


                $scope.schoolGrantStuff = angular.copy(schoolGrantStuff);
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.districtGrantCapitationPaymentsData) {
                runGrantCapitation(scSchool.tracker.districtGrantCapitationPaymentsData)
            }

            $scope.$on('grantCapitationPaymentsDataLoaded', function (event, districtGrantCapitationPaymentsData) {
                scSchool.tracker.districtGrantCapitationPaymentsData = districtGrantCapitationPaymentsData;
                runGrantCapitation(districtGrantCapitationPaymentsData);
            });

            function runSupport(districtSupportData) {
                var schoolSupportStuff = {
                    donor_support: {
                        name: "Donor Support",
                        cash: 0,
                        kind: ''
                    },
                    community_support: {
                        name: "Community Support",
                        cash: 0,
                        kind: ''
                    },
                    district_support: {
                        name: "School Support",
                        cash: 0,
                        kind: ''
                    },
                    pta_support: {
                        name: "PTA Support",
                        cash: 0,
                        kind: ''
                    },
                    other_support: {
                        name: "Other",
                        cash: 0,
                        kind: ''
                    },
                    total_cash: 0,
                    received_support: false
                };
                /*
                name: "Donor Support",
                in_cash : item.donor_support_in_cash,
                in_kind : item.donor_support_in_kind,

                name: "Community Support",
                in_cash : item.community_support_in_cash,
                in_kind : item.community_support_in_kind,

                name: "District Support",
                in_cash : item.district_support_in_cash,
                in_kind : item.district_support_in_kind,

                name: "PTA Support",
                in_cash : item.pta_support_in_cash,
                in_kind : item.pta_support_in_kind,
                comment : item.comment


                name: "Other",
                in_cash : item.other_support_in_cash,
                in_kind : item.other_support_in_kind,

            });*/
                angular.forEach(districtSupportData, function (item, index) {

                    angular.forEach(schoolSupportStuff, function (itemVal, itemProp) {

                        schoolSupportStuff.total_cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                       if(!schoolSupportStuff.received_support) schoolSupportStuff.received_support = angular.isDefined(item[itemProp + '_in_kind']);
                        schoolSupportStuff[itemProp].cash += isFinite(Number(item[itemProp + '_in_cash'])) ? Number(item[itemProp + '_in_cash']) : 0;
                        schoolSupportStuff[itemProp].kind = angular.isDefined(item[itemProp + '_in_kind']);
                    });

                });
                $scope.schoolSupportStuff = angular.copy(schoolSupportStuff);
                $scope.isNotVirgin = true;
            }

            if (scSchool.tracker.districtSupportData) {
                runSupport(scSchool.tracker.districtSupportData);
            }

            $scope.$on('supportTypesDataLoaded', function (event, districtSupportData) {
                scSchool.tracker.districtSupportData = districtSupportData;
                runSupport(districtSupportData);
            });

            function runFurniture(districtSchoolFurnitureItems) {
                var schoolFurniture = {
                    total_adequates: []
                };

                var adequacyElems = ['pupils_furniture', 'teacher_tables', 'teacher_chairs', 'classrooms_cupboard'];

                angular.forEach(districtSchoolFurnitureItems, function (item, index) {
                    for (var i = 0; i < adequacyElems.length; i++) {
                        var adequateProp = adequacyElems[i];

                        if (item[adequateProp] === 'adequate') {
                            schoolFurniture.total_adequates.push(adequateProp);
                        }

                    }

                });
                $scope.schoolFurniture = angular.copy(schoolFurniture);

                $scope.furnitureItems = [];
                angular.forEach(districtSchoolFurnitureItems, function(furnitureObject , index){

                    var furnitureHolder =[];

                    furnitureHolder.push({
                        name: "Pupils Furniture",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.pupils_furniture],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Teacher Tables",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.teacher_tables],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Teacher Chairs",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.teacher_chairs],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });
                    furnitureHolder.push({
                        name: "Classrooms Cupboard",
                        term :   furnitureObject.term,
                        status : $scope.formatAdequecy[furnitureObject.classrooms_cupboard],
                        year : furnitureObject.year,
                        comment : furnitureObject.comment,
                        data_collector_type : furnitureObject.data_collector_type
                    });

                    $scope.furnitureItems = $scope.furnitureItems.concat(furnitureHolder);

                });

            }

            if (scSchool.tracker.districtSchoolFurnitureItems) {
                runFurniture(scSchool.tracker.districtSchoolFurnitureItems);
            }
            $scope.$on('furnitureDataLoaded', function (event, districtSchoolFurnitureItems) {
                scSchool.tracker.districtSchoolFurnitureItems = districtSchoolFurnitureItems;
                runFurniture(districtSchoolFurnitureItems);
            });

            function runStructure(districtSchoolStructureItems) {
                var schoolStructure = {
                    total_goods: []
                };
                var goodElems = ['walls', 'doors', 'windows', 'floors', 'blackboard', 'illumination'];

                angular.forEach(districtSchoolStructureItems, function (item, index) {
                    for (var i = 0; i < goodElems.length; i++) {
                        var statusProp = goodElems[i];

                        if (item[statusProp] === 'good') {
                            schoolStructure.total_goods.push(statusProp);
                        }
                    }
                });

                $scope.schoolStructure = angular.copy(schoolStructure);

                $scope.structureItems = [];
                angular.forEach(districtSchoolStructureItems, function(structureObject , index){

                    var structureHolder =[];

                    structureHolder.push({
                        name: "Walls",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.walls],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Doors",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.doors],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Windows",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.windows],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Floors",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.floors],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    structureHolder.push({
                        name: "Blackboard",
                        term :   structureObject.term,
                        status : $scope.formatGoodPoorFair[structureObject.blackboard],
                        year : structureObject.year,
                        comment : structureObject.comment,
                        data_collector_type : structureObject.data_collector_type
                    });
                    try{
                        structureHolder.push({
                            name: "Illumination",
                            term :   structureObject.term,
                            status : $scope.formatGoodPoorFair[structureObject.illumination],
                            year : structureObject.year,
                            comment : structureObject.comment,
                            data_collector_type : structureObject.data_collector_type
                        });
                    }catch(e){
                        console.log(e);
                        structureHolder.push({
                            name: "Illumination",
                            term :   structureObject.term,
                            status : ' ',
                            year : structureObject.year,
                            comment : structureObject.comment,
                            data_collector_type : structureObject.data_collector_type
                        });
                    }


                    $scope.structureItems = $scope.structureItems.concat(structureHolder);


                })
            }

            if (scSchool.tracker.districtSchoolStructureItems) {
                runStructure(scSchool.tracker.districtSchoolStructureItems)
            }
            $scope.$on('structureDataLoaded', function (event, districtSchoolStructureItems) {
                scSchool.tracker.districtSchoolStructureItems = districtSchoolStructureItems;
                runStructure(districtSchoolStructureItems);
            });

            function runSanitation(districtSanitationItems) {
                var schoolSanitation = {
                    total_availables: []
                };
                /*
                *   sanitationHolder.push({
               name: "Toilet",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.toilet],
               year :sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Urinal",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.urinal],
               year :sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Water",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.water],
               year :sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Dust Bins",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[sanitationObject.dust_bins],
               year : sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
           sanitationHolder.push({
               name: "Veronica Buckets",
               term :   sanitationObject.term,
               status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
               year :  sanitationObject.year,
               comment : sanitationObject.comment,
               data_collector_type : sanitationObject.data_collector_type
           });
                * */

                var availabilityItems = ['toilet', 'urinal', 'water', 'dust_bins', 'veronica_buckets'];

                angular.forEach(districtSanitationItems, function (item, index) {

                    for (var i = 0; i < availabilityItems.length; i++) {
                        var statusProp = availabilityItems[i];

                        if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                            schoolSanitation.total_availables.push(statusProp)
                        }
                    }
                });

                $scope.schoolSanitation = angular.copy(schoolSanitation);

                $scope.sanitationItems = [];
                angular.forEach(districtSanitationItems, function(sanitationObject, index){

                    //An empty object that gets initialized every time the loop runs.
                    //It is used to hold the items in object being looped over for the purpose of display
                    var sanitationHolder =[];

                    //reformatting the way the objects are coming from the server tp be used in the table for display
                    sanitationHolder.push({
                        name: "Toilet",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.toilet],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Urinal",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.urinal],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Water",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.water],
                        year :sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Dust Bins",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[sanitationObject.dust_bins],
                        year : sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });
                    sanitationHolder.push({
                        name: "Veronica Buckets",
                        term :   sanitationObject.term,
                        status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
                        year :  sanitationObject.year,
                        comment : sanitationObject.comment,
                        data_collector_type : sanitationObject.data_collector_type
                    });


                    //Merge the contents of the temp object to the scope variable that will display in the view
                    $scope.sanitationItems = $scope.sanitationItems.concat(sanitationHolder);
                });

            }

            if (scSchool.tracker.districtSanitationItems) {
                runSanitation(scSchool.tracker.districtSanitationItems)
            }
            $scope.$on('sanitationDataLoaded', function (event, districtSanitationItems) {
                scSchool.tracker.districtSanitationItems = districtSanitationItems;
                runSanitation(districtSanitationItems);
            });




            function runRecreation(districtRecreationItems) {
                var schoolRecreation = {
                    total_availables: []
                };
                /*
                *   recreationHolder.push({
               name: "Football",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.football],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Volleyball",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.volleyball],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Netball",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.netball],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Playing Field",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.playing_field],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           recreationHolder.push({
               name: "Sports Wear",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.sports_wear],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });
           try{
               recreationHolder.push({
                   name: "Seesaw",
                   term :   recreationObject.term,
                   status : $scope.formatAvailability[recreationObject.seesaw],
                   year : recreationObject.year,
                   comment : recreationObject.comment,
                   data_collector_type : recreationObject.data_collector_type
               })
               recreationHolder.push({
                   name: "Merry-Go-Round",
                   term :   recreationObject.term,
                   status : $scope.formatAvailability[recreationObject.merry_go_round],
                   year : recreationObject.year,
                   comment : recreationObject.comment,
                   data_collector_type : recreationObject.data_collector_type
               });
           recreationHolder.push({
               name: "First Aid Box",
               term :   recreationObject.term,
               status : $scope.formatAvailability[recreationObject.first_aid_box],
               year : recreationObject.year,
               comment : recreationObject.comment,
               data_collector_type : recreationObject.data_collector_type
           });

                * */

                var availabilityItems = ['football', 'volleyball', 'netball', 'playing_field', 'sports_wear', 'seesaw', 'merry_go_round', 'first_aid_box'];

                angular.forEach(districtRecreationItems, function (item, index) {


                    for (var i = 0; i < availabilityItems.length; i++) {
                        var statusProp = availabilityItems[i];

                        if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                            schoolRecreation.total_availables.push(statusProp);
                        }
                    }
                });

                $scope.schoolRecreation = angular.copy(schoolRecreation);

                $scope.recreationItems = [];
                angular.forEach(districtRecreationItems, function(recreationObject,index){

                    var recreationHolder =[];
                    recreationHolder.push({
                        name: "Football",
                        term :   recreationObject.term,
                        formatted_term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.football],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Volleyball",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.volleyball],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Netball",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.netball],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Playing Field",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.playing_field],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    recreationHolder.push({
                        name: "Sports Wear",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.sports_wear],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });
                    try{
                        recreationHolder.push({
                            name: "Seesaw",
                            term :   recreationObject.term,
                            status : $scope.formatAvailability[recreationObject.seesaw],
                            year : recreationObject.year,
                            comment : recreationObject.comment,
                            data_collector_type : recreationObject.data_collector_type
                        })
                    }catch(e){

                    }

                    try{
                        recreationHolder.push({
                            name: "Merry-Go-Round",
                            term :   recreationObject.term,
                            status : $scope.formatAvailability[recreationObject.merry_go_round],
                            year : recreationObject.year,
                            comment : recreationObject.comment,
                            data_collector_type : recreationObject.data_collector_type
                        });
                    }catch(e){

                    }


                    recreationHolder.push({
                        name: "First Aid Box",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.first_aid_box],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });

                    $scope.recreationItems = $scope.recreationItems.concat(recreationHolder);

                });

            }

            $scope.$on('recreationDataLoaded', function (event, districtRecreationItems) {
                scSchool.tracker.districtRecreationItems = districtRecreationItems;
                runRecreation(districtRecreationItems);
            });

            function runSecurity(districtSecurityItems) {
                var schoolSecurity = {
                    total_availables: []
                };
                var availabilityItems = ['security_man', 'lights', 'gated', 'walled'];

                angular.forEach(districtSecurityItems, function (item, index) {

                    for (var i = 0; i < availabilityItems.length; i++) {
                        var statusProp = availabilityItems[i];

                        if (item[statusProp] === 'available_functional' || item[statusProp] === 'available') {
                            schoolSecurity.total_availables.push(statusProp);
                        }
                    }
                });

                $scope.schoolSecurity = angular.copy(schoolSecurity);

                $scope.securityItems = [];
                angular.forEach(districtSecurityItems, function(securityObject , index){

                    var securityHolder =[];
//                var format_term = $scope.academicTermDisplay(securityObject.term);
                    securityHolder.push({
                        name: "Walled / Fenced",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.walled],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });

                    securityHolder.push({
                        name: "Gated",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.gated],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });
                    securityHolder.push({
                        name: "Lights",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.lights],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });
                    securityHolder.push({
                        name: "Security Man",
                        term :   securityObject.term,
                        status : $scope.formatAvailability[securityObject.security_man],
                        year : securityObject.year,
                        comment : securityObject.comment,
                        data_collector_type : securityObject.data_collector_type
                    });

                    $scope.securityItems = $scope.securityItems.concat(securityHolder);

                });
            }

            $scope.$on('securityDataLoaded', function (event, districtSecurityItems) {
                runSecurity(districtSecurityItems);
            });

            function runMeetings(districtSchoolMeetingItems) {
                var schoolMeeting = {
                    total_frequency: 0
                };
                /*
                *
           meetingsHolder.push({
               name: "Staff Meeting",
               frequency: meetingObject.number_of_staff_meeting,
               males_present: meetingObject.num_males_present_at_staff_meeting,
               females_present: meetingObject.num_females_present_at_staff_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

           meetingsHolder.push({
               name: "SPAM",
               frequency: meetingObject.number_of_spam_meeting,
               males_present: meetingObject.num_males_present_at_spam_meeting,
               females_present: meetingObject.num_females_present_at_spam_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

           meetingsHolder.push({
               name: "PTA",
               frequency: meetingObject.number_of_pta_meeting,
               males_present: meetingObject.num_males_present_at_pta_meeting,
               females_present: meetingObject.num_females_present_at_pta_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

           meetingsHolder.push({
               name: "SMC",
               frequency: meetingObject.number_of_smc_meeting,
               males_present: meetingObject.num_males_present_at_smc_meeting,
               females_present: meetingObject.num_females_present_at_smc_meeting,
               week :   meetingObject.week_number,
               term :   meetingObject.term,
               year : meetingObject.year,
               data_collector_type : meetingObject.data_collector_type,
               comment : meetingObject.comment
           });

                * */
                var frequencyItems = ['number_of_smc_meeting', 'number_of_pta_meeting', 'number_of_spam_meeting', 'number_of_staff_meeting'];

                angular.forEach(districtSchoolMeetingItems, function (item, index) {

                    for (var i = 0; i < frequencyItems.length; i++) {
                        var freqProp = frequencyItems[i];

                        if (isFinite(Number(item[freqProp])) && Number(item[freqProp]) > 0) {
                            schoolMeeting.total_frequency += Number(item[freqProp]);
                        }
                    }
                });

                $scope.schoolMeeting = angular.copy(schoolMeeting);
            }

            if (scSchool.tracker.districtSchoolMeetingItems) {
                runMeetings(scSchool.tracker.districtSchoolMeetingItems)
            }
            $scope.$on('meetingDataLoaded', function (event, districtSchoolMeetingItems) {
                scSchool.tracker.districtSchoolMeetingItems = districtSchoolMeetingItems;
                runMeetings(districtSchoolMeetingItems);
            });

            function runCommunity(districtCommunityItems) {
                var schoolCommInvolvement = {
                    total_goods: []
                };

                /*
                *       var format_term = $scope.academicTermDisplay(communityObject.term);
            communityHolder.push({
                name: "PTA Involvement",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.pta_involvement],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });
            communityHolder.push({
                name: "Parents Notified of Student Progress",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.parents_notified_of_student_progress],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });
            communityHolder.push({
                name: "Community Sensitization on School Attendance",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.community_sensitization_for_school_attendance],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });
            communityHolder.push({
                name: "School Management Committees",
                term :   communityObject.term,
                status : $scope.formatGoodPoorFair[communityObject.school_management_committees],
                year : communityObject.year,
                data_collector_type : communityObject.data_collector_type
            });

                * */
                var goodElems = ['school_management_committees', 'community_sensitization_for_school_attendance', 'parents_notified_of_student_progress', 'pta_involvement'];

                angular.forEach(districtCommunityItems, function (item, index) {
                    for (var i = 0; i < goodElems.length; i++) {
                        var statusProp = goodElems[i];

                        if (item[statusProp] === 'good') {
                            schoolCommInvolvement.total_goods.push(statusProp);
                        }
                    }
                });

                $scope.schoolCommInvolvement = angular.copy(schoolCommInvolvement);
            }

            if (scSchool.tracker.districtCommunityItems) {
                runCommunity(scSchool.tracker.districtCommunityItems)
            }
            $scope.$on('communityDataLoaded', function (event, districtCommunityItems) {
                scSchool.tracker.districtCommunityItems = districtCommunityItems;
                runCommunity(districtCommunityItems);


                $scope.chart = AmCharts.makeChart( "attendanceChartDiv", {
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": [ {
                        "country": "USA",
                        "visits": 2025
                    }, {
                        "country": "China",
                        "visits": 1882
                    }, {
                        "country": "Japan",
                        "visits": 1809
                    }, {
                        "country": "Germany",
                        "visits": 1322
                    }, {
                        "country": "UK",
                        "visits": 1122
                    }, {
                        "country": "France",
                        "visits": 1114
                    }, {
                        "country": "India",
                        "visits": 984
                    }, {
                        "country": "Spain",
                        "visits": 711
                    }, {
                        "country": "Netherlands",
                        "visits": 665
                    }, {
                        "country": "Russia",
                        "visits": 580
                    }, {
                        "country": "South Korea",
                        "visits": 443
                    }, {
                        "country": "Canada",
                        "visits": 441
                    }, {
                        "country": "Brazil",
                        "visits": 395
                    } ],
                    "valueAxes": [ {
                        "gridColor": "#FFFFFF",
                        "gridAlpha": 0.2,
                        "dashLength": 0
                    } ],
                    "gridAboveGraphs": true,
                    "startDuration": 1,
                    "graphs": [ {
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "visits"
                    } ],
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "country",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridAlpha": 0,
                        "tickPosition": "start",
                        "tickLength": 20
                    },
                    "export": {
                        "enabled": true,
                        "menu": [{
                            "format": "PNG",
                            "class": "export-main",
                            "multiplier": 2
                        }]
                    }

                } );


                var chart = AmCharts.makeChart( "pieChartDiv", {
                    "type": "pie",
                    "theme": "light",
                    "dataProvider": [ {
                        "country": "Lithuania",
                        "value": 260
                    }, {
                        "country": "Ireland",
                        "value": 201
                    }, {
                        "country": "Germany",
                        "value": 65
                    }, {
                        "country": "Australia",
                        "value": 39
                    }, {
                        "country": "UK",
                        "value": 19
                    }, {
                        "country": "Latvia",
                        "value": 10
                    } ],
                    "valueField": "value",
                    "titleField": "country",
                    "outlineAlpha": 0.4,
                    "depth3D": 15,
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                    "angle": 30,
                    "export": {
                        "enabled": true
                    }
                } );
            });





            $scope.$watchGroup(['teachers_in_school', 'schoolEnrollmentTotals.total_pupils'], function(newVal, oldVal) {
                if (angular.isDefined(newVal[0]) && newVal[0].length > 0) {

                    if (angular.isDefined(newVal[1]) && newVal[1] > 0) {

                        $scope.pupilTeacherRatio = Math.round((newVal[1]) / (newVal[0].length))
                    }

                }
            });



            $scope.downloadPDF = function (){
                var pdfDocVariables;
                // pdfDocVariables = localStorage.getItem('pdfDocVariables');

                if (!pdfDocVariables) {
                    pdfDocVariables = {
                        title: 'mSRC School Report',
                        entity_name: $scope.selected_school.name + " (School)",
                        entity : $scope.selected_school,
                        upper_level_name: $scope.selected_school.districtName + " District",
                        term_in_scope: $scope.one_school_terms[$scope.filter_term].label,
                        // top_box_left : 'Number of Schools : ' + + $scope.selected_district.totalSchools,
                        top_box_left: '' + $scope.schoolEnrollmentTotals.total_pupils,
                        top_box_middle: '',
                        top_box_up_middle_up: '' +$scope.schoolEnrollmentTotals.total_male,
                        top_box_up_middle_down:  '' + $scope.schoolEnrollmentTotals.total_female,
                        top_box_right: 'Number of Teachers : ' + $scope.teachers_in_school.length,
                        top_box_down_left:  $scope.schoolTeacherAttendanceTotals.total_above_50 * 3 + "%",
                        top_box_down_middle_up: '' + $scope.schoolSpecialEnrollmentTotals.total_male || 0,
                        top_box_down_middle_down:  '' + $scope.schoolSpecialEnrollmentTotals.total_female || 0,
                        percentage_trained_teachers: $scope.teachers_in_school.trained + '/' + $scope.teachers_in_school.length,
                        gh_lang_performance_array: $scope.schoolPerformanceTotals.gh_lang.class_score,
                        english_lang_performance_array: $scope.schoolPerformanceTotals.english.class_score,
                        maths_lang_performance_array: $scope.schoolPerformanceTotals.math.class_score,
                        teacher_pupil_ratio: ( $scope.pupilTeacherRatio + "  :  1") || '-',
                        teacher_attendance_ratio: ($scope.schoolTeacherAttendanceTotals.attendanceRatio + '%') || 0,
                        teacher_unit_covered_gh_lang: ($scope.schoolUnitCoveredTotals.gh_lang.total + '') || 0,
                        teacher_unit_covered_english: ($scope.schoolUnitCoveredTotals.english.total + '') || 0,
                        teacher_unit_covered_maths: ($scope.schoolUnitCoveredTotals.math.total + '') || 0,
                        punctuality_title: 'Teachers with',
                        teacher_punctuality: ($scope.schoolTeacherAttendanceTotals.total_above_50 + '/' + $scope.teachers_in_school.length) || 0,
                        adequate_marked_title: "Exercises",
                        adequately_marked_exercises: ($scope.schoolTeacherAttendanceTotals.num_of_exercises_marked || '0') + '/' + ($scope.schoolTeacherAttendanceTotals.num_of_exercises_given || '0'),
                        schools_with_or_number_of_title: "Number of",
                        schools_that_or_amount_of_title: "Amount of",
                        schools_that_received_or_any_type_of_support: "Any type of",

                        record_books: ($scope.schoolRecordBooks.books_available.length + '/11' ),
                        books : $scope.books,

                        grant_received: ("GHS " + $scope.schoolGrantStuff.totalAmount + '') || 0,
                        /*todo put kind here*/
                        support_received: ("GHS " + $scope.schoolSupportStuff.total_cash + '') || 0,

                        adequate_furniture: ($scope.schoolFurniture.total_adequates.length + '/4') || 0,
                        furnitureItems : $scope.furnitureItems,

                        meetings_title: 'Number of ',
                        community_title: "    Active Community Participation",

                        good_structure: ($scope.schoolStructure.total_goods.length + '/6') || 0,
                        structureItems : $scope.structureItems,

                        sanitation_facilities: ($scope.schoolSanitation.total_availables.length + '/5') || 0,
                        sanitationItems : $scope.sanitationItems,

                        recreational_facilities: ($scope.schoolRecreation.total_availables.length + '/8') || 0,
                        recreationItems: $scope.recreationItems,

                        security_facilities: ($scope.schoolSecurity.total_availables.length + '/4') || 0,
                        securityItems : $scope.securityItems,

                        meetings: ($scope.schoolMeeting.total_frequency + '') || 0,
                        community_participation: ($scope.schoolCommInvolvement.total_goods.length + '/4') || 0,
                        pdf_export_name: $scope.selected_school.name + '_' + $scope.filter_term + '_school_report.pdf'
                    };
                }

                // localStorage.setItem('pdfDocVariables', pdfDocVariables);

                scReportGenerator.downloadPDF(pdfDocVariables)
            };
        }
    ]);
/**
 * Created by KayGee on 17/06/14.
 */

schoolMonitor.controller('scGlobalController', ['$rootScope','$scope','$http','$interval',
    'app_name_short', 'scUser','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','Pusher',
    '$state','scCircuitSupervisor', 'scHeadTeacher', 'scGlobalService',
    //'regions', 'districts', 'circuits', 'schools', 'allHeadteachers', 'allSupervisors','$state',

    function ($rootScope, $scope, $http, $interval,
              app_name_short,scUser, scNotifier, scRegion, scDistrict, scCircuit,
              scSchool, Pusher, $state, scCircuitSupervisor, scHeadTeacher, scGlobalService) {

        //This allows us to display the name programatically since the global scope can't reach that side of the dashboard
        $rootScope.appName = app_name_short;



        //This function makes the first letter of a word capital.
        //Its primarily used to make the admin role type a capital letter,
        //If this function runs without an argument, it means the session for the administrator timed out so
        //a redirect is done to the login page
        function makeFirstLetterCapital(word){
            if (word) {
                return word.charAt(0).toUpperCase() + word.substring(1);
            }else{
                $rootScope.logOut();
                if (angular.isArray()) {
                }

            }
        }

        if (!$rootScope.callOnce) {
            /*these are here so they run only once*/

            //Re initialize the side bar tree since it may not be showing when the login screen first shows
            $(".treeview").tree();


            //Use Jquery to replace the name on the dashboard so the
            //curly braces don't show on slow internet
            $('.hide_until_angular-loads').css('display', 'block');
            $rootScope.currentUser = scUser.currentUser;
            // $rootScope.currentUser.level = makeFirstLetterCapital( $rootScope.currentUser.level);

            $rootScope.callOnce = true;
        }


        if (scGlobalService.getLoadingCounter() >= 6) {
            scGlobalService.initModules($scope);
        }

        $rootScope.$on('updateLoadedModelsFromRefresh',function(evt, data){
            if (data >= 6) {
                scGlobalService.initModules($scope);
            }
        });


        //This check is to hide certain feature from the general admin until approved by App Owners,
        //eg is allowing multiple submission
        // $rootScope.keepSecret = !!(window.location.host != "www.msrcghana.org" ||
        // $rootScope.currentUser.email == 'francis@techmerge.org');

//
//        <!--Key for Coloring the progress bars
//        <!--Key for Coloring the progress bars
//                     0 - 2 = red
//                     3 - 4 = warning
//                     5 - 6 = yellow
//                     7 - 8 = aqua
//                     9 - 10 = green
//                     -->


        $scope.colourProgressBar = function(value){
            return value < 1 ?  'danger' :
                value < 3 ?  'danger' :
                    value < 5 ? 'warning' :
                        value < 7 ? 'info' :
                            value < 9 ? 'primary' : 'success'
        };

        $scope.schoolColourProgressBar = function(value){
            return value < 1 ?  'danger' :
                value < 3 ?  'danger' :
                    value < 5 ? 'warning' :
                        value < 7 ? 'info' :
                            value < 9 ? 'primary' : 'success'
        };

        $scope.labelStatusColor = function(status){

            return status < 1 ? 'label-default' :
                status < 20 ? 'label-danger' :
                    status < 40 ? 'label-warning' :
                        status < 60 ? 'label-info' :
                            status < 80 ? 'label-primary' : 'label-success';
        };

        $scope.labelStatusText = function(status){

            return status < 1 ? 'no data' :
                status < 10 ? 'very bad' :
                    status < 20 ? 'bad' :
                        status < 30 ? 'very poor' :
                            status < 40 ? 'poor' :
                                status < 50 ? 'fair' :
                                    status < 60 ? 'average' :
                                        status < 70 ? 'good' :
                                            status < 80 ? 'very good' :
                                                status < 90 ? 'healthy' : 'excellent';
        };

        Pusher.subscribe('dataChannel', 'dataSubmitted', function (item) {
            //When new data is submitted, get the section type submitted and assign
            // sectionToGo variable and sectionUnderText variable

            var sectionToGo, sectionUnderText;

            if (item['0'] == 'schoolreportcard' ) {
                sectionToGo = 'dashboard.school.view_selected_school.report_card';
                sectionUnderText = 'School Report Card';
            }else if (  item['0'] == 'schoolreview') {
                sectionToGo = 'dashboard.school.view_selected_school.review';
                sectionUnderText = 'School Review';
            }else{
                sectionToGo = 'dashboard.school.view_selected_school.school_visit';
                sectionUnderText = 'School Visit';
            }


            //Check current admin login level and do pop toaster with click function
            if (scUser.currentUser.level == "National") {
                scNotifier.notifyInfoWithCallback(sectionUnderText,
                    'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                    function(){ $state.go(sectionToGo, {
                        id : item.school_id,
                        district_id : scSchool.schoolLookup[item.school_id].district_id });
                    });
            }else if (scUser.currentUser.level == "Regional" ) {
                if (scUser.currentUser.level_id == scSchool.schoolLookup[item.school_id].region_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else if (scUser.currentUser.level == "District" ) {
                if (scUser.currentUser.level_id == scSchool.schoolLookup[item.school_id].district_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else if (scUser.currentUser.level == "Circuit" ) {
                if (scUser.currentUser.level_id == scSchool.schoolLookup[item.school_id].circuit_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else if (scUser.currentUser.level == "School" ) {
                if (scUser.currentUser.level_id ==item.school_id ) {
                    scNotifier.notifyInfoWithCallback(sectionUnderText,
                        'New data submitted for ' + scSchool.schoolLookup[item.school_id].name + '. Click to view.' ,
                        function(){ $state.go(sectionToGo, {
                            id : item.school_id,
                            district_id : scSchool.schoolLookup[item.school_id].district_id });
                        });
                }
            }else{
                scNotifier.notifyInfo(sectionUnderText, 'New data submitted for ' + scSchool.schoolLookup[item.school_id].name);
            }
        });

        //This function helps to change the texts on the dashboard to the selected view
        $rootScope.changePageTitle = function(content_header, content_header_small, selected_page){
            //$parent allows us to change the variable in the rootScope property
            $rootScope.content_header = content_header;

            $rootScope.content_header_small = content_header_small;

            $rootScope.selected_page = selected_page;

        };
        $rootScope.changePageTitle('Dashboard', 'Overview', 'Dashboard');

        //A function that gets called several times, in different places It here to prevent code duplication
        $rootScope.goBackInHistory = function(){
            window.history.back();
        };

        var date = new Date();
        $scope.thisYear = date.getFullYear();

        //This is a associative object to format the lowercase and underscores to readable words
        $scope.formatAvailability = {
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };
        //This is a associative object to format the lowercase  to sentence words
        $scope.formatGoodBad = {
            good : 'Good',
            poor : 'Poor',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };
        //This is a associative object to format the lowercase to sentence case words
        $scope.formatGoodPoorFair = {
            good : 'Good',
            fair : 'Fair',
            poor : 'Poor',
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };

        //This is a associative object to format the lowercase to sentence case words
        $scope.formatAdequecy = {
            'adequate' : 'Adequate',
            'not adequate' : 'Inadequate',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable',
            'not available' : 'Not Available',
            '' : 'Not Available'
        };

        //An object that us used several times to display all the terms is a school
        $scope.all_school_terms = [
            {number : '1st', label : '1st Term', value : 'first_term'},
            {number : '2nd', label : '2nd Term', value : 'second_term'},
            {number : '3rd', label : '3rd Term', value : 'third_term'}
        ];

        //This helps to quickly associate the term from the server to a term object above
        $rootScope.one_school_terms = {
            first_term : {number : '1st', label : '1st Term', value : 'first_term'},
            second_term: {number : '2nd', label : '2nd Term', value : 'second_term'},
            third_term : {number : '3rd', label : '3rd Term', value : 'third_term'}
        };

        //Helper function to know the chosen term in a dropdown and associate the server format to readable format
        $scope.academicTermDisplay = function(term){
            return term === 'first_term' ? '1st' :
                term === 'second_term' ? '2nd' :
                    term === 'third_term' ? '3rd' : 'NA'
        };

        //This strips the underscore and capitalizes the data collector type from the server
        $scope.formatDataCollectorType = {
            circuit_supervisor : 'Circuit Supervisor',
            head_teacher : 'Head Teacher'
        };
        //An object that us used  to display data collector type in a dropdown used in school review
        $scope.data_collector_types = [
            //{type : 'circuit_supervisor', label : 'Circuit Supervisor', value : 'data_collector'},
            {type : 'head_teacher', label : 'Head Teacher', value : 'data_collector'}
        ];


        //This strips the underscore and capitalizes the Grant  type from the server
        $scope.formatSchoolGrantType = {
            school_grant : 'School Grant',
            capitation : 'Capitation'
        };

        //This strips the underscore and capitalizes the rank type type from the server
        $scope.formatSuperitendentType = {
            superitendent : 'Superintendent',
            assistant_superitendent : 'Asst. Superintendent'
        };

//This helps to know the extension of a position. eg, 1 returns 'st' as in 1st
        //2 returns 'nd' as in second, etc
        $scope.positionExtension = function(key){

//            if (key == 10) {
//                return 'th'
//            }else{
//                key = parseInt(key.toString().charAt(key.length - 1));
//            }
//
//            return key === 1 ? 'st' :
//                    key === 2 ? 'nd' :
//                    key === 3 ? 'rd' : 'th';

        };

//        A helper function to format the dates returned from the server using moment js
        $scope.momentFormatDate = function(date, format){
            return moment(date).format(format);
            //moment().format('L');    // 09/11/2014
//            moment().format('l');    // 9/11/2014
//            moment().format('LL');   // September 11, 2014
//            moment().format('ll');   // Sep 11, 2014
//            moment().format('LLL');  // September 11, 2014 3:01 PM
//            moment().format('lll');  // Sep 11, 2014 3:01 PM
//            moment().format('LLLL'); // Thursday, September 11, 2014 3:01 PM
//            moment().format('llll'); // Thu, Sep 11, 2014 3:01 PM
        };

        $scope.momentDateFromNow = function(date){
            // eg. Just now, 22 days ago, etc
            return moment(date).fromNow();
        };



        //A helper function to scroll pages to the top on load
        $scope.scroll_to_top =function(){
            $("html, body").animate({ scrollTop: 0 }, 200);
        };

        $scope.uniqueArray =  function(array){
            var seen = [], result = [];
            for(var len = array.length, i = len-1; i >= 0; i--){
                if(!seen[array[i]]){
                    seen[array[i]] = true;
                    result.push(array[i]);
                }
            }
            return result;
        };

        //This function is called when a circuit/district is clicked on
        function clickBarChart(event) {
            if (event.chart.categoryField == 'region') {

                $scope.$broadcast('regionChartClicked', event.item.dataContext.id);

            }else if (event.chart.categoryField == 'district') {

                $scope.$broadcast('districtChartClicked', event.item.dataContext.id);

            }else if (event.chart.categoryField == 'circuit') {

                $scope.$broadcast('circuitChartClicked', event.item.dataContext.id);

            }

        }

        $rootScope.removeTrailingComma = function(classSubject){
            if ($.trim(classSubject.charAt(classSubject.length - 1)) === ',') {
                return classSubject.substring(0, classSubject.length - 1)
            }else return classSubject
        };

        /*Start of AmCharts*/

        $scope.changeSerialChart = function changeCharts(chartData, categoryField, chartTitle, chartDivID, chartType){

            var valueAxisTitle =  "Rating (%)";
            var titleTextTwo = 'Rating';
            var graphValueField = 'rating';

            if (chartType == 'ht_average') {
                valueAxisTitle = "Average (%)";
                titleTextTwo = 'Attendance Average';
                graphValueField = 'headteacherAttendanceAverage'
            }else if(chartType == 'cs_average'){
                valueAxisTitle = "Average (%)";
                titleTextTwo = 'Teacher Attendance Average by Head Teachers';
                graphValueField = 'supervisorAttendanceHolder'
            }

            // SERIAL CHART
            var chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = categoryField;
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 45;
            categoryAxis.title = chartTitle;
            categoryAxis.gridPosition = "start";

            //value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = valueAxisTitle;
            valueAxis.dashLength = 5;
            valueAxis.axisAlpha = 0;
            valueAxis.minimum = 0;
//            valueAxis.maximum = 6;
            valueAxis.integersOnly = true;
            valueAxis.gridCount = 10;
            valueAxis.reversed = false; // this line makes the value axis reversed
            chart.addValueAxis(valueAxis);

            // value
            chart.titles = [
                {
                    "id": "Title-1",
                    "size": 15,
                    text : chartTitle
                },
                {
                    "id": "Title-2",
                    "size": 10,
                    text : titleTextTwo
                }
            ];
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.

            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };



            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField =  graphValueField;
            graph.balloonText = "[[category]]: <b>[[value]]  %</b>";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);

            chart.creditsPosition = "top-right";

            // add click listener
            chart.addListener("clickGraphItem", clickBarChart);

            $interval( chart.write(chartDivID), 200, 2);


            return chart;



        };
        /*End of amSerialCharts*/


        /*Start of amPieCharts*/
        $scope.changePieChart = function(chartData, chartTitle, chartDivID){
            var chart;
            var legend;

            // PIE CHART
            chart = new AmCharts.AmPieChart();
            chart.dataProvider = chartData;
            chart.titleField = "field";
            chart.valueField = "value";

            //COLORS
//			chart.colors = ["#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01", "#B0DE09", "#04D215", "#0D8ECF",
// "#0D52D1", "#2A0CD0", "#8A0CCF", "#CD0D74", "#754DEB", "#DDDDDD", "#999999", "#333333", "#000000",
// "#57032A", "#CA9726", "#990000", "#4B0C25"]
            chart.colors=["#0D8ECF","#FF6600","#8A0CCF","#04D215","#CD0D74","#754DEB","#CA9726","#F8FF01"];

            //3D SETTING
            chart.depth3D = 10; //0 for flat
            chart.angle = 10; //0 when it is flat

            // LEGEND
            legend = new AmCharts.AmLegend();
            legend.align = "center";
            legend.markerType = "circle";
            chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
            chart.addLegend(legend);

            //TITLE
            chart.addTitle(chartTitle);

            //LABEL
            chart.labelRadius = 30;
            chart.labelText = "[[title]]: [[value]]";
//                chart.labelRadius = -30; // label Inside
//                chart.labelText = "[[percents]]%"; // display labels as percentage


            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // WRITE
            $interval(chart.write(chartDivID), 2000, 2);

        };

        /*End of amPieCharts*/

        // PIE CHART OTHER SETTINGS
//        chart.outlineColor = "#FFFFFF";
//        chart.outlineAlpha = 0.8;
//        chart.outlineThickness = 2;
//		chart.labelsEnabled = false;
//		chart.autoMargins = false;
//		chart.marginTop = 0;
//		chart.marginBottom = 0;
//		chart.marginLeft = 0;
//		chart.marginRight = 0;
//		chart.pullOutRadius = 0;

        //START OF LINE CHART




        $scope.changeLineChart = function (chartData, chartTitle,  divID) {
            var chart;
//            var chartData = chartData;

            var graph1Title,graph1ValueField, graph2Title,graph2ValueField,categoryField, categoryAxisTitle;

            if (divID == 'teacherLineChart') {
                graph1Title = "Present";
                graph1ValueField = "days_present";
                graph2Title = "Absent";
                graph2ValueField = "total_number_of_days_absent";
            }else{
                graph1Title = "Boys";
                graph1ValueField = "boys";
                graph2Title = "Girls";
                graph2ValueField = "girls";
            }

            if(divID == 'enrollmentLineChart'){
                categoryField = 'chartLabel';
                categoryAxisTitle = 'Term';
            }else{
                categoryField = 'week';
                categoryAxisTitle = 'Week Number';

            }

            function zoomChart() {
                chart.zoomToIndexes(1, 10);
            }
//

            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "http://cdn.amcharts.com/lib/3/images/";
            chart.dataProvider = chartData;
            chart.categoryField = categoryField;

            // listen for "dataUpdated" event (fired when chart is inited) and call zoomChart method when it happens
//            chart.addListener("dataUpdated", zoomChart);

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = false;
            categoryAxis.minPeriod = "WW";
            categoryAxis.minorGridEnabled = true;
            categoryAxis.axisColor = "#DADADA";
            categoryAxis.twoLineMode = true;
            categoryAxis.title = categoryAxisTitle;
            categoryAxis.dateFormats = [
                {
                    period: 'fff',
                    format: 'JJ:NN:SS'
                },
                {
                    period: 'ss',
                    format: 'JJ:NN:SS'
                },
                {
                    period: 'mm',
                    format: 'JJ:NN'
                },
                {
                    period: 'hh',
                    format: 'JJ:NN'
                },
                {
                    period: 'DD',
                    format: 'DD'
                },
                {
                    period: 'WW',
                    format: 'DD'
                },
                {
                    period: 'MM',
                    format: 'MMM'
                },
                {
                    period: 'YYYY',
                    format: 'YYYY'
                }
            ];

            // first value axis (on the left)
            var valueAxis1 = new AmCharts.ValueAxis();
            valueAxis1.axisColor = "#0D8ECF";
            valueAxis1.axisThickness = 2;
            valueAxis1.gridAlpha = 0;
            valueAxis1.title = graph1Title;
            chart.addValueAxis(valueAxis1);

            // second value axis (on the right)
            var valueAxis2 = new AmCharts.ValueAxis();
            valueAxis2.position = "right"; // this line makes the axis to appear on the right
            valueAxis2.axisColor = "#FF6600";
            valueAxis2.gridAlpha = 0;
            valueAxis2.axisThickness = 2;
            valueAxis2.title = graph2Title;
            chart.addValueAxis(valueAxis2);


            // GRAPHS
            // first graph
            var graph1 = new AmCharts.AmGraph();
            graph1.valueAxis = valueAxis1; // we have to indicate which value axis should be used
            graph1.title = graph1Title;
            graph1.inside = false;
            graph1.valueField =  graph1ValueField;
            graph1.bullet = "round";
            graph1.hideBulletsCount = 30;
            graph1.bulletBorderThickness = 1;
            graph1.lineColor = "#0D8ECF";
            graph1.balloonText = graph1Title + " : [[value]]";
//            graph1.type = "column";
            chart.addGraph(graph1);

            // second graph
            var graph2 = new AmCharts.AmGraph();
            graph2.valueAxis = valueAxis2; // we have to indicate which value axis should be used
            graph2.title = graph2Title;
            graph2.valueField = graph2ValueField;
            graph2.bullet = "square";
            graph2.hideBulletsCount = 30;
            graph2.bulletBorderThickness = 1;
            graph2.lineColor = "#FF6600";
            graph2.balloonText = graph2Title + " : [[value]]";
//            graph2.type = "column";
            chart.addGraph(graph2);

            // third graph
//            var graph3 = new AmCharts.AmGraph();
//            graph3.valueAxis = valueAxis3; // we have to indicate which value axis should be used
//            graph3.valueField = "views";
//            graph3.title = "green line";
//            graph3.bullet = "triangleUp";
//            graph3.hideBulletsCount = 30;
//            graph3.bulletBorderThickness = 1;
//            chart.addGraph(graph3);

            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick:function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0.1;
            chartCursor.fullWidth = true;
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chart.addChartScrollbar(chartScrollbar);

            // LEGEND
            var legend = new AmCharts.AmLegend();
            legend.marginLeft = 110;
            legend.useGraphSettings = true;
            chart.addLegend(legend);

            chart.titles = [
                {
                    "id": "Title-1",
                    "size": 15,
                    text : chartTitle
                }
            ];

            // WRITE
            chart.write(divID);
        };


        /*-------------            Bar Chart        ------------------*/

        $scope.changeBarChart = function (chartData, chartTitle, chartDivID, type, average){

            //type determines if the graph is used i rating views
            //if its a rating graph, change the value axis title to rating, and the
//            display type to a stacked view
            var chart_display_type = null;
            var valueAxisTitle = "Days";

            if (type == 'rating') {
                chart_display_type = 'rating';
                valueAxisTitle = "Rating";
            }else if (type == 'ht_average') {
                chart_display_type = 'ht_average';
                valueAxisTitle = "Teacher Attendance Average";
            }else if (type == 'cs_average') {
                chart_display_type = 'cs_average';
                valueAxisTitle = "Teacher Attendance Average";
            }

//            average is the district average if it is a district average attendance
            var teacher_average = average || 0;


            // SERIAL CHART
            var chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "field";
            chart.startDuration = 1;
            chart.maxSelectedSeries = 25;
            chart.mouseWheelScrollEnabled = true;
            chart.pathToImages = "http://cdn.amcharts.com/lib/3/images/";

            // SCROLLBAR
            var chartScrollBar = new AmCharts.ChartScrollbar();
            chartScrollBar.graphType = 'column';
            chartScrollBar.resizeEnabled = true;
            chartScrollBar.scrollbarHeight = 21;
            chartScrollBar.scrollbarWIdth = 2;
            chartScrollBar.scrollDuration = 0;
            chartScrollBar.updateOnReleaseOnly = false;
            chart.addChartScrollbar(chartScrollBar);

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 0;
            categoryAxis.gridPosition = "start";
            categoryAxis.labelRotation = 45;



            //value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = valueAxisTitle;
            valueAxis.dashLength = 1;
            valueAxis.axisAlpha = 0;
            valueAxis.minimum = 0;
            if (chart_display_type == 'rating') {
                valueAxis.maximum = 10;
            }
            valueAxis.stackType = "regular";
            valueAxis.integersOnly = true;
            valueAxis.gridCount = 10;
            valueAxis.reversed = false; // this line makes the value axis reversed
            chart.addValueAxis(valueAxis);

            // value
            chart.titles = [
                {
                    "id": "Title-1",
                    "size": 15,
                    text : chartTitle
                }
            ];
            if (chart_display_type == 'ht_average' ||  chart_display_type == 'cs_average' ) {
                chart.titles.push({
                    "id": "Title-2",
                    "size": 12,
                    text : 'Teacher Attendance Average : ' + teacher_average + ' %'
                })
            }
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.

            //EXPORTING
            chart.exportConfig = {
                menuTop: "21px",
                menuBottom: "auto",
                menuRight: "21px",
                backgroundColor: "#efefef",

                menuItemStyle	: {
                    backgroundColor			: '#EFEFEF',
                    rollOverBackgroundColor	: '#DDDDDD'},

                menuItems: [{
                    textAlign: 'center',
                    icon: '/img/amChartsImages/export.png',
                    onclick: function(){ return false;},
                    items: [
                        {
                            title: 'JPG',
                            format: 'jpg'
                        },
                        {
                            title: 'PNG',
                            format: 'png'
                        },
//                        {
//                            title: 'SVG',
//                            format: 'svg'
//                        },
                        {
                            title: 'PDF',
                            format: 'pdf'
                        }]
                }]
            };

            // GRAPH
            var graph = new AmCharts.AmGraph();
            if (chart_display_type) {
                graph.title = valueAxisTitle;
            }
            graph.valueField = "value";
            graph.balloonText = "[[field]]: <b>[[value]]</b>";
            graph.type = "column";
            graph.labelText = "[[value]]";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);

            if (chart_display_type) {
                // second graph
                graph = new AmCharts.AmGraph();
                graph.title = "Difference";
//                graph.labelText = "[[value]]";
                graph.valueField = "remaining";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 1;
                graph.lineColor = "#ffc299";
                graph.balloonText = "Difference : <b>[[value]]</b>";
                chart.addGraph(graph);

            }

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);

//            chart.creditsPosition = "top-right";

            if (chart_display_type) {
                // LEGEND
                var legend = new AmCharts.AmLegend();
                legend.align = "center";
//            legend.markerType = "circle";
//                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                chart.addLegend(legend);
            }



            $interval(chart.write(chartDivID), 200, 2);



        };
        /*End of  bar Charts*/

    }]);

/**
 * Created by Kaygee on 18/05/2015.
 */
//This controller is responsible for the selected school
schoolMonitor.controller('scViewSelectedSchoolController', ['$rootScope', '$scope', '$state', '$stateParams','$modal',
    'scSchool', 'selected_school', 'scNotifier', '$timeout', 'scGlobalService',
    function($rootScope, $scope, $state, $stateParams, $modal,
             scSchool, selected_school, scNotifier, $timeout, scGlobalService){

        $scope.selected_school = selected_school;

        scSchool.tracker = {};
        //Variable to keep the stats when you change pages


        $scope.loadingTeachers = true; $scope.loadingData = true;//show a gif when loading teachers in the school
        $scope.teachers_in_school = [];//define the variable so it is inherited in child scopes
        scSchool.allSchoolTeachersData({school_id : selected_school.id})
            .success(function (successData) {
                //Assign school teachers to scope variable
                //to be used to find the number of teachers in the school
                $timeout(function () {
                    $scope.teachers_in_school = successData[0].school_teachers;
                    $scope.teachers_in_school.trained = 0;
                    $scope.teachers_in_school.atpost = 0;
                    $scope.teachers_in_school.studyleave = 0;
                    $scope.teachers_in_school.otherleave = 0;
                    $scope.teachers_in_school.retired = 0;
                    $scope.teachers_in_school.deceased = 0;

                    prepTeachers();

                    $scope.loadingTeachers = false;
                    $scope.loadingData = false;

                },1);
            });


        // /!*Set a variable to allow sharing of data between enrollment and attendance states*!/
        scSchool.loadedEnrollmentInfoFromServer = [];
        scSchool.loadedSpecialEnrollmentInfoFromServer = [];
        scSchool.loadedAttendanceInfoFromServer = [];
        scSchool.loadedSpecialAttendanceInfoFromServer = [];
        scSchool.allSchoolTeachersPunctuality_LessonPlan =[];
        scSchool.allSchoolTeachersClassManagement =[];
        scSchool.allSchoolTeachersUnitsCoveredData =[];
/*
        /!*Load the school images submitted*!/
        $scope.loadingImages = true;
        scSchool.getImages($stateParams.id)
            .then(function (schoolImages) {/!*Image fulfilled callback*!/
                /!*Image Prep Section*!/
                $scope.image = {
                    indicators : [],
                    slides : schoolImages
                };
                //get the number of carousel markers to display
                for (var ci=0; ci <  schoolImages.length; ci++ ) {
                    $scope.image.indicators.push(ci)
                }
                $scope.loadingImages = false;
            }, function () {/!*Images rejected callback*!/
                $scope.loadingImages = false;
            });

        function prepSchoolData(){
            //assign the selected school to the scope variable
            if (scSchool.schoolLookup && scSchool.schoolLookup[$stateParams.id]) {
                $scope.selected_school = scSchool.schoolLookup[$stateParams.id];
                $scope.lookupDataCollector = DataCollectorHolder.lookupDataCollector;

            }else{
                $timeout(function () {
                    prepSchoolData();
                }, 2000)
            }
        }

        function checkSchoolsAreLoadAndPrepSchools() {
            if (!(_.isEmpty(scSchool.schoolLookup)) && angular.isDefined(scSchool.schoolLookup[$stateParams.id])) {
                prepSchoolData();
            } else {
                $timeout(function () {
                    checkSchoolsAreLoadAndPrepSchools();
                }, 2500)
            }
        }


        $scope.$on('updateLoadedModelsFromRefresh', function(evt, data){
            if (data.counter == 6) {
                checkSchoolsAreLoadAndPrepSchools();
            }
        });

        /!*starts from here*!/

*/


        $scope.$on('teachersUpdated', function(evt, teachersObject){
            $scope.teachers_in_school = teachersObject[0].school_teachers;
            $scope.teachers_in_school.trained = 0;
               $scope.teachers_in_school.trained = 0;
            $scope.teachers_in_school.atpost = 0;
            $scope.teachers_in_school.studyleave = 0;
            $scope.teachers_in_school.otherleave = 0;
            $scope.teachers_in_school.retired = 0;
            $scope.teachers_in_school.deceased = 0;
            prepTeachers();
        });

        function prepTeachers() {
            $timeout(function () {
                angular.forEach($scope.teachers_in_school, function (item, index) {
                    if (angular.isUndefined(item.highest_professional_qualification)
                        || item.highest_professional_qualification === ''
                        || item.highest_professional_qualification.toLowerCase() === 'others'
                        || item.highest_professional_qualification.toLowerCase() === 'other') {

                    }else{
                        $scope.teachers_in_school.trained ++;
                    }

                        $scope.teachers_in_school[item.teacher_status] ++;

                })
            });
        }

        //define the variable so it is inherited in child scopes
        $scope.schoolTerms = [];
        $scope.schoolYears = [];
        $scope.schoolWeeks = [];
        $scope.schoolWeeksTrendHolderTracker = [];//this only ensures we don't save one week twice
        $scope.schoolWeeksTrendHolder = [];//this holds the weeks to be displayed in the trends
        $scope.cs_comments = {};
        $scope.loadingWeeklyDataParams = true;//show a gif when loading teachers in the school



        function prepWeeklyDataParams(weeklyData) {
            //Loop through the weekly submitted data and save the weeks, terms, and years for the  generate report dropdown
            angular.forEach(weeklyData.data, function(week, week_index){

                if ( $scope.cs_comments[week.week_number + week.term + week.year]) {
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_students_attendance_comments += scGlobalService.appendIfNotNull(week.cs_students_attendance_comments);
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_students_enrolment_comments += scGlobalService.appendIfNotNull(week.cs_students_enrolment_comments);
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_teacher_attendance_comments += scGlobalService.appendIfNotNull(week.cs_teacher_attendance_comments);
                    $scope.cs_comments[week.week_number + week.term + week.year].cs_teacher_class_management_comments += scGlobalService.appendIfNotNull(week.cs_teacher_class_management_comments);
                }else{
                    // /!*The comment may occur in different rows in the db, the appending function is to concatenate all available comments*!/
                    $scope.cs_comments[week.week_number + week.term + week.year] = {
                        cs_students_attendance_comments: scGlobalService.appendIfNotNull(week.cs_students_attendance_comments ),
                        cs_students_enrolment_comments: scGlobalService.appendIfNotNull(week.cs_students_enrolment_comments ),
                        cs_teacher_attendance_comments: scGlobalService.appendIfNotNull(week.cs_teacher_attendance_comments ),
                        cs_teacher_class_management_comments: scGlobalService.appendIfNotNull(week.cs_teacher_class_management_comments)
                    };
                }


                //save the current term in the loop to a variable
                //one school term is an object that formats the format returned
                // from the server to a user friendlier one
                var currentTerm = $scope.one_school_terms[week.term];

                //save the current year in the loop to a variable
                var currentYear = week.year;

                //save the current week in the loop to a variable
                var currentWeek = week.week_number;
               

                // check if the current term isn't already added to the scope variable that displays the available terms
                //in the submitted data
                if ($scope.schoolTerms.indexOf(currentTerm) < 0) {
                    $scope.schoolTerms.push(currentTerm);

                    $scope.schoolTerms.sort(function(a,b){
                        return a.number.charAt(0) - b.number.charAt(0)
                    });
                }

                // check if the current year isn't already added to the scope variable that displays the available years
                //in the submitted data
                if ($scope.schoolYears.indexOf(currentYear) < 0) {
                    $scope.schoolYears.push(currentYear);
                }

                // check if the current week isn't already added to the scope variable that displays the available weeks
                //in the submitted data
                if ($scope.schoolWeeks.indexOf(currentWeek) < 0) {
                    $scope.schoolWeeks.push(currentWeek);

                    $scope.schoolWeeks.sort(function(a,b){
                        return a - b
                    });
                }
                // check if the current week isn't already added to the scope variable 
                // for the data trending in the submitted data
                if ($scope.schoolWeeksTrendHolderTracker.indexOf(currentYear+currentTerm.value+currentWeek) < 0) {
                    $scope.schoolWeeksTrendHolder.push({
                        week : currentWeek,
                        term : currentTerm.number,
                        termValue : currentTerm.value,
                        termObj : currentTerm,
                        year : currentYear
                    });
                    $scope.schoolWeeksTrendHolder.sort(function(a,b){
                        return a.week - b.week
                    });

                    $scope.schoolWeeksTrendHolderTracker.push(currentYear+currentTerm.value+currentWeek);

                }
            });
            
            
            delete $scope.schoolWeeksTrendHolderTracker; //free some memory of the scope

        }

        scSchool.fetchAvailableWeeklySubmittedData(selected_school.id, 'weekly_total', 'weekly')
            .then(function (data) {
                if (data) {

                    $timeout(function () {
                        prepWeeklyDataParams(data);
                        $scope.loadingWeeklyDataParams = false;
                    }, 1);
                }
            });

        $scope.emptyCache = function () {
            scSchool.clearSchoolCache();
            location.reload(true);
        }
    }]);
/**
 * Created by Kaygee on 07/07/2014.
 */


/**
 *  Setting up configuration uiRouter
 */


schoolMonitor.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    //The redirect from the server after login has no trailing slash, hence the redirect send the route to the error page.
    // urlRouter uses the 'when' function to redirect to the main dashboard
    //$urlRouterProvider.when('', '/');
    $urlRouterProvider.when('', 'dashboard');

    //This happens when a url does not exist.
    $urlRouterProvider.otherwise('/');

    // This is an abstract view for resolving all data from the server. It loads all the data and all other views extend it
    $stateProvider
        .state('dashboard', {
            abstract: true,
            controller : 'scGlobalController',
            // Note: abstract still needs a ui-view for its children to populate.
            template: '<ui-view/>'
        })
        .state('school', {
            url :'/',
            templateUrl: 'partials/view_selected_school.html',
            controller: 'scViewSelectedSchoolController',
            resolve : {
                scSchool : 'scSchool',
                selected_school : ['scSchool', '$location', function (scSchool, $location) {

                    return scSchool.retrieveOneSchool($location.search().school_id)
                }]

            }
        })
        // .state('dashboard.school.statistics', {
        //     url :'statistics',
        //     templateUrl: 'partials/school_statistics.html',
        //     controller: 'scViewSelectedSchoolController'
        //     // resolve : {
        //     //     scSchool : 'scSchool',
        //     //     selected_school : ['scSchool', '$location', function (scSchool, $location) {
        //     //
        //     //         return scSchool.retrieveOneSchool($location.search().school_id)
        //     //     }]
        //     //
        //     // }
        // })
    ;



}]);

/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Factory service for User
 *
 */

schoolMonitor.factory('scUser', ['$resource','$http','$q','$rootScope','CacheFactory','scNotifier','$state','$timeout',
    function($resource, $http, $q, $rootScope, CacheFactory, scNotifier, $state, $timeout){

        var userService = {};
        var url = '/admin/user/profile';

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('userCache')) {
            var userCache = CacheFactory('userCache', {
                maxAge: 60 * 60 * 1000, // 60minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    if (key == url) {
                        $http.get(key).success(function (successData) {
                            if (successData.code == '200' && $.trim(successData.status.toLowerCase()) == 'ok') {
                                if(successData.user.type && successData.user.type  == "circuit_supervisor"){
                                    successData.user =  appendCSMissingProperties(successData.user);
                                }
                                userCache.put(key, successData);
                            }else{
                                $rootScope.logOut();
                            }
                        });
                    }
                }
            });
        }

        /*The CS that logs in has some different properties from the admin login object. This function makes sure the login objects are the same*/
        var appendCSMissingProperties = function (returnedUserObj) {
            returnedUserObj.username = returnedUserObj.first_name + " " + returnedUserObj.last_name;
            returnedUserObj.firstname = returnedUserObj.first_name;
            returnedUserObj.lastname = returnedUserObj.last_name;
            returnedUserObj.level_id =returnedUserObj.circuit_id;
            returnedUserObj.level = "circuit";
            returnedUserObj.role = 'super';/*normal*/

            return returnedUserObj;
        };

        function fetchCurrentUser(){
            var deferred = $q.defer();

            var cachedData = userCache.get(url);

            if (!cachedData ) {
                $http.get(url)
                    .success(function(successData){

                        if (successData.code == '200' && $.trim(successData.status.toLowerCase()) == 'ok') {
                            if(successData.user.type && successData.user.type  == "circuit_supervisor"){
                                successData.user =  appendCSMissingProperties(successData.user);
                            }else{
                                /*parsing the JSON because the server returns a string*/
                                successData.user =  JSON.parse(successData.user);
                            }
                            $rootScope.currentUser = userService.currentUser = successData.user;

                            userCache.put(url, successData.user);
                            deferred.resolve(true);
                        }else{
                            $state.go('login');
                            deferred.resolve(false);
                        }
                    })
                    .error(function (data) {
                        $state.go('login');
                        deferred.reject(false);
                    });
            }else{

                userService.currentUser = cachedData;
                $rootScope.currentUser = cachedData;
                deferred.resolve(true);
            }
            return deferred.promise;
        }

        userService.loginUser = function (loginForm) {
            $rootScope.submittingLogin = true;
            var loginUrl = '/login';
            if ($rootScope.csLoginCheck) {
                if(loginForm.cs_phone_number.length == 10 &&  loginForm.cs_phone_number.charAt(0) == '0' ){
                    loginForm.cs_phone_number = "+233" + loginForm.cs_phone_number.substring(1);
                }else if ( loginForm.cs_phone_number.length > 10 && loginForm.cs_phone_number.substring(0,3) == "233") {
                    loginForm.cs_phone_number = "+" + loginForm.cs_phone_number;
                }
                loginUrl = "/cs/dashboard/authentication";
            }

            $http.post(loginUrl, loginForm)
                .success(function (successData) {
                    if (successData.code == '200' && successData.status.toLowerCase() == 'ok') {
                        scNotifier.notifySuccess("Login Success", "Credentials are valid. Redirecting to the dashboard");
                        if (successData.authticated_cs) {
                            successData.authticated_cs =  appendCSMissingProperties(successData.authticated_cs);
                            $rootScope.currentUser =  userService.currentUser = successData.authticated_cs;
                        }else{
                            /*parsing the JSON because the server returns a string*/
                            $rootScope.currentUser =  userService.currentUser = JSON.parse(successData.user);
                        }
                        /*delete is used so we don't have too many variables in the scope object*/
                        delete $rootScope.submittingLogin;
                        delete $rootScope.loginError;
                        $state.go('dashboard.home');

                    }else{
                        scNotifier.notifyFailure("Login Failed", "Please check credentials, and re-enter them.");
                        $rootScope.loginError = true;
                        $timeout(function () {
                            /*delete is used so we don't have too many variables in the scope object*/
                            delete $rootScope.loginError;
                        }, 5000);
                        delete $rootScope.submittingLogin;

                    }
                })
                .error(function () {
                    scNotifier.notifyFailure("Network Error", "Please check internet connection and try again.");
                    delete $rootScope.submittingLogin;

                })

        };


        userService.authenticate =  function(){
            var defer = $q.defer();
            if ( userService.currentUser &&  userService.currentUser.id) {
                defer.resolve(true);
                return defer.promise;
            }

            fetchCurrentUser().
                then(function(success){
                    if (success) {
                        defer.resolve(true);
                    }else{
                        defer.reject(false);
                    }
                }, function (error) {
                    defer.reject(false);
                });
            return defer.promise;
        };

        userService.getAllUsers = function(){
            var deferred = $q.defer();
            var url = '/admin/users?country_id=1';

            var cachedData = userCache.get(url);
            if (!cachedData ) {
                $http.get(url)
                    .success(function(data){
                        userCache.put(url, data);
                        userService.allUsers = data.users;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                userService.allUsers = cachedData.users;
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


        userService.createUser =  function(formData){
            return $resource('/admin/user', {}, {
                create:{method:'POST', url:'/admin/user/create', params:{}}
            }).create(formData).$promise;
        };
        userService.editUser =  function(formData){
            return $resource('/admin/user', {}, {
                edit:{method:'POST', url:'/admin/user/update', params:{}}
            }).edit(formData).$promise;
        };
        userService.resetPassword =  function(formData){
            return $resource('/admin/user', {}, {
                editPassword:{method:'POST', url:'/admin/user/password/change', params:{}}
            }).editPassword(formData).$promise;
        };
        userService.deleteUser =  function(user_id){
            return $resource('/admin/user', {}, {
                edit:{method:'POST', url:'/admin/user/:id/delete', params:{id : '@id'}}
            }).edit({id : user_id}).$promise;
        };

        userService.reset = function(){
            userService.allUsers = [];
            userCache.removeAll();
        };

        $rootScope.logOut = function () {
            userCache.remove(url);
            userCache.removeAll();
            userService.currentUser =  '';
            $http.get('/logout')
                .success(function () {
                    $rootScope.currentUser = '';
                });
        };

        return userService;

    }]);



/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.factory('scRegion', ['$resource' , '$rootScope', '$q', 'CacheFactory','$http','scNotifier',
    function($resource, $rootScope,$q, CacheFactory, $http, scNotifier){

    var regionResource = $resource('/admin/ghana/regions',{},{
        query:{isArray:true,cache:false},
        create:{method:'POST', url:'/admin/region/create', params:{}},
        retrieveOneRegion:{method:'GET', url:'/admin/region/:id/edit', params:{id : '@id'}},
        editRegion:{method:'POST', url:'/admin/region/:id/update', params:{id : '@id'}},
        deleteRegion:{method:'POST', url:'/admin/region/:id/delete', params:{id : '@id'}}
    });

    var scRegion = {};

    // Check to make sure the cache doesn't already exist
    if (!CacheFactory.get('regionCache')) {
        var regionCache = CacheFactory('regionCache', {
            //maxAge: 60 * 60 * 1000 // 1 hour
            //deleteOnExpire: 'aggressive'
            //onExpire: function (key, value) {
            //    $http.get(key).success(function (data) {
            //        profileCache.put(key, data);
            //    });
            //}
        });
    }

//    var health_status = {
//        0 : 'very poor',
//        1 : 'bad',
//        2 : 'needs attention',
//        3 : 'healthy'
//    };

    scRegion.regions = [];

    var url = '/admin/ghana/regions', loadingRegions = false;

    scRegion.getAllRegions = function(){
        if (loadingRegions) {
            return;
        }
        loadingRegions = true;
        var deferred = $q.defer();

        var cachedData = regionCache.get(url);

        if (cachedData == undefined ) {
            scNotifier.notifyInfo('Info', 'Fetching regions from the server');
            $http.get(url)
                .success(function(data){
                    
                    data.sort(function (a, b) {
                        return a.name.toLowerCase() - b.name.toLowerCase();
                    });
                    
                    regionCache.put(url, data);
                    scRegion.regions = data;
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.resolve(false);
                })
                .finally(function () {
                    loadingRegions = false;
                })
        }else{
            scRegion.regions = cachedData;
            deferred.resolve(cachedData);
            loadingRegions = false;
        }
        return deferred.promise;
        //if (!regions) {
        //    var defer = $q.defer();
        //    regionResource.query({},
        //        function(data){
        //            if (data) {
        //                defer.resolve(true);
        //                scRegion.regions = data;
        //            }else{
        //                defer.resolve(false)
        //            }
        //        }, function(){
        //            defer.resolve(false);
        //        });
        //    return defer.promise;
        //}
        //return this.regions;
    };

    scRegion.updateSupervisorArray = function(){
        regionCache.remove(url);
        this.getAllRegions().then(function () {
            scRegion.regions.sort(function(a, b){
                if (a.name > b.name) {
                    return 1;
                }else if (a.name < b.name) {
                    return -1
                }else{
                    return 0
                }
            })
        });
    };


    scRegion.init = function(){

        scRegion.regionLookup = {};
        angular.forEach(this.regions, function(region, index){

            region.rating = 0;
            scRegion.regionLookup[region.id] = region;

            scRegion.regions[index].inclusiveness =  0;
            scRegion.regions[index].inclusivenessHolder = 0;

            scRegion.regions[index].safe_protective = 0;
            scRegion.regions[index].safe_protectiveHolder = 0;

            scRegion.regions[index].healthy = 0;
            scRegion.regions[index].healthyHolder = 0;

            scRegion.regions[index].gender_friendly = 0;
            scRegion.regions[index].gender_friendlyHolder = 0;

            scRegion.regions[index].effective_teaching_learning = 0;
            scRegion.regions[index].effective_teaching_learningHolder = 0;

            scRegion.regions[index].community_involvement = 0;
            scRegion.regions[index].community_involvementHolder = 0;

            scRegion.regions[index].supervisorAttendanceHolder = 0;
            scRegion.regions[index].supervisorAttendanceAverage = 0;

            scRegion.regions[index].headteacherAttendanceHolder = 0;
            scRegion.regions[index].headteacherAttendanceAverage = 0;

            scRegion.reviewDataExists = false;

            //This is to keep track of how many districts are in each region
            scRegion.regionLookup[region.id].totalDistricts = 0;

            //This is to contain all districts under this region that can be looked up by id
            scRegion.regionLookup[region.id].allDistricts = {};

            //This is to contain all districts under this region that can be looped over
            scRegion.regionLookup[region.id].allDistrictsHolder = [];

            //This is to keep track of how many circuits are in each region
            scRegion.regionLookup[region.id].totalCircuits = 0;

            //This is to contain all circuits under this region to be looked up by id
            scRegion.regionLookup[region.id].allCircuits = {};

            //This is to contain all circuits under this region
            scRegion.regionLookup[region.id].allCircuitsHolder = [];

            //This is to keep track of how many schools are in each region
            scRegion.regionLookup[region.id].totalSchools = 0;

            //This is to contain all schools under this region to be looked up by id
            scRegion.regionLookup[region.id].allSchools = {};

            //This is to contain all schools under this region
            scRegion.regionLookup[region.id].allSchoolsHolder = [];

            //This is to keep track of how many supervisors are in each region
            scRegion.regionLookup[region.id].circuit_supervisors = 0;

            //This is to hold supervisors in each region
            scRegion.regionLookup[region.id].circuit_supervisors_holder = [];

            //This is to keep track of how many headteachers are in each region
            scRegion.regionLookup[region.id].head_teachers = 0;

            //This is to hold headteachers in each region
            scRegion.regionLookup[region.id].head_teachers_holder = [];

            /*kgTodo - Calculate region rating and position*/
            //This is randomly generated positions and ratings pending actual implementation
            //scRegion.regions[index].rating =  Math.floor(Math.random() * 100);
//            scRegion.regions[index].position = Math.floor(Math.random() * 8) + 1;

            //This is randomly generated school review ratings pending actual implementation
        });

        scRegion.regions.sort(function(a, b){
            if (a.name > b.name) {
                return 1;
            }else if (a.name < b.name) {
                return -1
            }else{
                return 0
            }
        })

    };

    scRegion.addRegion = function(formData){
        var defer = $q.defer();
        // process the form
        regionResource.create(formData,
            //success callback
            function (value) {
                if (value[0] == 's' && value[1] == 'u') {
                    scRegion.updateSupervisorArray();
                    defer.resolve(true);
                }else {
//                    scRegion.addRegionStatus = value;
                    defer.resolve(false);
                }
            },
            //error callback
            function (httpResponse) {
                defer.resolve(false);
            });
        return defer.promise;
    };

    scRegion.deleteRegion = function(region_id){

        var defer = $q.defer();
        if (scRegion.regionLookup[region_id]) {
            regionResource.deleteRegion({id : region_id},
                //success callback
                function(data){
                    if (data[0] == 's' && data[1] == 'u') {
                        scRegion.updateSupervisorArray();
                        defer.resolve(true);
                    }else{
                        defer.resolve(false);
                    }
                },
//                error callback
                function(){
                    defer.resolve(false)
                });
        }else{
            defer.resolve(false);
        }
        return defer.promise;
    };

//    Retrieve a single region to view or edit
    scRegion.retrieveOneRegion = function(region_id){
        var defer = $q.defer();
        regionResource.retrieveOneRegion({id: region_id},
            //After retrieving the region to edit, return it to the controller

            function (data, responseHeaders) {
                scRegion.regionRetrieved = data;
                defer.resolve(true);
            },
            //Error function

            function (httpResponse) {
                defer.resolve(false);
                return false
            });
        return defer.promise;
    };

    //After posting the form for editing, this gets called
    scRegion.editRegion = function(formData){
        var defer = $q.defer();
        regionResource.editRegion(formData,
            //success callback
            function (value, responseHeaders) {
//                        the value returned is a json object, hence the wierd checking to see success
                if (value == 'success' || (value[0] == 's' && value[1] == 'u')) {
                    //If successful, requery the new regions and assign them to the parent regions variable
                    scRegion.updateSupervisorArray();
                    defer.resolve(true);
                } else {
                    defer.resolve(false)
                }
            },
            //error callback
            function (httpResponse) {
                defer.resolve(false);
            });
        return defer.promise;
    };

    scRegion.sendNotification = function(){

    };

        scRegion.fetchTermlyData = function(params){

            var aRegionsTermlyDataUrl = '/admin/totals/termly/data';
            var aRegionsCachedTermlyDataUrl = '/admin/totals/termly/data?country_id=1'+JSON.stringify(params);

            var deferred = $q.defer();

            var cachedData = regionCache.get(aRegionsCachedTermlyDataUrl);

            if (cachedData == undefined ) {
                $http.get(aRegionsTermlyDataUrl , {params : params })
                    .success(function(data){
                        regionCache.put(aRegionsCachedTermlyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


    return scRegion;



}]);


/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for District resource
 *
 */

schoolMonitor.factory('scDistrict', ['$resource', 'scRegion', '$q','CacheFactory','$http','scNotifier','$rootScope','$timeout',
    function($resource, scRegion, $q, CacheFactory, $http, scNotifier, $rootScope, $timeout){

        var districtResource = $resource('/admin/ghana/districts',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/district/create', params:{}},
            createItinerary:{method:'POST', url:'/admin/district/itinerary', params:{}},
            getItinerary:{method:'GET', url:'/admin/district/itinerary/:id', params:{}},
            getAllItineraries:{method:'GET', url:'/admin/itinerary', params:{country_id: 1}, isArray:true, cache:false},
            retrieveOneDistrict:{method:'GET', url:'/admin/district/:id/edit', params:{id : '@id'}},
            editDistrict:{method:'POST', url:'/admin/district/:id/update', params:{id : '@id'}},
            deleteDistrict:{method:'POST', url:'/admin/district/:id/delete', params:{id : '@id'}}
        });

        var scDistrict = {};

        var districts;

        scDistrict.districts = [];

        scDistrict.districtLookup = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('districtCache')) {
            var districtCache = CacheFactory('districtCache', {
                //maxAge: 60 * 60 * 1000 // 1 hour
                //deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        var allDistrictsUrl = '/admin/ghana/districts', loadingDistricts = false;

        scDistrict.getAllDistricts = function(){
            if (loadingDistricts) {
                return;
            }
            loadingDistricts = true;

            var defer = $q.defer();
            var cachedData = districtCache.get(allDistrictsUrl);

            if (!cachedData) {
                scNotifier.notifyInfo('Info', 'Updating districts...');
                districtResource.query().$promise.then(function(data){
                    if (data) {
                        scDistrict.districts = data;
                        districtCache.put(allDistrictsUrl, data);
                        loadingDistricts = false;
                        defer.resolve(true);
                    }
                }, function(){
                    loadingDistricts = false;
                    defer.resolve(false)
                });
            }else{
                scDistrict.districts = cachedData;
                loadingDistricts = false;
                defer.resolve(true);
            }
            return defer.promise;
        };

        scDistrict.updateDistrictArray = function(){
            var defer = $q.defer();
            districtCache.remove(allDistrictsUrl);
            this.districts = [];

            this.getAllDistricts().then(
                function(status){
                    if (status) {
                        scDistrict.districts.sort(function (a, b) {
                            return a.name.toLowerCase() - b.name.toLowerCase();
                        });
                        $timeout(function () {
                            $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                            $rootScope.loading = false;
                            defer.resolve(true);
                        }, 100);
                    }else{
                        defer.resolve(false);
                    }
                }, function(){
                    $rootScope.loading = false;

                    defer.resolve(false);
                });
            return defer.promise;
        };

        $rootScope.reloadDistricts = function () {
            $rootScope.loading = true;
            scDistrict.updateDistrictArray();
        };


        scDistrict.init = function(){

            angular.forEach(this.districts, function(district, index){

                //Initiate the district rating
                district.rating = 0;
                scDistrict.districtLookup[district.id] = district;

                scDistrict.districtLookup[district.id].inclusiveness =  0;
                scDistrict.districtLookup[district.id].inclusivenessHolder = 0;

                scDistrict.districtLookup[district.id].safe_protective = 0;
                scDistrict.districtLookup[district.id].safe_protectiveHolder = 0;

                scDistrict.districtLookup[district.id].healthy =  0;
                scDistrict.districtLookup[district.id].healthyHolder = 0;

                scDistrict.districtLookup[district.id].gender_friendly = 0;
                scDistrict.districtLookup[district.id].gender_friendlyHolder = 0;

                scDistrict.districtLookup[district.id].effective_teaching_learning =  0;
                scDistrict.districtLookup[district.id].effective_teaching_learningHolder = 0;

                scDistrict.districtLookup[district.id].community_involvement =  0;
                scDistrict.districtLookup[district.id].community_involvementHolder = 0;

                scDistrict.districtLookup[district.id].supervisorAttendanceHolder = 0;
                scDistrict.districtLookup[district.id].supervisorAttendanceAverage = 0;

                scDistrict.districtLookup[district.id].headteacherAttendanceHolder = 0;
                scDistrict.districtLookup[district.id].headteacherAttendanceAverage =  0;


                //Get the region name into the district object
                scDistrict.districts[index].regionName = scRegion.regionLookup[district.region_id].name;

                //This is to keep track of how many circuits are in each district
                scDistrict.districtLookup[district.id].totalCircuits = 0;

                //This is to contain all circuits under this district for easy lookup
                scDistrict.districtLookup[district.id].allCircuits = {};

                //This is to contain all circuits under this district for displaying
                scDistrict.districtLookup[district.id].allCircuitsHolder = [];

                //This is to keep track of how many schools are in each district
                scDistrict.districtLookup[district.id].totalSchools = 0;

                //This is to contain all schools under this district to be looked up by id
                scDistrict.districtLookup[district.id].allSchools = {};

                //Use the school_id in the scDistrict so that we can lookup a district using only the school_Id
                scDistrict.searchSchoolDistrict = {};

                //This is to contain all schools under this district
                scDistrict.districtLookup[district.id].allSchoolsHolder = [];

                //This is to contain all itineraries of each district
                scDistrict.districtLookup[district.id].itineraries = [];

//        //This is to keep track of how many supervisors are in each district
                scDistrict.districtLookup[district.id].circuit_supervisors = 0;

                //This is to hold circuit supervisors that are in each district
                scDistrict.districtLookup[district.id].circuit_supervisors_holder = [];

//        //This is to keep track of how many headteachers are in each district
                scDistrict.districtLookup[district.id].head_teachers = 0;

                //This is to hold head teachers that are in each district
                scDistrict.districtLookup[district.id].head_teachers_holder = [];

                /*kgTodo - Calculate district rating and position*/
                //This is randomly generated positions and ratings pending actual implementation
//            scDistrict.districts[index].rating = Math.floor(Math.random() * 100);
                scDistrict.districts[index].position = Math.floor(Math.random() * 10 + 1);

                //Increment the total districts of the region by one
                scRegion.regionLookup[district.region_id].totalDistricts += 1;

                //add district to the region object and associate it by its id
                scRegion.regionLookup[district.region_id].allDistricts[district.id] = district;

                //add district to the region object's districts
                scRegion.regionLookup[district.region_id].allDistrictsHolder.push(district);

                //This is randomly generated school review ratings pending actual implementation
                //scDistrict.districts[index].inclusiveness = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].safe_protective = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].healthy = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].gender_friendly = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].effective_teaching_learning = Math.floor(Math.random() * 10);
                //scDistrict.districts[index].community_involvement = Math.floor(Math.random() * 10);

            });

            scDistrict.districts.sort(function (a, b) {
                if (a.name > b.name)
                    return 1;
                if (a.name < b.name)
                    return -1;
                // a must be equal to b
                return 0;
            });
        };

        scDistrict.circuits= [];
        scDistrict.schools =[];

        scDistrict.addDistrict = function(formData){
            var defer = $q.defer();
            // process the form
            districtResource.create(formData,
                //success callback
                function (value) {
                    if (value[0] == 's' && value[1] == 'u') {
                        scDistrict.updateDistrictArray().then(
                            function(status){
                                if (status) {
                                    defer.resolve(true);
                                }else{
                                    defer.resolve(false);
                                }
                            },
                            function(){
                                defer.resolve(false);
                            }
                        );
                        defer.resolve(true);
                    }else {
//                    scDistrict.addDistrictStatus = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };

        scDistrict.deleteDistrict = function(district_id){

            var defer = $q.defer();
            if (scDistrict.districtLookup[district_id]) {
                districtResource.deleteDistrict({id : district_id},
                    //success callback
                    function(data){
                        if (data[0] == 's' && data[1] == 'u') {

                            scDistrict.updateDistrictArray().then(
                                function(status){
                                    if (status) {
                                        defer.resolve(true);
                                    }else{
                                        defer.resolve(false);
                                    }
                                },
                                function(){
                                    defer.resolve(false);
                                }
                            );
                        }else{
                            defer.resolve(false);
                        }
                    },
//                error callback
                    function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };




//    Retrieve a single district to view or edit
        scDistrict.retrieveOneDistrict = function(district_id){
            var defer = $q.defer();
            districtResource.retrieveOneDistrict({id: district_id},
                //After retrieving the region to edit, return it to the controller

                function (data, responseHeaders) {
                    defer.resolve(true);
                    scDistrict.districtRetrieved = data
                },

                //Error function
                function (httpResponse) {
                    defer.resolve(false);
                    return false
                });
            return defer.promise;
        };



        scDistrict.editDistrict = function(formData){
            var defer = $q.defer();
            districtResource.editDistrict(formData,
                //success callback
                function (value, responseHeaders) {
//                        the value returned is a json object, hence the wierd checking to see success
                    if (value == 'success' || (value[0] == 's' && value[1] == 'u')) {
                        //If successful, requery the new regions and assign them to the parent regions variable
                        scDistrict.updateDistrictArray();
                        defer.resolve(true);
                    } else {
                        defer.resolve(false)
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };

        scDistrict.sendNotification = function(){

        };

        scDistrict.fetchItinerary = function(district_id, reloader){

            var aDistrictsItineraryUrl = '/admin/itinerary?country_id=1&district_id='+district_id;

            if (reloader) {
                districtCache.remove(aDistrictsItineraryUrl);
            }

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsItineraryUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsItineraryUrl)
                    .success(function(data){
                        districtCache.put(aDistrictsItineraryUrl, data);
                        scDistrict.districtLookup[district_id].itineraries = data.itinerary;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                scDistrict.districtLookup[district_id].itineraries = cachedData.itinerary;
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };

        scDistrict.allTeachersData = function(district_id){
            //return $resource('/admin/ghana/districts', {}, {
            //    allDistrictTeachersData:{method:'GET', url:'/admin/school_report_card/school/teachers', params:{country_id: 1}, isArray:true, cache:false}
            //}).allDistrictTeachersData({district_id : district_id});
            var aDistrictsTeachersDataUrl = '/admin/school_report_card/school/teachers?country_id=1&district_id='+district_id;

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsTeachersDataUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsTeachersDataUrl)
                    .success(function(data){
                        districtCache.put(aDistrictsTeachersDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };

        scDistrict.fetchWeeklyData = function(district_id){
            //return $resource('/admin/ghana/districts', {}, {
            //    getWeeklyData:{method:'GET', url:'/admin/totals/weekly/data', params:{country_id: 1}, isArray:true, cache:false}
            //}).getWeeklyData({district_id : district_id});
            var aDistrictsWeeklyDataUrl = '/admin/totals/weekly/data?country_id=1&district_id='+district_id;

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsWeeklyDataUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsWeeklyDataUrl)
                    .success(function(data){
                        districtCache.put(aDistrictsWeeklyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };

        scDistrict.fetchTermlyData = function(params, reloader){
            //return $resource('/admin/ghana/districts', {}, {
            //    getTermlyData:{method:'GET', url:'/admin/totals/termly/data', params:{country_id: 1}, isArray:true, cache:false}
            //}).getTermlyData({circuit_id : circuit_id});

            var aDistrictsTermlyDataUrl = '/admin/totals/termly/data';
            var aDistrictsCachedTermlyDataUrl = '/admin/totals/termly/data?country_id=1'+JSON.stringify(params);


            if (reloader) {
                districtCache.remove(aDistrictsCachedTermlyDataUrl);
            }

            var deferred = $q.defer();

            var cachedData = districtCache.get(aDistrictsCachedTermlyDataUrl);

            if (cachedData == undefined ) {
                $http.get(aDistrictsTermlyDataUrl , {params : params })
                    .success(function(data){
                        districtCache.put(aDistrictsCachedTermlyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


        return scDistrict;



    }]);

/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Circuit resource
 *
 */

schoolMonitor.factory('scCircuit', ['scRegion', 'scDistrict', '$resource', '$q','CacheFactory','$http','scNotifier','$rootScope','$timeout',
    function(scRegion, scDistrict, $resource, $q, CacheFactory, $http, scNotifier, $rootScope, $timeout){

        var circuitResource = $resource('/admin/ghana/circuits',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/circuit/create', params:{}},
            retrieveOneCircuit:{method:'GET', url:'/admin/circuit/:id/edit', params:{id : '@id'}},
            editCircuit:{method:'POST', url:'/admin/circuit/:id/update', params:{id : '@id'}},
            deleteCircuit:{method:'POST', url:'/admin/circuit/:id/delete', params:{id : '@id'}}
        });

        var scCircuit = {};

        var circuits;

        scCircuit.calculateRank =  function(){
            return {};
        };

        scCircuit.calculateRating = function(circuit_id){
//        angular.forEach(scCircuit.circuitLookup[circuit_id].allSchools, function(circuit, index){
//            scCircuit.circuitLookup[circuit_id].inclusiveness = 0;
//        });
        };

        scCircuit.circuits = [];

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('circuitCache')) {
            var circuitCache = CacheFactory('circuitCache', {
                //maxAge: 60 * 60 * 1000 // 1 hour
                //deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        var allCircuitsUrl = '/admin/ghana/circuits', loadingCircuits = false;

        scCircuit.getAllCircuits = function(){
            if (loadingCircuits) {
                return;
            }
            loadingCircuits = true;
            var defer = $q.defer();
            var cachedData = circuitCache.get(allCircuitsUrl);

            if (!cachedData) {
                scNotifier.notifyInfo('Info', 'Updating circuits...');
                circuitResource.query().$promise.then(function(data){
                    if (data) {

                        data.sort(function (a, b) {
                            return a.name.toLowerCase() - b.name.toLowerCase();
                        });

                        scCircuit.circuits = data;
                        circuitCache.put(allCircuitsUrl, data);
                        loadingCircuits = false;
                        defer.resolve(true);
                    }else{
                        loadingCircuits = false;
                        defer.resolve(false);
                    }
                }, function(){
                    loadingCircuits = false;
                    defer.resolve(false);
                });
            }else{
                scCircuit.circuits = cachedData;
                loadingCircuits = false;
                defer.resolve(true);
            }
            return defer.promise;
        };



        scCircuit.updateCircuitArray = function(){
            var defer = $q.defer();
            circuitCache.remove(allCircuitsUrl);
            this.circuits = [];

            this.getAllCircuits().then(
                function(status){

                    if (status) {
                        $timeout(function () {
                            $rootScope.loading = false;
                            $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});
                            defer.resolve(true);
                        }, 100);
                    }else{
                        defer.resolve(false);
                    }
                }, function(){
                    $rootScope.loading = false;

                    defer.resolve(false);
                });
            return defer.promise;
        };

        $rootScope.reloadCircuits = function () {
            $rootScope.loading = true;
            scCircuit.updateCircuitArray();
        };

        scCircuit.init = function(){
            scCircuit.circuitLookup ={};
            angular.forEach(this.circuits, function(circuit, index){

                circuit.rating = 0;
                scCircuit.circuitLookup[circuit.id] = circuit;

                scCircuit.circuitLookup[circuit.id].inclusiveness = 0;
                scCircuit.circuitLookup[circuit.id].inclusivenessHolder = 0;

                scCircuit.circuitLookup[circuit.id].safe_protective = 0;
                scCircuit.circuitLookup[circuit.id].safe_protectiveHolder = 0;

                scCircuit.circuitLookup[circuit.id].healthy = 0;
                scCircuit.circuitLookup[circuit.id].healthyHolder = 0;

                scCircuit.circuitLookup[circuit.id].gender_friendly =0;
                scCircuit.circuitLookup[circuit.id].gender_friendlyHolder = 0;

                scCircuit.circuitLookup[circuit.id].effective_teaching_learning =0;
                scCircuit.circuitLookup[circuit.id].effective_teaching_learningHolder = 0;

                scCircuit.circuitLookup[circuit.id].community_involvement = 0;
                scCircuit.circuitLookup[circuit.id].community_involvementHolder = 0;

                scCircuit.circuitLookup[circuit.id].supervisorAttendanceHolder = 0;
                scCircuit.circuitLookup[circuit.id].supervisorAttendanceAverage = 0;

                scCircuit.circuitLookup[circuit.id].headteacherAttendanceHolder = 0;
                scCircuit.circuitLookup[circuit.id].headteacherAttendanceAverage = 0;

                //This is to keep track of how many schools are in each district
                scCircuit.circuitLookup[circuit.id].totalSchools = 0;

                //This is to contain all schools under this circuit to be looked up by id
                scCircuit.circuitLookup[circuit.id].allSchools = {};

                //This is to contain all schools under this circuit
                scCircuit.circuitLookup[circuit.id].allSchoolsHolder = [];

                //This is to keep track of the circuit supervisor of this circuit
                scCircuit.circuitLookup[circuit.id].circuitSupervisor = null;

                //This is to keep track of how many headteachers are in each region
                scCircuit.circuitLookup[circuit.id].head_teachers = 0;

                //This is to hold headteachers in each circuit
                scCircuit.circuitLookup[circuit.id].head_teachers_holder = [];

                //Assign region name to circuit object
                scCircuit.circuits[index].regionName = scRegion.regionLookup[circuit.region_id].name;

                //Increment its region by 1
                scRegion.regionLookup[circuit.region_id].totalCircuits += 1;

                //add circuit to the region lookup object and associate it by its id
                scRegion.regionLookup[circuit.region_id].allCircuits[circuit.id] = circuit;

                //add circuit to the region lookup object
                scRegion.regionLookup[circuit.region_id].allCircuitsHolder.push(circuit);

                //Assign district name to circuit object
                scCircuit.circuits[index].districtName = scDistrict.districtLookup[circuit.district_id].name;

                //Increment its district by 1
                scDistrict.districtLookup[circuit.district_id].totalCircuits += 1;

                //add circuit to the district lookup object and associate it by its id
                scDistrict.districtLookup[circuit.district_id].allCircuits[circuit.id] = circuit;

                //add circuit to the district lookup object
                scDistrict.districtLookup[circuit.district_id].allCircuitsHolder.push(circuit);

                /*kgTodo - Calculate Circuit Rating*/
                //Calculate circuits rating and position
//            scCircuit.circuits[index].rating = Math.floor(Math.random() * 100 + 1);
//            scCircuit.circuits[index].position = Math.floor(Math.random() * 10 + 1);


            })
        };


        scCircuit.addCircuit= function(formData){
            var defer = $q.defer();
            // process the form
            circuitResource.create(formData,
                //success callback
                function (value) {
                    if (value[0] == 's' && value[1] == 'u') {
                        scCircuit.updateCircuitArray().then(
                            function(status){
                                if (status) {
                                    defer.resolve(true);
                                }else{
                                    defer.resolve(false);
                                }
                            },
                            function(){
                                defer.resolve(false);
                            });

                    }else {
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };

//    Retrieve a single district to view or edit
        scCircuit.retrieveOneCircuit = function(circuit_id){
            var defer = $q.defer();
            circuitResource.retrieveOneCircuit({id: circuit_id},
                //After retrieving the region to edit, return it to the controller
                function (data, responseHeaders) {
                    defer.resolve(true);
                    scCircuit.circuitRetrieved = data
                },
                //Error function
                function (httpResponse) {
                    defer.resolve(false);
                    return false
                });
            return defer.promise;
        };


        scCircuit.editCircuit = function(formData){
            var defer = $q.defer();
            circuitResource.editCircuit(formData,
                //success callback
                function (value, responseHeaders) {
//                        the value returned is a json object, hence the wierd checking to see success
//                 if (value == 'success' || (value[0] == 's' && value[1] == 'u')) {
//                     //If successful, requery the new regions and assign them to the parent regions variable
//                     scCircuit.updateCircuitArray();
//                     defer.resolve(true);
//                 } else {
//                     scCircuit.updateCircuitArray();
//                     defer.resolve(false)
//                 }
                    scCircuit.updateCircuitArray().then(
                        function(status){
                            if (status) {
                                defer.resolve(true);
                            }else{
                                defer.reject(false);
                            }
                        },
                        function(){
                            defer.reject(false);
                        });
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(false);
                });
            return defer.promise;
        };




        scCircuit.deleteCircuit = function(circuit_id){

            var defer = $q.defer();
            if (scCircuit.circuitLookup[circuit_id]) {
                circuitResource.deleteCircuit({id : circuit_id},
                    //success callback
                    function(data){
                        if (data[0] == 's' && data[1] == 'u') {

                            scCircuit.updateCircuitArray().then(
                                function(status){
                                    if (status) {
                                        defer.resolve(true);
                                    }else{
                                        defer.resolve(false);
                                    }
                                },
                                function(){
                                    defer.resolve(false);
                                }
                            );
                        }else{
                            defer.resolve(false);
                        }
                    },
//                error callback
                    function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };

        scCircuit.clearCache = function(){
            circuitCache.removeAll();
        };

        scCircuit.fetchTermlyData = function(params){
            //return $resource('/admin/ghana/districts', {}, {
            //    getTermlyData:{method:'GET', url:'/admin/totals/termly/data', params:{country_id: 1}, isArray:true, cache:false}
            //}).getTermlyData({circuit_id : circuit_id});

            var aCircuitsTermlyDataUrl = '/admin/totals/termly/data';
            var aCircuitsCachedTermlyDataUrl = '/admin/totals/termly/data?country_id=1'+JSON.stringify(params);

            var deferred = $q.defer();

            var cachedData = circuitCache.get(aCircuitsCachedTermlyDataUrl);

            if (cachedData == undefined ) {
                $http.get(aCircuitsTermlyDataUrl , {params : params })
                    .success(function(data){
                        circuitCache.put(aCircuitsCachedTermlyDataUrl, data);
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.resolve(false);
                    })
            }else{
                deferred.resolve(cachedData);
            }
            return deferred.promise;
        };


        return scCircuit;



    }]);


/**
 * Created by sophismay on 6/18/14.
 */

/**
 *  Service for School resource API
 *
 *
 */

schoolMonitor.factory('scSchool', ['scRegion', 'scDistrict', 'scCircuit', '$resource', '$q','scGeneralService',
    '$http','CacheFactory','scNotifier', '$rootScope','DataCollectorHolder','$timeout',
    function(scRegion, scDistrict, scCircuit, $resource, $q, scGeneralService, $http, CacheFactory, scNotifier, $rootScope, DataCollectorHolder, $timeout){
        var schoolResource = $resource('/admin/ghana/schools',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/create_school', params:{}, responseType : 'text'},

            newTeacher :{method:'POST', url:'/admin/school_report_card/form/create/teacher', params:{}, responseType : 'text'},

            transferTeacher :{method:'POST', url:'/admin/school_report_card/teacher/transfer', params:{}, responseType : 'json'},

            retrieveOneSchool : {method : 'GET', url : '/api/school/current/status'},

            editSchool : {method : 'POST', url : '/admin/school/:id/update', params:{id : '@id'}},

            deleteSchool:{method:'POST', url:'/admin/school/:id/delete', params:{id : '@id'}}
        });

        var scSchool = {};

        var schools;

        scSchool.calculateRank =  function(){
            return {};
        };
        var allSchoolsUrl = '/admin/ghana/schools';

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('schoolCache')) {
            var schoolCache = CacheFactory('schoolCache', {
                maxAge:  24 * 60 * 60 * 1000, // 1 day
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    if (key == allSchoolsUrl) {
                        scSchool.updateSchoolArray().then(function (status) {
                            if (status) scSchool.init()
                        })
                    }
                    //$http.get(key).success(function (data) {
                    //    schoolCache.put(key, data);
                    //});
                }
            });
        }

        var calculateCircuitStats = function(school, total_number_of_schools){
            scCircuit.circuitLookup[school.circuit_id].inclusivenessHolder += parseFloat(school.current_state_of_school_inclusiveness);
            scCircuit.circuitLookup[school.circuit_id].safe_protectiveHolder += parseFloat(school.current_state_of_safety_and_protection);
            scCircuit.circuitLookup[school.circuit_id].healthyHolder += parseFloat(school.current_state_of_healthy_level);
            scCircuit.circuitLookup[school.circuit_id].gender_friendlyHolder += parseFloat(school.current_state_of_friendliness_to_boys_girls);
            scCircuit.circuitLookup[school.circuit_id].effective_teaching_learningHolder += parseFloat(school.current_state_of_teaching_and_learning);
            scCircuit.circuitLookup[school.circuit_id].community_involvementHolder +=  parseFloat(school.current_state_of_community_involvement);

            scCircuit.circuitLookup[school.circuit_id].supervisorAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_circuit_supervisor);
            scCircuit.circuitLookup[school.circuit_id].headteacherAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_headteacher);

            scCircuit.circuitLookup[school.circuit_id].inclusiveness = scGeneralService.roundToTwoDP(Math.floor ( scCircuit.circuitLookup[school.circuit_id].inclusivenessHolder /   total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].safe_protective = scGeneralService.roundToTwoDP(Math.floor( scCircuit.circuitLookup[school.circuit_id].safe_protectiveHolder /   total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].healthy = scGeneralService.roundToTwoDP(Math.floor(scCircuit.circuitLookup[school.circuit_id].healthyHolder / total_number_of_schools ) );
            scCircuit.circuitLookup[school.circuit_id].gender_friendly = scGeneralService.roundToTwoDP( Math.floor( scCircuit.circuitLookup[school.circuit_id].gender_friendlyHolder / total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].effective_teaching_learning = scGeneralService.roundToTwoDP(Math.floor( scCircuit.circuitLookup[school.circuit_id].effective_teaching_learningHolder / total_number_of_schools ));
            scCircuit.circuitLookup[school.circuit_id].community_involvement =  scGeneralService.roundToTwoDP(Math.floor( scCircuit.circuitLookup[school.circuit_id].community_involvementHolder / total_number_of_schools ));

            scCircuit.circuitLookup[school.circuit_id].supervisorAttendanceAverage = scGeneralService.roundToTwoDP(Math.floor(scCircuit.circuitLookup[school.circuit_id].supervisorAttendanceHolder / total_number_of_schools));
            scCircuit.circuitLookup[school.circuit_id].headteacherAttendanceAverage =  scGeneralService.roundToTwoDP(Math.floor(scCircuit.circuitLookup[school.circuit_id].headteacherAttendanceHolder /   total_number_of_schools));

            var currentTotal =
                parseFloat(scCircuit.circuitLookup[school.circuit_id].inclusiveness)
                + parseFloat( scCircuit.circuitLookup[school.circuit_id].safe_protective)
                +  parseFloat(scCircuit.circuitLookup[school.circuit_id].healthy)
                +parseFloat( scCircuit.circuitLookup[school.circuit_id].gender_friendly)
                + parseFloat(scCircuit.circuitLookup[school.circuit_id].effective_teaching_learning)
                +parseFloat( scCircuit.circuitLookup[school.circuit_id].community_involvement);

            scCircuit.circuitLookup[school.circuit_id].rating = scGeneralService.roundToTwoDP(Math.floor( (currentTotal / 60) * 100 ));

        };

        var calculateDistrictStats = function(school, total_number_of_schools){
            scDistrict.districtLookup[school.district_id].inclusivenessHolder += parseFloat(school.current_state_of_school_inclusiveness);
            scDistrict.districtLookup[school.district_id].safe_protectiveHolder += parseFloat(school.current_state_of_safety_and_protection);
            scDistrict.districtLookup[school.district_id].healthyHolder += parseFloat(school.current_state_of_healthy_level);
            scDistrict.districtLookup[school.district_id].gender_friendlyHolder += parseFloat(school.current_state_of_friendliness_to_boys_girls);
            scDistrict.districtLookup[school.district_id].effective_teaching_learningHolder += parseFloat(school.current_state_of_teaching_and_learning);
            scDistrict.districtLookup[school.district_id].community_involvementHolder +=  parseFloat(school.current_state_of_community_involvement);

            scDistrict.districtLookup[school.district_id].supervisorAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_circuit_supervisor);
            scDistrict.districtLookup[school.district_id].headteacherAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_headteacher);


            scDistrict.districtLookup[school.district_id].inclusiveness = scGeneralService.roundToTwoDP(Math.floor (scDistrict.districtLookup[school.district_id].inclusivenessHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].safe_protective = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].safe_protectiveHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].healthy = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].healthyHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].gender_friendly = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].gender_friendlyHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].effective_teaching_learning = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].effective_teaching_learningHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].community_involvement =  scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].community_involvementHolder /   total_number_of_schools));

            scDistrict.districtLookup[school.district_id].supervisorAttendanceAverage = scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].supervisorAttendanceHolder /   total_number_of_schools));
            scDistrict.districtLookup[school.district_id].headteacherAttendanceAverage =  scGeneralService.roundToTwoDP(Math.floor(scDistrict.districtLookup[school.district_id].headteacherAttendanceHolder /   total_number_of_schools));


            var currentTotal =
                parseFloat(scDistrict.districtLookup[school.district_id].inclusiveness)
                + parseFloat(scDistrict.districtLookup[school.district_id].safe_protective)
                + parseFloat(scDistrict.districtLookup[school.district_id].healthy)
                +parseFloat(scDistrict.districtLookup[school.district_id].gender_friendly)
                +parseFloat(scDistrict.districtLookup[school.district_id].effective_teaching_learning)
                +parseFloat(scDistrict.districtLookup[school.district_id].community_involvement) ;

            scDistrict.districtLookup[school.district_id].rating = scGeneralService.roundToTwoDP(Math.floor(  ( currentTotal / 60 ) * 100));
        };

        var calculateRegionStats = function(school, total_number_of_schools){
            scRegion.regionLookup[school.region_id].inclusivenessHolder += parseFloat(school.current_state_of_school_inclusiveness);
            scRegion.regionLookup[school.region_id].safe_protectiveHolder += parseFloat(school.current_state_of_safety_and_protection);
            scRegion.regionLookup[school.region_id].healthyHolder += parseFloat(school.current_state_of_healthy_level);
            scRegion.regionLookup[school.region_id].gender_friendlyHolder += parseFloat(school.current_state_of_friendliness_to_boys_girls);
            scRegion.regionLookup[school.region_id].effective_teaching_learningHolder += parseFloat(school.current_state_of_teaching_and_learning);
            scRegion.regionLookup[school.region_id].community_involvementHolder +=  parseFloat(school.current_state_of_community_involvement);

            scRegion.regionLookup[school.region_id].supervisorAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_circuit_supervisor);
            scRegion.regionLookup[school.region_id].headteacherAttendanceHolder +=  parseFloat(school.current_teacher_attendance_by_headteacher);

            scRegion.regionLookup[school.region_id].inclusiveness = scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].inclusivenessHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].safe_protective =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].safe_protectiveHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].healthy =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].healthyHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].gender_friendly =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].gender_friendlyHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].effective_teaching_learning =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].effective_teaching_learningHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].community_involvement =  scGeneralService.roundToTwoDP(Math.floor ( scRegion.regionLookup[school.region_id].community_involvementHolder /   total_number_of_schools));


            scRegion.regionLookup[school.region_id].supervisorAttendanceAverage = scGeneralService.roundToTwoDP(Math.floor(scRegion.regionLookup[school.region_id].supervisorAttendanceHolder /   total_number_of_schools));
            scRegion.regionLookup[school.region_id].headteacherAttendanceAverage =  scGeneralService.roundToTwoDP(Math.floor(scRegion.regionLookup[school.region_id].headteacherAttendanceHolder /   total_number_of_schools));


            var currentTotal =
                parseFloat(scRegion.regionLookup[school.region_id].inclusiveness)
                + parseFloat(scRegion.regionLookup[school.region_id].safe_protective)
                + parseFloat(scRegion.regionLookup[school.region_id].healthy)
                +parseFloat(scRegion.regionLookup[school.region_id].gender_friendly)
                +parseFloat(scRegion.regionLookup[school.region_id].effective_teaching_learning)
                +parseFloat(scRegion.regionLookup[school.region_id].community_involvement) ;

            scRegion.regionLookup[school.region_id].rating = scGeneralService.roundToTwoDP(Math.floor( (currentTotal / 60) * 100  ));

            scRegion.reviewDataExists = true;
        };

        var calculateSchoolStats = function(school){
            var overall =
                parseFloat(school.current_state_of_school_inclusiveness) +
                parseFloat(school.current_state_of_teaching_and_learning) +
                parseFloat(school.current_state_of_healthy_level) +
                parseFloat(school.current_state_of_friendliness_to_boys_girls) +
                parseFloat(school.current_state_of_safety_and_protection) +
                parseFloat(school.current_state_of_community_involvement);

            scSchool.schoolLookup[school.id].rating = scGeneralService.roundToTwoDP(Math.floor (( overall / 60) * 100)) ;


            scSchool.schoolLookup[school.id].current_teacher_attendance_by_headteacher =
                scGeneralService.roundToTwoDP(scSchool.schoolLookup[school.id].current_teacher_attendance_by_headteacher) ;

            scSchool.schoolLookup[school.id].current_teacher_attendance_by_circuit_supervisor =
                scGeneralService.roundToTwoDP(scSchool.schoolLookup[school.id].current_teacher_attendance_by_circuit_supervisor) ;


        };

        scSchool.schools = [];

        var loadingSchools = false;

        scSchool.getAllSchools = function(){
            if (loadingSchools) {
                return;
            }
            loadingSchools = true;
            var defer = $q.defer();

            var cachedData = schoolCache.get(allSchoolsUrl);

            if (!cachedData) {
                scNotifier.notifyInfo('Info', 'Updating schools...');
                schoolResource.query().$promise.then(function(data){
                    if (data) {
                        scSchool.schools = data;
                        schoolCache.put(allSchoolsUrl, data);
                        loadingSchools = false;
                        defer.resolve(true);
                    }
                }, function(){
                    loadingSchools = false;
                    defer.resolve(false)
                });
            }else{
                scSchool.schools = cachedData;
                loadingSchools = false;
                defer.resolve(true);
            }
            return defer.promise;
        };

        scSchool.updateSchoolArray = function(){
            var defer = $q.defer();
            scSchool.schools = [];
            schoolCache.remove(allSchoolsUrl);
            schoolCache.removeAll();

            this.getAllSchools().then(
                function(status){
                    if (status) {
                        $timeout(function () {
                            $rootScope.loading = false;
                            $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                            defer.resolve(true);
                        }, 100);
                    }else{
                        defer.resolve(false);
                    }
                }, function(){
                    $rootScope.loading = false;
                    defer.resolve(false);
                });
            return defer.promise;
        };

        $rootScope.reloadSchools = function () {
            $rootScope.loading = true;
            scSchool.updateSchoolArray();
        };

        scSchool.init = function(){
            scSchool.schoolLookup = {};
            this.schools.sort(function(a, b){
                return a.name.toLowerCase() - b.name.toLowerCase()
            });
            angular.forEach(this.schools, function(school, index){

                //Initiate a counter for how many teachers are in a school. It will be used in the district teacher count view
                school.totalTeachers = 0;
                school.totalMaleTeachers = 0;
                school.totalFemaleTeachers = 0;
                school.teachersInSchoolHolder = [];

                //initiate a temp parameter for the school to know its average_teacher_attendance for certain views
                school.average_teacher_attendance = {};

                //Initiate an array for the teacher attendance average for the district view
                school.teacherAttendanceAverages = [];

                //Initiate an array for the school weekly attendance for the district view
                school.weeklyAttendanceHolder = [];

                //Initiate an array for the school termly information for the district view
                school.termlyDataHolder = [];

                scSchool.schoolLookup[school.id] = school;

                //Assign region name to school object
                scSchool.schools[index].regionName = scRegion.regionLookup[school.region_id].name;

                //Increment the school's region by 1
                scRegion.regionLookup[school.region_id].totalSchools ++;

                //add school to the region lookup object and associate it by its id
                scRegion.regionLookup[school.region_id].allSchools[school.id] = school;

                //add school to the region lookup object
                scRegion.regionLookup[school.region_id].allSchoolsHolder.push(school);

                //Assign district name to school
                scSchool.schools[index].districtName = scDistrict.districtLookup[school.district_id].name;

                //Increment the school's region by 1
                scDistrict.districtLookup[school.district_id].totalSchools ++;

                //add school to the district lookup object and associate it by its id
                scDistrict.districtLookup[school.district_id].allSchools[school.id] = school;

                //Use the school_id in the scDistrict so that we can lookup a district using only the school_Id
                scDistrict.searchSchoolDistrict[school.id] = scDistrict.districtLookup[school.district_id];

                //add school to the district lookup object
                scDistrict.districtLookup[school.district_id].allSchoolsHolder.push(school);

                //Assign the school's circuit name to the school object
                scSchool.schools[index].circuitName = scCircuit.circuitLookup[school.circuit_id].name;

                //Increment the circuits schools by 1
                scCircuit.circuitLookup[school.circuit_id].totalSchools ++ ;

                //add school to the circuit lookup object and associate it by its id
                scCircuit.circuitLookup[school.circuit_id].allSchools[school.id] = school;

                //add school to the circuit lookup object
                scCircuit.circuitLookup[school.circuit_id].allSchoolsHolder.push(school);

                //Lookup data collector and assign school
                if (DataCollectorHolder.lookupDataCollector) {
                    if (DataCollectorHolder.lookupDataCollector[school.headteacher_id]){
                        DataCollectorHolder.lookupDataCollector[school.headteacher_id].school_name = school.name;
                        DataCollectorHolder.lookupDataCollector[school.headteacher_id].school_id = school.id;
                    }
                }



                //Increment the circuits review sections
                calculateSchoolStats(school);

                calculateCircuitStats(school,  scCircuit.circuitLookup[school.circuit_id].totalSchools);

                calculateDistrictStats(school, scDistrict.districtLookup[school.district_id].totalSchools);

                calculateRegionStats(school, scRegion.regionLookup[school.region_id].totalSchools);

            })
        };
//        inclusiveness
//        safe_protective
//        healthy
//        gender_friendly
//        effective_teaching_learning
//        community_involvement

        //The overall rating, add up all the ratings of each category
//       var overall =  school.current_state_of_school_inclusiveness +
//        school.current_state_of_teaching_and_learning +
//        school.current_state_of_healthy_level +
//        school.current_state_of_friendliness_to_boys_girls +
//        current_state_of_safety_and_protection +
//        current_state_of_community_involvement;

//        Now that we have the overall, we can assign it to the school,



// to/*
//          To calculate the the circuits rating, loop through the circuits' schools and
//        for each section, add them up and divide them by the expected total number
//        and multiply by 100 for the circuits rating
//
// */
        scSchool.addSchool= function(formData){
            var defer = $q.defer();
            // process the form
            schoolResource.create(formData,
                //success callback
                function (value) {
                    var status = '';
                    angular.forEach(value, function(key, prop){
                        if (prop != '$promise' && prop != '$resolved' ) status += value[prop]
                    });
                    if ($.trim(status) == 'success') {
                        scSchool.updateSchoolArray().then(function (value) {
                            if (value) {
                                defer.resolve(true);
                            }
                        });
                    }else {
                        scSchool.validationFields = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };

        scSchool.createTeacher= function(formData){
            var defer = $q.defer();
            // process the form
            schoolResource.newTeacher(formData,
                //success callback
                function (value) {
                    var status = '';
                    angular.forEach(value, function(key, prop){
                        if (prop != '$promise' && prop != '$resolved' ) status += value[prop]
                    });
                    if ($.trim(status) == 'success') {
                        //scSchool.updateSchoolArray(); Todo..... Update the teachers in the view
                        defer.resolve(true);
                    }else {
                        scSchool.validationFields = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };



        scSchool.deleteSchool = function(school_id){

            var defer = $q.defer();
            if (scSchool.schoolLookup[school_id]) {
                schoolResource.deleteSchool({id : school_id},
                    //success callback
                    function(value){
                        var  data = '';
                        angular.forEach(value, function(key, prop){
                            if (prop != '$promise' && prop != '$resolved' ) data += value[prop]
                        });
                        if ($.trim(data) == 'success') {

                            scSchool.updateSchoolArray().then(
                                function(status){
                                    if (status) {
                                        defer.resolve(true);
                                    }else{
                                        defer.resolve(false);
                                    }
                                },
                                function(){
                                    defer.resolve(false);
                                }
                            );
                        }else{
                            defer.resolve(false);
                        }
                    },
//                error callback
                    function(httpResponse){
                        defer.resolve(httpResponse)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };



//    Retrieve a single school to view or edit
        scSchool.retrieveOneSchool = function(school_id){
            var defer = $q.defer();
            $http.post('/api/school/current/status', {school_id : school_id})
                .success(function(data){
                    if (data.status === 200) {
                        defer.resolve(data.schools_current_rating);
                    }
                })
                .error(function () {
                    defer.resolve(false);
                });
            return defer.promise;
        };



        scSchool.editSchool = function(formData){
            var defer = $q.defer();
            schoolResource.editSchool(formData,
                //success callback
                function (value, responseHeaders) {
                    var  data = '';
                    angular.forEach(value, function(key, prop){
                        if (prop != '$promise' && prop != '$resolved' ) data += value[prop]
                    });
                    if ($.trim(data) == 'success') {
                        //If successful, requery the new regions and assign them to the parent regions variable
                        scSchool.updateSchoolArray().then(function (value) {
                            if (value) {
                                defer.resolve(true);
                            }
                        });
                    } else {
                        scSchool.validationFields = value;
                        defer.resolve(false)
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };

        scSchool.getImages = function(school_id){
            var defer = $q.defer();

            var aSchoolsImagesUrl = '/admin/schools/images?country_id=1&school_id='+school_id;

            var cachedSchoolImageData = schoolCache.get(aSchoolsImagesUrl);

            if (!cachedSchoolImageData) {
                $http.get(aSchoolsImagesUrl)
                    .success(function(data){
                        schoolCache.put(aSchoolsImagesUrl, data);
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.resolve(false);
                    });
            }else{
                defer.resolve(cachedSchoolImageData)
            }
            return defer.promise;
        };

        scSchool.fetchItinerary = function(school_district_id){
            var defer = $q.defer();

            var aDistrictsItineraryUrl = '/admin/itinerary?country_id=1&district_id='+school_district_id;

            var cachedDistrictItineraryData = schoolCache.get(aDistrictsItineraryUrl);

            if (!cachedDistrictItineraryData) {
                $http.get(aDistrictsItineraryUrl)
                    .success(function(data){
                        schoolCache.put(aDistrictsItineraryUrl, data);
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.resolve(false);
                    });
            }else{
                defer.resolve(cachedDistrictItineraryData)
            }
            return defer.promise;

        };

        scSchool.fetchWeeklyData = function(school_id){

            var defer = $q.defer();

            var aSchoolsWeeklyDataUrl = '/admin/totals/weekly/data?country_id=1&school_id='+school_id;

            var cachedSchoolWeeklyData = schoolCache.get(aSchoolsWeeklyDataUrl);

            if (!cachedSchoolWeeklyData) {
                $http.get(aSchoolsWeeklyDataUrl)
                    .success(function(data){
                        schoolCache.put(aSchoolsWeeklyDataUrl, data);
                        defer.resolve(data);
                    })
                    .error(function () {
                        defer.resolve(false);
                    });
            }else{
                defer.resolve(cachedSchoolWeeklyData)
            }
            return defer.promise;

        };


        scSchool.deleteSubmittedData = function(type, id_of_selected){
            var defer = $q.defer();

            var data_to_delete = {
                id : id_of_selected,
                data_type : type
            };
            $http.post('/admin/delete/enrolment/attendance', data_to_delete)
                .success(function (returnedData) {
                    if (returnedData) {
                        defer.resolve(true)
                    }else{
                        defer.resolve(false)
                    }
                })
                .error(function () {
                    defer.resolve(false)
                });


            return defer.promise;
        };


        scSchool.teacherAttendanceClassSubmittedData = function(type, id_of_selected){
            var defer = $q.defer();

            var data_to_delete = {
                id : id_of_selected,
                data_type : type
            };
            $http.post('/admin/delete/teacher/data/submitted', data_to_delete)
                .success(function (returnedData) {
                    if (returnedData) {
                        defer.resolve(true)
                    }else{
                        defer.resolve(false)
                    }
                })
                .error(function () {
                    defer.resolve(false)
                });


            return defer.promise;
        };


        scSchool.fetchAvailableWeeklySubmittedData = function(school_id, category, data_submission_interval){
            var defer = $q.defer();

            var aSchoolsWeeklyDataUrl = '/admin/retrieve/weekly/submissions/under/school?school_id='+school_id+'&category='+category+'&data_submission_interval='+data_submission_interval;

            // var cachedSchoolWeeklyData = schoolCache.get(aSchoolsWeeklyDataUrl);

            /*do not use cache for this so it always reloads, perhaps the cs submitted a comment*/
            $http.get(aSchoolsWeeklyDataUrl)
                .success(function (data) {
                    // schoolCache.put(aSchoolsWeeklyDataUrl, data);
                    defer.resolve(data);
                })
                .error(function () {
                    defer.resolve(false);
                });/*else{
                defer.resolve(cachedSchoolWeeklyData)
            }*/
            return defer.promise;

        };

        scSchool.clearSchoolCache = function () {
            schoolCache.removeAll();
        };

        scSchool.allSchoolTeachersData = function (params) {
            return $http.get('/admin/school_report_card/school/teachers', {params : params});
        };

        scSchool.transferTeacher= function(formData){
            var defer = $q.defer();
            // process the form
            schoolResource.transferTeacher(formData,
                //success callback
                function (value) {
                    if ($.trim(value.status) == '200') {
                        //scSchool.updateSchoolArray(); Todo..... Update the teachers in the view
                        defer.resolve(true);
                    }else {
                        scSchool.validationFields = value;
                        defer.resolve(false);
                    }
                },
                //error callback
                function (httpResponse) {
                    defer.resolve(httpResponse);
                });
            return defer.promise;
        };


        scSchool.editTeacher = function (params) {
            return $http.post('/admin/school_report_card/teacher/edit', params);
        };

        scSchool.deleteTeacher = function (params) {
            return $http.post('/admin/school_report_card/teacher/delete', params);
        };

        scSchool.updateWeeklyTableForEnrollment = function (params) {
            return $http.post('/admin/update/weekly/table/enrollment', params);
        };

        scSchool.updateWeeklyTableForAttendance = function (params) {
            return $http.post('/admin/update/weekly/table/attendance', params);
        };

        scSchool.updateWeeklyTableForTeacherAttendance = function (params) {
            return $http.post('/admin/update/weekly/table/for/teachers/attendance', params);
        };

        scSchool.updateTermlyTextbookData = function (params) {
            return $http.post('/admin/school_report_card/school/class_textbooks/update/totals', params);
        };

        scSchool.updateTermlyPupilPerformanceData = function (params) {
            return $http.post('/admin/school_report_card/school/pupil_performance/update/totals', params);
        };

        return scSchool;


    }]);


/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Region resource
 *
 */

//Usage
//$resource(url, [paramDefaults], [actions], options);

//action parameters
//action – {string} – The name of action. This name becomes the name of the method on your resource object.
//    method – {string} – HTTP request method. Valid methods are: GET, POST, PUT, DELETE, and JSONP.
//    params – {Object=} – Optional set of pre-bound parameters for this action. If any of the parameter value is a function, it will be executed every time when a param value needs to be obtained for a request (unless the param was overridden).
//url – {string} – action specific url override. The url templating is supported just like for the resource-level urls.
//    isArray – {boolean=} – If true then the returned object for this action is an array, see returns section.
//    cache – {boolean|Cache} – If true, a default $http cache will be used to cache the GET request, otherwise if a cache instance built with $cacheFactory, this cache will be used for caching.
//                                                                                                                                                                                            timeout – {number|Promise} – timeout in milliseconds, or promise that should abort the request when resolved.
//    responseType - {string} - see requestType.
//    interceptor - {Object=} - The interceptor object has two optional methods - response and responseError. Both response and responseError interceptors get called with http response object. See $http interceptors.

//options
//stripTrailingSlashes – {boolean} – If true then the trailing slashes from any calculated URL will be stripped. (Defaults to true.)

schoolMonitor.service('scViewServices',[])
    .factory('Region', function($resource){
        return $resource('/admin/ghana/regions',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/region/create', params:{}},
            retrieveOneRegion:{method:'GET', url:'/admin/region/:id/edit', params:{id : '@id'}},
            editRegion:{method:'POST', url:'/admin/region/:id/update', params:{id : '@id'}},
            deleteRegion:{method:'POST', url:'/admin/region/:id/delete', params:{id : '@id'}}
        });
    })

    .factory('District', function($resource){
        return $resource('/admin/ghana/districts',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/district/create', params:{}},
            createItinerary:{method:'POST', url:'/admin/district/itinerary', params:{}},
            getItinerary:{method:'GET', url:'/admin/district/itinerary/:id', params:{}},
            editAnItinerary:{method:'POST', url:'/admin/district/itinerary/:id/update', params:{id : '@id'}},
            deleteAnItinerary:{method:'POST', url:'/admin/district/itinerary/:id/delete', params:{id : '@id'}},
            retrieveOneDistrict:{method:'GET', url:'/admin/district/:id/edit', params:{id : '@id'}},
            editDistrict:{method:'POST', url:'/admin/district/:id/update', params:{id : '@id'}},
            deleteDistrict:{method:'POST', url:'/admin/district/:id/delete', params:{id : '@id'}}
        });
    })

    .factory('Circuit', function($resource){
        return $resource('/admin/ghana/circuits',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/circuit/create', params:{}},
            retrieveOneCircuit:{method:'GET', url:'/admin/circuit/:id/edit', params:{id : '@id'}},
            editCircuit:{method:'POST', url:'/admin/circuit/:id/update', params:{id : '@id'}},
            deleteCircuit:{method:'POST', url:'/admin/circuit/:id/delete', params:{id : '@id'}}

        });
    })

    .factory('School', function($resource){
        return $resource('/admin/ghana/schools',{},{
            query:{isArray:true,cache:false},
            create:{method:'POST', url:'/admin/create_school', params:{}},
            retrieveOneSchool : {method : 'GET', url : '/admin/school/:id/edit', params:{id : '@id'}},

            editSchool : {method : 'POST', url : '/admin/school/:id/update', params:{id : '@id'}},

            deleteSchool:{method:'POST', url:'/admin/school/:id/delete', params:{id : '@id'}},

            //<-- START of School Report Card  Methods-->

            allEnrollmentData : {method : 'GET', url : '/admin/school_report_card/enrollment',
                params:{ country_id : 1}, isArray:true,  cache : false},

            allSpecialEnrollmentData : {method : 'GET', url : '/admin/school_report_card/special_enrollment',
                params:{ country_id : 1}, isArray:true, cache : false },

            allAttendanceData : {method : 'GET', url : '/admin/school_report_card/attendance',
                params:{ country_id : 1}, isArray:true,  cache : false},

            allSpecialAttendanceData : {method : 'GET', url : '/admin/school_report_card/special_students_attendance',
                params:{ country_id : 1}, isArray:true,  cache : false},

            allRecordPerformanceData : {method : 'GET', url : '/admin/school_report_card/records_performance',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolTeachersPunctuality_LessonPlan : {method : 'GET', url : '/admin/school_report_card/school/punctuality_lesson_plan',
                cache : false, params:{ country_id : 1}, isArray:true},

            allSchoolTeachersClassManagement : {method : 'GET', url : '/admin/school_report_card/school/teacher_class_management',
                cache : false, params:{ country_id : 1}, isArray:true},

            allSchoolTeachersUnitsCovered : {method : 'GET', url : '/admin/school_report_card/school/units/covered',
                cache : false, params:{ country_id : 1}, isArray:true},

            allSchoolSanitation : {method : 'GET', url : '/admin/school_report_card/sanitation',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolRecreation_Equipment : {method : 'GET', url : '/admin/school_report_card/recreation_equipment',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolSecurity : {method : 'GET', url : '/admin/school_report_card/security',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolStructure : {method : 'GET', url : '/admin/school_report_card/structure',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolFurniture : {method : 'GET', url : '/admin/school_report_card/furniture',
                params:{ country_id : 1}, isArray:true, cache : true} ,

            allSchoolCommunityInvolvement : {method : 'GET', url : '/admin/school_report_card/community_relations',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolMeetingsHeld : {method : 'GET', url : '/admin/school_report_card/school/meetings',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolTextbook : {method : 'GET', url : '/admin/school_report_card/school/textbooks',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolPupilPerformance : {method : 'GET', url : '/admin/school_report_card/pupils/performance',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSupportTypes : {method : 'GET', url : '/admin/school_report_card/school/support_types',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolGrantCapitationPayments : {method : 'GET', url : '/admin/school_report_card/grant_capitation_payments',
                params:{ country_id : 1}, isArray:true, cache : true},

            allSchoolGeneralSituation : {method : 'GET', url : '/admin/school_report_card/school/general_situation',
                params:{ country_id : 1}, isArray:true, cache : true},


            teacherAttendanceAveragePercentage : {method : 'GET', url : '/admin/average/teacher/attendance/percentage',
                params:{ country_id : 1}, isArray:false},




            //END of School Report Card  Methods


            //-- START of  School Review Methods-->
            schoolInclusiveData : {method : 'GET', url : '/admin/school_review/inclusiveness',
                params:{ country_id : 1}, isArray:true},

            schoolEffectiveTeachingData : {method : 'GET', url : '/admin/school_review/effective_teaching',
                params:{ country_id : 1}, isArray:true},

            schoolHealthyData : {method : 'GET', url : '/admin/school_review/healthy_level',
                params:{ country_id : 1}, isArray:true},

            schoolSafeProtectiveData : {method : 'GET', url : '/admin/school_review/safe_protective',
                params:{ country_id : 1}, isArray:true},

            schoolFriendlyData : {method : 'GET', url : '/admin/school_review/friendly_schools',
                params:{ country_id : 1}, isArray:true},

            schoolCommunityInvolvementData : {method : 'GET', url : '/admin/school_review/community_involvement',
                params:{ country_id : 1}, isArray:true},

            //<-- END of  School Review Methods-->


            //<-- START of  School Visit  Methods-->

            allSchoolVisitsData : {method : 'GET', url : '/admin/schools/school_visits',
                params:{ country_id : 1}, isArray:true}
        });
    })

    .factory('Report', function($resource){
        return $resource('/admin',{},{
            totalsWeekly:{method:'GET', url:'/admin/totals/weekly/data', cache : false, params:{country_id : 1 }, isArray: true},
            totalsTermly:{method:'GET', url:'/admin/totals/termly/data', cache : false, params:{country_id : 1 }, isArray: true},

            /*This route has been taken care of in the weekly above*/
            aggregatedTotals:{method:'GET', url:'/admin/retrieve/parametric/aggregate/data', params:{country_id : 1 }, isArray: false}
        });
    });
/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.factory('scCircuitSupervisor', ['$resource' , '$rootScope', '$q', 'CacheFactory','$http','scNotifier',
    'scRegion','scDistrict','scCircuit', 'scSchool','DataCollectorHolder','$timeout',
    function($resource, $rootScope,$q, CacheFactory, $http, scNotifier
        ,scRegion, scDistrict, scCircuit, scSchool, DataCollectorHolder, $timeout){

        var scSupervisor = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('supervisorCache')) {
            var supervisorCache = CacheFactory('supervisorCache', {
                maxAge: 15 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        scSupervisor.supervisors = [];

        var url = '/admin/ghana/data_collectors/circuit_supervisor', loadingSupervisors = false;

        scSupervisor.getAllSupervisors = function(){
            if (loadingSupervisors) {
                return;
            }
            loadingSupervisors = true;
            var deferred = $q.defer();

            var cachedData = supervisorCache.get(url);

            if (cachedData == undefined ) {
                scNotifier.notifyInfo('Info', 'Updating Circuit Supervisors...');

                $http.get(url)
                    .success(function(data){
                        supervisorCache.put(url, data);
                        scSupervisor.supervisors = data;
                        loadingSupervisors = false;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        loadingSupervisors = false;
                        deferred.resolve(false);
                    })
            }else{
                scSupervisor.supervisors = cachedData;
                loadingSupervisors = false;
                deferred.resolve(cachedData);
            }
            return deferred.promise;

        };

        scSupervisor.updateSupervisorArray = function(){
            var defer = $q.defer();
            supervisorCache.remove(url);
            scSupervisor.supervisors = [];
            scSupervisor.getAllSupervisors().then(function () {
                sortSupervisors();

                $timeout(function () {
                    $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                    $rootScope.loading = false;
                    defer.resolve(true);
                }, 100);
            });

            return defer.promise;
        };
        
        $rootScope.reloadCircuitSupervisors = function () {
            $rootScope.loading = true;
            scSupervisor.updateSupervisorArray();
        };

        function sortSupervisors() {
            scSupervisor.supervisors.sort(function (a, b) {
                if (a.first_name > b.first_name) {
                    return 1;
                } else if (a.first_name < b.first_name) {
                    return -1
                } else {
                    return 0
                }
            });
        }

        scSupervisor.init = function(){

            scSupervisor.supervisorLookup = {};

            angular.forEach(this.supervisors, function(supervisor, index){

                //assign the property name to each supervisor
                //This formats the names of the supervisors to their full name
                supervisor.name = supervisor.first_name + ' ' + supervisor.last_name;
                supervisor.fullName = supervisor.first_name + ' ' + supervisor.last_name;

                //Increment Circuit circuit supervisor count
                if (scCircuit.circuitLookup[supervisor.circuit_id] !== undefined) {
                    scCircuit.circuitLookup[supervisor.circuit_id].circuitSupervisor = supervisor;
                }else{
                    console.log('The circuit with id of ' + supervisor.circuit_id + ' does not exist.');
                }

                //Increment District circuit supervisor count and push him into array
                if ( scDistrict.districtLookup[supervisor.district_id] !== undefined) {
                    scDistrict.districtLookup[supervisor.district_id].circuit_supervisors += 1;
                    scDistrict.districtLookup[supervisor.district_id].circuit_supervisors_holder.push(supervisor);
                }else{
                    console.log('The district with id of ' + supervisor.district_id + ' does not exist.');
                }

                //Increment Region circuit supervisor count and push him into array
                if (scRegion.regionLookup[supervisor.region_id] !== undefined) {
                    scRegion.regionLookup[supervisor.region_id].circuit_supervisors += 1;
                    scRegion.regionLookup[supervisor.region_id].circuit_supervisors_holder.push(supervisor);

                }else{
                    console.log('The region with id of ' + supervisor.region_id + ' does not exist.');
                }

                //Assign the data collector to a spot in the global data collector variable
                // via his id for easy retrieving
                DataCollectorHolder.dataCollectors.push(supervisor);
                DataCollectorHolder.lookupDataCollector[supervisor.id] = supervisor;
                scSupervisor.supervisorLookup[supervisor.id] = supervisor;
                //scSchool.schoolLookup[school.id].supervisor_name = supervisor.name;

                //$scope.lookupDataCollector[supervisor.id].school_name = school.name;

            });

            sortSupervisors();
        };


        scSupervisor.addDataCollector = function(formData){
            var defer = $q.defer();
            // process the form
            $http.post('/admin/data_collectors', formData).
                //success callback
                success(function (value) {
                    if (value[0] == 's' && value[1] == 'u') {
                        scSupervisor.updateSupervisorArray()
                            .then(function (data) {
                                if (data) defer.resolve('success');
                            });
                    }else {
                        defer.resolve(value);
                    }
                })
                //error callback
                .error(function (httpResponse) {
                    defer.resolve(false);
                });


            return defer.promise;
        };

        scSupervisor.deleteSupervisor = function(supervisorId){

            var defer = $q.defer();
            if (scSupervisor.supervisorLookup[supervisorId]) {
                var deleteUrl = '/admin/data_collector/'+ supervisorId +'/delete';
                $http.post(deleteUrl, {})
                    //success callback
                    .success(function(data){
                        if (data[0] == 's' && data[1] == 'u') {
                            scSupervisor.updateSupervisorArray()
                                .then(function (data) {
                                    if (data) defer.resolve(true);
                                });
                        }else  defer.resolve(false);
                    })
//                error callback
                    .error(function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };


        //After posting the form for editing, this gets called
        scSupervisor.editSupervisor = function(formData){
            var defer = $q.defer();
            var editUrl = '/admin/data_collector/' + formData.id +'/update';
            $http.post(editUrl, formData)
                //success callback
                .success(function (value, responseHeaders) {
                    if (value) {
                        //If successful, requery the new supervisors and assign them to the model service variable
                        scSupervisor.updateSupervisorArray()
                            .then(function (data) {
                                if (data) {
                                    defer.resolve('success');
                                }
                            });
                    } else {
                        defer.resolve(false)
                    }
                })
                //error callback
                .error(function (httpResponse) {
                    defer.reject(false);
                });
            return defer.promise;
        };


        return scSupervisor;



    }]);


/**
 * Created by sophismay on 6/25/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.factory('scHeadTeacher', ['$resource' , '$rootScope', '$q', 'CacheFactory','$http','scNotifier',
    'scRegion','scDistrict','scCircuit', 'scSchool','DataCollectorHolder','$timeout',
    function($resource, $rootScope,$q, CacheFactory, $http, scNotifier,
             scRegion, scDistrict, scCircuit, scSchool, DataCollectorHolder, $timeout){

        var scHeadTeacher = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('headteacherCache')) {
            var headteacherCache = CacheFactory('headteacherCache', {
                maxAge: 15 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive'
                //onExpire: function (key, value) {
                //    $http.get(key).success(function (data) {
                //        profileCache.put(key, data);
                //    });
                //}
            });
        }

        scHeadTeacher.headteachers = [];

        var url = '/admin/ghana/data_collectors/head_teacher', loadingSchools = false;

        scHeadTeacher.getAllHeadteachers = function(){
            if (loadingSchools) {
                return;
            }
            loadingSchools = true;
            var deferred = $q.defer();

            var cachedData = headteacherCache.get(url);

            if (cachedData == undefined ) {
                scNotifier.notifyInfo('Info', 'Updating Head Teachers...');

                $http.get(url)
                    .success(function(data){
                        headteacherCache.put(url, data);
                        scHeadTeacher.headteachers = data;
                        loadingSchools = false;
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        loadingSchools = false;
                        deferred.resolve(false);
                    })
            }else{
                scHeadTeacher.headteachers = cachedData;
                loadingSchools = false;
                deferred.resolve(cachedData);
            }
            return deferred.promise;

        };

        scHeadTeacher.updateHeadTeacherArray = function(){
            var defer = $q.defer();
            headteacherCache.remove(url);
            scHeadTeacher.headteachers = [];
            this.getAllHeadteachers().then(function () {
                sortHeadteachers();
                $timeout(function () {
                    $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : 6});

                    $rootScope.loading = false;
                    defer.resolve(true);
                }, 100);

            });
            return defer.promise;
        };

        $rootScope.reloadHeadTeachers = function () {
            $rootScope.loading = true;
            scHeadTeacher.updateHeadTeacherArray();
        };

        function sortHeadteachers() {
            scHeadTeacher.headteachers.sort(function (a, b) {
                if (a.first_name > b.first_name) {
                    return 1;
                } else if (a.first_name < b.first_name) {
                    return -1
                } else {
                    return 0
                }
            });
        }

        scHeadTeacher.init = function(){

            scHeadTeacher.headteacherLookup = {};

            angular.forEach(this.headteachers, function(headteacher, index){

                //assign the property name to each supervisor
                //This formats the names of the teachers to their full name
                headteacher.name = headteacher.first_name + ' ' + headteacher.last_name;
                headteacher.fullName = headteacher.first_name + ' ' + headteacher.last_name;

                //Increment Circuit circuit supervisor count
                if (scCircuit.circuitLookup[headteacher.circuit_id] !== undefined) {
                    scCircuit.circuitLookup[headteacher.circuit_id].head_teachers += 1;
                    scCircuit.circuitLookup[headteacher.circuit_id].head_teachers_holder.push(headteacher);
                }else{
                    console.log('The circuit with id of ' + headteacher.circuit_id + ' does not exist.');
                }

                //Increment District circuit supervisor count and push him into array
                if ( scDistrict.districtLookup[headteacher.district_id] !== undefined) {
                    scDistrict.districtLookup[headteacher.district_id].head_teachers += 1;
                    scDistrict.districtLookup[headteacher.district_id].head_teachers_holder.push(headteacher);
                }else{
                    console.log('The district with id of ' + headteacher.district_id + ' does not exist.');
                }

                //Increment Region circuit supervisor count and push him into array
                if (scRegion.regionLookup[headteacher.region_id] !== undefined) {
                    scRegion.regionLookup[headteacher.region_id].head_teachers += 1;
                    scRegion.regionLookup[headteacher.region_id].head_teachers_holder.push(headteacher);

                }else{
                    console.log('The region with id of ' + headteacher.region_id + ' does not exist.');
                }

                //Assign the data collector to a spot in the global data collector variable
                // via his id for easy retrieving
                DataCollectorHolder.dataCollectors.push(headteacher);
                DataCollectorHolder.lookupDataCollector[headteacher.id] = headteacher;
                scHeadTeacher.headteacherLookup[headteacher.id] = headteacher;

                //$rootScope.lookupDataCollector[headteacher.id].school_name = school.name;
                //$scope.lookupDataCollector[supervisor.id] = supervisor;
                //scSchool.schoolLookup[school.id].supervisor_name = supervisor.name;


            });

            sortHeadteachers();
        };


        scHeadTeacher.addDataCollector = function(formData){
            var defer = $q.defer();
            // process the form
            $http.post('/admin/data_collectors', formData).
            //success callback
            success(function (value) {
                if (value[0] == 's' && value[1] == 'u') {
                    scHeadTeacher.updateHeadTeacherArray()
                        .then(function (data) {
                            if (data) defer.resolve('success');
                        });
                }else {
                    defer.resolve(value);
                }
            })
            //error callback
                .error(function (httpResponse) {
                    defer.resolve(false);
                });


            return defer.promise;
        };


        scHeadTeacher.deleteHeadTeacher = function(headteacherID){

            var defer = $q.defer();
            if (scHeadTeacher.headteacherLookup[headteacherID]) {
                var deleteUrl = '/admin/data_collector/'+ headteacherID +'/delete';
                $http.post(deleteUrl, {})
                //success callback
                    .success(function(data){
                        if (data[0] == 's' && data[1] == 'u') {
                            scHeadTeacher.updateHeadTeacherArray()
                                .then(function (data) {
                                    if (data) defer.resolve(true);
                                });
                        }else  defer.resolve(false);
                    })
                    //                error callback
                    .error(function(){
                        defer.resolve(false)
                    });
            }else{
                defer.resolve(false);
            }
            return defer.promise;
        };


        //After posting the form for editing, this gets called
        scHeadTeacher.editHeadTeacher = function(formData){
            var defer = $q.defer();
            var editUrl = '/admin/data_collector/' + formData.id +'/update';
            $http.post(editUrl, formData)
            //success callback
                .success(function (value) {
                    if (value) {
                        //If successful, requery the new head teachers and assign them to the model service variable
                        scHeadTeacher.updateHeadTeacherArray()
                            .then(function (data) {
                                if (data) {
                                    defer.resolve('success');
                                }
                            });
                    } else {
                        defer.resolve(false)
                    }
                })
                //error callback
                .error(function () {
                    defer.reject(false);
                });
            return defer.promise;
        };


        scHeadTeacher.sendNotification = function(){

        };


        return scHeadTeacher;



    }]);


/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Region resource
 *
 */

schoolMonitor.service('scStakeholderService',[])
    .factory('DataCollector', ['$resource', function($resource){
//        return $resource('/ghana/data_collectors');
        return $resource('/admin/ghana/data_collectors/headteacher',{},{
            create:{method:'POST', url:'/admin/data_collectors', params:{}},
            retrieveOneDataCollector:{method:'GET', url:'/admin/data_collector/:id/edit', params:{id : '@id'}},
            resendLoginCredentials:{method:'POST',
                url:'/admin/resend/data_collectors/login/details/:firstName/:identityCode/:number',
                params:{id : '@id', firstName : '@firstName', identityCode : '@identityCode', number : '@number'}},
            retrieveMessagesSentByDataCollectors:{method:'GET', url:'/admin/retrieveMessagesSentByDataCollectors', params:{id : '@id'}, isArray: true},
            retrieveDataCollectorSMSMessages :{method:'GET', url:'/admin/retrieve/sms/messages',
                params : {id : '@id'}
            }

        });
    }])
    .factory('DataCollectorHolder', ['$resource', function($resource){
        var datacollectors = {};

        datacollectors.lookupDataCollector = {};
        datacollectors.dataCollectors = [];

        return datacollectors;
    }]);
/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Administrative and Configurations resource
 *
 */
//Usage
//$resource(url, [paramDefaults], [actions], options);

//action parameters
//action – {string} – The name of action. This name becomes the name of the method on your resource object.
//    method – {string} – HTTP request method. Valid methods are: GET, POST, PUT, DELETE, and JSONP.
//    params – {Object=} – Optional set of pre-bound parameters for this action. If any of the parameter value is a function, it will be executed every time when a param value needs to be obtained for a request (unless the param was overridden).
//url – {string} – action specific url override. The url templating is supported just like for the resource-level urls.
//    isArray – {boolean=} – If true then the returned object for this action is an array, see returns section.
//    cache – {boolean|Cache} – If true, a default $http cache will be used to cache the GET request, otherwise if a cache instance built with $cacheFactory, this cache will be used for caching.
//                                                                                                                                                                                            timeout – {number|Promise} – timeout in milliseconds, or promise that should abort the request when resolved.
//    responseType - {string} - see requestType.
//    interceptor - {Object=} - The interceptor object has two optional methods - response and responseError. Both response and responseError interceptors get called with http response object. See $http interceptors.

//options
//stripTrailingSlashes – {boolean} – If true then the trailing slashes from any calculated URL will be stripped. (Defaults to true.)


schoolMonitor.service('scAdminServices',[])
    .factory('SMSService',['$http','$q','CacheFactory','$rootScope', function($http, $q, CacheFactory , $rootScope){

        var SMSS = {};

        SMSS.sendMessageToDataCollectors = function (postParams) {
            return $http.post('/admin/sendMessageAsNotification', postParams)
        };

        SMSS.sendSmsFromDashboard = function (messageToSendObject) {
            var dataToSend = {
                from : $rootScope.currentUser.type || "admin",
                sender_db_id : $rootScope.currentUser.id,
                recipient_type : messageToSendObject.recipient.type,
                recipient_db_id : messageToSendObject.recipient.id,
                message : messageToSendObject.message,
                recipient_number : messageToSendObject.recipient.phone_number,
                // recipient_number : "+233277482171",
                region_id : messageToSendObject.recipient.region_id,
                district_id : messageToSendObject.recipient.district_id,
                circuit_id : messageToSendObject.recipient.circuit_id,
                school_id : messageToSendObject.recipient.school_id || 0
            };
            return $http.post('/admin/send/sms/message/from/dashboard', dataToSend)
        };

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('publicSMSCache')) {
            var publicSMSCache = CacheFactory('publicSMSCache', {
                maxAge: 24 * 60 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        publicSMSCache.put(key, data);
                    });
                }
            });
        }

        SMSS.getSMSes = function (param_name, id) {

            var deferred = $q.defer();

            var url = '/admin/general_public/sms?'+param_name+'_id='+id;

            var cachedData = publicSMSCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        publicSMSCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };



        return SMSS;

    }])
    .factory('Email', ['$resource', function($resource){
        return $resource('/admin/general_public/sms',{country_id : 1},{
            query:{isArray:true,cache:true},
            sendAnEmail:{method: 'POST', url:'/admin/send/Email'},
            removeFileUploaded:{method: 'POST', url:'/admin/remove/file/uploaded'}
        });
    }])
    .factory('RecentSubmissionService',['$http','$q','CacheFactory', function($http, $q, CacheFactory ){

        var RSS = {};

        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('recentSubmissionCache')) {
            var recentSubmissionCache = CacheFactory('recentSubmissionCache', {
                maxAge: 15 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        recentSubmissionCache.put(key, data);
                    });
                }
            });
        }

        RSS.getAllRecentSubmissions = function (param_name, id) {

            var deferred = $q.defer();

            var url = '/admin/recent/data/submission?'+param_name+'_id='+id;

            var cachedData = recentSubmissionCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        recentSubmissionCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        return RSS;

    }])
    .factory('Timelines',['$http','$q','CacheFactory', function($http, $q, CacheFactory ){

        var submissionTimeline = {};

        var recentTimelineCache;
        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('recentTimelineCache')) {
            recentTimelineCache = CacheFactory('recentTimelineCache', {
                maxAge: 15 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        recentTimelineCache.put(key, data);
                    });
                }
            });
        }

        submissionTimeline.getAvailableTimeline = function (params, id) {

            var deferred = $q.defer();

            var urlCache = '/admin/school_report_card/submissions/timelines' + JSON.stringify(params);
            var url = '/admin/school_report_card/submissions/timelines';

            var cachedData = recentTimelineCache.get(urlCache);
            if (!cachedData) {
                $http.get(url, {params : params})
                    .success(function (data) {
                        recentTimelineCache.put(urlCache, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        return submissionTimeline;

    }])
    .factory('UserLogs',['$http','$q','CacheFactory', function($http, $q, CacheFactory ){

        var userLog = {};

        var userLogCache;
        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('userLogCache')) {
            userLogCache = CacheFactory('userLogCache', {
                maxAge: 15 * 60 * 60 * 1000, // 15minutes
                deleteOnExpire: 'aggressive',
                onExpire: function (key, value) {
                    $http.get(key).success(function (data) {
                        userLogCache.put(key, data);
                    });
                }
            });
        }

        userLog.getCreatedActions = function () {

            var deferred = $q.defer();

            var url = '/admin/created/actions';

            var cachedData = userLogCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        userLogCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        userLog.getEditedActions = function () {

            var deferred = $q.defer();

            var url = '/admin/edited/actions';

            var cachedData = userLogCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        userLogCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };


        userLog.getDeletedActions = function () {

            var deferred = $q.defer();

            var url = '/admin/deleted/actions';

            var cachedData = userLogCache.get(url);

            if (!cachedData) {
                $http.get(url)
                    .success(function (data) {
                        userLogCache.put(url, data);
                        deferred.resolve(data);
                    })
                    .error(function () {
                        deferred.resolve(false)
                    });
            }else{
                deferred.resolve(cachedData);
            }

            return deferred.promise;
        };

        userLog.clearCache = function () {
            var defer = $q.defer();
            userLogCache.removeAll();
            var temp = {};
            userLog.getCreatedActions()
                .then(function (createdData) {
                    temp.created = createdData;

                    userLog.getEditedActions()
                        .then(function (editedData) {
                            temp.edited = editedData;

                            userLog.getDeletedActions()
                                .then(function (deletedData) {
                                    temp.deleted = deletedData;

                                    defer.resolve(temp);
                                })
                        })
                });
            return defer.promise;
        };

        return userLog;

    }]);

/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Global Functions that will need to be called regularly from several places
 *
 */

/*Don't delete. It is used in the app somewhere*/
schoolMonitor.service('scGeneralService',['$rootScope', '$http','scUser', function ($rootScope, $http, scUser) {
    this.roundToTwoDP = function (value, decimals){
        if (!decimals) {
            decimals = 2;
        }
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    };




    this.report_image_uris = {

        teacher_to_pupil_image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABQ7SURBVHic7Z17dBRVnse/t7ob8uBhAHmTLiQgFCSBpDuiCEkQ2fGBuoy6jswJ+JjVUVcdHXFcZfF1xvE5HN3d0YNzHOKwuo7r8YXKgCQBw0C6E0OAJiEkdDo8EgLkJST06+4f/aCqUtXp7lR1d0h9zsk5Xbeqfvemf7/+3Xt/91e3CKUUfAghJH12zv2EkFsA5AKYBHU4CaCSUvqlo7bqfSpuSISwnOlaAG8CyFOkdZc+FQCeJPzvPX12zmSGYTYDKIhxY0q9Xu8qR23ViWgFsJxpLzTlR0oFE/hECCFxUj4AFDAMs5kQQgYgQ1N+5OQFDSB9ds79iI/yAxT426ARQ/SBD/4+P8ioJNS8/guDLpclc9WouNJODz71kcvT1YssURs2KiHftvsrJcRccnDXrBAcM7zPufwTaiofAHJZMvf1Xxh04mK16tOQhm8AgtG+msoPUYdaMw4NGZj+L9G4lNH3f4kP03qnIhVaXximiBwNZdA8wBAnbA9wqbFzT2XNk8+9qjt3vkfRsU5qSvLBN19+2rNkYW6W1Pl41SvHkPUAaigBAM6d75n75HOvimc3ca9XjrA9wKXWd6uhhHBkx6teOYasB9DwwfcAJ8Gbh1fa6UG1YwGVdnoQAL+Ok2rWF4qBejjxLEkccYtVvZHC9wCV/BNPfeTy+BWkCoFQsLhYrfo0pAl6AErpl4SQmwPHXb3IeuADV/DCy1JJ9at36g0Br1BeT2v+/W8u3bkLiNZL9LmPUvpllLI0oiToARy1Ve8DKJW7sOMcnf/gBy62voU2AsAAlS9Fqb8NGjGE7wFo+uycVaFyAiiQes9Gl3fb74adV1r5Xq931UCzggaCUpHOAHKrkeKxgdL1RopgGuiorTpBCFkaKiWs142Me993/QDg2gHWrWhKmEZ09IkD+BWxEbx1eeNc0weEYk3guL6F9lG+3WYdSDZPzElNST6o1pw8NSVZPLuJe71yhBUHcCfjYQCqzQjiwZsvP+3xf2GKEgjJJlq9cpBwPS+bmTsHHmIBkCp1Pt4egOVMgn9ksGcEiccKSv0/oTKCQmLfX3mIgjykSCs0EoaIVgObbJZilsvNB8i90VSWkZ093u0ymCmlJoZhcimlJgAghFi9Xm8lIcSqN7gsR/btOxWNfI3IiXg52NM9/BHdSGcegHnh3kMKC/XG1u5nAMM6AAZCCPhdD6V0BSFkBQC4XQYXy5leapow8hVaUuKOtH0akRGxATQ37+6ZfqXpDqqDFRfHA4fkrmczc+ewbmyihJjDrMIA4EW2pWsFm5m72r6/Ula2EoQbs4+UwTIGiWo18GidtZZhyFIQlFCgmVDm11LXGTlzETykKgLlB6GEmOEhVUbOXBRNGzXCI+qMoMYDlgoAS+XOs5m5cwjIewCS+OVEp7NfbsxoSps6PSVtCpsOAO3H7Y6zzY3nTjsaWOrxsLzLkwjoe2xmrkVtTxAros0I6s9TRZsRpEpKGCks1PvcvkD5dPTk9J1Zy1fmMXo9y79+4qx5EybOmgev291T8/fPyjpPOJYACEwrk4gbm0hh4TWXwphA5Yygg5btn0R0nyoGYGztfkbk9unsghutEzK4/FD3MXp98vwb78xvPWKz1JZ+Y4LfCCghZt8gEi8p3dZY99WXfEZQRnb2eADr+GWjJ6fvnJDBhT0OmJDBmUdPTt8pKl7nl62hIIp7ALfLYIZvJA/A1+dnLV8Z8ZO7WctX5v3w4Tt23pjA4Je9RZmWJgaJlBGkCIHgToBx6TPsjF6fHKkcRq9PHpc+wx5KtsbAUdwD+CN8weMx066QXDsIhzHTrkhtO3pYIHtgrZNn08ef73574+YxAPDC04/8dPPy/JDG1t+ofLDEARQ3APGvNDDViwbxvWp5gMee+X3ptrJ/FASO1z7/hrurq3vP3bffvFCN+hKJIftkEAA4na4LP1/zWGWDvblAdEr/8lvvmQCobgTxzghSfAxACLHyj9uP2x3RyhLfK5Y9ENo7us4uWVFU12BvvkbmEv3Lb71n+p9Pv96jVJ2JiOIewL+qF+wgzzY3nps4K+x1IwFnmxvPiWUPsHkAgIajDvvK1Y8Rl9stipqRDoCOwMXvRdYTRNvHD8qMoEgQ/0pPOxpYr9vdE6kcr9vdc9rRwIaSHQ2l5ZZ9t/zykdEut9vIL2cYnd18+5rOjKuXWgDwM2sU9QSDNiMoXPxr/sfAiwWMnpxeNv/GO0NGAcVUf/NJWecJB/8el97gmtpfrsD5jpOy/9D7H35a/tafNpkBCCbf+uFJNXl33DfNkJScBgDHDlT+o2FPSR4A/sOW7tee/211f7MDpUi4jKBw8StIELLtPOFY0nrEZglXRusRm8W/HsDnpYEkijyx7tXSt/60aRFEyk9NG1d+9apfzw4oHwCmzsu9esZVBXsh8gQvvv7fEcczEh1VZgFNE0a+wrZ0reCtB5Da0m9MJw8fKPMvBkl+kf7FoArRYhAIpRb7xFGvRNMWl8vtvP2exy31jU0F4nOXT59Vyl13S59yAKDUG5ZrlIsHDNk4AADQkhI3m5m7Gh5U4eJyMOk84cj/4cN3+lsOFncVvVSP1dGsBHZ0drXfeNeDjo7O7kWiU87p5iWW9Oy8Aqn7mmsqyhsrdi6EqAv4j6ceingsk+ioFgew7688ZOTMDxBQQU4A9XjYU4117KnGOv7lE2TE9FKQB5r2WyPOBTjadMxxW9GjXpfLlS04QUj7vOtvc4xNnyE2CgDyyn/uiQesNy/Pv+QCQ6ruD9BksxRDR3MIpWH3/wEIpRboaE6TzVIc6b0/7Knaf/PdD41wuVysQCajazKtXN05Nn1GttR9oZR/qUYFFZ8FSFYSTArFOvBmBzK4AESVFMpypmn515iLy3ZbrgYwnH/OP9KfakhKHiN1b6yUH+89gsRjlpgYQAC10sJZzrQYwKMAboNEt5Zy2Zjduf+8OpfR6Yb3uRmx/eWbl92paiDIsv2TkLLFBhDTtQC/YrdAgTV9ljMlAbgbPsVLunQAGMfOLJ277NZ88GYVfGLt9hMtI2jQLQaxnGkagIcA/ArAWNkLCTl7hXnxoWlZ0iN9YGj2+WIGjQGwnGkJLrp52e3QGL3+8LRMU2t69kITo9dLjvSByJUf6fMD4cYBiselRCRXTNHp8wO6P6ENgOVMyfC5+X9DCDcPwJM0crQlY2Fh0lhjxnwAs0LJ1X75F4naAFjOVAjgZQByy6nqQ0j7mClszcxFy2YmjRwdluI05QuJygD8o+5vIZpqxQqRmw97kUlTfl8iNgCWM80G8Dlir/yI3Dwft/NCV93O76pO2+sXI0rlqxXbH2gfPlAiMgCWM00A8A0AyWCKGhBCWtOmsLWRuPkAbueFrsM//L2qrbFuAfpufDWkf/kBwjYAljOlAPgawHR++cSZc0uvzL+hQOF28ZkA+bUCSXyK31bV1lgrpXggjsoflBlBLGfSAfgYgCAZYtT4STtVVn5EuJ0Xumw7vi4tL36HtjXWFgAYLb6GENL+4u8eqYzXL39QZgSxnOm/4Au+BBmeOtJy1V3/mkMIiXiLcqVxOy9015dvqzzVULsAEkoHAEJIx43XL6le/9RDuSNSU0bGuIkJQWvbmdbCW9fwvenJfrsAljOthUj5OsOwQ+Y77uXirfyLiq+bD9ACqWsIIR03LFtc/fzah3NHpKZIXjNU2F1R7YCwO7WGNACWM/0LgD/wywjDHM+7496xOr0hqid+yt5/I+T5/Pt/268Mn+K3V55qqA2l+M6fXXftj8+vfThn5IhUyWuGGh99tqVXVCRvAP65/iYIFlFIR86tv7wwLGXEFFVa2A/hKv6fli768YWnH9EUz2PHrr3VBw7Vizf4lDYAmbm+c97y25pGjB0vGZI91VhbWVv6zXhCiIu77pZuuaSLaDltr//x4PdfsqDSigfQCWDDnq0f/UZTvJAdu/ZWP/rM7ydBuCK6F8DWPgYgM9enM64qsI5NnyEZ9u1sPV57aMfXHIBkCsD2/ZeNi+/5jXL/AYBDO75OA6VpEqc6AWwAsMFus3ac7zi5XtGKBymtbWdad1dUOz76bEuv/5fPV34PgNV2m9UjMIAQc/2yqZmmAqmKnD3nz+zb8r+jAAQzfb0ezxVyDQv08eKxQH99v9cr2DsIECle7j61dgEbBISKnzxrt1nrAF4gSHauf/mkXXJzfer1uq3/90Ez9XrnK9LkyGBDKV5Dkh4Az8L3wwEgjAS+DUDwcxmeOsIy/5a7ZVf7ar77tNzV2xPREz9KIaf8lMsmiTN/tG3ofeyFz+0L0rH1QKi5/n2yc33Hvr3lHcJHtzQSi5MArLy/rXabtU+kUM9ypp8jwrl+Z+vx2qOWXTkqNFpx4r2LeaLDAHgDwhFip3+uL7kjl9SgD76+RWMQwkD0sKRxwcKaEWPHS47ieYO+yfzyK/Lyf1SxjRoqwgD4jl/QfqxJ9mL/oE8w4k+bYiyblmWOX1qYxoBgAPyNX9B9umUOpbTPYOHYfutu8aDPkJTyY+bPbh/oy6M04ggD4HsAZwMFlNJxZ5qO1IgvbLTsEsT/CcMcN9++xhjvFUGNgcHYbVYXfHH/IM37Kn7qeykVJI9MmZvbYEhKiVlqmIY6BJQq2GJaqhu4bHJ6I/+4tf7AKJXbphEDAgawA/10AxlXFRrBi6q5enuyz3ecsceikRrqwQBAON1AStpY1pCUvI9XRBr2lMhPGTQGBfx+vd/ZQPr8hYKAz9njTfO8Xk98t7rUGBB8A+h3NjCZm58LQs7g4kVjT9iqFdm8USM+BFcD7Tari+VMnwMIvhOweV/FT+PYmcGLGUY3LG2K8UD7MXswHuCo3pM8dV7km3iHk/unoT7i5wL67Qa0weClhdgA+u0GtMHgpYXAACRnAzUV3YHPbueF7kMlW8pcvb0s/5r2401XqtlIDfWQygr+BLxxQHdbC+e+0NtRv/v7facaa7NAaZ8kEEqpRORQYzAgZQCBoNAYwNcNlH/4n+cByGX/nAXwAHzdh8Ygo8/DoVLdAACpjWzOwrfv33S7zbpDhbZpxAC5J4ME3YCIswD+COBtu83apUqrNGKGnAHsANAMYBqvbMgpXmZjyxRQ2CmBHUAJ9M7ippqa9nDkGbOy0uAeVgSgkFCwIGABQGF55yPZeFP28XD/42F/hW/O/wX6UTzLmQSClA70iB8kUTPZM8KtbXspwcfDnN619fVVbVIXzJyZc7lzGPMaobgLopdpx0BeyK13FdsqNlIDONt8tMa24ysdAHBLV3jGTJseco/bWBkAm5k7h7ixKYpX3rdQkKImm2Ubv9DIma8noMUAJsZTHqHUQvVYLX4Lu6q7hYfCtuMrncflnOtxOecGDCHeGDlzETykKgrlA8BEArqV5cz3BQpYznwfAd2KyJWvuDxKiBkeUmXkzEX88rhtFOlxOedKfY4XbGbuHAIieLcBABh0sOfP1jUtmomUq2Yw6QCwt8HrKD9Mz5XVeVmXx9+P+yAAfdfI5TX5Dui7EO1RHGd5SQT0PTYz1xLwBAm9U2isIIWFetaNTZQIlE9N05mdG1bp85IMgi8RKxboJqxYAPS60PP4ZneZ9aiX/4obPYH308DnBJSXRNzYRAoLr6ElJe64dQGJhLG1+xmR26cvrNRZ312jz08yQPZFUUkGJL+7Rp//wkqdFcJnEEdDuFdRQsmjhJj9g9z4jQEShYzs7PHwjfaDmKYzO2/K1oU9DrgpW2c2TWd2yp1PUHnrMrKzxw95A3C7DGbwpnoGHewbVunzIpWzYZU+z6CDXVyewPIMbpfBPOQNQPxG8vw5jD2UW5UjyYDk/CsZu7g8keVRSk1D3gAYhhGkMy3KIFHtfgYAi2b1vTeR5TEMkzvkDUDsAQJTqWiQujeR5WkewMfAXtkxuEnRDIAKB1p7G7yOaEVJ3ZvQ8ijsQ94A/KtwQcqP0HPRyio/3PfeRJZHiWYAAFDCPyg75GV7XZHveNLrQk9ZnZcVlye4vBLNAPTOYgDBPXRdHrCPb3ZXRCrm8c3uClHcPdHl9ULvLB7yBtBUU9NOCT7ml1mPepds2ecJ+33HW/Z5LP54uySJKI8SfNxUU9M+5A0AAIY5vWsBtPCKyPrPPKYH/+IuC+Vue13oefAv7rL1n3lMEK7Stfv/ElVei/9/1lYDAaC+vqrNyJmL/GvtgS+KWI968wtfcfa33CrOlm4HsMz/eTuAtASTRylIUSDbSDMAP002yzaWM/8KvjX34Pfi8oDdftDDbj8IAMGn5OT24G0HsMxus1YBAMuZlkGotHjLcwPkQX6WkdYF8LDbLH+mYG6AbyPqSBEoyyfPWgXfrzesJE+V5XVSMDfYbZY/8ws1AxjiaAbAw5dz5/0WMi+e6oc0ANtZzhTcQtf/WeCy4yhvNIH3W36OIaCNAYL4s203YmA5dwGlSQ3aEkGeHqAbjZzZERgHaAYAX549MTDFECo/2py7NPgUFficaPIIAS2eOTMnq76+qk3rAgA4hzGvQZhqPdAcvjSIlJVg8ib6/2dtDGDMykrzP2ETJEFz+BSVRyjuMmZlpQ15A/A/WxdMB0/gHD6l5SXBPaxIMwCgkH+QyDl8KsgrVNIATvIPLpz7qVVB2apBqHAAlcg5fErLIxSskgZg5R+0H7dHnbkSU4jQABI5h09xeURFAzhxqFr8ntpERRD0GT+KyMXR+0Xq3gSXN1o1A+hua7n2TNORagXla6iAkgawFb530wUgB7d/MUkzgsRGMQPwv5NuNXhvEKOUTjiw7fPsqi/+uqvl8AHLYBkYDiUUDQXbbdY6ljM9C+AtXjHpbmtZXNf2ndxtAPruAKIRG9SIA2wA8AS0dwkOChQ3ALvNSu026x8BLIBwTKAkEWfFakijWiTQ/5LiRQBuArAewFcQBYuipALAkwrI0QDw/43f1OjMfigqAAAAAElFTkSuQmCC',

        attendance_ratio: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z15fFTV+f/fdyazJJN1QkgICYQkJAIim0arXzewSF2rUL+2IrWKQkGUnwtQQamFWgW/KKhYFyyv2i+KX0ALiAUBFS27ZRUNSWSHBLInk2TW8/tjZi53Zu5MZkI2kM/rdV9zz5lz75y557nPec5zPuc5khCC8xWSJOkBM5Dk+fQeiYDJc8T4Hf550YDGc0ieQxPk03u0BC6gEjgJrAeWCCGKW3ivVoMUrgBIkpQMdAOi8H0Q/g9GmdaFOPSKT4Pi0xAkz4C7YZWNbWrJn+4kaABuF0Js7MhKqAqAJEn5wC+B64FcIAP3m3IRrYsDQoh+HVkBHwHwqNT5wMOAtqMq1VJoNBr0ej06nQ6DwYBerw9I63Q69Hp9QNqbFyrd3KHT6YiKisJqtdLY2OhzOBwOnE4nq1at4vPPP1dWO0UIUd5Rz8xfAOYCTwUrrNFoSEtLw2QyIYTAe63yXJnWaDRER0cTHR2N0WgMOI8kL1RDeBtYq+38MltfX0+PHj2w2WzerF5CiMMdVZ8o74kkSdHAZOWXWVlZTJw4kX79+pGZmUlGRgY6na7dK3khITY2lszMTEpKSrxZHWrHRCnOByvTU6dOZdq0aRcbvA3gcDg6ugoyNIrzOOUXDzzwwMXGbyPY7XZl0hasXHtAo5aZlJTUlJmZ2d51+cmgMwlAlFpmly5dnO1dkZ8S/ATA2pr3liRJAroDArAIIapDlVfVABqNRjX/IloHbaEBJEm6XpKkZcBp4BhwHKiUJGmHJEl/8Bj5AQgmAJ1/PHUew88IPCcNIElSb0mSPga+BEYCXZRfA5cDLwD/kSTpCv/rVQVAez4MqM9jtIYGkCQpVpKkV4HvcHttZZh0errFxiH5TltcAnwlSVK2MlPVBrioAdoOLpcLl8vlTQohhD1UeTVIkmQC1gDXAmgkiVtyL2Hc4AJG5OQT5enBiysrWPjtFt7bvZMaaxO43fkLgRHee6kKgNuOuIi2gNPpY19H7BCQJCkG+BRP4/fvmsaHd/+Gvl26BpTNNScz7+e38cCAIVz9tzex2G0AN0uSNEwIsQGCdAEX0XbQ6XSYTLLzTxfMOFODovGvB3hgwBC2PThRtfGVuKxrNx4YMESZJXcDFwWgA5CSkqJMJkdw6WLgBoBbcy9h0W2jiI4Kz1k3pFt3ZVI2FC8KQAegJQIgSdIjwK8AeiQksuSuX6OJoKsus9Qrk7IRclEAOgCRCoAkSd2Al73pozXVjPhgEUsP7MUpXCGuPIsTdbXK5E7viaoAXDQC2xYt0AAv4zdXs+X4Ue5dsYRrFr/JgfLTzd5gd+lJ76lAIQCqo4BIIYTg+PHjFBUVYbPZfObpdTodmZmZ/n/6Jw2/Z2EOVVaSpOuB3wT7ftuJYwx+ZwGvDL+N3w+5SrXMnrJTfHPssDe5XQhR4020WABsNhsrV65k8eLF7Ni+HUtDQ9CyUVFR/Pa3v2X+/Pmq3wsh+Oijj9i/fz/PPffcBT8LGa4GkCQpCnjdm37st3om3Gdgb6GTl9+1sn2Pe0hpdTqY8NknOF0uHr3i6oD7LNjxb2VyiTLRIgE4ceIEI4YP59CRI6rfa4BMvZ5so5ETNhsHm5pYtGgRd955J0OHDvUpe/z4ce699152794NwP33309eXl5LqnXeoEsXpbc2ZBcwCbgU4IrLtMx/1j1izM/WMGqEjrc/tDF1TpOoqRMSwGNrVxGl0TJ+yJXyDcobLCzZv9ubdABLlT8QsQA4HA7uuO02ufFviItjUEwMOUYjOQYD2QYDWQYDeoUdcXtREaurqykpKfERgJKSEm699VaOHz8OQEFBQUDj79mzh/Lycmw2G3a7HavVis1mk9Pec5vNhsvlCqCpqVHXhBA4HA6Zp+d0OuW08vDz2gFu+yguLo7ExESSkpIwm8307t07QLBDIRwNIEmSGZjpTb80xej3PYz7tZ5f/lwnPTy9oWnVBodRIJjw2SdEaTSMHeR2+7+9aztNZ+cePhBClCnvE7EncN26dRQWFdFdr+cf2dncEBcXtKwX/202s7q6mkOHDsl5VVVV3HXXXXLjS5LEc88953OdxWLhvvvu4+jRowEN0ZHQarX06NGD3NxccnNz/d/oZpGc7NPmwTTANCABwJwoERuj3iapXST++VeTccpLTbUvv2uNFwge+XQFA1K7MTCtGwt3bvEWFcBL/tdHrAHWrl0LwKSuXcNqfIAsgwGAIx6t4XA4GD16ND/++KNcZsqUKdxwww0+15lMJvbv348QAovFQl1dHXV1ddTW1lJfX09tba2cV1dXh8vlkg1PvV5PVFSUfO7N9zJ3w4EkSTJ72Gg0ykd6evo52SlGo8/bnKjyu+nAo56kqKwWUsHd9Qz9WRR/GG/gpmui/MrD3GnG+KwMzZlJf2xMEoioZ79ax135/ZTDv9VCiO/8fytiAaipcRuQtyQG1DsosvR6AI4dOwbA008/zVdffSV/f8cddzB9+vSg10uSRGxsLLGxsXTr1i3SKnc66D3PwwOLSpE/4VmHMXWI2XV9RrR25vYKNm5pYuMWB3cM0/HG80Yy0nxH8RNH61OKD7vWvrrYevPakoNsPn7Exdmh/otqdYlYAOrr3R4lpUJyCsGPVis5RqOqYyHZ88YdOXKEN998k3feeUf+7qabbmLx4sWcKwfFbrfjdDpVqerN2QDKT+XhdDpVbYD4+HjZBvBrzLDgtxinyu/+1wEPAvRO1Ik/XGHWGrQSN2bEsKyojin/LmflBjtfbHXw0hQj43+jR9ljvzLDOPStD21bG5vEVXVWq/ehfi2E2KxWlxYLgEPxJ6794Qe21NdzS0ICS3JySPCjE3gfYUVFBVOnTpXzr7nmGj744IOgD9FisTB69GhOnz7tY+wpD7vdjt1u93+o7QaTycTIkSNZuHBh2Nco1gSAQgAkSTIAb+PW6rx5Y6pk0J5t3VG94xje08SMLeW8s7+GCTMbWfYvO4v+Ek1WhvwC6X42SMvGLT4TjapvP7TACPSSGZQCcF1cHFvq61lTU8MVBw7wz9xc+kSfneRyKcp636hBgwaxfPlyoqODT4aZTCbZCFT29cpDaQcIIQL6/aioqHO2AbyHd6FKZmYmubm55OTkkJOTQ0xMTFj388JPAJScvRlAPsDYSxO4Jj3w2cTrNcREaUg2ajHpJDZucdD/lno+ei2GX1zv/l9l5aK/4pK9Qog1weoSsQYweAw6u6JR/9y9O4VNTXxSVUVRUxP/9cMPbMzPZ4DnwexvbPS5R2ZmJsuWLSM2NrbZ3xs1alSkVez0UNMAkiRdCkwFSIuJcs7+WRdVUs620iZe21PF/Ou7cndOLPevK2XjsQbunmARaxebpC5JGr4rcioXmwRY/kpE3PF6LVilAGgliQ9zcrgxPh6ASoeDnxcW8l1jI4VNTdxRVCSXjY2NZdmyZaSmpkb60xcM/ChhjZIkaYH3cK+WZsENXbXx+sCmaXIKHtlQRv8uBh7sm4DZqGXl7d0Z0yeeJivSLQ82OJ95uUl5ySH8HD/+aLEGsPn1uQZJ4p+5udxYWMi3FgtnHA6GFRailyTKFSTI2bNn069f+Ativ/nmm2ZtAO+5mhEYzBj0NwLVDMFwjMCkpCTy8vK46667wv5Pft1eHO71mFcA3JUTK27vZVLtg/+0rYKD1TY23J2BxlNCK8GC67uy+4yVveVW7T/X+wjXTCFESIq/UgDCmgL0Gmx2FaMrTqvls7w8rv3+ewqbmiizB9LdBgwYEM7PAG4j8Omnn+bo0aPU19f706k6DHFxcbINEBcXF1ZXpkT37j7kjKuAmwFidRrHK9d1VX0pd5Q1sWB3Fff0juPqbr62gUEr8ffhaVy19ChNTrldvhJCvN9cXZQ/JuucUEagVwNYg3jmUqKiWJefz93FxXxrcQ9x47Ra6jyNl5OT01ydZJhMJrZskT1ZNDQ0BDUC6+vrcblcQZ0/SgdRuKTnYEZgQkJC2P9BDV26dMFgMGC1WgHu8Oa/fG1KVGpMYN2sHtUfHaXhL9eoex2P1TuUjW8Hfh9OXSLWAF4BqAnxNvbQ69napw+vnz5Nd52ObIOByw8cICEhAbM55OxnSMTExBATE3Pe2w+SJNGtWzcOHz4s592YEeP4bZ941bd/1vYKfqiy8caNXUk3BRaptroYt8HHxf8/Qojvw6lLxALglf7KZtRxlCQx2dNQ4zx/tH///iGu+GkhPT1dFoCYKMn516Gpqo2/rbSJV3dV8YueJh7sq655Jm86zUmLbGcdAWaFW4+IBSArKwuADyoq+K/YWAbFxBAlSdiFoNRu55Tdzhm7nTqXiyqHgxVVVayvrUWr1fLCCy+EW68LGvX19Rw+fNiJJwrLX65J0faIC2z/yiYno9eeItGg5a9D1bXeiuJ6lh6sU2Y9JoQITs7wQ8QCMHLkSKZPn84Oi4WCAwcAiNFoaHS5COaLS01J4X9eeYXBgweHW68LGtOmTePkyZNagGvTo8XDlyYEPHuXgAc+L+V4vYMPf9GNriq2QVmDk0lf+dDBVgkhVkZSF43aeSgjMCkpiW+//ZZ77rlHLteg0vjR0dEMHz6cOXPmsGvPHn75y18G3uwniLVr17J48WIAjFGS669DU1Wf9os7K/n8aAOjL4nnzmz1Ucb4jWVUNsldcQPwWKT1iVgDAGRkZPDee+8xdepUSkpKOHPmDOXl5SQkJNCjRw+ysrLIyspq0UTJhYyqqiomTpwop2dd1UWTnRA4rbzhWAN/3lFBXqKeV65T51K+d6CGfx3xmUic3ZJYQy0SAC/y8/PJz8+P9LKfLCZPnkxpaSkAV6UZmXBZ4JT6iXoHD3xeikEr8b8j0ojVBXoED9XamfqNT2CxncDcltRJVQCE22V2kRveili+fDnLly8HwKCVxFvDUiWN3xO2uwSj156ivNHJGzd25dJkQ8B9XAIe2VBGvV32wzQA9wkhWhR4SNUGcHUw/8pLOrlQUFpayuTJZwOwPVeQLOUlBnaPz2wuZ2tpE/+dFxd0yPfG3mq+OekzufaEEOJgS+sWTAPIQ5RQsNlsrFmzhq1bt3KwsJCiwkJsVit6rRa9TkeUwYA+JoZe2dlMnDiRK6+8MuT9jh49ypQpU1i9ejUrVqxg+PDhLf1fnQoTJkygqso97V+QauTxQUkBZT4uqef1PdX0TtTz+g3qiz0PVtt4bquP6l8lhHjrXOrWYhtgzpw5vPnGG5ypqGi27K7du/nss8/Ytm0b2dnZqmXefvttpk+fTmNjI0lJSVx66aWRVKfTYtGiRaxbtw6A6CiJRTelofV70sXVdsZvLMMYot93Chi7vowmhzzeKgMeOtf6tUgApj/zDPMXLJDTMRoN2QaDTAtXUsRP2Gw8cPgwRxsbVQXA5XIxbdo0H0bNwoULSU9Pb/m/6iQ4dOgQzzzzjJx+4eou5Cb6Wv2NDsGv/3WKWpuL12/oSn+Vfh/glV1V7Cjzmep9UAhx5lzrGLEAFBcXM3/BAiTgHrOZ57t3J9+X5eqDPKORR7t2ZcqxYz6+b3Czg8eMGcPKlWd9F2PHjuX222+X05s2beLMmTM+6wFsNltAWkkLCzUVHIz753A4sNvtcp4/xUyn02E2mzGbzSQnJ5OSksKDDz4YlB3scrl4+OGHsXgmxIZlxjCuf6DV/8zmcvZXWPlV7zge6qfe739XaeP5bRVKw3xhKJZPJAg2GxhUGD744AMAxnTpwuJevcL6kb4eATmiWEkkhGD8+PE+jd+3b19efPEsfc07HXzs2DF5tq+joNFo6NGjB3l5eRgMBjIyMkLOKr766qts3boVgASDhreGpga8YV8cb+CtfdXkJup440b1ft/hEvxuXand4RJeSfuBEPGcI4WqBgjF0D169CgAD0SwGCLT4xBSCsBTTz3Fhx9+KKd79erFsmXLfDjzJpOJbdu2AW6B8a4F8B41NTXU1dVRU1ODzWaT+X/KqWD/iODhrHzWaDTy9K/3iI+Pl2dCm8N3333H7Nmz5fSr13Wle6yvr7/W5mLcxjKiNBL/GN6NOJV+H+AvOytd+yqs3sa34x7yNaoWbgFUBSAUadLLCk7xK1NitXKgsZHbEhMDJD3DIwBe4Zk9ezZvvXXWeM3NzWXNmjUh+33vkqy4uDh/QkWngs1mY+zYsTLv766cWO7NC1xA8/Q3ZzhW5+APl5sZkKIuWLvPWHlxZ6Uy6zkhxH9as76qAqDV+tupZ6FGCwe4/ocfOGGz8YuEBP6RnY1ZISDRHo1y4sQJ5s+f76PmL7nkEj799NMWzfFHagMIIYKuBXQ6nWHbAGazOSgT+M9//jP79u0DIDVGqzqk+/Swhb9/X0s/s55pl6vzI2xOwZh1pVaXwCsdm4A5ET+kZhBMAIJe0OBZBu4vAPcnJ/PiqVN8VlPDPSUlrMvPl40Kb8/tcDh8VgD16dOHzz77LOjaOovFwq233upjBFqtVpkH2FHQaDT07duXL7/80qfL2rZtG6+++qqcfnNoKmaj77OsbHIy8YsytBK8NSwVfZB3bdb2Coqqbd7GrwHuFyLMcCARQNUIDCUAXqvX3+/454wMdjc08K+aGjbU1jLr5ElmelS6U4U/2KtXL1atWhVyYaXJZGLatGkcP37cp+/3twFqa2uxWq2q/b4yT6/Xh7UCybvziNIGSExMJC8vj7y8PHJzc/3X92GxWHj44Ydl3uLv+ibwi56BWwE8vukMZQ1OnhycxJCu6qOnHWVNzNtVpbT6JwohjjZb8RZAVQPodLqgXYDaugBwS8+HOTlctn8/R202nj9xglyDgfuSk9lU50NYID09nVWrVpGWltZsBUeMGNFsmc6A6dOny4tds+J1zPmvQMFeVlTHsqI68hL1zChQXxTc6BA88HmpwyXktvlACPG/bVVv5esQVhcg08JVhmQJWi1/69ULCfda5NE//kjevn3cXXx2d7Tk5GRWrVolM4suBGzdupVFixYBoJHg3WGpAd68sgYnj286g8aj+o1BVP/MreX8WGP3Nv5RYEIbVj1yIzAULRxgaHw8j6em8mqZm6RY1OTjvWLu3LkRTSGvXLmSM2fO+DiAlIEivOlzNQL9j+aMwJSUFCZPnoxGo2HSpEly+d/3T1Rd0jXxCzd549EBiVyVpq76vznZyOt7q72q3wWMaS7c+7kiYhsg2MIQJeb16IFOkpjrmftWYuDAgWFXzmKx8Nprr3HixAm5v+8oZ5DJZCI3N5ekpCSSk5PJz8/HYDAwd+5cvv/eTcBNN0Ux86pA1f5BYR2fHraQnaDj+auCGLx2F2M3lLmEkNvhZSHEV6qFWxHB/ADN2gCWEA0hAXMyMxlkMrG2pobeBgMzTpzAaDRGvC5AucWaEIKGhgZqamqCGoFqhp/SQRSpERgdHS0HifDHoUOHePHFF2Vjbd51KQEOncomJ1P+fQYJ+OvQVGKCPNpnNpdzpNbuvXgn7oWibY6IBcA7/q0MY+OjX5vN/Nps5v8q3c6M/Pz8c9raTZIkTCYTJpOpU0wWPf7448JqtUoAt2SZVLl70/5dTnmjk3GXJnCtStcAsOlEI+/slzkQdcC9LYki3hJE7AfwBjj6wa9vV4NDCD6prmasZxLonnvuaVktOyGWLl3Kxo0bJYCYKIlXrgt0+Hx5vIH3f6ilZ5yO2Verq36rU/Dol6eFOPv8ZwghSlQLtwEiFoDhw4cza9YsXisrY3N9PZdFRxOj0VDrdFLmcFBmt1PhcFDnclHvdMo+gBEjRjBp0qQ2+yPtifLycp588kkHnuc3oyAZf15/k1Mw6avTSLitfrU5foA531ZSVG3zPvsy4B3Vgm2EiI3AQYMG8cc//pG5c+fyrcUir/8Lhry8PJ544gnuu+++CyYE7ZNPPimqq6ujAPonG5g0IHCa98WdlRRX2/l9/0Su766u+gurbMz91sfhs6A1J3rCQcSTQeCeyRs9ejTz5s3zoYXHx8fTs2dPevbsSd++fRk2bBgX2vZza9asYfny5RK4H9hrN3Qlyo/d+V2ljXn/qSInIbjqdwmY8MVpl90llKrhR9XCbYiIp4O9SEtLY86cVp+b6NSora3l0UcfteMJ5JCTqKPO7sJid2HyqHiBe8zvFIK3hwW3+uftqmLzqUb/B90qsZsjQcSewJ8yZs2axenTp2UKUHG1ndtXniDrb4f42sPUfWd/DdtKm3jk0sSAdfxe7Dpj9TJ8/BH27iGtBVVa+EUBCITD4WDJkiWqS6Lr7S5+9elJSmrsPLulnCSjlucK1Kd5LXYX96895XK43PF9/SaV2l0AIh4F+MPpdHLo0CEKCwuDhovPzs7m2muvPa+NwK+++oqamhotQGKciWcfHoXRoOOpeX8XjVabVGNzcfMnx6m1ufifa1NIMqo/w6e/Kaekxu3wMZvNrsmTJ2sUIXLPHwE4fPgw7733Hv94/31On2menFpQUMCaNWsCplHPFyxfvlzu+1e+OpWrB7jnM/YUHpbeXrEecC/rArdPf2CKIaALWFJYx98OuB0+kiTx7rvvaiy+o6gO7QJkAWjO375z505+VlDAvHnzAho/TqtlQEwMv0xK4v+lpnKP2YxOq2X79u2sX7++VSvfXnA6naxYscIJ0L93D7nxAQbkZwWU/7iknttXnmBfxdlNQfdVWJn4ZZnc7z/99NMMHz7cn1Xc7m+HqgYItb+9xWLh7jvvpK6hgViNhompqQyKiSHbYKCXwUAXlSHkQ4cO8V55eQAt/HzB119/TX19vRHg3puv8fmub68M1WsaHIL7/lXK3vt6UmNzce9np2hyuPv9O+64g2effRbAn2jaoV2ArA1CCcDy5cuprKmhf3Q0a/Pz6RZG1OybEhLOawFYvXp1DZ7Q7SOu9p3NvHZwHx65+ya+3vU9V/TLZem6zVhtbjd+UbWNw7V2nvr6DD/WuPMGDhzIu+++K9tDfpNMncMGCBWOzct1fyw1NazGB+Ryp06dakEVOx67du1qxCMAvboHklcXPvOwfP7I3Tdx15NzOVPlDtP+4PoytpxyDxG7devGRx995EMo9XvZOocAhNIAdR5619URxMbzCkCpCj8gFJxOJwsWLKCuro7GxsaAw0sCEULIu4Uoo4H77yjiJY54bRxJkjCbzaSnp9O9e/egAZ+Lior0AIlxMa54U3RIL9lVl+Xx2tSHuHfaKwBy46empvLxxx8HzGI2+obR7fwC4LVaIwnZmOYRgLKysmZKBv7WF198walTpygvL6eysrLVgkUajUZiY2NJSUlh8ODBFBQUqJazWq1UVVUlAvTu0S28UHp6XzsoNzeXTz75RJUG1+Q7q9r5bYBg6wJCwbt/UKQCEB8f77N0TAhBVVUV5eXl8j5CRqMxYBWP0WgM6s7WaDTExMSE7esoLi5GCLe/vslmlzZs38fQKy5V9Wm4XIKXFn/C82/9n5w3ZMgQ14oVKzR+28TI8BOAzj8K8HLfIhEA76CysbGR2tpa4j1BpSOFV2WbzeZ221ls//79sgG4r+goN0+YTe8e3fjbHydw1WVn63CqvIrfPvc6G7fvl/NuueUW1+LFizWhwsn7CUC7e8oiFoBgtPBQUO4XUFZWFpEAOJ1OmpqaaGxspKGhQT4PxwbwDybdnA2gRlfbsWNHOR4ByM/LxdLQQNHRk9wy6S988c5MBuRl8a/Nu3nguddFeXWdBKDRaJyzZs3iscce0zbn/fSzAdqd8KgUALlzDdXPtkgAFOelpaX07t07rOsqKyvJycnxD6/e6jAajQwcOFDVUbVr1y65+mlpXYmLiyWlSzJ79uzn57+fxV03Ftje+2Sj3svoSUpKqv7444/jL7/88rBC8fsJQLtve6IUAPkph6MBQrGC/WFU9MenTze/z60XZrPZxwj0P87FBvBuQtW3b18uu+yyoCHtiouL5eFOTIzbRuvevRv19RaKin9k0Scb5QuHDRtW+f7775sj0XB+XUCHagB5sV0oAWhuXYAaDJKEVpJwChHxrl8DBw6MiEremrDb7VRWVqYAGDxRxl0uF8XFhyj58bBcTqvVOl566SXn+PHjI46E3Zm6gIg0QFME/PwaDzdQkqTzKmD0kSNHEEJEAUTHRFNWdoYD3xdisZwNxdurVy/HkiVLovr3798iMkdnEoCwNIB3Nq8qDFq4Fwc9ai4rK4u4MDebBPfy7xdeeIHa2lqampp8jMCGhgZ5BU+4RqDNZpO3hAVfIzA9PZ0lS3z2VfaZuaytrWPHzl0+399///28/PLLUSZT4CLQcOHXBXR+GyApyR3irLlw8V7YhOAPir2BI4HdbqekpMTHBqipqTmnLeIkSfKxAfr160dBQYFqCDvl2F05QxofH89rr73GyJEjW1wPLzqlDRBqFODd72dBWRmJnqlfmRZut/vQwqscDlbX1HDSZiMxMZE//elPEVXOZDLx/vu+u544HA4qKip8jEA1Q/BcN6IEd8DradOmyQEttFoto0aNYubMmfTo0eOc7w+dSwDC0gC33347OTk5lJSU8EiYs3tXXnkl8+fPb5XQLlFRUaSmprbbriEzZsygX79+2O12Bg8eTG5ubqve32+I26FdQNg2wI4dO1i4cCEvvfSSPDnkX6ZPnz4MGzaMm266iWuuuea8poNFsiNYpPDjA3SOLiCUAIB7KDh58mQmTJjA6dOnA9YFpKamntcN3p7wcxN3qADIP96cAHih1+vJyMggI0OdFXMRzcNvD8F2FwBVWni4AnAR546OJsmqTga1NAhDRUUFNpvNZ6+9cDdq/qnCTwN0DkJIuKSLw4cP8/e//53Nmzfz/XffUVFVFVBGo9GQnZ3NE088wZgxY865whca/ASg5R6lFiJiQgi43Zdjx45l5cqVIZ0yMRoNTS4XxcXFTJw4kX79+jFkyJBzrvSFhM4kAGFrgN/cey+fb9gAQFedjr5Go0wLz/YcvQwGUnU6KhwOfn7wILssFvbt23dRp64OjQAABZ9JREFUAPzgJwDBmSNtBFUN0Bwr+PMNGzBpNPyxe3ceS02VKV9qSI6K4uEuXZhgsfgEi74IN/yMwM6vAVasWAHA+K5deSqMQI/g3ksY4LhnTiASbNiwgZqaGhoaGgKOUOFivbF/I2EFjx8/PuL6nSv8/ACd3wYoL3fvWfPrCDaB7uYRgEhp4ZWVlTz00EPyb7YFkpKSKCgoiHgL+NZCp9QAoYaBXlq4IYLJlpauCzCbzRw+fBi73S5PALU2IygtLa1DvZad0gYIZdm3hBae6KFgR0oL90Kn05GWlhZWbOHzDX4CoJEkySiEaD4EWytB1UsTijPvnb6MRAC8JauqquT4AecCLymkORsgEkJIYmJgoKf2gJ8AgLsb6BABkFs0lPfO23gtWRcghOD06dNhzx1UVVVx2223yUagxWKhsbGxTcLFpqamUlLSbuH5ZKgIwLkTGSKAakuHEoBzXRdQWloatgAkJiYybty4NmcFX3nllfTp0yfs/9OaUJkLaJcIoV5ErAFaQgvXKoysSAxBSZIuePdxRwuA8jWJqAuIRAPEtAI960KFyqKXdp2KbRcN4PTEQHdBxFvCrlixgurqahobG2UbwPvZmkZgeno606ZNi6hurQGr1eqf1WFdgIxw9guojWCZ9o9WKy7cbNpIdgqpqqri+eefp7S0FEszIWlbiqysLOLj42W2c3vDnxbeFhtDhULEGsDrMQsnXLwXq6vdm15EuigkKSmJPXv2AO4ZyGBGoNIQ9Mb3D2UEekPOx8bGBt3+rb3gJwDt+vZDEAEIth8uIEe42FRXx++6dCHOT1tYhaDC4aDB5aLW6WRFVRUvekLDTJjQ8u1voqOjyczMvOBiD/t1AZ1DAEK5Ru+8805mzJjB8qoqNtTV0UuvJ0ajodrp5JTdrqoZNBoNTz/1FHfeeWerVv5CgJ8GaHcunqqeDOUKzsrKYtmyZfTu3Ztqh4NdDQ38u76e7xobAxrfbDYzatQoNm3axMyZM1u35hcIamtrlckO1QAymlt6dfPNNzN06FCWLl0aMlz8wIEDW2WFzoWM//zHZyvgQE5dG6PFjE2dTsfo0aNbsy4/OTQ1NbFmzRpl1p72rkPEXcBFtB4effRRDh48qMxaGaxsW+GiAHQQZs2axYcffqjM2gq02RaxwXBRADoA3nWVCuwBbm1vJxAEiRZ+UQDaDkuXLmXq1KnKrL3AMCFEZUfURykA7c5H+6lh/fr1jBs3TvmCFQM3CSEqOqpOFwWgnbB//35Gjx6tJNxWALcIIZrfbaMNoRQAmRZ7IXLvOhKnTp1i5MiRMp8SN+XrDiFEUQdWCwgiAD179uyAqlyYsFgsjBw5khMnTnizBO5t4Td3YLVkqHYBfqHLLqKFcDqdjBkzhr179yqzpwgh/i/YNe0NpQDIIlpYWNgmxMufGp588knWrl2rzFoohHi5o+qjBqUAfO092bt3LwMGDODNN9+kuLjYf8LiIsLAK6+8wrvvvqvMWg081kHVCQpJOeaXJGkZoBr8TqfTYTAY5D0BDQYDOp1OTnv3CQw37f+d/xHqWpPJRFJSUoct5xJCYLFYqK+vx2KxoNFoiIqKQqPRUFJSwsKFC12rV69WvlzfAtcLIdqG1nQO8BeAWOBZYDJwbqs32gFarZaEhAQSEhJITEwkMTGRhIQEWTi8AuuNVqIUXm/0Em9DBjvq6uqwWCzyZ319PQ0NDZE4yw7gdvREti6unSCp/RFJknoDTwGXAnmA+hbYF9Ec1gOjhBA1HV2RYFAVgIBCkpQIxAEGxaHDrSX0Qc4j+d6IexhqUn56NFKsEOJ82m7UDvwAfAZMF0J06ohbYQlAR0OSJA1uoUgAEoEkz6fy3PuZgDvYkgG3YCk/lecawALUez6V5/55DYAVd+Pa/A5l3mngByFEuzN7Wor/D76G2Hou4LZgAAAAAElFTkSuQmCC',

        punctuality_percentage : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAEbAAABGwBr11x4QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z15dFRVtri/W1MgFGHIJDQsqEJGedgiIAEMyBAFpbuBhwoCPhuBx/gTaMT4lEFcAiKgyCCTPgbBJ0voJ70cABXCPDTd7fIBMlQRgUBmhiKBpKru749biam6pyp1qyoT+P2TlXNv3Tp19r777DPtLcmyzL2G1WypC7QC2gCtgRZAfSAGqOvzF+AmcMvn73XgAvAzcAY4a3PYb1Xer6gcpJquAFazJRroDvQGHkMReOMK+roMFIU4CnwPHLQ57AUV9F2VQo1TAKvZYkQR+BMoQu8CmKqoOkXAMRRl+AFFIYqrqC4hUWMUwGq2dAZGAc8DcVVcHX/kAJ8BG20O+/GqrkwwVGsFsJotTYERwEigbTjPkiSoXRvM0TLmOlAnWsYcrVxzFMDtAgnHbXAUSBQWQgSa5TSwCdhsc9gvhf20CqJaKoDVbOkAvAEMAXTBfk6SoEkjGWszNy2ay1iaybRo5sbaTCY+VkaSgnuOLEN2roQtXeJCug57usSFixK2dB2Xr0palcMNfAG8bXPYf9T0yUqgWimAx8y/AQwEghJXs6Yy3Tq5SOrkJqmTm9gGFft7cvMlDp/QcfiEjkMn9KRfClKrQAZ2oihCtekeqoUCWM2WrsAc4Mny7tXroFtnNwNTnHTv4qZRYtXW/2qmxMFjOnbuMnDouA6XO6iPfQvMsTnsRyq2duVTpQpgNVvigXeBFynnjW/Xys2gAS4GprhIiKt6pRWRlSOxc5eeHV/pOXW23J5LBjYAr9oc9uyKr52YKlEAq9miA8YA84EG/u4zmWDoQCejhrpoaQ3u1aounLPp2LhNz7adBoqKAt6aD6QCa20Oe6X/yEpXAKvZ0hFYhTJ+FxJdG4YPdvLyC85q+7YHS1aOxLpPDWzZbqCgMOCtx4DxNof9ZOXUTKHSFMBqtkjATGAeYBDdU9cs8+KzLl4a5qRBvZoteF/yb0h8stXAhs/13HL47e2cwJvAQpvDXikNUCkK4OnrNwJP+btn0AAXqVOKiWt4bwnel5w8ifnLjOz4Sh/otm+AUZXhG1S4AljNlmRgK37m51taZN6aWcRjHWtWHx8uR0/qmLXQxDm7X2uQAQyzOexpFVmPClUAq9mSimLyVepeuxZMGVPM6GFODMIO4d7H6YT1Ww0sW2uk8I7wFhfwps1hn19RdagQBbCaLXoUR2+M6PqDzWVWLLxLS8u9be6D5ZxdYuLMKM5f9GsN1qI4iK5If3fEFcBqttQCtgCDRNcHDXAxb2YR0bUj+rVBkX9DIitbIjNHIisbMnOUBk+Mk0mIL/krV4kDWlAIby40BfINdgDDbQ672FaESEQVwGq21AP+F+jpe61WFMydUcTQP0Rcib1wOuHI33WkHdFz5ZpEZrYi9KxcqbzxeCkmEyTEKsqQGC/zuwdkkru66Pqou8K7q21f6pm9yMSdu8LL+4A/2hz2G5H6vogpgNVsSUSZ4nzY91pivMzH7xfRtmXFOHoFBbD3sJ5de/XsPaTj5q2g5+c1EVNXplc3Nym9XPRKchEdXSFfw+lzOv78ionMbOHv+BfwpM1hz4zEd0VEATxv/j4Ewm/RXGbDsrs0fiCyZjUnT2JPmp5d+3QcOq4P+u2OFCYTdOvsIqWnm77JrogPXzOuSbw4JYoLYr/gX0DPSFiCsBXA0+d/g8DsP9LezbqlRRHtU//xk473Vho5elKHu5qMHHU6eKyjm79MKOaR9pGrVP4NiZenmvjHT8J1hX3AU+H6BGEpgMfb34bA4evV3cWK+UXUrhVG7cpw8ZLEopVGvv4u4ASKEIPBQMP4hsTFxxGbEEdcQiyx8bHEJigbi3KzcsjNziUnK5fcrBxysnPIy87D6XRq/q7+fVzMmFBM86aRUfrCOzAx1cTeg8LfvQMYGs7oIFwFWINgqNeru4u17xWh1y4rFXn5Eh+sM7B1h4Fg5CFJEm07tCW5bzJdenQhsfEDxNSPQQp2N4gHWZa5ef0mmRnXOHbgGGl70jj942mCaS+DQVnLmDLaScMI7E9wuWDMX/wqwVqbwz421GeHrACeSZ53fMsfae9m88q7Yb/5hXdg/RYDqzcauV3Ovluj0UjHrh15vG8yPfr0IC6hYrYM5mTlcOC7A+zfk8bJIycpLg68/7NONIwbVczo4c6ItMeICVH+uoPXQ50sCkkBPNO73+Mzw9eiuczna++G3ef/9Ws9C5cb/XnBgPKmJ/dL5on+vUnqmUQdc52wvlMrtx23ObzvMD98/T1pu9MCWobEeJmZk4r5U//whsD5NySeHSN0DF1A71CmjTUrgGdh55/4zO0nxst8sT48b9/lgreWGNm0LfBgu0uPLkx4dSIPtnkw5O8CKCwoRJIkaoX5ep4/c56V767g2IFjAe8bOdTJrGnFYXWNGdckhoyOEr0cGcDvtS4gaVIAz5LuV/is6tWKgi8+vhvWOD//hsSk10wc/rv/nTStHmrNhBnj6dStc8jfU8Inyz/hs4+3AvDvo/6dMa+E3I2WcuLQcVYuWsXZ//vZ7z1Jj7pZviC8kdHpczqG/DlKNFn0DTBAy1KyVgV4DWUXjxcL3whvhu/sBR1jppu4lCE2+Y2aNGLs1LH0faafZmdOxJVfrjAs5XncnnGkJEls+XYrTZs3DfvZsiyz52+7WbN0DVcvXxXe07SxzNrFRbRqEfoLs+1LPTPfFp6HSbU57AuCfU7QW649O3nm+ZYPGuAKS/i79ukZPDpKKPyoWlFMTp3Clm+30m9gSkSED3B43+FS4YMitKP7j0bk2ZIk0W9gClu+3crk1ClE1YpS3XMpQ2Lw6Ch27Qu9Lxj6BxeDBgjbfZ5HVkERlAJ49vCtwmcnz4PNZebNDH0K7sP1Bsa/aqJA4OXHJ8azYstKnnvpOYxGY8jfIcLlUjecqCwcjEYjz730HCu2rCQ+MV51vaAAxr9q4sP1oS8uzJtZxIPNVRbcAKzyyKxcgrUAY/DZw1e7FqxYeDfkVb3pc0wsXW0UHrJo16Ed67avp037NqE9vBrRpn0b1m1fT7sO7VTXZBmWrjYyfU5oRxujaysyEPiwXfCzFO9LuQrg8fpV/f6UMcUhr+d/uN7gd9mz38AUln+6gtj42JCeXR2JjY9l+acr6DcwRXh9x1f6kC1BS4vMlDHC+Yj5HtkFJBgL8C4+W7dbWmRGD9M+TQpKn//+GrVJlySJcdP/k9mLZ2OKqqrDvhWHKcrE7MWzGTf9P4W+zPtrjCH7BKOHOUUvYwMU2QUkoAJ4Tuy86Fv+1syikNbFz17QMW22SWX2a9WuxTsr5jNy3EjtD61hjBw3kndWzFfNPcgyTJtt4uyFoP3yUgwGRSYCXvTI0C/lfdscfE7sDBrgCmkDZ/4NiTHT1Q6fJEnMXjyHx/s+rvmZNZXH+z7O7MVzVJagoADGTDeRf0P7aOexjm7RqEBCkaFf/CqA56Cm11m9umaZ1Cna4x+4XDDpNfE4f+y0cfeV8Et4vO/jjJ02TlV+KUOZEAtlUJI6pZi6ZlVX8KRHlkICWYA3fAtefDa0jQ9vLTEKZ/j6DUy5L8y+P0aOGyl0DA//XcdbS7QPfeMaKgdrBKhkWYJQATzn8weWLYuuDS+F4Pj99Wu9cG6/XYd2pL6Tqvl59xqp76QKh4ibthn469fancKXhjlFQ/OBHpmq8GcB3sCn7x8+WPtxrcI7sHC5WpPjE+OZv2rBPenta8UUZWL+qgXCyaKFy/2eF/BLg3oywwerXlQJP1ZApQCesCxDvCppgpdf0P72r99iUK1aRdWKYv6qBffUOD9cYuNjmb9qgWraODNbYv0W7cOtl19wYlK/W0M8svVCZAFG+JYPHaj9lG5evsTqjeq3f+zUcffEDF+kadO+DWOnqp3C1RuN5OVrGxUkxMkMHah6YXUoslUV+qLyykYN1e6SfrDOoNrJ06hJIwaPGKz5WfcLg0cMplGTRl5ltwuUttSKH5mpZOulAJ7hglc0rnat3JqDM1y8JLF1h7rSY6eOjfjCzr2E0Whk7FT1voStOwxcDD4WEQAtrW7atVLJra3vkNDXAozy/YSfJceALFppVG3gbPVQa/o+00/zs+43+j7Tj1YPtfYqczqVNtWKH9l5ybhUATwROJ8ve1Gvg4Ep2hTgHz/phFu3J8wYH7H1/HsZSZKYMGO8qvzr7/T+NoT6ZWCKC736I897ZA14W4Du+ETg7NbZrdn5e0+gqV16dInINq77hU7dOtOlhzqCjqhtA5EQJ9Ots6obiEORNeCtAE/43jkwRdvQLydP4uhJb5WTJIkJr07U9JzfgAmvTlRZzKMndeTkabOifmRYKuuy0urte1f3Ltqcvz1petVxreR+yWHv3r0febDNgyT3S/Yqc7uVNtaCHxmWyloHpSHXvWxOs6ay5iCMu/apO5wn+qv06jeCRNR2ojYORKNEmWbqY2pdPDIvtQDd8Qm53q2TNuevoAAOHffWTqPRSFLPJE3P+Y1fSeqZpBo2HzquF+6hDIRAliY8fkCJAqhULamTNvO/97D6iHbHrh0r/cTOvUQdcx06dvXe4FtUpLS1FvzIsjf8qgCPlb0iSdoVYNdedaUe75ssuPM3tCBqQ1FbByKpk1sUKf0x+FUBvGYemjSSNUXddjph7yG199+jTw9NFf0NNT369FCNBvYe0gV1UrqE2AYyTRqp5NkaQOdJsOR1zs/aTNvbf+Tv6rAsbTu0rbBTuvcTcQlxtO3gnSvj5i2JIwGO0IkQyLSx1Wypq0PJruVFC/Vhg4CkHVGbpOTfzH/EELWlqM0D4UemrXQoqdW8sDTTpgBXrqk7GNFMVlVz6+YtNq/ZzNZ1W1TXPlu/lc//+38oLCeic1UgaktRmwfCj0zb6PDp/wFaaOwCROf4Exs/oOkZFc0Xm79gcPIgPnpvFTlZOarr2ZnZLHtnGUN6DmbXl99WQQ39I2rLQLETRPiRaWsDSlJFL6waLUCWT2UMBgMx9WP83F35rFm6ho2rNgR1780bN5k3Yx6FBXf44/N/rOCaBUdM/RgMBoNXzCLfNi8PPzJtoUPJqFmKJEF8rEYFyPWuTMP4htVm5W/TRxuDFn4JsiyzaNa77N65q4JqpQ1JkmgY39CrzLfNy8NP0qz6On5NnwooqdW0yC7/hjoCZ1x89fD+83Ly2PjRRuE1azOZwU87Gfy006/FW/nuSu76CdlZ2fi2aVERmg6QlKTN8yHGgJJDtxRzdHjmHygNv1bVfPzhxyqnrnYteGNqMcMGlR1IF7N1h4G3l3rvws3OzGbbxm2MGKvaSlfpiNo0K1vStFPbHC1TUOAlr7oqC6B15rYk4HJZ4hKqx47ftN37VGVq4SsMG+TkjanqU097/ra7QuqmFVGbito+EALZxujwsQB1NFsAdVl12PJ988ZN8nLyvMqszWSh8EsYNkjdHVz55UqF1E8rojYVtX0gBLIVWACNAZBFWlgduoD0CxdVZb9vX/4Kp+89hQWF5GbnRqpaISNqU80WQC3bGO1nkWsIbnfk4hO7g8wGWRPRATfLFjg0rjUnCvYM5gomWiqbJs2aqMr++VP506e+99RvWJ/4B8oNtFHhiNpU1PaBEMj2pg64VbbkdoE2s5IgaJtqYTLjY1X9pi1dfF6hhK07DNjSvX9/dTnFJGpTUdsHQiDbW2oLcFvbQ0VamJNV9QoA8MzQZ1Rlby81CpWgZBjoS7uH1Sd3qwJRm2q2AGrZ3jTgYwEcmi1A9ewCALr16s6Gld6zgIV34L/mG1m/xVDq8P3zJ73qzQflFPOz//FcpdS1PERtKmr7QAhke8uAjwUoLFTi1QQ7G9ignozJhNdsYE521StA1tUs5k6f6/e6LV3Clh74zF3q/Ncx1zVHumoh4dumJhOaJoFkWZGtDzd1wHXfG7M1zjMn+Kwd5GXnBRVXv6LIzMhk0oiJZFwKfQz/7H88V22WtGVZJi/be07Dt83LIztXEsVkvK4DLviWisxhIHxNkdPp5Ob1m37urliuXr7qEX5GSJ+vG1OXWe/NYsrrUyJcs9C5ef2mKnuJVvPvR6YXdIAqtPWFdG3TA4mCymRmXNP0jEiQcSmDySMnCYM0N2nWhHnL3qZHnx7oBfHa6zesT58Bfdj01WZS/vCk6npVImpLUZsHwo9MfzYAZ3xL7RotwO8EOQKOHTimOuVakVz55QqTR04i62qW6lrT5k35cPNy4hLieOKpJ7iRf4OLFy5yJf0y5pi6tGnfhoRGCZVWV62I8hCI2jwQfmR6Rgec9S31k6rML8ld1VOsaXsqNOexF1d+ucKkEROFwm9mbcbyT1d4bVCt16AeD3d6mAFDnia5X3K1Fj6I21LU5oHwI9OzOpvDfgsl20QpNo1dQNdH3cTU9dbI0z+eFm69ijSX0y8z6YWJZF9Tr4xYHrTw4ebl1WJxKlRysnI4/eNpr7KYujJdH9U2PS2QaYbNYb9VUurlB1y+KpGrIS6NwQC9unlXSJZlDnx3QFMltfKL/RdF+Jlq4VtbWVm26UMaxjUUfLLmcOC7A6oRVa9u2lLY5uZLXL6qkufP8OvBEK9sCbIMh09oswIpvdQmaX8FdgPptnQmj5gktDItWrfgw03LaRDbQPDJmoWoDUVtHYjDJ3SiIeBR+FUBvhd9SAu9klyq0GQnj5zktta55SCwn7czecQk4fx4y7YtWbbpQ+o1qBfx761sbjtuc/LISa8yk0lpay34keX38KsCHAS8dvYdOqHt4EF0tJJLtyzFxcUc3ndY03PKw37OxpSRk1WbPQBaP9SaDzYuo179mi98UFLb+OYm7NZZe9JqgSyLUGSuKIDNYS8AvMYa6ZckrmZqGw2k9FQ7Jj98rTIuIXP18lUmj5xMfm6+6lqbf2vD+xs+IKZe9dmOHi6ithO1cSCuZkqkqyOMHfPI3CtCiOrbDh7T1g30TXah8/lI2u40zp85r+k5/tixZTvX866rytt2aMsHG5ZRN6au4FM1k/NnzpO227v/1+mUNtaCHxmWyrrs1R9879q5S1uAwriGsiqXgCzLrHx3habn+OPsKdWUBe0fac/7//3BPReHYOW7K1Te/2Md3ZqjtfuRYamsyyrAQcDLpT50XEeWxn1nf5mg3ll77MAxThw6ruk5Inr37+P1/7917MCSj5fec8I/cei4cPZP1LaByMqROHRcZQFy8PT/UEYBbA57MfBZ2Ttdbti5S5sz+Eh7N/37qM3UykWrwl4hfHrI0/zXwjfo2jOJqbOmseTjJUTX0egRVXNkWWblolWq8v59XDzSXlv/v3OXHsF2xs88sgbUkUJVx2j8ZfcKxIwJxaqJirP/93PYe+z1Bj39B/XnvbXvMWTEEGqHmrOuGrPnb7tVqWcNBqVNteJHdl4y9lIAm8N+HPCadzx1Vsc5mzZnsHlT8f77NUvXlJty/X6muLiYNUvXqMqHDXLSXB3pKyDnbDpOnVXJ7bRHxqWIJLvJt2DjNu1W4P+97MTXOl+9fJXtm7drftb9wvbN21VL2XWilbbUih+ZqWQrUoDNgFfPsW2nQbMz2LCBzLhR6rd9zdLVnPlJtQJ933PmpzOsWbpaVT5uVDENNcRrAsX527ZT5f27UWTrhUoBbA77JeCLsmVFRbDuU+0x60cPd6o2Lty9c5fU8a9Vi63j1YXc7FxSx7+mOomcGC8zerj2t3/dpwbViW3gC49svfDXub8NeEluy3aD5nx2tWvBzElqK5CdmU3q+Ncouht64ul7haK7RaSOf024ojlzUrEoL3BA8m9IbNmuelllFJmqECqAzWH/EdhZtqygED7Zqt0K/Km/i5FD1Vp86sdTzH9dlZL4vmP+6/M59eMpVfnIoU7+1F97roZPthoQhDna6ZGpikDuvUpjNnyu1xytGmDWtGKSBBsYdu/cxabVKr/kvmHT6k3CKCRJj7qZNU37aCknT2LD50LnT/j2QwAF8AwXvKIl3XJIzF+mPXOFXg/LFxTRtLHamVmzZDX79+zX/Myazv49+1mzRO30NW0ss3xBEYJ9q+Uyf5mRWw7VC/qt79CvLMHkDvaS2o6v9KqcAMHQoJ7M2sVFqqVMWZaZO33OfaUE+/fsZ+70OaqZ0ehoWLu4SHN+RlByCQgmfmRCzR0MYHPYjwCqCEuzFpo0hSotoVULN0vmFqlOHd0pvMPrE1Pvi+5g0+pNvD4xlTs+GSElCZbMLaJVC+1H0Z1ORSYCNnhk6JdgXuVXAa8F+HN2ifUhOIQAKT1dvDJW3b/JsszqxR8xd/rce3J0UHS3iLnT57J68UfCNZFXxhaT0jOEjNHA+q0GztlVpj8fRXYBKVcBbA57NqBK8rtsrVH0pUExebTTbzay3Tt3MemFiffUPEFudi6TXpjoN+zcoAEuJo8OwaSivIzL1gr9slSP7AISbGe+Fp8dQ4V3YOLMKNGQIygWzyli6rhi4SHUUz+e4uXBo++JGcMzP53h5cGjhUM9SYKp44pZPCc0i1dQqMhAkF/4GIrMykUKdonWarZ0RNlJ6mX7Bw1whfwDAHbt0zNttkmYBSOqVhRjp45j8IjBNS7hZHFxMds3b2fN0tXCWIPR0UqfH6rZB5g+xyRy/JzAYzaH/aTgIyqCVgAAq9nyGqCavVn4RhFD/xD6Dzl7QceY6SYuZYi7lEZNGjF26lj6PtOv2kQg9Ycsy+z5227WLF0jPKMIylBv7eLQHL4Stn2pZ+bbQscv1eawLwj2OVoVQAK+Ap4qW14rCr74+C5tW4b+g/JvSEx6zcThAHHwWz3UmgkzxlfbHIQnDh1n5aJVqvX8siQ96mb5gtCGeiWcPqdjyJ+jEBiWb4ABNoc96IdrUgAAq9kSD/wTnyQTifEyX6y/S2ONhxbL4nLBW0uMbNoWeITRpUcXJrw6sdqkozt/5jwr310h3MZVlpFDncyaVhzSJE8JGdckhoyOEkULzwB+H4zjVxbNCgBgNVuSUXaWev2UFs1lPl97NyztBvjr13oWLjcGDIkuSRLJ/ZJ5on9vknomVfq+wNuO2xzed5gfvv6etN1pAbe7JcbLzJxUHNLcflnyb0g8OyZKdNDTBfS2Oeyaj2KFpAAAVrMlFXjHt/yR9m42r7yreRXLl8I7sH6LgdUbjao09L4YjUY6du3I432T6dGnR4WlqsnJyuHAdwfYvyeNk0dOlru7qU60sp4/ergzIu0xYkKUv/zBr9sc9pBW1kJWAACr2bIGGONb3qu7i7XvhTaf7UtevsQH6wxs3WEIavZRkiTadmhLct9kuvToQmLjB4ipH6PZeZRlmZvXb5KZcY1jB46RtieN0z+eDmpjq8EAwwc7mTLaqXkzhwiXC8b8xcTeg8IGXWtz2NU554MkXAXQA9uAQb7XenV3sWJ+UdiaX8LFSxKLVhqFmcnLw2Aw0DC+IXHxccQmxBGXoMQQLAm/mpuVQ252LjlZueRm5ZCTnUNedp4qLEsw9O/jYsaEYs17+PxReAcmpvoV/g5gqM1hD7lvCUsBAKxmSy0U77On77VH2rtZtzQ8j9eXf/yk472VRo6e1KnyFFcVOp1yaOMvE4o1b90ORP4NiZenmvyZ/X3AUzaHXT0NpIGwFQDAarbU81ToYd9rLZrLbFgW3uhARE6exJ40Pbv26Th0XJ21tKIxmZSDmik93fRNdmk+sVMeGdckXpwidPgA/gX0tDnsN8L9nogoAIDVbElE2T+gUoLEeJmP3y8Ka54gEAUFSjrVXXv17D2kzmEYKWLqyvTq5iall4teSdpP6QbL6XM6/vyKyd8o6F/AkzaHPTMS3xUxBYBSS/C/CLqDWlEwd0Z4M4bB4HQqiSzTjui5ck0iM1siK1siK1ed2sYfJpMShy8hXiYxXuZ3D8gkd3XR9VFtkTlCYduXemYvMokmeUCxsn+MxJtfQkQVAEp9gi0IHENQ1g7mzSyiKg715N9QlCEzRyIr+9d4+4lxMgnxJX/liPoswVJQCG8uFM7tl7ADGB5un+9LxBUASkcHqxAMEQEebC6zYuFdWlqqLppodeKcXWLizCjO+4/OthYYH463748KUYASPJNF8/CZMQRly/iUMcWMHuascLNaXXE6lc0cy9YaRUu6oMzwvRnqJE8wVKgCQOm08VZ81g5KaGmReWtmkSquwL3O0ZM6Zi00BdpUkwEMC2V6VwsVrgBQuoC0EZ9VxLIMGuAidUpxxIdT1Y2cPGVndTmnrr8BRmld2AmFSlEAKF1KnonSJQiNfl2zzIvPunhpmLNKHLGKJP+GxCdbDWz4XC/aul2CE3gTWKhlSTccKk0BSvDsLFoF+I3FHl1bmUt/+QUnCRqzYlQ3snIk1n1qYMt24YmdshxDcfSC2skTKSpdAQCsZosOZYQwH/AbzdFkgqEDnYwa6qKltWb5COdsOjZu07Ntp/CgZlnyUTbdrrU57JX+I6tEAUrw+AbvAi8CAafv2rVyM2iAi4EprmprFbJyJHbu0rPjK70oOIMvMsqZi1cro6/3R5UqQAlWs6UrygmWcgP163XQrbObgSlOundx0yixaut/NVPi4DEdO3cZOHRcJ4rJI+JbYE55hzYqg2qhACVYzZbOwBvAQMqxCCU0ayrTrZOLpE5ukjq5iY3A+nsgcvMlDp/QcfiEjkMn9KIgjP6QUU5cvx3orF5lU60UoASr2dIBRRGGEPzZBSQJmjSSsTZz06K5jKWZTItmbqzNZOJj5aATYZXkTbKlS1xI12FPl7hwUcKWruPyVWHunUC4UQJuvO3viHZVUi0VoASr2dIUGAGMBNqG8yxJgtq1lRTq5jpKIuWSXLqOAiWpouO2klqtJHNamJxGicmzWRSZo7pQrRWgLJ7uYRTwPFD12anF5KDEWtxYncx8IGqMApRgNVuMQHfgCaA3ynyC8IREJVCEMn7/HiX86sGyQRhrAjVOAXyxmi3RKArRG3gMaI2fdYcIkIGSaeMoitAPlkTdrqnUeAUQrZX8/gAAAC9JREFUYTVb6gKtgDYoCtECqA/EAHV9/oKSPfWWz9/rKDkVf0bJrHbWk1/pnuL/A6t7Ru6t8SvPAAAAAElFTkSuQmCC",

        adequately_marked_exercises : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADngAAA54BEWVixwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAArvSURBVHja7Z17UFT3FcdFRBCJYpRH6iOACZgo5oEao+CgQR5NDanTGG07VSsZrZMWOmnBpGlKJpNXH+JoFZ1uJpM0NU1boZUEa6JuYoqmNRYivuWhBEPNSiFGJSB4es8iHXbvj+V39/7ub+/uPWfm+9fufew5n733nt/v/M4dAgBD9EqxaEXrFbUqApJTZxU9pShChI+N0hBBAGyggA+oD80MgYjgBytqo0D7JwQiAAijAHNpvxkhIAAsDoFhANhsNjh48KClVFhYyAvByIAHAB1iNSstLdVyJRhJAFgXANQHZoCAADAYgKCgIFNDQAAYDEBcXBzk5OSYFgLpAJw5cwby8/O51Nzc7PcAxMfHw86dO00LgXQA7HY7932yuro6IACoqKhwQpCdne3p977vCwgIAEkAmBUCAkAiAGaEgACQDIAGCMIDEoDa2lrIzc3lUkNDQ0AC0AdBVlaWJwjsMiCgNNBHAPRBkJmZ6VMICAAfAmAGCAgAHwPQB8HChQt9AgEBYAIAOCHYZwQE0gHo7OyElpYWoWpvb/d7AHwFganTQF4tX748IADwBQQEgMEAhIaGQlJSkiYlJiYONosoDAICwHf1AHpVRACYDIBt27bJBKBeURABYCIA6uvrB7t0i1aY3wHQ0dEBjY2NQuVwOEyTCubl5cmEwP8AsIJh0cvGjRuhpKREiAoKCggAKxv6kgAgAAgAAoAAIAAIAJ12vQug6wLAlRMAXxwA+PKwkpqcBei+RAAEHACdnwE4dgA0Pg1wejXAsUcAjmQPrNpFACe+B1D3Y4DmTQCXDinAXCMA/AeA6wBXTwP85zUl4Gs8B5tXR78JcO4FgDa7cpW4TACYEwAl8G17AU6uEBP0Aa8Q3wD4dL1yC7lIAJgGALyHn15rbOBVV4WHAFpeBei5QgD4DICOeoCGp+QG3l3HlwBc/KvhzwkEgLt9/kclADm+DX5/4RUIMwsCwGAAejoBml40T+BdrgaPAlyuJQAMA6DLAXDmh+YMfv+HxNZKAkA4ADhoc3yZuYPfX+c3K88F3QSAEPvqXG8u7i/B7w8BAaDTcHj25ErTBTfx1pEQM3a4i/bZZqm/K+h2YE0A8BLasE5XoHpqsuDt36bAzx6bDJVbUoQBMDpimOr37iqdwX4mEPBgaE0Azm/RHagn8xJczunF/ES5APRlBzpTROsB8N89uoN0+u15EBTkek7BQ4Pg3O50uQD0jRPoGCyyFgA9V5V/zVLdQSr6fgLTaT9dES8fABSOGBIAHHbhDd0B6vp3lvPBjHVeY0aFwJV/LeTaT/OedMidH63SsGB11e/9d0WqvvfmL+9yHTb2cu7AOgBcaxOS8u1Yf4/HcurSp6dy7efE39J0lW3/4ge3ue4TJ5AIAE8PfpuFPKRlzx3nMTB3JETA9U98AADOInoxlWwNADrP96ZNOoOPD3lDhw6+MOPvW2fIBwCF9QQEAMNabEL+/eh0nuDkpEb5BgCEXGNlkTUAOLVKd/Bx4GdibJjqXFhXBEwRT+5M87i/C+8vgPzv3KrS8JChqv09vCBG9b0BMwMsLyMA+o/3Nwn597+zOYXpqJcKEpkQrH10krw0sL+wxpAA6F/g8ZYQAPBf6H4eUWOGQ+fhTOZnI0cEQ1vVA/IBwExHw8BQ4ANQl687+C375jPz8yeWxzk/t78yi+nEXz8xRT4AKCw5JwAw928VUt6F4/ys8+h/n5+eeJO6n//XRkC38uwgHQBcd0AAKHbpY93Bx5x+8sRw1Tmk3TvG5XuvPDuN6UgcONJyvIzZY+G+5EgXHXxjtrbzxsUnBABO/OzWDcBe20ymg15/frrL9zoOZcK4SPUQ8byUm+XXFeAKJAIAx/6363bm0uxbVMePvCkErh5Sj/ljbQCzM/mf5kiuH1xEADgN74U6HHlx/wMQOlydlz++jJ3ind87H0KGqR8WV+SOl38V4FyQGtgAnC3W5cSSwilM53zyl7kDbrMsR33FQIhw4EcqALgq2fIA6Cz1vnNyhOrYs6aN9rjNR3+YzXTos2tvkwsALmuzPACnHvPagVWvswP5u+Jpg247e3qkarvYcaHOQSNpAGB/AssDUF/otQPxvu1+3IjwYPjyo8ELPrBYg3Xev39hujwAcL2D5QFoetkr531xIAPCw4JVx81bPIFr+2vVWTA+Wj1xNGPqaHkAcBaLBjYAXk4DY1UP67j/3H4/9z6e/xF79PAfr90nBwBsV2N5ABzlXjnv3jtGqY6JQ71a9uHYvwDCQtUp5COZscYHH9vV0DiAYu37NTvv8FtzmA7Z9OSdmve1SrlluO8HJ5Wa3k03FgDsWUQA4MLPk5qdt2bJRNXxRoR6N7V7ZMdcpnMLV8YbCwA2riIAbiwBO/YtbsdhSfcoxmwcFoJiCZc3unuK+nZy82j+8nGvhN3LCIC+TOBX3I579blkae3Yt/58qnEAYAs7AqDvOeBDbsfNuTtSGgA4ymjM/X8NlYSploPVPjSo446Vp8p8GYNTu7fOEA8A9jEkANys8ZlBHVfw3TjpAHw9LUo8ANjMkgBwLwx5z6PTvvo4E8ZGhkgHAMvHT1WkiQs+NrPExpYEACMb8LA2YPvL7LH7ik0pzvRPhGrLUpnl4wPVFngl7GhKK4O0PwwumDVWdYyECeFca/y0CFf1siaY2g9kiOknqPHfb73l4YwS8bp31M0eUDiWL/r+jL1+WL/nNz+ZIm3+39oAXD6icty6VQnM4drP9s43JE1Lvl1dPh4/foRz6ZnX+8W2ttQgQntGgNO2WKjhvn+8VBs1SIMFJazfVL7hHi/3mdPb25gA4DRcQ3/8207nlZWwmz1g9y+jAMBqYlbGkT7Ty/Jx7G2sw6zZJQwniWoXOfNw931PiAnTvJpHb4exPtX8ea62fWFvY51m2UaRTbVvMtOyZ9YYX7z56XvpzLWGKx/WUD6OBa/Y4JoA8M6Ki4uZa/1FtHrj0ZKsWGb5+OcfcJSPY2/jLjGvtLUkAD09PTBp0iTmtK+smr2Bqo6fe/z2wZd/cxZ8EgADWGVlpZCFnHqFRaLu53BLVKizFR17qHdlb4NrgWZJABYvXqzaZ0zUGOiqflAqAFgmzpomxsEpda6/zpD3D1oSgLKyMsjIyHB5FXtRUVFvdnAjRZQhXCiC4xA4EvngvCh4d9vMAdrDbxH6jgB6COwbGa6rcwY+NjbW+Vr2/48TcEwfixI2f8Dew+yHvaW9vY0NNHpjiGLd3d3sYWMB7WW8fqEktrXFYhaDjQDgmUUU0GaO+71A2NEU29pKMgKAt54Ai0rw1sBRXqZZCBiuYsKOppKNANA8iHC196qA1cYaSs5VEzh4e8EWdtjH0IdGAOi9MmDmgCuQcBka/otxQSquSsal6Thci00qsFMJtqvBnkXYuAq7l5nECACLGwFAABAABAABQAAQAAQAAUAAEAAEAAFAABAABAABQAAQAASAnwFQVVUFMTExXDp69Khq+8LCQq5tU1NTmcdPTk7mPr5soW8CHgC73c69Nr+6ulq1/erVq7m2TUpKYh4fHS27xwCv0DcEAAFAABAABAABEKgAOBwOKC8v51J7e7tq+5qaGq5t9+xhV+ru2rWL+/iyhb6hNJCMACAjAMgIADICgIwAICMACAACgAAgAAgA/wLAZrM5T5ykX+hLvwOAJEUEAAHgewCCFbVRMKQLfR7scwBuQLCBAiJdJUJiJwiAaEXrFbVSYAxX6w1fR4uI3f8A6GJNyL+CJlsAAAAASUVORK5CYII=",

        with_record_books : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAB8VAAAfFQEIJ2vyAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAihQTFRF//////8A89Vc/6oA/8wz/9Ur48Yc5swa68QU89Vc7cgS89Vc89Vc8cYc8skb9L8g9cIf7sQa78cY8MEX89Vc89Vc8sMb8sQa8sYa7sIc78Mb8cUd8cYc7sQa78Ud8MIb89Vc8MQa89Vc8cUb8sMb78Qc8MUc8cMa8cQc78Ub78Ua8MQa89Vc89Vc89Vc8MQb8MUb8cMa78Qb78Qb8MMc8MQc8cQa8cUa89Vc8MQb89Vc8MQa8MUb8cQb78Qc8MUb8MQa8MQc8tNX78Ub89Vc78Qb78Qa89Vc89Vc8MQb8MUb8MQb8cQb78Ub8MQc8MQb8MUb8MQa89Vc89Vc89Vc8MQb8MQb89Vc8MQb8MQb8MQb89Vc8tJO89BL78Qc8MQb8MQb8MQc89Vc89Vc8MQb8cQb78Qb8MQb8MQb89Vc8MQb8MQc8MQb89Vc8MQb8MQb8MQb8MQb8MQb8MQb1tC719G82NK92NK+2NO+2NO/2dO+2dS/2tO/2tTA3NbD3dfD3dfE3djD3tjF3tnG39rH4NrI4dvI4dvJ4t3K493L497M5N7M5ODN5eDO5uHP5+LQ5+LR6OTS6ubV6ufW6+bW7OfX7OjX7ejX7enY7enZ7unZ7una7urZ7+va8MQb8Ovc8Ozc8ezc8e3d8e3e8e7e8u3e8u7f89Vc89py892B8+/f8/Dg9N6G9OKd9OSk9O/h9PDg9PDh9evH9e7S9e7W9fDh9fHh9fHiSvpmDQAAAHR0Uk5TAAECAwUGCQoNDQ4ODxITGBkeICEkJSYnKC4vNTY8PkNERUVLTFJTWVpgYWNjZGVnaWpwcXd4fn+DhYWHjI6UlZucoKKio6SkpamqrLKzubrAwcPExcfIyM7P0NDS1NXX3d7h4uTl5uvs7fDx9fb4+fv8/f6tMyeMAAACcklEQVR42u3aZ1MTURSA4QPYe++99967Ym/YY8PeBWsiKiJ2EILE3rCAgbVLbPl75iZZZSaJy+4qrzPe99OZcz/kmcnu7MzOiuh0Op2OzmOjDc3/AsCw0ygasI4GGJ1owDQasCcdBhj9aMASGpDbEgYYo2nAehpgdKYBM2jA3nQYYAygAUtpQG4rGGCMoQGHtzlr4Z8COM2jARqgAf89YPvWxLIaEpD9PLFlNGAsDegCA/YJDMikAcNpQHsYsEtgwDwaMJQGtIUBOwQGzKEBg2HAqdYwYLPAgFk0YCAMONEMBmwUGDCdBvSFAceawIA1AgOm0IDeMCCnEQxYLTBgEg3oCQOOZMCAlQIDJtCA7jDgYFpywIevNvvsELA8xUuqj2GbfXEIGEcDusKA/QIDFtGAEZaAYGW8anPzzhxeqXXIHaCDBaDqivdnV9+qzcvLvmu1aqi8FN1euOsGsFssAH5vnQJqUxIZbquhOL4t+O4CMN8KUFwXUKY2RZHhlhqux7dnQy4Aw6wAz879+v38KrV5muctqFHDk7zo1lfu5i9oZwUIh+7fiffwW2zz6bE53FProJuLcKcIexvOpQFDYMDpNjBgi8CA2TRgkDWg1p9/JrHzN2MPhsDFkmoXgJMtrAEBb/JK1eEjc3AI2CTWgBspAIXqsNwcHAJm1gPwIAUg+jgK+uKDQ0D/egDCL8pKE/NXxA5rAhUuLsLjTYW9C9YKDJhKA/rAgJzGMCBLYMDk3wNev7fZG7uAXuQXFBHA0QwYsEpgwEQa0AMGHEqDASsEBoynAd1gwAH4U67sxTRgJA3omAzgabgWiE6n0+n+xX4Aj4wEZA3pk5QAAAAASUVORK5CYII=",

        received_grant : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzA3NEQyQzRFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3NEQyQzVFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1RjcyNjY3NURFQjExMUU3QTgyOEYwMUNCNDExQ0RFRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1RjcyNjY3NkRFQjExMUU3QTgyOEYwMUNCNDExQ0RFRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtwUdIEAABMUSURBVHja7F0JWBRH2n4Hhku5EQZB5BA5FAVBEQ80mhiN0Y3ZRLMmJmvMxhwmJms2ye7mWjf+mmP1T0w2Me6/ia6rcd1cqybqRiWKB2pEQA5BBOU+5L7P+etrUKenRxhgZpjp6fd56mm6pumuru/t+r6qr+ormVKphDlCJpMtYYffsTTJDF+/nqWdLK2TmSMBmPC92eEqS3KYNz6zMNMXf0gSPodHzJUAjpLsOdhbSHVg3pCawW5MWBmECU8Fi/odP4/aJ8iTWgAzh0QAiQASJAKY1xiAOzuMkkRvJkYgE7gdO0xnaQ5Ld7EUQdmS6EVKACZwatUmqAichG8jiVrEBGBC9+8WNgl9NktukmhFTAAmcJduQd/4ygddp5cmVaLo7HVAT74VW1cbBMz1ho2jlfkRgAmcmvCpKl951EAMWJshcrQ0tuusfId/exZ5x0v1Xg/nPkjH3L/GQBHhKm4CMIGTkTZeReCxLA3p7/0c3W0xZroCYTMUGBOrwPFdOfhuU5pOylp1pc4gwie0N3cg7csccROAyX44O3zJ0syBfOHBMe6csMeyNCLEGTI92f15x0sMWj/58WVQdiohs5CJtgXY1VfhW1jK4D/etUvgMzwRONENcivDDHFUpNcYtHI6WjpQnVMHl0BH0RIgSJuLPAMcuK+bhB46TYEhOjaOtEFnhxJlF6sE+fc/Nhnevrpppr/9xxkUXqvk5ZVcqBQ1Aeo1ZTq42XTrcU9O6G7eQwa9oMl/v4zG8ma+veFshz/99SHY2OqGkDZ2Vtj4x728vAufZcL/Li/YulgP+P7GOBR8WD0jZtFIbE5ehGc+mYLYX/kbhfDpyycCqGP+kiidCZ9w3yPRsLTki6m5qhUn1yVztoAYCfCjekZucpXejLi+gir94vZsHFh5Cp3tneoGLBY/MVWnz/PwcsLM+WMF+dd+KsH+5SdQc7VedCogjmwdlixvDrLk1qGisHHQvvw69uyiM+WoyKxBKdO/1PXThCdfmYOxkT46f/5bHy1BUkIuKsv5wi5Pq8Z3S49hxFQPuIU4YdgYZ3ix3g8ZxSZLAKVSWcO+pHPU8qvmp7Hu1oylAQb/2uNePc99bb01txOnj8ILa+/VSzkU3s54b9tjeHLBp1Cfxd3R2smVjxJhqMIW014Lx4hpHiarAjTaAWk6GGy5fO46tjyXgCPbsrXr4x8rxdWjxb0KP3yyHz74coVAV+sS0+8OxdpPHoKVtWWP1zWUNuPnjzNM2gbQaAeknygd0DB7/O5cvPPgUSR8ew11lS2aRqA0DLr0TrrHX5yNnXEvYpin/icaL/nNNOyOfwk+/sN6vK4yq5YRocmkCXCayMzTw0xoeWlV/b7h0R3Z6GhX9qR7hFkdmq+nr3DuAxHY/uNqvPr+/ZBbWRqsYsjG+ObcK3jxzwvgG+h++9fpUJouAZiea2OHY7pUA2RE6gIPPTkNJ/LX48PdT2DyHaMHpX4cnOzw9B/m4lDGm9h17LdwdOm/cWzMU8IEaiAtvv8E0NXQcESMP5xchxhNJUVODYC9o60oCSAwBLNYV6ytpaNfNxuMoWJTgNESgKmBVHYoVs0j4WfRpIt+oLmhXZK2ibUAhCOC3kA/1MDVlCpcz2+QpG2CBBDYAan98L+f/OqqJGkTJYDADshLrUJ9VavWN0g8WIjDX1yWJG2KBGB2QBG1+urddRoU0gYX44qxZdVpnXjNJAIYU3ewFzWQcaoMGx44io3LjqO1WdhrcBlmLyohevm48Lu8dpawcbYWDQEOazseQCN9u9cm4d3FcchMKNd4zYx5Y7D0qemiIgANEasi8F4fWA3Rzs9nCtPCf6IeIEs3O/Jk0ZderYfCz16th1CCg1szb3ujwFBPbNr5OLZ9cFRUBFj48ES4eThg++fHoAyTc+sHtIXRE4DZAfUymSwBXVPDeWpA4RfIu7a8h64ejd2//enSAY2aGStoIsq0OSFwjnLE9wWporMBtFYDLp52whe0kGH9/z3Cjd0PZMxcMgKNzBDMYD2BTjWPV8gUD1jK+W7dTtYDiJ45WpK0iROAZgjVqmY01rYhN4U/XdrOwQoBE4TrQk8dviRJ2pQJwOwAGsiPU8/XNCxMC0PUceJHiQCm3gJotgM0jAfQ+j91JMRlcapAgmkTQGAHZP9cIVjl6x/hxqkCVdRWNSL1fJ4kbVMmAFMD1MHPV81rb+sUDPiQERg61cOs7ID62mZsen0fVk74GPFvJaGuoEGULcBt1IAGO2Cm0A44KWI7YPfWE9j67n9RUVSHy/vzcWZjmmgJoGGamHZ2wIWEXDTWt4iSAEf2pvDOC06WcauIxUgAmiDCs+YKLtWgpoy/QFPh7wC3EUPBVxcdOHs8W5QEmLVgHO/cm8ZDbCzFRwBmB5SxQ4qwFSjVqhUQqx3wq5XTsWLNnXAd7oBR93gjes1Y0aoAzWpAQ3dQ03iAWAlAS9JfeXcR/pb0HGaui4ST71BRE0ArvwDFElAPo5KdUYLSwmqp76cCUwwTF88SWXM3gz9WlzahKKsWXkG3lmfZu1jDb5wLcpP5w8WnjmQOSqFLCqq5sQhKaYn5yEgugJu7A8KifDA2aiQ7jkTIeG9Y28glAvRiBzTKZLJTZPuo5tNkUVUCdKkBhYAAJ5ka8OthSZWuUVvdhPVrvsJ3O84KfrteUovMi4X4elsCd+4+3JFzWd9xb5jBymeqwaKFi0e19AucZi2AofbJOnYgDQvC/0ej8DWhvLgWTy/6DH944p+oq2mSCNAXO+DSadb3VYvYMXrSMFjb8btDFWV13Fenb/z5+T146hdbUFbU9yhiFBhqYcR6pCcVSAS4Dc6zxGvbaeXPlfMVfP1mZYHgGOGwMDmH9An64ndtiR+wzfDyo9vR0tzW67Xk6PrphzR8+OxeZH2XxwWUFLMRSHZAJ7MDaGLfg+q9gaDJ7gI7gKaHq4LGzvUF6mWQztcEit4RtMgX7mOduZAuTRUtuJ5ejcLT5cg9XCS4/sqlEmz+0/d4+Z1FPT5z785z+P2KHV0nX3eFjpn22nhRtwCau4Ma3cOeBi3UG09/yRl+6ghaNBK//PcsbnMqCt9i52YDV2a0Uv6sd6Nw9+bJGOIhnK/4xf8e5eID9YSvPj/FJ86BArRpGQvZlAkgMARzkirRVMdvMkewL83JwzATQeMPpeP4wXRBfvjjozH9jXBYDb19g0ukWLgtFtb2VoLmfcNL3/T43KJ8fuCM9qYOtFS3ipsATA3ksAPv06A5grQoRJvegD6QeCpHkOcc4ICIlVoFP+VUxOSXhcO4qYl5WtkC5mQE3rYV0OQeDotVGKQwqefzBXlRz4bA0vpWNbfUtiFFLUgVhaC72XNZ4MOpBlVQ7+ZSSqFEgP7aAWNmGIYAaYnCWUce4fxlW4eePc3FHVTFwWcT+P8z3kXDvfP1UmZT3zKG3MOdqkQuyalDZVEjXL1urQFw9rDjbAFyHesLxXlVgkCOQxV2sHPtGrFW3bXzekYNMr+5xrv2xu8rzi/kAj4C/N9Tf86TCKDBDqhk3cEk9mekaj7FA1i4egzfDoj11CsB2tp6trpJsISdsw5i4vOhCP6lL3dORPj5oww8EjdvQPc3SxXAhP8LUvHq+Wf3CZvLsJn6VQPefm6CcHEUq69JLSbhrHeiOIHvXXacS/Q35amCxgbU4TfaQ2oBNAj/3ywJ1kEXZFRzcQUdXG/tFkcjgjQySBNJ9QGKEjoyYBhyMvlGaFlyFXxn3eqFeE12x4P/uRMVGV1Cdgt1FmwEVZYijIfoH6SQWgBthN+lGrp8A6ognwD5BvSJ0IgRgrzzn1zi4vmqggRORKCkLnya1EmRPtUREu4tEUAb4d/ApZOGHw949DnhTje0vUvSVu18DxTn98z7whm9FCc4IFhqAbQWPiFdIwH0awdQEElN0UOTv7iME28no62HUHU0k3ff8ni01gsHfJ76/d16K7NcDMKPCvbBhcsFvOVfxdm1qC5r4rqANw2pcS7cTKG+BJnqK557cz7OHd8sWIpGXrpCppb64gwiTJkdjEmxgeZNgJ6EHzPGD8ujhuLj1uFIzS0SqIGY+31v3cdCxm0wdW5/vt7KSsL646YHsO7FrzQ28bTfj7bwD/LApp3L9Vq3cg2VTfv2zWBpIro2b9TnxsvkNqPI4LTy9wzr17f2R/jWcguM93UVECBdjQBd3UFPvRKAsGzVTJQV13KrdfoLmh729x9W6T2glVylomkK7fMsbcAAdunsB2gEZC2RgRXhdUaCTX0VPve1OAvNGU2OoTADDQuvWbcQru72+Gjt92io69uKJApo8faWpfDyddV7OS26K5oqmCZYfGhg4auClPVGVpb3ZV3QWviEEQ4dkFvyB2LKr9WjQm2hJK0YopVDhsDyF2bhQOobmL8kUqvradOJ97f/Gv84vLrHvQB0ql5pDxpW2evY368ZkdonL99MbYV/Ax+frEJiVgHEApoiHhTmxW0SMW6SL7eFnKYtY2YHvoUitc0ll+y7E/Ze/G9Z1R9xswVgwo9gx1eN7N3n9FX4hHE+LhATWlvauXUE//rbSby+chcemPwet55A1zbAw+rGoExuBafIe+Accz8sbXs3Qq68t1iQF/DKnl7/r7O5AdUJ36Im8QCU7T1PeOhN+ARfJ4gaWalFWDzlL3jzoyVYoqP9CeXd1j4PHvNXQXHfSwO6sX3wFK2ucwy/C6X/2YjSvR8MSPgEHwclgn0UyMwvFS0JaJXz26v3IDzaF8HjBj48TDUapZ7pGrvUoC/V0/O0FT7BkvXzH412Rey4AIFBKCa0tXbg1eU7uKMuWgDBfmdWrl4GfaHbPa8vwr8BL0c5Hp9IaYzpf+0dSpQ0KHHwUiNOpfFnBtMUsR/2nMd9y6IH3g00VvRV+GKD3FKGEY4WeCLaHlPG+gl+v6iDWUJGXbvmLHyeUc7S3GChMa5pDqKoCCDhFlxshBNZrmWXSwQwF1S1CEU1KsRTIoA5gBzLhzLrBfnjo3110guQYAK9gNNpVwW/h0f7SQQwNAprOhCX24rjKblMQB2DWpaXHt0G91ec+hWDQCJAP7ErsRIZV0uMoiw0EKTuBJJsAD2ioqHNaISvK0gE6APchloh1M/TJMraXNUqEUAfeDjSFbMnBBq9r0F9BbJkA+gI3k6WWBZhx9Lg+xqa2ztRVG+BrxPLkXGNr5quHi1GRWYN3IKdpBZArLCVWyDAGXhhhhv8hwtXPRWduS6pAHMA+UwWRArnBlTn1kkEMBc4a/AV1Bc3SQQwF1Rr8BU4ePc+wVsyAkWAVmYM7k8sFuS7BDhIBBAzbvUCKpBbrGbwyYDhWiyHlwjQRxiTL6An+N/lJYg2JhFABzAmX8BthWpryYWn0waSEdgHmIIvwNrBCvM+iYHjyKESAXQNY/cFDAt1wvytU+ERrv2iUkkF9BHkC4hzsTcqG4B0/eTfhWF4lFvf1YUk0r5hsHwBlUz95NTbYNvRDDQ28z19FIyyP8KXVIAJwZWpn4mKTqy5J1jwG4WVU/Zzd3SdEMB2RGiP5xJ0B3L+RAbxw9HR3gAVmbWDRwD3Ob+hKBPdAxCyrnMJeoOPm3CRSGVW/+YF6sQGcJm+BENGT0LthUNwnDAXNgp/SUp6RH6FcIq400j7wSMAgYTuPu9pSTp6Rk41BFFQKPqZa7Dj4BJAguF6AYKv388eVkPkEgEMAWP0BUxcFdLv/5UI0EcYmy9g9EIfjLzDc3AJ0FZVjLJ9m1Gb9F84RtwNj4WrYeUyXHTCNzZfgHuYC2JeHtg+wzrpBpLwK479E201ZdyRzsUIY/EFWMgtEPlMCBZ8Ma3HregM1gLQl69+7v3YhgHfl+L+3xheMBYMli+AtpSjzSWGhThxG1C4BDrq5L46IQA1+/Tlq57rRN+mNGFxmC2sLY2HBfrwBbTSKmDWtd+XVoPzmcI4xvM+nQKvaP1sdqETFUA6323mMlg5eXBHOtcFjiRmY8upKtQ1G+/MG12ACD7SSYZnpjghfJQwYFZ5apXenq2TFoAMPmryddHsqyMpuxAb65rwTKw7FA7WoiaCBdN388cNQ/KVIjUCVOvvmaZQMXmllXjvUD7yapQQO5zkwkWd2izwGAgBBE/sbGkw6Etr87yqukZs2H8JGRXi9mDXtAtbOWc9Rjen2kwRVHbCtwZ9aW2f19Lajr/sS8H+rHY0tIrPLuhk3Z4fLl7X0N931tszaY0zBbPnxQtuzkuHhY0dbIePgkxurdcvv/LkHm7coLNZ4OE60d06CSa4ZeRfx5lrjbB3ckWbkl6iHTYmHFOQegGFrJXfmViDC5eFm0SHrxit1Sqf3nDhNruX0cYMSiNMd7BEfZ/TRlo+cSTaMILhGyMrWDyVq7tstJPI15Kw9EsA2kin1EgKdZnU3g0CqBCBQooXSELTaapTrWCFEbQEh1nyUxe+ShlptcN6MlMk4ekkbZF1V+xNdG/WdA9ubRunz9EX2iaENtK7yNIOVpYD2vwTKyPNiowl+0glic/9qD+Qxb2TpXX/L8AAK23SCjk4axoAAAAASUVORK5CYII=",

        school_support : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAejAAAHowBNXh8qQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA59SURBVHja7Z17WFPnHcftzbW7dN3WzRdlU6ttVUgK+ri2thWsgrWInW3dUwSn1W5t0a66ro9b3Trt3XWtruvNboBgFQFJoCAQLpKEJIT7xQACllsAEUQoUIRwOfu99LDRkEA4Sc45Oef94/uEy8nJOfl+3vO+v997m0VR1Cw+KDcl8juglaDnQJ+BikBdoFqQGhQDOgraBrplhue+DbQZ9CZIAWoHDYB6QfmgT0DPgJaDZvPlO2FDfDD+dtBpkAlE2ahu0BHQrdOc+ybQi6DOGZx7EBQO+iEBwPnmLwJdnoE55moCPWrhvNfRJb7GjnMbQW4EAOcC8KkdBk1UCuhJ0CrQ26BGB533HQKAcwHodZBRzpKRAOBcAFp5DkAlAcC5AGTzHAA5AcC5ACwD9fPU/K9A8wkAzodgB2iUZ+aP4EYlCQPZg+B5ngGwkySC2IfgCE/MjyeZQO5SwU0cm4/DUncCAHcQPMMxAC+JyXw+AnAj3fnDhfll+PMJANxDEMwRAKvEZj5fAbgeZGDZ/Gwxms9LAGgIXiF1v7gBCGIZgCcJAPwC4D6WAVhJAOCRIg7vvysj9mOKLX34xr4HXwjecCsBgGPt23L/LaFb/eW7g/2HQBTLugYKIwBwqD1b/XdxYLyZ/NYSADjS7uD1e7kGYE+I/68IABxp7w7f23aH+JdxCMC5LVu23EAA4FihwY+sjP7ktfL4sHcoNnTs8B+PPrfN35M0AvkVCipZDAMPkTCQL6V/x6MIqoGXjxwMbfr4rb0UG3r3wG/LQkPW78FVEAGAQz2/dd0dUA8PctgGuLpz56YfEAA4iwL8DnEfBax/mgDA1eM/ZP0jXAMQum3dUgIAVxcza9Z1u0P8/gBPgop9OwNNL+16jGJLL4SsLwYAtpM2AH+igDKWO4SWkCiAXwA0sAyAHwGAR1XAS7s2jbJZBex7euPA9s2rjx08OOv6qa7R19f3xkBfr2c2+nrnBq7x/mCzv/RnBAABNQI3+68s2rRGerel69u0drl0o493EZhP/V/LjxIABBQGPhVwPzb2WqDv8uQAH+9DAb7LQwJ8vd6Av6UF+nqbvm0+yMc7jwAgkETQ80HrqMCHV1CTTJ5CAWuW/44A4OBU8N9eDPqIrTTwuI4fPUD9esN9MzG/C54M20kj0DkRQCgXcwP27XqiyDbzvZI2+XnPJVGA8wA4xgUAurPHDwWs8QoAk1+Huv0svF6iTW8GfQENvoObfLz8SBjofADyOJodpDO/Fn9/6fdIHoD9mUFfcwTAsFjWB+QzAHdzPEOYjAkU+WohHxIAuFsyNoIH6wX1gLYTANg1/zczXM+XDSWRpWKdb/ytoFM8XicQQxlEAHCO+feC6ni+UOS44nAVRQBwXJj3J9CQi5g/Lryq+WMEAPvMdwNlupjx5orEG1AQAGZufgC9UwclAOG9BPwJALYZP5ve6oUSoPBWM98nAEyd1SsWqPnj+hL0EAFgsvlPg/oEbv7ExaXfA90segDo2D5aJMabq8rV1htyxuJODo/t9WknqMLMaKrkXCxVrpZTBu0XVKUueex3HkKAw9vX8Y5logGA3qXrz46M7fWpUVRJdhxVpT9L1RamWxUGg6dPgxKQp+ABgJu8ge7EccgXl684OVbKq/PTpjR+XOVqGZ+rBLz6+AbBAkCHeGcc8WXlpX0+9mi3xfSJwsDwvF2AB5k8JzgA4Ka+C0pzxJeE63JbS3wNHIfrf1zy8xWfu1ID8V1cVQoCADxsCpRjd6kHAyt0SVMajsEoU8mogoxoaBeccPUoIczlAYCb+IkjkjvFUOprCqyXetz4K4ZGIG4MCixUfNNlAaBb+wp7v4RSZbz1Ep+XAi3700LPF+xxVQD+Yu/Nl+fIrZp/PieByhVeief1tnQzMX8N3aJleNNR0MpPtFLqU6ki4Zd6c+E0+Z0uAQBc6BzQJcY3C6XaWmMPt+jz0k5QIk0d63EehdcA0CN4spibH2nVfBz34/+L1Pxxvcp3APbbc4M4hLNY32sSKX2K6M0f7ztYyUsA4MJ+BOpmenNFWTGW07fQECTGf0vFXCWJpgPgHaY3VZB+iqotUFhu6RPDLSmIVwDABSGmkzRxxg7H85MafLnJY9EAMduiLnLRhTwVAP9iejOWwj2c0hVxa99W7bboR+GKm8bEFgBwIQtAg0xuojAjenIHDlQFhenRxGBrafG0D7rrsn5f1qF6UjaUK/3HsFby+bBOkjmikxjg9QpolNYV+m+ZoJNDWsl7Izrpy8NazxAq12uBIwH4jOnNWBrAwdORO5xJnxLRVZOxP6VLvSHVpPW+CGZSjtCQTlKFoRjKlayjKjxmMwIA10OgLqYdPJYSPcT0b1Sd+YqhT7O6cFgn7XGU6VOoD54kicM6z40zBWA90yFc5n36NXjARvpJ0Rtfmf5qtkm7Ip8F061IqhnSSB+yFQBGj/9SVbyFkE/c8X6p4silfs0DedwZP0kplObbq6BaSvteZpLurc5PNWv4pQlhAAfzDKji721QJ3fwyPxxdZr0khXWAPBh1PLPnNzyL1WeEXXp79esKuah+bQ8s6wB8E8mN3tek2AGgELUpR+rPXVpKh268Q2A0c7U+TJrABiYNP5qzFK+Bm2S6Bt+Rvm8utYEt9LuzEVak8azm2vjh7QePT2Zi3WtiXOLm+WoYRIAdP0/4IjQrzjrNAEAAIAvmqJlwl/8VcVCVb9qSaVJ4/EVG4YPwGd1pS9UtSa6FcI1DE64HosAzGdyoxVmY/nxsG2S758EwGQloCstCXPPt33hrrmSuiAbnhSansw7c/vO3VXYn72kHMyrHshZ2jCYs6xtSOvZi4V/xn/D/8PH4GN7su7Ud2cs1lxJWZDdluyeA+csh/N3TPnZclRvCQA/Jjd6wazTh/T22QgAt7L4BGC0Qrd5/S/21r8rA3CEycQOUv8LB4Czjuj5wwNBCACuCYB25nP64iYBIJJx/dOqSe5eyVcAjHJUZQmAbHtDwAv6FGL+eOP41MJ0+LJ7eQhAX81JlGgJAAWTJNDEHkD8RCDm030B4fPqzkeg1osxSGmUIQN88aMcmj4Kpb7iyxiUbYhELWVhlquAJKbdwMVZsa4wT591AOCLpsYFMLRdjEaqxjNIQwNx1YmGd+HPaIxHGvyZGMSJ12INABkxznkAWFL5cdRVEYUM1SeRBpfOuliUXR+LlA1xSI1BaTqD9MZ4VNgkQ2VYYz/D3/D/8DH4WPwe/F54rGsq4Vzl4ejqdJ9rDYDTxDh2AeBQFgGIIsaJG4DXiHHiBiCQGOdIANzrXQ0AN2Kc41QecUcVXwEwhKNiawNCmol5Vvo9UsOoqoy/Uq3KbVSn+jGqJ2cd9bXmAXhdO/Z7a/Y26kLGATjuP2PH18k8DIYI1Mg7848jI0QUmdYASCBmm43sTTtCtSu3UCatt00DMUw6b6pD9QTVnOxlhHj8WlM8yqs9hdQQi3dwZTp8dmfNKZQD4aMerqnfYl8ADcABYjo9uzn12FipHtLdw3jsXb9qSW5r4tw+OjkzYpShcojblbXRSF0VhYrprNyoA80eBbNbqk6gEjBcXReDlDh/AJ89PG1nkD2DQoSmkrT34fH+oKOmazW3J7tXTJG1u9YsQ7XwpChojEe6hjiUUx+HVOMJnounkRKbiYV//l/CCI7Bx0Kp1sF78+E8tXTptiVTWG8NgJsYzQsQ1CyeQ9SgdoWjx+j1d6YtrOJ1d/AECA6LdybPe9SgboWzBmr2X07+ebErALA4l/vtWzmo8z912GPfenXg2daaOK+H1wDQEGSJDYBL2VtZGZ8/oF5W4AoAPCW2R/+QTsrazJzLKe6lXAKAG47TAYD3AegQCwAdqsdZnaUzqPEo4cp8CA2LysOR3pY1gt4Sg/n5Kf+mTDovtqdqDbUmuH3FkukjEDaWVUUhZVkYarLaF2Bl9682wa/akfEKJ3P1OtMWJV2MQYWNcagS4v9Go2xshNCIPUbDeYx1p1FRzSmkqohESkMEygezO2zqDJpi7z9hN/6UwZwA0HduaYGlTB6ouywcNcFjurI8AhWdP450lVFIXRGFVIbjKAekw8aej0AlcAweAVSLj4f3DdjVGzjF/gD5QgbgqnojJwAMqj0qeNUdPM1egILNC/RpfDkBwKSVNLkEADQE4UIF4Jr2Xq7m7Pe4EgB4h7BSIQLQq/Hh6Ang2eIyAExYPfSK0ADoUgdwAsCA2qOas/EB4SiD6Z5BD9u3bQz/1KZ8ihMAerOWst4ILA9DPZWRY2MEquzZNm6vkACoydjPCQCtiYtKWDL+UkUkUtXFoInLxDTYu3Hks0zWE+LryB87Rv0w1Uh9zFxF5QmkNEQiNcT6uTiuh3geVwt4GtfXMzB4BNSL8wFwHn1lFFLWfjMaqKQpHrUy6gyyEYKVoEYBQDB6TXN/Ias5gByPCzZk9vB4wraGOFRbH4NK62JRcX0sMjTGolqc8YP/dRptHwFk24gghruIKlw/Hbz/IzbX9Gs/+4s2XncHM9hNFC8wnQwyuSgExmGtNIadDOCyCt6PB7BzO3kv0OOgENAOV1G7KkgCBrU5GYCutiT3dsEC4Ooa0nmuApMGndUF3JEyv5onYwLrCQBWNKyTBoFZJkeb36VYWM+bUcGyOe8TAKZ8Ekh86L16HGF+N5R8PgwJx0vU5LXI5+xivHu4qJQjuWM4V3LWLvO1knQq7567jIk/XWyUuQW3yNEHYAKenjXAkul4karMFhnabUxyn2f39vGifBpoJavBTOUMwsTRsS1adNK11s5ZEesxG2L4XwIQe+D1MDySP21OQNFgVkqz3E3b8s0aQnhuYY+FxaVGaGMv47qcPjYPdKYlwe3tFtmcnU2Jbqsb4m93c+j28aKXeqnbiE7yLJgbD684cdRCNxhb4fcieJWPaKWhlO6eeQ793IOzrq+XL7jNGOv+4/qIBTc7+r7+CwrY4DGTSNpzAAAAAElFTkSuQmCC",

        adequate_furniture : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAN2AAADdgF91YLMAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAWJQTFRF////AP///wAA////gICAgICPVVVjXlFrTb/y/00ai4CXioCVnWJYe1pjnGNa5r2Ua2t5g4OSc22AhoaUhISSXVNrW1JpAMR7gF5imWZZ/1EaSrnthYWQ/04aS7rt/04YXFRqSrjsXVVr/1AZdG2Acm6AmmVYXFRqXVNrW1RqdnGChYSRdnKCS7ns/04ZXVNrXFRqf1ximWNZ572US7ns/1AZmGRYS7nt/08ZhoSRhoSRmmRZeHSEeXSEhoOQeHWFAMN6XVRrfl1immRZ/08ZXFVqS7nthoSR/04ZS7nshYSR/08ZTLnsh4SR/08ZfHeHe3iGS7ns/08ZflximWRZ5r6ThoSQXFRqflxihoSRmWRZ5r6UhoSRf3yKXFRqg4GOAMN6S7nsXFRqb8Pid3KDeXWEflxigX+NgoCNhYOQhoSRj2FclmNamWRZzt3G1eDE5r6U/08Z/245/8CM/8eS/+u3xyVjjwAAAGB0Uk5TAAEBAQIQEhMUFBYYGh8fHyMjJSY2Nzg8PDw8RUVFVVVbXV1dYmRlgIGJlpeYo6Olpqenp7Cws7W1t7y/wMHEyMnJycnJytDQ0NbW1tjY2N/g4+Pm5ubp7vDw8PDz9P7+Pm8aaAAAAo1JREFUeNrtmvlzEjEUx+PWo9UCHtUe3hdqPSktHvXAgvctS609FKqi0qKs2v/fbFdmd0yybPLemFLf57cuefl+ZrJ50wQYIwiCIAiCkDJWLo+hTnhy6rUn8PCwcnzZ88qI8QPXPCnPHdno4WJx2P8UUWDK0xEo1mpFXIGjinzFEtQ4uAKX+Gw/fq0JKIbjCzzhs/1cAwk42YLb0sAtZB1MgcHpljbTg3hL4BjkcwMH7SXMtozIGmzDc++WfZo+X+90nhbMBAoGjejucijQXOo8dc0EXINWfCUq8LLztGXIn/Ljk6/qAvcPStf/bFTgFopA/9W6lKdbZAInogIXUAQm6zoCB6ICpzEEjijyFUuwIyqwF0PgIg/78D1xE2JvQoH3DEPgERf4lrwNs3uhwJIVgcuhwAtmYwnOhAI3mYWXkB0LBcaZhW3I9ocCp5iFRsS2hwK72b9pxZl8tW2Jaj7D8xfaFlnIsHzbKnlWtStQtS9gfQmsv4TWt2FAg6M+WgEbkQQhjwQ2uADCwQQmAD6aQQXAh1OoAPR4DhaAXlDABYBXNAgCPv7/akmupJKMIwES6CWBkZkGgJkRsAAonxuABRpAel+gAsuvgAVyMIEcWCA1B8mfS8H7QCpnvAqVXAqnESVtQclrSIAESIAESIAESKAXBUwggc0lQLuABEiABDa5QHpldXUlrRq959OXzx936eXH1/ydl573b7jnFQb7Fv1fE8zu1MmPrxHyJoI79gn58OvBr4rO6wjE1wh5btyXTFsXg8kea+R3qRHyYr9m2xbM1XyrIdClRsgjAesCpeDvknR037NgshsaAl1qhLzR9SelUfnwQw/8uW4P6WzD+Jr4PIIgCIIg/jd+A+MYLb+J8y4eAAAAAElFTkSuQmCC",

        good_structure : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAN2AAADdgF91YLMAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAotQTFRF/////wAA/4AA/1Ur/0Ag/0kS5r6U/1Uc/04d/00a/1IZ/1Eb/1EZ/1EX/04c/1Eb/04aXlVq/00X/04a/08Z5AAe/04a/08Z/04Z/04Z/04Y/1Aa/08Z/08Z/08Y/08Z/1AZ/08Z/08Z/08Y/08ZXVRr/1AZ/08Z/1AZ/08Z/08Z/1AZ/08Y/08a/04Z/08Y/08Z5r6U/08Z/08Z/08Z/08Z4wAe/08Z/08ZS7nsTrPjULDfUa7bUqzYVKjTVKnVVabQVqXPV6PMWKLKWaDHW53CXFRqZoijZ4WfaYKaaYOca3+WbH2TbXuQbnqOcHWHcnKDeWRue2FqfF9mfGBnfV1kfV5lflxiflxjfl1jf1xif11jf3R7gF1hgF1jgF5jgV5jgjxUg2FkhWJlhWNlhmRmh2Rmh2RniGVmiGZniWZniWdnimdnimhoi2hojGlpjWppjmtqjmxqj2xqkGxqkW5rkm9sknJvk3BslHFslnNtlnVvl2RZl3NumHRvmHVumWRZmXZvnHhwnXpxnnpyn3tyoH1zo390o4V7qIR2qYR3r4p5so17s498tpF9t5uIuJN9uZR+vZeAvqOMv5qBwZuCxZ+ExhIux6CFyaOGyqSHzKWIzaeIzqeIz6iJz6mJ0KmK0quK06uK1K2L1K6M2MCe2bKO2cil3LSP3LWP3raQ37eR37iR4AEg4LiR4bqS4wAe4wEe4wIf4wIg4wMg4ys547uS47yT48yl5DA85LyT5LyU5Xpq5Xtq5Xtr5Xxq5X9t5Ypz5Zh85b2T5b6U5r6U6RId6WdT6mRQ6mVQ6mpW62BM62FN62JP62VR62ZR62hT7GFN7GJN8t2v9jUb9zka+ua0/08Z/+u3Qc3uHwAAADl0Uk5TAAECBggOEBIaHh8mKSwuOTs8QkVRVFhnaG9/gIeXnq6wtbi8xMnKzM3W2Nrb3N3l6fDx8vP5+/v+kdRjUAAABSlJREFUeNrtm+tfVEUYx9e0tMwSC83ykuVaaWpWWmQlVm4pmpfM9bIl4iVAC/ASUtlai0a5QYLiDVNSkCxl8RaUeaPxWlqhsH+Oz5yze87ZMzNnZ/bADn44vzfMPjszv+85M/M8sy9wuRw5urOV4nanSAVwezxuqQAekAPQRmhMP8kAbaNkA4yWDTBAMsCT3eQCvHCf5FMwQPIxTOICdFIA6UsgfxNKP4byE5H8VCy7GCW5HDs3IgegawMs2bFjiUz/1COh0JFUif4TQqAJqXL95REo/sePSyNQ/U+flkWg+p9B6IwcAs1fEoHBXwpBjH+7EvR5oo1DNxuw/1mk6SwmaLjJM/bx3lb+vZ5PyF+I4LmeFgBDuf0bYvwR+rOBm2AI2/9hfv9LyKR/+AmYN8a7x3L7X0aErnETPNuDATDYjr8IwSC6f9+XbfkLELz0AM2/+zM2/QUIRtxFAXjMtr8AwUDS//7x9v35CV681+zfbTin/xVkqb85CYaZAR5pH39+gv6iOZjiXx3052dl5fuD1QkQmDLyUHH/ygKvpoJKcYIhQjmY8K/d4I3Rhlphgn4COVj1v6577FnuNWn5Hv3b/7gIDBl5sLB/JvZcXFJR09JSU1GyGH/KFCYYxJuDCf9a5fkLG8MRNRYq76BWkCCakePlYMIf4fVfVNYK1od8vkPwp7VsEd4HSJAgkpEfFfWvxM9bpjy7z+PxKY0yHKsUJRjIkYNV/xvGk47PX2Gr4ot/niuNVrwKBcZe/3MQ4IwcJwdT/Kvx/ousvwYQbsQ7sVqUYFi8HEzxR0FwKgmbAcIlEA4iUYL+rtGi/sgPThUkQAWE/UiU4CnXGItvb9H8UT441cD+90QUAaiBcD6iEdyysHja9ZAFwUUYfuyqud5lgVNLWPOPArRAOMvc999jMMVFtsPIB9VUcA+9tDaFQifOoTgAPisAdO5EKNREnXycsRgxAC40nTxPRmOXQElErCUAnT/ZdCFhALqMm9Agyia0VOIAxmNoEHkMOwrAmIh0kYmowwCMqVgTmYoTBvj1+0D2x5t+OKVHDge/XvHJ5q2/U4qRpthiVF9anJNTXFpvNSsLYNdK9Yaz+kfN7yM1su4nshxHnz+2HFflqSPyqtizsgB2anesD/epke1aJPMg34WkamF0xMIq1qwsgF9y9VveauV91S3TI+v/4LmS1efpI/Lq6bMyAbZAl/emvv7G9PehsRVHvoPG3GmT3pyBR2/juZSWwqd5GenpGfOgUUqflQnwJfR4Jw30LjS+wZH10HgbR2ZC41uea3kxfMzAIzKgUUyflQXwF+T5+a/hrm9B1zUQ+Q1e74JXcWQKRD7l+WGSA4F0PCIdGjnUWZkAR6HDHNwz7RVoLYXIz/B3thKZCK1sFP+nGcJEyog03KLOagmgKwqgK5snw8SuDXVWB6BzA/BvQgsAO5tQOgDHEjSXB3LNmTA3UN6crD1Q5/dS5a+zDdBcYOz6Fc5iq4yRgNLpcy9DfuUdFBlDRdRZ2bVg9zq952f7lUq61mBwAEfKvUyV4+/3GgiK9tJntXUlC8A0syanmTR5VvQNdfidEO8/wh8I8E5MCoB+zGIV2XJdBIClOx+APDDkoYoHwDMHfyIi00o8AK45+FMxmViVyEZVX0Dzg0h7IzvxUpep8wKYCidRXHkA4s7RqQGcPeAAOKega+8B7mJkAWCrGHGXYwsAW+WY+0JiBZCUG5ED4AC0I0CK2yNR7hSXVH/8/5rSAaQvgSNHsnUbfogttl2Xh5oAAAAASUVORK5CYII=",

        sanitation_facilities : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAqwSURBVHic7Z3tj1TVHce/v/swswMLyMMiCzOubLBCtNaUALuupjbRxLQ2YglNSRGtL/gDTNs0Teo7jZqYRl/4qjWxDdp20tSGNtpADa0FXDak2kSsLURghwWWfZqdnYd7597z64uVhV3m3lmW+zB3z/kkGzOem/v7ce9nzj3n3DPnEDMjLE6ePGnW6vWnSVAfCNsAbAGghRYwIQghStVabZlt2ShNleG67qxiAJ+BMMBMRyulsbf2799fDysXCkuA/v7++1yBtwDcH0qARYLrCoyNjWOqXPY65GMN2tN79uz6dxjxQxHg2LET3wbxHwGYgZ98kVIsljA2Pu5VXGfCk3u/v/svQccNXIATJ06sdlz+FMDtgZ5YAi5dHka1WvMqvuzWjXv27XtyNMiYgT+P6654HermL4iO1augEXkV364bzutBxwxUgHw+rxNoZ5DnlAndMJBKp/wO2ZnP5/UgYwYqwB133HEPgCVBnlM20n4CEJZYQtwbZDwjyJOZZtu9cOufEGCCyJj1X5BBBBOAASKTpmNL3yWcy/LlK2DbzsxnBteZucyCpwTzlGHoXwXwSVDxAhWgY+3qtQC+FuQ5ZaO9vQZDn/W9MAHc9uUfCFgTZDz1DZQcJYDkKAEkRwkgOUoAyVECSI4SQHICHQeYD2fPDeLIP47hysj832m0taXx9fvvQ1/vthAzk5PIBfjze4cxchM3HwDK5QoO/e3v6N7Yhc51a0PKTE4ifQQwM1zHaX6gB0KIALNRABELQET41mOPoHPdWui6Pu+/pUuX4KEHe7Bh/boo05WCyB8Bd23aiLs2bYw6rMID1QuQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5AS9RMyFmzlYmxxH27/+CePSeZhD56CVJgJOJx7YMMGlCXC1Arc0AVGdvSMYr1wDt3sL3Ds3o/7NJ8Crrq18NmcLuQbQTV3jZgS6adRgodAF4Ox8jm37+BiW/vV30KxqYPFbFWdsBO7IELjBKmecaYe17znUv/E4AGBsbBzlcsXzXBrxxp6enrNB5Rb4rmGDhcIwgA6/Y9rfexuZgSOBxm11hFVF/dz/GkoAAPVHd6P2wx/j8uVh2LbXPpF05YHe7YEulBhGG+AXfoWpM59Kd/MBQEtnYKzp9Cw3D+XBJz/0ufkAg32v7YLyCvqEAF4B8FGjAqrbWHbw1yGETAb6yjWgzFLP8iW/fBFkW40LCR8NFc6/EnROgQuQy2ZdAE8B+HxumTF0Ftqk5+6Yix8i6O0rPIuN4igyhdONij4nFk/t3r27WQvxpgmlG5jLZk9jes/gVzG9GTIAwLh4PoxwiYIy/rvqZQpnrv8oQHjVtqr39/b2NjTjVgltpdBcNlsD8KPBQuF1AL0AtlkTY7uwrmu1YGgM1gRIZ2aNmUkwiJmJGQSEt6N53FC7hfHqjeslE8ApXavXxkcugZBn8ABc83hf39ZQvzWh7R7eiAO/zb8LxhORBWxBmBlnzw96l4P+9POfPhfZ7qtqJFBylACSowSQHCWA5CgBJEcJEDkUdwKziHTHEI35VwLakauf7+zK/UTXNe8B8kVCcbI0s9+REAKmeW3HFNd1L37xxbmZIV7S+MyNZwiPSMcB5jI4OHgIRI/ElkBEFApD8LrODBzu693xaMQpzRD3I2Ao5vihw8yeN/9LYr0GcQsQ6OyWVsR1/be602K+BrEKQESLvgYQ7C+AQLzXQNUAISNUDeDLoheg+SRPV14B0un0KQAeU2AWB47vVrlkWZZ1KrJkGhCrAB0dHVMAPogzh7CxLduzjMAfPPzww1MRpnMDcT8CwETvxp1DmFi2twAAYv+3xy+A4xzEIp0CZFm23xgAO45xMMp8GhG7AF1dXRcBnIg7jzCwLe/mDQEnHnpo68UI02lI7AIAALdAVRgGtRav/oEWEcDU9bexyHoDQgifBiBZQhhvR5qQBy0hQGdn53kwvxF3HkFSrVRn3gDOhSDeCHu273xpCQEAQNO0FwAU484jKEpTZa+ioq5rL0SZix8tI8CGDRtGGXgp7jyCwLYs1OuNf+NHwEvbt28fjTglT1pGAADQgNewCIaHfb79FwDxWpS5NKOlBMhms1UwPx93HreCcF1UKo3XPCDg+d7e3pZaEKGlBACAXC73JoB34s5joUxMTHoVvdPbu+PNKHOZDy0nAAC4jvMsgIG487hZajUL5Uqj1T14wLaqz0ae0DyIdU6gH4ODg+tBNABgfdy5zAdmxqVLw43e/g2xcLb19fW15OSXlqwBACCXyw2BeSeAWty5zIfJYqnRza+BtZ2tevOBFhYAAHK53ACYnwHg91I9dmy7jslSae7/dpj4mQce2NbSj7KWfQRcz/mhoUdIiN8DWBl3LnNxHBfDw8NzJ3+Oa8Tf6+npORxXXvMlEQIAQKFQ2ATgIAOb487lKq4rMDw8DMeZNe3rPwTxnbBW9Aialn4EXE82mz1t23YPgPfjzgWYftlz5cqVWTefQe+nU0ZPUm4+kCABAKC7u7uYy2YfJ+BlxNguEEJgZGQU9fpMCg6IXh4qnHt869atiXqfkZhHwFwuXLjwFZf5RQJ2RRnXqlkYHRu/NtuX8Aeh0c8e3L79v1HmERSJFeAqhUJhh2XXD6VS5rKwYxUnipgsTc/htGy7ZKbNRx/s6ekPO26YJF4AADh2vH+iLdO2YsWydqTS6cDPb9t1jI1PwLYsWJaN4uQkKpVq8Qd7dt8WeLCISbwAH354slM3nJmBFk3TsCSTQSbThnRbGkQL+z2+YEalUsFUaQrFyRIqlQoqldqsH3qwq6/fu/e7sc/ruxUiXR8gDHTd3XL9ZyEEpsplTJXL0IjQ1jYtgq7r03+GDl27se3LzHDqdTiOi5ploVyuoDxVwfDoqOcCzzwdWwkQJ0S8xasOE8yoVKuoVG98AzstgwEww3GcxtO3CJ43HwA04i1I+A9bEi8AwJsXsuyK67pNf7dnmqZvOYFaZlBqoSRqHKARDGxpftTCME3/7weL8GJHReIFACjUm5BK+dQCpASIlf7+/uUIeb5Ak8fA+gMHDiwPM37YJFoAIUToz+Cm7QBKJbodkGwBSA+9Ck41EYCnewKJJdECgDmCGqBZRynZPYFEC0Ah9gCuYpqm/2hiwhuCiRYAHP7FJyLohu5dHkEOYZJYAU6dOpUCoTuKWKbh/RhgoDufz6eiyCMMEivAaKm0CRGNZPqOBQCGbWNTFHmEQWIF0N3oql7T8P+Cs5bcnkBiBQCiu+jNegIaaUqA6NEi6341GwwSHP6AVFgkV4AIB2B0XYPWYA7BtVTCfR8RJkkVgAi4O8qAqZRPO4BwN1ptK5B5kkgBjh49mWPAexfmEGjSDlj6m3w+F1UuQZJIATTNjbzKbdYO0OvJ7AkkUoAoewBXSTWbHJLQnkAiBRAx/D7QNJsN9iWzJ5BIAaJ4CTQXw9Ch+U4xT2ZPIJECxHWx/doBYc5NDJPECXD8+PFVAHfEEdtMmZ6rlRDQkc/nV0WZTxAkTgCi+KraTCbzmV+5bSevJ/B/kaOnefVwGsIAAAAASUVORK5CYII=",

        recreational_facilities : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABpASURBVHja7V0HmBVF1gUm55xzBoYZHGBIMmQBEcSE6Vd3WVkVRFFWV8HsuIAKJmRBRRQRBAUVluAikgVEUDGsoqigAoZ1jauw7lp/naLfo7u6ul/3e/1e1xuZ7zsffDPzerrrnq66devce1sRQlq1NNCvYoqeFCMoRlNMpJhO8QTFaoqdFPso/k3xH4pvKD6j2EPxGsVmisUUkykupuirXLNNixurFmLwKsXQ8yk+oSBBwk8UmyimUgynyDhOAHcM3pbiUoqFFAeCaHAreI9iJkU/iojjBAie0btTzKP43GWDm+FLiocpBlFEHidA4EaPpriQ4lW7xsjNziC9ezSQ0RecRu665Uoy78FbydLH7iJ/f3oG2bJyDnlj/QKyd8dzDLs3LCRbV80la5fOJMufnE66d6lzggxfKUtF8XEC2Dd8PsXtFF9YGeyszDRy1vD+ZMaUa8nLqx4lX733Ijn82ct+46Wlf9X9jZzCMlJYVkPSs/NIbHwiad26tVUi/JdiCUXv4wTwbXh47osofjEb1Pi4WDJiSB9yb/ME8tq6JwMythEG9G7U/M3omFhSU9+V1HTsxlBV14UUlrclGdn5JC4hySoh3qA47TgB9IZvUrZmhoOHAe7TsxN55J4bAn7DrWDT8od195BXXOElAI+qDl1IdkEJiY6Ns0KELRQ9fvMEoF8pFLMpfjVby2+//jK2Vgfb6DyG9O+hmwWqVbOAEYoq2pGk1AwrswKWhqrfJAEwFVrZwk0Yc37IDe8B/AkdIYvKfRLAg8raTiQrr4hERceYPSMCUc1weH8TBKBfeQrzLTlRD02b6BoBgOGDmzT3A2NamQV4FJRWsxnE5FnfoujSYglAv1or0bpvRAMQGRlF8ksq2b/q729c9pCrBNixZp5uKs+lOwK7BACaBgwl1e3r6TNGmu0YEH6OaVEEUEK1643Yn5KeRSo7dGaOFP+zQ++84CoBgDNO6cfNAtG2Z4GGHn3J4FNHMvQdPJwUFJWazQZvU1S3CALQr4uUGLruQTElwmHyDFJxZa3m59lZ6a4bH8BWs02bNtq4QEGpLQL0HXyqlwAedO89gKSmZRiR4FuKoWFLAJycUUwz2tJh/1xd36gZJDhY6t9DJE8GAgBnjxioXbKionX3b4T6rk0646tR16krvV6UiAT/o5gUdgSgX4kUy0XGRxSttKZOOFCIsql/95KLTpeGAG9ufIpERGhnAez7rRDgRLr2mxEAaBpwMklMSjaaDZ6hSAgLAtCvLKOgTmZuoelAJSanan5/+u1XSUMA4Pwzh+gc1+o637PAScPO8EkAYODQ00lOXoERCXZhbKUmAP0qUY5HNTffJiKChU19DRS/TVq58D6pCPDOlsXU6BHaWSC/2PSZ6hp7WTK+GlXtOhgFkfZijKUkAP0qVVQ2nMccS8radvRpfKynrbiHdiP65wu/O3eY5h4j6CxQZTILNPYaaJsAQOfuTSSK+hkCEuxzkgROvvn79Ot9AouGWVkn4ReoP5uYECed8YE925aQKG4fn5VXHND6b+4XpIhI8BFFoRQEUNZ83bQfn5jMTsysbpPyS6o0n+9c31ZKAgDQGGhngUjDZ+0zaLjfBAAGDD2NpKVnikgA/WKOqwRQvH2dw5dAWWvFOVIDDqL6GuedMVhaAmBpiomOsuTg9hsywpbBBw07k/TqP4Sc0NiT+gJ1JL+oxGgWALZRRLlCAGWfv1z05ts1PpCcqg2I3HbdpdISABgz6kztLBARySKZ/HMNPOUMwze7e9MAUtfQlZRVtSXZuQUkITHJjtDEg/vcIsA00ZpvZ9pXIyYuXnOtxXMmS02Afa8tJ7ExWictI6dAEP8/mXTp0Zu0q2sgxaUVJD0zm8RY0wzYwVkhJQD9ukDk7Vt1+ERozYVaodmTmQDAlX88R2cM6ACS0zLZy9CmTYSjhkY4urS4gGSmp/I/+85fXYG/Bzs/8vt8K1s9I5S3O4ELsESQH/ZtlJ4An76xgiQnJTiuLo6LjSH1tdVk5IhB5KZrLiVPPjSV7Fy3mHy3fzs58vlrZNuaBdQH0W0Rd1PEBZUAypGu7lTPSpDHDIXlNZrrVVcUS298D6bceLnfhs7KSCO9ujWQiy84g9x925/I8oUzyJ4dfyM/H9zJDG2GB++6QXTNR4NNgNH8H83IyQ/I+ACiaeprnjqkd9gQgJeNiabt8tJCcvLAJnLVmAvJ7HtuJuuXzyUH313v08i+8H8jTxH9zd8HhQCKkkcj5oiOjfdLHcMjJSNb8xB/HndhWBhfJBw957STmIZxwexmsnPtE+TbjzYGbGgjfPPxVlLbtlKUvlYXDAJoZVx0u1JU2S5g4wOQVauv/eh9N4YFAXjpeHlJAflx3ybd7x35fFfQSPD2y8+RpMQEUZAozjECKAJOzR9JpW+tE8YHIrjQKjJ3ZDf+umdn6d5+I+IeObgjaAQA5s+eIloK/uwIARTp9gFeDFHZvpMjxsfWkb/5UOj+AwVyFNT3XFVeRP69f7P49w9sDyoBgIF9db7I17CdEwSYrUuQKKpwzPjQA6qvXVqcJ73xkV/IjwlyD80+c+TQzqAS4M0tz5KoKJ3YtDkgAigZO5qkjaSUdCbgtLvPx1YP3j4cPqz5EQbKWKR8yU6Anl3rNffcrrqU/PTJZvPPhWAWmDD2d/x4Il6THQgBdvLx7pLqDobn+TjSxakeDkYQ20d4l4/w+QJi4Q9MvkZa40Okwt/zwofusPTZYM8C/9y7meTm6E4O7/eLAEqiJqd+KWFvM9S7EHBCwwcZF1Py2D/ECEsSdO2kVS7Xt68kP3+6xdrnD2wL+izw+Mw7+PE8YiYgMSPAIt4wmAGcDntizce0z5+CyUiC55+Yprv/Zx6dausawd4RACd2O4G/z8dsEUDJz//FKSMjto/wLiJ8CPJgu4Stntrbh7FlJwFEKur761Rf49d1jhx6NagEWPHUTFHGUTs7BLjdH0NDxoVBgpgD5/k40sWpntWDHZlJsGTunbrnxYzg7/WC6Q8cPrSLtK0q02UiWyKAUpbFtDIHMnaQtAHdPqTbcIycEnDKSAKs8R1rtZI1+AKBXXdrUEkw485JoiSTPCsEuFAjdcpIJaPOG04m3zCWJWqGIldPFhJ8//EGJgKddff1upfAKcl6sJYDnBOkpegSTcZZIYCmINP4S84jb21aRD7bvTKkgx9KEnz57hqyecUjZM69N5Jrxl5Ahg3qxSJ7fCaQByd26+jo3z9y8JVQxQU2mhJAKcWmGfAXl8xkBPh6z9qQT71OkwAyrhcWP0Duu2MC0/T169WF5OVk2vZ17Hr+lreIDi8JH+xcyZNYtwzwBJinWecaapnxAcM4t2QkwH0igwcl4f4yaSy58OyhbL1OSUp0bOsaVL0CIoaOEWEXnc16my4DPAE0RRjvmDiGGR/roJseuBEJ7mm+mtX1u+HqUeTMYf1JbU25Tq7tRFBK9H2Ukgvqc4MILGbgx1Ey3QUcpssKrvPw9EmmywBfflWjS9v+wmOMAPtf/5vr2zARCRwVXEZEsMxlHE6hrk9BWTXTOULwUlHbSZe3GNLUdbo8wKCMEHAaMUNQIx/991X2fWZwSprDB7ZqPvsJtR03blgGckUEuFT9gAOaunqn/y/+8XcpAjFOkABH2UhcScvMJTmFpaxARYUFNTOfuQTAnwgH4UqXju34e79cRICF6l+aNH6UlwD/+uAlaR4G076VaRv1+nByCa0+6vvhEMvfnAVv7kKsNnehR2N9WBDgpj+N5sdog4gAGtHHc09M8xJAJok21nxdKlpyqm7adkqtxFf64v/2igX3Sk+AbavnioJCOV4CKFp/7y+kpyV7jf/25kVSPQwcPt74wTC2EWLjtPq7xob20hMAkUzBdneEmgAauffg/t29BHB7B8AD3r42NbsopARAwWh+Fnhu3t1SEwBCVf4gi+I6NQHmq3+INcNDgI92Pi/Vw2Crp75XTPuhJACbBeITHTkVDJXx+TJ3CuaqCaBps7LokcleAux/fbk0D4MgD7/PDyQlzf9MJt3bRJ6eMyWcjA9sVWzPmiFpfujZ/wOfvLFCqho9vLcfLIfPbi6DLWWQ+8ZnqmEPAXryp38e47NDoDdXSvNQCO9qM5PiXDG+pyK4v9pACYzvAaq7sNZqGq9WTYCDb62ShgCI7fMKZbcIAKAYhvp+2leX+VYHu2R8FLPi4xiK6lu7Axh56kANAWSo1esBDnZ8FWQIJYor29vOD3DL+FBsp2bm8D+D7VlTRe83rx13kYYAn0sSBhYpcs06eIQKCVz9Hmgf3Tg59WV83Cu2zNzPUeWFddT0fvPOm6+Qdgbgj3SNchRCOgtU1eoGfe79N0lnfAAvDPc7K1op7VS930TnLRl9AIg5+AcMNLbv2CzAlbitKC0UZgm7aXyD7SvK0rNeuscyXCl7NbuA3XLsAnDyxp/qyWB8ADORG51O7BjfYLZ6S5f+9dTDf9EQQJY4AGRcfC1CWQjACl2npGmXp6K8oB6i2TU+q8ZaXSdqY6ct8bps/jQNATD1yliXD+f5MhFAMLjkwanXSmN8AFFT7jN4+VkLde83PSLQY2cBy6QgAASc2k6epVIRAEBcQlM8Kz+bSctlMD5Q0b6B/9z2VkrbMkMCvP/Ks1IQgD/OVLeakWYWqKnXGcZJGXsgxheV46N4uRVf+Gn5/OkaAiD+7rbxodvnH7qitpN0BBCVvMXp5dfvv+S68Q2c1Y0gwGdGJ4Ee/Lh/k6sEQNIGL+CU0fiisveeQ6uighxyUp9u5IrR5zDfAJ3Kre6wnDC+wTbwpVZKRSnvNx+fcYuOAN98sM5VAiBjR6NYjk+UlgCCadYU6WkprOII0u8QhFtGZ2CIcNQni0i2DdT4R4WtupJya0CA19TfnDXteh0B3FYFI12L7zMoo/ERmOKPiQMpF4uE1P5NjY4YH0CrO+5aq0CAzepvntChmsybcauGAJ+6HAxCrp6bMrBAjO90ooq/xhf1ZFDK/bdaLPpDPRrryIJZzYwAbvfuQaKm2zIwf4zfoW0Fk9RDVYXKYtgRjB11FnurC/Ky/SIATiD9vc+0rFxROzrWq9bwDzZ1byCL5kx2Te2CfTSfpeuGDMyu8XEe8OGr5i8OKqSgUgoqpqByCnIOcZrIdyZzaveDMvbc9e4BAS62wryhA09kjZTdaNIkiwzMSeObASFkVFbBjiwjPcVW70U7h1bIEAIB+tpJlMR2BL10Q6VnFxVnkCEGECzj8xh38dm6LbCoNY0VCNrXnywUhYJlUdExpiXQ0UsX7VSDWZOHL8uibjqNXL2WbnwA1+MdSX+UUCjuKRjLMk/zJ01379zCMlJWU0+y8otNiYC1Ge1UnYwWIvVLkMwoBDRuSNdqqcY3OgjDLGC3WiscZ+6ev2cNQBRp8CbNPjsjizlaAOLbmXTbFSnuYuktA4eOmoFkEa1edD/p1rmDX54x0rWQsdMSje8RwyAuoJkFsu016kjPyuPve6U6MWSq5s2Ki/cSQE0E7L+joo2JgI6aaKpoZ9uIkGivbubRM9TkQVkWeMmmRIhPDLh9jWzG92D8JedyyzCdBWqtzwJ8NhPFBDUBhvPOHg4OeBIA6JOLiJLZjIA1C9OWmZYAFcf4I14eEIHy1bhQmQPFGUyjaNRQTp4Wum18T4OqhHitE4dSvdbuv1FUV6GjmgAZ/MOhFrCIAJ61B8WhswtKWAt1wzcyJpq1VvtUpSrCvndQ3+6mBkSuna8ijJCIIT/f7DrQ7QcSOJHF+EYhcTjjVnZERfpDoK+w/vP1Ad7jO4KICIDDDk2VcMoulIGPMCFCUmI8S+o45aQTTQ2G9Cq7FbiQn49kFrPrQj4GPVw4Gx+AQBdjqVFGZflWRmHXwD3D06ICETN5x0pEgKMFGBqF0wy6aBv1ATAD6u0jrSqQaCNStDFzmBIhOdWylFw243swcfzvtbEZzAI+urfwGUwoByQiQD+dH1Al9gPM/iAGDnEEK5XFEeNHJo2T6VTI0sVMYlrTOCWNafjCzfgAEnX4/AgzfSSepXVrXcHLShEBIii+5HsCGs8C5uFYRKsEU4+3uxbi38HKoMFMghkFuXpmRICGD7ubcDG+Uc0fGBh6P4tHwPvN6gQ+zEfcjAhgtU9wEieROu/0QSFLmsDMghkGByxmRICMC0es8G9kN75HIpeemmypi5sgIXSuGQEG8Q8PFYm/s4DoBKp54mWuFJZAuhYMadc/kc34HmAc+SWbd9BFyasUfcwIEKlsEY6tl8lpAc0C6KKtvh46arqZPYuMHSRthLPxAQhNUctBq5TK9nX8u9tKtfCp/PpSQrdQ/s4CfAt1tFN1e/Bw3AphJnT7RsaHmENW43sw9aZxhrMAooQC52+0FQIUKy1GNHFnIwIYOR+iRAQELr79cL00AwixCVQ6fOEpRDJlKo5phG/2riM5WRlCvSS25IJGknFWW8Zo+gTjRNCIAJ7wsJUIFLx/WadTPlQqU3EsM6BjC9/TGTaJitad/U+10zOotz40XGZIAN758LaF57YgJw/oKe1AQrevvldo+MKBAN99tEGnL0TtJEHTqGK7bePe0MwCdEuIPbOxQ6g/meJLklx16XnSDiSSNoKV0hWKIto+HNol/vQN1HUMN/MFRCFiPgQ5++6J0g4iMnbU9wr1brgQAL5McWGuGQH6+Ns6dguvRDHbEfBLAX9kvP65WdIOInYF6nuFdDtcCADg5TIw/q5Aegf3EIVPzWYBzzkBQsH8Z2UqOScSpmhyD+i6Gk4EQIyjrDifH3M0/u4ZaPv4JbroYGmV+a6AGr+EK0eCoIXMA4hETf451Z1NZQdiFqnJOtXP7IDax6tKyf9He1Qcb0oAAIKSYLZaCwaQqKm+Z4hXwsH4OPwSqKsOUaQGTACFBM382wHFsBkBUjO0O4A/nH+q9AOJLF1Nwaz7bgwLAky7bbxo7T/Xkm0tEiBaqSilcQghwDQiQHySdgdw1y1XSj+QSNFW3zPStWS/59fXLWDSO874q63Y1TIBFBJ04UPEiA0YiUf5SBTy3mUfTOTnh6w/oENnGg11OhUUcjzKHCeAQgJdIinedJGEnA+tvrd1ifQEAEn5sq8y3+91V1wkmvqvs2VTmwSIUapL6iRJagIUltfo1MEf71omPQH4RFQkvMjUMEuNJ2c1i6Teu3GkHzQCKCSopviWJ0FO4bGzAl6GVFNZ4i05J1NTBZE3zWfgIEtXtvtEboSg8AR0HBW27Wn3AwoJhiqtx4ROYRqXhjSkfw9vtZG9O551vaa+GfiEVKRoy3R/IGRaik62dthXwMdRAigkmKRLDYNTWNWBqYj4uLq65Mye7UtDpgu0C74mD/LzkaLtdpUUAEfUgpg/on3n+G1Hfz+okOAZUaImn4d+963jdYWn3t36DPlOInEICCmqxsWnu7nly/xzz1pWv0lwb5MCsmGABEjAYYMvbd3Sx+7UEcDTlFKGvsQ2euwwhxYnh5+EsKE2jG+QR/loIPYLmAAKCdB46AOzYhI7184XEsBbkHrXctf8AjvG12x/42LJhDHnkwNvrgr6tG/w5q+liHKdAAoJCik+Eg1UQX6WqfE9eI9uwUK9JJhV4ISkGiIXZDnBwTXMMkqIYxHDg2+vDorDZ3DO/w5FiiO2c+IiCglKRCRIpR6rqPys0ZKANnWh2CraKb/qyXIyI0JyUgLra+xUjyVs9VJTkoyMX+SY3Zy6kGom2KOr6RMVSW6+ZrQlEngcRKx7MjVb8NTZgSqKl7prCJ+cyJ41kKNkBHkMCkyuderNDwoBFBLkKHXodQ8wfHATeXXNPMtE+HDn8+SHjzdKYXwNEejSgOIM8G8MawCnJrPsHTuVwiHtQiUQQYSPOXxOrPlBJ4BCgiiK+4UZwWVFLKffKgmwLKBtzY8OhGSdqrrtzXuo7cTy81ubEAFCmCk3Xm6aZ4Alb8Oyh0i7qlLRNX4NdKsXcgKoiDBSqUalzdOPj2PHw1ZJ4CHCfrr18ndGcNr42gSYTuw8RJCJcyxUnpXBzu3ViTE/UcMfeGsVufbyC9gyKfjc4UCCPK4TQHV28KZoUCDA4BtUWAFqD9nZMQTT+HwmFLJ0Dabwo80uczLJvc1Xk310e/fikgdJZ+OSeF/5G96VigAKCeIoHheWmKOeNYQY6o7lVvH+9qWssaVZQ4tQGZ/vGYBETTMi5GZnsJnQ4Oe7/TnYkZYAKiKgV+3PhmvlTeNsk0DtMMLzVgeU3DC+ngiZLF3LYoAJYo7r7B7phg0BFBKcQLHXaBAa6mvI7GkTWRlaf8nwwSvPsmVihKCuYLCNj2xpVBlB6jwIcFQf0ZYdlPkw/mo7Sp6wJYBqSQDT/2U0IEgkRSkUO9tGDxBBG9S3m9D4kKs7VW38qLEbWaCoQmVwD/C30rPySWRUjJnhD1kVcLYYAqiIkEoxhe9dyEfY4COseXpGYMaPiGT1cvlMJjhu8OLxxmJ/j2APAKOy/+N79GcwMH4Pv88bmgeqquBI3GxXoGzvZluRbrdYAqiIkEfxVz7/gC9M3a9XZ9J8/WVkw/OzAza+04A4NjOngETHxFlZ63eFwsMPGwKoiFBBsVB5O0z7FqCCx9g/jCSL50xm/kKojV9SXUfyispZBjS6mPl42z0p2siy6i3VmMt0MyoidFR62/9qxXvOykzTVflw2vhYz9GuFhU4UEy7tXXP/itFTV0k5VjLeFMqIuRTXKlkKf9q98we0zEKJaFUOjKZ8Mai0QR6DMAzx5Evikoj1xGpbNn5JWwaR1QvmW7fsI7HJSab9kwwwesUoyhipR5jmW+OI0MBxXil3+2vfhgkFPiYYg5Fr7AZ13C5UY4MRRRXU2xzmQyorLqI4o8U5WE5luF404KZ4RSKa5Qj0618Q2wH8b3im4B89Z6S62E9fuH+ACbEyFU6oo2heIDiRYp/UHyqOGY/UPyinLh9QfE+xQ7l95YoZJqu+CDDUGC5JRicx/8DEyLaClD4WBsAAAAASUVORK5CYII=",

        security_facilities : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADnwAAA58BwyKAIwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA5CSURBVHja7V17cFTVGQ+GBGQsIhaKGMsrCGE3u5u9N2+QyEskVMZBsJQBq9ZOyVQUYUAoWGpbWrBOp44IRUV8VJHhIfgAhBleQsXyfskjhFBehopgQEQIbL+TfIHN3e++sjd3z549f/xmGHY395zv+91zvvO9TlIoFEoSDV6/0gTgBQwF/B4wGzAfsBywCbAXcBxwFlABKAXsAGwAfAJ4H/AK4BnAzwAZ7G+KKCsRlJ0KKAI8D1gKOAioAoQcxlVAORJkIqAAkCIJ4L7CbwIEAeMBKwHfNYCyrYI9ezVgCiBPEqBhFe8DvAg4FUOFm+EIYBrbfiQBnFF6a8DTgO0OKegaoBJwDLAH8AVgN9oAJ9Em+MGhZ+0CPAu4QxLAvuIz0Wi7Ug/BnwF8BpiD5LkP0AHQgm0fFp/fDI3IQWgIvowG5Ff1GA8j1KuAuyUBzAWvAj7AN9WqgI8C3gCMBNzlwhg7Ax7DZx62aUQuAmRLAkQKtQDfMKuCZMbfE4BOHIw9DfAU4N82yMAMx0DCEwCE0AbwrkWhHcDj150c2yzt8XSyzSKRZwFuTzgC4FGuBHDOREiXAfMA+XF4XFXQoVRlwWYZBUi2uerMQZ8Hc3Slxg0BYLBZgM0mQvkeMBPQTgBHVUecy0WTOTNPZJbFvzlX89sR3BMA3/qpJm/EBcDfeD861XP+rQDTTY6Zl5g9YeFvaZ1fy7kmAE5+lckbwGyBNiL62zWySAcsM5HFh0a2AdoP4d/fwC0BYHCFGHjRmyzbx/qIrnhCLv0A+wzkwmRWpPNbrX9kE5cEgIGNNXDmXMItoUmiKT9MPo0BkwxkdJXaEohtZDNXBGAWLTpK9Ni9P5785S4QIQ9QZiAvFgNpFPZ9rUG5hRsCwGCaojdPbzLMxfsjqfgIud2KstGT24La1RKN5fDPtnNBABhIc8BaA5/4b6WyTWX4mMGRcT3gNgxo1Qk8xZwAGLnT84CdAORIBdtyjX+tI8t9BEH2xpQA7MiCrlpqwIeYi1Qq1rZMu2B+gSU3ecwIgClZ63QGxuL4raVCo4qVbLVAgNJYEkDP2mekaC4VGbV8b8Hop2EmUkwIAA+eoDMgljjZVCrQkgxTAGNwq6xEC/8i+kkuE14/Cv91nQDw0Ad1EjfYsn+LVK5lOQ5zIP3shKsEwKxcKhuX5dWlScXakuU7DhBgm2sEgIe11fHts2VLlUq1Lc9BDhBglCsEwJDuJp1s24d4F3bXnJzbPT51pMev/tPrV5d5fep/qsnsU45m+pXPPT7lA/j3TK8/+HB6bm5zlwjAZPpLrErai9nEbBvdAvgcsBEdQGswlWwl2lgsYvgvTHpt5BYBntRh4CRele7xeFJBqaO8AXWdzYoh5rlckRkIDk9KSrpJ1BXIbvJjJSGoN7mcWFJSI3jbf24zc5dGQNmZGVAHJDoBluokanIXzvX5ctMya5ZPpyt/Vno8+S0TjgAw8cE6AinibULdfEqujfKxc9X7rq/av15p8TeHfD61S8IQAMOUJwlBzOVuv/epgzGhVDfLJtOvsiqffmAbRPgqFAXmGlCKa4xEwyqgbzx+pXuiEGAWIYDTgJYcvvmXdBRW4QkoJUVFRY2t/r309PQmQIQxsDroReXOeILBdKEJgGFJyts3nKdJ+P3+O/WX/eB7XboU1jsBpXpV8CuLdUjwZSAQaCEyAdZQhhBv1r6OwXfN6wtOcuoZsDVMo0gAx8SlQhIACziohggdeJoAO6dTivEE1PGOO210SODzZfUWkQBvEZOdztPga/bpyKSJTJ8yr6FWG53tYCv7TBgCoL//MlGnx1VhJhhoowllHGvfvqjBQtHVNgFpGAaHikSAP8eDxw+W5C0Rb78/+GiDHzfZ6SBCPuoyIQiA3THOEATwcaV8b/ZdhMu2dMiQIclubD2En+AirA7NRCDAbwjlf8phGPVJ4u13zUZBZ5HmRKAO4jTk3EKvhFz7xUZYtaMlQD/+JqUuIiz/Qhe3n+KI5/uVFzlU/riwMY4zI8A9VLcrHllNnP2r3Fj+NQ4irR0wnzPlZxB5G4VGBPgLQYDHuVzWfNXNoeqko8VAwHUDSAF1HWcEyCf0+YIRAXYS1ak/5jHWT1TIbo0BCbXl3Qc5I0BbggC7SQKwMz7x5Y08vv1ohdcdqy+4loNt6BSHNsBOQq9pFAEej5dUL+bo4ZQAX3FIgBmEXn9FEWAh72d/SYB6EaAXodeFdQiAnSrOOVVtIgnAFQFY7eZ5IhOqcTgBqOPfLEmA+CcA6pdqStU9nADU8W+gJIAwBCgh9PuncAKsIo5/zSQBhCFAR6qAN5wAZU6VG0sCcFuCVkH5LGoNQG3FzCpJAOEIsInI70hmH3QilodXuCzzCgbTPT51mKfmgijtmMu9AXWqq4gskr0ApJgCY3woIyurXRxUIXdkH/QlPhjDVeAnU+lqsx8/L1jBqpQ4IcAfqCivXvyfmxMAvGl9TYo9eMdZrzfo44AAI4mxlbAPXiA+6MqD8jHkeiyOlX+9uJRVKceYAIVUB9IkvMdGewRM5WLp9yvPxb3yryerBJ/goOOYdlxLk4gr2Y5xZLis0A56fEFu6JM+BaHlfQu5xEcwttH5uRQJXuNAntoWs7uSiBSwMo4I8L/wsfUMqqHjxT1CJwfyjXIYoxpQtdvAFg7keVDbtJt3AtQp9nxAzeZe+bXoEVQjOqRzIM8DVghwWBJAWAJYWgFKJQGEJcAhKwQ4JAkgLAFKrRDgoCSAsAQ4bIUAByQBJAESggA7+3cPLYFz+9yi/NCR4oQgQBlFgN2a/zyfCARY3LsgFAj72/cqamgh/J/gBNC61Xew//yY8Fr9RGQCrO9XGAoGIt212VlqqKy4u5AEwORQbd7HEvbBSwQBCkQmwGxY8vV89vPuzReVABl6waCniA9GiEyAaT1ydQkwo0eeqAR4QC8cPJD4YKrIBFgNW4AvQBNgZd8CUQkwjpjvfXpLw9uiG4HPd49cBdj/iWoEwvPnEHpOr73p85pTlxHHCwFYxC4v64aS+sAp4LjAx0Ci5yMzCFP0jgcVieAH6K/eUNIvcrKF9gPg5Z0Rqf+1H1L3/nWUBBCDAHilr3Y8q8MJQB0FSyQBhCHAg4R+Z4QToJjKF5MEEIYArxH67RVOgGZEq3VWUpwiMgFGgNJr//ajeWISADu/nSB0m6ptEPEpbzeCNDQB1vQrDPlZs2fAe73yRSVAltHqHv5Fov2p8lfRw8H7B3QPHXLI/88pASYb3TNo5iveIfMB4p4AVEldB702ceVEY8E2kgDxSQDW4o+4fPqAUZ/A2TxFBiUBopYf1fntH0YEGED8oJskQNwSgOoR2N+sW/hLYcvGszyVMt0fJwQ4MbBunAGx12XZ9SV7KPiVZCsXRrBCwphfiYaXO9eZxILe+dwrfy6ZcKIuinVdJeAZSzeGcNQQeiYVs8/JUkK5AZVLZEe++bWt5Ce7qHwvMYZvWUwgrgjQza8yJ8YVAcrDKwOBQHsXCfAGlf5l+c4grvoC0f2A4qs3gO9Gb14XlH8H0Umdxf7bxSUB2CUQmWCMGlwLy/Wb76bykQDUhV/zLd8ZxCtYoyi2j7KbOrFP/35OsdfrDy4E0k7wKMpPXVZ+O6IJBENO3BNAwhIBqBqPDYaXb0jBCaP8h3W2oZ6SAOIr/zbiHkOG102v35ECFIIAVMbPaUBLSQDxld+TSOtnGG7l91KI8a38JkTjJ1s3vUpBxjcB3iSUf5E1AJcEEF/5k3Ws/ol2/k602ab3A94FfMhuoACsZAUHWIa0nt076K3pqb8FO5LuqnGUVBeiPAK4SSqzXrIfqrPv77SbyR3NIJ52wFU6SCrUttzzdLqnnw7P9XODAHscIMA7Uqm2Xb0VhBwv1Td1L5rBnHGAAMOkYm3V9+1xWo7RDOi0BQVfxbtpLqF1egETEw7gFpIilWtJ1s3QtqJk/Fw0fzuaQZ0yUf5HPF89F2du3o0NtYVGM7DjFlaAzYBWUpFRJXfs0pHtBuYIiiUBjlrc51l/2s5Sobbl24lo7Hi9uIMVfTjxHCe7Tp4nulHXgl38kCcVa1m22QZb7HYn+zg62XueKbkVLvshHRflI1LBpnIdTeT01YI512518nnRDPRL7fVoYRbrMoMtYb7TkxBE8S0Aiw3kxmR6s9PPddIRVBn2WTK7ft5gMuW115dLXF/yjxjI6y12xW9DPNvJurPviO9MNJhUFd5mmZrAir8Z8EeDJZ/JaAqLuzTUGKIZ/DbNYH/Q+V6xidOI2RLFCaj8QSZv/Uk3OrREMwFt3V6VwXfb6LSgCQfLaL07QY53H5vIgsmqtRvjiWYia822ACJ8PA5dw3oTZ0vhDBGdRzCnNMDfTQpcqjDO71qYPJoJaS+dnmvxdwpgn8kbcBGNyM4CKL4Lk40J8UOYM5Hj9viiDVBMR4cQ6yySZuO3KaxUGXDOQjBpMS/3F9iUTwHey3zVZI7fsKZNsUqOibWQWgNetSCkEF5t8zs7+W4xmE8GWvWlFubDMnpej/V2x4vgghjcsJpH8AWuIO04GHs3tG222Rj/Kl5c47y9Qfegx+uaDWEyp9LbgF8Durqk8BLAAp3sHKPtjG0JKk8y53Up7YpbQ31Kwk9jUir7/QTAYIAfXa3JFp7dFGMaCtbbMat8HuAzwNf1GM9lbNjQlUdZ825ItUEF7HGwZv97VGQ5Zijvw/sSzjrcjWQbbg1teZZxPFnVXjSwDnLcEOIwjjEjXuQar2frLPSRr9JpiOAWvsV6iAm87e0JUxmEPgWWKz8e8xCP2zQireIKZuIsAYxlCrdiU0gCxC7K5sGAy1j0Ki5GH/smzLMrQ4OxAs/t2zHhgr3R7wNexjsVWffU9IYKx8Ya/wdkRPdTmKgdlwAAAABJRU5ErkJggg==",

        hold_meetings : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzA3NEQyQzhFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3NEQyQzlFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozMDc0RDJDNkUzRTcxMUU3QTgyOEYwMUNCNDExQ0RFRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozMDc0RDJDN0UzRTcxMUU3QTgyOEYwMUNCNDExQ0RFRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PsGrDmUAADbzSURBVHja7H0JeFzHcWZ19zvmnsGNwUkcPEVSPExSJ62LsiTLkiytHa0de21ZdpJNNnHs3WxiJ+vYOTf7eXej7LdyEkeWb3sT25J8yLJsnbZMiRQt3pQIXgCIGxhgznd2b/WbAQkQwOAakJDFp+8J4GDmvTddf1f9VV1dRepfGISLcXDKvJ/rDvz09qYjL/2hZqavBiAqZ8qxnuYN33xj483/NFbZkFCsHBAh5nZRQgDkdRmelALYFoBpACTHGIwMqJBOqtDbrYJja5BJKzA8QPF9ivzkBaconBw45xCKuFBR5YAQNsQbbQiELKiqtSEcdcAfBNC0/P1dN38KDm/Wg1wMADiqDoy7sOXn3/5U4+u7/wyF7hOEFv4qgLkOGP7w4Y4NN/3XE1fs/CEOPIJghkGVgh4XuhT42CiF0WEUdJfPO7NpFHZKAdtWwHVIHiXjYKFQEHTRMZH3RyDAuQckRICquRAIOngiGOIm1DUZUFFtQSTmgj+Qf78EA37PywCYcLiKBvHOQ1tbD7/w2zVnX3/QUbTp5YoDp1iGOL7plgf2X33fo8w2J890Rc3/zKQAZ7IGpzv80NMZgME+P1gGCtulHigksCjNC62UhxDEm+mcE0/YeVA4UFZhQLwpC83tOahBYERiwnsGx54IorcmAFxFh8aOPddve+4rP8Z5FJCaoOjDCKkNbHP3ro9s62m+8iATTl7wZg6g86QGHUeD0HUqBKkxHw4w8wTOlNILez6g4C4Bx5EaiaN5sFAzpKFtTRpaVhloSgqawQEQYlkCQFk6aBEg+MVbjrz4AP4Lhe8DqfaJmFlFCk9bC73t0POf7Gvd/CEYTlB443AQXj8YgaG+AA4mCl3JD7Y8L/n0QeBJALLCMJqGDh1HfHD8cAVEy3PQviYFqzemoLbBOWeylhkQlgwA8msy7oBqm+Vc8i6czYqdBoPFQGEzAQG1LNPAHR2pF9//RpCcOVGNKl/3BlhRuUfSlvMhTQ/V8hLOJAOw9xcBOLC3AolkCq7cPgrta000G8sKCMqSakgUKAo/4AoK5dAPV5efhGOZajhlxcEkIRwvOk7RgAsJGBM0hUAulRnhfQdiVEHCoPvenBSboobSmRQ0g66TZXhGEQhpd93mYb5mo6lK4rgMgLCEACB4cQc0NxuwcbJH9AyEfBS2aMPQZiWgKxuCYR4DQ/jlvIcQTUNcH4bD2SboInXfA1Ulc/cHlzPL8sii9z3c7tOhiGOW+42k3R+tPslWXWF7HEcC4dcNACbVYNvYrzbfRZ7d+pxbA+3BBNhowuVMD6EWXK8n0aSPgSvyKkBF7ZlzGfRlBuCdK8QdXznTvlsYtqQF5E0PAsSxYzucByPm//zj3/2L9fHyDTf9/ufuTr/60qh64x0JqG92PPfRvfguJIt8+I+WBgBMh02J167Z2fvj9xnIhGuC/JwkvYiL9KpAmgDinRx/l69rwoJGv1H7bKbxG64p3Dc7AFzHkYQV6tvborWNDXpiYGhk/4nOPSmiDPmFG07vfiHoDg1w2tBsQCB00eMIS6YBFCR5Pf66Ewb1Q2vUBkrItBGYia8xFHVjyIGDInzScGiGEVd70056zoXDBa+qrwtXNTQyc3SEnjl40Dicyf0cv7MI6Jq/trlJ37Bta93ZU6dDg1//whBs3znANm23pJoUUhuQNzEApAfQE2zsMPyxXl96KG6DPrfPIW86QGp+ZptgIR/US/U8NoeMuEgxW8lc/JGo3tLeEhWOY548dDidTKYsRVGooue/kg2QOtVxKtnb0zfcsmpleVmdWtmz9/lQ4mxnB7v+VkcLBC5KiHlJA0Em88F7Bn90+337Pv81yzDKRWE9YKbDh6TxGX3lt/+ld80f0TFDl2GDEjkjtC2ivMPHSOhi0Eo0dmJdU/yGnGme6urt72SUKejxEJgmDC1QyLbj2k21VSuqIqHNLx87+V1/LEZXVobXIAde8gjX0gFAqnxNh+wbHXDb8X/8gwftn/9vU6hFfAaUEzLBz+R23nbytHJUV7m/RPEIjo/CPtgSeKXGxyKm7UDWsi+C7c+rcMbY3N7vctT86DAozFP/nC+d6FWKGormTfLSmAC56CLZ2+5ng/4Xf1b9Qizygxsbaj7WmhtYZ5Hpb6kLB/ay5mdOj/iOaszylc4ZBYrj6v603/y4LtzylpXt9b9968aPw0WYXcsyPIFyOTKSfu7HXcM/0ChhpQeAvKTUdk8/EYNf/bKG6j6SS0PyB1b7Q7+vDD9MXDnwZMrst1XN/WGm5WGexjew0tIfCYLOlPt0OpvLRrbEN1xz7bUfh7fw0ZuAF75/eOzzEUaBlpZSoorPZQAe/0Yl7H+5Fnx+qQm4Tzih3QMVP9irNf5Eg6lujo6vPau0fePwYPglnbqBJWG7FAI6I2Em3CC8xQ/XMnXdsUBzzBICYFz4T3yjBl4/WAmazsdJj7c6mnTcPcmyp9g0d5R/70jr+3nOFb8GYZ83l0koofAJPP71Wjh7ugxnPr+QiQniGpVW72rHcr3Az3muSMAxHKjPdW9BbBogLgvlzQWAc2r/6zUo/BhoUxZvhGXamY3XbbzvXetX3jM6mgLunA9yuMjKR8dScNv2q2/ccN2m+yzTygBchsGbAwCS8GXT0wtf4GE7BvpcI3R72wer79r2xTsf+HxN/bXvgeTwMBipnHemR8eg6eYPwe2/+dl45V1bvsi2t79ffsb7rBCXgbDEx8K9AC83D6fxT5+ohK5T5aj2PXYnXO6A4+aYpuhKc/VGurH5XvPK5v/Qj1rCVnxw1Qf/FvRYDbzx1KPeZda/99OwftdHQa6HDeSyoNy+5a/1uorV/MCZ7zpnhw+7pm2CwvyEUeWyuJYLAEgh1/LpJ6Jw/HAl6D5HWE4OX+FqRbheWV13M7mi8R5eV77NVZH2mTakHQeSjg0VaDJW7nw/wLGHPPbXtPUO75JJZKVp2wvQMHd7+wN0S8uH9LMje8Shru85r/c844ykzgoJOZX58N70suguJQA0HWD3c0HY91I1UC1JGQsqbbXXs/VNd4v22l085CsXMhfOwROFL9f5Uq4DacGhzDIhu/tPYW3N2bzgd/8ZhG55GNCQQIq7+TVB/AxHIfPGih2kuWqHsnPtiHqi/yf8UOfj9unBV3nOSgqFSq2gLSe+kE6nobunF1G6PPApM9yrqiqhoryshADQfABvHFLh5z+poYEg1W7e+Glord4pqiPrHS833/EEODkQQ8DEh0mgFmhEDSDj3+NS4yLvFSRQA1j4HjruIUjzb7v4Phdcn1oOm1bcTzc23a/3jx1yekZfg18cfdjqHz1NdDW0XACg4cSoqqyY5OVcykNSqEDAX0INIBl/YpDAz74fF7brKm1VW8V1q/+jjHsTmfYDM69l2/gwQ7YB1B+EbORq8I/8yKMQvOoGIKoGQ9kUWPge33SDxwtaQf5aHV0P9eXrldbq6+GxVz5hH+/5OehqdDmMuuu6kM3mlhUANJmD6CuFFyDVmkxWePqJKkiNBQlVTHZFw7vkokVe+EU+WgDAiEyfxqMipoI/HABfOAgidcp7bRhNhCPmEAeSLmTOAjvib2bvu+6rvp1XfIy4Iicc1wK4xGEkAucSXJbLOduIKPPQb2j3nw3B6TfKhKpZasBXLdpqdwk5++d4iUEnbxqGT74MjQUuSXOnPWCN/23OY433dSgJ0Ns3/YWvqXKb/aN9f24n0v0X2yRYRqpgGcPA0AT6fD5YJgoABE5OZZbVyLkBQKqRMx0MXnqm2iOAFrp5bTXv4LFgLZnH0uog2nkp7NpqH1C0JJSp4I4hAEwThlxn/hNOZs4YNjjrGu5Sa2Mb6Y/2fdo62v0zoSohQgkrxSCmh7vgzMv/Cr5INZ6VkOw/Aa6RhmTvG2DnkpAZPANlbVvh6g8/DDbyn0RiFJUlKZVCye9SW2BiiCSBBEHp9/sXAQAJZymcF5+uwp+63CNHkanRdY13OvMg4PIywzLNycjBQMc+aC5XvUyNaNABw0zCsNQkCx035Ad2NLCC3X/dV/273/g/1rOHHnJMO0c0ZVE5BbnUILzy6O9C8sxBoHJLm8xxwO+g+EMQqm0HX7QGylu2QkXbNu/9uk+HeLwGSpXGKOS+SZzBfp9vybSEMqfZ/8rzQeg5E8XZ7wqX20pVZIVoqtw5H/UvycYwqnkHf4nX+IBbKH6mgJ0ZAJbpgxERXFRYUpoEz3W8fs3va02VO+gPX/203TV8WOioDQozCZ/XplwUzcHOoobSKAM3MwIHn/hbSHcfBS143o3i+B38FY1wze88ikMzeWExm81CZ2d3yTSAVOE+vw/aW1suEQBkqDcxROCVF6vQA8inNDluTllVdxMP6UFizF39yyX+UYHuXu9REH2n0T9VPXIf1JEcDp2A4fAVwBZrPKWUpUlorNjBPnTjd9jPDv61/erJ/+dSommaykRQbcyGlfYX0qPokVgwiOcAmiUJzGH8OSR/4mtfWLUVqvY9AT17HgfNH7lAKC7oofIpwpdHJByG9Ves/TUKBEm//pUXYpDL+LzlXfQrmKb6yNr6d3GXz0vRSf9+xDLBDbpQVabmN86iZ0GFiSZgAMb8a4CWisRb6FGoLEpv3/zffZtb3m8R8Cu6pqp+tfI4o8H7Ow4QW/DCUkNeYctJK++fQwH32yasrGhGjvLrH31Wivr8PWcUOPyrivGdLcLhJmusXM/ryrcQZ37ERA6wjASe7D4JlTLgIxMD5N5K5oNeS4EUenGzak5S+N8c1oiImw82mUgOx93QfN0B9I3xGvoMHFG+L+vawMZ3MksCVojsydnP8W91W975awMAWnT2v/pSGRI/ZXz7NeHcYusabuMaMpN5LtTJeZa1s6Csvh36zDXgWllwzQz0WFcArLrH+9ts5AmFajOXG6AjgWRzYwySG8jYgQSE9BokKmbDWdo2QAnEQEVV78r4BLJ+x855AatQ/Woob7ry11wDyNl/pkOFE8di47Nfxm9Z0BeDVfE7UBPMW1nL98tQ76iMTzd/GOyeT+XjOisegDSSLlsWiCjCAYTKQD8z8kv7h7/6NNvYfB9c0XA3rwi1eDiUgagSrRxLvdFtZEGrWws3fOJ7kBo4CYmugxCsbIJITTv4y+pwSAK/5gCQgpCzXxZhyKd2gbBdg7XH3y4qI23EXoDPLuWEgzuCM71+0z1w9NhXvfus3XQ37B7p8lSvWgQA0p81k8aI3T10BHpHOthLx/5ZWRl/O9u44j7eXHk99yFS7fxMn5ug0buV6cJy354QnsslASg3IxgSTPi7H908eVavvPotxAHGbX/nici52Z+3Fa70/d2ZDLVUybMMPvfCwTYKyQbfpv9UUOu2x8L5HGIKesY4azOqowkIO4ZtOvtOPk73n/mBUl++VtnQdI9YW38nLws1nluJLAgyH3Ik3jNKlW4gSQyi1qlElV6v6dCqB2ClPH0BaNH9ENd0eKscyrSzf98vo2AjMyvMfhSsw2LBOLRV74Jp4v4CBzfWN3osXR5qcxlVi91wCAXOkfBVt+dnlfx92J2bO2mNZs6e1wjI4nQ1JGFjdQ+/bncOfo69eOxhZXX8RtQK/443VlwDmsLQIxAka/aTZK7X7E+8vrKiJvOHV2/+cKvup3EEQJmizhpNS6czy05w0oPRdc0LPZcOANLtGRmkcOZEFFT13JQUjlT/tdfxSKDiwqVeObOUtNmXfP7IZ/i7t3/NW4AoYo9HCiFf7hSKQOFMHJ5DGFju7iBpY1iQqWEWojKcskx3ckbW2dPxb+y104+xpsotNBZs5EOpDpHM9rlZcyw7lh6N3Lprw523Vz4w1wGSK3xDwyPLDgByES4WjZQYAAr+s+NI0Cu1NqEyB7pslK5ruNuZTrCaAuLg8X+Fs8NHNSFy6HOrM225kX72CJ+86idtfwIBQIsFgWQI1ubAk7lu/F0pwhNQK9CQt7n21MA+HKU9Xl1ARtC8o8bQlLCrkHntC1BwTOrr48sOAOPRTcM0p4sheyFkVVXmAQApACMHcOxgxAPC+dlvqdWxNtFYec0U9Y9zkWXMDNribyMBy9KcPUJ0NTJTko6culLY9gQgSesvQUGLxsTxQR0HPUX0G+eSDiZXQmXqGCx+Pci2bThzpmsZqPz5aAcOsVgU4rXV8wCAtIUnjqpeNa58QaaCH80NtrruFjeg+eDC0C/OfvLame85A2Md4NeizJYMjxQJOhAYQ3fPFBy0ghxlplBSagAo5gGgBshZgyDPEq3yzfXQNA1aW5ov7WzHMaV0adLMJmuAjqMhrxTbeDUuZBrUpwbI2vq74cLIn5z9Odtw93R8STDqU1ye0zPmqRyBlaRI1CmNAEgLFyoLXyjtul6EkBYfAdROVsa1bPtir7XbtgWnTpdQA8wzXCGXgiPhCNTV1SwhAKQwZAVOWYRxkvrnJmup2cJrYuunuHhoX8jh009YPSNHibStlpPKpY0s8QTrzmACCGQQABmc9dWFeZzBLyhfK8YBpJdBk8YpYvOsUOhF3dunoGZc0dxUMumLBQCGMVpijTIFAEyWX1W9CpwTCjASzm2ttfpqS9ZuM6xJV6Co7pFxP4qs3FsmlEZKz1lD1izExSiQvjbV5/1b/i5fo7OoQCdjpLjcRK9c3Ixbx3Ggq/vs4lm7KzN0KzzbfMk9CG+4yQQAyLSh0x2BQvlVPlHQViTQNoWBqAzo4e6nrM6h/Ui2zqVgIUs/NZdQqwPnvQAvCjcesClyqGPZ1y9FMTXJpmuqq0rkty+PANOxk5194KLYFA0Ub+BlnTpZeHli/pjchaEwRYv4Y5KokQnTWC6qIPP/Ghdiok9OrIxhsiJmTiIrjByuVjm/SbwWVWwINVAGecFMZQHkq+jFJ+ESJH1K8hUK/XrtKLf6ewlwBHVNLaoCqf7HRgkM9vm8urfnEcCFwgJ2yNc8uV4j+li247hj2UFk55McTebVPioSVMHrVKLAK9A1dwpx+AqmQiVT8PfiPhATwrq8UbBEHECW3+0768V98iXWR4c1sAx1YtVtFL9Q/Zpf8WvBiTmJsnKjYjoDquXIqNxEl4zTmtjqYkKS2zzqcPaHKPXoEPcqhDKoU3XvbzPKX6Y447XJcq8V/CbCAAz1M4/LeR6AbLQg6+1PfIcQjuPXqh2fWjNRA0j33TRsyzJsk0yg7kRhKqmJrocixY3kjG9GYauEnmvRoeElmqRJKBbpkHsPaqIb5D0uy64kdg2gr8svexpQz/73dvvggvxxubtbD/sjRJb2njA7JX3TLTtBXG8FhxS0hUsDvjJREWoXszRJWDFNz4AWzTdbaAtERbidBvSYvNdlCS4WAEzA8KCOXh8CQPbYyaRUuCDShETPtsO+FWKy/L0AkEgZXeDw83ugOHdoWbBeBH1xMoMGkK/Kmd+k6JOWfiVc5GvjWmEmDSBCvjgrC9XLe12WYAkCAaahyD5KFFHAIJtG+0+njL8eCcTEND65lbNs9GvP/8nlNq2OrhYyXiBmBoC0/fWqhgRwgp+N769HExCkRQAgXUVNoaQqskre67IEFw0AgZqfQWJIpV4ASK79T2m7goKOBVZP9/lAxjw5US1IckZqYxvELB5AObL9SrkbaMJn5e/SMyj3PAFRLCAGJF62/jIRLCERHBnUUAMkUSLuFP+aKpRpIX9ETKPSs8lcYoJkBFUVHUnaumJNkmTwJ44zPUyZlxl0XrsLiOBrcdkppFigVMYiqiNXELzXQiKql49pjnQKNUBftzolwCKQyiks4IR9LRf2bJCJGcywUmI8dIdvJUFfuSgLthVz5uXsbkTZ6QVbP7Fhn3yt0TMNxT0BUR5qZ0G9TAh+WQss9pCkPzGIAHAcbarJ9WIAPipjABOFQjx7DzRtnEH66AWBhCtsWh5qRAJYNduidQt6AGTSxci531aos3kCCICgr5qUhRoRTZd5QCkOx0ESKJssXhCHRybvuH6thvu0msmsnniLGkbWtEnBA5ALRrQGCaDKoBgBlNu+mlHI0gMIIvqeSgx4p/xd/l3GB4puDZNEUK5BINkklz2B0hBBy2SynSq70AXEWc/RA4hyn8LMc6uA+WIDAdsdpZadMAhhhTksaG1sfTGJ5D0A5vn73t4MFOYjg93AVBXuqqjx3tCK4JCZuk6RlUEPSHgvWAARxMtKuiH3S7/l4wiEMrk3y+XptFCA0inrN4gNYSm0QhszTm0sL2+Wu25dwUHGClNjQz0DWStBiGfMBdFUP1QVJ4AiH1mEtGNDMBCBZ3Dm702NQaAsAq8aadgZiEFWuJ7gxTmGMD0RBCSbSAR9Xir/PPZhM4WoqopfOpcK9fb0eCB8KzJJRgkYyYQW1Jnfr9GAMu1oa0rAOtr1rJqE5//uUw98s13JbvIWb/BPn3i24+HH07lswKcFhAwAhQOSALaKIptFpWrPWBY8+Ppr8MmGVng6MYQznYOD6uCLowPQYRnw0EgPyN5CRXcHyf1+ZaFWFvZX2KksgnD22oGFfgHKnfW+r5WpgUY2ejz8pX/sIG9VR0Km3iGx+9OPro79gefszTTdGGMi0T+S+Lu//9Nv/ZayZ5OJykIoitg3fPU+ghNQjiuSMYdWhFeIgFZOimgAeQMFATRgmfCHJ46Ahij0owKRBaGeyY7BU+mEtyagzJITILUI3qsCSWcTjGYGx4norP4u4mDI5IcsVww4QmZUv7VJJPWGmkjyJRSYcYOonF9cbSVd261cFiw51qhyf6P82J98A1b/1VAvGVCQAKL/v0YoTGZ3Fifxbn52qxP5hgBvO5g614RHyR/wXkg615CO3t0IU98cpC/bEYgX+sxPi8tBpCmHUsTr4oGACLSx1GZXLvsXVn7fnu240ylnzj8k1/6eMmqDjADOZVT5ND3xFlIK2PtEbdmG+daTUQgELot7OpMwQwIPWnQ3QqzqsGtU8gmDnQINNvC+m2vCZgOXlRHjZZtni8t4Wy/5BRxhfL/+PB9YUvlgNLRVVZSQx+ovH4s5xMwAEMQNKXYsKozwRCGphMMpEds/klL6VI36WNo466UFaMrMeX2iBLu3C/eQewT8I5leTfaYvBwRLokJ4NPxAGQIaoIGzh4INLywKtu3PegYPtn+3U9sSAlt2LCpSW1TzX3jxY+qbbU72JbW9/O2mpu5ruogS8eJaZX3ZGLI+ZxESKTdd7lJjp39KX/15NfPnhp4hRMh81EuF41epAYg9b/x8RVAqT6dJkDV7zoBzdhRk7jtGqX7ziTRMymu9R8zyvYe7Aq8pALX830B3BwKQ3jbtDe1vMe9svkDglH/xGmfG00Cl5VCCx1EpfCr/H5IRoIwKdpYKANDzj0D/q9zcB996sAn7d7EcTQBctuXf9nUY30Tx4NQPrYCoYgDMklwmvGkQjA1aQReyYV/+ktlw5OeNbeFUIWro/B9HmhkTFhTAuPbtK3e0b9QW6pvJpWhtkkL/xcccu9xjWnBKISATUwQcfJkUXIGB//uyjpCp/reIGcGD5CgXnlZ6qWa+0IWNuQKlFe7MjUIZigpShhOOBcFjuf5GTqNTofCNm3GFNmTb2aXPG/PHZfnjDFzP60pv2qcyslJ7do2mOl8vSAv2IdQQzfRdL0t4JeP0gEABz0Y4hR/seegLCYu3s3BWYdp3ny+OpP84aD6OZvIvjY1mnv+TeNannGRuSyxJTEBDoV4owWlbLogplEOYvIr3qKOANe1bDGDjphsijjPXZZXiQ+517OiGjlAMGQX0sFKYl5luJbJKBLOXl5oIesVZKJ5S0/luj7eSeUi7XfdlAliVgSQWcq7LtACTioYR7ynuvRdC/PRSjI+T0g+DxvoktzJH0QAVMdtWQAaHFudmhe4IG/dBoVlJHv3Z7Og5izQbAsymRxYCAjDp0MuIDeCEMNnOGctMZ2DOCUoUZLYPQdiuUKYCEVNJUL2ZyfjaUkOUFNWE0JvRrZv9l1MMHBZhUGAIX/D5wpTWQWRgg95sIHj5DiCpuXQMrmEko/dlAYBngaQXkAg6MDYiAqLq71AOFCUJ7fb9xxI12TGIDCSBAWZPJX+vtQEDAGg6zBWEYXOQGTMzIiThFLBXV7Up/M6kS3qmxKOgs/GiLm2TnNur1D4dj+VLQvEeO1RMAUZTjjk1V5HeXrI1XYDoQoKYsl7ErkIOh/Y1XWqjc/lbg8zWI0sWq6T6aimTAcnStIh+/sd+lyfoz6DszXLPPAu4rk8P1zhUFZhKagGAAFgQWIo4G0YWKDw8UEzQWLVXekz/7L2yNGtcIEJkKaBoosXtDMQSaagzuWrhqLlf3xoLC0ysTBh430HplMACy2YX5hdRLhwhZb7z20+/lsagQCH8ez18zcLEmiuVMSWNmF99KxlP3bA0P/CINogaoPA0oCASI86VUdzN24K2P8jyKBeQD5Gcs6JKlS4izGxukl33jvqOIcO5tzP9nP95yqByGLiP6j1bQhHkATKTiBVdSYsPM9SNgTPRsFsvTqQ+1K9BrtcFW2KgjiWuf6FOn2icEpQ2PJvuhapzoxeu+27P6bhwRFwixQ0chYIACl8yh2x1Zf9H+v84g8pkIAtiJe7ymHyKV+zvdcJNOlwzzVB46tBYdaiCs4uhTlwBE+vVLMf2xGyv+RnpF4+lwybXPhs8t+OdxKIKWT9jqD1SAPN3YqvLdwz4i5erMKEULRgT+oajYXafxwwM0jM+I6g8aWoQtfNdQuvfJvDFAgPJeBtj/8UAmNpNBHM8//JFMIqxELUPhUu2eLPfh4Feu9UrjHzc8n34mCvuypoPhoAKy65QymnPgIt2arkPnhlwP0sflvdFXMFjeyCTsLbQs4/xWlup+QsC7M7LkBtvSG7v1DvHxXVJqoEZ+YAzsyMFQdZ3eo3Hw4z2uosAEKOpkJ4OAFrX3i5ED4g08Qs5n9htPmZdjX3YLMO77YX8lz4mTKVrEOT9jdIQkqWhIoaJVdFzW3r/e6fSW0zX9XGve6ZRN8adL4QIWb7wsCJE6q6zvBcbA8AkRj3VAJ3yfwGmeTiirWrSoUtziKspARBzYlOiPUPgutFJC/cpjA/DSCXsgPEibfo/AFnEdrblimIqripitpXS8GVKALntmnOb2uUBPkCx0xqDB8lkRbN/g+oHK15yp6AL+Cg92dK2VMvJOhHnlPXlIF5Al2u8lcp/KbFmkjJDZjteObA24xKpvHa56eZ7BB125DpVy9mGTqfzg5QqfDrSpFGJs2SRngkqvD17qI1iXwusVMBHp5XplPe/uegvEo2PygEGCQBXNGeRS+ALwCN6ZJQZJIHwkzBpQUMdslaj8tOhVCSZnD5VGb5bCUYLjn2GbyWOy+SKjV+U2vW6wV1LgVfvlgdt9AltBAMZO4PQeiIS3cv1kuSonJUFdIVMS9mMPXv8x4cJcPpGVNAYjFiIwUmPubSQ7IsYgmERpHtp1IueZ2WAAAJl+xB05Qh8wEApS40rMiOa9WCBkAAhGMCzUAazcCcL4aDoiVc5ZDJIbeY6SEzijOxCKTKpwLAix/k1e/cnwuEkhOsf8whBxb1XPjhnAtDo5wdwYEqyWokfht30KEvLHbSSF0y7CqvnOtnMzf1TyFSZkBtgzVu7ukEZAC0rUl7nZ3nPtBqltPOUYe8uphOadTlMNxYB7ZPn6K4CynlmfkmgMio5IDNnlsMAOTgjDjkFQRTHyrukpSoxXHShx3lZfRMbLLwa8gY8ciIw/bK682dbaPQG1vSEAzDeNznPABk69aWVQZEy415eAPEpkpuCJTddDGIll+oKlSoHkimMDHKRU7Mk2nK9aZBpr3iLiDxdCL5HCTqbk5YyfYRIMCVMaadynA4u9BJIz82KuihDFV7aT61f24fY4oL6zalJtZxohPirQChCED72iTYcwOnw6nZUm2uvzF68j2mQxaqEmVzebj/8DehIindQGXKl0UAmPMxAVKHsZBCdzX23K86ORALXEyzUTPtrOh6d0U1qUGaVJI9hThhjBuahu5rpIN19gKVisUprFPPXnlNQ+JdpmBzCwY5KNOa+gzEGx2Y0KBj8shIEKzZkAJNn1NQyAZqrFET125hPc34WAuKmMpgSIiloM3og4r0MLgXbhLJN8+dFweQzewrVLPxVnHiN/wkDe4CnssDJrrY15HTm1exkWttwowSEADgCuVbtMEbGuig5iwwECCr87Qqg9HtSu9dnNG5+e6yw8SaDWNe72chZgCAJAY1DS6iJOUhZladIlgCfIM+jUOYpIAvYKbJL1OtpuS2MzCpMj3jn2dlMLlkmhNK2lS0RLU6tqBlDgnMKE15SyVJNNtElGhNXsiVR2ZW6Tlg88N14ctRUEUOKrQcjJBgL8zFwknPLhg2oH1dFrX7FJ4zebbJSNym7aMwh+ACE0I9a4XecFXNqWBpcOcLaFmoEAehWknBmB6G0UA5XpNPjg0IrwGCO5+dwPilaMpSRnpp9HgNggsWEMORk7MKnyur+kd6Tf8pBfjiaxQSrwK7OO3EDpdpFrK3+Zsnjoo5SHMQ0lw45UQPynDs7OrfIrD2ylGIlgnP45sRAJ5eR1XettZELSC7JRZ9Oka4MpTVzg7S0OlKJZ03vvOMSurEhDLUrt2ROIwFoggId8q75HIwmd8kIZYhrE47fKxcM0FD8zTvCYwDVaWmYRCCnWM5ZZjSEnkBrlBO5UKHmUYgQrMLWguIURSNqrmn8TqK4NoshIiAL2jBldtTMM32PDod8/W6h2zclpht8x6aa5rJkdQhUf1CnZbMq7R5eGue/Yc0xEQWDtesRgKoFrYFkEmPs5DVQOa46l6r9sc+P3j3mJ8vQEBFYlqrZWCvqPuRZYBBStQTXiWu3pGN7BvUo521aEBdPl8AyLJ6CTirlh3pyfhOKmQWzSQJ/er1CSir5NOF+lnkw380nc1A/Rd3oPOkDsmEr1iiCHUEO0XK9gejJKqk3Q0WCRbSGuYSRqbQyHqhd2ULPL7xbiRImszdAccwz7sA+D96tOfrkDaHgJI5d3NmRKhDpt6TLIv2VBpj16V5VKVzDClyYBAhY3C8vOqLTw40/RPPOlT2rCiJGyhrbZtgdYRrX6qjyU25nL8G5twOhoDmopdeJl7+trHujxLDZITRIm6gtP2+gA233dsPmk9Mp6HpTP6v1zlk6zUjXgeiYgPNhDrSD8M/TLf8L81nZeezcKc4JgRCDjxy7UcgGaxAd8+ZxmiKBe0i9pQICu7p7tovmwH1qDqf+pKoZ30+M/t0ouHh1DBPMwol7VOEals/dMq395hS9XgIzcBczZN8X5Bl4CXS8M/HuvT9KuHFg0C2SdDvH4HyKj7TQt/Md/a4wBoDmtvH8PeiTb10hfuHRtXu7mDkO7przInZChzTIEk5B2vXfSelRngxAYkF5ixThpjKusphUvmonyS9e84FOTrPQKce/M5QUuvTGPdBqQ9UkX7Xiu3PVf2b43cHyBzdQYpCTIeUIx2p2PMBYceK5rjIYF6kLAebrxrzIoBFop0zj7oMylx7yzCOZPG4gJyoGUc/aJb/M2jpVH49qUipF8JQDxqQDdPHnk82P0THZ/h4nh6ZEj1buM3lbuDQWMUTI2H2HEXdK4qFzmXaGo6b48v1vmrH/4HkHH2pcoPR2VJGhtnAG/7YQwpPeWanuFlS8L8kvK7EHkqPiKwEd/HZjxP4bdcNQVmVKLbMP/tF6pocuGLzkKdOiqk1KrTeQa3jTCz2lxXQDQ4yTkdWkeH5BAYvt43nc9uImYKaUN/AflHzV6NDBitsAZ1k9yf6zYvZWy49AnOMi/0k/pmy0MAotbJTnsv1Wg2j4G0HqkkXnIpW/OXgoNKN9nVJy9MrjhM6OFb+VVFuPBe0BsH2qu6Q888l8s8lE1Nidh9YlfDYsZHo9xXuhmaJ+lGobUzB+i0ZlFvxZ5iLO4RaYAw6T4QhNRZErcBnDL7bTmjPcPVXt9T177hl7LV70+DzCCEt1P4aZ+Jh1bS/79/8JyeOqd3+KreW5FN+yAzactGr5yrlvq4+7fgb7fFP/TvtV/8blZU2fm0oPJc8g8SAg9GmbxzoKntcda0QLPHmcwlOO+HSl8Itf/y7dS//m5rubnCQ55LCE40/l4beVTIaPPR/k1s+y1OOrrBZfGtCHXj77YOgB2A2AMz+FaXvGI4K2HHDAKoSXky3S83ujFrixUzzL6oCGWhgfVCvDkFcH4U6PYG/D0IL64EBvez5n3ZWPKkIN6RwkZXndAamsIXMZGJ+awHTgdPn2uHnOqu+e8pf+dM2dhZdqUGo0xL4XKPeMzbjcyECur81tPJzIm3L6rUXZWOIyrh2uls9+aLT/MUVWj/UKXhqw/kxw5/1Sh/EfSPie9aaz/X10X6FieJ+v2UQT2M3t9mzCX9uGsC7KF5o/VYTzp4egkOvVnsuxUxjzV2WtEhCb7sSGtqvhVjDeiBKPvvEMdKQPPlL2HfoRDKbMawQhYDiuCk805amhqbjNKgdDHmKxabkEEHMjGG7wXguvnU7hFuvBsUXztt9x4ZU9wFIduy3s12WgZxEBbg4tSck43FNE9KuP1Gx6Q5oxOcKVjTJYo6e9jXHBqC/40VI7UW/1DIZ+Irs3ZCqv6YhBdffOlqM+M0fAIV6PrDzthE42xmAsUTI21kyJeiEVCYcZL/57+9fee2O1ejeTjWh9RvfAbGbxjbt/9Yz1zz51DP7ddMxBJ4QgvO74iaMjkwGobLT3CITDw2XG/e86/Zdv3PvNTdUyvXwC47atW+HFTe7Da+v3v+hv3/k249Sx9IJXXoQWLZtr96wrvmPf+vd92+YoTto0/b7yGd3dP3e7z30rU9me7s5UxQ6reqnzIYb7+hH3zo/aecWNp/rqg3KIIADt+vufpSsPZ1XYJqWtenK9as/fvX6P5lO+ONHPBJt/dz9t/1NMBJlpmW7tuPy84G2yV4A4eAQmRG0iEicbDgZKi/X/+Y9u/4KhV8xM1dg6qdv2vLn669Y3W7KnrFLfeCkynHI/e5dN//O9rqaq4u99fa2xnfdf/vb787abnZaRSIFfs1Nfei2O3MV/tw1wPnAAsCKlTbeqBeef7IRdP94xfc8ALjIrYqXe00mhs70w8DRThg5NQginQOBfo8/HoGq9jqIr2uCikggEgz5aSKT4nRi2VZyvox8YZDc/GrgwhUA51wEQ0Ee9VG1pzcBLx44A4dO90PPUNq7U1VZANY3V8HVVzRC24paqCiLItH2olLakoNAoXZDRbDcybnQcXQUuk6nYLDf8KqiMh+FeNwHza0RWNEeg83x6GaXki9PuYZpUFi1fgC2XZ+Zj/DnD4BxPrDt+iwMDw7Cwb3VwufHkRImus9WMODz+08Nrnv2v30dTu/tACdtTJq4Hq9F7RWpi0HlNeuCdYKtTVDyKuqSYcOntGo5WcL43K7ogvxlAzvBF2YCvJx5kymM+Tm58ZN/+734T149Dr2jmfze6/GWB4WsofKQDrs2tkKGZN+u+/UjhIikbPiJ79SghNvDuJBVE11vN1ZNsKz50I8H4r863QOdvWmvfC6ZtBaCD6BSWNkYhlzcbigLhSotOzOKlIYxwnzEtlWorhtDzTy8EJeZ1L8wuIAoBuLGzAE89rVq2ttdFddD7Vfa6jvWmcqtZWmrzbFdYLrqlXSbdgDw7wJPEtLNbpXv7tpa39a5prqhszECiTIfOIMJBJqTT6BLm6fVf9vzTsuwLbzeHEyWkDPXoFS+W2sRXL/RdbQ7TAO2pHOWqqsKKDM0Y5a5CQbe168x4fezw4pqPU2Z+RMurGOoRWQpA58shLMwbY9SR6FLNz+oBKprtZqrGpWGO2qhcic3SExu0lOVmSPztsO93Cnhd/p63f6fdTpnfzyQ6zvgaOoouecD3RBv5POd/QsHgHwoBQVspskHvv2vj27qGPmgBIVsCzOfzDDp33vtSpx8ubjhygD8YmcjvNwWhKRGwPSrEBjOnvL9v1fe6aJRhpkBgBpCyBU7F7lHHTp916HQ77QtdpXjiJA3w8m8zbOnvZhCHFXlrzLF+D6h1nNIR84UditLMCjFw91eRz0LbZjpU/RItVb9tial/rY4rbk5KAJ18iqu1yhn7rOWyl6fMmqIH05ro796dlfzTWPxplG6AOEvCgCe16Fq0DY4sPZDX/vuj4NDySZbW1zgTHHyJmAsqsNwVIXX1pXDK63hE+kf7rtBN2yVEjJxv760EzYKNkcIK0NzvZW7+u2Ore6ybVI5nty82O0c4xVvpFpGGpNSNeclxqwfIvpfQrj34m3wSxMfXFAFxxZ2RmGKXq6UrW1Q63Y1KvXviIroWiKjffgfX0SRUyTGsmCnve/G+vcebqt6jFoLz1ldFAA890rToLW/b/2DX3vsJ6HhZHyxIPAsDKpihkBQ8OyKBzIHqfno85D+Qo67WXZu1knPQCknIvgx21JvcRzSml+3FLBUFQTHawrI6yuKGFI19zlCM48IYR3C1/0FniMJhb0y0Hp/C2u+twzKtqhCYV6/hRLklUrhK8gi9twQf//hVVXf1MzFXXPRAPBAoGvQ0tu/5cGvf+9HkeFkjaWVLoQuQSBLy++Oiie+oqX+k8KFtwSKNt51nNiXjQy7TtqSi1020gODrLOgw6DuH72bu3af5Ae2cNIbQms+sk3Z+jnHSyV2S9bkrCB8eGVn3UeOrKl8ZLHCn18coMjhMy04Fa/Z9y+/+e53pCojZzSrdOX4HUYgTQVsTtO71gj97RaIHAo8C8J3m5lTrpP9Li9FzVCv7gWV/RehituBj+G/DaQGTlgN1a9laz5huTZyIqd0wnc9tW/tuaHug6USfskAMA6Ck7U1+7/4gXfvGo2XHdDN0sVRvDUB1ATrbWUXp8SRZWk519/BxXIoFi1XW9UbKVXKcLZna9WqHX7wx3gJC5lLXsQUPrbnxvi7D6+s/GqphF9SAJwDQVXN8S984N5dnasaXvTL1K4SyUgq0ipQmlRCZPORIHfoKrJMqoVzTuqEIHEZKotq4cZSPpZiI3cI0t4XdjXehoTvR5pV2p5XJQ92+ywLeqLlA//4vntv23fNxn/xSe+NL35EZODZb/J6xsHP0cW3XOFbDnWDpSlA50V1BPFWvFRHaynVtTUUfrpW/+Xzd6y4/mRj2W7VKn3DsyVZ7dBR6BlFy37p7tsffPKWq/6zrNKlOot7+HyRyXzajEIJVRkjy6VbgIx36YxRL57IlUUzYC8+gsLuavY9+tRtrbf2RIMndGtput0t2XKXggxYsRz4/s07P/+V9911x2gk0KUvwiTIj2lS8DLEB0Sj+fDssji8gBEhAbmy7OFgEdyEObKOiGMe2FbxiWd3tX04pyjpxU6eSwKAPJLRhUNesO+KNU/+w4ffu+Pw5lXf9DkWEjq+IADohGq6LF4OyAEAQssFAPLZkKeVIQiEnymqWOBFpH3PVSovP3drw859b2v4XxTtHlviNskXJevBhzN/pLK894vvvft937nv1o8YYV+/z7DmneyJDxtSZPNSR/gdhweXS8sIue/SsnhUdjBRKQvM1/VjtgANuHN8U9nfPPnOthu6V1S+IsFALoKXo1ysQVJtx6sB9LO3bXnk2Iqmp2977hef2fza6x+RSze2OrfHYA4PoEsUkI3/OIdl0zsiv3sJqCJ78QENz929k6bSgZE67ZlD2+o+dbIu9jKTjNK6eN1tL2rPHc8koDboj5V1ffneOx/84gfuuaW7sepl6Tkos9g56QWg8CnDc9pKYpcaBFTafqIQPjsAZERPznArKDr3Xl/90afuWHnLiXjsZSn4UnhMy1IDTNIGjuNlux5YufJnx5sbn9928OgHb/j5K/+lui+xxmYquMqMvSxxoAsFdCenjSwDd5BI9k6pQ7WZwOmFcnGGW0EYOnpl7OGja6sfGgv7h1R09zT70vS0Vi7ZgKHwdJz5DlGc57ZteeTAmrZvXbXvwANX7Tn0B5VDY+0Ocn1HUSbPdC/8SpFxU7moBmIZdY1jlJKZ2qrkBe+CHYDEiTWhR46sr/n74bJQl4KMX7MubTNz5VIPHBXcMwtpPZh9cuf1/+elLZse3XTs+Huu3fvab8W7h3YQHCTJEYRXp40QP9pZtLXU8YobLx8EoIsqg0GFWof5TVsyfC3rHZhhOHlsZeyRN1ZVfmU0GugaNwHL4VCWzQziLjDThazuSz//ti1f2rth7aMrT3fu2rHvwIfbTvW8I5jKlVGmyK0m0t92l1v3MOp5qQQUTgKqiwxece2RWu0Xp1ZWfrmrKfqdZCiQkjF9GdpdToeyzMbR83uZaYJNmNi/atVPDq5s/0ltYiS+8sixm6860fmgSJ8hYFoV3PZ7bWnkoF9KMMhC1rLVHTetWkVxqVXDXjsU11/obq14YqgqethCMyaDO+oymfFTzFMp8gGW+nAYQz7AwI8uE+k7q9Ceng30eM/nnL7BrWYqG5cOhOxD4G2mKJjipQCFKCRdCjnDJVBlCDioJ9SKsoNkdf0/2LU1j9H6Jke2xZFqXga8iFje7W1J9fPLHwATI25eQioCgnEb1GQqRgb61ou+wRtF39B2d3B4lciazY7l6OMl4sV4syrIh2wnBo/IxOtOuEm+WUOhlKqMxMlGWHLvo8o49avdrLzsBNRUvkrjVc9ATc2vnLLyPkdR8wBx7GUv9EkAuH7PCLxZDync5Kises7R9uKsTCU1PZ1ppsOj65yRxConlVkrhoZjqu2uHuNCy4JTg3INcFnYRHbHLuxtkRBhlEieKb1Tywe0P0oVA2nmGbuyos8XCnRCZfkRVhY9ZETCp3k4nHJVzYNIpMwHChHLyyWZx/H/BRgA4E/tO3/9oP8AAAAASUVORK5CYII=",

        active_community : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAuQSURBVHja7d17VBTXHQdwPaY9SWuaNG1P2ticpua0p6f1FDQPNGJiYhpFAUHi6YlaWU2MJjFGhQV8svjgobKLiqhAxV1UzLLIQ9QAK+D7iShh8RWBVqNtT1KtSIKwM7f3zoIizAV27+zuzO7vj+9RmL3jnPl95s6d8e5MP4RQP69LxaaBXJl2M1+mvUVC/k5+5437wvuKj1B/XPQSHNQlJWQZAPD0mFNeESm+ELIMAHhSNk8fhDLDo1B6uBZlqkKE35UmT6MCwMuEz5DPCm1wW7IOAKDAZKiGo4zwOzioU3bhIqt6AKASPvNomzvCugCA0gCE13UppC0FyzZQAeBlom3IugCAgpL5/jOUQiKUG11KBYCXUduRdbZfQaCDumGectXgoed+1QvUQu6OqKMCwMuo7cg6y3Sx+HOt7Z9vJT8DAKUByFlwmwoAL6O2K4qLEG1TljwLACgKwHxEBzAf0drx++OPibYzJx8CAF4BILFZHIC2AQB4A4ADibR2jQAAAAAAAAAAAAAAAAAAAABIngrN6Md2TPX7CQDwMgBbP3zpR9tn+KfhNOPw21X+p7bNGDUMAHgJAFzwTFx49GhGfZPxgd+zAMDDAbQf/dbuAHBm+n8CADrHaByAzLrBwp8KBLAq0GeQZqLvWk3Q0CJNsM8KzeQ/PaOfPurPosUXMlJL2uE2U/DnczTBvnr89/HeCaBU+xkuQlN7MZrIz0oCsDLQ93e4gLdxUKd8vWXqq2NoALJm+Kfgz6R2aYPjs9S7AJh1U0ULWbz6E6UA0Ez0MXUvpC9KCns5hwYgM/y1HWJtcNpWBw951msA4J1fLlqUkuQjcgSgnz5iUJZqVMj2cP+X8K4XZiCTo12smAmhw76kAdjyt9cOUgAgTZDPO94DwKxtEC9K8tdyA6BX+Ufj4rV1KuRhfEn3C1y0O2KFjA8ddo8GIG3qiPN0AL4h3tQDNIr3AOv+IycA22a8/halmJ/3AADRAKROGf4PANAjgLW35QQAX8+nUop5zxEAG6f4NQGAngB8sbZJTgCyZowsoBUTF63JXgAb3vNDAKBHAGtaFATgewAgNYADSRza9j4Sze4FGEiSaMgyWjt+3ypauwYWAHH40g0ASN8DID5X7aJE3WACMHEoAgDSDwJdCEDdCAAAAAB4GOOA+3WGIehSznMAwMsAWC36AKvFcBMH2ZK9H13e9XMA4A0AanYOxkVvflj8B8kHAF4AgKs1LBApPgmPrhmfAgAeA6Bff5Qb9TFvjDrEG9UnUK56Mdr64Q/aag06CgDUWpvtg3arX+RMkQa8w2r43MhcZFo4FAA4D8C1xMlPNSS8O/qfa0JelBQAl6ve3G0nGNX5bRZ9Cg2A9WhKIC56U5d2rSgv4hUAID2AhoSwOQ2JYd/hIJLGhLC9DbqQp9kB5Cx8nrYjrNUZu6kA9sflU9odAADSAsj/+M0YXHS+o/gPkhC2nR1AbkQAFcDptEoaAK449iKl3b+F9V43PmGtzQ7j6gzzWy36VwGA4wCqlgYWdyu+LXeF0zcTAFNUCBXAiY01VAB7l7VQ2t0hVw/49HGlS5s0JgDC7eAk2/37oljE71mMeFOUVwC4EBtcRQGAyLjAeQCOrb/RAwDazrtjrcsuEW1Xpw9lAtB9kgjiC5Z6fg+wLPAKDYDYOEAyANwRXbNDACyGFtF2tfoMSQF0ZN9q3BtEeyyAU0sm3HQPgMNaZDcAU9Q96sDRYihwCoCOMQI5LXgggGMxAXeVBKDFLQAEBGvwv+95AA5Hj+OUAyAv2soIoN5hACTFK6UAUN/bdn4++429xtlvILFsmjIcpU7x65Yt04YjWhvbxFA/0RyNCUDKAbAnBjECOMMEgGTPElYAZ3rbzorIsUU4yBU5HxvkTQB0hcwAyFw/FgBGdVEfABS4CoBlxUTvAdD+0gfEHAYAnEm9FQC4CQAq1c6RBADDFQEyRc4CAO4CcFA7SBIAhbGOA8hZ+DwAcBcAMg4w684yAyC3jB0DUNuXbQQATgSAzMnzmAHsj3fs6DeqP/JcABUVj7VZdozBhVChmqw/yBaAUfND5vsBjgAwqRvIRBjPBHBZP4izZFd3ntbVZjHEyxKArRf4q6sB4MHftL5un+IAWC36QtFCHtWtlCUA4VVwyfmuAsDlqnfZc+pUGADjALzzvxMtyrn0L2QKQHiVCy7mBRcAuIRPOwPlCqCOGUB11tPUQlZnVMsWgHBZuO43fKn2uhMBXEN5MYPt3S5nATikHodOLhqPzi0PQpa4EHQ1fhK9+FIAsFZtvSprAEJPsOaXfOm6eskBGCNuoLz5v3Jkm5wB4OTiCeir3goueQ9wZvMt2QMgyYuZK1zblyVLA2DnXPLWsAWObo+UAMhR/6Um2L7CSwbgVFqTIgCYoubY1r8Y8QcSHAdAnheQ9YHtIRHpqjnuBnA0epz9R72kAE6mcooC0JH8JYgvXmWbF9gbAGOE7YgnD4bo/JQQNwOoxLm0MsTx4kvTA2yy3TcXS4UWcafTRcPvXyXeZm8ctQ13OiNPMgAPH+Zg6xUKMcgiDUaxwjYppHC5bfmuz6iPiGEBgIuXxwqgalkQW/FxLiUFP8kG4Gw6kuQ/X/qWAukB9JKd85wDQD1uK1vXH4DqE8JYAXzfty+G9ASgKhMAOHQKGKdhAeDwoO/RXAMA7uoBIsfOYgFweXUoM4D6pLCjAMBNAMwRAX90ePCHL/vq2Y9+Ei0AcBMA4TQQMbbBEQDHF42XoviIfFUcALgTQOTYjY6N/gOlAPBf8uIpAOBGAOaF43+PC2q1F0D18iCndf8AwIUA2geDejcAuH0jPvRnAEAGAMwL3h6METS7FsCkqJ62CQC4EABJuXrsdJcBSJhUebaXaWv2ATgHAKRIReQ7WS4AUH953Xu9PqPRvvkAtu/tI656G+LOpCPuWBriy9cDADtTOHPkk8WfjnEmgFtfrQ4d0pdtsR9ANxC4ZzieBgDsSJZq9OPkG75Fc99yBoCqq0mTft3XbWEH8GC62N8RX5ECAOwAQGKcPRqZF77DDKA+cRK5Y5h5XTv5CXu2RTIAD04PlRsAgB0ASAwzbb1BeYRjAI7HjEdp04bXOLIt0gIgPcH5bYg36wCAHQA6op85Cpk+ehOVzP8LKu8FwPnYYIxmDHmBVMfTQU5KA8BiHMgCwDZzaIsUAEzeBuARDDg7Z72OCnHPUIZPEST75r2Nj/QRaM27L4s9HkYiAMIXQwzNbAjwqeAQ26mAK9OleTMA6kumKM8UkhrAVeZe4GwGEwBk1i0DAO4DUMkKwHohiw1AabIKALgJQJvFsIoZAA7LYBCVrP0tAHATgFaLwU8KANzhVEcB1DHtZADABgCnPy7gLWYAJzY7NgA06xIBgHsB9OMshuVuuhxsIc/+AQBuBoAuGH7M2gs4AoAzJ6cy72QAIAEAWy8wx8UAvkWlSc8BAJkAaB8LFLgIAI/KdBMk2ckAQDIAwq1h3BPUOBsAV6pNkGoHAwApAZBczHpB5LUu0gHYuwKhLM3jAECuAEhqdv4UF7VMcgDkW7rkqVsAQOYAhBgHCHcJa/UcM4CSpEee0QsAFAHAFr58/TkyJ5BMB7MbAHldS1Fct7d4AQAlASjTnhSKeTBFmBhKZguT2UC9AiCFpz14EQAoEMAj0dlAVG5A3JFUnE2Ir+gyYzh/KQDwXAB9CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIC+AeABAAAAAAAAAHgrAA4AAAAA4LUAggGAtwOwAgAAoFQAS3gA4N0A7gMAVgC+bUoG0CxzADwAcC6A/8kbwKctAMCZAPYs/lbmAO4CAOcC+JfMAXyjAACtigXA7Vl0Q+YAbgIAZ/YAeYsaZQ1gx9xGAOBcAFdlDSB77hUFALivYAAxF2UOoBYAOBdAjcwBVAMAZwIwRZ+TOYBTCgDQIiWA/wPnX3R8b+9OXAAAAABJRU5ErkJggg==",

        teachers_trained : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAYAAACAvzbMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjM2MzU4Nzk2QkVCMTFFODlBRkJFRjMxQTQ3MjlCOEUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MjM2MzU4N0E2QkVCMTFFODlBRkJFRjMxQTQ3MjlCOEUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyMzYzNTg3NzZCRUIxMUU4OUFGQkVGMzFBNDcyOUI4RSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyMzYzNTg3ODZCRUIxMUU4OUFGQkVGMzFBNDcyOUI4RSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Ps87rkYAASU1SURBVHja7L0HlFzXdSV67nuVOiI3ckYjUwSTmHMGAwgCBAESBKOoPLL/96xla3l57D/f/3/PLFv2jMa0JOYkgiABEAwgxShGMScRGSByjo3urvju/fecG181pHGQGO8lC11dXbnqnX3P2Wfvw4QQEFZYYYUVVlj/1hWFtyCssMIKK6wAIGGFFVZYYQUACSussMIKKwBIWGGFFVZYAUDCCiussMIKKwBIWGGFFVZYAUDCCiussMIKABJWWJ/zSmq1zAN33fXdU48+etMPbrzx4c2ffjo2vCthhdVzZcJbEFZY6bVj+/Zhn3z00THbNm8ekc/lSuvXrp04ZNiwzZlsthrenbDCcosFJXpYYam1f9++/k8sXnz1Q/fdd/O6deu+wRiLkySBXr167bpg+vSl195ww8+nHn30e+GdCiusACBhhUWrVCw2vPCrX01/8O67vyuB4/Q+/frl+gwYAPmGBqiWy3DgwAHYvW0b5PP5zbPmzLlz9rx59w4fOfLT8M6FFQAkAEhYX9NVq1az77399kkP3HXXt999++2Zg0eMaBw1diz06d8fGgsFiOMYEnl8VBBE9u6FzRs3wppVq6BtwID3rl2w4F8unjFjcZ++ffeFdzKsACBhhfU1Wis//vjoB+6++9bXfv3rK1v79h3UPmkSDBoyBAoy64A4AsaYPDgABJMHifxPJAmUSiXYu3s3rJUgsnHdusrYceNeve7mm//lnAsueKqxsbErvKthBQAJK6yv8JJZxJilDz987RNLl94gGBszftJkGDpyBLT26gWRBI1EXkdwLoFDIgceGwwvAIgiBSp4WXdXF2zdvBnWfPIJ7Nq5s/ukU055ct78+Xecce65z4R3OKwAIGGF9RVbMntoePSXv1zw0L33fmffwYPTxk+ZAqNGj4bevftAnIkRI6BWq0FNZhq1ag244AQcCCAsYhDHWcjIzCSTycgEJQIur9fR0QFbNm6E1StWQKmra/c555235Jbvff8fx45vXxXe8bACgIQV1pd8VSuV3FtvvHH6//rJT368bv26M0aPHZcZ094Offv3h1w+BwkXgJ1W8npQrlShXC5BtVqly8yxgVxILpdTp6z6mc1mJLBEdL2D+/bBp+vXw9qVKyCbyW6S2cjPrp4//87+bW27wicQVgCQsML6Eq4P33vvhAfvuuvWl195ZVb/toF92idMgLZBAyFfKFByUa1VoSJBA7mNcrlMPyuVMmUinHOZhagKFpaustmsBJw85CXoFPIFKBTyEkjyBCaRPIaq8nZ79+6D9WvXwobVq2HkyJFvXHfTTbedd/HFj7f26nUwfBphBQAJK6wvwdr06adj773jju8+u3z53FyhMLR98iQYNnwENDY2IhpAFQnxYglKMujjz2KxW4JHUZ4QPBKZVSgAEfK/iCnuI45iyMisI0cA0gANDQU6FQoN8vc8ZHNZItsrEoR2bN8Oa1auhM2fflqdNm3aCzd++9s/PffCC58In0xYAUDCCusLuvbu3j3w4fvvv3HJI4/cWKlWx4+VGcfw0aOhV2srsDimzKIkM42yBIruYlH+LEJXd7fOQLB0VZPXqRKIIIAYEgQJ9DiTIf4DS1eYeTTIDKQgAaSpsUmCCIJJg8xOCpCV1xGCQ2dnF2zbvAnWrlwF+/ftPXz6aac/cfP3vveP04477q3wSYUVACSssL4gq+PQod7PPvXU5XffccePdu3adey48RNh1Ngx0Kd/X5k15IjwphKVPHVJ4ChJ0Oju1llHuQKVSoUEg1WZeSCnQSWshKsmLIYcSCRBJCYAoSxE8yB4amhsgEYJHpjdNGA2gkAis5E4ztB9HT54EDMiWC0zkkp3977LZ826d96CBXeMnzjxk/DJhRUAJKywPqclv7vsueXLL/vlvfd++8MPP5w+Zvx4QIK8X//+MtBniQRHQhyBo1RU2QZmHkUNHpVylXgQzDyqVVW+qiUGPIQl0TEDoSxEgkJWZjIxZSGGTM/aDKRBAwme8nnFj5AYUQLU/v37YSMS7atWo0hxw+y5V985VwLJgLa2neGTDCsASFhhfYbr4w8+OO6O22770zffeOPytiFDWtonToQBAwcS0Y3cRbWigaNUkqAhgaPLZB1YrqpIsKjQdZBER9DA0lUigYPIc87BPy5IVMiwhTemFl5XzsraE4KFAZKmpibKRhqaZEaSK8i/ZQmAEMz27d4D69auhfWrV8GI4cPfXnDzzT+9+LLLFjc2NXWGTzWsACBhhfVHXEiQP/LLX17/xNKlN2YKhWETJk2CocOHy2DdpDqrqlVVrpKZRrc5dXfJrKNIpSoqV1HGUaHsA8tbCZ14qnXXz0CwXZcJBlHMbDaCJyxnZTMOQBBU8tSplYfGhkYJJI3QgKUtzE6IaM8RGJU9on3npk2lo4899vmbvvOdfzzljDNekPfLw6ccVgCQsML6Ay4kyB979NG5SxctuvHgoUNHt0+eDCNGj4bW1la1u5dZRLmsWnKxPNXV1UWlqiJ2W2lthwKPqibLFd+BgkGhsw5EIBIQYg7jHRbIgyCAQKQykShi+pQhfgTLVJSJIJjkFJggiChOpEAZieJICqrsJYEG24OxpEb+WitWQMfBgwfOPe+8R6698UZy/A1AElYAkLDC+jespFZsTJJDveK45XCcUSUdJMhffPbZix9+8MFb1q5Ze86Y8e0wetw46Nt/AGUAiiAvQbFUJqAoyUyjS/McZdJ1yKyDQEMBCJHkCBxUrkoUeAjVbSUIPBgBRnphS6/yxiJlOiCI4PmYVOqZKCYgQd7FlLbyurU3j6Ut4keaFNmOrb9EtKuMBYHr0IEDsGHdOli3ahVevv6i6dMXzbn22rvHtLevDt+KsAKAhBXW71nVSkfvXduWzC13vn5aIbNzSBKNXp9v/ZP/b9XKTSMfvPuO73/80UcXDho2vGls+zjo39ZGwRc9qxAYyggWJSxTFYnrKHXJ37GjioBDlamQ60BtB2YehuMwOg/BhTJLJOdElW4w42FiFpazKAvR56mkpa8SEZyQTgRLXJk4QxxJLodAktNtv5iRaO0IdW012uwEhYl426rMiPbv2Q0b1q6D9atXw4ABAz68Ytas+y+fNWvh4KFDt4RvSVgBQMIKq251HNrb1rHr7/8hHy+ZM2BAnIVCDOXOMtz2L1Nfvu/+LaPbBrcMbx8/HgYNHQqNTU10G8wkkCAvInhQd5XiODADwWykVqmpjENrOlRbbg0SBI1EQCISBRq6TVcfCrpcxeVBEWlAYRZHhPlXmN8ZZSZ4HZWNaG6EIVcSQ1YCSS6ThSinbFDy2TxkZEZSkIDR0NBou7WQH0FlO4JNLMGoVC7C7h07Yd2aNdi1xceOGfPGNQsW/OLiyy9f3NTcfDh8Y8IKABJWWBh+OY/ee+feHx8z6Zn/ykQGfruicuD5FzY0lstJftvWVjjYORzaJ42F5uZmIp+x9FRGzyoJHt1ay0EEOYoC5WWVqu6qqlaVMSLyHDyxbblUshI2qaCUIiKHXZZKOFQOojKTdCIi6HlgyUvhC94v03YnClQIQAzJji2/tmNLZiM5RboT0V5AEaIDEuzgKmBZS4IMPrmuzm7YuX0rtf3u3rGja9rRR79w/a23/vPJp5/+krx9KXx7wgoAEtbXO/voONR7z/Y7fj12YuEbv3llbeVPfvh4ZceuasPosaPjyVMnwnDsrpI79JonBPTbchE48PcqkeNYsqpZNTknk0RXsqJ8Qai8QaUdjuuIqIQFkOY/VM2KuaSDQIUu9zq0DLBoRCJOhOn7o5Zf2/aLQkSfI1HlKwQOBBBq/c3niR9BpTs5BMvn3XX4MGzbvBlWr1oFHXv3Hjzj7LOX3SCBZNrxx78ZvkFhfd4rE96CsD6vtX/fvraW1qHDAAbC+vWf5g525nJTj54Aw4YPIZt1jObd1FVVsuWqIrXlKj2Hadmt2oyjSmBB3VUEHC7QEz1Oeg5TqtLnNSZEPZlzDSI6CRFgb4vPi9p8dSrD9O9cA4nQpS50+uWRIukTel7cZUYJZks5qGglPL4eKmnJ14MlLjRrzEugaW5tBew4GzB4MHpr9X7z7bcXvPbqqxdePnPm/fNvuunno8eOXRO+SWF9Xiv+67/+6/AuhPWZrxeee+68n/zd3/7TBZecNq6peRArlWuwbz+HIUOGQq8+vWjHjtkEDm86fLhTng7TqbPzsM48yqpsZVtzURzIraZDjfNQAMJMZlAPHqBmfZjLTHKi/m5/cZd59Sz1uypbqTlT+FODCZjKlwYvrsDFtAsn1P0l9OUGVBINhFVICGBqujtMHqQyc0H+Z8CAATQ1McrEza+9/MopTy5ZfEmpWGqYNGXKx7l8vhy+VWGFElZYX+m1euXKqXf97Gc/enzpsutHtY/K/s/bvgtjxg0DnpTgoQffhY9XHIRsNtJ6jpKXcZTJap3I8apSkdNOnqzXBbXg1qwQUOj/WSq7YF6ZSpWZsIPK/oHCfWQBwJDoKA5R9yPA8CCqYiXDv7of1JD43IgGLkO8M/NfpDgSpktbEghSQsRsJge5fEYp2guN1KXliHZtjSJvh2aQOFp33bo1sH7lGhgydNDHP/qz//w3Z51//nJ53e7wLQsrAEhYX6n16fr17UsWLrxu8aJF32poaRk0ZOR4GDWqF3z326Nh0NBW6NjfBT+7fRNs2pLIQFkjjoO8q7qL5FOFIkEa9FRTwFHVQkAsDTEjABSGp1DkdhRHqvSkW28jrySlOnYjDSYKK1wb7+9amlrXZSqEH/u4mH1gRgEac/Q1MBVS3V5CZynGWwuBJAMZ/BlnIJ/NkI6EBInYtUUakrwi2ZEfwdbfBtUOnEMhImPECe3evgNWr1oBm9avh9PPOHPx/BtvvO2kU0/9tbyvavjWhRUAJKwv9dq5Y8fQJ5csmbNk0aKbDx0+PGX8pEkwauw4ufvOQN9+e2DuzCI0N2ZkRsFh+fPd8KvnI+js5vJ3RZBjZ1VNZx2UcVTVvI7EajkUQHBdNhLCWLAz20VlfzKLEgokokiT6g5UwBMMpqEDXG+WIj8IJAxoMa54FqdiF/a8SopUlmLummkiH1uAI23SiO2/yvFXggc6/mJmglxIvqC7tVA70qj0JChUlNkLZjXdXZ2weeNmGq3b2XFw/+mnn/HE9bd+66dTj/rG+3EmUwvfwrACgIT1pVqHDh7s88wTT1yx+KGHbtq8bdtJY9rbM6ggb2kdKIPgIThm8mswZPBe6NMrkUFUkF16Q57Dk882w10P9oFiCdtvy9Z2RBke1kAkipTGspWnziDgiHRWYepVETMchRYHmlKWLieZTiuZnsjrCtuJZQQizNy/ALAQgzfhCkiM+NB0dymcEA7ImClnKb0IZitMuHIY854LDq7CjAlPaHVibFEyWNbK6dZfCSRNzaqkhXNI8mjYmFeKdny8gwcOwsYNOFp3tXxS1e3TL73sl1dde+3dEyZN+m34RoYVACSsL/zCGeQvv/ji+YseeODm999/75JhI0flxra3w4CBg2iaX7nKoFjcB6cd9yicdsJK6C6BBAZ1WxkXJXAA/Pn/fRJ8+HELNBa6qTW3ZngO1HRo8GDM2I/ojEIZVqkJghpAwMCFbZ7yAMSQ4O5qroSFgBCxFHBo2QgYZkNlE5zAR5XMNJSZtl/d6osPybm5nVK+u4zGFsUssY8nbCBAQEUhYkxlLWePQnoR1JCgUSMKEvH3BuRHCgQktUoV9u3bCxvWroH1q9dAv379Pr585swHZs2bd9/AQYO2h29oWH/IFbqwwvqDrY/ee+/4f/7JT/78nttv/3G5Wj3uqGOPiydMngx9+vUj4pisR8qdsGtPCd79IAeD+q2AwW0KQJieP37oEMDy5/rC9h3oL4XlK6PlSJz9iNfn5AIvkKUI/q+6piJ6zIg5wIgM4RGZriywIkBSlBtORP7UMkLbeRVRpgCptt5IZztWtC6Yj1sasCKLTwbYzFXc5o1pW5U0+CS6OcC3X0m0e7A1hNTtweae0B8MhZdtbYMkaLdBx+HDA599+unz3n3jjWPl+8GHjxixKR86tsIKGUhYX5S1eePGMUsWLrzmscWLb4IoHj1u0gQYPnwENOEoWRkwKzLQVcwo2e4u2LX7AGzdvht+cONHcNmF26BL9w01FABef6c3/B9/OVwG0SxkYhUcgXbx3IvMym7ElKEY8zQaenePRDW11YrIAo2qHglV5hJgS1YRc2LB+iZcn/ewfliaEFeVLlWeshmGIdmFug6RI8KV2axdPNW+bKOvuq7u6gKawR5Ze5QM8SORnYZoZpAoIaLiRhobm+indfwl63iZ0XV2wfZt22DtypWwe+fOrm+eeOJT82+88Rennnnm88HxN6z/6ApCwrD+3etwR0evJQ8/PG/pokXX79y168SxEyYwtFjv27cvqa/RYl35VlXIJdfYj3Qe7pCAchgSGAnZwjjIJmrnn2sQsGV7N+w/cAj69sLMRIdl4fMFGHRjp9UwPIbZ62Pg1aUjuiRyXIbJJYwlSWSJC6Hbd5lHpjui297ekPDCtAfj84vcZdSXJajrFzwbFOO7FUXa8VcoqIBII4jQ92G4Ea6Eh0L+PZJ/w+wrku8R6kcS7e3l80LVSknrYhqhhHNItONvFn23GhthzLhx0H/AANiycWPTJytWXPVnP/jB6edffPGieddff+eUo476IHyTwwoAEtZntiQINL307LMX3nv77d9fv2HDqcNHj8qfctZZ0L9tAOTyBSKLUUGOdupFPHWXlOFhEYc6yWBXQSuSMhTLo4A1SwDhNeUxlYth/cZ3gCd7ZKDNOt6aCUeOgys7Oa8qTxio53b416X78OI8gY/OGlgq2Hu+JTpPUKS3vi7dXjtlmcxdiwdtx5UpemmCXeYSElAQZrgCDYQYhsCg8w1T26LWYAmM2PZLPL4i3MnHSyvdscaFvyO4WABB+5YKtjgn1KlGExdLMiORoF1oVPoRVLS39u4NE6ZOhbbBg+DTdesHLX/yqR/++oUXLp45e/YD19xww+2DhgzZGr7ZYQUACeuPul5/+eWz7/7FL77/7ltvXzJk5IjCyWeeCW0DB9KO1/IcMojhTI6SFgGSBQk65RYrZOFR1gLBvfubZGCcJjOPKu3Ou2sRbNn2jnyUxHY4RUo4QZGfRZ7VujUvBCviYPZyn5g2WYcCkch4WRluBFwW4Uh1VYYyaY3QXVnCemmpXIMyHS0YdCU07ph55lp8CXg8roS6xBBUNPmuymrCciRcg5DgBpgipSMhBbsEEfmekDVKJiH1OoooybpeAke5pDKSxlIj2ccbISKWvbCZobW1FwwdPgI2rF877u477/wvjy9dOuemW2/9n5ddeeXDvfv23Re+5WEFAAnrD7pWr1hx1F0///kPX3z+uav69uvf+5Szz4aBcjeLwEETAeWO2ABDUZequrTFekVbjqBTLgoBq1SCKcPuPbiLHgj5vAqctVIV9u4tKW2E5SwiG4xVduFIcZdmCM+nSl7Fy0IMcBDBbXyqbDnLNupal16mCRHGXHbjHoe5Mbd4G7/EpetsBuKMS68BId+U0Qe5yFbFtOLdgzSu7VHoGSKJbhTzQti57TxB8MiQFQq+v9lsGcqVnDKelO99o8xGijjICnUkaNSIQCJPQ4YNhX79+8HIUaNh/Zo1k/7ub//2n59YuvTquQsW3D5j9uwHAz8SVgCQsP7D68D+/f3ukcCxaOHCbxeamgZNO/EkGDZ0KDS1NNOuGcHAuOKq2RwKPIpd3TKQlfVAp4p1ya3RTxQKytuVKhDFRp8RQ8IPQmdXiYR1QO24wovdzHZL+eBhvKfAAxBgaX8r3dpUlynYP7jrCv+61rukDkNcEE/Vu7S/ifB+TT2EbjPWnLrNfdxtVanOZC9cYyDXz8EYNuJzwcxD8SkCEnT9xRknEkCoa62WJeFlVZ/wc8l3F6mUWNatv8oqpUD8yMjRo2UG2Qajxo6F1atWnvlf/vzPT168cOF1/+eP/+Kvph0XHH/DCgAS1r9jIUG+/PFlVz5w510/3Hvw4DHjp06FkSNHQkuvVtIo1GQQK5eLepxsCbq6OjVJXrQuuZh1UI1e24/4LagxS6Bjz05486WXIVvAdtsYdu/aB9Xuw5CT22+s9cexc8w1+/lUhuBlE8yGZW+WRx1URHWdWqwOFJzAz5zx3Ky8+VIiTZSkSmYg0n+3vlnCdILZiVQWEIX5xYlDVAbGdXHNlM9sBmQU7mY+SQ0iHqmsBMtbSLRjeSupWaNJPOFn1dRYIp+tapMSJGLHVqGxiQBk4KBBsHXz5tyqFZ9ccNO8a066bObMe+bfeNO/tE+csCIcEWEdaYU23rBSq1wqFV5+4YULFj344E3vvvPOjLETJgCe+vbrJwN6TASush9XJRIqV1nDQwQUBRrK8FCep4BWo9nlNV12GTKgF4wb1g/6NjUA2lXJlIOCIk+UPciejk5Yu+UA7D3QRX+3wdmb1cFS55UvlRGRRxFLQYQZ9uQrxCN9fTflgzlPK7+Z1x9MKAz/Ilz5yxMQ+gpzZ+qorp/yxQKv7ddYoRgg0cDFuTeFxICSMOfByN4VPwRgNSz42hHgsfUXLVFwXC4q2lHEmcvmSXSIflpNNFpXtf/m0S4llycBI3Z8HTrYARvWrIF1q1bg39fMvvrqu2bOmfNAGK0bVgCQsI64atVq9t233jr5oXvv/dabr79+eb9Bg1rbJ06CgYMHU/Ah4NAlEZxD3u0NdjLDnqrGWh05DvSuSrRfFVdKclSVN+QycPY3J8HRU8ZBfwlK6PlkAip+FcvVMuzYtRve+WgNvPnRp0QmZxBgqJMpMt2yGjBUgGa6zcoYFoIGFFfBcoOhnFBQ5xams0oIQ6NrMt1r6RUeb6LBCISnFjGZh+AuuzGPbR1907YrTGcYzu7EdHeBmylilO2GI7FOv2DbgZ2A3uOA8LXL7A0BH2e0o34Eh1mpaYg5ApR8rkCeWo1N2ParSlsKSHLydhFtAvbu2QNrVq2CT1evhlGjRr05Z/78Oy669NIlcjOxNxwxYQUACYvWJx9+eMzDDz544/NPPz23sVevAeNkxjF02HBobG6iwFapVXRLbpFmj+NPLFVh5oEEOWUcSJKbwU4cTQ9rOqPgunsJASSBlkIEJ4ztD6MxA2ltgUI+YxMLjKfdpQrsP3AY1mzdCx9t7oCqQINBN4vDD5SgSWYBkafVoP04QF0ZCbUWzPhdGXLa64oSzAFKyr6kR5lJpLQifhbkrE/A40N8U/e6SYb+vdgBVeB1E3vCQ53RcD3fxIgShX4RJkNheoZJpAn+OBMRkFAm4rn95vT5Bk2w40/Di+AwKxQs4u3wc9++FUfrroLtW7aUJ0+a/MrcBdfdce6FFz4ZZrSHFQDka7y2bNo0eumiRXMfeeihb8toMbJdAscw5DlaW8kZFoWA5WKZaudd3cpevainAWJgwfnk1aRqtQjKdiSBmkhAJMLV65URrQSHMgxu6wNzLz0Dpk0aB20D+spglU0F1e7uisxA9soMZBU88vTr0FWsko25sB1JdUBisgbWk/NgqXJXz7B/pIt6XsFkGfXXZT0vt2AknKniEe5V1OlN6o9Bk2FYoPCABC/jgqeAxYzr9V884YhV6jM7WjfSJS3MNNQcEjWLBPU7WNLC0boGUNB7K0dGjUCOv1s3bYa1MhvZs2N76Yyzzloy7/oFd5x02ukvSaBJwtH09VyBRP8aLuysenrZspky67hlx86dqCCH0WPGQK9+fanUgVlEWU/9wzIVZhxdnV2K56iUFM9RUTbr1STRg52qRJALbXaoKizcywKUkZTAdlMsf3V3yfuM5f1kU4GvWwJGqdhF5TAqfxFoRNq+RGcKdVP/6sGDlOWQFhQeMZSLNNSYiG9ZEMHBigyNX3zK4MTs/n0dokiNv+3Jh9R1YPUAMkb2KLac5nWeWRChTMjK1lU7sPd+c5M9Jdy+90JPRYwi/IwS4Khgl+BRrWbVPPmK6pbDTNI0QNC8dk20NzY3w7iJE2HgkMGweeOnhfc/+HDem7/53rkXXXLJwnkLFtw5OSjaQwYS1ld7Vcrl/IvPPnvRg/fc8+1VK1eeM3z06PyY9nboN2AABQnkOZDDKMoAUpTAUSpid1WXnUVe0QHGlKqwTIX2Gihi4xo8hHDzMJwrrhIBluX1m+Vm9fgxvWHkoN7Q2liAbCZOPceyBJiDHV3w6c4D8MGWIiRxDgq4C7aZhPDKV8563fZP2d+Zr+cDJ/RwnVKprinm3da7DKzbrwdZ1irFj/4sTbIbIGIOcAQXqZKXLZWZmSFMkemcCStkdF1XzGYvRKmYyVWm/OW1EJvPQDUMCGshHxnCHX21ZCaCGQmWqbLkr5WxkxHz6ParsxDMSHAyYiGvPLbw+lxuFvbv3Q8b168njqSpobBuzjXX3DFj9uyHho0YsTEcaQFAwvqKrbffeOO0+++889u/ef31Gf2HDGkZN348tW3m0UNdRsOqVpB3FxXP0V1S5apiEctVCBpqnKxqy1U2Gv44WaGHO1mhuNE9gFFdR9Al739gnyaYecFJMLV9FPTv00olFJelAGlDdsvg9MGKDbDkhXdkxsNlMCtQwGSqv1Xv1l3pSnhjY00XlX0yHsakikoCPP2FV1qyXIXLAOyltpXWAaSwFipgu62IkPeIeZejcJO3OLDx8hnLmIOyirfP24CyVrNz8zfhgZPwGBchLKFv3jfhTWPERSN1I23SGKshVrlchuavI6AUyKixoIwam5odP4JlLezuklkNZip7du+G9WtXw4Y1a8WokSPfmnPttXdePmvWwpbW1kPhqAslrLC+5Audcu+7/fbvPLHsseubW3u1HXPSSTB42DBoaWlRwIHlKuyiorKS0nFgeYl4jkpZZi0VssegtlwcKaut1VH5bIhf5ZTrrESsoaEqJLnsQV6WzeZlxtMGA4cPh8HYhdWQ0QpsBTpdxTKwQjP02dMp70b+TZQtOWxMCY13lInDjlgX3oAP4WOINyXQ67QSwpWTRH2xy/Vk2TkgRkFutB0+8U7A4qlQrMWJcEpzYRyCje+uC+wORI3vlZdxQWQV6Tp/8VIrsCUza7ViMy3mBmVxj6RHHkUDvoBE28SjwBMBpCIBokaWM0rPo1q2ETwa5XeiubGRMhQcboXdeUNGDIc+/frCsBEj2bpVq078f//mb054atmyWd/90Y/++wknn/xqPp8vhaMwZCBhfcnWjm3bhj2+ePGch+5/4LuVpDZuwsSJiiDv1YsIcrK+kKBA6nEqU5WobIW/mxo4lqpIy1GraQV5leroZKthBiZpUtc61zJIDXQyJoioTyhWqtArz+D0yQNh3PA26NXSBPk48r6NIDOOGhw41AWrt+yBV1ftgarc4zTmMiojiDwRHih/KGW0qEpawpRrUv1PfkA98ne9XjTuX8rqZYPW/NcbB8X8jMXnKlKJhgMy5qBKpADOAUHP49J1jRG/ZK1TvHIVeIS8cE7Gppsr3c6sAUeP/sXPydjGYykrzqiurbwE/KyeiFjIq4ykwbOOx8txkBU+ULf8Hm3fugVWrVgJB/bu7Tjr7LOXLrjlltuOPeGE34QjMgBIWF+CJQGg8amlS2c9cPfd39u2fftJ2JKLM8h79+lN0+0wg8DuqZK2H0HwwAMfS1cm46gY0MCTDOiJcDyHq7E7AGFRfWeUG/Tkny/KnWyfxiycf8I4aB89CPq1NtPUPT84F+Xj7zvYAZ+s3wbPv78JKkkMjflMKoj7s83/MOt33JeoQ5g/8KP43LkL/OkrmbkhqcuOADCp9mD/71Y7kgYYex07aEv9RKCP4wwBieJHsmoGSQ6BJEO6Hdv6K0/NjcoaBT22EIBwc3GoowM2bdgA61aswO/YnsuuuOJ+nEEybvz4leEIDQAS1hdwyWCfeeXFF8+/5xe/+MHKTz45Z9jo0YUx49qh/8ABdPBzLQTE7MIox4vUmttFg54qCB5kC64yDSpXofUI8hxE2tbsLl8J4DxtBsku1PQ/sKPGo5TJIRK4XV1FGNzWG+ZfeT5MO3oSDG0bIINRPhViD3d1w7Ydu+Gtd38L9y95Xj6/MjQ2NShbD00Cp61KfP7A7PrNuFrveilPK+bxIlZYnro/Vqcs97uofD4evF2+9eWqe36pHEeT6+YBelikaL7DzR5RD2TnrtvnjbwT8ziSnlYnmihRpoyeLsX4a4EetSu8TjaakxKpQVa4MUDxIfIjdk57XgEKZh40UheFiAgmhQYi21GoGLOYOrsO7N8Pn+KMdgkkvVpaV86eO+/u2fPm3tc2aNCOcMQGAAnrC7AkMEQfvvvuCffcfvv3X3n55VkDhwxpRIJ80OBBVGrAzSuZ6lXK2inXDHbqoowANR4IHDWddVRrihynjIOsR5TlOM2psESy10rKjHOu2sEKoeaSU9yLmKfWjuRzqEJLjsGpk4dB+8iB0Ld3CzTkXQaihIRl2HfgEKzcuBt+s3IHJBBBIZdRjxWx35cvpHbcabsTsKpv28GlBz/55aie0g4PZY5Q2DLBW42RcikLq69f+dwMO/KTVkCmbsMjnyB34EV3qT4M95yNe7wRGhpuhbvJh246okfSc66BiZNFvH2JukmBQERb6ZMtirZGwXIVKtvR+gRPCBomI1H6EXlZrkBiRRQv4oZl766dsHb1GtiwdjWMGD78netv+dY/X3TZZUtae/U6GI7gACBhfU5r/dq1Ex68++5vPbF06fWtfXr3HzthIgwdPlwexE3AMoxM9cglV4+SJU0HdVZhqaoIZfKrUiryqjY75AQciRolq6M6tZWCI6yZHjzOMFxFkbMIsemH1oLbmRvq4kqNS6Dqhmljh8CUMaOgrX8vuXPNa+8q5TLb2V2CHXsPwMdrNsCarfuhoalZlUbcVt92W8ERqj7CTymEN5KWaW0K87p+PdddmwGYdl6P3LYZR7plS2U4iEIx2AYq230mmGcd7xk8HhFAmLWHt5Ah/A5lM48EnOLddHhpEDMZluU9jliy8lBW348CGuZsWKya3djBaCFirNt+tSDRtPwWJIggmY4ZiVW0I6A0KEU7Xg9fAn7vdm3fDmtWr4ZN69bBtGOPfe7m73znf5x13nlPS2CqhqM5AEhYn9Hat3fvgIfvv//GxQsX3lKq1drbJ06EESNHQjMpyDPKeRWn0xXLOtOQWUdXkcwOqbuqrFtyzWyOqso6Eq53q0iSw5G8ltIkuSmzGKLct08nXsQOT1J/Q+L+8OFOOPboAkxuHwuZqL8MNKqEpTbIgpx9u4u74fV3N8CWHRz69GrW1uUeY6DLVJEJ5BFXancX5h31XWeFbjIOszP3erKOSKbbR/0dJLxKUCI9a90rMQmWGnSIl3E0OvSyAczU8G2m3E6DHu78zX1gizTrAWKu+wsseZ5iU9x75TkD/y6S3c1r5zZTS2VTXgmS6Rntyh4lQy3YVNYy+hEk2gtO0d6g+RGjeieiXQLJts2bYc2KFbB/z57OU88444lvff/7P5l23HFvhSM7AEhYf8TV1dXV/OvnnrvwFz/96X/eun37ie2TJsGocUiQ96EDFFsxVdtlSfMcXfI2ShCIzrkIGhXTkquBQ2UbOvNIBaX6bipb/0kT5DrdMMOefBW2H4DwZ5II6DjUDaefvAfaxxyEDRubobUZ4OBBLrMTgOFDVa2/pbkIL706ADZu6wt9ehdSnU02CPraPb+byf7BjIYFL3syXUhaZ4GBX/us6H6yHlNtabBTDwBxWQZ2hhnOhdVpTlKW81qJD7pjzTn8MlcS1M8/AgbQQ13fs/zlRrr/fkLd+0UllUZBL3jqek5L4n0PmPtMzclwJBkrRFSlLbI+0UCCnEgDaUgkkODvKEg0QkR5v4cPHsQWcxxUBpXu7j2zrr76rutuvvm2IEQMABLWH3jJAB+/9frrpyPP8cZrr105qr09Qt+qAQMGQKZQIK4C222LWJaSWQeOk8XOKipXYUuubstVxLjqriKyHNXjpAFI9FwJcYQZGY4ONiUYF/C8LitwDrap23vnEUAOd5Zg1iUdMGXSfli9OobevTns3sNA4hxMnijIRbZXE8DiJwfAB580Qe9eOQsMPXbR+rwSZQvPxTbqMfDJ6UCOEFg9cPjdR0M6S6k3x0rbqTBIez5qy3ldP/MNIf0MzV5mGxSc6YlqIPDKUL5Viq15gZeh9QQV4b0ZqS4t5nEm3u0ccS9cpmnmlRghosyqiBfBLIOI9gyBSJaI9pwtazU3N1NmktejdfE2mPXu37cPPl2zBlavXAl9e/deueDmm//XxTNmLG4bODAQ7QFAwvqPro8/+OC4h+6776Znly+/tu/Agb2wLXfgkCF0YGJIULM50C23qDiOYsnOIUdleUV3VJHZoTfUSXEdumTFPHtwiluR5ZtNzcr43brMQ3XpmJp8esSsx32YKEclLCEzojJ894YinPbNbvh4JYP+/QB27hJwqIPD8d9AHQIDuVmFn97VCC+9loe+vZTZIhccfNsPYe0+FPBRxuFxB8K7PnhB1ijmiUQ+ErD45Z1UqapukBXzR9YqyaRxwjV/Y8xZzUdexsYif0YJS5cEmT+yl9nr+8/DvNfcZIMcfGInla0cCXTrL2e6M8sXVPZoFTYcSiTc56+fB+lHEDziDOlHMhZEVMcWcSJNimTHEciUkWiPrUi7IOzauRPWSRDZsG4dTJ069YWrr732rvOnT1/W3NLSEaLAF3cFJfoXdK1bvXrS4ocXzl/++JPXyqNs5LQTT4Sho0ZBi4yueAjTbI6y6qyi+RwSPFS5SgsBq3WjZGsSOHjNggapx/G8JpftztcPYl5gU5RtZGv1zPeksjO/jU06gosuk+jKFthBSkxmFXkYMSKCQ10chrTFUMglsPdAAuPHF9SXMsflblUGlnJVAqA3SEkPZOLGqkM4Z1rKpIQyiaJAmJjruGFOzGgd6ngd4lh0uQpMNuNnJpo7ScDxJ8xYF+pST6IbrfB9dcAbufdVDXonCxDQegvGYnqf1Gx0zSNFejAUixyIcDej3Wo2wPP7MhmCyXyYuZz3yAzr25n1WwM9J/0K05/tNgHcS3Rocpf6LmAmG2G3HmaySQxZbaxZrTnrG9Qe4XfVdGyV5O+N5LHVQFzKsBEjoL/MqIeNHg3rVq0656/+4i9OffLxx5+6ZsGC208788znc/l8OUSFACBh/W/Wvj172h575JG5Cx944Dtdxe5J4yZOIoK8tU8fEndx7KySBx+CheqqKmoFubJZx0wDwYPEgFrHQW25PFEEqRd8hTdJL9LBLjIaiSjyKyU6oAllO8JMS63zinI78kgFlroOVort2NklHxutw7H7qqmhAq0yw2jpqkF3uQYtrU3yOmj2x+WutkT+W5WKBjwCQN1abF1/OT1UTOaAEWTJtjxWdXi581Vtp6pWn43lebQu1+UWGraEZRi0OJcn6jTy6vy+2MOQ7Ylw4CsSZf+Bl9H7i8p9LA3Knxg8a3qwVlVfVqpWnKZGvgcVbryuFLAQSU1KcGyZxc9ClYdif76HARB0FouYLSlFZtqiJd5dCcyflmiGZ1luQ5etCLSEagGmPMToeISXfVjFfURckTKH1O3Q5M2lRupG+NkgzyHPozVKktGt4TRet0JcHH5PG3GTU0KepEKAkqfRug0wFlvQBw2CzZs25desWDHzz374w7PPPf/8R6+5/vrbjwmK9gAgYR15dXV2tixftmzmQ/fdf+uOnTtOGDl2XO64MaOhT79+ZHaXyKCDPlWo28CfnZ1dMttQ42Qx40AVuZnJgQOg0G4dVefkXYWTAfVuXMUD33aDaQEgc95Pke4SMk2lzNTfmdN26CFNNjNx/T62zdbNxFCit0SbLuL884Tn5e8YAPMySJb1br0ggQ4gFyNf0gWHDx+GLAKZvI3cq5NjbE4GJ+zuaWpuhgYMOjIjw/NI1pLRX15N1cvmlXqa6vMIEhicjUAOA7L83XA3FvwiV5axbriMpUpbrntJd6updil8wlDDIKz1M8qt2JQP1UAuVW5UY4A7NUeF2WOXHgtMAISfp5rLS4GdnjsCIarDIwUkeHnEmXUB4JrYZppT4Uy4dusobQ5ppjkK3bZmtCcEGtyZPwo/AcPOMOXKSN5alPnZWe/6FlyXQkHphhBEMxnFr8UI/klVlbRqOXJxJtv4ogIS/P4iP2Jcf9FuZ+LkyTB48GDYtGlj79dee+3m137964suuPTSh+ffcMMvxk2YEBTtAUDCwmUI8rt//vMfvPfeexcNHj6s6ZunnUZOuRgMsVxDs8clQGA3FY6Qpe6qoumuKtP4WCpZVfXscSwhyIOW2x2zUBVzv4mIMVfyADeulVm88Ha+FGAizxhQKaj9QU0cFBCla+eq9IS7dNrhYscX5xQMBeTkZVV5FQkgsXG7bVC7WflYCBxNcic+pE8/maW00JCrXjKwNLW0qKFHEjTyGjAw08D25Zh28ZEqRXnEvi3EsN/tSeLGcbCe9u/Qc7Rtnb48bVFSx6Fw/foTDTCUiaArAGaK2sSys7MTOiVgdhw8CJ0dh6DjsPy987DqqpOfdZmpDCWDJDR1P2Xoc0IgQUAWkbCW7cwq7XlaKOmVp2zXlzFp5KZcqc671ufIu77LXiLdimzLeOCGtZuNA5b8lFEj19/LRJW0cjWbJePGB0G1hBuBSiNUGqrKGkV+pr3l5gnBZMiQobB+zZqhSxcv/tOXX3zxwgU33vgv06+44pFAtAcA+Vqv33700bGo53hy2bKr5E564DeOP56EgBgg8YAtkUtuWQGIHsKEJSvysUIFeVW17SbaKbfG1YGa6F2winnOpNzyF4YQrvOwsoDgtZZavyRw7aq2tNPDUgSsko3jUKRERSMDZLVqQs+VRRj8JDhCVd5NnjqvKNjJ80JzGiNGjIRzzzsOxrUPgebmJgIKUjfrbKLHc/ZGu5rnzcFOiNVBkqc6kXxbEl8z8fuW44XAuu66J1I3NdHjWiibwN+xnCZfC4blRjNNEE8aWGo00KkiP+tOOHyoAw7t3w/79u2DA/K0/8B+1Z6NnANmJQUJvjIrixlmaJECFCrL6exKxPT8YmYyKWazKmZLWI7op22AUHoVO9XQfO7MtP1qAp9x7farVe34ioRrUKASKHqncTXEivOYSpCKG6lRNlLRGx/KoPF7Ll9bU2MTtf9iJpLXQsS+AwZQhtlfbqpWf/LJ5L/9m7/+yVPLll153c03/+z86dMfk4BTDNEkAMjXZm3funXE/Xfd+d3Hlyy9asuWLWNHjB0LE6dMgbZBA6lkUdTlDPSnUnYjZdWSW5KX4+8VzXHUEDgSmgZIJROyWueus0g4Sw+TVfjCMN8rypVz0ltq5g1nYl7Atippo3MQZgqh+smphKXahKmEJi8vy+dals8ZyeMobpBPoixPOeIwWFSTp4LMIvA5VqBtSH+Y+I12GDSwRe2q9YwNf8dvgElN2/O7tAS9F6CDmdGNcOs1JaxNiynjmeDn8KXH1HPvDRB25rgqfTHbORVFTlTJ6i73O6iEvh7oDInhTA4Z+PMNBWiSD92H9wU+VJXCsPzV2XEYDkoA2bt7N3Us7ZGnQzJjKcrXnkH/KQlKBlwjLPmhGSJTbdH6ATV1VSeIZJ59vbGmsZsBZluQ1chcLAEKzZ9HdiwL0/b93hBhLUbVGwgCEWFHAWD7L2Yj2SQjMzHFF9Hr1NlIQ7GBshASIWI2ohXwA9raqONLbiTid95796yPP/zwuDOXLp1zzQ03/OyMc855JkSWACBf6bV/377+yx59dN79d9313TXr1k3COjfurgYOHkxOuajSxlUj8KjSAYWZB5Y5Kqaerg821d1SVcRyjetuJO61rQobnAS43bEVnzGva0f4qnKWnvCXsqQ1u1Fw42K5sw1XpTIdoJFk1pYopDeRp9LhItTka4mZDHTQIO+iJO+1QQbOqgx8VXm7gtpBy69lBknvbAbU7Aqhaus1BQo1A5T6p8q6OHEsnLuT+buyZXGzS4Tp1PI6u3zR3O9PQZhWt+s+qEh3UEWqhMeQrYlAcxXM00tEFNTxPN6WrEFiRYY7biayvAZ1i0lQyWuBXktLK7QNHgSjx41Dvgz2790LO7Zthc0bN8NuCSaljg6oYYYmd+7YSpsk6GElsw8M9JEK/KZSpZZqnYtSxHrav8u0IQuRFkX6b4U/4teVCN0YLaHFmdSdhnoljq+dkzlnwjNQi2U2QtqkRH/f1XgBtEchu/icEifid4GknvIx+rYNhP4HD8KeXbtalj/99MzXX3v17AunX7r4pu98+58mTZnyUYg0AUC+UkvusLJPPfbYrDt//rMfvf/+ByfhAU21bLnbRAJgz/4DUJTAgNkHHsgGJJTwj1u1OB5kKjjWdF1ZBULDcXBdkI5SYjW1S478Mgt4Q4mA+bUWm1mwIwQFJ0uOrGGf8OreajKhUFoTzXfg6+Ayg5JpFDTFOeg3ejS09u4vn1BBBrgGItEZcSBZ+VAF0oEwnC8hHxcnIRZLkTfwyOlYakmVSmJcCyHx/cCMB3fKiRZIcs92XvhjX3XWkjZMFP8Kh/ie89NtRmfbdhW4RLajy5WWyCZdXxYTuR9R1xgR/bYDy5D+auwsZhAEOvIxzAjaQqEBevftC8NGjYKJR3XAjq3bYMO6tbB1w6fQJbOSWk6+l8ifZWR2wjVQ4f17mZA/Y9583tx3IPAnoRDnoY0aU5bAWrRZ53osdFYG3FHyBqCEBnDkcxh+hkxtljJRCaKMAlh6/dp/SzkBq80EiV3lZ4siWWxYaGxpIZHsoe5i74cXPoRaqRlz58//xdwF190xasyYdSHy/PFXEBL+ERfOIH/z9ddPv+O22/7Tc888cxkGRjrhgaHr+eCVMuLIKcYMh+H38RNQ2KZ9Z6THUnYjzgTPOWI4BfTvUoofSTkuPPxgLF228s35uC4TqUCudpMEcBIAhTzA8/L1Dhs5AkaNnwy9+7TC/KvL0D4GYPW6wzB0UCMc6qzBp592wOmnDKTnijvnxU9V4ZU3WkgzQi2gWlug7rtmeR57Em6ni383hK8l9YUjfH2wNa/Tig6ZKcmldOZQrzpPv+fgvd+ubMUM16A9rmLL3TCdfUQ6qMdWgEdBUzveZtBnCgEll7Uqb9VN5nQkoIl5BI4tmzbBqpUrCEiw1Vtu3YHRcChVHiMAw+ehO7zq57XY754uSULKCqZuvgh+z7gzGe4xb4QLz4eL2y4wN543bYBpfIRNBmZt6cG0kKvOMXcMqM8I27vpuyG/aww3WPKzbx8zduWcBdfdOW/Bgjt79+mzP0SikIF8Kdd//cu//G8PP/DAgsOHDvVmqtXItkxSPbiG9WBud34V3zkVwJZInBdRZI86xQu4jWAUmSAXpXaVoKfOgbe5NiCgXHVThX9NjruurHSQdBtxu2HnrlxVo8xABne5o4RSGSJ5Wa9+/WDyUUfBxCmToamlL3R3HpQ7zX3UaRVFZRCR0mRgGiJkBkJ8SIxgVITdu3dBuVKg8hferw9U3LYnCx1YuAULa0PP08OTDBfi26KYYAdGja9JAdGjXOMBhZlP7ovvI08MyFjafNKfsWHOx5HNRszfzN9TQEKTAXOqjOMpvHFKIJb5sLUZb4c6oUnYsTR8OIpQ0cEA9u3aBQKDq7w9R55EPm/UvgDt8IHmdoDXsWWklKZrzWkO05MWzWx29U86bTOWOFZ3YhsthCfe1J+T8eZSzHuPz8oO07J7IGbbkFWCo50FuNkgqPtbvXrVpP/rxz/+72++9trpN9x66z+fdd55gR8JAPLlW9d/61u3TZoy5ePlyx6f+eYbr59ZrFaa6FBLkORkFAgi2hnG1kEWD4aa3slzbame6pby9Bn+rtEMQWJmiBODOusR06EUeR1YwhazhA6KEfO4jsi3gnXWsq4cocpU1FmTKF5GVCRwyN0v7nIHjxkDRx97LIwYNYqsLEolLq+HpbqcKs9Q11WBCHRg6HlVoABH6uwI7U0Oyssb5HWUCNKUqwx4GF0J10FEBR0FHAZQ/jUAkgZtk1mZTiu/06wnoFhGwQP4IwGI4kNUKSrSOpSYBIMRNQ4YnsRkKnEU6eu4EwEIail0+zKaE2b1Kaf9pXr17Q1Tp02D3v37w0fvvQeb16+HmswC8f2p5uQLy6pDPmZZAo1IW9JYbRC4ludIazoUR2a+A9xmZ8LMqedcA4f+vurBY5CCjsiChP8emvkyllvTGxUzBCvK6gxNl/gi2ykoVLarhbPgDdxqHz9h1UWXXbrk4ssvXzJm3Li1IRIFAPlSrnHjx6/C08w5cx781VNPXf7UY49d9drrr5/TITMS1ASo8aEFr6QAZKmOpYpaEsuTC5isjpz0w5fVOUT+tYzdufISoSzF2uWCU0EbcZnhPYRHruvOHOGPkDVcScJtQDeDqKBSAiYPaFR2jxzXDkcfdxwMHNhGBz6Oqj3cWSRtQxw1yieTpy6sCIWEuCuWAIK8CJKkhViVaPC6jFXpFZv3QQFszXZ2iYRb4DUAona3LqtyU/pYmss5EsNhRHXC82IXad4DovTNhZfN+Vke8R82Q9RgYdqfI6M4V4CC3Af9PfZARHMBmViVnygzyahyF3pMGTAxHUtIPONlBN5DhiigkZevW7UKaqWijP3ys5IgbTYWichQt1yGiH+WSjtV/I/sZEdrT6+FiimrfwQg7rkeR06sKux3jtPGh9tuOpddOExwJUULolFk+SFmmkKoJbwqsyuZ7SI4yvMxi5IpR33jg/Mvvuixiy69dMnEyZN/GyJQAJCvxGpobOyeMXv2Q+dccMFyFA0uW7xk7uuvvnz2rj17huCBgK2KeRLE5ehgwSMISxxZrghy8rKiTibuz7xLcyQ2+4g8CjRKzaMQprPGyIyNuyyLbLlBlSeYHjhk7E5cZ5YJ1qb7qUbZRwJQLkGEICKD3NDRo2HKtG9A7759dHtmhTKU/fs7oVw8JINDq27j7ZZ3nVO7b/KFUueByjsZONzRKZ9DhYKuaQE1bbtYMrMsRp2Zoi15+N6CrM440ff0EuAJCJmbXAhut+tQAaz/FbPDOYQbR6ujIdOTA4UeGWgykireDst08nVVInCdV6a0hSWtjPppOrXiyJkUxh5fkiWuJEvAgboJ9JkqaL0M3hc64E6eOpXAd+O6dZSJyDSQCGhL/qM9jhYjupZub6qjbaQQdvYI88b/2kFaBBrOkSCy7s5CD/9ilmNiXKQAWdRxS7EGzDhL+ZHzBdNl36pu98WW9kImLsos9/3pl8949ILpFz82cvTo9SHiBAD5Sq6W1tZD51500ROnnHnmC++//fZJSxYtuvbll146f+eOHcMruFvEAFBokAdOrHkBrDUIKndluLBdTcITxAlmyicAaX2Gd6BHfrmaeZmMsOCTmp7nTezzuQ/T7mo5D2zLxOcjwSPGHaEMbFg6GT12LAm/UL+AZDpaq6Cdx/59HfK5d8oAmCMhIbbzImhALO8jyioAwZ1mrIJkqdQtf08kqBrHXaK/ndLbjmn1S3U+8+/KSyqoaY5IpxHGADKKnMJaxCbBcLoIiFmPTCUyXAiZIcYuNTGEvKnXiyg1b111rHE3NdDyBZpnwHJWVbUCx5pgJ3U9qvMpqGZsScvYomdKGSjmiqQVKmjL9Byp82PKQEaNGUNK9z07d0Aigy7DgWJ6R48BAEWBZNiI4B2lRZD2a5DKyjTXwd3QKfV+QZ0832W0pMJhTk9jiHNTLosjBRxRpDvSIt29BtowU4Igdlyh2LAqv1eFXK7rxBO/+faVV1314Jnnnvv0kKFDt4QIEwDk65GRNDR0n3LGGS8cd+KJr3/43nsnLF+2bNbTTz45c/uOHSOq8gDPoSeQBJJsLqNKW3hwaRfaHJW3vE4kTTam+/Try1xOgQym5qx6bdz8Po8Xcbf1ztUNHKIyAj4+8h3Y8YN6hcZGsppHryo80PE6lQpmIGru+qGDh2XQK8vgIEEDrUyoDp/VASaryliAZDqCSZYEkkkt1jPHdUC3JTfm2AlPAWm6i9ygPuHmlQijhVE1PREpsDDEuJrnbjBJve8cvG44/fbGOvMQkX6vVdHeaifc7JB06Yvui9l6IhjPY9OvZFTcyk1Y/k+cVGI76KqYqUlwQWCJM4pAN15f1VwVypkKuRfQZEBt8UIgITOSAQPboBvtUvB5ouEjZiJab8I1h4HPMFIvSnNq3O1SAKztvz+u15ug5Q8nAS7qZpWY5gwhUtkMGWHacbmxEjvq7jTi2LTAsCLBETMOmXUVTz/ttFdmXnXVA6efffazwc4kAMjXdskDvfTNk09+5djjj//NpTNnPrzs0UfnPvfss5du3bJ1dDVfVEAid5TYxollDKF5yJhM6rJkUJckpo1VUJgFr4RlMw6tEGd2ZrYvDGN2Jy/0DHEXD7zeftOymyhhHoGXPLgZggcGNJzx0NwMBQkedsa6KV8h0SnBoKOzA1qbZdDK5pUXVpyjEhYjcjlLoCIwBWAYHPPeHBDXGgsMUqUWa5kuPEN2344+cjYnLALH40QsvWO2DQSQ4jIi06nGwc3lUI71Hi+lH0e45oXYGlGCZwfjWrYNKNvHYiZ78ctFykdM3UTpYZjcOFCZh8SmkS1llctVyOVxnHFOORfo0ha1Dsu7QAdkBHi0SanIzy+qVkBUMpBga2+irEfITDESFkzAjv/ltqPPlvgMfqSs4kFdUzgVibW21yJTcuylRjDMMrJ6PG5MNvfKbl+9djRcxBHMOOemJoGjIZfrPOHUU9+YdfXV9yFwDGhr2xkiSACQsPBDyGarx33zm68ffcwxb8+eN+8eJNuffHzZ7E2bNo0td3XTgV9oaCQPJTzgUKkdx3gwRtoINp2RaGbTbn4NMQyCWVLUyAmdcFBnGZGe7+3t4LnZfdt2We3yK8GB4QyIfI5U00jaImhguQRvUyML84TAA3+iAKyQz1LgwBIWiAyBBQEI+mOhqJDOy8ASZ7XWgWm7cR88WGp8rEuyIpeMpKpOrK4vWbiSDDjS3BvpXpeBCYDI70JzgjrjiVWvMUz9NBnOETh8VifYs4U4UT/TnKc4CNowMDWLAwWVcaZK9umVbIUIdtzVs1IMuViR6tipFGWUpiRBR1wsPcrPj0ug4VlGKnEUcfqmkcIMxLJcmEiLDD2gcEaTIvV3+/0haBGkbyF+g4hx3U6M5VotREU9B5kryoyjWiL+q3bhhRc+hhusU8844/k+ffvuCxEjAEhYvwNIjpo27d2pRx/93ozZsx98/plnLv3lffd9a+u2raPQ+hrnJaDldbbQQAFCRLHe1WXkKQc1rtXaGBwsaJhNoymdMB0Q3LbRKJKNUthDFG+XrdxkQessMHDJ1EJlHzRTI6bHRqv5WpJYQR/WyZWiPiGPr1otIvBAvoNR626WyjRYjacTlbUyKrgQmKn6vwDRw0CR1YME1Ase08SFsFYukTUKTKUVbtigI498Jxfwxs8K4SY3Mk0Uc9YTPVJOMMyClwUWAyKm5uN6unoAjhNu4i+JLp2hpXwCGRFr6/Qalf4IgKsxVFDhzWLb7EDuxLp9PMFSFnYwYUBXk7rkd4rrhgKWmswoPOLbTIIkaDDaJnM9obIXJlxbL3F0uosMtSvUnmx8wjQnhN8RtLYvye9PFTm1KKpcccWMRXgcnHDSSa+GyYQBQML6Vy4ZWMTEKVM+wtOc+fPvfGzRomsfuu++W9ZvWD+x1JWlMhGS1EiQMhKdMaoQRElE5QzTWlszQEI7ZedZFaV2165EYQYn+doHFQ+Y2kVyJ3pk1FKreROmjPXQnryru4tq1sKKxDhlH7i7xIFCIBp1y25WlbCwdIXlOcCWUlTpZykrQTLVMP+2xOOByBHeMxuprYtsHb5EXlbml5KYd76H5IPVX2iZed8DXr1fFosEHMngwTU69PyjEox677zlq5jlnZxuQr06ZROv/KFqJL6XAIKbiUTIAFxTTgc6s0RARxLackPGABFBhTYhoG6ncdNkcSkVugFij8NIvVbdZWW63RgYOxJN/EexBQ+6F/yeVqqUbeCYAnx+fXv32nva+ec9d8v3v/8Pk6dO/QA3VSEiBAAJ69+5sNYrD6a/nzV37j2LFy5c8MRjS6/+8P0PvlkudlNZq6G5RWYkeXWAokgM68dEsCbWOwp3dz2sUWzJRWgzPeuQotlSqB9oB05BzK1xIkQZu4unDETuIkmrQNYTDkRM66+KjVly3sVMg4msDo94Pk9ZCYkKZZaihHXG2sKVr1L2XZr6MB1VZkqiKw9BmutIAYFrUQVfPGhKMyaIQ7q5wHSAMd/CnvVACucbBT6BzFKOx0yPlDXCO2N+aZyC630/lKGgzKE42AYK4kpQdKqdckkrgzepVdX1dfcebShM+wRj9vWpz4nrPNP3tTLdUn4rr/fegW9Xor8feiIiWbHQjPSMyziM5xaWXMsVKMrNRqmri3iyfn367Lp4zlVLrpTf82OOPz5MHgwAEtYfcvXp12/vzd/73j9cMWfO/c8//fRlv7zn3lt/+9uPjy13d2dyMhtpaGqEfL6gxrjGyoEVa9/IVeBBbEbbJto3inkzK/xOmpRPnt1Q+3372rZdpGBFd0kJInAziR7upLuHKLgxFAMKtQslDiRDJxFlnCkfqa8z9LVUpY7IDjWK7KRDbnUpTFgOXbczg/1bKqYzDwQM58NMicoNyzJgQEQuBnMiy837FNn3IjKtzmAGKzldiG0U0ztzpYdQ1+NWYuIAQWj9jpnTwjVXAJ5liu1A80bTOmt219oMPKL3h3gqsnXXA710Rmp8qCh7ZObxhAYit1RDRl0JTgj3OXslN+GBhgIO3VqMJSvTQYiCSdIz1WSWoQZklWTGUSuXoH/fvjunX3PNIzOvvvrebxxzzNvhSA8AEtYfcfXr33/3nPnz7zjv4ouXvfDMM5cuvO++Wz755JNjDnV3N+QLDTTiFWdKoCgxi4IzkCcsa8gDW+1CtflcjVObpgkkascb6TKXUg3bKAng5oxoJ1ew7a3Cutti/cPUs4W2LrelGApMie66yZBQUJkBmhkaSoSBWYkCEBWAQE865OAFcubEfkYTYlt1IycgVC6yOjOxtvXOIywlNvfpEqvQZz2mC9phjfpywaAHseyyJGM5o8AlthobYd9L+psRamLgFsrd1gRwyguEAgb7HEybsuc1ZTIc8z4B2nwI9XnU9Ax5Y8/OTZOE2yE4gLBlTMP3COM/aRHFwIjhOGI9eleVqrLKuNHrlkto8mIFyt1FGoqGLcRDhw3deOH06Uuvvu66X4weN261zKCTcHQHAAnrM1p9+/XbM/uaa+66+PLLH3nqscfmLH/ssaveeuutUw/u3dNMZHtTM7UAZ6kjh/zElUV4RtAgH2y1NRkJGQ8aQ73IOfEa5brJNtCTilMJi3m2IOl+fzsNUJe4rDcXKJI2jx1bVJtXrbsqVKIXWAw4Vw80gOSo4yyWYKcDuQ56wsyqcEy0DqieBsaUjkSae/DLTMxY0jPTXOtlLaYlmHmFMNORBJF2qmX2MiHqfMlEvV1KmpFhpuZmsj5XA6KHJRAx4K45k0inWq4UqbIxXw1vgAA5Ecad2aDTYWgnW5/pd2MkHV8WOSJcCH9OuhvYhdc3FiPY6ZXJZXR2GRG9gi+PXAiKSuBYkRlHVWanY8eNW3nehRc+ftnsWQ9OmjLlw3AkBwAJ63NcTc3Nh6+69to7Lrz00kdfefHFC59cuvTq37z66tkde/f2LhUKpM1APQnqKqhzi9omlXU8eW3JbAQzElFTpQ/FKcRut8lTDKlTtJvaN1O75MizRGe6hIRCMGbKUGjNR3qEiHapBCAs0jWoGCxzqx1ic+g2m40oYzIWI4q74eDG8gpvholTQxsLcMtxcEjNrBBeYPfbdevJdVE3991rn3JOtX77r29IaUpjqiZE5StfzW2CM6sn6W1mZD1xPXtz585MWQqPbD6gwCcC5VOvfcKM5UoU6RZszRVp0aMhh6wgXqRLmp7fAd1W6Dc3shlHlr5TGW12yCLtFoDiURzD3NlF5SpRrfKJkyZ9cMH0S5ZedNmlj46bMGFFOHIDgIT1BVqtvXodvOSKKxaecc45T7//9jsnL37ol9e/9sor5x3as7c/zs5ulECCehLKSHCnmItIQMaJ6MzqAU01IjipXKUJ5QjA1siFnnWuZ9eCRRDhO/46ugRs4FNkOAYm1ChEcdYb/xo7U0cVIemybCZH1h0lkYAbqauDoX0AZktcrs1X75Z9LqJ+3onXa2Z+Nzv9dJYieu7WPVmIMDUt8DumTHur1wZrZoP4XlnOhUUHXgM63nRIcC3XzppQZ1jccTtc/8u1m4CfjZEA0RhkRl6rseLNKYuxn61QPFZUN+fDvNWkLYm1Bxe5SGepVEWfI1fiv0pRAkdXFzkRRJxXJraP/+2Vc+fee/aFFzwxYtSo4FMVACSsL/JCv60zzj3n6RNOOfnl995881Ts3Hr91VfO3bt3z+Asdm2hKLGxCbJ5Q7aDspMQOF60art17KhYsxvXbrfAna0J02Z5zKuV95ibEaXFfZhZYIuuMmqMbDkJnwgXzF4xQ4OQYgsMUX2rk+nKYs72LzXIxNvZRz2behWoeVmUMP3NqXHhUbr3yhAgUbp11fIEVlzHUpmMzXo81bY/QpY4DuCpv9vyVuSGO1kw0RU2zr0CGWU+2O2m5k+q9407MwJeT/R4RjaCu0FPwufNuR5ApaxGqGSFyvfYWc+TPghbcWWmgcCBeiUhkuqx0455c/qMGQ9fcMklSwYNGbI1HJkBQML6Ei302zr1rLOePfmMM55/89VXz162+NFrXvjVs5fu27u3rdjQBQ1NTVBoaiTnVrIQB9V6m6VspKYU5Kgk1yNkKbTpunqd/FvHI2Z04G5Sohl65eUlsXacZfqnr/kGD0Bo4FIE4LO9jLG6llmvrKbdhilj8hTpdqgRc7oN5ynpmwMCQI8RvnWjf201i6V7g4XDKw4eMQ7g+pb0dbg1TvSfiOvyohIQU9P+nA6D2beGCf/9SFW+wOl6/DyQ60zM6HqEFj/qtrXUJEphO/SMjTp+HzBDVU7AEanVI2p+4OQ0UEGOA0tV3d04lLh07LHT3poxa9aD50vgwIaPcCQGAAnrS7xkIOAIIsefdNIrV8374M7Hlyye99yvfnXZzh07RhQ7C1DA0lZBObhiWQIiNZMiiRPKAGoEJGU1fxzLSUJPAWQi5diaiqfIC8Rek2/kikY4SY/aOgWri+b+BpnTc6EZ8YnHd5gAKNzQI/PglKWkWrBcDGWeGSATImUfwozfk3X1NbtwZ1diMis78pb5uhg/BjPt+MtSrvB+lxYzPcTcfy3O1dbatTBd7uLCDnxi2tHWJS9aBGoen5uao+62Yq40ZktbwntSNuVQDgRJwtSgMwQMHKOLHIcW/2VjPfMdu+zKaKVe0hlHNwaR8sknnvTKlXOvvueUM898LvhUBQAJ6yu2ZKZROfbEb7529PHH/WbmnDn3PrFkydxnlz89Y+u2rWO6cwVobG4khTv6bdHuP6NtxLW1ejUqQxnjHs6UEIkjdY28j6V3xkwwz9tQzRghZ/TYXJjYwBfpgOYG1inDvTjyhnr4WQ9Lt+qmxtCmMh4HcJHtpnLPKe1d5duQeLqRevPGeo8Sz5TSTtDwBiOlbHzrzrtx9SzlcMv8EhZzwIRzO5iqUGntBXj8k5fd6Psh7QmYuM/t0zbkuind0caAqc8ZtUS5XJ5cn8l3DbMOqpehILUEZZlplLpVqUrevHr22ecsnz7j8kVnnXfeU73C/PEAIGF9tRf223/j2GPfOuqYY96eNW/e3U8//vishx948OZdu3cO7TqclyDSDI1NDQQkxEOgTQruQsneXI3eJWWzk5p5kdzZW/gB32r2NIBEUaKK+4bJZUKXZtCPA0lzpVzGAOaAQJde6gc+9aBGmD/3qUcJza/5WEJb+NUs15nktBwOUNJW+czrEIPUOFh7Kw+UfPGln7YJTWYzSHMmxrIetD9ZpDujUg3CjNlOuciNKCEwEWlUhnqTMKa715DjQG4qn8d5NHmVgcRq7jhOyEQ7/rJWjWPmgSPrzznv3CewjfzEU099CXm3cGQFAAnra7TQb2vC5Mkf4emyK6/85WOLHpm/bPGj8zZv2TKm3J2njq2GRvTbypM6PJONybfKjJZFjoTceL0dtXWp9SI4lU48D0ESmhPpnKgT3SZj+ktBtxcpLYFu/bWP4ZHI1t4kEhY4QAg7DErxDi6DqJ+mZ+1cTMeWKTN5Pk4iZcDoSmH+/BE3Y4SlBhfacpeoz5yczbufsZnpj8zTuQjtuCs8nUsErl1YVbiEezzhQy1J/eiBuG8vYLqq9PWR51ADqPK6dVrNG0FQQg4My1RF9KmSGUdLc+Ohcy666FnceHzzlFN+3djU1BmOpAAgYX3N15j29lV/+uO/+MtZ18y766mlS65evuyJWZ988ttjMXgQ2Y4zSbC0Ja+bQ34kk4EK8hPMM6DFWJWpH2OVnrGuJCZYTEqoT4iGJdFfE71Dr+ksBONd4tmrRyozYa5RyrbcCrDzul35SZdoTMnGz5FEurxlyl9eNcjLOpzkhTHXGmwdji1IQKr8JLyykm8Xr4ZQad6DsfRExcib2wJg3zfh+5ekuBtvgqR5NfgcOfMMDY0Bpfp8tGWmFZVTW3CsNR3IOWGtK5HAUdOlqmKRSHK0GznzkkueuXz2rAdOOu20F+NMphaOmrACgISVWtin/50/+dP/Z8ZVc+57fvnyGcsWL5733rvvnNLdmYO8BBFUh+O4WdwVI6dq7N1TQdK2RJE1E8SpShODDJnqmbbRRNX1iQgWJDxUFr+J40Dqorvw/gVPme6AImUHqIM9T4GBb5sOomczgJkPKJjHrXDneWWVJJ5Rohuw5PEfntARjLJcz8dgnnhPpRKejbo3YZLcdqGu9AV+BuRpV3Q2JLj3OnTpzGZSaamLnf7HyK23BDWZVaJ6vFYqQVvbwG3nz569bMbsq+6fdvxxv8GGjHCUhBUAJKzfuwYPHbpl/i23/HT6zJkLX3r22ekP3n3Pd957752TulF0mMsTwc4x2EfMBj2mLePBc/Y1O23mBbuIOqZq2g9K2x/hT45mf1V5vqZrVDUlSjeqOc+7KuWGbjqZWFoZbj2dvFnxYK1ZTOnIDdqyu3rGUiUxv22XCa/dF/wRwcwL0M4R2AEUs8OZhDBkvvCbyCy4CDslw7kj0z1yz6PKIARnunnBgZbw3xNP7OlnHuBjEHVVVaAmM44aepjJU/++/XbNXHD9/VfMuer+8ZMmfRx8qsIKABLWv3mh3xbaa5970UXLnnn88VkLH3jgpvc//PC4rq5KDnUcsQxqUeRlGB4H4CzVTY0+IsFgvhGdeKt6DoW2mZfAweQJfweoUOklE3PIIe+ix6iaNmDfg8sS9l5E9BiR1L+gHXZ9LkQc4W6sOaP11HLgYXgQ4X4Bf1tvwEEwN8HPT4VEnQLet2r3TRudRboGiBRDL9JiSdtVZZ6nIt6NPTzT3Vpc9MjfqDMN54yXJGjgZMmRw0dsvOjii5fNvmbe3ZOPOur9cASEFQAkrP/w6tW794E51113+8UzZixa/vjjVy5e9Mi8l19++SyeJFk00TMesL7BO6rKo9RsdbVTz+XwS1eFGldZB2UfUVZupmUGwssEJhgw47gmASSyAdU53aZ85h1H7fEQZjCTqWvZ2zLujAFTd8MhJWg0QdzXSaQud/HfcxVxob/HACbu+byDx3iry8wQSHcDJ0jhwnM8BpdRCFGfT4jUT9MMwMGrz/kqe23/gqaaQ4YM2Tnv2mvvuvzKKxdOmjo1GByGFQAkrD/8wnbNOTLQXDB9+mNPP/nkjH/6u//2Vzv37BrFRNpiRFiyO8VG0G46n1WEOOcV+dcy8AQ1BhJVQAJIUkUnPjUmN0ogm4u0XYdwPlF+KQtceYrKUZ6xIfjzSrTVij8EKjWryZLZLJUrOPGfe2F2Z29GAYO7b5EiWiA10yOVxYAj/hmIFPFubse8MlvKbl44gPKt3EVdJua3VftgYwBO6Pdz3PgJ2//+f/zTdSefeuoL4Rse1r91ReEtCOvfunr36bNf7lYfHtM+bm39dFYBfuuqK+GgFxN2U+URK1hJJh8q20DeQ7AyRGQVX5Un+bekBJlIZiA5bQTImDc9EWznleFeUpG5LmOwHLsRZev7EnWaC9vJxTxi3CvFWTsRry1XpAh+0yLriQGF8MJ5/SRCkRI8Ondcc7mwEGGNJE3mYm3mI6tMYf4tNUdiylapKcBe3Q7J83MuOP9XATzCChlIWJ/p6jp8uCXhPD7yX0XKqsRvj2UoIuRFIssRMNQw7ow8j/O65eU8r7c1ElQi3Q/F66kOz+kWmG3ZVVMAnejE8CdHnM8rRKry40pWzCsDpctCDii8AbfCKFeOWEhS4FnX+STq5+DWg3B6PGDqqbuSHvMGQbGUT5bhaphVm9fdv3MvFoMGDtwYvs1hBQAJ6zNdAiM1dzUj5c4LlpA2w6jceFdBXVVZ0opg5lGRfytrHkF1X4kEfy9ryUcZMrEgEliYRlbhl5ccOBiLEmNuDr6oz/In3kAqkSbZbcnImykCXgsvY94EQ9/x17THeuUrVmd/AuAs5c15b/i8fhnes/HGJDIfLA3ZkkIZYctcpEY3gGEAkXlgleo1toDOBw5s2xK+zWEFAAnrs19p6gMA6sRv4LhbLGFh8M1QBlKSpyr9TCgWZolATySAcHkZ+VZJIDGzyZ2FyBEyA88Ly/0uejjvCuZPTUy325oBTb6tCXiB3ifn02p18PtwbcnJlNVcV1Z9dpB+t1JlN3tfXonMZlyeeJDxFJnvhOjCgbYGTkXSC6jn3VkU8daW1uBfFVYAkLA+jzSE6/KSsxBJFXO8LTsHMx6kRsDBqW23oueMZIgL4TIrwQwkoUhYhThjRq1CqnNJeLt6VlcfIqBidX5TKedfsyvnuq3XdDmpK3BwVuymfJSmV1wZzG3ufXW78EDClJrS+j/XdWXmjXvchkjlRk6o6TUFcJO5CNfV5R4XwBdJmsc0flneBFsc8sXTxbWwwgoAEtZntiIXlFK7euY6loQzalL6EM2BYAuvzDpIIIcAIsGEy8sFz6rgKP+uMpBId09x1SxsEwPmSjy2FOSsPZg/I114doiWUmDOEVekfHA9/6u6sbngcSzgurdEHW6a7APAs1402RRPj411bvYsfVt/IiDzQNPAg/AaFrQnJRHnAHVdWa5MZoaAmcyGsaAsDysASFifXwqiR5lr479IpIOwns8h9GhWFfer1IUlsG1XZhwcx+pyCSBcZiRJCYfZqvnh8u8squngL+zEPKYDdqpBuD7p0cBiuQ/wyQvhN4c54LB0iJs5bseu+ypJEL5bS4ov8Uti4FmaMI8z8RIhlX3hz8hXjpvzrl2Z87QA0GU0rIcy3xlECgtWFiRFei4jYywASFgBQML63ODDdgL5+jfXWctJfS5s8FL6DmzVRcAAbNklJXpMnAen0lZWb6ZrakSrMCNX/Z29gFThKDVuQzi3Wzd60M7bYHWBuC4i1/HNTm8Bou51g8eO+AaNfhuWlzH06LTywY+D1xLsSoHMsyYxJSi/JMVMOYuBG0kLbjQt16RHPXB4VvAifIvDCgAS1hcASfySi9ZpRC4gm/IJchtYwmLkulsmTy3sBkZhIU+KlI2ou0zIK8uCj6UJnP2HKe/4JHjqeejnxYUbeGWnC0J6ZogRBPoQw11BLF0W03bnApgV+qXxyEwlNHxKj5TFiSz9946nw3w90NlXyLVNO69rGjCz2Xma9klPHxQ2G+mBamGFFQAkrM96HcnlVe36IzWnnDELKJkMZh5dancsinqyHiMwEdidJWIdOhPI5RLw/a30vaYyBjsZUbC0CNAEegsezJuXYcpWwvO6Yl4H1RF8qAB8VaEFIfNg3AAMcyaLUDdP3bb8+pnNEQHZteCyVHlN2IRPGUGm55ZYAMLKoQE4GkGc+lT0e2EY+wAiYQUACetzTD4U1S3SDrjaRh0nD2IpCxXlmQyDQhY9r4q0i8aOK5Fop13kQySAoOkibrBxVkghp++Pp2K3fgwHCipec6/rS5BK22/ztfM1NNox8E0L63b6KcIcvHns4G3tFYhxvyvL6D/EEUbb6jPcC9gpYbtgKXsV8LUo5n64e5Im+/D/5mZGcfsaeA+oSqvZQxdWWAFAwvoc0SO9g/V0cJ5eIqKAl83ibHYEii4VAFEHgkCSCNKBcBwgIoxNOVAGQnP1OE+bDdrgnIq2XmaSNi8XxPULmqLrurA8sqNu1KtnkJ6+3zpHW+bXrazPlhMyivocQ6j55MrX0TM6JLDjaa9Ij0PxH9BmXMyR67bkpU0W7WNraxVeD2TmDgWPQgYSVgCQsL4QWOICqleCiXQbLVdK9FymYjMF1H4kCVflFyTRaxFdjzIQLHdlqyqwmpKRP74V6ohr5k/oE17ZytiJaM8RYHWbbmFndaQ8tEDfJ3fttuCVw1LNVkwrSHqy8z5iWVsWm8AYksa/sA63erzJBlu4+4UanAUDl984kLXzqrwOMKcFEcELL6wAIGF9fqDhhWFPkMeoFTdy0V2JCBnO4K6qXTLFzCrNWEfNB7XxUjUroZ01tgRn44SuS51afkutsQ5JlYMi9bfI7MT10Cahx7pGZpcuvDGw4IZEMa+85ZWVhBkkBYYxcFYoBEzgWnLtc2EuyPtPk6tIrvMjZnkKO4DKAxvLsXDdzst8G3rDhXhNBJAWE6phXU54aJxQhFeO40xEIf8IKwBIWJ/LYsQAqKAVYbDlYP2d/QQAAQD1HjHjMgMpKzFbwongpXhGo2yLdD2R1PTAPQ4N+QwUCgXI53PKWFF4NX4QWsWuO4p0pxZPDFhEiCU2GxHcZR48nTPVCSmYVyYSaeK9LrFIt926lR7epN4EygAY11yHcsLFFmfM0KJI+/WyyE0+lCkYT3VNMQ2m2p5EcPs86X0z4MgYPZbwRvPaZ8N5Cvnl54CvNmQhYQUACetzyEAEzyijRG53/Gp2uG/3wW2AzMQ4KArtSrqJdud6JroAnDxYk+drio6nTKQCufwAaG5pgZaWPMQZGRiTxOobuAmsMqAmEoAwk8GfPFGz1fE6ic0cmO1gYnVTDIXJTnjPeblCHDnbMhd4pSDwVYwW0ExRDTOvTCRfe5ZAIopjiHEWuT4pl2IPQDCbYi5zEORYrMpQ5oRgQK8bX3+tJl9/TQE1gon+qcp+3KribVeWN8ZQ1IrN4ZscVgCQsD77xat5YQVvgkwQbRCOVFDMyqCZzccy0EXQ0FSTgXSfDG4HZZDLqywlYRTwBLbvUqA0ZZouyGb60gCqbCYrbyf/JANvJBT5zL0yUg31IhItMMtJZICsSTDhtYSCakJK90QFVDJE0W3FzBWyKGup4xycuwg7ghDQtMi62evc1I50KQuBIYNAgXPkYwSPmH6PYjXal8CDuTZfwg5tAZPSo5hyHBgg4HbIFoEJAkmiwUS/3hoCijzhT0ruyN5YfTY8Ydb5BN2URaWjf/gihxUAJKzPfiW1nCOhI9UUioExxuCZgUw2Qz+zuSwFvUKuKAPnYaVCR0t3GTRrPKtmo4u8DP7l1PTViBUhqVYhKZcJaKx8gelduw7AOSbBJ6cGf2NgxGyEU0DF4Io79CpUa8o2pZZwt7M3j+NrRzyxoCI4uHPV1ZebQVaGoqadvzwXy+cVRzG97hhBDwEkI0EjiulyTMRUU4FTsUdGrU6ZEzeB3bNeMRYwuuWWqcyCaZ6E7juOqKEKRZjY1YYAqrKShECkJl8/ZiqYwSGnk+jXKTDtqnb3Cl/ksAKAhPXZl7CSSt7UcbDEFGcydMpSuSYnswZVqsngDjxSwZNB0QVrLFXVqqo8g3buVX2/2sOpWu6Eg3v2Au/O0v0LbOOKEFgiul8W4eOp3X0sgzYGUwSwrDwJ+Vg8ksFSfsMT3P3LgI7BNJbgVavUqHym9/5HbGS1ZoTCV4yA5VqEMVEUKqBn5WPkcjnKODIEGvJ1E3Awu/tH3iepcSrV8aoK7nQ+Ebb0RPkCdqEBt91fkeZKCESiiO4zlu8Fw7QM3w983cxxK/hc8GEp88Hry58InLUqZmYVReDLO04QGauHe4dvclgBQML6zFelWspjoI31TjiD5aoMBmsVzGM9w5xXa1DurkG1sWgpA6P7I1pDl+TNeV0lkqcOEGUZ+KMcgUGlyl3bKgVVip4628mqjKeQh1wmBwx/x+eCwTYbSyCJCWyycpdeiSqqzJOo0pYQ4vfI6Y7cp0QdVnjfmGHRY6nngK8ZA3akVfBUVpKBu1qVj1kpy5MEsJp8XMyseEJ8DTUfyH9iKvtFkMtGVMbC7Iles3xyNZU22NdsAIMhSGT048ufUUaVzCgTRKCQYIK3R18xBEss9yVRjRI6qrYl5ZbwTQ4rAEhYnz2AlGUGAqraggEzzuraPqjMomxKKXLXe3BPCZpQA8KtWFz95ApMIvB0JNqT/KOXCq9NPzbTIu9jpIyIRQkaVblpbsjm8v8/e+8BJ0dx5Y+/6p6wSRu0QTmChAAhIwkhhAQIY4JNMhbghI+/fT58Pt/Z2DhHbM73t3H2+XwOHDYYg03OOSiBkBCgCEpIq7TS5jg7sbt+9aq7uqt7esIGCa1cT5/W7s70dJqq963vi3tr6kb3lY2qHNOXILO2N7bDO42dEItluONcY0ASLSmFSEkJhKMlEIna5iS2GfZqHRUuKvI0mrdSKTuzm/CVuxziK8xWIIXQoiLGz3PGoVnHtXwclvMbNXOKAUYqmYRUIgGpeIKt/lNo8mPgpkFD/Sg49YTxMK6hrM1I9u/sbu/I9PbGxrET1Fv2LSMkefDjwFkIaSsti7aVV5QZJeWlVWkaOv1QSz/sPhiDw+0xiMfQD8KuDEGTAVmYXRsHNAYqxAY0BFPuR7JNf+hD6evrqVEjWYkCECVHW0gqZZuwhB0fHdfEssGj4kwzBYoAUl8VNuZOixysr80cYDr9bMN0qzChVYqKnBGRPyHcD4x3MHSZUxLFVbRWRdwOgXU9HR2AG/49oRRg7Eytu6SsZlP12Inzdu7tKN++uw3aW3ogxBQpAklJeTn7WcpX59yZTTTbh2CZfTKMEaCZh/qbRRFvpV7076BTn7MO20EeshkB3j8yjXh/PyTjcf4MShiozphUDSdNnwRlWnJ1S9PhiSE9NZX0H4aWRqhjB8UNSvEencZRnibw5fa569CX09PZzbYu/hKi96x6CidWm9CfMFprG0bv6YjrZ+451A+H2rqgjz1ILWzdP14vDwgwTSeHBC1mzS1tY6UaMEqUKABRcuSFGqnSZDodtswrGTDj/ZBJaNwOhaAR1igsmB7dOCpMu6mRbIhGMrOSyUy0vMRK+yCi5LuI/jWt10FiJ6EINeImety1sBWFpEkOZW+6Nlv9V2WS8XPa9u2CavbavMlk0/hzZyS3N8YWbN7RAp2xPoiWlkFpRQVjJCWcpXBfhZ1/wcNqGYikMhm7wKPTtNbqvEgJ3x9ZB7IP7izXdW7GwgtnzwISff2Q6O+DdCIJUydWwnkLZ6d7Wg690t/be2qqPVaXorAEGYjXNCa3y6V278YAw1lAZrp4FmjyCoe1+ky8t76S7TOzJgPTKsnGqvraujd29kzY39oFCQZ26DPBgAcMTAA7yfHAofYGMFJloEdjalQrUQCi5CgBSKI8mUhFNNvRa6ZSPOoJFVOGKdylcytfLoP4Yt1KP7eUPoFupqvHiBLjRHPLUJkkO+/iQ9eZS/78M4ixt8LWfnamordyu1e72rkfkRDMaTt4AKo0s3vp7NK3aHnDohXr9kJnS5yBSBljJAgkUW564iHHaMKKaFzBIougImGPWOHJ4TD6OiIW69Bs1sHuM53IMLbBgKOvj0eLnTKjDhacXPP6wcZ9te17d01lxzgvrJOsmmHZWYnev0nQ3iTrF+f2iVTmhCdgUvoeo7cdThptwKljoysau0Lnbd3bb7cV1qzcHAbI2/e11be3HJpaO27qVjWqlQxUVBaqkkFJW8uhCb3xZAgji7SMCQTDZZnSRTd5EiOBIGWg4nQYg60R9RDIHZkchY8GHF/PJKbceVhWxlGNotmF22TE2+Tbv2EFYE2rAjOzyOw+COeeXLr2/UsmM7YUg+6WVujr6uJ+CsLoj24n90XCYbZF7HwNvC6Nm38sk5VmJUNqVlJjrK8XetpbobetDU4cXwafuGz62gmlsbbWAwfmM6Yx1QUGfzsniXU4+9jOIJptSQq2LZHA30VqiXjuJRHGlCBzHrsuOGFqLQPWEGgI9IZVHWB/S6xs27a3zlAjWokCECVHD0BaW8b296d0M5PmJqw4bjz724rKYmgQkoHDzoU2hZOaSItvGrAIl5Rh3KNKZRCRgMJXxjbrbwwj1iCzEHoPwRWL65efNqMGejs7obu9HRKxGFfe3B+CYa8hi22gIxpzWLjJyk7+w6iyDGNbvQx8ets7oCJkwCc/NDs2sTx2uL3p0EJGVOpyAlogcAjwyA0VAxUiYYoAFDSdjR1bDWWjayFUWsKjwDCktz+e1pevePmSdDodUaNaiQIQJUdFtm3bfnI6bZAMU4RxtqUk3adhzgY1Siw9JnqX2zkNmndpLfLlNOJdSyMb0UL8hbR3jS2DiAQkVFbMkBNM8Dz9PT1L60Nd3Z/60KmxCE1BNwOS/t4eXoeLRyvpuh0WG+JOeCu/RefXkEwkuPM+1tUJC2fXw5kn6K+37N1dHo1oY3MChnPdfuCAguCRG1JoQULi74PFgTSsQ1nNaCitqrQy0xn7WrNu/Tm7du2arUa1EgUgSo64sNVq+M2NW2dn2Ko8xZb2htv8g6s19BNTTC2XFJq9KsYoWutvKyfOrjRr/W4nmTvv8+RD0JLZ6pJK1MUMZiRU3geygCQa0ataGneXX/ieUatOGFMKPQxE+rp7+MocXRZhO78jjCCi67ymFDIV3M9M9MM/L5vdXUU70+xe55Ms9gNeQMsytdnX7ZisimQegyIoJOsY+GzLampgVF09N9UdONg84fHHH/+nTCYTVqNbiQIQJUdUmg83j9u2853pSUQEqa+TaPIU5gUCYVSgHtO9oEIhR3M8BBX0oWg0U1CjeoAEAk1YLr64QMOT9dKJc6ZWxdsWz2mA/u5u6GVbqj/Jw8J4mC8W/Eil2Hu90MuYR7megY+8b8KqlsZ3qsJhEvaCFuSwx0mmKuHrKAIR6ECAISgVMoCVyFdWUjkKQiWl/Pfnnnvu6k2bNp2lRrcSBSBKjqi8tv61hTve2TuZevqUu1qPR16BzUCIV5NpxKsdPW/7ahbaJdyNwstyH5BQM5B15FL2pVG9rpJ2wmXnToF4by9jIp0Q6+nhjCPe28f9HbGebqivDMHS2WUvd7e1n0OyLGQ0z/nAvabAfYu4vQGzDwIuLtseKOrtoCLa5uL32NLcMuFvf//b5+LxeLka4UoUgCg5IsIUTOnzzz13SV9/f6kbTGp6lH/IMoQEmkO4WUqUh+JhvDbOkAhQLey8bpm1qtCxXSSA+LVsEJDQnJ/D+l205zAsu2A6TwDs6+3hTvbezi4GJP0wsa4E5k8hG8DILM7t6wgCjXzMaJitVQU/TKT+JnZFYWcRQOGlF1+66smnnvqYGuVKFIAoOSKyZcuWOS++9NKFDvegdi9CYhc+56GzvE5TJBA/NNEBUGefxTzECgYY2DCK/aQVdtUQS61hbSe7PxKQXHjhMRtBHiAxc4T8ugfGEN10RxNcuXQqECzMlU7zWlWT6kthzjhzQ1iH04P7zPrNZDJoBACHhwFBUPTxwFlHEfua9oGp1K9ejjtIpVKR22+//esbN248W410JQpAlAyrmKapPfHEE1fuP7B/kqwHidXlyWl4h82TmMKPeI0pXAwgVYAbBbZhigbDGQKVDDOw1AZ+pIa/p5Fqtl8pApGoPp5fSZJcJivIAybZgIJ5hUbXIVg6bwxQBiLVpTosmK6tDofo6cHa3g8aeYDDY7cbDiG+X0nePTXxpRGXhQCI0vLW7wf275/+m9/85odtbW3j1IhXogBEybDJpk2bTv/73/92nVORSqhG25YuwkXDPAkPGYjrGbdXubqmI8uoYhsyjjLL0kXKQSdhsD5Swd8DMgpCkUJBQW7PjCw2EphkmAdMpA2ZSCTZ3jZj8ii49qJJu8x0ekk2dpg5QAMgMLQ4i/iIZ0OHz3SVBS0+QBE9RaTLFa1vCXW/0fXr1y9ds2bNRWrEKykkqpSJkqIkk8mEfv+7339u/4EDk4BK9ahEG1dnNcuoR5jE2fulWUqNEE1nwJDhjlsTqG5y05eG7ENPWp9GRsJ7h7DXdWQ9sbCuEQ9cBK7AcwGMUzdFVs8kG0x8UholddOrk+mmxgMn6kUts/z9b4uFAjIE8KA5jkdzvOM2wSLUC8LU93RTqVSJGvVKFIAoGZTcuOw8rbIicpbBgKNm3PT1e3bvufCB1VuukavveoswCV4CENVJjOn8UlnrCzu7FmYMI21ygOArXzPFfSCWBUa3ficGbxYViRIIhbvjchwWHZQFSA4/CqpKGHzEkqiWnwJRWqRSHxwUFAc+NOAABLJC2rBMo90el0ikzSo2bLXRBVOsB9AfBHDTskVLR9fXJ7/1v4+stQuRKVGiAERJQfAgJNl2S2dn2w1Yca+zaVv7O5kxh3t7+0YhG6C28tTk4n5SNfCIjuWwSNZin7dD0srAdmuADhkLd7QKCBHLSs/W/nZ8kAbRCEZ06Wls/BRkuBqcBLAQOlTDEX0XP53niHZAguPzoHbcFfXjps0/7LIACDQP/Piz36oqhfLDnbvNL14++4+/eHzLt1TJdyV+UT4QJVlSFiXnpmPNX6E0XUfNzOhMOjmjrfnAOVb/c6GbXIMIFaYsW3VpOjEICVBpvGRJCfujhO1eAiZuZoi9jH+HwGAbhai1oQNdj2JpEXpklG6uaKzBfP5dhp7A2u/ZTIk4pkbqvEylX6htmiwvIQzc2XdPM6Opma5jY+HLX71myXlqZihRAKIkr/zmu58PJfralwA1w0IXRcMEZtaH3sAVLDWpvGbN0lnc/AGQyrIKiU5NWinfNAYQOsEcEDRZMVDRouzdKH9d00r5a1ooiv026NFZudMBbsPGE46sEJlqUKmeoxu/a9qmR1H8cs6kKJRGNNeHxcZCJtnDxoShLBZKFIAoyS2dLYdGp/s7P+R/vVJLzTvrpKo20UPc65QGu9Md5Y2Xwmib8q2C7UAtsExYZewPZB0IIAgSiFVhG7PK7PDdMr5vcUp45FlW6FCvuVgs8+1jyt+Z+F7Yq6gIsBrx1PoIpA5up1gFQBY2Jq6iqb7RaoYoUQCiJI+YUWpmav2vRkIETihPaTMnlAP6JEzR/lUoQ0Kc5DRd44FWXmOK1atW4+yCMwzGNjQ0V1mgoWshHsar6SWciSADCfF99YKalh7R6UGG/ajDcr0DdEdwd4jpsg+nvzuhvFQ9so/aUTpU9e5uYoyT+FUDjglqpqJqfiiRRVFSJV5FQWl2XKn9SllUG71gLF3f1hM6QyNhtkrVSHtvEjxlESmvxpsmgYoXm3Igq2AAZGas3BHaz01aRCux3uc+EsrLmgCG+TrlN4pTymTI+l42y2HD9owPROgQnu1RoTa5X+axDnY5XvQ0hTUojYSgN2mynzpMgYNrIxFYSHNVW6FHAE2VKAai5LgTEqyECCqdMz5wasXy06eW7rnh0klQWxl1NQ2xnOw6YQwk8Ki4sEWgwIxz9pMgA8GefyWciZgUs9FLwcT3uGmrmFQEOgxKmkB2OWAbQAKr3pIBMxM65FAAWtznSPb78hW7xYIpzDuhDKaNifAXz6jrWRHVjIV57kuBhxIFIEoGs6j11gIPm5mlYyKp1j3b9h+6aGZ4ZWVZyErFE2YRkHIG/IFC3InOgINGuBnLxIrojG1oyDhIiIMGvqexnxr345I8CpXmVNa0aF94Hr1ItBheU15tXeD4WT2uBnAfAUao/Pv6q6XQXCBG4ZTJ5XD5mTW8HP8FJ9J1Zn/3eQ7QEIUVShSAKBk8WuRtw61jb3ANFmLPjxLNPPcDp5WviOiuMtUINXNpZDRhUc44QhaAZDSrIi8DDeBO9Aj7O8R9ILpewtvIDk7hFrFnXkWJyXV6vKBNLMf7A4/VKvSJoWW/8MmuWYmENRUhqOhp7H34kdfjFSUaxFoPnenk95B8t0QVsihRAKIkt2RSfeMhRyl2oS95xV0EEZ1UoNIpoenzLp1bs17oN424tUGotPwlRDMxworneVDLhMXNROj3YD8xFwTzP0xqmbiQhZgmwFDDZ7M+SUhhZwm7HgZe8aKmiHS84ckMydMWt6jP5/huM9bXMm9MYkVlmTaKfY/xzOE9b2CAhGlnqJPchwn/z/e/PFbNECUKQJTkFNMw0IakFbKmoKLhVXeZ1sHVa7kZP2PhSVXNVmSPDSC+8lBMITMAwSZ+OqQNwv0emFqAYbwEmYfJ2IgZYooOTxAGLYQAQrXhcj7bfQiLM2+JcoRaqK1oU9URcxPQIhFSCtGl2Z/FFJ7ZU8rA6OviSYEVpfro8og5z66O5U0Zyc7j0XZv31ahZogSBSBKhjREiOwTsXtKlER1mFaW7Kwqj2CkD/VbeHjwDzdHlXCTlWlYYbsY9GUieDjl3CNgoCkLWYlewgOEKR3eGrVFcRqiievPuH14c/OEoHMcG+LGU1eXa1DRd2AzNs8yBeDQfJXglXpQkl9UGK8Sj+jhaCvTmpksB6xYkROSE0zKo6FZl8wuW0mcziDgMd1rejgJ3HwVZiwjZNdl0q38D00Hw6DcfIXvYVVe0CK8NXlhxVwIYMgA1/eoOPV+jEjGqo+UkAHapciwXFfeqxwopiLIx9uXs0e61KqJBWBFbLtVlG37ZPDxCTFOX7ikXc0QJWqJoST3iiJS2g2Qqw85AZlZZJk72M9wJn4uQ4DSbP1FGDhpBgIIBwca4s5yE81WyDwYkKAJi7+HK340dWE4b1ZO9OBZR/EfIWhu66sJm+OGNk2ONhtxw4uDapFl+vuWahrxsJIBXGLmE1/4eoeaIUoUgCjJvVClUl32ga52McQ3pENEMxdS3+dNk6ZCepjyHBAasku3hzlXMfnfFqBoxPqdAw1uWjGrfzLMClxDc5sLokTvH34QIYM4Dinu+6AkoGI9gZII8YT3Fo0fVhUBFYGlRAGIkqGvb0ke9YNuDrSxBxSENT73g39+DwKIiZZTEzsQIoAQbtJC1sGxC4EEHewcRHAfDeigokeHoO8kXYksRCNa39BS3MnwfgukuGRG2ZCmyRFWno8SIAFFMZUoUQCiZJA0ZIC5DVlshQR1DjR0TVTfZWDBV/lowkK/h25HYFkdCZGJWO65CITDYaPw6lhadRdKDixKR2tpz586spGhalaS41oH+t1AQPMoUhBESGB9ffGravOhRAGIkmFb5FJzIGtpr24iuawshlV1NwSGqYOJKdCMdSCAYDkTBBT+O3vPyJg8nBf31XQ9fwwWCVqdD21KEBLqdf0fDqgMw3P1KfsjutoP+h5Ijj1JYbaUZ0wo+ccVFYWlxKfjeJlXO49DMkVRkl1TUA7RLVBCyQ3j1fnv2PUO2xYZaQqGoVm+EMNS1IaJiW0aH546A5ABrdaHuphm16hpWtLXJ51dmc5z7GE49eiwLvyDIr+Ciwp4XqW5jpUVjWWqjoRKFANRUkB/6qgsk5YeMYek83z7GnrIirBKM9BIpw0LF0xq62oEFpN3pkinM3YgsA7haITSoz0liDcKjbGRiQzYksf+t0eLep/SbNwpTIbYmCC6oWaIEgUgSnIK0+dphiI9A9BHwb9nt1lNR0JW46hM2oRMyuSMhDo+EMweQRpCIJ1McxZipWFoR7dfFC/hDlkbY0Kdw2LGOrIXn5X9n/v7kHcqwguCY0ILpdUMUaIARElOCYUjcU0LNw1+7UtzYUwmFIlwJWdkMpBOpXh0FUZZEbAK/QFYpqwUAgiv26RBpKTk6HWhwGvRoCfHu8a7N120ozyl/d8hLy1zkOiRuJohShSAKMkp3/j1X2KRsuoVrvYwXaXiMYvTQnonK1qI2k7kTNqAdDpt114yOYgggBBeUwrZSQpMu/BfSOeO9tjRmg4E9AQ3WeVa4R/VqSk2M+A1/7Mmnui5XD4OWii/x/O+a8IsKa9eSULRmJohShSAKMm3DKeRssq3i172S5oqr5WECJZh8o0DhkZ5TSYsraHx1T+xWICu2f4XAu//yPuXInsJVpx5FOqgbp14EwgDGMqRAYig1wfw2oDzZGhe3A+SDNW3KSe6EgUgSopSpeALQ/JrG5pXNwUZ4okFDCh2qSzCFJ+GGekILrpmZzQYVktb0woG00L4PukpTvEOx21DHkcxGSYzljhGEczC/2yzKryTfJjueXVA7MN5iR/fDGHpZSVKFIAoKTgodIy40eKW8tDyaJhsNKG5lBOBtMYBJMPAwXSyFEzsj44Eg6MH7xlil0c3+Ps6ZyPEODrDOfdK3orEIunhyDNxwaPI65V703tSScxiSUaeXun5vlNNMK94Op1OqJmhRAGIkoISLh3VSPTodkdJFamMcu5j2bBS0YgFIBkjY6+IqTUAqQm6Zq91qckBxjQx4CcNeogDSqbwCc3cQ1r4B/KaerSsDHS/6FoRkVh5zzUE8AjsQ6IVJIDiRVro+6O5wQXHws/vebxRzQwlCkCUFJTv/eHhxlC0Yrlkwii8yJU0F82lzewcxUwmAybFMF6T+zrQFyI6HVJGNkzK3s9kwPKVULsnRzFDOQeICNN9PhM+4S1se3M70MG2rxWYMjnPJa7PPGLTjga9QnPvRYs8kB6pWKGXj92jZoYSBSBKihKih3sGYxLJe0yuPDNAGQPRdJNXxyA8kdDgIALYsJBYpYApTfN9MUqLDsiElUNJk8K9OawM9EK7FdPilg6MIRWPCEMHlqLZh/RUQ+EeNSOUBIkqZaIkUCLR8j0p4CU9olLVEvDUMLHNHJ4OhcR633ld2p3wACfMAUlAJpXhHhDu2OWhvJbKMqkV4ptJpwD92SGdYHCt4Ttt4TWRU1RRSrWmebSyJjLQC50En4kezV3ShASci+Q+bNB9Dbh5FZVOZwawD5qDfQTYtrIbSSXCkfJdakYoUQxESdFSOnr8Y0SP7PAonsI+9ODVLLX6puposoI0pDMJ0Kjl48DiJdTMYAoh29FgejzNo7SMDPpsU6CHeQ0tc8CKlUioIUqy5Ordyh3jesFD1oTpRO7QJyQ/eIAplYE5kuDh+wgZ+HeT+3jC/1Gy48f3PP+EmhFKFIAoKVpu/sODnaCFWgvqHmfpSwOUl7TipUTTQ5SDQjKRYBiRAcu1YfLgJsQIHU1a6GRPJSGdSHK2Eo7qGOKb9iHDAEFEC3hNbqeI+R96Ud32dF3ryDZj+QtKiZBckv9ys8DDXxW3cN+PQICgJAsI5BhepzplMWZJoreTSEWnmhFKFIAoGZCUV4/7i7WUJgGAkU/3eE0lVskStPmgWSoJRpIBiMEwgbEQwlgHpQgklvMcAcQw0IRlMZAQ+kqcayA+01QRSpYUeM1Kh+f+D84wCk0YdMpgZV4/CwliJSSXxg+4dkp8wBF0sDzMx/MFFFExOAs8/PUF+PnM0soxd6qZoEQBiJIBS/X4E+8HKbEuZwlwmkMhUaGYTClHLw0Unea2Q50nDSKQaAxIiMVQUEdbYbwWgGg6MaGoJnxkEP4Dmy0QKMpRXxUyJ7OLTXkZzABsTSQP0JFisJHkJgz8mUtA4PkO8uJ/LoQxbr135f1qJihRAKJkwNJ8cH9cL625p9jQHZqj8Lr7cpKDgmFkOIhQxjTQkU7Y65qW4T4QATCmYe0L+DohxTfhIGI1XwBtHP2v8Qq8GtEGco6UZcYqpha6lBcy2CTEQmXHaNB7OVgipTm/IA/MYFBcSc3fVQFFJQpAlAxKfvnACqNm3Ik/KyrHI1CX+aN/GCjQBNssYKBMDyNomGbSyjznzCQpMZAEG6AILoQW6xNwFDoNWtoHrfr1ov0fQsKhUKcDBqQQnSiWqOSgHbnLi/jet8KXaUG6GPyNBe31i789+1M7dE6JEgUgSgYulXUT39bLRt8ZCBI0L93wWlZ42nSK/UyAkUaWgRFYSaviK0kDth3n/hCTMRQGJBnDMmERBjREC0qsIIXBoQjFjuChaVp/VciYXDwBCfKDDOVacqALzX6P5voiZAc8zRG2S4uooCzYR+nou0JVk99SM0CJAhAlg5abfvLH9Ki6SXewoRIvypRFslmI25MWe4AkuQmLm6so5oIg80jz37kTHf0hJltJp9OcsVDGVjRMRy+s1mFAznX7PQSPmghMzP857+uVujGV+0EcMxYZpmvKd2+FxGYflASDB+QG+yxQIlr8xptvvR00XTWQUqIARMnQ5Id3vfhiqLz2zzm73QVUU6S+16xVexLMDGMgmRQHE2Bsw8rHS/L8DyxjAiTFgCQNVu0+NGExPa3LDGQwijj3/pqWz7+S+xw6mr0Cs9KH79r8L9NcoEKpBB5mMMjT4JjdIJMjYx93nLDoqpfUyFeiAETJsEjN+JN+QkIlWwNXtr4oLILd60jE19XQ1BAQTCMOqQQ2jEpzdsF+YT8Tlj/EMEGjCd6xMJFEJ3o/U5MpW/kVCm/NxRzy709gAM5zGUB0Palp4ZbCgEaKvFbpfbkgo5NTk9s0RnkSpgwe1EMy8LsgcpdJmtt0Bew7/uX9K36iRrwSBSBKhk1uvu2RPZVjZ97I6EB3TlOW/bIerXiWbU+KqCwrE92M8BwQM8l9IJgDoqOJipd3T3HTFZq1CAMVE30gKcuJroetiNlcEV75V/VF1D8ZZGBUZciYGo4U6tAnX8NAG3QFsQQS8Mh9/g8fePCS+NGKp7VIxbN+xphtutK7P/utWz+vV4zdrUa8kmJE1cJSUrT8190vPX/TVfO/lejY91OmcUogh4omeqS5rKpuZXdTx6eY4tdMS5NFuQnLTEMqnoKMEeH5Hxiyi1FZGobvUsvvkUkyBtJPLQDRM+ArqTVAIMmPEpl0uqzT0A6gnqXU1Is6KtEM3JeQdEnhqyKDu2T7j5zASf0Z5mYgeGDCjV5a/YCZjJ1r5MIpvp+WKBk96VuzL/j4i2qkK1EAouSIiKFV/G953Qld/R37fkCN1HSufpyKHVzNZ8oq61aOHj/j+Z7Du3ZSI3mS5QNhhIMmuOnKYBsv2c4BBZ3nSd4DhIfykiQvqGhkcGWdZMDCWIl+5DqpmplkQy61Ouy0ZYBCPSwG8gCJmfvTWnTnz+5/5bkvXnUW/nkTez3k3YUiZdxdPnry9269f83daoQrGYgoE5aSAckvH1hh3vrAq3+tnfqej2iR8mf97+ulNX+KJeGp/bt3psPldXdYKorGCcHMQQSQfm6iwsgr00wA4ZFXSZ6bR9Gjjv4QphRNA/dJcECxwGWASn1QOp4UuQ2SCA3qY3n60HrAw2UkMmvB7+DGay5I01D5k1pJzZ9l3ODniFQ8t/iyT1zLvtO7BpSwqUSJAhAlg5Xv/+mp1+qmz7++omHGPzEl9b8QLlsVrZ70lZpJp31N7DOqfvK9QMKHmK4yPvfNuWMYjjAwiFu5H9jrw7ASCCl7HSOyTN6pMMHfM7FvOu3nLITtQwor+ID3yTCCwGDBhxQLStnXRrPQx5tcSJ3qKxJ4eC4pfOjrv/jTveLPXz365ldLaiZ/hYTKVoXKa393ysL3ffTXj2/8p4/d9OPX1YhWMhhRJiwlg5bv/eHBwzcuW/qXmklz7tMIrWxvbW3pam933u/u6t4dqaj7Q7z9wI3cl4HgYWSsRlIUGYdmlTJBcgJWrxCMyOIxR1hgEcuZIBuhJrFW1YPwhJBc1qmcbwwPvRg0RlGXXAS950kepLnBg0mkovaPDSec7jjEv/jRSztBr/zpr5946U4SLuv54rUXJrSSajWQlSgAUfLuSWdbS8KmDn4tSn9y50OPrf/7l/+NaM3cWY4RV9zXAbpVwsQO49XY3xluqkpxhoIWLF72BB3opkEo71SVr5iUZvfgIK6ilXfP2ZGQDB5DSDFvUgn3xLXJ7XezP0eB5nZ9iEN4wCMIH8MH551zycM3Ljs/660vffzKFjVqlQyHKBOWkiMqevX0N6H2lOVhTGo2+7nfwzSwfEmG+zh4H3T0e6C/A4sosn2waq9VG6uPm7Eu+/AJSzgRgaA+45o7jKnmallCCvTX8JmZCCmONdjl3wubpsC9JiLnd2jB1w4iPaMwmlmhu4YEHtnVFMNl1X+77iu3blQjUIkCECUjVm68+nwTame/GCpJG+gDMdMiMTBjFUxEHwgvqpjifUG4jwRVIi7QeUXeBITCyFJ40SxJVQb0PCfUTcDLyzryoAMpsA3YhBZwTSQbCD25Hggy+S5dlG33g4cgJXq48fwrr/sLf/ZKlCgAUTKSJR0edTgUxQzBXp6Jbum8lM1GMpxpIAsxhBMdK/RmsNVtH39PCyNTIdl9STxKVShoIilt7V0c5j5WJFvgqL9AohkAhsEAZ1muzHxPAsKlo+++4oZvbFIjT8mRFuUDUXLEJZOOl0UiKRMBxDBKmN6L8P5N1EC3SRSskiW4o8ZBg5IaTDgBk/byTuUlkQjmhjDk0Uv8IEI8q3kJMEjAax4xpdfNAYKM/7O5QEoLaKGrSf3SrW6N3uP4gUFODhTMw4Bcxi6iR7d9+7f3/DnI96FEiWIgSkYegBjJknCoTwe9C4x0kulKwn0gBu8Dgg2l4ryIomFgH5CYRSAMYpmz9F4IRbCcCTpNxJCV/QYk67Xih/5gGYr/swP/POXmOJLjmMTXz0TqhJsLPOzgrWhlw223fOk/dqpRp0QBiJLjg+aGynsOHa7p7OlMQV9fJk0YgOjYFdbE6rxsEOp93AdiZjKgab08pDeTDEEs3gsdrXFobwkjqJR4V/+u4hxpS22aBRI+ZkNz9ACx2UdWDSuRFBgqXXvrPS/co0ackqM2t9UjUHIk5dZ7no28tXVz4/o3lv1p1fr3nBRK9dWPCh9cHI7GdaL1QLp/DLy6qgpSvSWQ7NcgEmaAwthIVKuC+/48G+L9ERhd2sAIyJYQmnAICahgK9UTzHYdkHf5CeToy+FSigKflt7n+TJBNfUtoldaM/7XX7ruqiY16pQoAFFyXEgmkwlVjKrqmjBp0Yr29plbamuqetM7HzZLSzcuLasERitKYPuaMVBREoGyqjSUjgIIa0loqK2Cw+0zoSxMYVzNKPR15EaCgJ4Z5F0HDu/F0YDGW4Wokzcyyw5tDgYP0Eqq7/3x31c+/MVrL1KDTokCECXHh4TD4XRFRUVfbW1te0lJSaKsrKzfjFxyx56tK8+CXR0ltaMiUFUahWhIhwRNwqpnSmDF050rErHnTg5HovuvunLJ/Hvvf3GbRvSZlsWVFkEwiORkf5f5B82DGEEgQkkAPlDOPuTEQS96hA7PPut9v2DgEVcjTokCECXHjXzlIxemf/TXp7sQSJLJZJRSSvqrKnuad1726DP3/mVsIr67zqS0nICeYcq+/82NlZ3xWHqcrpMG08jUPfDgclMjZKamEc2KuJLcdqRQA6d3h5HkTgYMQgzfa4T6SrjzVHyXfQR4zyMVdb+/4fu/ff3GZeerAadEAYiS40ui0WgSAQTBA01amqaZU8788C8Tf7nnpwTSp0RCGleaGYNCrM/o1DVSg11sdQ0YbOiuWqaaXTBW86NDbjpilxGhhUjLsACHDxRoHhChOT5PA0AGIw0CP8NeCJW9/LMH1/5Che0qeTeEDKzTmxIlXrlx2dIB7f+L+18isVisvKenp7J1+ytn/e67n72LAUUprtoNg9cr4cSCoQxoOlvf6BFJ6Wt2FncuFuKFBkKGudlTEdCR9Q4Nep/6dwA5AsuTWIjswy4qGXD9/TWT57y/tz+zcrBX/ssHlqtBrGTQosJ4lRxV+eLV59PS0tI4+kRmnX35E9NOnvstsbpmzANCOm6aVcoKzTbcdGOV+qCEDgAAiBPhSrM+M9wl3YNLsTuBVgWuU56KWfeI7IMGARCFSOXY/xwKeChRogBEycijvYRQXdcN3L78P4/cBuFRT5KgMFwqnMeu2YoOkk2IxL0jybepdJ6BEhcv0NkFGDFs18wEf5Y9s589tO6/1WhS8m6K8oEoOerypWvea4KbEdh78Udu+NYzf/31PGKmx2Y5wZkCJURn9ERe3VuAQkQCCBEqmGQraJJXbw+Jh9Bh2Um43KUkQmKVLKFYbDLogFq4+eIP3/CtL374kj41mpQoBqLkH1peeOqZDZHqiV+hwMvyZqtMXrVX9n0UYiNQtJXKZSYD34o6QZ7dslgH398CRc48qJkFNfiMymqn3PTC089sUCNHiQIQJUpw/a2V3xUur/sfGgQN6Eh2TDmaRztTyNNssCDPGA4/yOCOTQP3s+thUatnfBZP4UFXdb9N0ZK/qhGjRAGIEiWS/OLRN79LwuUvBjILVKgcREhhPT0UR0eRfaKGRQKPZ0qMS7odjE6LlL/4y0ff+K4aKUoUgChR4hO06f/HD//wOaJFdlN/j2/HJ2B4tblNQWjOVf2RZB8DOQeRL9d3wcRlVAiSkuOc2vduUGr+6sHV//7Fj7y/V40UJceKqDwQJUOSgeaBFCMVpdrF7Xs3PaVh+jjx5ZDrUex5kUNPu3u6OSBFKPeiTV40z58072e88yxHSRLsyJiJW2ApAafJtp640V8/5fTy4X7WKg9EiWIgSo4r6Yubz8SSRlKsvj161khxRQtBVXml36mTuF3EAmmgayha7GGta6c0AJSCMtGNlHS/diQW+9cbT0MykyueV4kSBSBKlHgknjJSfYm0swqn4CpWmrH7qhMi6WWaV9cXjxFFJP2RYcAXj2+FcPBA85ULHtZusUQG+pNGJ1FDQokCECVKitXjhPYljHh/KiPhgw0iaOrB8h5ZmtirwkVr8aL83w7S0OCyIU472cLo4MEGGgQp8h4a9+249+OCB947buzPDCFE2ZqVKABRomQAPCDJQAQSaSMLRFDhUiMD+XwcrvmIDvzs1E9fBsYBOGOiUvfBnOcxGTwkwBMNwATvOZYwbCwlZURRECUKQJQoKZaAYGdCWo2/98Yz6APwgIhFRPrtZLv88bY0sC4WLUBHinnN+573HCQfL3E/ycCD8ntwwQPvtTeRthPpsSQxlrtXDESJAhAlSooEEKsFoShujiCSNvwgQq2opQEK9YHKYMRLUAZRY4toFosyUw544DHwHnsTGQYqxHaPUMvVo4ECECUKQJQoKWpgMp1MMI6XK1ARyprmCpZKIML9BwgixMc8iERlCjAKGrj5/+VzyOfwmZAcTnfuNE9z9iHsXHiEDLs3vEfTpM59a2xfsSlRogBEiZIiGYjGzVhEmLMcEMnYIELt//hKHiOzCEhAQqRQX/Ju3ID003tNFKvs2uY3AUgOeFDK75UQTbp3AsqJrkQBiBIlRTMQa9WNLETj+YSE+wNQwXZL5iwncIopZMupXixeHI1MdN8uxEoopGl2rabp8BYED7wnvDd+t/zeqXXvYlMAokQBiBIlRQ5MpkEtxQlgmbKEP0DjgNEbN7jiBRtG0BFNMzGumAvnchwJRlLccS3wSIMwe6U58zBsi5dmm60EiIhgAsukp0aFEgUgSpQUxUDANmGJjViKFSynMnolUPGmM27Jc76qRyZCZRA5ktUQ8wGJez5R+p2m45Rnm9vCHeYIHvY9ueBBvfdtsRAFIEoUgChRUpQa5j4Qx3xjMxHq/G3zDuhNGJDKSEwEM7rTcSeU9t3SunI4L3fjZ5LUzCSIeBWv2QEPIJLJTnKcy/eu8EPJMSiqI6GSY3Nlo5EUb6yEDfg0ysNarWgl7EdIRT9C7lNAECmPUigJ65byNhJsec+UbrhcqO8AlnAkICPoDIQ7+c10zAEPniSYtHI/CHHDdS2gIL6fEMPnENK1TjUqlCgGokRJAfn5fS+W1o6ZtJ3okS5Ng5gdheQJadU0K1pJsBFUyP0pw1XnmQRT2v2mV50PsIvggKxffrMVODWuGHg4AIPXaIGHMMtRiWkQcO+Vf7wrrGsdn/vsleWf++Knx6iRoUQBiBIlBcQ0zdIPfubLS2646aZqVKC6RlqFKUc41i1Fq0krdyzAaAKWPhGl02kmrjEgsVwQQ0wcLIZ/eMxWPFEwzcCjzwrXZdeE14bXKBzjxAnXpW60lWbdG94zu/feL3zyzEnpunMgUzI58ov7X9LV6FCiAESJkjzS398/KxMeHUrWLIDPfOGGSXoIenWdHGIKNkY8vgE31FdkbFtlQEwwTEuVo+mIZpJO5d7CRGJwLQk979qJgmaql4MHXksfuya8NieazHcPbqAAiem6digc0rtu/NSCSfG690KifAakwg2hVCo1Vo0OJceSKB+IkmNO2tvbLzW1Uo0J9NWeD5/79/j0UM9W+NWf39yfNsw0U79lDB4iWB6Kl/xgmwmam5RnIhOhUBbVgK3imSLvsxR8qMTKKSde1V+0R4SAB0Q8/UaowA7bbJXExoEmj7TqT1kgIhIDiWOqAifbnP2aYmSkP6wx1vH/zZsU0gn015wNfZVnAD6HZLg23N3dfQ476N/UCFGiGIgSJQHy/dsfWdLR0fGpqqoqwK2iugHiYy8FWjkDbvzkvElf/OT8asZGuoVvxFLEcuSStZJHAoK+hkTatBzvjA1gdBYRDEOqsDuQniHZ+9rHsn0XFnj0cFc/nhuvAVNT5GvTtCyfDrKOdg1I2KR07K/+/Aakqs+ARP17obJ6NH8OlZVV0NTU9M0f3/3MJDVKlBwrolraKhmUmKZZkslkatLpdC2aViKRyKGSkpL9N117Qc9gj/mjvz49bc+ePX9lY3JRNBoFXdeBnQOSySQkOhrh99/+NBAzmbZbc4RM/pMS/lN0IaRWGXXTLqeOyj0S0qA0YivtcAUl4XKCewz3Wow77lM9PA8lnqI8VJfY5VU0AId9OMwD2Q/7mP1exrKyMRDRy2Of+dFd5aWVdSA/h0QigX//ffr06f/CnvOgeqP/7N4XKtlxJrHvbBz7zg6Hw+H2UCjUyVhOQo1qJcqEpeSoSDwen9nX1zcXAYRtdUwRtVVUVLzO3loxmOP98M4nTtu7d++vmIJdVF5ezhUnmm4MwwB2bKZgp8Fl//y1rU/8349P1Uwe4cvRguEHxRQJk/KSi+x1i33o1III3C9lMBaQJFAaZqqa9hGNsr0jCCIaDA+QaLygI4KHYVCIpyk3o3HfDFgOc5DyOawS7fxSRa0r/BHG/0ytDD7+9Z/3llc3lONzYEreeQ5M0aN/6MONjY2pW+959mtf/ehFhwZ6pb29vXPZ9zZffGcIIOx7e5Oda5Ma1UoUA1FyRKWjo+P9hw8f/iRbxZ7JWMgUVG44hphCwlVyz5gxY/7KVshfYSvkWDHH+8X9L4VbW1uvaWtr+yZbaZ+KSpMxGa44Ua2ycwBbLaPiBKb44M2n71y7+tE7FhKaBpM6jQKtjhqcfTAVLRoHUmrvQ53iuNEQYZsGoWjZfqqXTQKiDwFELACiqVi/mekvS6bRUS7ORaRoK+LUecSrIy4jIaJYIjePaRFY9oX/v3HirDOnMqUOZWVlNni6z4EBN8RiMQSVFxsaGv7r+5++6oUimUf57t27f9Lc3PxxBkSV8nHZsfayZ/7a2LFjbx89evTTAKpsihLFQJQMozDAmMYYwrfZCvZqptwrhXJDAEElhADClFslA5fP2uaQLxUCDmQwTKn9CwOHjzEFVobHLC0t5eCBq23uU8DqtJrlqsPf33PRdQvbD+1bse21F87TwK5mSzj9AJMQCyuo/YsddkVdoIFExmIHo7REnw50h6mXzQRND25jWwg8qAE0HWsCI9kdS5onZ0w7q1xzzVVe05UAD4tyeIsHh+G91/7r6xNOWjBfgAeyMPEcbEVvH4cgoL53//79c7/48zt+V19ff+c3P/GBbfmulrGWW/C7wWPjM/Z9d1MYOE1h38VFDMwfnDJlyg/Y97FHjXolioEoGbJ0d3cvYcrld0xxnYrKRzAEWbkhgAiWwICkZc6cOZd8+/rL3gxYCVewFfSp7e3t17KfyxjrmCKOiQpTHBft/gJA0P6Pq2/0AeDqm4EY3Hfrv69q3//2ORZQSO3KOVAgCTE5GzHtF02JlThsRIdY1ajyd1JQMgf0aPEggh82UqBD4u2+3r7JSQPKxTEF2/CZragFG9QDHNZP/EWHMy685uWFH/zXxajgBQtDJS8/BzRj4XMWzwI3ZCRMtlRWVt7N2MNj7Fm+86Vr3uvpsvWfdzw+d9OmTU+z9xpkZiPYo/x88Xjsta2MRf5rVVXVajX6lSgGomTQwlak1zY1Nd3KFDtX9IIhyMpNmFdQUMklk8kG9JGwP9/ErHL2foQBy0kMiOa/9dZbF7B9FzEFOT4IOGTwEIoTXxPCzVLsfMtu+uU5f/rOJzake1tOF8VKnEZQ6BahvOwiQggHEhCmLEJkNlKe6emfUx5N7yRhcwaG+VqVGmlu4ACw/B3p/j29idTJGcP2Y4iKweAxXVHimKqI/R51mYcNIJNOXrB8wRU3LEXgEMwDn68AaPEcxP6o+PEZ4fu4H3ves9mz/a+Ojo7r2Wuvfv1//vYUA5TN7Nk2MjDpx++CfaYBn7fM8gSA4HeGf4uNLQROZYzlrvHjx3+VsZt71SxQohiIkgEJU9JlBw4c+DxTSl9jiqhaVjwCPIRJRTJhcXbQ1dUFbKX7JTa2VnZ2dt7EwGMmA40Z6BWurq7W8wEHHlMcFxzGQJ1zCD8AMp2+ni74v29+fKuZ6DhVMBBwmIaolcVd7fx3tDAJBkLtKC1qp4WE0TdSUh7XI2Wl3C/inxccWAxGPOKQSsRSqYwZoY55ygUOuwwJFexC9ILygIbzO4HaSSevWvblX5/DFD7I/h8ZSMVzsL8XvqHSR+YgnglGqiEDxI09/xT7XCv7ztbV1NT8kP1+LnteP2fPHkaNGuUxYYlnK7MbfL54HMZIuhir+fGECRP+m11LTM0KJQpAlBQUDPHct2/fLUyJXM+UUCiIefiVvDCBoGJHAHn66acbVyxfUXbZ5ZdVjxkzJswUEUElKUwowkQjFKXMOoivfasAEaE0UVkKEOnt7oA/fu2jhzWjb6wotsjVNQcQ0wMkpo0wFpDYpi0BOtzXAghqreFoRT1Bk5bjS7YKImaSsZYEY1dWy1nLBqWBbKqyshTt34mIuLKwIhtEotXjX/vEzX9egEpd+CYEeAgg9YsMqEGKX5j4EAD27NmTfuD+BzrPW3pe/yWXXDIVAQTPIwcpyKzOfywbSDLs+7pz8uTJ38ZQbTU7lCgTlpJcrKOkvb398paWlq+wPxeI1apQ9jJ4CHOKEKHwhGnFyBhTEUheXv0yTJw4ET5w6Qf451FxCebhByOSo++3eB33DVKi133j16G7/utzoJtxcDLF0atu8QBRj5crdB72C3ZxX2oXyCKuDyWRSNWn013snitAj5ZZZrlUDJLxPmxg1QCOuco6j89UxW1Vwr/BUQSselduVxD7fKGqxuu+88cFfp+HAI98z0Js4pn7mSAeB8HglZdfCbPvoIF9F4EMTz6HH8DFd8m2EAORT+3cufO0hoaGn9TW1j6mckaUKABR4hEsk8GA4wa26vwgU2YVsplJVmy5lJvfNj9t2jQOPvv374fOrk44c+GZwFiIB2hk5lGQKucAEQ4kE06ou+yfv7L1idt+dKpGU24BdyKt4ImDKUSE/vI/bSChws+AuRiMYSSTMYiI3h3Jfv6aphFXiVv44DNVycBhnxNsEHEuggESKYXrv/2bmvJRVQ4bk5lHPjD1Pw9hhvKDye53diMD4d/B1KlTPQDiP7Z8LPm7lL9PNi4WHDp06HYGSA8zIPlDVVXVKjVrlCgAUcBxbmtr68disdg1DChGI+sIioiSV6i5FJqscKZOmwoVoyqgpbkFOto74M033+SggitjofgHbG+VFB2Cmgwk0+a+99Sly1peW/HA/y0gkLEUNhXdC63uIZIalxztFpiATVhsKxc37SYTMYlhceVPxRGIMFNJgALE698gdn6J+zeCRwSW/cfNu2rGTj0RmYdstir0jIOeh/wcBStD09OmTZv4z4YxDTBt+rS8AOL/DsX7AQ77CnbM6xobGz/Arv2++vr6uxmQrFSzSAGIkn8wwYq3TU1NX2DAcSVTDuNk27gwL/lZRyGziqxscGV94okncgDB9zZv2gznnXceN9cIM4uc2zAQEJF9A7I569TzP7ygp6N19Ybljy4hNGPTAcsHwW1WFgbYVisLDajNSkDkjVDHbSK7QNwfxOYXnnBcoXCpCy4W7EgfN8EkYbj0U1/bOmHWwlP94FEs85BFvnfhH0L/xb69+2D7ju18H/wO8LvwA0gx5sIgJoLXm0gkRrPtMwxIrmD38cj48eN/xc6xTc0qBSBKjnNhK8gTmpubP9XR0XEDUwZ1omxIMRFRxSh2WdHMnz8f1r66liu3lpYWeG3da9DQ0OA512AUpwwiCESyOWvhVf+2JNbdsWLnG6vOk3FAKHUq1961czeo8ye3c7nNY4MAxAYI6WVPSK58F/LvJk8U/Mzr0+a9d74MHkFBCQMROT8EgwtwW//6eoj1xfhx8TuQGU4x55F9If7FgfCF4THZucYxIPnXbdu2XT169Og/jBkz5nbMQ1GzTAGIkuNMurq6zms62PSx/nj/RxnTGIURUUKRy4zDv0odiDnFn5+AzvPJUybDnt17uKLbsGEDnLXoLI9vZbDKU1ybUGoyiJx//bfO6+740srWvW+da5mxqB2V5bIGaudkuBYgh0cADUIA+U9POG7Wpzz9ywnnHiFYcOGyl2ed86HF/lyPwd5/EPNA8EC/x9tvvc33wWeP30HQdzuQ79T/3YrvVwREsPPW9fX1fbOtre0/ykrL7hk/Yfzd1dXVK9SsO/5FlXM/zoWt/K9atWrV6u3btz/L1OUNbGKPwvLgInRUduLKCm0wpiWhaIRyqampgfnz5jvv7927F9atW8fDfVHZ4aoZFSAqwsH6RPB8gvHgfeD94H1d8YWfnFtRN3mNQw9ABkTbCQ5yi1zRmMptK+tZgYvGVf5mVh5mIo6vOeG7JugwY+6SFfMv+/TioETBwTKPIPaBz3XNmjXQ2Wm1T8dnj9+B+G6HwvZkBuJ/1jiWcEzh2MIxhmMNxxyOPTUDFQNRMoKEKWONsY3pjY2NH21tbf08m9Sja2trNdlMJZRXodDOwSh04XDHc+D5Zp82G1599VU4cOAANwmtW7sO5s6dm5UwN1gWwldBNoh4IrMYKH346/+z6M/f+adNZrxzTpZtSeoDJUGM7SWnOZpM+ZtPWbWvgNAgwsLe1WHM1JNXnn/9N8+T61vJWfyDNVsFsY+dO3fC1i1b+T7IPPDZy+crNuIt3wKBUhrISAQbwWth322EXc9ixoYWvfnmmx319fW/njp16j1sLO5mnzPVLFUAouQYk+7u7oZt27Yta25uvoQpjEvGjh0bmTJlSlbehRxVNVzAEWTGEitVdh1ckR08eBA0XeM/169fz18fqi8kCESEghXb9d+/fc5t37huv27EJnksUIR4wMP6HHjCbX1oIJ3Ta8tyYYV6dq+om7Tm8s//9Fw/0xsq87AXCh72gRFXa9eu5UmceFx85viMh8PPEgQkQQ522axlA4nGmEkdu7YfsEXEdxjYPTVmzJinZ82a9QB7vUXNWgUgSt5ltsFW9u9Zt27dVxLxxJLxE8ZPwlBZ2bchO8blqKrhBA6/chEKBa8DleaCBQtgw5sb4PDhw3w/dKafccYZWaHCQQmKAwGRoBwR3D7+jV+W3vXDf0+HaDIMPiIiK/y8pyV5/8yiNiRatfXDX//NoqDM+6E8ezmBUjZdbd++HbZs3sL3YUqaP3PhaxpMiPBQgESAiQQkGH0XZtd6BWPFVzz26GPfKCktWX3mmWf+ZNKkSRuI7DRSMqJEv/nmm9VTGHlso+6tt9666tlnnr2trb3t62wSzps6dWoVJuoJO7tcfiTIST4cq9FilR0qkv5YP+zatYu/3tPTA6UlpXDijBNzAttgFJpfsTmDPFpRNunEWfu3rl1RrYERCAEkEAzkrRB4uAwko5W2XX/zH+sra+ojokTJcClyuS6YqH+Fz/Pxxx/Hku38+EvPX8oBOleo8HBKLiCRTZkCUHDD59EwpqGKgcppjDF/Yt3atVealPazMYsdLfvV7FYMRMkREqYgznj77bevO9R06EMTJ02ccPrc0zU56U9M0nxmqiMNGn4zlmAhi85eBBs3bYS9jXv5PmjGCmIh/mS2oZiz8NiChYydMW/qxf/0+U3P3vmLOS6IFMMqilbt/H+DROGjX/1pprJ2bLnfbDUc4OGvCYbsA6Outr1tpWGg72Px4sXDEuk2GCDx+0gQ7GQfCQIfbghsVVVVZez6F7S0tNzJGOrBcePHPXjyySffxRZD69VsVwxEyTBILBYbs2nTpmtXrFjx++bDzV8dO3bsOdOnT6+qq6sjoo6Sv7qt30k+FKU8HCwEN6HEGHPiVQ27u7sgWhKFk046KcuhPxQlG5TDILbyukljomGydt/2zROtLPHhfR4GROCqf79517gT506Wneb4nQyHCSmIfaDP49FHH+VBCniey6+4HGbNmuVhH0cDQPIxEpmVyCYu4TOprKwk48aNq2Lvn8WA8ONsoXEluz+DAcwh9r6qAqwYiJKByr59+y7cvn37NZ2dnR+sZTJ79mxNNkcFsY3B5nAcDRaCymzevHncVv/qmldBD4V5ocX57LVZJ5+cZWITK9mhnB9FzhFB5Xva+z62sL+nc/WG5Y8tIU4r26E8J7vMOptKyHDGzzpzjlDeggEMh79JXL8o4S7YB2b4b9tmsQ9MGsRnLJ/7aIJHPpOin5GIHiQyK0HAra6uLk+lUmcyVnLGE0880V5TU/MwW2TcN3ny5OeUVlAAoiSPMOVQ1dTUdNXWrVs/ySb/mYxllKBJIgg0ZGXrr6T6bgFHkBKXczQwefGSiy/hpTaaDjZBb18vPPvsczBl6tTA3IihhhSLZ4Pnl9nQgg9+dkl7c9Py/W+vX0qcVlQDPZfr9zVBg8WXX7f2hAUXL/SDx3AocBkA5e6B7e3t8MKLL3LlO378eLj4kov5Mx7OaK8jZd4SICJYCN6XAEcbTDR2T/XsPv9lx44dn2BzYt2pp576J/baQ+yz3UpbKBOWEks56PF4fFZjY+PXt2zZ8hs2ea5jwDGNkY6QMIMIh7g/c1yYBORs4WMBPPwrUFkRRqLs2iNheJutmlEhNh8+zJUfhpz6HerDASK5zFoTZy+aunP9qjWp/u5J3qjdYs7nZrBjJsiMeeesWPihf/O0ox1OBZ6rodaLL7wIr61bx5/plR+8EpiCBb/TPldfkXcTSHKZt+RwYNlvMmrUKJwLU7q6ui7ftWvXdQxoJrJ73M/26VQRXApA/iElmUxO7uzsvHjPnj0/Z6wDv4TzGX2vYpNFQ9AQDljZtxGULX4kwnGPBJDIUl9fz1abadi1ayf2DeFRRKeffrpjry/UYGooikvORp8x/5yxG1Y9vQ0yyYaAT+VkHLLUTZy56gP/9sNzMRtbgAd+X0NN3POzD5HzIZpGYZn8++6/nzMRZB4XXHABDGey4tECkoD+Ix5fiewvwbwSdn9VbO6cfejQoU90dHSczz6XZPv0KlaiAOS4F6YA6tkq6n2HDx/+1L59+37R29v7z2xinMAmflQOv5WBw+/rOBbZRrEMQJZJkyZBW2sbMEXAfrZCOVN+ct+K4XCoBzEhD4hoIf2k+eeVv7niqWZC01UDPXZpzfi11379v8+uGFUZGDI71JW/HzwQLNBxjuzjgQcegHfeeYdHsl199TJuuhruZMV3i5UUAhLb7BVlz2Y6W4Rd09zcfDV7NmOwDTN7r4N9ToUDHyVRPpCjIGyQv6+9vf0KBhgL2HaWbJISW5BDPKg/w0gTObkQFZtchmMZU3yoEDG897lnn+PRQzNnzswyZQ3FoS4/t8BEw/rx5Z/4zm8zd3z/M90hmqgq1hcSrqjd8LFv/24hNoUaSEfBgUpQxjn2Vtm4YSOcesrJcO2114DcOXK4Ir7ezcWGaJAlxooYD8LxLgcS4DNnPyex+fU1jERjz+JVtr1WW1v7aE1NzfNK+ygGMiKFKcZTGNO4fseOHbc1NTV9tqenZzEb6BOFQzmXecrfh+NYN1ENxpQl7gnvdfr06dB8uJknwSWYcpw9e3Zgna6hAmg+f4gWLi05Yfa8rk0vv1ChQaYwMwiP2nX9LX8+paKyBoY7UVBmHwI8hNMcTVdYFv++++6DsWPGwXWfuI5nnQvT1XDlmxyrJq5cOU1ykAGbdxPZIm1ha2vrNexZfZy9VsrGWRfbWpVWUgAyYgT7i7NBHUYnORvoTAeksMF2uahWK1Nzf9Lf8QIaxShvVHpoukIQwZU1KkR0qh+JsGT58/7rCJVWVkyYesKut9avHp0r0RDF0ErbGGMZVVU3rkQGj+H0OcgKUc75QAB56qmnOQv52Mc+CuPGjRsxfo/h+M7k5yIXk5Sjt/B19pnmysrKN9m2jm1r2UJtH5tnPUorKQAZObZBNmDZxN7BBvMbTz311OQnn3xyEVOSUcM0uMkDgSNXwlu+SXQ8KAL//aLyw8KPhw4fhk2bNsLJs2ZxxXgkij4GPWvxs6SqYXR1VdWGdza/NpYEOM0xy/zam37UXjfppIYjrbhFyK4wWyF4bNmyhefRXH31NTBhwgQYrt4ix6r4qysLwBDRaKKBFrKz7u5uYGwfXnvtNVi9ajVl8+yRM88889ba2to3FHgoABmR8vbbby/4wfe//9cXX1p+TXt7e3RvYyPs2LkDdu3cBfFEnE9+nPhyn3C5L0aQkhvpIBJkmsCfaM5DJrJj+w44cGA/nHzKKTkz1I9EVJY45qgxU8ea8d7VTY3bJgPFREMrT8QkIbjs01/dOvGURScGgcdwKe58pqtX16yBSy+7lDG0YOZxPIKH7AMSoIGAiowMNwzCwHYBTz35FLy69lU+t9rb26KbN29ezBjt0hkzZ26or69vUtpIAciIETbIy9mAvv6nP/vZb/c17j3ZKcfH/sukMtDZ0cGBZPu2bWwSxKGktMTpES43VxI//av34w1ExMZBZNpU3jMEFSKaaIJYyHA8h1wAgj8bTjx9ctve7cu7Wpqm8pwPBh7nffCTa2ctuXKuvOof7jIh/qgrYbrCqCss1Y4VdtG8dzyDh7yQEs9BDiAQzwMd5qtXr4ZHH3kUXn99PbS1t0EmneF4T+251t7WPp6ByyXsO+tli5PtbDGSVtpJAcgxLYxK1/73r3790zvuvPObsb7e0TiQqd1HlRf7Fj8ZUPT29jGWso1XqcVJgqGYIvrEz0aCWMnxykTGjhsLK5Yv5wCCEUZHyj8UxPDEa9PnLZ26Y/3KNfFY76TTzr5wxaJln1ssh1kPd6hsUKFE4fdgTJb7hiZPmQL+8vDHG3iIRZS/ZIt4Fq2trWiigkcYcOBCo7enBwz0e4hS+oTY7VysaK7+WKzq1VfXXNLW2j7utDmnvcKeW1xpKQUgx6Q0NzdPuuWWW/74zNPPfMTIZHRN00E0J+JtuT19t0VjbconwfZt22FP4x4I6SEYVTnKw0BysY+RrDSCwENsIqP75ZdfgenTpx3R5MlcPifelOnsC8c2zJjfN//Cq08qKS3zRMzJ0U7DqTz9pqumpiamSDNw4oknOs/lSLCfY8VcJZdrkf0/WDTy9ddfh4cfehheeeUVXozT6iBpl0kh1Jln/G97huHPRCKlv7V1y7zdu/ecMnfe3NXsOSqfiAKQY0saGxtn3Xzz9/+8bu3aC8EeuLypKYag2gzE+kkdc5ZQVpSvlkzoaGuHrW+9BS0tzVAzejRfZeKEkidakDP6eGEisjKsrq7mLA2BddLkSTkBZDjv39+IiiZ79De37iiZPHEilJZXZFU6Hq7zy+xDBg/M0McV98yZM0Auk3Ik8k2OBdYh7l9+BrhhiDcCx/PPPQeHDzeDSU0bHNz/wa4uYP203yG8vhyY6GNkf+/bv2/m22+9fcacOXPWsPHVprSWApBjQt7Z9c7sb3/723/dsnnLQm6qolZvbdO0FL5ONO+KV0x6CUmIbbvNGBne9hX7W+OkQtOF7BfJBR7HU8y/uBdskIXZ6h0d7VBbW5sFIMORHyJ/Xigx3PD3zRteh//80c9h8eJzoIF9D3Jm9HDVmPI7iuVs8wP7DwB2mJTLpIykLPNiwVru7S6bq/AZLF++HB55+BFecTjD9uP3TKX5w8eL3ZbeNl2JuYTAkc5IeT3s/UOHDk/ZuHHDktPfc/rLbHyptroKQN5d2b179ykIHju37zjdSWIm1iB2WAMqG59tlg9014rlzgnblBtjk2f7jh1wuOkQV56oPIL8IsdDpFa+PBGsm4VgKkxbucJ6h3rvQXkFDz32FDz55JOwaNEiOOGEEzxVAoarzpU/01yABy4iMFQX/WJ+8BhO09m7bbKSfR2ykxxZx/333w/LX1oO3d09DoGnFL9z6ratp27HSI4t1FqgIaPHY1PIWquhc33spo0bF8+dN29lTU2NSjBUAPLuyL59+2Z8h4HH9m3b5zoAQN2+2B7Tk0YsEHFWTu5ugboAGYxhQtPhQ7wJEwqyEX+Yb9BK/ngxaYkN73vz5s1QgWakaCSnL2SobWJlZdbZ2Qm33XYbB6+GhgZYuHBhVuLncHQXDMr3aG5u5uwLzXi52tKO1O85KKcDWYe4997eXlixYgXcd++9vNYXNcysRQb1oIJUfMZmI8g8Muy41A7JsszEXulo7xjLxtRZZyw446WqqqoOpc0UgBxVaW1tHf+9737vzk2bNy9yAkDAdd65/1mMA/9puIdGPPjirJqy8cNxCvazyfXW1rdg/4H9fEWOCiUXkIxUk1ZQ4UUZRFChbtq8CdiKMavk+1DDe+WQUWFG2bp1K/zpT3/iSg3fW7JkCTclBZWcHyx4yMwDz4krb+zxgWCBrFMGj+FqTHUssA5/S17BOhibh3vuuQdeeukliMf6wbFWEbEwk6YO8c8X22WOgGybIP2LM8FihM2rrbVtPFsgvIcxzGcx1FdpNQUgR0XYgK+45ZZb/u/VNWsuJkFMQiYhtssD27jyInG65vpBxH7EhxzScYiI4gKM8mqBLVu3cIXiZyNBLGSkg4i84YofV+TIROrq6rIc74MFkaBy6bg99thj3P6OigiVHNboEtWC/cmNQwEP0d9DRBrhe7hIkBtTjfT6VvlMVsLXsWrVKvjb3ffA3r17PXNHnlsUvGCQdR72z3Kam9klMUn24gz/O3To0JSmQ03Tzj333MdVnogCkKMit99++zcfeOCBz3i0vR9IPCDg+j54f/BA5yvxrapIYGHYRJytjrdsge6eHm4jFxVm/QrYv5ofySAinNV4r6hY337rbc5IgiKyBgMics0pBIvDhw9z89W+ffv4+/g6+l/QF+IveDmQZ5wPPLAUB54HzWUiWVHu/zKSwcOf2yFMVggcGGX24IMPwhOPP8GrMwBx/RlBQOI3/7rvU6cWVvC4ElYCN9FQfHZv496ZNdU1baeddtpapd0GJqqc+wBl48aNi++/7/7PWmOPegYzdSaNhAEiM9beF1dHaTCYAtK5T4RKEwCoCEEkjv3WtnH5FB6F1StXcVv55Zdfzkugi8ghEfqLiieIlYwkEJHBUc7ON080YfOmTXDanDnBqyL7c8Xcs1DoslkFI37QnCIL1qFCQEHFjopKgEixpeb95hu5syCG6+LvyCr9ZquRCh65MsplfwfWrnr80cd4sAg4TnLqziF5USbPCf+52Hww0GFOTS/i2HOHyHNShP7a081O6tX/8pe/fPk9p7/n5VNOOWW90nKKgRwR6e3trf7ed793R+Pexll+opCzr2aAWUsMdB6dpZHAfeWPeUxj4lAEHYHtsG37dq5oUPnkW8WPNJNWUPl3wUZ4Q6FolDu4q6qqct5rIcXr7zOOig2duHfffTesW7fOsy8qeWyChW1jBQsppulVkCKVmQceF30Aoiy7XN13KH6WY8VkFXTPwmSFUVZYkkSqt+uk2maxb5JjjlDLbIXnybmvPYdkMxjxzUl2XZW739k9+70XvPdB9vwTStspABl2efjhhz/N6PYNQeO7EGiADxC4T4T/rtkhvdQT304CDudxmNjLqng8wVfMODlF7ah84DHSQES+btnfgQoWlS3euwCRIPDJ9Vqubn/IPO644w7uzJbFsHMQzj77bKcIZqForFy2f5HngGCF50WzVVBfkZEMHoLV+e+5ra0NHn/scR4eHeuLBU8cAh5KT7IngkP10zZ4EPC6D7P8kNRnugowcR0+3DypYUzDwdmzZ69T2k4ByLAKL1Pyg1tuY6un6kCw8INEkZON56FzGz+BQsfN4QnkCmoPU3xYDh0dzGgGyaeIRyKIBDnNUdGi0sWIKZmJ5AMP+Xfh+5BXx2iPf/rppwPrkGFoL+aDYBOsXJWC863AZds/+jzwteMNPPzlSOQoKwzLxWZYr655le1jZAOGpOyzx7/vRQk8CswU+3unkMuvKB97957dp15wwQUPqHInCkCGVdiq9GurVq66XC7Y5o5U32tBaEJ8A1ZkP4sQX4zOAjfqyhrT8nFpjoQRl5pjyW8EEkw+w3DXIAU8Ek1auUBEdDXE+8UwZ8EK/AmX/t/9pitRNgMjgG6//XZeIjxIcF88B+aEoLL3JxTm610hK1JchePnhMN8pIOHeL5+H494rnjP2Czsvnvvg13v7HQHbQ6W7ip06ac0D/B0lsPccEHBTi5054gLGCJUPnhh5wWmvt6+6mg0kjjzzDNfUFpPAciwyMGDB6f9/ne//z5bgTZ4hyDNRgef/ZXapilCcq2PmMKhlkmLF18kJM9aylZUxHbgSwUZxWTp7enlvgFRDj3Xanwkg4g/dBcVL0ZloTkLlRWamIQiz6pv5TMpyaUz0CaPppVckTwoGDWEvhAsMeL3UfhX36IgoFyaAzPM8VpFnsdIL8sexLbkEF0002E49MMPPcSeXZs3JdwPFoQETyJwvSO4dyqd4vke3jlhJY0Q8NuwSA5QkuaNXblOfLqnt3f02Wef/RRbmHQp7acAZMiycuXKKx586IEbsgcmyaXjpT+J1y5Lslc+coiv18Tk8wJ6hnkQM7GWcjiBsUQ8Tt7JUybnza8YiSCCImejiw3Nd8jCsM/KKMZKcF8RnSYX7BNF+2RFh/klv//977N8H35BwEHH97x58zgAWJnR+Rsf8YZQzS382Ag+cob5SHaYF/LxYF7LQw8+CM8/9zz7uz9bodvx7vKolh0X1FMo0WqBwDPMTZo94Wy2TjzAQLLmmvdcxDNPxZ+dHR0Ns04++Y2ZM2duVNpPAciQhCmB8ttvv/1bjXv2zpKVuHeoB9tVqZSbHgwi3kFOxUpIlKgW08iprUWCrMHOCoxIV2MYGV5PCFe8qLQwLDQfCxmp5izBRIQ5CU13YXava15Zw1f84r79da5k8ECT0m9/+1teLrwYQRMXKv3TTjstZ1a56JiHodZo+y8rL+Nte3PVthqJ4BGU3yHMdFiG/q93/RXWrl1n+ymCMmZpzvlDZcUuwAOTBE0ayND5WKDUDVEMnKHUM0fcAHvimVkCtjC5kH3PKrkwj6g8kAKCzvOXV7/8Ac94FGYkKsivu7KhsqKjXrix3Bok51SxFJ3JJkua54lYEVVuYUbIXps5U4xQGrji2rx5C3R3dcPV11wNs2bNcia9KM6ICkys6EcSiPDVjxQBJQMJ3hOyEeyP/fjjj8PECRNh2vRpvJuf+Aw+A2QK6Bj/4x//CM8//3zR58fP/v3vf+dA8NGPftTxjQg2goqucU8jNLc0830QaIS5SgDHYJMRjyXw8Ps7BCCjKfH+++6D/fv3B5psA+Nss75je18qwD9t+1qylmTO3AJnzlEP93f3Jz4YI1kRX+KqcM7j3J86deo2pQUVgAxaDuw/cAJbXUXcYeiWX3fXUWI1RV0QEe1oA22x2UYse2Y6SU/YRMhS8OEsuCHeqlvSKsql7uJd3Pbv3wd/+r/b4dLLL4PFixc7Zh3ZL1BMTsOxCCIiGksAiAARVNB4r5hkibWVVq9eBfUNY2D06BoOLqjIOzu74LFHH4VV7L2BCq600eSFpkIM7R03dizE2GvIZtDEhWB11lln8bIkAjhEI6hc0VsjyWTlT4gUZquXX36ZZ5XzsiyeJZVPSwewA2+Uld3eACOteEl26u3/Ib0CUgl3Gsgn/J4R6TXqwor7P86/VIQB4IkKQBSADGXCkOdfeP5qmewG11WQ/qde265Mw4mHRAfQaWcOWb8giCBjD3MFmaVCQe67Jj7rwhwB2UXY1d0Nf//bvdDa0gqXX3G5xy/gXxHLCnqkMRGZheCG9/WhD30Idu3cCStWruRmPVR2qNTRJ3Gw6eCgz49mG6yUPJaBByq6vr4YO18Ili5dysN90ccht8CVc0dG0nP2g4ccXSaXYMf6YS+9+CJ/z88wvKM9mC2DMEHh2GUDH3vj8B4gHlOXd3EkL5yIb3lm+RaJ3cTNngnUZTf+Gege3/r/hRdeWLZkyZInCCFUaUMFIAMWtsoKvbV16wL/VHAHrTsInddIrmnj9YlQH4uQhz91gMhuisMmkx7WeZSWf9o5k8cX7UWl/8Wx05kUPPvsM9De0Q5XX301Xx0LEBEKYqiVZt9NJiKzEJmJoPI+dfZsmDZ9Os8ZWc4YyYY3N/AV81AFQehtBiJ4vvPPP59nqgvGIQNHsZnrx7LJKlcxRIxMwwi29etf9xiJcnALsdIBpziVmBv2y5zhZNJgYEM2ex54eYWfiYOHfUtFhez54KmJEmzKgiwMw+91PuoAVWRRAcighA1kraent8Ydsf6+At41C/jINfF6P6z9qD3oCfhARNQ5caovOu8aqOBTTLmHcbWtSaDgrtys3tC+YznX7XVWvs4mOtr+ly1bxntty5FKfr/ISGMjsoKWmQgqcjQhLViwgPsktm/fDps2beL1rbC45c5duwZ0PvRpzJ07l5upzjjjDO5fEmaqoN7pfn/NSAEOgOz8DjnSCk14Dz7wILzzTu7nRwIMSyJSypkb1JoT4jxOpJXTopb62Lc9y6g0PoUfkMhnlBZlUp904sszcQ0HLrTg3EcdoDRhnnkXlHGrxBI2WUou/cAH9nZ0dDRQj+HI5RzeGHIxECXgoCJKJJu2ewIWRac132pIrNDEW5oegnBI94T60oDJCj4GQv12Z3bw2rpaXowRE+OCnLwjecUs2+r9SX3yZmVI74ampoPc+Ysb+7756hqzxVEwyx1NUZi/MWPGDB5NNXHiRA5EmEUuwEJswhwom6tG+jP0J0QieKxduxYef+wxaBOhz9RNavW6uGWmQDzjXxRQtEqxGzx6ELJ8J/6oR+9oBt9MlP0kHgsZZKUngrd8qXwcgNrRo1ueePLJKew7VbWxFAMZuLDV6YnpdCbiLOLtKBuS5RcndsIgSOG3UgiiTKMdTPEOY6/pyYF3dyrZE5M7FNmk1lHBE/u8Pq6TlbRu55c4qzCbp3e0d8Df7v4b94tcfMnFWUxEttmPJHu93zci+0XwnpAhoEJEUEHgxKS+TGYuvP/973cq4wqFiSJAAUEEw3D9Phb5p+wgl/umj0Tw8NezEpnlmF/09NPPwIvPPw9J9oykaeAuoOxJIy+MHLOvk1huMxxq8nGN406Om3IXRNTj1aM2SyfiHOB3ngeH6TrvUvDUiyeeWEp3ruPcx66jjKVvVtpQAciApbGxcRabOFEXBESpadteKyKtCHUrr4MbScWVNpEYBrimKWsH6tiCqac6SnbUluSbB4NNODOVBNNpNCQQhoAn5pdKHd2cdRZx7gV/JFMJnn2Ndmx0rmP2ugwkIo9ipCW65fKNyEAiFKS8YddB2SckjuU/Rq4tqEfJSJKgelayvwNzYB599DFYv369lQ3ugIW8epf8Fk4sO/X4F6gdbsufO2Me3phC8PJ8MVc8rNpdVBF5rPvmj3+x5mCHKOkuxopTlp86n0mlktG9e/fOVACiAGRQgiVM2CSKyJYlFxDcgefmaoiVvmxr9fohHLocGApvgwyVHPPifEQy8dp748TDiR7S3egp9xjUbQUqTWIrMkXcizsZ1722joFIC1x2+eW8+55QqAJERmroqZOUid0g2bWLkGVZUcphzf7wZvkY/jIqfsAYamvdY8lkFZTfgT1R0GS1p7FRMldRZ0Hk9rYh0oJK/HTNUjjsDCp8HabrLxQ5HMT1iVjAkB2K4vj95ORb5zMkqxeVY8wVfkJpLolzWj4V1xqQThvhluaWiUoTKgAZlLS1to43TEP36wJK3fIjxA2Y8tBgKpu3fCACNq2nbs8be95RuQ+O12QmL7tsRmNlWVFImxkGIkyRYbivs/oTpix35UcdEBGTUkrMYr+iYvjLnX/h5qxzzz03i4mMVJOWfK0yoMhAElQzKwiIgsBEfn8kSpDJyl8ActXKVfDMM89AZ1enZyxaCazUrkUlLVyyQmttJU0FQBmSOUuKrBJRWTI4iARCKfnKIfDSucRnXPMWAU8VLSouj0qJwG75E2qfQ8wV08yEmluaFYAoABmcdHf3jCZuo0ApMkpSxNJKDLwY4a6owPWNgKzgPSs0kBpOibEvMRWZhFMvhWdDHdKGCTqyEYygsiv7+s/jMWvJJwPX+tXV3QUPPfQQb+t62WWXcccxrhTR/i86HQrlO1LNNP7S64K9yV0PC4GQ/7WRDBwCPPwhuggeGFDwxOOPw5o1ayCVTPn6bdAssxEVpl0pk1zUs2KLMbtnOXVAQ55DkJUe65rAuGIXvkYRnkslMxRQyY9Ig9veyqzIU1ARHKCicioXk66urlqlCRWADEr64/0Vvv5NzuCUfRJUTgIU7IFmzVaHWVBp7FKZZ3uRwgNcnkMROQDS3RfDfQ22ctSxDIomzE2+CSrXV6HejrnCD5NmimLF8pXQ0twMV19zDY84EnWkgmo4jcTeFUEAMJLBYDDgIZvw/CYrBA/sFoj9O7a9vc0yK/kWIm4bWm9coacLsxiXaG7lvg5vgIdnIWUzEodViN+JxNDBe2xPcApkZ4KIzxGfVUA43+V5Rl0LnPP5eH+8QmlCBSCDErYai1IpNFHubJZVgDdr1Hp/OqAiHYMS1xQmf84z2Imv17p8DSS4yxomH2Lvde4wRqagSeyH+stBBFwnWGYFDGn939/+L7zvwvfBOeec4wERUUVW9AUfiaGq/4gSFOIsR1kJ8MCWs1gfrL2tzR5z7hj1pur5FjwSc8BeN0ba4H07RBFEf29z4lvxy6BEfIrdtbpKhjHJxCs39XQIdvY6zlOHiNgTUfZtyhM7mUqWqFGjAGRQwlZNumfFIylrz8AUyjfL3JQNLCSLZdAsNKIAAVVTaFZTZ+pr2QkS8FhmCSs00vFbEO/MouAFI/m+xETD2k7YCOidXe/ApZddyms8CSDx13YayWatfyTgkHM7/BWEsYruE088AW+8/oan2x8NbPzkHf+uuYryEiRiIUMluuIxDxIfAEnjUoQAe87v5S3eH9klGpzcKQqeHEMv65AoFAW/aZjrAKUjFYAMXGKx2Kie3p4a4ps8VPrPjT4hWWM7cPkTACrZce/uBCKUeCJbsiazFGIi4wyn7LbPhToKIwMh7DtBNA9IUSnjSjjuHdOaqBDMVpBY2Razti+6+CKefY3mDn+dp6BILQUkxx5wyEmVwt+B26uvvgrPPfMsHG5pdnuU0YAFjTS+/GPdcMqum55FjzfRT/IbSvHrboSU5B+RxjjxA0mOpoYyw/eYip3ILWmeSFSeghTrYl8W0wOV7DlF2RhPqtGkAKRowVLOvT29NU5lXY8B2Bq2wldIpBWOp7YhkSYN9dmNpYlHpeU/Jd4qV3JphawJ5JtI7iqKOKWvxStoTkCFgQASsutqeVdxJNsUQd0cFrwO9IncddddvL/FRRddxIsI4jH9lWZFPS3FSI494JBzO5B14Ias4/nnn4M1r7xqmYT89iBPuKvMEtzxY5pWZBUmBbqDW2qYRmQKLsaUVIpHXvaL/jc+H6ATQQU+Ju6z4oouoNlmLOL6VSi4CzTqsYpJTIhyJ/rBgwenT5s27W01qhSAFC1tbW3j+mK9leBfAcmTy1+Ch/oIiMcMRT0x8n7a7oIPyVpZuSsqvweeZHfudJzjYnJ42ZFJsRufCbpmulVh/ZlYnlxE4vSU5m+bFF55+RXYsWM7LF68BBYtWsQzuWUQ8Zu1/I52BSZHDjTEzyDgEOYqwTow0m4tYx24tXd2gJcTezKZPHYiKte8RcZhMsaRMT0N0OQFE/UM5IAunk4hRZutUDcvI8umS32Tg3rLAmX5HwMWWp7gFXmuZh2DcAaCpYwUgCgAGZB0d3XXxuOJiiyOnLUqyu5O4PUCyvHsAFlJ6LKqlnNGHPruN2P5JlLQZPGFBcuFgQQ7ETkeoqe4xos0Uo+R2JuE5bF3QVtrOzz68COwceNGDiJYpBCzuGUgCaoJpcxbR45t+IFD9nPIrAMrCKOTfMOGDdB08CA4zgG/mVZmxh7lSpxMdV5oRErKcPKLZMYu20uDSvT6/HmU+lLMiVyIBLyAQgJi1P3DKhcjkfOifE4TUaMrFuurbG9rH6tGmQKQAUlvX081m2ylbviVn5n7bD0egKEebu1W3PEreLkyqWwWoL7lEvFMXg/6kOwOhzTIOEzlBEfbd0MtU0IqleQmLUvJM8WuWWBCSfbqU56UeB7svHdg/wFYt3YdLDlnCZxyyik8d0RmI/nqRClWMnxsI4hxCODADYEDQWPlypVwqOlQQENAmh1jK9loreQ6LD1inQt8oOL5XBYNB0/tuGx7LvWGZPknC/EBB/UjG0BwQTnZhOZrKSoHH/uAjdovJpKJ0r6+vio14hSADEgS8WSZafw/9t4tVs7rShNbq86F9ztFXUiKupEUbVm2ZXePe5xxujudwdiDQRqYDAbzkrzkJUDeGxkkD9MIel7zlodcgHSeEiAzjQAzjcBye3rgtjNqdcuWZImkZEkkJVK8k4eXQ55zqlb2/uvfe39r7fVX1aEoW+L5f6F0DuvU5f+r9l7fun7fcK5MamuDnpce29WHSWOIle1gITngUxK9Oq7GFIDX70jOpreZYXxNZrPZeNxqKTEiGTT05oM5pgHPaf4U3NxAOx/lRmNd5IMPPqBjx4/Rt771rQZIot7IJCDBgnsPJusHDY9x2LIOR+CIPGdRB+W1115rviPR1ASki3Z1KDtuwx01rbgFOIxkgAID0wKVjLvY50HEy+yADnekpdx8U70X8vQ7Dm1JvScUqMDrR+njocwFR3Jrv/p6AFnXEYnUEmCwijQsPbtYLhPjtYsJl62XZBOzGkPQ26op3413l/LBIhUBtquqy0ixnaKSYIhiamsYqeOHwcjPByBxYhzkYGHOHuqpkyfp9KlT9Nxzz9ELR1+gr3/9G80gYmS9RSCxXVs9mKwfNGxXFQJH5K6Kg4Axxfj+e+8H4PgVzF/gwBEAh2j22lgvG/OttdFqfmitp2HYDOtuD9tzLqzvRwdJsF/LpKVsMwubvaSAi/Xrq/1Gfl1GSjortcZnW9AfPYDMegxHo3n0oBT9Aay2DAlAX10cdgGxG+PokZ0J4dIdYieoRIyCSD14IoS8XKx62pOnh5xbJaJSZFyAiW2dJNJOBAQZDySCmBVs0PzZtJsx+qgxIom3v37tdXrmmWfom698s5F5jXrkCUSwa8uCCQIJ1ksedUBBwMC6BtatMNpIXVUJPOLcTvzc//Zv/7aR771+7RoQnSNbM0y3CtQYmvcc0Wqsb7Q6HWltsdh0pgEiKiJPxaBzaQ8n3V+bmXARgLieS2IVjUPKV0q9AqMYSQ4eDh2KjsLHzSEGrICMsWBO/NxHvZ3sAWR9x9ra6gIWA1kVBnGSVgyLeiqGC+gipNIFFqht+gAjAVKboWoXycVF7VkZmR4VlgtuQGj3ZTuMlds408YPxitEIysxKompLRqLWo2zYANFHcFGHz5+BleDAYt8Sr/85dt06PBhOn78GH3tay/T4fB7LLinaMRLb01ju31UwATJG22kgSkqm6ZKt1gYP3fuHL355lt06tRJ+uSTj+ne/ZX8pSpFQFVDS2tjFJvrgrOw1tY5RnrmQ6xuJkYqOOGhx8eR3TaBCKahCqsuGb1CW3CEld2etwC9e3mOlGK4pPOSrBuC5RM2fffICEwmyRzAdKG3iD2ArOu4cePmvnrxgrcCrYoMxjqJ5rAp9KGRtcpsRStBpxAovydr2dzqPilUE+nfkM8WWwJxai+c6RyKd8ow69IYuGEwZjE6G94PIDKOEuaaNuCxcc/vJ4leO/ukdG/lfiN/+qtw+w+vvUaHDh6koy8co6++9FXas3s3bd+xowISr4NrGn36Fx1ULGGjBxgWODDaSKBx+9bthhn3l2+/Te+9936UHgjRx2WqFPlIU6lL6zSMM1bDZmK8+X00rFKugmsaolvVFgJt44nc0BYZRDTDaHKmUiTMUNewVEGsroLM9emUF8pGS5suZlH01iZiSc4cw++istDxnps3e0LFHkDWcSwvL2+7euXKEylMZtiQbPKwNddh8tY0y63DhgLtsaQIGtn0t+tFLWXM0A42sqipW73ZUL+dnFy1rvUgQAnV3t5oODY4o8Gw+fdgMO7gaow66XoQCmzFI7YAX718hd5+62169Uev0tMhGjnxla80UUmUi42Riafy53Vxea3BXxRQ8cDCa7n1uqhstBF/xkjjzJkzTbTx7jvv0tlzZ+nW0lJMsRCBqh6z/fxBrjV2UY2Qwn5ElUgs0pKo6FJyZJ21UgwpVpYkUOulHZZVz8HIhlWbOqs1ZyjZQXWTrBMF9Q1W9BH1/inn3J6BcLXukzMY23jv3r27fevWrbd769gDyNQjtu1dv37jMam3lpnytlFDlXACzQGBmp7RmgAcEUM6pLq1II1gJXaKwhoSJYpKDVTPh7bIettauU9yFB4kp9FHo9WxoW66uFogacGkCGuJ0p9eC4bv+vXrdOP6DfrFm2/Snj176JlWb/z5F16gF8LNi0omRSY2OrGA8nmw7yK/k01H2TpGF2jYSAOjjRS5xaL4RwFA4mdmnQPlLpg1lt9nNMr1La0wboh0vDWYhNNY8vq33nppOBEwwTpHamTVqGLTzXxZ6Wq0EmFF5WiawSp6IeaiJ2LcOKnWuD6r9Joh0tsfAGRHDyA9gMx0RG/j5tLNvRUQMBpoqQYB69+018RiIoAKRixI6GC+RB+kYgN23pHBg6Jqu1cmUHd9KV9NDISJA5CJLiU+IBZ6iQbZeMei+BhMdHVGVHoiHtEwxlucVdi5a1eT2tr/2GN0/Phx+sqJE7Qr/NsrvCN1yqToZBqozAooFiC6wGISYKRBPK8gfvPGDXrn3Xfp1KlTTQvuzRs3G50W3fek14ldE9K+T3MOucPKVjHw22Zy5s/BtLb/UrxWukbHYl/Xfsfa+WCyQCUq8rHPrCN+Nmeg3LwcfFjnybtiu3PxWLq5tDdy4+3fv/9Cbx17AJl6xL7vuGCqELzKj/ppLDZpLLU0BadeyVIokpLPpRpwxEQe3Ale3obVz68zywKJCnsGQtPMK0MKYJQoKSQYxuEYUKI3ON+wAnPzb+FB5V0mA3jz5lLYuDcbj/vnb7zREEEeCGBy9NixBlBiN9euADJx+h3TWxiZrBdQpgHJpFTULIBhi+Hx561bt8K13my6pyJgvHf6dAMaMfJoAIAUkUChLofzSgAxntMYlnMzTgE73B7WkEvV663HMIzG4ERnhqDuRkZ0TdiCEzkORu3AMImz5ywoiht1SxU1sdt9b/fUrdu3dvWzID2AzHxE9s17y8vbyPj/5Hg+zuiSqpmkRavo3auAGfxKnvz6Re+cs6Fn5RPqrhgzkoWZ7Gqj+AVY3cDsDRb78Yzo+bKxW0yrozFn0oDHdZPYIpzARavFlTeKJH3RmMbc/9lzH9OPfvSjBjgitfzBcDtw4EATqTz++OMNsCCQ2FSXNwnfBSQegExLS6EMMAJGukWguHjxYgMSly9daogMPwm3CCQ2BqzSnJkvajTulordU8NRS2AoVoIGGqmkGgWy7LhKAnlCfEwQBWtFM+P4wPgGG844O4phoyGuVh/ZBva6xmJfD0TUyKSq6shHQ6bdEfeX72+NNqG3jD2AzHQM19YWsHVPbGwBk35sPK1q4JbF8a5MekD04+paB0QvSesA4aIV1WGqvTrL/MtcU+KxowJHFeDAzCDV9GC4/TweO3te0WuObaNxYLEBlDRj4kUHZOYBwi/R4MaBxXiLf9u8aRPt3bevoVHZuT2Ay6GDDbDEWyR7THruCCJdYOJFISn6mAQa+DNGEJE65FIAiXiLnFNL4ZxjS/O1cP/9e/eVUVdGTOlYjKO5GFlQm45qhvxGUkCDa+MnnvVnNVqUhaJwtMPzxi0tldLNsEQIhpxBjBeEw+TSASTkrXk4b4+RuqK/qmQKvOgCFA5hTsXyPa4OVxeGfStvDyCzHmvD4fxoOBaS0S2woinbmSZ7jdYjy/eVgUMiqgyJ0iUg/33Eblhv81uFNvY9LI+j0TMibjQjk5/jeoeOcVDcSm2Ekju3eAwwqWW4zOCUuDBSdkRv/kK4RZMYJ+g3LS7SplavJBboE6BEGvqY/ko0K/FnunmRiI08ErdUvCW6kKWlJbpw4UITVVy6eImu3bg+lohdvkcrqytN15qQR6IJqbH0XqPCNZUAAwv1IxRNcvKphtQ562Dgl4Pyymwkje16YRRjgu+NxaFi40kdhwBCTKbOwi4gWIo5zcSgIw6P0adi3fVAh+Es7KB8dCiDLVgb9sJSPYDMeIQNPAgbeFBFDawNu2eUa88bNzQkuLiOLogciVDRHHRVGsJjdtAEpp2COyqfznrYl0wSjTuuzXqVNGHDksnnS8XPWAR/Gl6uhqNL0ndCw2TIIEpIoGK709ZkXGOItOXx/qhlEqOVmDabXxi3CEdQ2b17d/MzAkqMVCKgRLCK90dASeB048aNxqhHUIiRRaxb3IhF/3B//Bknt9diu+1wqOZhkgpfajYYpShNxhotIqNcr0AQ7UqrMLAbiA0hqY4GNFeb+VXRmlladN0Wa5kTrONB3DXKClUWNsJnVNJMnU4QaUXBKpmsB+kr74RZi0RJh+OjnCTjdI1ae9Bbxh5AZjrCxlYLpnLiM3U1aTEax2AzLGQyTYJVJwwbL83qQqM8J9W1CAV0kAhOZHJMmmqleJIlt8WOR6dzynrzljQGGDbnOQLT/OV8LD8jWj+GdJ3e5UjoNxoyFZ4wbmoqhTaphZOYnmreKH6lwyYiiH+5dXOJzvFZzY6cFBmdEMqTY8FOLAwLm781BW09xY2zF1Tl/kXT93fUl3LK0XIViok2GKtw2EnF2eCnYVF1gWCoS7or8azZ1JXmIFBM61QaRqqCdaVGUN4b/41aIlzzLOTzqqN1GIz1Zras9rqqYOqILDqU4XvrAaQHkNmOSOO+tjrOeeJGyTMd3PJJtTtXoNah0kaON5YL58LVpswG3tQOGKxwLn2zYY3HThebXAbuKpz/EDTU2Xilfv/0ugwvUc+/5NmAbIjQIkrpOksAkfPNSeFOGyqueB9FTeaL2fWZ2C8Vf2NRGUBgLF86yufMEMHknjHIng0GbQdZIqWUwsYsqbss9diNRHXHNsVsmI2RMdqNgY5NOIrfcKLLYWQDQA8aDD07eUDbZpGjFFEOQPq8WWl3lLQqCZcieuKlEuudS9UXVbilNEO1bvSVMrEOa1mwcaJ9b0laNGkwlr3GXSpqngiawAIhsGY123wBJ2b8Dh0FxnDfcLg2H6LQzb1l7AFkpuPatauPD2U0x3lh6oUsOaKAaMGGA8BaKuAlaWlPywzKEHEI8GMVN1MRNNbcjiaJy4rFoUQyFqjKhnXTTYyTJQlokrHRY/Ro/LLfDcymNkdO6IFyMTrJMOQiPiKLobTPPW2OgR2PpiQrOBwbz6E2OpZ5vIqujMeaY0n4DtlJKZU0YjHqAsCadSqk1EIYQLYqcDAUeQVkYsUuKYZitiVQtOlLUdFeovMQxgi2XGde+2zqeBB5kehlyLA+VJKr5tcZDyLmbjNd4FPzjVCASZ+Z5M9HMpWOEu3E82FSQOmUYXLGYG04mr96tReV8o4+LHOOK40CmahkqTCr8DcNKAkmo8WMF3I9c86iY2pGgyRGTiG9duvSCrRjZtJDVYgUrSFSgQtpsiGGE0hJYStra9lJc/U0bXKbWABCO5XfZicig642olrrAdp7iwHFKWTo7BJRk/wC18WJRhaAU1RfsgDVuWmiRgMmAt1yrPIwGRiUgwDfjeKKYvCKuXwPYOkEBL9QUlkXH8w6Q5nkNuLLU91QUEtAxun1TcGtFNyRxqfIFuhIE5yjjLSc1ypLAURFJ588/+wYIUgZ8ILQNEvZCGvKnDQFz8D1pboJNB2PiBhQkiKnKzAHleh3Wmqj/ugjkInH2trawo0bN/YnbytxWKcUhqU9L9EHtJcYGU6VOLc07NlrYsCr4hILS13FFKglgNepJDkzyKne3SpPXdo7S15cVLoJXTfJ+ugEaTtBwSzWRmB8XYUjqbD8Qr5eeYCQ4oCIrwAKVVorJY0FRgz0JZTBA7W5FM0k/RRMmeTrtP2rtoCEUAiFE5UKSudmuw1yYYv196U0Z0oar+7iIEXUKSyanl8VR8jU3gTqEzaFCZGelKiTod4lLVGhihgVoRsCAXz4pt5RUlmYCrb7qIBEJSHN4kwuWUZertJYFZ0CPldEp1olkqve2B9tw/z8/GpvJfsIZCKA3Lx5c682/kaXnNEbE6oaVVGgh5WquJ4UZ+1CihWmYq9ZHoqGSmtEVIVdGYJ2TiSF+7GW3NzaonMlLlcNubDu0mEAUrXhbOhCJXIS6B1lo4BnwdECoIoGRX+SkBopHq5UHrr6/FTXjsnUq/R+/TosoGnBpW3Njn0Kpp3UsJ1pGxJ7FmyiHLgcMeeD3zWbtj3BMIv1Z60mPLkCXGSazk5TSleySVlmAs1WjKBlGWio/gewZ6rIWHNxafEzFW7piJ2dnGPVdSB1uhMr45XYlHX0ynef9tHS0tKeYd/K2wPItCMuktu3b+0G8iotuER1YbpyawQ3rafSZiRF0Vs0pjwbW6yUpxQN5sqZSY0IC5uBANPrKA7jkIhK65TQXkyHlMm/qYIq11K8ZHLdnk67CYsEz1N9VgzRHbZ4GY1tE824UsHObLbKt9scuSWnURQdjiyrNVT2fdhEE2xzUKJnJHCSUy05O53DOuWXf3Y0Y1uDLc5MuzWyJi2JMrCKrFENmlhVNbG+BlWynMLKccKZGRVBKFE0I19rh6Ycx8BU5vX1jgdXd6/1w4R9CmsGAJlbbmhMRPdIGnuiONjFzGWbYrQCoTyR1aZ3xLwWgo3X8M5WZ12oUppDT5FS+RtzwQPtOqQ0gDh1CKkLi9rD08CaJ84rDnvR04iYqhFnuix5pgznjvlw6jI+NbGMLg5ABGnlgFULmOjXZKdAJazqV8xiDLwBbHxfo5iX5YpVby6er+UDcYCgUl7m3GWkJIhFlKZ9iVZwUFacgQkNcIyOD34PjTZM+uhH7cfWFvaVPoeteVlwqb+T8WvYNLHUIFAFOqKjDzLOYfW5i4pwlpfvbo+2obeQfQQy8RiNRgFA7m3VNYyyGhmLzWr4zw5lcEXgo4rumMtlY+xUOkh04TR1N6GXp+hyRf3OA8xmiLM5zLg8GjHWgxxivVUx/EZJxMczgmbQTaTuDkr1DAHFOrYRhHXoxXjF7BkT0d1I4oytqWKqLZo4eKqUKdnQ48O5knEKmCqKAGGkFDfeu0OvrjhtVEaL8xwJU0m3edPipqVML1KsVzAp9b+6iOCoX5oIW4iV0qXLfmUjVVU00hT8OFUi4jxfXYJUqUHFWKq4h2r9kHREmxBtQ28hewCZBiCD1bXVRS01ILXV4tKxkh01rAfABindQY76AtS+c545G2wuRghyx8w2ZSZgy+A929z8gHGk3U7nmUkV0XonusUS9CZYyvRxMvBiXjPbP4Eef8rzBiraYVGgIVVbZTEYTKa3li0Bk9MhhbwdrF1olwoDayAs2q5CBKhA1X4vqA+u9MdJec3stAIr/8MCJ6TNhEtaiHPrN1X0Jt6wowIeqF9lJt+0zIVNyo6U8zJeMzYLJdngD6iwWQum9hRBF6vUL9Z1UHdGcj0PBxhhDVkektw0Ihr024I8Nl3kuSjjxElTG11d7KfRewCZesSp0+FaLJYJZb5bFYgYGgloJWVMAbCoZ5S7GKJlVEcr6SbCHv2yS4tRqVQH0ZEs5xA90EHqrhKTorKOLrwnE5tibykM60GxQstCtjZg2mJVMxh0fZFLqK2gtz1HqVTy7HVgpCYsVYBW0mgMLa5+Bsx0foKNwiYFnAdiXVw39lrXqNrVYhoPsmRsyxrAIHWchutyOysCpAI+UV1xxA7BuvEP9HAIDASaD0KT7rNKNTGoAjJ22hEpeWVW6Ukw+rn7r26FZ8XUTMa4Y+0Fu7xMaanKKhrmaXTQqmBGIiP0vIz6afQeQKYBSFgvw9E415n9YeWwCqSuJU8K6whcFz319hPVbSWEeXCwCIpGRFTbcMEcrkyvrQWq9BRhS6ju0hLw1upupjQ/AHMAIgbk8FqNjpyoQKmdh4D3UEOQZCaZW6NmGhZymcgwUIpHzWcyVilqYKd80JUKYfBoyaTpi/fLSm5VsM1b4HNrC9v2PXHeQr22IPF/+z5c2odz6ijNKrEoJ4NtPYbqMpaXlitMBbXyRlIOZB7otFX+bFm10ervA5Q5UvSSBl/bWRmGNKyIUfpk1otddHZWswFrNt5MFQN7N0VRzKD7Y9reY/1DXB7gHkD6o4pChBPNgUcyzRh2k+lFyeFxyklDX7wTKbDozQyqz5qRV2pXOSkcKrxSdY00YdzWJZoWy0Fu4UUakbTZmH1eujo1zIa/vsxTZANDdmiQfYonw3CXubWkQJ6u4ZBLqpKMS55MJk/1wfajoXcgZgCQM0WLuGUtUbVtwc69BBCi15E48ZYXf3FOjxop1pTGIVTI1F8QVyDIMFPi6PYpnrZaeZLxGzTUIvFxzZrK0UvLR9ZGDcxslDUtUBVtm1pzh30CT5GayDOngPEL4mrY3avmCNAkMxJhau+nB48eQGYAj7BQInEai97QYjxY4br2IApQCIjepK5Rqk0hpBtavK2k7880IqbzE18vpaMGbUP+eGOXNMmAjRSV1K9lYFN78biZWeeLVIOsSekJ5vUNQaSwadM09e8qXBR93SpiItLUMMZ4qPo75P3xM8bir5IkNumx/Hlnx6Gad8SgFJ1xh2YfjZlUDUJsnQuT9FOOulmnVQqQyufDbsAGE+NZmhmighYoBgOkwC/gUZwXDcKqUVgcDRnto1UNigYnTA2OYEYLrql1Cm2jmeqws/5Gu44iwar0INIDyLRjdXV1ceX+/c2aQho5merUSArnkcZbsZ+SbodHC1al2XEz2zkypLFLxURTFxCuJinaIrY27NgVK+JsSPLFryp2XEjb4T5mMxcm5k5kJ+kYClZjJTjIqT8zUdhlQVAqgOHK6NZKeLrxSVTtw517hFuBeTvWYRu9xAmClEYGizKkpaFN8zwpp4F92nRrfCt9cPs8qvusHM5BV9ZZkYfmqXTR/Q4G5KpqnkiO5CttEDNoT+J0Totz/mat11tZatKI9vHRJkTb0FvIHkAmHnEKPclXMtUCQMoDFV+OU8+rWTnNLnlOW4suQ3ZMtdhU1sAWMekrTfU9SAp/1N7AKxyA4p93fljfYVvHYGtMdO0BC/5MpFIomQdJAARYsX4UD1HstZvGhEqNL1HPqLqwqgGxjRioBgfuiMTYXKNteMCKl2WaIRt9EDYg2XModRMWQzTgREvKwBoJZfXZibgyzSK1QJSwKKcEAVBHl9w2a2CqFKn12VVNHLVrGM9JvDWniCLFvWasVYlNDXbtISeHyHp7Z4qb+8EmjBkq+qMHkAlHnDhdCZ6GZYMom5qr7KwaOlaRAni8yOBLuiHHtrLnfhY1jCsmOihe/Mhm+kUUfxRLvalsGgH5mrJRFeWbAeCJGnL21AftRhzXNtjxQs2QjNieH/CwbbSVjZm11EbLIme12I2u2HIpsavPZAxWASr7Kvp7FMU0g/n7LAkgha6dsRpsVhG7aU3nBIWrSIfNWQpRlbSy64PbNcuiG0WYdcRFRK5zlDyZ1JWl14lOJdmSg5dOgz5FrcuCkac435lQx4rSzk8FOLnzjpvMRLQNvYXURz+Jbo47d+7sHAHnDYt0bhJyUhNEyBzld76oJl5XylAnxlXDIWzEJHeqiQRtd/y4U6ZKWbTnOFInx+ClcwVotZfLbl4dFd3t+2JCkC3BHs5DwFVoMSKPd9tOwvvFegFjbQ09mmkWcqCAnM4yU4pnBFRxTTxeezFsrNQqWWzKTXcyMdUysIYLubpGGwdz13qW+jrZpPukqulwZxrLZU9pHzzC+hK5gpaaGAG+37pWZ+dCvLXHdQMBkVpfucaj1jU3FEd3797d0VvIPgKZeNy+fXvX6tragqgNX6YexCELtF5hF9BQ7jSxW0SMZ202P+wi5cnKyIvCTZjPsIvFGCBMubCXqKtSBk6SRL0u5whN/13r1nl+H1fpBTth4ECGA+asXrkYvLaZwWlQEF1dUkAo1SOkMlBpbSQ09pi2uhNi2ojhGuMqQpDK0HqrUJzuJf2tSs3pVVWGCITHpHIP2HRseavaukPivLcAfTqL/vxxz3BVt6oTl3a6SrPScLVH8VxEoRTnWZfEOry2troQbMPO3kL2EcjEI/JgjVk3RVW+JS8pNhDCZmvZZc2VF89Ut5fWPk8ZZaTczkm5ujdqPen4c2BEnJFuvUyhD5S/yg5A2m0qFRTY60FvT0BvEU2rVBDKVRLOepHWhOqYRqoEBHXCnZBfZNI1FXYTRaySdtZztddraLCEtQa3iagskOvrYFXroY5og8yjqAOi9LfmJPCUOBRej3ONWO1qGznSLIj+zMq7DsznUqSZ4XVy6qj+xsk6A2xHF1GmCsXbjCOlIn+PQw5qiPi9hBMeDWVuZWWlVyXsI5DuI1IVhDB1e6ZtZqm8NyFxzRbl9Ijpx5E6iSEm3PYG2sRsxkr1AAqQef4id/MgO61tALbeHbkmSo+fgT8PKoEu+LGXiAJOK+3r+XGYIHAlygmuwLuYdgvH7CSenJQj1Iww/sFIRYyxLoP+BgarUEjPsaTWV9G9eY4rwln+lSoYYQV6TU0Ja3KmtiSk50TErFV1FqA66abuVA3DA35riFvwIDadewIRtLSywE7rNiUKFb1iKlLofIVChiWLdOdCcQEJ2BZwOqm0a4sSuIrH2nBt/tatW7v6Vt4eQCYdfO/e8tYx5w2r6WMBo4rbUqROwShyUDbtg1InNQTZUDvqJX72v+3CyrMYhWZFGcQqpYEbSgAUSA1T1VEDZa1tNhUQsV1axh/VLVEg9gPXyagSBz4+dnCxWLPITn5ezOS4VKlAxScIyn0MHFcsTryJfGXQ7eWywTJEMM6EphhKelXsNfTqlkyEcG4GFBFrQloEna5yuk7mpd7pivPKWHkmUdGHSqABj5g9n3xGqjgvIGaG60DXgao6iej+x5QCLJrwTmJNdHLVarcnQTGG/RCJFO/fv78l2oZ2VmzirU9hbcAjfvErK6ub6vRHGbRTzLtmo4siF0UGUfCImOoFzZAkS9rZjvBOCb+pqX+ImDSRFIrr4immziMl1j72HViAe4vVn5l0541hgCoGl0Vx4SEPUokglGPe6qlTUY7LqQKTShOuvgMlSgXnkT/DRO5nvFUm1DQRxVGFfxOT1MPfbB6e8sQ8m3QJmSqBna40zIMWXBJwpWs0QzXjLiQuMshcGAik0rZAfOCclipyw1KTSbrsu3gKoji2WDkGsK4QA1mD1ygPI0r2Y5WyAWnA0Ua+u8KY0s6q9QKZ3ElqRmDFGdZGIM5ejs6ljAcKa5ED1qOu0ZbY+/oIZAMASPAyNqvdDnTdwqYgXVV8/abPLG1AUikHkimQZyZS3ELoVSNjKkwEo11jVomOms8bVN0EUmJlcAFZSrkmVcyTbZCSwc4hrsulmVaCUQ/bfgbiAmtFp66YiVmR8RHVekKZYbXywk1XkaVsAdJIXWsGaDFyGexpUkAwwu7EM1QpcIDSqSHhZ4kyt1qAELQymJXUsqrTAbsyuzUgorqTDNdTexYYhagJvCJLXKVwpVD3N80gQDRalgnUH0XXh4Q6+qxZbDwF0sKYEoCrs1xp7NDDhzvu31/ZnBh5baThRR8bIRLpIxADIKn+wWLaKIEvhyp6dKSbYK0eKALaT1wZRdRUxyZLTDmIne5jMPoEsrot75I1gIxTAyo1Ms5RD4V9dUHS3r0ucphxejvyXAlsYaGWlbpi0ngoEYdRpatCIwYAtNoahoMinyMrgr3McFwBrCP8ZFNKSqSQtT4W61qUSIlOUdYFjXBuG1XaG1wLmoloTXnlRXuiV6zPxWnfyA4Ac/VZdbdNQ/QnEIXgOTT8WGOerBGPgGWdlQM0vmdAZFKHjZPBVjOHq89OccpBM4BUgGcAnUU3UYizDkzcGWdBIoAgrXsCiRRtxJ9436MeicxvZLDw7ovtenWbozEkYn5n4ymyVbtjM3GM3V3w/8yiLtq4ZI+agS4FaR4gdFfN9zLWPM8/SU3I5SljJ/1St3di5OV4f5VaH+mNKxUlbvls2HzGZPPXRpKV2Zk6w/Ch4zzMZ1Mp41VyuFKnngR0QLL3L0pZUBlYXANswpukdwFqe1VUolTzaiNewK4Gpqp+Qfr6kPJey9qSNqhWohjWfSJOrGhweZzgSPUIYYcUrG3jFdbfl0p3icd1Y7vmRIGTCrFsyhDoYao0on0v0fQBUdI2svKGY80Ch5fSSuDxKIPI/EYFi67HNapjXRNQwj6Zj7KmUue6K4JAqwkKxWjWzxeujUHzl1GjXdLqWozMxtJj4DyIG3lkIoW6rVQBhXBdBK1y5OSo5XngQWZMmTt0qaG5NrG4iqnRVCku+5lPADHr/SuiJQNaoB1Ck/Y+i3MOdTJfckqG62uwBGVkalaWWMxGuSxkq1bszjHq9FItDWyJpKy2PRvVK1FlKSUV64EQQ20rdRHSuBOrc934OxX2olUOlXqfdvklGNRgJKpG28sTh8O1+QggKG2LkUcCi8FgMNoojngfgVgAGcoAyd8qb4/I117Gbh/WEua6mMigh9C2erJNzxhPz75/c88IcIa1Im7KJcf3ieDBIFQVN++AdDRha4LVJtadTMxalbAiv1Oa2uQAENVETSj1yqJp6u0QgEpzob41O4Bni6VJnMl83mA0UxdbngdQqRlxcYLs49F5SKlFUK8k0ClPOXc2VLvFOeZan70CbxC4srruzphLGpCr+2Lt52o/b6mBQjXCsgKnMUuWD7K5FsIjkOIV1WHGYh0Lyiku67RlNmwcTMy1OlKCXPV0jOj6kSHh5GYafTSXUlgYVSTwQBDpU1iPIEhMi0RKCoucxhnr2diJDdG8PGwMDyja5Ty/lDZN5fnA88qm0N5pfEpYyE2OWawehNHcHiAdPQoRQWlETQKnCKVD9lb3KdvWUNFlEXv+GZzNxrb0sZb1V0qawbL9aiIlbcCUMcLOr9yp5Qh45eAL00QE9YcunXIb4EiWg831H8zZq2ujirWQlWqimM+HfbAX8XlEso8CfWSYwkPkSV1MWUKWTRpXYBkMVKoIe9iyaCFzlR5roo9Rm4pFssskd+wIq7GRLVd9bpBizECuxELqyFXyd8E6Rak0Sko0NVwrEYgFkBh1pJ9oZ9JjHlUgmd8I4OH93hGBDBref7LqZjY1xCZnLqbLhnS6yAy+MhtmHi4eoZpjSK2pwJmUct6pa2rURCLGw0JvTVV8S7oGZXVtYw1jms1Md9nGKFZ3iC79iJ4PJhbFflXDsFPHJwHskKKXorSxNZSr9JRtIHJnOAytIHy/JUpsn2OBUxlTY6dY5+HrjJOlyLcpP/HLMuzVLMxJONONthcDQVnVC7CJC9ay2PPPn8kA9EFE2eH4mIHy5ctTRw0VT3zdufJx4vAui1EDLfodjG12KlLVe47x+5XaQVE1SNHrJ0WI6b2Ho1EEj/m1tbUhAkQCjQQiMUKxaay+BvIlBxJssfMAJX3xwcuYY09EWrOtqYgcd2clFmTSv9h/r+uEUjmUubaXXxu6dQSaGUcAOILpg/FzM4nJgHLbfaFGGRAZsgoCOm2byVAZHzbaHyYqyDPBIlVtUlHec60DUpUUUtQEMyDJQii9DPEL++xYURsEICDmYEAMaYshQFTG3V6vk+0Rx7azAizIaNoUXo4+xQA0OcIYrh9THiZUAbBXvhMw6mXGprTYJgVCzYWvDKcDqqVtvKHkYUdJ0VsDqs7taE2yUwJxakw2m2r10QtG63UbgCOCx3y4pgwgESii3ZibmxtaQNkIcyAbqgZiwcRGIWPd47p7EsWNlI2GJpiOuT/CtCvbQjvVAFTVoTGXK1pQaiRSp3IA8LJTOsdN0T1JmyqtjSaFoEN126lslHmVIWC72dUmFS1ChOqCjCUNrkGaHIYn5ZFKVadVKX8nkrIlbBtYMlNtTJNHDd+5QKpOdwprynm3kcs1jqIH+8SuPVFKkApgpKgOMtejSDaqUwQ0JlUoJvVlaz1io7AB5+4+Rnlk7NbWoYNKC41wlsk07LkaOaQb1ExpiqDkAUP8eqCwCs4gulIlOrd3Iqa4C4B40QbWQSyIPIqgsiFqIF30A9jPnQEkytkq1NDRQJWSNx5jpaCmGkIcg+qXEqrip8eFlYy9wLStjQYGg3YafaCH2MazX20bpjhRlJhGH8d7E9PEwwZoRBkecz2i0yP6I9BMYBacRXDGwqTLuyhhqW4ssn/ErFAVJSgc01ow3vuhMbVjMtXIgZRrIvsY7iCwx89aaqp6Ij8o4I7PQwdRI7I6uuKkG1l0lMHOmiCMUFgUiWVew0oaWgMWC1XfsbDRoTHqjPZzIvP6tqmNHcl2777V1bX5+/fvR6aKUYo+sOaRwMPWQfoayCNQ/0iAkX6m3y2ApCgE9RpqL07nJpJQkvISE4mc6A2ouJMYHlsPSatGJasKMW6DHJXzRG8sMfhK0ayWUZ2VszWOLEtrjYVonQnbsVn1F6Q6TvJKxdmc1DFmgkUBz3KSaVojvPbS1VZ1wXmRnmqxdWRwvflKzJljTMnle6wosYjqJj5NEuZ3zZJO2QgOLVLp+FKeONXpKltssoGf6l9gUixnil5ETK0510tYt+KS5pSqFFdS2WXUdhK2i9P2bLBhKq6cLTvKA91YVTqKtXgVAyVLVb/iEk0m2p2mZhNsQ4xAAkAsJLCIqat4w2gk/fSikB5AvkQg0hV14A1BJC6OsAoUO7qSHc3dQ6xdM8KiZ8tFlT1I3z1ShfHc/svg4XIljcvqyYWMrmycOp+dOqoGbAWl0uQ0Q+uvAA8T5zxwppN3dnEmrjNokHS7laeXNiXkxdN7pnx+SQ+xqploo8LAuVVgtSjfFWSQyhvtzjUy112qyA1QyCp1++e4AYIrT17VuaU4GxrLSleYl/oqkR876Up9HqzmNEwrqjizDlgvE9Z1CKkx137NwUhmHizmWsMkRUc5zQVOUKbiqXoHilHHV8SmlpSyS2vMihDY+qSNKBhqdOUcjGxB7uZSWVBeXV1dCNe9liINWwux4PEoRx8bpgaC4BEjjC4QiQAyEmFMB4hZcDadRZBP1ly9xtjkuQl2tKz16ypPUpDopFCixDbI6MERtnoKV3KhgwFI25qEDJNJI4kDjMTaE85pJDYGw+8W8mSglLcsWpaJ7IhJZVPZKUAjRmvRdhZF71u6rkSrfRROKDucDuZZakMqovN1Au3PqgMtSxSXD0bESZCxntNjBmciGU6k6yh87CVdQ12zc7ZZgiqpWQ/4csOAaBGtseVMb+TT6jUR8Ch59AD8bQ1kzCg9AGPNJjITiDIR5FiBiWpQFresqFqwBUnHDFqyWS+cMxRrg2gjQsQxb4vlmCa3mY8+AvmSpq8seGAE0oCFAZDoXTQ1EMjRoHetgQFz/awMAyNtBbi1OOCHBr9Oq7AePFQ1ZMnzASOlAyIZTNiIE0UQGRfcBdIWPBabYnZnF9khHbRpiLwpoWO2eIVl1oLMZyaQ0C5ts1T1CBtKLzXJXdXfRbP1CmHNB0xoppYRR5HF6kkQeNOSdcLVQKPFLNFpzuJ8iJbqEvIlgfWS0azOojuEKnEvSLmQWKgAz5q0vjhqrxQKeyMcIDXoNeBQevycJopxoZ1GwBANublcAxlIxRTECLyQJq2ZbJKAlNSCbawTjQxUM3r8pktaWn83cQulFFYED0xdIUdWT+f+iIBIV/oq1TowGkkAMgpehhJ8Mt5NMVyidDeEDfsB0rPj8Bp6T8r5F23w0qwGpHYSXXphMh2/0wgoua1+XCZkzX2pWgukACSbThgcABSjYE7OEL4oLx0LFarYaZguyEQApCSEIZ4SqVQQK9GgqitMakaVqs3V6GdXdCICERc8mjXvVTJOwlhkh6l3fJfshePvmv6eDVV9AY962h7p9/Waw1bUEuGx4hGDx6PjAR9IrmeI6CiRixNSOgbHjx1gBAIV/3RNo1xEH42lCaDjkGGBiKHAYZzNUefMRlYq1SZxMBAm3YWrRhkbJghEqqkZJ9qIAByrGHmgE7rRBKc2XArLAkgCkfj3BkByF1ZaY6xpithSUVApgmOFPDPLJoPPlYQpshehMCcaqEzpYQ1fqwci2GMqdUW6bCQx+uICBgBy4s5on1DRkUC1PoaNSqZ4nyMhqSki7MSJdKngqTqUTggp/RU7uQZGj4wOumY+wVZcJEe0zVzawKp8e454mGwrEerN6wE7KZEQRIwqpjAdfxpoERXrgr7NKeb6iZkkLGy6enUg+CPle24lB3LQZlAw87GVtZUoR+LfhoQzPGn/jJtAmiAFBxmZdC+y6KhCRHfo1VeOIsKi0sOlBiXVN4QQJJAeVWX8MVt35sJC8NiowlKP/CS6F414QJIAJBscQwdCkMe2kg7ZG8Xh59TlISYNoTQeRElolqKqSiqALoK2G2Mqk7zL1fMxf95kEZDUnQmm1E27pirAFp4mjISwuwATZuIU2HU22xTaK435KsNeKCWgPqPBxWhDGNZ5raoHWtmiChnVGFmd/iF9voKdehqYxSY8udQPlDwAwELRJgfDCLl6O0EuRmBWumjXcUhVijhV0ZSsmRwLfQnMwZAmTExrJ3KtWQGpHH8wqkAKaJGn4ffUxjsoBW1gXObKk2CQCVbqOFBsr9OSpfYGmQQWoDAhZwbTJh2byXlODmeKPqxtoQ12bAhBqUngsdby20TwuHTp0pG7y8v77USWwNIUqF+kDg4RvXh1+E9qElqApkRM3qR0VEnZxGyNm7RpjlGug6ipaOgGExmDBzXdMm1BvcWMQfyPB2XymjDNJaW7Rhk6LkVdKb6fgO4Iq4K91esWBaNkoy5M1eGYvuCVF/POeWJc2g6vVhkQZkSyoct5cojDWEj7o0D9YuSchESJfWnTK0pIizHNSUbbG9JLaogQOrQE60dio0ldc2NbbmajTUOQUsxpVK4otfLKhO47AYoaZlKK4uPyRoKKQVNEj+spdmaNBwzbtxxwc5/SIs+Stu36ldIGzZJ+FmVJghRn0WApUQOrxC3GiNhSh7rqWkVynHq0Ayyo7jn+ee/e/b2XL196BrMW3jhAn8J6RAvqXWCSCupLS0uPhfv3LCws0MrKCtnGVzSEuEFZJab0HIEYfWn0YKmOb/SMiYgXs2QvsQhKjdT56ViBS7qNWCnEjQ3IAGvPpn2VTN4cOoSQkE6QTBI0Mky6h0W3HNS1CKJaCb72sLEsSqYVtHiZUKMQo9dSkcMaxXBhaAiA71rsAIKuokj+t6gPND+fQcQKahpKndEREzYD8xDtab1wImwTNrr2Qm7SRmwHhxRoEohIWaSqPw0GcwEg5hQwlG+pQ9qWdAQyvo1q9V1oQhDVXch6eNORlKyjEy0Qhx2FTFbrnctjoV43vzBPc4PBjrt3l3d70cZGBZENI2lrW+wwEoGfC9u37zjw4osv0uLiojFhuLjZBLqaIlAR+qnlzJV5LNl9M3NQbyXt00uJQkTNlJBK9WQV3oq6uvAbRS8RjZjY5tvW6Kl9K+wUpL1SpM1Oa5Bkc32kfHiukgr4HKkeLzmGYbfS75YI1PurjLpYRwGuUqVSdCSFzRcQ16jCr4XprrNUkFvpqXPH1UjHZ0cqPed9XagCr7venCZw1vGkdHzr7DUv5PU70oOAZKIn0Q6GWOljqadhvM9AulQVq89fVIJMGvBYoOPHX6TtO3Y8Nmj2ysYsmG/YInpXbcTOh8S85tatW+Tw4cO0f/9+ev31v6alpVuV90uVyROq69xQqKvJjZzohkgrieuEvk5UQQ6ZRmQZaVVMA10wYjZn+ltd3tevVXO6aqjRGfmuK2BbGTAw6k0riA0YXONUE6HU9PPstBdwB5zUZ69fhavzxZhBKooWn1/F+uz1/Ib9DlQqrLrSDrUk5/W7zkbciEcq0M+fUJsCdedZYP0VanRTI0IuLAfG6moEOdfmfUv6GTbiZZM9IGelpH/v2LGdXnnlW/Tkk0/S1atX4zW7YlEbgThxQwOIJTVDEIFOisHmzVtkz549dODAATp06BD95V/+JZ0585HjLWJtgq0ILpGzIWuyCckzI0K1wIVUZgSBcAQpLM/bLiYg1juGjS615AJ9k4LgksIS885EtpfLN6V2XkIqQ1wDBE3wvdkBrKqcafizOo2zoHQwd56Fa4oq1ahOp903bprRT83zWKBix3MWZxRQJkR2pOjtuf70rZCTeg6Zio59dR/CB42c7aB1YvyJcJsyVbWcfBupqpgl+p8EHhq42Dlfrlwjcb855L4Zv87Bg4fod3/3d2nfvn1NSvv27dsuuQ7qgmw0UNlwEQh+qRZU4m1xcZG3b99OW7dubRZOBJGf/vSvwu1nzSLiKl/v8oUSUoZaihGtiS4zedV2wY+7sLTRqBgQ0wYejKeBx9PCKCg1oLm5ceFzNFqD9BQw8JKhSOlICVRmWowxxfZRNcDABpTrQkXVvcXSbUxy9xjV2uZEWsO8YgWGx1eUrF36qJ5BN3Uetp6ukS12gJ+V3vqk1GAZoBBPq570erM0z7ZBo/5S2fn+uW3KGLQUOX4nXYlS2MD2+IhdhAIDhch0LJOu1aUJFi2N0vkdSbXGbfC2sDBP3/k736G/+93vNnYgnufy8jJt3rx5tG3btltWidCCCN7fA8gjABZdX7J9/KZNi/fm5+dly5YtIXTd0dRB4uL5wQ/+IT3//Av053/+b+n8+Qvd/pFH48mOopyhrtCbggu9teOJ5yytF4HUXOb5DZpefVVXaA1V3OA0KJ4nDG9p7iNNUdu1ycXLnngMhlb4SQFnh3HulCLsEBAhMyzGTobH/Q4I5m9Kx9Z06CCTRvOgv5aDrXV/BYCDfFZIV1e+y1eXmkLXEyYhKuSILnCxeswgtVrZj73ifqvTa5i+ikSMwuTXhVzefXuRqF8/KekJn689pfZ5jz/+BH3/+9+nWAeNdiBeZ3Qc43kGwBwGQLlsgcLeNlIUMv8og4eVnZz0xce/bd267Up4TmTXDGCyqfE+4u9ra2v0jW98g44cOUKvvvoqvfHGG3Tr1hJVot1WnIDE2UjGi62U86QrSa1a4t3cMRtDIlzYeKtp7taL5DHVSXUebLpTKvU/p9tH6jmwiu4V97ExomIYarXxF0eykFQqpmL3xYiMuQOQqKYSzhQyhetbQHZW3IgAjBxyirmCLUb5yCrkVdkrT11Luj10L0ljgdwLqEDO1tO1we9lEPbFYG56D44fe1BuVc/twm6KFyKjDmbm6tIdARbNoSleaYi279hOX//6N+gP/uAPmsxDiDSavT8cDps9FH8fl3M4S9eijK2NRrzopAeQLzmg4Bdvb/FvweO4HkDjYlg0+8eh7EIMW5sFFL2R+Psf/uEf0iuvvEI/+clP6J13fkl3797V6hXcJThBVCn4kEzeFMXdI9t7aNt43bwwC3RgidJtSOmtaAR4wNM3ZiWrKO5jpctIV6/pRCUuQHi0gMbA2Pdlx1B63qirWsR+lGPB2QOEaubA8/QdAQ6uq2WuNkzX51lFZ8Y5mVCtqZNvdZCsbbtkoaimjZcdqT+g7+UkOiUwoW4jEJnyOU35+l1VM5S2nbCutwQn8cSJE/Td736Xnn32WYrp6wgeMfsQz+3evXvN4yKQbNmy9XKwAXciBxaCh7UjG6mgPv8oAkWqadgoo+tLT8Ro4bby3HPP/a/Xrl37lzt37tyUvI+4mNItgkiMTA4ePEjvvfcevf7663Ty5Lt0985ybTi6wm0vRyt12idzOTlRSenCsiksVMkh8G7ZzW+XVAR1ULLiqYrOnnSmqeprrz8OmQ405Djspohe9w5wx/lpskL9XFFsvuLNzVdqRGIEsKx+OnBxTXIOrIPBk7xr6tRJcegPu7t8rWiTh+1d5QdOLM/j+lmRDB7o72RQnBRc96iPI1aRUH3eiWW5aLx4feNKeZI1yLnX2b731i1b6dixY/Rbv/VbdPz48QY44i06iqmFP6auog24f/8+3bp1636wDf9bsBH3jc0YdkUjXlTSA8iXOPrwwCMugNjCm37fu3fvXy8tLf2rsGD+WVpMeIuprXh/9FLigjt69CidPXuWfv7GG3Q6AMq1a1cjKaPrMRonTm8Ih2yLJ3if49zxqJkyL5xGxnBIIbwbiZ9baH40k8JlXkTMe9cY4ZX4u7zXwj7bGZl1Rmycp7nzNL90pGKoSNAyRiFcG0fJg5ViUvZtmo/FPzXv/g6de/KkdrkDP7ijdEAd+JLXQHlPdkBSvMwdfghkZJaJyO04r+pwNJ4654FpaK+L+JyHV+33IVkUzaZi2ei8eGk5npRhcx4UXzNmFPbs3UsvvPA8ffObr9DTTz9NwVFs9nJ0CuPenp+fz3WPeIsZhtu3b8f7/9W+ffv+Q/i5loADwcNGIH0R/REHj7QAYgtv+pnaeUN08adnzpw5GhbUt+OCigsv5kAjgMR/p0gkAkkEkV27dlHwTujmzZv05ptv0unTp+mjjz4Ki+9OJoxDqVjXy6vQxYnqsT6BRIrG+CiDb2Vq7eOa/O5AzS4oaWzqqPt7O1j8tIrKfklH9od0uYOU4p2e7ifxc/RcpQs7vHbuSB96EYDhrGLxzpM68/TZeaaOEggZvXlPXlUHWKp53F4bd2Fyxcevr7lSMTT66snLzxFIrAkMGCT+aMKQiTNTI+TPgaivzWW2VHpuLpogiIe1HaONWL+MEcfLL79Mu3fvzqAR93AEjrin4x6P5xKBI6auInAEZzLu69fD8/8U7Ya9bVQQ2TBdWAgiKepIwBG9CmTVDAtq6dChQ//y/Pnz/0N4zIlxAW3M6ZOikAgqKRqJIW4EkujNxAHE3/md36Ff/epX9Mknn9CFCxea282lm3Q/LMoYnXRpQFi3lsWq8UENME+gS/HwWeNQSl3xmE2RJNIUGTaOdF2oZ53lWXPbrFNOYN/2ChvRJHI2vPKiC+mjKy0qWn9E9dyy1rcQpCpndtM3qqkHlRc9O8cgI5upWmAQku1nbcWYEPxKDSpXPFK6jUEBj025TExARlYx3nyWpOVVqmxf5kozwZwFRQ8MAOTmG2PbvsCAkDpKl5O4VQyx3baqhgcqgiANXOW8TLmFSEde8deF+XHdcsfOHfTkk081A4BPPfVUkymIezXd4mMQOOJ5Rmcv7s/Ysnvr1i26ceMGXbly5d1oC6JNsNEHggfWRSZ1ZfUA8iWrg6T7UGYyfeE4SBgXB06nh0V2MSyc/zaAyJ+Ep38leUrRc4mLLy46jEaiN5M8lwgocRjxpZdeau6LE6zXrl+jS59epI8/+Tgsyqthkd4Nj11uHr82HNHaympk+8z533jmMTKYm5tvuqQa0JqPoBWioIVFOnP27JiNt0lhGaMELL/N6Acn9lFDyS7j0ZB4Hc0v6f3zhmfocGIzIlgowqXiFKonzcu8hWZBVeokrPm8iIz+OReOJgZlvtL8BrKzVN4HDWqRP9Fa5JmuXhkk4EVqn8zQzVZmSQzFORmjnNuzIarDhgYUeVLhiKmHI6gw1+zDrKlwCgeXjiKSBgmzdISuVPV6YL0nNl4M5uea9ZkjbNQ2MUZfGLQ1YAA0pbDi+j58+Gkarq3RyuoK3Y/po7AvItFirEGMRkOQXR7XX6IDF39uajMBW7duoX379tPBg2PQ2L17D+3duzcDRfqZbvH5KV0l7fvEfXvnzp0mk3Dt2jW6fPnyO8EG/PPw+CvRXkQb4d28VJaXBekB5EsIIgk8rOB9AhAED+dlrhw8ePCPQiTxx+Fx34yLPeVs44JMqa2U3kpprejFxFvKo8aFnH5PHk5cqEu3lmjp5lLOtcYFjEda+LGGsTm8bny/+N7x+T/72c9yHrkUa1lLcrYu4YASiHCjvSA8AEamAFKDcYuijLTBZsgrYcctC9dt+lSnYCrIYdZa04TeN0OhGqarUSJWCjDkyEAwSuBc30Dvu6TzUJYVdM65CHiVMhKIORkGMRT14qSHnms0bd0maclwqeLWcsU6R4/XYmmvGOQFVK1E1VlQApaVOmAJIUpHXtJjLyzMHV3bFZ6FSDw4M5l5Ob7HgCEF1j56AGy5+Xvnwqzc1vrmw2t973vfa5yzeF7jPXK3GZS9d/9eE7njxUYnauvW2Cm1EKL+XW0dY2vYK7pmGfeL/RlBIwFHPOJ+jm36KWWVwCNEHj8P4PHfh+ddR7AIr7GKwIFRiG3U2QhRyCOvB2K9gPhlp793sWri/eE519pI5J+Hhfa9uNjiAo+Dhil/mqKRVC+JizJ6NPGx6Rafg/+Of8ebmsqlwlM1aIvkuMivX7/eLv443CSKqZSscQKK7ua+AStBJW4inTEIjoajqnBedBM06aEoiVkBtlf0tFnrdVsnFzxpr7aQRyelRAco8oOKh17DgmoAIFIRBhZdMHWk8y9A3U9msLKaXSziWem6rMYXp0hHUFmQVYpKpbiURDEroMYxI90gxlb0t4C+KW6xacpgqw0D+Tn4KJoIZC5EIPk7gTwXG4lkLO4w0PdwjkDGLxydrFibiPsqOk3JyE/aF3HNxp/xsbgHcS/i/c3k/GDcHpb2XXTaUsoq1jsieISfPwl7/k8CeCx5EUcEkQQkNp3lAcajrIu+YQYJEURSmBkXgAUOr/ge/nkrLKh/cfHixf8yRCP/NCy6TTFqiIs9dW+kAnta2ClaSOBgwcL+tAXFTILYHilKie/RdHmV8865aAHvWbKnWbzElKvOMrcpQpsbtK28rXHDUfSKJhGEfUBhMNkeVf9mTThIZOb7wNCl4bxM+Q55p5yeQgPZptgw6rESpGwKN1mdMYsyAVeuytFD7oXFCGVhZZyVhjy3sqnKJ0mGmTzlQK7qDswIIToCFLdOLEoy1mtmUKy6WGPA7i+hSm9cN2yl9Ce3FCaD8tkNUsF7DHQDAJSGL0u12xbBM6yBxBpiBJDYkBL3FLJhe/sigQgCSXKE0i3dl2tP7etEZyzupwgecU9F8IiRR4g67ofH/p+HDx/+38M+u+cBhwcoXvG8HyR8RLuxUn4yRSJYC7GFd/vcJ5988n8O3skbFy5c+K/D4jsWI4IEIinHmsJkXNx6aGqc902g4YEHvG9ziyATF3yajI3pr2qQUJDgsCiwJSM30rNVudbCLaFifG0SMG7Y+sT4epTZhd3haNVhy2pwTOt0c53myZVohzqc/JETMdFMrdSCReSUHhMAZ1GFfM4SsMVgEmudEDuDklNiAmqP0NYkGJUYiV1bDEeJW5xLybP/nCK+8llVZCPY5cWW7RkixJQysyJV8L0XhctyjjHl1HjyzPD5cdUpxkavXks+6TWfWuPjfopggl1RXfsiN4AAmODfUgSX9lvKBETgiHs3gUcbeZzes2fP/7Rz587XU70j/UzA4aWvsIDeDxI+wlEIRiOpoD5L51Z8bBScSv8OHtJfh8V++vr16/9ZMOT/Rbhtjos+Ln5sCcT6CHZxKXZS24Vifk+PjaARnxtBJIXrdmMxtO/gewzSoGBsw0oa4EmudNDWBJpC+lypEcCsBOqb51QEpEDQO1edZUyqplFPeKPHi0VotMpW550qaVfVGoSaKYqZNtVF0glDxKMAALrHmBWNWbkUgfkL1t1JWLxXRp7I9hsXtpvSvoW6LQUEOH+nqZBeNOoLbxVS9hOqSXKhTGFhlSbTrds62rQMMHgJyUHK8scDeBsINzitoQGDSmWJJrNDFRygFLWnWmKayejaE6oRgWuOLRv1p5pkSllFJ6wFj3vhWv40OIf/T3jPG1gsj78jaMTf7d+7wGMjRB8bKgLxCuuzAIg3zR5u1w8cOPB/hIX472/cuPFfhfD3twOwbMWBJCzk2dDaek7oMXlhe0pzYW0lRS7MOl2Su5BQi5p1eylqH3Bb/IxepUAEwigLy1ypKyj2VYwIMKJg8GqV114GQ6qoQoqXrBI+ynhDSgqBCmnPBcSdUL9dDeoneg3R+vBimb50Wg1psrQyoGkjFqr05gtRISipSDUCNI6AoLEga8rY7xy69krBX3KBPKXX7HyFSFFczMV57OYSU3+C7zsWr+N8xWhtmGtrAvM5KBLIioMtRZ6snKdYe8P0brylveGlc3FvpMen/WBri9jMklJWbafV3fD013bv3v2/BMfvDNYxurqtMCrpAo+NpguyIQYJu6KQrjTXpJsZRvwgAMZ/d/v27a+E2/dDGPwfhxB4b4pEoieVaiMptWXBJOVqLYhg6J3qH+kWPagIIriZRBcMiLL5HFNul6SCgLGhLE06lztTin4FZ86t8rxiLIrnjDrVxRCyEW5iVT2vmNyzF922ekKklL1wQolaAxpcjFVKG+FcAQ7561qybntlNsJNKGuba0Sa3LZow0NIJqVWpOYnxZC+sJ3aK3mwnO5KzQNo9FGiK9duWHfGZaeBwaBjt14LJ5p/paRAWZRE8fi1xkXr+F7Rvpc5HVZtvAzvPWBQPEHHpj2pteFas6bj2o57J0Uece9gakqtdyfKSDdsWsHW+vgeIeK4Fl73L8M+/fPt27e/YweLp7XrpppH1/DgRoo+NkwE4hXUE4h4+iCTqE/iAGJMaaVhxNgGHKKPt3bs2PHLsAH+77BIT3z66ae/Hx771QAuWwOIzKfaSLqlrpDUIZJAJHlcCCDYn56KfbET65VXXrnw+uuvHwjvNyeic+mYMh6/ZNyAKYVVtKqbZ4zGhrsxCtEgDEkZPjVADoz0eqjNdA8Rm9bTtqgPKZVUcygE9dYIk4o8cKAxz8pkj1ZPXKJ0LoPgB4MmN6JIjrTS+3OpH+n+NigNYeFayoyKboXFVmAjvWXoSEqtAUAaI0usJWAXFaRzkDGYAOhtVFmAL30+rPXL00vA/SndtrC40ESr6YsYDCB6iQX2tlsjzXs0vRkD3SPMWZJZImAMv/mNb14Ka/vJpmU9LNhU80sdjjb1a6Nx7HhMLfRQ61gL+/JuePwvA2D8xf79+98N2/JDy0qRmmosgNiIA4EDu65s9LFRIhGu5SQf3QPrHtiuixrpSd4WbxEwvJ8JQOwtLN6t4TF7wm1n8Hi+H/79UliE28OC2xM2zJbgAQ0SkGBkghsFNwgCyPXr12+H833r+eef/x9PnTp18I//+I//Rdig2/bv2zfAwnVK3D995EgzgCjtkIdqvW2NYKyRxAGuTy9epJX7K5jgykAgxFoXCrxlASPXpWKYU2JQVLdlcgUmjK2lorxqxb+EQkrkKdqhKBbn1BDWT1RqDNpsxdQNLN9Y9uBBXz5Z3xxFocZFNugAqA69eq7J5OtFgSbJ1Pw5NSioN18+7xK9IfiJLrMYMaZSMtHzOCkXuGPbNtp/4DHVa6U7kvH7kGZt37t3n86eO6OGXK9cvhLrkHf+6I/+6E+OHTt27uzZs/9NeOzX9uzZsy3VFFMdJO0N61hhpNGmqkYBK5bD36+H+26H574TnLt/E37GdtzrYa/dnUCkOrSA4bXpdlGWbETw2HAAsl4QsWCCwIF/x58okQs/58NtMSzuo2GRHwv37Q1vvz3c9obfnwwLbk9Y3DvbnvYt8dzChlgOt9XwunHo43J4zNnw2Esh9P5p8KROR3GbuFA//PDD51999dV/fOrUye9du3rtCTENVE8feXosjDUcYt5KRwCx02ttSJcvXaQ7d5dzgdiyhhAWikG5nLKRBxOGtB1VygWLzyb9BvCgZ1HASDIUqiFNlrt8sADOugieGw1UhQUG/xxpYmITdYlU74mFadVHxcDSi6BUEkWgkCiKy0RPdCAvCpveLP3O2PcrxKbpAFOCJoWFLeDIGgBKkJFhYf/e/e10eHvuwp5kVPPv6CAt37tH586cadeU0N69+y6+8MILf/V7v/d7f/bMM8980DpMc8FJOhpufyesx8fCXYfjz7Afdi+Mj2ZfhD243FKOLIWfN8I+OB/uvhZut+PMVljrp0OE8V64fyXc1rzhPow68KcFkq5Oqx48NjCAzAIi6acFEQso9jHxeQgk+JrpvcLfF8PP+Ni5EFk8EW7Phs1wMNwOtSmyJ+J3EhbsxfDv22HjnA2b4lzwyE6F++5FOmnvmj766KMTP/zhD//Zz3/+xt8LYfv25LUfOfIMLW5aHA8JGjOHOiHxX5cvXW6FsqwxhqJ5Aou25qHqI4pYCYAhAUBKjQFFt1hOKtM5RIqupEpOQQQCZX0Ww08FZr0ihgIwBQOuOphQnRBmRcr1QEoog3eZTldpKoImXpRwVVGCroektlpbm8BzRA4y8TomKqVx7GZjJXpYq0q2w39hFR848Djt2rmrqVuoEET3duXf5xfm6N7yvYaxevPmzbdffvnlv/r93/9P/q+nn376lGd4w/rfFPbLpuBsHYt7IgDG4XA92+K+aDvAPg1/57AvPg6/nw+v+UGIVD6NGBQBI0oyYJ3T48FLA8VdAGKp2r1J8x48NjCAWCDBORAvisBUlU1ddaWxEEQQQPC9ulqJ17MY8bXChtt06uTJV3784x//5++++863762sbH32yBGaX1wgGYLRclSfFubn6crVq3T1yhXwUkmnSDDSMBrblemwcxqEHWJiurdwHoEqIkI01oVAUYdHrCv8MCMBBB9spvDBUCswqChxdbtwmSTHaxUVRNW8kVwoysVWVqhEKphAFBMNEFdNC4JDHyaRpenh4dEZlCXXUwgG9UiBfAHgxYVFeuKJJ2jTls1jh6RLVwwua3FxU9xT9zZtWnz9e9/73r8+euzYG+F17k8auJt17U/TI0dj3yUkZ2sa9t9e9ILvtVGBowcQxwBbI48gYm/TgMOCiAceHo3KtAXpnae9hahmy1tvvfV3f/KTn/yju8t3vrMwvxDOd5SJ75rWXSnT6nEJRACJ7KOXLl0a00vk2QTWU+WQ+kD3W4y3W7p/NFUtDskJaHyIEy0QFNVzVKGcY9ba5dBZhkV2An4oLTlsazUY4RRjipS7otiSdTuzIN8HjtwT6IyrDJ8ZsGN4LZtqyzUhUtPcpXCBeAp9ZamDiwsVsSrom7Sd+kwYu6ao4ZqKQmrNWJHoBFpOxbWF9aZmITR66uDBX7z88tf+3+eee/7VECkse+JuD+LRe4O+FkQseMwKJDZV5bXp9uDRA8hMIGLrI12A0gUcFkS8SGQagMyacrPvd+fOnZ3nzp37+3/zN3/zg4sXPz0+rsVIARIwJJHOJLY5Xjh/oW0Rxu4g6O+3Rhjp5nHim0SPJUuZC5EuOW+p+csLcEAqzDN2OBcC2h2CPPXmaaUsUIrRKrslpvCtajAEZIQQ+KTBPlWjIbLzM5gi4op2sj5JNq0NmIISZe3N5y3Yfs0KlMiAjCr2Q3SZZkF27NjeUKPH6EPYFJbCv+civcm4m3Bt7959p19++eU/e/7553+6devWG54B9/49i2Ge1DlpQcSLQqZJW89KTbLRwaMHEAdIZjHWHqB0GfGUvkr3dQHWemo2HlhNSKHFWsvWN9968wfvn37v9wOQnAj3bVprIpJh26PfhCU0HK41dPMNK7BYs2fahKi4zMy68waLv7Yn1SSVnIE9hCDWHjYa9ursNI09AgEWrcWOy5uowxp3m+LPdSFVr0EMdKIcBAo9vl/ab4kqjY6qNRe6v6QLgDEqQMaADj10deVAp6+Bavz7/scO0L59e5sOqIxZURRzjml+rI9+f9/+x04dPXr0xwE8/s3i4uLdSRLSk/TEPePcdZ9n2FOqyQJI10/vcV1RUQ8cPYDMZKgnRSVeobzrMV31D+/9Zk1dTUulpaI+gkm4b/6dd975T8+ePfPbH3zwwX80HI62x2JoSsXEtMPHH3/czJmgacXihJC49lXq8mzHvxFkvFczGiBVch00T/DBeY6E8vV4XUrIFqjhpz4bVHHESym2WvXDVlERq/ZdNNmaLrEqQrN5LSFF015SgZp8zOsLy58BSh+K/ht2ulU1svY6Y1rq0KHDDdOCBMcjPSTq1czPz91+5plnf3rkyNOvHT/+4o8COKxNAg1Lgd4lxjRL/WNS9IFg4qW1Jv17UrTRA0gPIOsGkml1h2m1Dq94PiuAdNVnutqOu8AlPW9tbW3hwoULJ86ePfvbP//5G/9kdXVtu8iI4zzIhU8/pes3rpPufe3MOek0SyU92+UudxkFf1Jc3LqFf07YUdWVvSInRVUTv9fpIoNjzsvWIFWfsWa1rAMVqx1emhmkzq2ZyEUX1C3pjOEzMa3JVp1Kp6g2bV6kpw8fGSsRUjM7JHPz83deeumlf/3ss8/+7Iknnjg5Pz+/OitwzCIF+yBpLK8O0lUbmXSb9D790QPIukBkUkQyKbroeuwk0Jh0n9cpNqndeFKEkp4fW35DVPIP3n777T+8ffvWUzeuX1/85Pz5Vqehw/hURsxQndQm09RFnLSQSuewa3x9g2hf13Ql8SzYwxOuVWfssG6Cmh1uPabKS5kLdq6TnIfThHO3pMlW0rij6GJwHSIw9iItahT+otof82Bl27ZtF772tZf+7MSJr/xw8+bNS6i14w3o2d+RAn1S9LGeNNasNZFJ/55U3+jBoweQzwwiXRHJpLpG1+NnSVtNi4AmDT52AYpNb+FrnTp18g9On37vu6+//vpXLl++9FSX8z51UekETvHagSSxhgCmSWXnzEbrJH9syoamRh5TEnATQ5fpF48gqrvHTHQhk/Grqom4T/MbBLw6T/2z/i66fj967OiV48defDtEG//+2LFjf9FVkJ40X+FFHWkuo4uQcBbDPalG0QUMHnisB7T6oweQhw4m09Jfk6KbxNe13jTWtFmVaWBiCv9z4S3mz58/f+iTTz557uTJk9/+xS9+8fdu3769e6oxtVmPjvQXV3MoJk1k6gW1NZ0cPKiJ9akAMBtIeIZ6aiBjz8HrUlsXEPvP4XpWsKPmIhMuu44w47vOLyysnDhx4rVvfevbf/HMM89cfOyxx94Pf4ytuKNZUlVd/542nDdL+srjtpsl9TRrTaMHjR5Afu3prfXUUj5LTWZSJDJrSmtaaqsdTFy8devW7gAoz/+7f/fjf/zpp58eWVq6tW91bWURWEE+v8XplCnMzHkFEnUnk63L+MUXnOmYqVCzrmtgp9A+e6QzC1itAw8nvuCOHTuu79y589p3vvM7//arX/3q/xd/37Jly51Z5iim1TomdV49jMnu9dYu+jRVDyBfuKhkPaDyIO/1oAOP66mPeEOQkQcs3BZ/9rOf/eDDDz/8yieffPzChQsXnh2PW8iMVmqKhZvqGdO05M0E28xuumbqec3wp/VDwOTPZj3JM+5ICM5udAeyZ8/ui0eOHDkZIoxPvv3t3/rh448/fjbWKbxW11mmuKeBxuc5oDcrWPTA0QPIFwpI0iJcz0zHZwGsrs6vLjDwurNmoWTpmqq/ePHTp9977/2vx+jk5Ml3vx0ilT3Ly/eabq7PK200+VXqkGOdwcIDeu6f6/Y0ualZPq/Jj4/rNEQUt3fs3HH9xWPH/+aJJ5/6MPJSRQCZNr3t1S+m0YB0gcfDjDzWAwa4T3vg6AHkSx2lfNbXmdRC7EUlFiQ8AEFCyEnpLZwtuXTp0qHl5bvbf/WrD752+vSpVyKYXLx48XAcYHwwcziDrZ4xaLAgMu11u4OgXxd6ON1q0tXTVrfm4rmmf27atOluiCrOxfTU0aNH33j++eff2rp1260YcaSZDW/YbtrgnxdldKWpJgHH51mD6EGiB5ANCSq48GftBrNtvtPSW+vh95oEJpaH6969e1vff//9l8+c+ejE0tLS3nPnzh2PIDMKYEP0G3Di1wEgv/moYz2n4V/c3Nz82oEDj3186NDh07t27bx65Mgz70bQ2Lx5813ko5o0lT3r9Pi0SMOCU1rb6+1+Wu+e6Y8eQPpjxlRWF5BMo19Zz81Otk9Kb+G/4/MCeBy+fPnSobt3l7fH1NfJk6e+vbR0c3+sq9y9e3dH1ILov9X1H1ETJqajFhYWVnfu3HXlxRePv37gwOPn4n0x4ggRxsepljELaDwI2eA0HilU/ewH9XoA6Y8vEIhg6++k+ZQuLq9JgDBrFGJnSrooXdK5xMffuXNnVwCO7SFS2fbee+99I0Qox27dWtoTH3f9+o0DN25cPxCn5ftvuRxxynv37j2X9uzZfSka5R07dl4/dOjge0ePHvt5BIzYKbV9+/Yb0yg6JoFGGga02hn291lSVB63VVc7bV+P6AGkP76AUck06nckepyVoHFSBDItvYX34XklepVYT7l8+fLB8+fPP3f16tUn1tZWNy0vL2/75JPzz4XI5UgEnodVU/rCbshgSLdt23bz8cefOHPw4FMfRGCYn19Y2bdv34Wnnnrqg1S3SLQh1rN/UOCwgNAFGJNICdfDXtsDRg8g/fElBRJMb3WlujwAmQYY01iJpzEUd0VON2/e3B9u+2J9ZXV1ZVNcsgFYtp89e+bF8+cvPHv16pUnw98fS68L1zRnP5NfNxikVBMazGhwd+3adXnfvv0BFJ788Omnj5yMUUQs+C8sLN6PdYrw96s7d+68OikFZGsLXYyys6SspqWyul43ncckoaUeOHoA6Y9HAEhmiUqmdXN10ddPum8agHhNAF2pOLyOlZWVTbHTK9w2RzVG1FO5ffv2rhjJ3Lu3vC0OQMYZlsuXrzwVopooKRw5wLY+rNpLBIhYoI4T2jFaeOyx/edDhLDyxBNPnNm8ecudGDls3779ZryuZGAXFhYiUNyLHVKLi4v3PYPrKex1RRxdoOEBQ1etYj305tOijR44egDpj0cEQLz7p6W5PAM/KbKYFH3MEpXMotw4Cx1+l8bKgwx9rifKmHb/NL6maeAxyXB76apJKaxpEcqsbbezAEcPID2A9McjCCjev6dR0HdFDrOAxaRC/oNS4k+KrGa97gcFkfVSjXvg0AUg66Uin2b4Zyl0e8+ZdD7TQKMHjB5A+mMDRibWYOPfbGppWtrrQf426X0mndsslPmz0ud/ViBZD4DMGnFgCmua4X+Qv62X7ryPNPqjB5AeSKYCybR01yyA8jBu6wWP9RJZPqwIZFYW2M+atnpYt673neVaeuDojx5A+uOB2ISnRSyzpKJmfUxXim0aeDxIKuthRSKzalLM+vt6wWWW505Lq60XMPujB5D+6I8HUkqcNSqYBgSfF2h8EQDkYYPJpL/PEu2sN8roAaM/egDpj3UDiSd2NYvk72cFGi/imRU8Jt3/6wCQLiP9ICAyS9Qwa71iVrlY+533wNEfPYD0x0OPTh6kljLJ+D+INPCDRCCfFVBmpQ2fZsg9gJgGMrNGEg9St+iBoj96AOmP31iq62GAyiy/z5KemiYV/LCikEnpHuR5Wk+dZL2/f9b0Uw8c/dEDSH98YQGly5Cvp07xsOc7Pm8AWY8xX08Be5bIYhJBYQ8W/dEDSH88sqAyC2B8FnD4vLixHtTDX88sSQ8W/dEDSH/0YDLlcdPA5WGDxMOugTwscJkFJHrQ6I8eQPqjB5Vf43MfZiTyWQ3256kB3h/90QNIf/TA8hsCh4e20X6DINMf/dEDSH/0R3/0R398oY7/X4ABAIL5vD7J2Ha3AAAAAElFTkSuQmCC"


    };

}]);

/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Global Functions that will need to be called regularly from several places
 *
 */

schoolMonitor.service('scGlobalService',['$rootScope','$http','$interval',
    'app_name_short','foursquare_client_id', 'foursquare_client_secret',
    'scUser','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool','Pusher',
    '$state','scCircuitSupervisor', 'scHeadTeacher', 'scReportGenerator',
    //'regions', 'districts', 'circuits', 'schools', 'allHeadteachers', 'allSupervisors','$state',

    function ($rootScope, $http, $interval,
              app_name_short, foursquare_client_id, foursquare_client_secret,
              scUser, scNotifier, scRegion, scDistrict, scCircuit, scSchool, Pusher,
              $state, scCircuitSupervisor, scHeadTeacher, scReportGenerator) {


        // Check to make sure the cache doesn't already exist
        //if (!CacheFactory.get('publicSMSCache')) {
        //    var publicSMSCache = CacheFactory('publicSMSCache', {
        //        maxAge: 24 * 60 * 60 * 60 * 1000, // 15minutes
        //        deleteOnExpire: 'aggressive',
        //        onExpire: function (key, value) {
        //            $http.get(key).success(function (data) {
        //                publicSMSCache.put(key, data);
        //            });
        //        }
        //    });
        //}


        //This counter will be incremented to know if everything has been loaded
        var modelLoadingCounter = 0;


        this.getLoadingCounter = function () {
            return modelLoadingCounter;
        };

        this.incrementLoadingCounter = function () {
            return ++modelLoadingCounter;
        };

        var that = this;
        /*----------------** Lookup ------------------------*/
        //Lookup is an object that uses the id of the region as a key to find that region,
        //without using a for loop when we want to determine which region something belongs to

        this.loadAllModules = function () {
            scRegion.getAllRegions().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});
            });

            scDistrict.getAllDistricts().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});
            });

            scCircuit.getAllCircuits().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});

            });
            scSchool.getAllSchools().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});

            });
            scCircuitSupervisor.getAllSupervisors().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});

            });
            scHeadTeacher.getAllHeadteachers().then(function () {
                $rootScope.$broadcast('updateLoadedModelsFromRefresh', {counter : that.incrementLoadingCounter()});
            });
        };


        this.initModules = function ($scope) {
            scRegion.init();
            $scope.regionLookup = scRegion.regionLookup;
            $scope.regions = scRegion.regions;

            scDistrict.init();
            $scope.districtLookup = scDistrict.districtLookup;
            $scope.districts = scDistrict.districts;

            scCircuit.init();
            $scope.circuitLookup = scCircuit.circuitLookup;
            $scope.circuits = scCircuit.circuits;


            scCircuitSupervisor.init();

            scHeadTeacher.init();

            scSchool.init();
            $scope.schoolLookup = scSchool.schoolLookup;
            $scope.schools = scSchool.schools;
            //$rootScope.schools = scSchool.schools;

            if (scUser.currentUser.level == "National") {
                $rootScope.adminRight = 5;
                $rootScope.currentUser.geography = 'Ghana';
            }else if (scUser.currentUser.level == "Regional" ) {
                $rootScope.adminRight = 4;
                $rootScope.currentUser.geography = scRegion.regionLookup[scUser.currentUser.level_id].name;
            }else if (scUser.currentUser.level == "District" ) {
                $rootScope.adminRight = 3;
                $rootScope.currentUser.geography = scDistrict.districtLookup[scUser.currentUser.level_id].name;
            }else if (scUser.currentUser.level == "Circuit" ) {
                $rootScope.adminRight = 2;
                $rootScope.currentUser.geography = scCircuit.circuitLookup[scUser.currentUser.level_id].name;
            }else if (scUser.currentUser.level == "School" ) {
                $rootScope.adminRight = 1;
                $rootScope.currentUser.geography = scSchool.schoolLookup[scUser.currentUser.level_id].name;
            }else{
                $rootScope.adminRight = 0;
            }
        };

        this.appendIfNotNull =  function(stringToCheck){
            if (!(stringToCheck == null || stringToCheck == '')) {
                return stringToCheck + ' '
            }else{
                return '';
            }
        };

        this.makeFirstLetterCapital = function(word){
            return word.charAt(0).toUpperCase() + word.substring(1);
        };

        this.drawStatisticsBarChart =  function drawChart(labelsArray, label, chartData, target, canvasId) {
            if (angular.isDefined(scReportGenerator.barChart[target])) {
                scReportGenerator.barChart[target].destroy();
            }

            var ctx = document.getElementById(canvasId).getContext("2d");
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labelsArray,
                    datasets: [{
                        label: label,
                        data: chartData,
                        backgroundColor: [
                            pattern.draw('square', 'rgba(255, 99, 132, 0.75)'),
                            pattern.draw('circle', 'rgba(54, 162, 235, 0.75)'),
                            pattern.draw('diamond', 'rgba(255, 206, 86, 0.75)'),
                            pattern.draw('zigzag-horizontal', 'rgba(75, 192, 192, 0.75)'),
                            pattern.draw('triangle', 'rgba(153, 102, 255, 0.75)'),
                            pattern.draw('plus', 'rgba(255, 159, 64, 0.75)'),
                            pattern.draw('cross', 'rgba(239, 7, 7, 0.75)'),
                            pattern.draw('dot', 'rgba(31, 29, 171, 0.75)'),
                            pattern.draw('disc', 'rgba(231, 235, 8, 0.75)'),
                            pattern.draw('ring', 'rgba(16, 204, 13, 0.75)'),
                            pattern.draw('weave', 'rgba(119, 9, 146, 0.75)')
                        ]
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: label
                    },
                    legend: {
                        display: false
                        // labels: {
                        //     fontColor: 'rgb(255, 99, 132)'
                        // }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            if (chart){
                chart.update();
            }


            scReportGenerator.barChart[target] = chart;
        };

        this.drawStatisticsBarStackedChart =  function drawChart(labelsArray, labelArray, chartData, target, canvasId) {

            if (angular.isDefined(scReportGenerator.barChart[target])) {
                scReportGenerator.barChart[target].destroy();
            }

            var ctx = document.getElementById(canvasId).getContext("2d");
            var chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labelsArray,
                    datasets: chartData
                },
                options: {
                    title: {
                        display: true,
                        text: labelArray
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
            if (chart) chart.update();
            scReportGenerator.barChart[target] = chart;
        };

        this.drawPieStatisticsChart = function drawChart(labelsArray, label, chartData, target, id) {

            if (angular.isDefined(scReportGenerator.pieChart[target])) {
                scReportGenerator.pieChart[target].destroy();
            }

            var chart = new Chart(document.getElementById(id).getContext("2d"), {
                type: 'pie',
                data: {
                    labels: labelsArray,
                    datasets: [{
                        data: chartData,
                        label: label,
                        backgroundColor: [
                            pattern.draw('weave', 'rgba(75, 192, 192, 0.75)'),
                            pattern.draw('diamond', 'rgba(54, 162, 235, 0.5)'),
                            pattern.draw('zigzag-horizontal', 'rgba(255, 206, 86, 0.5)'),
                            pattern.draw('plus', 'rgba(255, 159, 64, 0.5)'),
                            pattern.draw('dot', 'rgba(31, 29, 171, 0.75)'),
                            pattern.draw('disc', 'rgba(231, 235, 8, 0.75)'),
                            pattern.draw('cross', 'rgba(239, 7, 7, 0.75)'),
                            pattern.draw('ring', 'rgba(16, 204, 13, 0.75)'),
                            pattern.draw('square', 'rgba(75, 192, 192, 0.5)'),
                            pattern.draw('circle', 'rgba(255, 99, 132, 0.5)'),
                            pattern.draw('triangle', 'rgba(153, 102, 255, 0.5)')
                        ]
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: label
                    },
                    responsive: true
                }
            });
            scReportGenerator.pieChart[target] = chart;
            if (chart){
                    chart.update();
            }

        }

    }]);

/**
 * Created by sophismay on 6/17/14.
 */

/**
 *  Service to handle notifications
 *
 *  2 functions:
 *
 *  function notifySuccess:
 *      parameters: (title, message)
 *      returns:    returns toaster notification
 */



// wrapping around toaster.js
schoolMonitor.service('scUserService', ['toaster'])
    .factory('scNotifier', ['toaster', function(scToaster){
     return {
         notifySuccess: function(title, message){
             scToaster.pop('success', title, message);
         },
         notifyFailure: function(title, message){
            scToaster.pop('error', title, message);
         },
         notifyInfo: function(title, message){
            scToaster.pop('note', title, message);
         },
         notifyInfoWithCallback: function(title, message, functionToCall){
            scToaster.pop('note', title, message, 5000, 'trustedHtml', functionToCall);
         }
     }
}]);

// Display an info toast with
//$scope.popToast = function(type, title, html, timeOut, trustedHtml, callback){
//    toaster.pop('success', "title", '<ul><li>Render html</li></ul>', 5000, 'trustedHtml');
//    toaster.pop('error', "title", '<ul><li>Render html</li></ul>', null, 'trustedHtml');
//    toaster.pop('warning', "title", null, null, 'template');
//    toaster.pop('note', "title", "text");
//};

/**
 * Created by Kaygee on 2/07/14.
 */

/***
 *  Service for Global Functions that will need to be called regularly from several places
 *
 */

/*Don't delete. It is used in the app somewhere*/
schoolMonitor.service('scReportGenerator', ['$rootScope',
    'District', 'School','scNotifier', 'scRegion','scDistrict','scCircuit', 'scSchool',
    'coat_of_arms', 'msrc_datauri_logo', 'scGeneralService', 'DataCollectorHolder',
    '$interval', function($rootScope, District, School, scNotifier, scRegion, scDistrict, scCircuit, scSchool,
                          coat_of_arms, msrc_datauri_logo, scGeneralService, DataCollectorHolder) {

        this.barChart = {};
        this.pieChart = {};

        this.loadStats = function (dataParams) {

            //we use week 7 because that is usually the middle of the term when most schools have finished enrollments"
            // dataParams.week_number = 1;
            School.allEnrollmentData(dataParams).$promise
                .then(function (pupilEnrollmentDataLoaded) {
                    $rootScope.$broadcast('pupilEnrollmentDataLoaded', pupilEnrollmentDataLoaded[0].school_enrolment)
                });

            School.allSpecialEnrollmentData(dataParams).$promise
                .then(function (specialPupilEnrollmentDataLoaded) {
                    $rootScope.$broadcast('specialPupilEnrollmentDataLoaded',
                        specialPupilEnrollmentDataLoaded[0].special_school_enrolment)
                });

            delete dataParams.week_number;

            School.allAttendanceData(dataParams).$promise
                .then(function (allPupilAttendanceDataLoaded) {
                    $rootScope.$broadcast('allPupilAttendanceDataLoaded',
                        allPupilAttendanceDataLoaded[0].school_attendance)
                });

            School.allSchoolPupilPerformance(dataParams)
                .$promise.then(function (pupilPerformanceDataLoaded) {
                $rootScope.$broadcast('pupilPerformanceDataLoaded', pupilPerformanceDataLoaded[0].pupils_performance)

            });



            School.allSchoolTeachersPunctuality_LessonPlan(dataParams).$promise
                .then(function (teacherPunctualityLessonPlansDataLoaded) {
                    $rootScope.$broadcast('teacherPunctualityLessonPlansDataLoaded', teacherPunctualityLessonPlansDataLoaded)
                });

            School.allSchoolTeachersUnitsCovered(dataParams).$promise
                .then(function (teacherUnitsCoveredDataLoaded) {
                    $rootScope.$broadcast('teacherUnitsCoveredDataLoaded', teacherUnitsCoveredDataLoaded)
                });


            School.allRecordPerformanceData(dataParams).$promise
                .then(function (recordBooksDataLoaded) {
                    $rootScope.$broadcast('recordBooksDataLoaded', recordBooksDataLoaded)
                });


            School.allSchoolGrantCapitationPayments(dataParams).$promise
                .then(function (grantCapitationPaymentsDataLoaded) {
                    $rootScope.$broadcast('grantCapitationPaymentsDataLoaded', grantCapitationPaymentsDataLoaded[0].school_grant)
                });


            School.allSupportTypes(dataParams).$promise
                .then(function (supportTypesDataLoaded) {
                    $rootScope.$broadcast('supportTypesDataLoaded', supportTypesDataLoaded[0].school_support_type)
                });


            School.allSchoolFurniture(dataParams).$promise
                .then(function(furnitureDataLoaded){
                    $rootScope.$broadcast('furnitureDataLoaded', furnitureDataLoaded)
                });


            School.allSchoolStructure(dataParams).$promise
                .then(function (structureDataLoaded) {
                    $rootScope.$broadcast('structureDataLoaded', structureDataLoaded)
                });


            School.allSchoolSanitation(dataParams).$promise
                .then(function (sanitationDataLoaded) {
                    $rootScope.$broadcast('sanitationDataLoaded', sanitationDataLoaded)
                });


            School.allSchoolRecreation_Equipment(dataParams).$promise
                .then(function (recreationDataLoaded) {
                    $rootScope.$broadcast('recreationDataLoaded', recreationDataLoaded);
                });


            School.allSchoolSecurity(dataParams).$promise
                .then(function (securityDataLoaded) {
                    $rootScope.$broadcast('securityDataLoaded', securityDataLoaded);
                });


            School.allSchoolMeetingsHeld(dataParams).$promise
                .then(function (meetingDataLoaded) {
                    $rootScope.$broadcast('meetingDataLoaded', meetingDataLoaded[0].school_meetings);
                });


            School.allSchoolCommunityInvolvement(dataParams).$promise
                .then(function (communityDataLoaded) {
                    $rootScope.$broadcast('communityDataLoaded', communityDataLoaded);
                })
        };

        function centreTextInPage (text, y, doc){
            var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
            var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
            doc.text(textOffset, y, text);
        }
        // function centreTextInPage (text, y, doc){
        //     var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
        //     var textOffset = (doc.internal.pageSize.getWidth() - textWidth) / 2;
        //     doc.text(textOffset, y, text);
        // }



        var dummy = [{
            name : "Pupil Table",
            status : 'Not Available',
            term : "2nd"},

            {
                name : "Pupil Chairs",
                status : 'Available',
                term : "2nd"
            },
            {
                name : "Doors",
                status : 'Available, not func',
                term : "2nd"
            }];

        function createTableAndPopulate(doc, loopArray, x, y) {

            // doc.rect(x, y, 15, 10);
            // doc.text("Term " , x + 2, y + 6);

            doc.rect(x, y,  41, 10);
            doc.text("Item", x + 5, y + 6);

            doc.rect(x + 41, y, 37, 10);
            doc.text("Status", x + 55, y + 6 );



            for (var i = 0; i < loopArray.length; i++) {
                var obj = loopArray[i];

                // doc.rect(x, y + ((i+1) * 10), 15, 10);
                // doc.text($rootScope.one_school_terms[obj.term].number , x + 2, y + 5 + 10*(i+1));

                doc.rect(x, y + ((i+1) * 10), 41, 10);
                doc.text(obj.name, x + 5, y + 5 + 10*(i+1));


                if (obj.status === "Available" || obj.status === "Good" || obj.status === "Yes" || obj.status === "Adequate") {
                    doc.setFillColor(1, 166, 89); //green
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10, 'FD');
                    doc.setTextColor(255, 255, 255);

                }else if (obj.status === "Available, not functional" || obj.status === "Fair" || obj.status === "Inadequate") {
                    doc.setFillColor(243, 156, 18); //yellow
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10, 'FD');
                    doc.setTextColor(255, 255, 255);

                }else if (obj.status === "Not available" || obj.status === "Poor" || obj.status === "No"){
                    doc.setFillColor(245, 105, 84); //red
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10, 'FD');
                    doc.setTextColor(255, 255, 255);

                }else{
                    doc.rect(x + 41, y + ((i+1) * 10), 37, 10);
                }

                if (obj.status) {
                    doc.text(obj.status, x + 44, y + 5+ 10*(i+1));
                }else console.log("no status obj.status ", obj);

                doc.setFillColor(255, 255, 255); //yellow
                doc.setTextColor(0, 0, 0);

            }



        }

        /*This is for the school level*/
        this.downloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            /*
            * // Filled square
                doc.rect(40, 20, 10, 10, 'F');

                // Empty red square
                doc.setDrawColor(255,0,0);
                doc.rect(60, 20, 10, 10);
            * */

            doc.addImage(msrc_datauri_logo, 'PNG', 20, 8, 24, 15);
            doc.addImage (coat_of_arms, 'PNG', 170, 8, 19, 17);
            doc.setFontSize(11);
            centreTextInPage("Republic of Ghana", 12, doc);
            centreTextInPage("Ghana Education Service", 18, doc);
            centreTextInPage(pdfDocVariables.term_in_scope + " " + pdfDocVariables.title, 24, doc);
            doc.setFontSize(6);
            doc.text("Ministry of Education", 170, 27);


            doc.setFontSize(8);
            doc.setFontType("bold");
            doc.text("School", 35, 32);
            doc.text(pdfDocVariables.entity_name, 50, 32);
            doc.setFontType("normal");
            // doc.text("Circuit", 35, 36);
            // doc.text(pdfDocVariables.entity.circuitName, 50, 36);
            // doc.text("District", 35, 40);
            // doc.text(pdfDocVariables.entity.districtName, 50, 40);
            // doc.text("Region", 35, 44);
            // doc.text(pdfDocVariables.entity.regionName, 50, 44);

            doc.text("EMIS Number", 125, 32);
            doc.text(pdfDocVariables.entity.ges_code, 150, 32);
            // doc.text("Head Teacher", 125, 36);
            // doc.text(DataCollectorHolder.lookupDataCollector[pdfDocVariables.entity.headteacher_id].name, 150, 36);
            // doc.text("Phone Number", 125, 40);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 40);
            // doc.text("Email Address", 125, 44);
            // doc.text(pdfDocVariables.entity.email, 150, 44);
            // doc.rect(15, 5, 180, 42);

            doc.setFontType("bold");
            centreTextInPage("Pupil Statistics", 60, doc);

            doc.text("Total Number Enrolled", 30, 68);
            doc.text(pdfDocVariables.top_box_left, 70, 68);

            doc.setFontType("normal");
            doc.text("Boys", 30, 72);
            doc.text(pdfDocVariables.top_box_up_middle_up, 70, 72);
            doc.text("Boys with Special Need", 30, 76);
            doc.text(pdfDocVariables.top_box_down_middle_up, 70, 76);

            doc.text("Girls", 30, 80);
            doc.text(pdfDocVariables.top_box_up_middle_down, 70, 80);
            doc.text("Girls with Special Need", 30, 84);
            doc.text(pdfDocVariables.top_box_down_middle_down, 70, 84);

            doc.setFontSize(25);
            doc.text(pdfDocVariables.top_box_down_left, 26, 95);
            doc.setFontSize(8);
            doc.text("Student Attendance Rate", 45, 95);

            doc.addImage(this.barChart['enrollmentBarChart'].toBase64Image(), 'PNG', 100, 65, 80, 56);


            centreTextInPage('Pupils Average Performance', 126, doc);
            centreTextInPage('Ghanaian Language', 130, doc);

            var classArray= ['KG1', 'KG2', "P1", "P2", "P3", "P4", "P5", "P6", "JHS1", "JHS2", "JHS3"];

            var pupilPerf_X = 18; //this should shift the table right or left
            if (_.size(pdfDocVariables.gh_lang_performance_array) <= 10) {
                pupilPerf_X = 30; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 8) {
                pupilPerf_X = 45; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 6) {
                pupilPerf_X = 57; //6/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 4) {
                pupilPerf_X = 80; //3/11 classes showing
            }

            var pupilPerf_y = 134; //this should shift the table up or down

            var ghlpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var ghl = 0; ghl < 11; ghl ++){

                if (pdfDocVariables.gh_lang_performance_array[classArray[ghl]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X+(15.5 * ghlpusher), pupilPerf_y, 15.5, 20);
                    doc.text(classArray[ghl], (pupilPerf_X + 4)+(15.5 * ghlpusher), (pupilPerf_y + 7));
                    doc.line(pupilPerf_X+(15.5 * ghlpusher), (pupilPerf_y + 10), (pupilPerf_X + 17)+(15.3 * ghlpusher), (pupilPerf_y + 10));// horizontal line
                    doc.text( (pdfDocVariables.gh_lang_performance_array[classArray[ghl]].average_score || 0) + '%', (pupilPerf_X + 4)+(15.4 * ghlpusher), (pupilPerf_y + 16));
                    ghlpusher++;
                }
            }

            centreTextInPage('English Language', (pupilPerf_y + 27), doc);

            var elpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var el = 0; el < 11; el ++){
                if (pdfDocVariables.english_lang_performance_array[classArray[el]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X+(15.5 * elpusher), (pupilPerf_y + 30), 15.5, 20);
                    doc.text(classArray[el], (pupilPerf_X + 4)+(15.5 * elpusher), (pupilPerf_y + 37));
                    doc.line(pupilPerf_X+(15.5 * elpusher), (pupilPerf_y + 40), (pupilPerf_X + 17)+(15.3 * elpusher), (pupilPerf_y + 40));// horizontal line
                    doc.text( (pdfDocVariables.english_lang_performance_array[classArray[el]].average_score || 0) + '%', (pupilPerf_X + 4)+(15.4 * elpusher), (pupilPerf_y + 46));
                    elpusher++;
                }
            }

            centreTextInPage('Mathematics', (pupilPerf_y + 57), doc);

            var mtmpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var mtm = 0; mtm < 11; mtm ++){
                if (pdfDocVariables.maths_lang_performance_array[classArray[mtm]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X+(15.5 * mtmpusher), (pupilPerf_y + 60), 15.5, 20);
                    doc.text(classArray[mtm], (pupilPerf_X + 4)+(15.5 * mtmpusher), (pupilPerf_y + 67));
                    doc.line(pupilPerf_X+(15.5 * mtmpusher), (pupilPerf_y + 70), (pupilPerf_X + 17)+(15.3 * mtmpusher), (pupilPerf_y + 70));// horizontal line
                    doc.text( (pdfDocVariables.maths_lang_performance_array[classArray[mtm]].average_score || 0) + '%',
                        (pupilPerf_X + 4)+(15.4 * mtmpusher), (pupilPerf_y + 76));
                    mtmpusher++;
                }
            }

            doc.rect(15, 55, 180, 242);


            doc.addImage(this.barChart['performanceBarChart'].toBase64Image(), 'PNG', 20, 215, 170, 80);


            doc.addPage();

            doc.setFontType("bold");
            centreTextInPage("Teacher Statistics", 20, doc);
            doc.setFontType("normal");

            /*Teacher To Pupil Ratio*/
            doc.rect(20, 25, 53, 30);
            doc.addImage(scGeneralService.report_image_uris.teacher_to_pupil_image, 'PNG', 23, 29, 17, 17);
            doc.setFontSize(13);
            doc.text("Pupil \nTeacher Ratio", 42, 34);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_pupil_ratio || 0, 42, 50);
            /*Teacher To Pupil Ratio*/

            /*Teacher Attendance Ratio*/
            var attendance_ratio = scGeneralService.report_image_uris.attendance_ratio;
            doc.rect(78, 25, 55, 30);
            doc.addImage(attendance_ratio, 'PNG', 80, 29, 17, 17);
            doc.setFontSize(14);
            doc.text("Teacher \nAttendance\nRate", 98, 34);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_attendance_ratio || 0, 112, 50);

            /*Attendance Chart*/
            doc.addImage(this.pieChart['teacherAttendance'].toBase64Image(), 'PNG', 136, 15, 60, 42);
            doc.setFontSize(9);
            doc.text("Teacher Attendance Rate", 148, 62);
            /*Teacher Attendance Ratio*/


            /*Percentage Trained Teachers*/
            doc.addImage(scGeneralService.report_image_uris.teachers_trained, 'PNG', 23, 69, 17, 17);
            doc.rect(20, 66, 53, 30);
            doc.setFontSize(13);
            doc.text("Number of", 44, 71);
            doc.setFontSize(15);
            doc.text("Trained\nTeachers", 44, 77);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.percentage_trained_teachers, 44, 92);
            /*Percentage Trained Teachers*/



            /*Adequately Marked Exercises*/
            var adequately_marked_exercises = scGeneralService.report_image_uris.adequately_marked_exercises;
            doc.addImage(adequately_marked_exercises, 'PNG', 82, 69, 17, 17);
            doc.rect(78, 66, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.adequate_marked_title, 107, 71);
            doc.setFontSize(15);
            doc.text("Marked", 107, 77);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.adequately_marked_exercises, 104, 92);
            /*Adequately Marked Exercises*/

            /*Punctuality Percentage*/
            var punctuality_percentage = scGeneralService.report_image_uris.punctuality_percentage;
            doc.rect(138, 66, 53, 30);
            doc.addImage(punctuality_percentage, 'PNG', 140, 72, 17, 17);
            doc.setFontSize(14);
            doc.text(pdfDocVariables.punctuality_title, 155, 71);
            doc.text("Punctuality\nAbove 50%", 160, 77);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_punctuality, 164, 92);
            /*Punctuality Percentage*/


            /*Number of GH Language Units Covered*/
            // doc.addImage(scGeneralService.report_image_uris.teachers_trained, 'PNG', 23, 100, 17, 17);
            doc.setFontSize(30);
            doc.setFontType("bold");
            doc.text("GH", 23, 115);
            doc.rect(20, 100, 53, 30);
            doc.setFontSize(12);
            doc.setFontType("normal");
            doc.text("Number of\nGhanaian Lng.\nUnits Covered", 42, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_unit_covered_gh_lang, 45, 126);
            /*Number of GH Language Units Covered*/



            /*Number of English Units Covered*/
            // adequately_marked_exercises = scGeneralService.report_image_uris.adequately_marked_exercises;
            // doc.addImage(adequately_marked_exercises, 'PNG', 82, 100, 17, 17);
            doc.setFontSize(30);
            doc.setFontType("bold");
            doc.text("EL", 83, 115);
            doc.rect(78, 100, 53, 30);
            doc.setFontSize(12);
            doc.setFontType("normal");
            doc.text("Number of \nEnglish Lng.\nUnits Covered", 100, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_unit_covered_english, 106, 126);
            /*Number of English Language Units Covered*/


            /*Number of Mathematics Units Covered*/
            // punctuality_percentage = scGeneralService.report_image_uris.punctuality_percentage;
            // doc.addImage(punctuality_percentage, 'PNG', 140, 100, 17, 17);
            doc.setFontSize(30);
            doc.setFontType("bold");
            doc.text("MA", 141, 115);
            doc.rect(138, 100, 53, 30);
            doc.setFontSize(12);
            doc.setFontType("normal");
            doc.text("Number of\nMathematics\nUnits Covered", 160, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_unit_covered_maths, 165, 126);
            /*Number of Mathematics Units Covered*/



            doc.rect(15, 15, 180, 125); /*Main teacher rectangle*/



            doc.setFontType("bold");
            doc.setFontSize(8);
            centreTextInPage("School Status and Statistics", 145, doc);
            doc.setFontType("normal");

            /*Meetings Held*/
            var hold_meetings = scGeneralService.report_image_uris.hold_meetings;
            doc.rect(20, 147, 53, 30);
            doc.addImage(hold_meetings, 'PNG', 23, 150, 17, 17);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.meetings_title, 41, 152);
            doc.setFontSize(15);
            doc.text(" Meetings\n Held", 41, 158);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.meetings, 45, 173);
            /*Meetings Held*/


            /*School Support*/
            var school_support = scGeneralService.report_image_uris.school_support;
            doc.rect(78, 147, 53, 30);
            doc.addImage(school_support, 'PNG', 82, 153, 19, 19);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_received_or_any_type_of_support, 104, 152);
            doc.setFontSize(15);
            doc.text("support\n(kind/cash)", 104, 158);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.support_received, 104, 173);
            /*School Support*/


            /*Grant Received*/
            var received_grant = scGeneralService.report_image_uris.received_grant;
            doc.rect(138, 147, 53, 30);
            doc.addImage(received_grant, 'PNG', 141, 152, 19, 19);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_or_amount_of_title, 162, 152);
            doc.setFontSize(15);
            doc.text("Grants\nReceived", 164, 158);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.grant_received, 164, 173);
            /*Grant Received*/


            // doc.line(20, 246, 191, 246);// horizontal line
            // doc.line(20, 248, 191, 248);// horizontal line

            doc.rect(15, 140, 180, 40); /*Main Termly vectors rectangle*/



            doc.addImage(scGeneralService.report_image_uris.adequate_furniture, 'PNG', 21, 190, 19, 19);
            doc.text("Furniture", 42, 202);
            doc.setFontSize(8);
            doc.text("Were the following items adequate?", 42, 206);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.furnitureItems, 20, 208);


            doc.addImage(scGeneralService.report_image_uris.security_facilities, 'PNG', 111, 189, 17, 17);
            doc.setFontSize(15);
            doc.text("Security", 130, 201);
            doc.setFontSize(8);
            doc.text("Were the following items available?", 130, 205);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.securityItems, 110, 208);




            doc.addPage();

            doc.addImage(scGeneralService.report_image_uris.good_structure, 'PNG', 21, 10, 17, 17);
            doc.setFontSize(15);
            doc.text("School Structure", 40, 21);
            doc.setFontSize(8);
            doc.text("Were the following items in a good state?", 40, 25);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.structureItems, 20, 29);



            doc.addImage(scGeneralService.report_image_uris.sanitation_facilities, 'PNG', 111, 10, 17, 17);
            doc.setFontSize(15);
            doc.text("Sanitation", 130, 21);
            doc.setFontSize(8);
            doc.text("Were the following items available?", 130, 25);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.sanitationItems, 110, 29);



            doc.addImage(scGeneralService.report_image_uris.recreational_facilities, 'PNG', 21, 128, 16, 16);
            doc.setFontSize(13);
            doc.text("Sports/Recreational Facilities", 39, 137);
            doc.setFontSize(8);
            doc.text("Were the following items available?", 39, 141);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.recreationItems, 20, 146);



            doc.addImage(scGeneralService.report_image_uris.with_record_books, 'PNG', 111, 128, 16, 16);
            doc.setFontSize(15);
            doc.text("Record Books", 130, 137);
            doc.setFontSize(8);
            doc.text("Were the following books available?", 130, 141);
            doc.setFontSize(10);
            createTableAndPopulate(doc, pdfDocVariables.books, 110, 146);







            doc.save(pdfDocVariables.pdf_export_name)



            //
            //
            //
            // doc.line(20, 53, 191, 53);// horizontal line
            // doc.line(20, 55, 191, 55);// horizontal line
            //
            // var active_community = scGeneralService.report_image_uris.active_community;
            // doc.addImage(active_community, 'PNG', 114, 65, 19, 19);
            // doc.rect(110, 58, 80, 30);
            // doc.setFontSize(15);
            // doc.text(pdfDocVariables.community_title, 113, 63);
            // doc.setFontSize(26);
            // doc.text(pdfDocVariables.community_participation, 150, 80);
            // // doc.save('a4.pdf');




        };


        function commonModelPDF(doc, pdfDocVariables) {
            doc.addImage(msrc_datauri_logo, 'PNG', 20, 8, 24, 15);
            doc.addImage (coat_of_arms, 'PNG', 170, 8, 19, 17);
            doc.setFontSize(11);
            centreTextInPage("Republic of Ghana", 12, doc);
            centreTextInPage("Ghana Education Service", 18, doc);
            centreTextInPage(pdfDocVariables.term_in_scope + " " + pdfDocVariables.title, 24, doc);
            doc.setFontSize(6);
            doc.text("Ministry of Education", 170, 27);

            doc.setFontType("bold");
            centreTextInPage("Pupil Statistics", 60, doc);

            doc.text("Total Number Enrolled", 30, 68);
            doc.text(pdfDocVariables.top_box_left, 70, 68);

            doc.setFontType("normal");
            doc.text("Boys", 30, 72);
            doc.text(pdfDocVariables.top_box_up_middle_up, 70, 72);
            doc.text("Boys with Special Need", 30, 76);
            doc.text(pdfDocVariables.top_box_down_middle_up, 70, 76);

            doc.text("Girls", 30, 80);
            doc.text(pdfDocVariables.top_box_up_middle_down, 70, 80);
            doc.text("Girls with Special Need", 30, 84);
            doc.text(pdfDocVariables.top_box_down_middle_down, 70, 84);

            doc.setFontSize(25);
            doc.text(pdfDocVariables.top_box_down_left, 26, 95);
            doc.setFontSize(8);
            doc.text("Student Attendance Rate", 45, 95);

            doc.addImage(this.barChart['enrollmentBarChart'].toBase64Image(), 'PNG', 100, 65, 80, 56);


            centreTextInPage('Pupils Average Performance', 126, doc);
            centreTextInPage('Ghanaian Language', 130, doc);

            var classArray = ['KG1', 'KG2', "P1", "P2", "P3", "P4", "P5", "P6", "JHS1", "JHS2", "JHS3"];

            //this should shift the table right or left
            var pupilPerf_X = 18; //if all 11 classes are showing
            if (_.size(pdfDocVariables.gh_lang_performance_array) <= 10) {
                pupilPerf_X = 30; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 8) {
                pupilPerf_X = 45; //8/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 6) {
                pupilPerf_X = 57; //6/11 classes showing
            }
            else if (_.size(pdfDocVariables.gh_lang_performance_array) <= 4) {
                pupilPerf_X = 80; //3/11 classes showing
            }

            var pupilPerf_y = 134; //this should shift the table up or down

            var ghlpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var ghl = 0; ghl < 11; ghl++) {

                if (pdfDocVariables.gh_lang_performance_array[classArray[ghl]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X + (15.5 * ghlpusher), pupilPerf_y, 15.5, 20);
                    doc.text(classArray[ghl], (pupilPerf_X + 4) + (15.5 * ghlpusher), (pupilPerf_y + 7));
                    doc.line(pupilPerf_X + (15.5 * ghlpusher), (pupilPerf_y + 10), (pupilPerf_X + 17) + (15.3 * ghlpusher), (pupilPerf_y + 10));// horizontal line
                    doc.text((pdfDocVariables.gh_lang_performance_array[classArray[ghl]].average_score || 0) + '%', (pupilPerf_X + 4) + (15.4 * ghlpusher), (pupilPerf_y + 16));
                    ghlpusher++;
                }
            }

            centreTextInPage('English Language', (pupilPerf_y + 27), doc);

            var elpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var el = 0; el < 11; el++) {
                if (pdfDocVariables.english_lang_performance_array[classArray[el]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X + (15.5 * elpusher), (pupilPerf_y + 30), 15.5, 20);
                    doc.text(classArray[el], (pupilPerf_X + 4) + (15.5 * elpusher), (pupilPerf_y + 37));
                    doc.line(pupilPerf_X + (15.5 * elpusher), (pupilPerf_y + 40), (pupilPerf_X + 17) + (15.3 * elpusher), (pupilPerf_y + 40));// horizontal line
                    doc.text((pdfDocVariables.english_lang_performance_array[classArray[el]].average_score || 0) + '%', (pupilPerf_X + 4) + (15.4 * elpusher), (pupilPerf_y + 46));
                    elpusher++;
                }
            }

            centreTextInPage('Mathematics', (pupilPerf_y + 57), doc);

            var mtmpusher = 0; //used to ensure the performance tables are drawn from the left in the pdf
            for (var mtm = 0; mtm < 11; mtm++) {
                if (pdfDocVariables.maths_lang_performance_array[classArray[mtm]]) {
                    doc.setLineWidth(0.4);
                    doc.rect(pupilPerf_X + (15.5 * mtmpusher), (pupilPerf_y + 60), 15.5, 20);
                    doc.text(classArray[mtm], (pupilPerf_X + 4) + (15.5 * mtmpusher), (pupilPerf_y + 67));
                    doc.line(pupilPerf_X + (15.5 * mtmpusher), (pupilPerf_y + 70), (pupilPerf_X + 17) + (15.3 * mtmpusher), (pupilPerf_y + 70));// horizontal line
                    doc.text((pdfDocVariables.maths_lang_performance_array[classArray[mtm]].average_score || 0) + '%',
                        (pupilPerf_X + 4) + (15.4 * mtmpusher), (pupilPerf_y + 76));
                    mtmpusher++;
                }
            }

            doc.rect(15, 55, 180, 242);


            doc.addImage(this.barChart['performanceBarChart'].toBase64Image(), 'PNG', 20, 215, 170, 80);

            doc.addPage();

            var teacher_to_pupil_image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABQ7SURBVHic7Z17dBRVnse/t7ob8uBhAHmTLiQgFCSBpDuiCEkQ2fGBuoy6jswJ+JjVUVcdHXFcZfF1xvE5HN3d0YNzHOKwuo7r8YXKgCQBw0C6E0OAJiEkdDo8EgLkJST06+4f/aCqUtXp7lR1d0h9zsk5Xbeqfvemf7/+3Xt/91e3CKUUfAghJH12zv2EkFsA5AKYBHU4CaCSUvqlo7bqfSpuSISwnOlaAG8CyFOkdZc+FQCeJPzvPX12zmSGYTYDKIhxY0q9Xu8qR23ViWgFsJxpLzTlR0oFE/hECCFxUj4AFDAMs5kQQgYgQ1N+5OQFDSB9ds79iI/yAxT426ARQ/SBD/4+P8ioJNS8/guDLpclc9WouNJODz71kcvT1YssURs2KiHftvsrJcRccnDXrBAcM7zPufwTaiofAHJZMvf1Xxh04mK16tOQhm8AgtG+msoPUYdaMw4NGZj+L9G4lNH3f4kP03qnIhVaXximiBwNZdA8wBAnbA9wqbFzT2XNk8+9qjt3vkfRsU5qSvLBN19+2rNkYW6W1Pl41SvHkPUAaigBAM6d75n75HOvimc3ca9XjrA9wKXWd6uhhHBkx6teOYasB9DwwfcAJ8Gbh1fa6UG1YwGVdnoQAL+Ok2rWF4qBejjxLEkccYtVvZHC9wCV/BNPfeTy+BWkCoFQsLhYrfo0pAl6AErpl4SQmwPHXb3IeuADV/DCy1JJ9at36g0Br1BeT2v+/W8u3bkLiNZL9LmPUvpllLI0oiToARy1Ve8DKJW7sOMcnf/gBy62voU2AsAAlS9Fqb8NGjGE7wFo+uycVaFyAiiQes9Gl3fb74adV1r5Xq931UCzggaCUpHOAHKrkeKxgdL1RopgGuiorTpBCFkaKiWs142Me993/QDg2gHWrWhKmEZ09IkD+BWxEbx1eeNc0weEYk3guL6F9lG+3WYdSDZPzElNST6o1pw8NSVZPLuJe71yhBUHcCfjYQCqzQjiwZsvP+3xf2GKEgjJJlq9cpBwPS+bmTsHHmIBkCp1Pt4egOVMgn9ksGcEiccKSv0/oTKCQmLfX3mIgjykSCs0EoaIVgObbJZilsvNB8i90VSWkZ093u0ymCmlJoZhcimlJgAghFi9Xm8lIcSqN7gsR/btOxWNfI3IiXg52NM9/BHdSGcegHnh3kMKC/XG1u5nAMM6AAZCCPhdD6V0BSFkBQC4XQYXy5leapow8hVaUuKOtH0akRGxATQ37+6ZfqXpDqqDFRfHA4fkrmczc+ewbmyihJjDrMIA4EW2pWsFm5m72r6/Ula2EoQbs4+UwTIGiWo18GidtZZhyFIQlFCgmVDm11LXGTlzETykKgLlB6GEmOEhVUbOXBRNGzXCI+qMoMYDlgoAS+XOs5m5cwjIewCS+OVEp7NfbsxoSps6PSVtCpsOAO3H7Y6zzY3nTjsaWOrxsLzLkwjoe2xmrkVtTxAros0I6s9TRZsRpEpKGCks1PvcvkD5dPTk9J1Zy1fmMXo9y79+4qx5EybOmgev291T8/fPyjpPOJYACEwrk4gbm0hh4TWXwphA5Yygg5btn0R0nyoGYGztfkbk9unsghutEzK4/FD3MXp98vwb78xvPWKz1JZ+Y4LfCCghZt8gEi8p3dZY99WXfEZQRnb2eADr+GWjJ6fvnJDBhT0OmJDBmUdPTt8pKl7nl62hIIp7ALfLYIZvJA/A1+dnLV8Z8ZO7WctX5v3w4Tt23pjA4Je9RZmWJgaJlBGkCIHgToBx6TPsjF6fHKkcRq9PHpc+wx5KtsbAUdwD+CN8weMx066QXDsIhzHTrkhtO3pYIHtgrZNn08ef73574+YxAPDC04/8dPPy/JDG1t+ofLDEARQ3APGvNDDViwbxvWp5gMee+X3ptrJ/FASO1z7/hrurq3vP3bffvFCN+hKJIftkEAA4na4LP1/zWGWDvblAdEr/8lvvmQCobgTxzghSfAxACLHyj9uP2x3RyhLfK5Y9ENo7us4uWVFU12BvvkbmEv3Lb71n+p9Pv96jVJ2JiOIewL+qF+wgzzY3nps4K+x1IwFnmxvPiWUPsHkAgIajDvvK1Y8Rl9stipqRDoCOwMXvRdYTRNvHD8qMoEgQ/0pPOxpYr9vdE6kcr9vdc9rRwIaSHQ2l5ZZ9t/zykdEut9vIL2cYnd18+5rOjKuXWgDwM2sU9QSDNiMoXPxr/sfAiwWMnpxeNv/GO0NGAcVUf/NJWecJB/8el97gmtpfrsD5jpOy/9D7H35a/tafNpkBCCbf+uFJNXl33DfNkJScBgDHDlT+o2FPSR4A/sOW7tee/211f7MDpUi4jKBw8StIELLtPOFY0nrEZglXRusRm8W/HsDnpYEkijyx7tXSt/60aRFEyk9NG1d+9apfzw4oHwCmzsu9esZVBXsh8gQvvv7fEcczEh1VZgFNE0a+wrZ0reCtB5Da0m9MJw8fKPMvBkl+kf7FoArRYhAIpRb7xFGvRNMWl8vtvP2exy31jU0F4nOXT59Vyl13S59yAKDUG5ZrlIsHDNk4AADQkhI3m5m7Gh5U4eJyMOk84cj/4cN3+lsOFncVvVSP1dGsBHZ0drXfeNeDjo7O7kWiU87p5iWW9Oy8Aqn7mmsqyhsrdi6EqAv4j6ceingsk+ioFgew7688ZOTMDxBQQU4A9XjYU4117KnGOv7lE2TE9FKQB5r2WyPOBTjadMxxW9GjXpfLlS04QUj7vOtvc4xNnyE2CgDyyn/uiQesNy/Pv+QCQ6ruD9BksxRDR3MIpWH3/wEIpRboaE6TzVIc6b0/7Knaf/PdD41wuVysQCajazKtXN05Nn1GttR9oZR/qUYFFZ8FSFYSTArFOvBmBzK4AESVFMpypmn515iLy3ZbrgYwnH/OP9KfakhKHiN1b6yUH+89gsRjlpgYQAC10sJZzrQYwKMAboNEt5Zy2Zjduf+8OpfR6Yb3uRmx/eWbl92paiDIsv2TkLLFBhDTtQC/YrdAgTV9ljMlAbgbPsVLunQAGMfOLJ277NZ88GYVfGLt9hMtI2jQLQaxnGkagIcA/ArAWNkLCTl7hXnxoWlZ0iN9YGj2+WIGjQGwnGkJLrp52e3QGL3+8LRMU2t69kITo9dLjvSByJUf6fMD4cYBiselRCRXTNHp8wO6P6ENgOVMyfC5+X9DCDcPwJM0crQlY2Fh0lhjxnwAs0LJ1X75F4naAFjOVAjgZQByy6nqQ0j7mClszcxFy2YmjRwdluI05QuJygD8o+5vIZpqxQqRmw97kUlTfl8iNgCWM80G8Dlir/yI3Dwft/NCV93O76pO2+sXI0rlqxXbH2gfPlAiMgCWM00A8A0AyWCKGhBCWtOmsLWRuPkAbueFrsM//L2qrbFuAfpufDWkf/kBwjYAljOlAPgawHR++cSZc0uvzL+hQOF28ZkA+bUCSXyK31bV1lgrpXggjsoflBlBLGfSAfgYgCAZYtT4STtVVn5EuJ0Xumw7vi4tL36HtjXWFgAYLb6GENL+4u8eqYzXL39QZgSxnOm/4Au+BBmeOtJy1V3/mkMIiXiLcqVxOy9015dvqzzVULsAEkoHAEJIx43XL6le/9RDuSNSU0bGuIkJQWvbmdbCW9fwvenJfrsAljOthUj5OsOwQ+Y77uXirfyLiq+bD9ACqWsIIR03LFtc/fzah3NHpKZIXjNU2F1R7YCwO7WGNACWM/0LgD/wywjDHM+7496xOr0hqid+yt5/I+T5/Pt/268Mn+K3V55qqA2l+M6fXXftj8+vfThn5IhUyWuGGh99tqVXVCRvAP65/iYIFlFIR86tv7wwLGXEFFVa2A/hKv6fli768YWnH9EUz2PHrr3VBw7Vizf4lDYAmbm+c97y25pGjB0vGZI91VhbWVv6zXhCiIu77pZuuaSLaDltr//x4PdfsqDSigfQCWDDnq0f/UZTvJAdu/ZWP/rM7ydBuCK6F8DWPgYgM9enM64qsI5NnyEZ9u1sPV57aMfXHIBkCsD2/ZeNi+/5jXL/AYBDO75OA6VpEqc6AWwAsMFus3ac7zi5XtGKBymtbWdad1dUOz76bEuv/5fPV34PgNV2m9UjMIAQc/2yqZmmAqmKnD3nz+zb8r+jAAQzfb0ezxVyDQv08eKxQH99v9cr2DsIECle7j61dgEbBISKnzxrt1nrAF4gSHauf/mkXXJzfer1uq3/90Ez9XrnK9LkyGBDKV5Dkh4Az8L3wwEgjAS+DUDwcxmeOsIy/5a7ZVf7ar77tNzV2xPREz9KIaf8lMsmiTN/tG3ofeyFz+0L0rH1QKi5/n2yc33Hvr3lHcJHtzQSi5MArLy/rXabtU+kUM9ypp8jwrl+Z+vx2qOWXTkqNFpx4r2LeaLDAHgDwhFip3+uL7kjl9SgD76+RWMQwkD0sKRxwcKaEWPHS47ieYO+yfzyK/Lyf1SxjRoqwgD4jl/QfqxJ9mL/oE8w4k+bYiyblmWOX1qYxoBgAPyNX9B9umUOpbTPYOHYfutu8aDPkJTyY+bPbh/oy6M04ggD4HsAZwMFlNJxZ5qO1IgvbLTsEsT/CcMcN9++xhjvFUGNgcHYbVYXfHH/IM37Kn7qeykVJI9MmZvbYEhKiVlqmIY6BJQq2GJaqhu4bHJ6I/+4tf7AKJXbphEDAgawA/10AxlXFRrBi6q5enuyz3ecsceikRrqwQBAON1AStpY1pCUvI9XRBr2lMhPGTQGBfx+vd/ZQPr8hYKAz9njTfO8Xk98t7rUGBB8A+h3NjCZm58LQs7g4kVjT9iqFdm8USM+BFcD7Tari+VMnwMIvhOweV/FT+PYmcGLGUY3LG2K8UD7MXswHuCo3pM8dV7km3iHk/unoT7i5wL67Qa0weClhdgA+u0GtMHgpYXAACRnAzUV3YHPbueF7kMlW8pcvb0s/5r2401XqtlIDfWQygr+BLxxQHdbC+e+0NtRv/v7facaa7NAaZ8kEEqpRORQYzAgZQCBoNAYwNcNlH/4n+cByGX/nAXwAHzdh8Ygo8/DoVLdAACpjWzOwrfv33S7zbpDhbZpxAC5J4ME3YCIswD+COBtu83apUqrNGKGnAHsANAMYBqvbMgpXmZjyxRQ2CmBHUAJ9M7ippqa9nDkGbOy0uAeVgSgkFCwIGABQGF55yPZeFP28XD/42F/hW/O/wX6UTzLmQSClA70iB8kUTPZM8KtbXspwcfDnN619fVVbVIXzJyZc7lzGPMaobgLopdpx0BeyK13FdsqNlIDONt8tMa24ysdAHBLV3jGTJseco/bWBkAm5k7h7ixKYpX3rdQkKImm2Ubv9DIma8noMUAJsZTHqHUQvVYLX4Lu6q7hYfCtuMrncflnOtxOecGDCHeGDlzETykKgrlA8BEArqV5cz3BQpYznwfAd2KyJWvuDxKiBkeUmXkzEX88rhtFOlxOedKfY4XbGbuHAIieLcBABh0sOfP1jUtmomUq2Yw6QCwt8HrKD9Mz5XVeVmXx9+P+yAAfdfI5TX5Dui7EO1RHGd5SQT0PTYz1xLwBAm9U2isIIWFetaNTZQIlE9N05mdG1bp85IMgi8RKxboJqxYAPS60PP4ZneZ9aiX/4obPYH308DnBJSXRNzYRAoLr6ElJe64dQGJhLG1+xmR26cvrNRZ312jz08yQPZFUUkGJL+7Rp//wkqdFcJnEEdDuFdRQsmjhJj9g9z4jQEShYzs7PHwjfaDmKYzO2/K1oU9DrgpW2c2TWd2yp1PUHnrMrKzxw95A3C7DGbwpnoGHewbVunzIpWzYZU+z6CDXVyewPIMbpfBPOQNQPxG8vw5jD2UW5UjyYDk/CsZu7g8keVRSk1D3gAYhhGkMy3KIFHtfgYAi2b1vTeR5TEMkzvkDUDsAQJTqWiQujeR5WkewMfAXtkxuEnRDIAKB1p7G7yOaEVJ3ZvQ8ijsQ94A/KtwQcqP0HPRyio/3PfeRJZHiWYAAFDCPyg75GV7XZHveNLrQk9ZnZcVlye4vBLNAPTOYgDBPXRdHrCPb3ZXRCrm8c3uClHcPdHl9ULvLB7yBtBUU9NOCT7ml1mPepds2ecJ+33HW/Z5LP54uySJKI8SfNxUU9M+5A0AAIY5vWsBtPCKyPrPPKYH/+IuC+Vue13oefAv7rL1n3lMEK7Stfv/ElVei/9/1lYDAaC+vqrNyJmL/GvtgS+KWI968wtfcfa33CrOlm4HsMz/eTuAtASTRylIUSDbSDMAP002yzaWM/8KvjX34Pfi8oDdftDDbj8IAMGn5OT24G0HsMxus1YBAMuZlkGotHjLcwPkQX6WkdYF8LDbLH+mYG6AbyPqSBEoyyfPWgXfrzesJE+V5XVSMDfYbZY/8ws1AxjiaAbAw5dz5/0WMi+e6oc0ANtZzhTcQtf/WeCy4yhvNIH3W36OIaCNAYL4s203YmA5dwGlSQ3aEkGeHqAbjZzZERgHaAYAX549MTDFECo/2py7NPgUFficaPIIAS2eOTMnq76+qk3rAgA4hzGvQZhqPdAcvjSIlJVg8ib6/2dtDGDMykrzP2ETJEFz+BSVRyjuMmZlpQ15A/A/WxdMB0/gHD6l5SXBPaxIMwCgkH+QyDl8KsgrVNIATvIPLpz7qVVB2apBqHAAlcg5fErLIxSskgZg5R+0H7dHnbkSU4jQABI5h09xeURFAzhxqFr8ntpERRD0GT+KyMXR+0Xq3gSXN1o1A+hua7n2TNORagXla6iAkgawFb530wUgB7d/MUkzgsRGMQPwv5NuNXhvEKOUTjiw7fPsqi/+uqvl8AHLYBkYDiUUDQXbbdY6ljM9C+AtXjHpbmtZXNf2ndxtAPruAKIRG9SIA2wA8AS0dwkOChQ3ALvNSu026x8BLIBwTKAkEWfFakijWiTQ/5LiRQBuArAewFcQBYuipALAkwrI0QDw/43f1OjMfigqAAAAAElFTkSuQmCC";
            doc.rect(20, 20, 53, 30);
            doc.addImage(teacher_to_pupil_image, 'PNG', 23, 26, 17, 17);
            doc.setFontSize(14);
            doc.text("Pupil to \nTeacher Ratio", 42, 28);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_pupil_ratio || 0, 42, 45);

            var attendance_ratio = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z15fFTV+f/fdyazJJN1QkgICYQkJAIim0arXzewSF2rUL+2IrWKQkGUnwtQQamFWgW/KKhYFyyv2i+KX0ALiAUBFS27ZRUNSWSHBLInk2TW8/tjZi53Zu5MZkI2kM/rdV9zz5lz75y557nPec5zPuc5khCC8xWSJOkBM5Dk+fQeiYDJc8T4Hf550YDGc0ieQxPk03u0BC6gEjgJrAeWCCGKW3ivVoMUrgBIkpQMdAOi8H0Q/g9GmdaFOPSKT4Pi0xAkz4C7YZWNbWrJn+4kaABuF0Js7MhKqAqAJEn5wC+B64FcIAP3m3IRrYsDQoh+HVkBHwHwqNT5wMOAtqMq1VJoNBr0ej06nQ6DwYBerw9I63Q69Hp9QNqbFyrd3KHT6YiKisJqtdLY2OhzOBwOnE4nq1at4vPPP1dWO0UIUd5Rz8xfAOYCTwUrrNFoSEtLw2QyIYTAe63yXJnWaDRER0cTHR2N0WgMOI8kL1RDeBtYq+38MltfX0+PHj2w2WzerF5CiMMdVZ8o74kkSdHAZOWXWVlZTJw4kX79+pGZmUlGRgY6na7dK3khITY2lszMTEpKSrxZHWrHRCnOByvTU6dOZdq0aRcbvA3gcDg6ugoyNIrzOOUXDzzwwMXGbyPY7XZl0hasXHtAo5aZlJTUlJmZ2d51+cmgMwlAlFpmly5dnO1dkZ8S/ATA2pr3liRJAroDArAIIapDlVfVABqNRjX/IloHbaEBJEm6XpKkZcBp4BhwHKiUJGmHJEl/8Bj5AQgmAJ1/PHUew88IPCcNIElSb0mSPga+BEYCXZRfA5cDLwD/kSTpCv/rVQVAez4MqM9jtIYGkCQpVpKkV4HvcHttZZh0errFxiH5TltcAnwlSVK2MlPVBrioAdoOLpcLl8vlTQohhD1UeTVIkmQC1gDXAmgkiVtyL2Hc4AJG5OQT5enBiysrWPjtFt7bvZMaaxO43fkLgRHee6kKgNuOuIi2gNPpY19H7BCQJCkG+BRP4/fvmsaHd/+Gvl26BpTNNScz7+e38cCAIVz9tzex2G0AN0uSNEwIsQGCdAEX0XbQ6XSYTLLzTxfMOFODovGvB3hgwBC2PThRtfGVuKxrNx4YMESZJXcDFwWgA5CSkqJMJkdw6WLgBoBbcy9h0W2jiI4Kz1k3pFt3ZVI2FC8KQAegJQIgSdIjwK8AeiQksuSuX6OJoKsus9Qrk7IRclEAOgCRCoAkSd2Al73pozXVjPhgEUsP7MUpXCGuPIsTdbXK5E7viaoAXDQC2xYt0AAv4zdXs+X4Ue5dsYRrFr/JgfLTzd5gd+lJ76lAIQCqo4BIIYTg+PHjFBUVYbPZfObpdTodmZmZ/n/6Jw2/Z2EOVVaSpOuB3wT7ftuJYwx+ZwGvDL+N3w+5SrXMnrJTfHPssDe5XQhR4020WABsNhsrV65k8eLF7Ni+HUtDQ9CyUVFR/Pa3v2X+/Pmq3wsh+Oijj9i/fz/PPffcBT8LGa4GkCQpCnjdm37st3om3Gdgb6GTl9+1sn2Pe0hpdTqY8NknOF0uHr3i6oD7LNjxb2VyiTLRIgE4ceIEI4YP59CRI6rfa4BMvZ5so5ETNhsHm5pYtGgRd955J0OHDvUpe/z4ce699152794NwP33309eXl5LqnXeoEsXpbc2ZBcwCbgU4IrLtMx/1j1izM/WMGqEjrc/tDF1TpOoqRMSwGNrVxGl0TJ+yJXyDcobLCzZv9ubdABLlT8QsQA4HA7uuO02ufFviItjUEwMOUYjOQYD2QYDWQYDeoUdcXtREaurqykpKfERgJKSEm699VaOHz8OQEFBQUDj79mzh/Lycmw2G3a7HavVis1mk9Pec5vNhsvlCqCpqVHXhBA4HA6Zp+d0OuW08vDz2gFu+yguLo7ExESSkpIwm8307t07QLBDIRwNIEmSGZjpTb80xej3PYz7tZ5f/lwnPTy9oWnVBodRIJjw2SdEaTSMHeR2+7+9aztNZ+cePhBClCnvE7EncN26dRQWFdFdr+cf2dncEBcXtKwX/202s7q6mkOHDsl5VVVV3HXXXXLjS5LEc88953OdxWLhvvvu4+jRowEN0ZHQarX06NGD3NxccnNz/d/oZpGc7NPmwTTANCABwJwoERuj3iapXST++VeTccpLTbUvv2uNFwge+XQFA1K7MTCtGwt3bvEWFcBL/tdHrAHWrl0LwKSuXcNqfIAsgwGAIx6t4XA4GD16ND/++KNcZsqUKdxwww0+15lMJvbv348QAovFQl1dHXV1ddTW1lJfX09tba2cV1dXh8vlkg1PvV5PVFSUfO7N9zJ3w4EkSTJ72Gg0ykd6evo52SlGo8/bnKjyu+nAo56kqKwWUsHd9Qz9WRR/GG/gpmui/MrD3GnG+KwMzZlJf2xMEoioZ79ax135/ZTDv9VCiO/8fytiAaipcRuQtyQG1DsosvR6AI4dOwbA008/zVdffSV/f8cddzB9+vSg10uSRGxsLLGxsXTr1i3SKnc66D3PwwOLSpE/4VmHMXWI2XV9RrR25vYKNm5pYuMWB3cM0/HG80Yy0nxH8RNH61OKD7vWvrrYevPakoNsPn7Exdmh/otqdYlYAOrr3R4lpUJyCsGPVis5RqOqYyHZ88YdOXKEN998k3feeUf+7qabbmLx4sWcKwfFbrfjdDpVqerN2QDKT+XhdDpVbYD4+HjZBvBrzLDgtxinyu/+1wEPAvRO1Ik/XGHWGrQSN2bEsKyojin/LmflBjtfbHXw0hQj43+jR9ljvzLDOPStD21bG5vEVXVWq/ehfi2E2KxWlxYLgEPxJ6794Qe21NdzS0ICS3JySPCjE3gfYUVFBVOnTpXzr7nmGj744IOgD9FisTB69GhOnz7tY+wpD7vdjt1u93+o7QaTycTIkSNZuHBh2Nco1gSAQgAkSTIAb+PW6rx5Y6pk0J5t3VG94xje08SMLeW8s7+GCTMbWfYvO4v+Ek1WhvwC6X42SMvGLT4TjapvP7TACPSSGZQCcF1cHFvq61lTU8MVBw7wz9xc+kSfneRyKcp636hBgwaxfPlyoqODT4aZTCbZCFT29cpDaQcIIQL6/aioqHO2AbyHd6FKZmYmubm55OTkkJOTQ0xMTFj388JPAJScvRlAPsDYSxO4Jj3w2cTrNcREaUg2ajHpJDZucdD/lno+ei2GX1zv/l9l5aK/4pK9Qog1weoSsQYweAw6u6JR/9y9O4VNTXxSVUVRUxP/9cMPbMzPZ4DnwexvbPS5R2ZmJsuWLSM2NrbZ3xs1alSkVez0UNMAkiRdCkwFSIuJcs7+WRdVUs620iZe21PF/Ou7cndOLPevK2XjsQbunmARaxebpC5JGr4rcioXmwRY/kpE3PF6LVilAGgliQ9zcrgxPh6ASoeDnxcW8l1jI4VNTdxRVCSXjY2NZdmyZaSmpkb60xcM/ChhjZIkaYH3cK+WZsENXbXx+sCmaXIKHtlQRv8uBh7sm4DZqGXl7d0Z0yeeJivSLQ82OJ95uUl5ySH8HD/+aLEGsPn1uQZJ4p+5udxYWMi3FgtnHA6GFRailyTKFSTI2bNn069f+Ativ/nmm2ZtAO+5mhEYzBj0NwLVDMFwjMCkpCTy8vK46667wv5Pft1eHO71mFcA3JUTK27vZVLtg/+0rYKD1TY23J2BxlNCK8GC67uy+4yVveVW7T/X+wjXTCFESIq/UgDCmgL0Gmx2FaMrTqvls7w8rv3+ewqbmiizB9LdBgwYEM7PAG4j8Omnn+bo0aPU19f706k6DHFxcbINEBcXF1ZXpkT37j7kjKuAmwFidRrHK9d1VX0pd5Q1sWB3Fff0juPqbr62gUEr8ffhaVy19ChNTrldvhJCvN9cXZQ/JuucUEagVwNYg3jmUqKiWJefz93FxXxrcQ9x47Ra6jyNl5OT01ydZJhMJrZskT1ZNDQ0BDUC6+vrcblcQZ0/SgdRuKTnYEZgQkJC2P9BDV26dMFgMGC1WgHu8Oa/fG1KVGpMYN2sHtUfHaXhL9eoex2P1TuUjW8Hfh9OXSLWAF4BqAnxNvbQ69napw+vnz5Nd52ObIOByw8cICEhAbM55OxnSMTExBATE3Pe2w+SJNGtWzcOHz4s592YEeP4bZ941bd/1vYKfqiy8caNXUk3BRaptroYt8HHxf8/Qojvw6lLxALglf7KZtRxlCQx2dNQ4zx/tH///iGu+GkhPT1dFoCYKMn516Gpqo2/rbSJV3dV8YueJh7sq655Jm86zUmLbGcdAWaFW4+IBSArKwuADyoq+K/YWAbFxBAlSdiFoNRu55Tdzhm7nTqXiyqHgxVVVayvrUWr1fLCCy+EW68LGvX19Rw+fNiJJwrLX65J0faIC2z/yiYno9eeItGg5a9D1bXeiuJ6lh6sU2Y9JoQITs7wQ8QCMHLkSKZPn84Oi4WCAwcAiNFoaHS5COaLS01J4X9eeYXBgweHW68LGtOmTePkyZNagGvTo8XDlyYEPHuXgAc+L+V4vYMPf9GNriq2QVmDk0lf+dDBVgkhVkZSF43aeSgjMCkpiW+//ZZ77rlHLteg0vjR0dEMHz6cOXPmsGvPHn75y18G3uwniLVr17J48WIAjFGS669DU1Wf9os7K/n8aAOjL4nnzmz1Ucb4jWVUNsldcQPwWKT1iVgDAGRkZPDee+8xdepUSkpKOHPmDOXl5SQkJNCjRw+ysrLIyspq0UTJhYyqqiomTpwop2dd1UWTnRA4rbzhWAN/3lFBXqKeV65T51K+d6CGfx3xmUic3ZJYQy0SAC/y8/PJz8+P9LKfLCZPnkxpaSkAV6UZmXBZ4JT6iXoHD3xeikEr8b8j0ojVBXoED9XamfqNT2CxncDcltRJVQCE22V2kRveili+fDnLly8HwKCVxFvDUiWN3xO2uwSj156ivNHJGzd25dJkQ8B9XAIe2VBGvV32wzQA9wkhWhR4SNUGcHUw/8pLOrlQUFpayuTJZwOwPVeQLOUlBnaPz2wuZ2tpE/+dFxd0yPfG3mq+OekzufaEEOJgS+sWTAPIQ5RQsNlsrFmzhq1bt3KwsJCiwkJsVit6rRa9TkeUwYA+JoZe2dlMnDiRK6+8MuT9jh49ypQpU1i9ejUrVqxg+PDhLf1fnQoTJkygqso97V+QauTxQUkBZT4uqef1PdX0TtTz+g3qiz0PVtt4bquP6l8lhHjrXOrWYhtgzpw5vPnGG5ypqGi27K7du/nss8/Ytm0b2dnZqmXefvttpk+fTmNjI0lJSVx66aWRVKfTYtGiRaxbtw6A6CiJRTelofV70sXVdsZvLMMYot93Chi7vowmhzzeKgMeOtf6tUgApj/zDPMXLJDTMRoN2QaDTAtXUsRP2Gw8cPgwRxsbVQXA5XIxbdo0H0bNwoULSU9Pb/m/6iQ4dOgQzzzzjJx+4eou5Cb6Wv2NDsGv/3WKWpuL12/oSn+Vfh/glV1V7Cjzmep9UAhx5lzrGLEAFBcXM3/BAiTgHrOZ57t3J9+X5eqDPKORR7t2ZcqxYz6+b3Czg8eMGcPKlWd9F2PHjuX222+X05s2beLMmTM+6wFsNltAWkkLCzUVHIz753A4sNvtcp4/xUyn02E2mzGbzSQnJ5OSksKDDz4YlB3scrl4+OGHsXgmxIZlxjCuf6DV/8zmcvZXWPlV7zge6qfe739XaeP5bRVKw3xhKJZPJAg2GxhUGD744AMAxnTpwuJevcL6kb4eATmiWEkkhGD8+PE+jd+3b19efPEsfc07HXzs2DF5tq+joNFo6NGjB3l5eRgMBjIyMkLOKr766qts3boVgASDhreGpga8YV8cb+CtfdXkJup440b1ft/hEvxuXand4RJeSfuBEPGcI4WqBgjF0D169CgAD0SwGCLT4xBSCsBTTz3Fhx9+KKd79erFsmXLfDjzJpOJbdu2AW6B8a4F8B41NTXU1dVRU1ODzWaT+X/KqWD/iODhrHzWaDTy9K/3iI+Pl2dCm8N3333H7Nmz5fSr13Wle6yvr7/W5mLcxjKiNBL/GN6NOJV+H+AvOytd+yqs3sa34x7yNaoWbgFUBSAUadLLCk7xK1NitXKgsZHbEhMDJD3DIwBe4Zk9ezZvvXXWeM3NzWXNmjUh+33vkqy4uDh/QkWngs1mY+zYsTLv766cWO7NC1xA8/Q3ZzhW5+APl5sZkKIuWLvPWHlxZ6Uy6zkhxH9as76qAqDV+tupZ6FGCwe4/ocfOGGz8YuEBP6RnY1ZISDRHo1y4sQJ5s+f76PmL7nkEj799NMWzfFHagMIIYKuBXQ6nWHbAGazOSgT+M9//jP79u0DIDVGqzqk+/Swhb9/X0s/s55pl6vzI2xOwZh1pVaXwCsdm4A5ET+kZhBMAIJe0OBZBu4vAPcnJ/PiqVN8VlPDPSUlrMvPl40Kb8/tcDh8VgD16dOHzz77LOjaOovFwq233upjBFqtVpkH2FHQaDT07duXL7/80qfL2rZtG6+++qqcfnNoKmaj77OsbHIy8YsytBK8NSwVfZB3bdb2Coqqbd7GrwHuFyLMcCARQNUIDCUAXqvX3+/454wMdjc08K+aGjbU1jLr5ElmelS6U4U/2KtXL1atWhVyYaXJZGLatGkcP37cp+/3twFqa2uxWq2q/b4yT6/Xh7UCybvziNIGSExMJC8vj7y8PHJzc/3X92GxWHj44Ydl3uLv+ibwi56BWwE8vukMZQ1OnhycxJCu6qOnHWVNzNtVpbT6JwohjjZb8RZAVQPodLqgXYDaugBwS8+HOTlctn8/R202nj9xglyDgfuSk9lU50NYID09nVWrVpGWltZsBUeMGNFsmc6A6dOny4tds+J1zPmvQMFeVlTHsqI68hL1zChQXxTc6BA88HmpwyXktvlACPG/bVVv5esQVhcg08JVhmQJWi1/69ULCfda5NE//kjevn3cXXx2d7Tk5GRWrVolM4suBGzdupVFixYBoJHg3WGpAd68sgYnj286g8aj+o1BVP/MreX8WGP3Nv5RYEIbVj1yIzAULRxgaHw8j6em8mqZm6RY1OTjvWLu3LkRTSGvXLmSM2fO+DiAlIEivOlzNQL9j+aMwJSUFCZPnoxGo2HSpEly+d/3T1Rd0jXxCzd549EBiVyVpq76vznZyOt7q72q3wWMaS7c+7kiYhsg2MIQJeb16IFOkpjrmftWYuDAgWFXzmKx8Nprr3HixAm5v+8oZ5DJZCI3N5ekpCSSk5PJz8/HYDAwd+5cvv/eTcBNN0Ux86pA1f5BYR2fHraQnaDj+auCGLx2F2M3lLmEkNvhZSHEV6qFWxHB/ADN2gCWEA0hAXMyMxlkMrG2pobeBgMzTpzAaDRGvC5AucWaEIKGhgZqamqCGoFqhp/SQRSpERgdHS0HifDHoUOHePHFF2Vjbd51KQEOncomJ1P+fQYJ+OvQVGKCPNpnNpdzpNbuvXgn7oWibY6IBcA7/q0MY+OjX5vN/Nps5v8q3c6M/Pz8c9raTZIkTCYTJpOpU0wWPf7448JqtUoAt2SZVLl70/5dTnmjk3GXJnCtStcAsOlEI+/slzkQdcC9LYki3hJE7AfwBjj6wa9vV4NDCD6prmasZxLonnvuaVktOyGWLl3Kxo0bJYCYKIlXrgt0+Hx5vIH3f6ilZ5yO2Verq36rU/Dol6eFOPv8ZwghSlQLtwEiFoDhw4cza9YsXisrY3N9PZdFRxOj0VDrdFLmcFBmt1PhcFDnclHvdMo+gBEjRjBp0qQ2+yPtifLycp588kkHnuc3oyAZf15/k1Mw6avTSLitfrU5foA531ZSVG3zPvsy4B3Vgm2EiI3AQYMG8cc//pG5c+fyrcUir/8Lhry8PJ544gnuu+++CyYE7ZNPPimqq6ujAPonG5g0IHCa98WdlRRX2/l9/0Su766u+gurbMz91sfhs6A1J3rCQcSTQeCeyRs9ejTz5s3zoYXHx8fTs2dPevbsSd++fRk2bBgX2vZza9asYfny5RK4H9hrN3Qlyo/d+V2ljXn/qSInIbjqdwmY8MVpl90llKrhR9XCbYiIp4O9SEtLY86cVp+b6NSora3l0UcfteMJ5JCTqKPO7sJid2HyqHiBe8zvFIK3hwW3+uftqmLzqUb/B90qsZsjQcSewJ8yZs2axenTp2UKUHG1ndtXniDrb4f42sPUfWd/DdtKm3jk0sSAdfxe7Dpj9TJ8/BH27iGtBVVa+EUBCITD4WDJkiWqS6Lr7S5+9elJSmrsPLulnCSjlucK1Kd5LXYX96895XK43PF9/SaV2l0AIh4F+MPpdHLo0CEKCwuDhovPzs7m2muvPa+NwK+++oqamhotQGKciWcfHoXRoOOpeX8XjVabVGNzcfMnx6m1ufifa1NIMqo/w6e/Kaekxu3wMZvNrsmTJ2sUIXLPHwE4fPgw7733Hv94/31On2menFpQUMCaNWsCplHPFyxfvlzu+1e+OpWrB7jnM/YUHpbeXrEecC/rArdPf2CKIaALWFJYx98OuB0+kiTx7rvvaiy+o6gO7QJkAWjO375z505+VlDAvHnzAho/TqtlQEwMv0xK4v+lpnKP2YxOq2X79u2sX7++VSvfXnA6naxYscIJ0L93D7nxAQbkZwWU/7iknttXnmBfxdlNQfdVWJn4ZZnc7z/99NMMHz7cn1Xc7m+HqgYItb+9xWLh7jvvpK6hgViNhompqQyKiSHbYKCXwUAXlSHkQ4cO8V55eQAt/HzB119/TX19vRHg3puv8fmub68M1WsaHIL7/lXK3vt6UmNzce9np2hyuPv9O+64g2effRbAn2jaoV2ArA1CCcDy5cuprKmhf3Q0a/Pz6RZG1OybEhLOawFYvXp1DZ7Q7SOu9p3NvHZwHx65+ya+3vU9V/TLZem6zVhtbjd+UbWNw7V2nvr6DD/WuPMGDhzIu+++K9tDfpNMncMGCBWOzct1fyw1NazGB+Ryp06dakEVOx67du1qxCMAvboHklcXPvOwfP7I3Tdx15NzOVPlDtP+4PoytpxyDxG7devGRx995EMo9XvZOocAhNIAdR5619URxMbzCkCpCj8gFJxOJwsWLKCuro7GxsaAw0sCEULIu4Uoo4H77yjiJY54bRxJkjCbzaSnp9O9e/egAZ+Lior0AIlxMa54U3RIL9lVl+Xx2tSHuHfaKwBy46empvLxxx8HzGI2+obR7fwC4LVaIwnZmOYRgLKysmZKBv7WF198walTpygvL6eysrLVgkUajUZiY2NJSUlh8ODBFBQUqJazWq1UVVUlAvTu0S28UHp6XzsoNzeXTz75RJUG1+Q7q9r5bYBg6wJCwbt/UKQCEB8f77N0TAhBVVUV5eXl8j5CRqMxYBWP0WgM6s7WaDTExMSE7esoLi5GCLe/vslmlzZs38fQKy5V9Wm4XIKXFn/C82/9n5w3ZMgQ14oVKzR+28TI8BOAzj8K8HLfIhEA76CysbGR2tpa4j1BpSOFV2WbzeZ221ls//79sgG4r+goN0+YTe8e3fjbHydw1WVn63CqvIrfPvc6G7fvl/NuueUW1+LFizWhwsn7CUC7e8oiFoBgtPBQUO4XUFZWFpEAOJ1OmpqaaGxspKGhQT4PxwbwDybdnA2gRlfbsWNHOR4ByM/LxdLQQNHRk9wy6S988c5MBuRl8a/Nu3nguddFeXWdBKDRaJyzZs3iscce0zbn/fSzAdqd8KgUALlzDdXPtkgAFOelpaX07t07rOsqKyvJycnxD6/e6jAajQwcOFDVUbVr1y65+mlpXYmLiyWlSzJ79uzn57+fxV03Ftje+2Sj3svoSUpKqv7444/jL7/88rBC8fsJQLtve6IUAPkph6MBQrGC/WFU9MenTze/z60XZrPZxwj0P87FBvBuQtW3b18uu+yyoCHtiouL5eFOTIzbRuvevRv19RaKin9k0Scb5QuHDRtW+f7775sj0XB+XUCHagB5sV0oAWhuXYAaDJKEVpJwChHxrl8DBw6MiEremrDb7VRWVqYAGDxRxl0uF8XFhyj58bBcTqvVOl566SXn+PHjI46E3Zm6gIg0QFME/PwaDzdQkqTzKmD0kSNHEEJEAUTHRFNWdoYD3xdisZwNxdurVy/HkiVLovr3798iMkdnEoCwNIB3Nq8qDFq4Fwc9ai4rK4u4MDebBPfy7xdeeIHa2lqampp8jMCGhgZ5BU+4RqDNZpO3hAVfIzA9PZ0lS3z2VfaZuaytrWPHzl0+399///28/PLLUSZT4CLQcOHXBXR+GyApyR3irLlw8V7YhOAPir2BI4HdbqekpMTHBqipqTmnLeIkSfKxAfr160dBQYFqCDvl2F05QxofH89rr73GyJEjW1wPLzqlDRBqFODd72dBWRmJnqlfmRZut/vQwqscDlbX1HDSZiMxMZE//elPEVXOZDLx/vu+u544HA4qKip8jEA1Q/BcN6IEd8DradOmyQEttFoto0aNYubMmfTo0eOc7w+dSwDC0gC33347OTk5lJSU8EiYs3tXXnkl8+fPb5XQLlFRUaSmprbbriEzZsygX79+2O12Bg8eTG5ubqve32+I26FdQNg2wI4dO1i4cCEvvfSSPDnkX6ZPnz4MGzaMm266iWuuuea8poNFsiNYpPDjA3SOLiCUAIB7KDh58mQmTJjA6dOnA9YFpKamntcN3p7wcxN3qADIP96cAHih1+vJyMggI0OdFXMRzcNvD8F2FwBVWni4AnAR546OJsmqTga1NAhDRUUFNpvNZ6+9cDdq/qnCTwN0DkJIuKSLw4cP8/e//53Nmzfz/XffUVFVFVBGo9GQnZ3NE088wZgxY865whca/ASg5R6lFiJiQgi43Zdjx45l5cqVIZ0yMRoNTS4XxcXFTJw4kX79+jFkyJBzrvSFhM4kAGFrgN/cey+fb9gAQFedjr5Go0wLz/YcvQwGUnU6KhwOfn7wILssFvbt23dRp64OjQAABZ9JREFUAPzgJwDBmSNtBFUN0Bwr+PMNGzBpNPyxe3ceS02VKV9qSI6K4uEuXZhgsfgEi74IN/yMwM6vAVasWAHA+K5deSqMQI/g3ksY4LhnTiASbNiwgZqaGhoaGgKOUOFivbF/I2EFjx8/PuL6nSv8/ACd3wYoL3fvWfPrCDaB7uYRgEhp4ZWVlTz00EPyb7YFkpKSKCgoiHgL+NZCp9QAoYaBXlq4IYLJlpauCzCbzRw+fBi73S5PALU2IygtLa1DvZad0gYIZdm3hBae6KFgR0oL90Kn05GWlhZWbOHzDX4CoJEkySiEaD4EWytB1UsTijPvnb6MRAC8JauqquT4AecCLymkORsgEkJIYmJgoKf2gJ8AgLsb6BABkFs0lPfO23gtWRcghOD06dNhzx1UVVVx2223yUagxWKhsbGxTcLFpqamUlLSbuH5ZKgIwLkTGSKAakuHEoBzXRdQWloatgAkJiYybty4NmcFX3nllfTp0yfs/9OaUJkLaJcIoV5ErAFaQgvXKoysSAxBSZIuePdxRwuA8jWJqAuIRAPEtAI960KFyqKXdp2KbRcN4PTEQHdBxFvCrlixgurqahobG2UbwPvZmkZgeno606ZNi6hurQGr1eqf1WFdgIxw9guojWCZ9o9WKy7cbNpIdgqpqqri+eefp7S0FEszIWlbiqysLOLj42W2c3vDnxbeFhtDhULEGsDrMQsnXLwXq6vdm15EuigkKSmJPXv2AO4ZyGBGoNIQ9Mb3D2UEekPOx8bGBt3+rb3gJwDt+vZDEAEIth8uIEe42FRXx++6dCHOT1tYhaDC4aDB5aLW6WRFVRUvekLDTJjQ8u1voqOjyczMvOBiD/t1AZ1DAEK5Ru+8805mzJjB8qoqNtTV0UuvJ0ajodrp5JTdrqoZNBoNTz/1FHfeeWerVv5CgJ8GaHcunqqeDOUKzsrKYtmyZfTu3Ztqh4NdDQ38u76e7xobAxrfbDYzatQoNm3axMyZM1u35hcIamtrlckO1QAymlt6dfPNNzN06FCWLl0aMlz8wIEDW2WFzoWM//zHZyvgQE5dG6PFjE2dTsfo0aNbsy4/OTQ1NbFmzRpl1p72rkPEXcBFtB4effRRDh48qMxaGaxsW+GiAHQQZs2axYcffqjM2gq02RaxwXBRADoA3nWVCuwBbm1vJxAEiRZ+UQDaDkuXLmXq1KnKrL3AMCFEZUfURykA7c5H+6lh/fr1jBs3TvmCFQM3CSEqOqpOFwWgnbB//35Gjx6tJNxWALcIIZrfbaMNoRQAmRZ7IXLvOhKnTp1i5MiRMp8SN+XrDiFEUQdWCwgiAD179uyAqlyYsFgsjBw5khMnTnizBO5t4Td3YLVkqHYBfqHLLqKFcDqdjBkzhr179yqzpwgh/i/YNe0NpQDIIlpYWNgmxMufGp588knWrl2rzFoohHi5o+qjBqUAfO092bt3LwMGDODNN9+kuLjYf8LiIsLAK6+8wrvvvqvMWg081kHVCQpJOeaXJGkZoBr8TqfTYTAY5D0BDQYDOp1OTnv3CQw37f+d/xHqWpPJRFJSUoct5xJCYLFYqK+vx2KxoNFoiIqKQqPRUFJSwsKFC12rV69WvlzfAtcLIdqG1nQO8BeAWOBZYDJwbqs32gFarZaEhAQSEhJITEwkMTGRhIQEWTi8AuuNVqIUXm/0Em9DBjvq6uqwWCzyZ319PQ0NDZE4yw7gdvREti6unSCp/RFJknoDTwGXAnmA+hbYF9Ec1gOjhBA1HV2RYFAVgIBCkpQIxAEGxaHDrSX0Qc4j+d6IexhqUn56NFKsEOJ82m7UDvwAfAZMF0J06ohbYQlAR0OSJA1uoUgAEoEkz6fy3PuZgDvYkgG3YCk/lecawALUez6V5/55DYAVd+Pa/A5l3mngByFEuzN7Wor/D76G2Hou4LZgAAAAAElFTkSuQmCC";
            doc.rect(78, 20, 55, 30);
            doc.addImage(attendance_ratio, 'PNG', 80, 26, 17, 17);
            doc.setFontSize(14);
            doc.text("Teacher \nAttendance\nRatio", 98, 28);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_attendance_ratio || 0, 112, 45);


            var punctuality_percentage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAEbAAABGwBr11x4QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7Z15dFRVtri/W1MgFGHIJDQsqEJGedgiIAEMyBAFpbuBhwoCPhuBx/gTaMT4lEFcAiKgyCCTPgbBJ0voJ70cABXCPDTd7fIBMlQRgUBmhiKBpKru749biam6pyp1qyoT+P2TlXNv3Tp19r777DPtLcmyzL2G1WypC7QC2gCtgRZAfSAGqOvzF+AmcMvn73XgAvAzcAY4a3PYb1Xer6gcpJquAFazJRroDvQGHkMReOMK+roMFIU4CnwPHLQ57AUV9F2VQo1TAKvZYkQR+BMoQu8CmKqoOkXAMRRl+AFFIYqrqC4hUWMUwGq2dAZGAc8DcVVcHX/kAJ8BG20O+/GqrkwwVGsFsJotTYERwEigbTjPkiSoXRvM0TLmOlAnWsYcrVxzFMDtAgnHbXAUSBQWQgSa5TSwCdhsc9gvhf20CqJaKoDVbOkAvAEMAXTBfk6SoEkjGWszNy2ay1iaybRo5sbaTCY+VkaSgnuOLEN2roQtXeJCug57usSFixK2dB2Xr0palcMNfAG8bXPYf9T0yUqgWimAx8y/AQwEghJXs6Yy3Tq5SOrkJqmTm9gGFft7cvMlDp/QcfiEjkMn9KRfClKrQAZ2oihCtekeqoUCWM2WrsAc4Mny7tXroFtnNwNTnHTv4qZRYtXW/2qmxMFjOnbuMnDouA6XO6iPfQvMsTnsRyq2duVTpQpgNVvigXeBFynnjW/Xys2gAS4GprhIiKt6pRWRlSOxc5eeHV/pOXW23J5LBjYAr9oc9uyKr52YKlEAq9miA8YA84EG/u4zmWDoQCejhrpoaQ3u1aounLPp2LhNz7adBoqKAt6aD6QCa20Oe6X/yEpXAKvZ0hFYhTJ+FxJdG4YPdvLyC85q+7YHS1aOxLpPDWzZbqCgMOCtx4DxNof9ZOXUTKHSFMBqtkjATGAeYBDdU9cs8+KzLl4a5qRBvZoteF/yb0h8stXAhs/13HL47e2cwJvAQpvDXikNUCkK4OnrNwJP+btn0AAXqVOKiWt4bwnel5w8ifnLjOz4Sh/otm+AUZXhG1S4AljNlmRgK37m51taZN6aWcRjHWtWHx8uR0/qmLXQxDm7X2uQAQyzOexpFVmPClUAq9mSimLyVepeuxZMGVPM6GFODMIO4d7H6YT1Ww0sW2uk8I7wFhfwps1hn19RdagQBbCaLXoUR2+M6PqDzWVWLLxLS8u9be6D5ZxdYuLMKM5f9GsN1qI4iK5If3fEFcBqttQCtgCDRNcHDXAxb2YR0bUj+rVBkX9DIitbIjNHIisbMnOUBk+Mk0mIL/krV4kDWlAIby40BfINdgDDbQ672FaESEQVwGq21AP+F+jpe61WFMydUcTQP0Rcib1wOuHI33WkHdFz5ZpEZrYi9KxcqbzxeCkmEyTEKsqQGC/zuwdkkru66Pqou8K7q21f6pm9yMSdu8LL+4A/2hz2G5H6vogpgNVsSUSZ4nzY91pivMzH7xfRtmXFOHoFBbD3sJ5de/XsPaTj5q2g5+c1EVNXplc3Nym9XPRKchEdXSFfw+lzOv78ionMbOHv+BfwpM1hz4zEd0VEATxv/j4Ewm/RXGbDsrs0fiCyZjUnT2JPmp5d+3QcOq4P+u2OFCYTdOvsIqWnm77JrogPXzOuSbw4JYoLYr/gX0DPSFiCsBXA0+d/g8DsP9LezbqlRRHtU//xk473Vho5elKHu5qMHHU6eKyjm79MKOaR9pGrVP4NiZenmvjHT8J1hX3AU+H6BGEpgMfb34bA4evV3cWK+UXUrhVG7cpw8ZLEopVGvv4u4ASKEIPBQMP4hsTFxxGbEEdcQiyx8bHEJigbi3KzcsjNziUnK5fcrBxysnPIy87D6XRq/q7+fVzMmFBM86aRUfrCOzAx1cTeg8LfvQMYGs7oIFwFWINgqNeru4u17xWh1y4rFXn5Eh+sM7B1h4Fg5CFJEm07tCW5bzJdenQhsfEDxNSPQQp2N4gHWZa5ef0mmRnXOHbgGGl70jj942mCaS+DQVnLmDLaScMI7E9wuWDMX/wqwVqbwz421GeHrACeSZ53fMsfae9m88q7Yb/5hXdg/RYDqzcauV3Ovluj0UjHrh15vG8yPfr0IC6hYrYM5mTlcOC7A+zfk8bJIycpLg68/7NONIwbVczo4c6ItMeICVH+uoPXQ50sCkkBPNO73+Mzw9eiuczna++G3ef/9Ws9C5cb/XnBgPKmJ/dL5on+vUnqmUQdc52wvlMrtx23ObzvMD98/T1pu9MCWobEeJmZk4r5U//whsD5NySeHSN0DF1A71CmjTUrgGdh55/4zO0nxst8sT48b9/lgreWGNm0LfBgu0uPLkx4dSIPtnkw5O8CKCwoRJIkaoX5ep4/c56V767g2IFjAe8bOdTJrGnFYXWNGdckhoyOEr0cGcDvtS4gaVIAz5LuV/is6tWKgi8+vhvWOD//hsSk10wc/rv/nTStHmrNhBnj6dStc8jfU8Inyz/hs4+3AvDvo/6dMa+E3I2WcuLQcVYuWsXZ//vZ7z1Jj7pZviC8kdHpczqG/DlKNFn0DTBAy1KyVgV4DWUXjxcL3whvhu/sBR1jppu4lCE2+Y2aNGLs1LH0faafZmdOxJVfrjAs5XncnnGkJEls+XYrTZs3DfvZsiyz52+7WbN0DVcvXxXe07SxzNrFRbRqEfoLs+1LPTPfFp6HSbU57AuCfU7QW649O3nm+ZYPGuAKS/i79ukZPDpKKPyoWlFMTp3Clm+30m9gSkSED3B43+FS4YMitKP7j0bk2ZIk0W9gClu+3crk1ClE1YpS3XMpQ2Lw6Ch27Qu9Lxj6BxeDBgjbfZ5HVkERlAJ49vCtwmcnz4PNZebNDH0K7sP1Bsa/aqJA4OXHJ8azYstKnnvpOYxGY8jfIcLlUjecqCwcjEYjz730HCu2rCQ+MV51vaAAxr9q4sP1oS8uzJtZxIPNVRbcAKzyyKxcgrUAY/DZw1e7FqxYeDfkVb3pc0wsXW0UHrJo16Ed67avp037NqE9vBrRpn0b1m1fT7sO7VTXZBmWrjYyfU5oRxujaysyEPiwXfCzFO9LuQrg8fpV/f6UMcUhr+d/uN7gd9mz38AUln+6gtj42JCeXR2JjY9l+acr6DcwRXh9x1f6kC1BS4vMlDHC+Yj5HtkFJBgL8C4+W7dbWmRGD9M+TQpKn//+GrVJlySJcdP/k9mLZ2OKqqrDvhWHKcrE7MWzGTf9P4W+zPtrjCH7BKOHOUUvYwMU2QUkoAJ4Tuy86Fv+1syikNbFz17QMW22SWX2a9WuxTsr5jNy3EjtD61hjBw3kndWzFfNPcgyTJtt4uyFoP3yUgwGRSYCXvTI0C/lfdscfE7sDBrgCmkDZ/4NiTHT1Q6fJEnMXjyHx/s+rvmZNZXH+z7O7MVzVJagoADGTDeRf0P7aOexjm7RqEBCkaFf/CqA56Cm11m9umaZ1Cna4x+4XDDpNfE4f+y0cfeV8Et4vO/jjJ02TlV+KUOZEAtlUJI6pZi6ZlVX8KRHlkICWYA3fAtefDa0jQ9vLTEKZ/j6DUy5L8y+P0aOGyl0DA//XcdbS7QPfeMaKgdrBKhkWYJQATzn8weWLYuuDS+F4Pj99Wu9cG6/XYd2pL6Tqvl59xqp76QKh4ibthn469fancKXhjlFQ/OBHpmq8GcB3sCn7x8+WPtxrcI7sHC5WpPjE+OZv2rBPenta8UUZWL+qgXCyaKFy/2eF/BLg3oywwerXlQJP1ZApQCesCxDvCppgpdf0P72r99iUK1aRdWKYv6qBffUOD9cYuNjmb9qgWraODNbYv0W7cOtl19wYlK/W0M8svVCZAFG+JYPHaj9lG5evsTqjeq3f+zUcffEDF+kadO+DWOnqp3C1RuN5OVrGxUkxMkMHah6YXUoslUV+qLyykYN1e6SfrDOoNrJ06hJIwaPGKz5WfcLg0cMplGTRl5ltwuUttSKH5mpZOulAJ7hglc0rnat3JqDM1y8JLF1h7rSY6eOjfjCzr2E0Whk7FT1voStOwxcDD4WEQAtrW7atVLJra3vkNDXAozy/YSfJceALFppVG3gbPVQa/o+00/zs+43+j7Tj1YPtfYqczqVNtWKH9l5ybhUATwROJ8ve1Gvg4Ep2hTgHz/phFu3J8wYH7H1/HsZSZKYMGO8qvzr7/T+NoT6ZWCKC736I897ZA14W4Du+ETg7NbZrdn5e0+gqV16dInINq77hU7dOtOlhzqCjqhtA5EQJ9Ots6obiEORNeCtAE/43jkwRdvQLydP4uhJb5WTJIkJr07U9JzfgAmvTlRZzKMndeTkabOifmRYKuuy0urte1f3Ltqcvz1petVxreR+yWHv3r0febDNgyT3S/Yqc7uVNtaCHxmWyloHpSHXvWxOs6ay5iCMu/apO5wn+qv06jeCRNR2ojYORKNEmWbqY2pdPDIvtQDd8Qm53q2TNuevoAAOHffWTqPRSFLPJE3P+Y1fSeqZpBo2HzquF+6hDIRAliY8fkCJAqhULamTNvO/97D6iHbHrh0r/cTOvUQdcx06dvXe4FtUpLS1FvzIsjf8qgCPlb0iSdoVYNdedaUe75ssuPM3tCBqQ1FbByKpk1sUKf0x+FUBvGYemjSSNUXddjph7yG199+jTw9NFf0NNT369FCNBvYe0gV1UrqE2AYyTRqp5NkaQOdJsOR1zs/aTNvbf+Tv6rAsbTu0rbBTuvcTcQlxtO3gnSvj5i2JIwGO0IkQyLSx1Wypq0PJruVFC/Vhg4CkHVGbpOTfzH/EELWlqM0D4UemrXQoqdW8sDTTpgBXrqk7GNFMVlVz6+YtNq/ZzNZ1W1TXPlu/lc//+38oLCeic1UgaktRmwfCj0zb6PDp/wFaaOwCROf4Exs/oOkZFc0Xm79gcPIgPnpvFTlZOarr2ZnZLHtnGUN6DmbXl99WQQ39I2rLQLETRPiRaWsDSlJFL6waLUCWT2UMBgMx9WP83F35rFm6ho2rNgR1780bN5k3Yx6FBXf44/N/rOCaBUdM/RgMBoNXzCLfNi8PPzJtoUPJqFmKJEF8rEYFyPWuTMP4htVm5W/TRxuDFn4JsiyzaNa77N65q4JqpQ1JkmgY39CrzLfNy8NP0qz6On5NnwooqdW0yC7/hjoCZ1x89fD+83Ly2PjRRuE1azOZwU87Gfy006/FW/nuSu76CdlZ2fi2aVERmg6QlKTN8yHGgJJDtxRzdHjmHygNv1bVfPzhxyqnrnYteGNqMcMGlR1IF7N1h4G3l3rvws3OzGbbxm2MGKvaSlfpiNo0K1vStFPbHC1TUOAlr7oqC6B15rYk4HJZ4hKqx47ftN37VGVq4SsMG+TkjanqU097/ra7QuqmFVGbito+EALZxujwsQB1NFsAdVl12PJ988ZN8nLyvMqszWSh8EsYNkjdHVz55UqF1E8rojYVtX0gBLIVWACNAZBFWlgduoD0CxdVZb9vX/4Kp+89hQWF5GbnRqpaISNqU80WQC3bGO1nkWsIbnfk4hO7g8wGWRPRATfLFjg0rjUnCvYM5gomWiqbJs2aqMr++VP506e+99RvWJ/4B8oNtFHhiNpU1PaBEMj2pg64VbbkdoE2s5IgaJtqYTLjY1X9pi1dfF6hhK07DNjSvX9/dTnFJGpTUdsHQiDbW2oLcFvbQ0VamJNV9QoA8MzQZ1Rlby81CpWgZBjoS7uH1Sd3qwJRm2q2AGrZ3jTgYwEcmi1A9ewCALr16s6Gld6zgIV34L/mG1m/xVDq8P3zJ73qzQflFPOz//FcpdS1PERtKmr7QAhke8uAjwUoLFTi1QQ7G9ignozJhNdsYE521StA1tUs5k6f6/e6LV3Clh74zF3q/Ncx1zVHumoh4dumJhOaJoFkWZGtDzd1wHXfG7M1zjMn+Kwd5GXnBRVXv6LIzMhk0oiJZFwKfQz/7H88V22WtGVZJi/be07Dt83LIztXEsVkvK4DLviWisxhIHxNkdPp5Ob1m37urliuXr7qEX5GSJ+vG1OXWe/NYsrrUyJcs9C5ef2mKnuJVvPvR6YXdIAqtPWFdG3TA4mCymRmXNP0jEiQcSmDySMnCYM0N2nWhHnL3qZHnx7oBfHa6zesT58Bfdj01WZS/vCk6npVImpLUZsHwo9MfzYAZ3xL7RotwO8EOQKOHTimOuVakVz55QqTR04i62qW6lrT5k35cPNy4hLieOKpJ7iRf4OLFy5yJf0y5pi6tGnfhoRGCZVWV62I8hCI2jwQfmR6Rgec9S31k6rML8ld1VOsaXsqNOexF1d+ucKkEROFwm9mbcbyT1d4bVCt16AeD3d6mAFDnia5X3K1Fj6I21LU5oHwI9OzOpvDfgsl20QpNo1dQNdH3cTU9dbI0z+eFm69ijSX0y8z6YWJZF9Tr4xYHrTw4ebl1WJxKlRysnI4/eNpr7KYujJdH9U2PS2QaYbNYb9VUurlB1y+KpGrIS6NwQC9unlXSJZlDnx3QFMltfKL/RdF+Jlq4VtbWVm26UMaxjUUfLLmcOC7A6oRVa9u2lLY5uZLXL6qkufP8OvBEK9sCbIMh09oswIpvdQmaX8FdgPptnQmj5gktDItWrfgw03LaRDbQPDJmoWoDUVtHYjDJ3SiIeBR+FUBvhd9SAu9klyq0GQnj5zktta55SCwn7czecQk4fx4y7YtWbbpQ+o1qBfx761sbjtuc/LISa8yk0lpay34keX38KsCHAS8dvYdOqHt4EF0tJJLtyzFxcUc3ndY03PKw37OxpSRk1WbPQBaP9SaDzYuo179mi98UFLb+OYm7NZZe9JqgSyLUGSuKIDNYS8AvMYa6ZckrmZqGw2k9FQ7Jj98rTIuIXP18lUmj5xMfm6+6lqbf2vD+xs+IKZe9dmOHi6ithO1cSCuZkqkqyOMHfPI3CtCiOrbDh7T1g30TXah8/lI2u40zp85r+k5/tixZTvX866rytt2aMsHG5ZRN6au4FM1k/NnzpO227v/1+mUNtaCHxmWyrrs1R9879q5S1uAwriGsiqXgCzLrHx3habn+OPsKdWUBe0fac/7//3BPReHYOW7K1Te/2Md3ZqjtfuRYamsyyrAQcDLpT50XEeWxn1nf5mg3ll77MAxThw6ruk5Inr37+P1/7917MCSj5fec8I/cei4cPZP1LaByMqROHRcZQFy8PT/UEYBbA57MfBZ2Ttdbti5S5sz+Eh7N/37qM3UykWrwl4hfHrI0/zXwjfo2jOJqbOmseTjJUTX0egRVXNkWWblolWq8v59XDzSXlv/v3OXHsF2xs88sgbUkUJVx2j8ZfcKxIwJxaqJirP/93PYe+z1Bj39B/XnvbXvMWTEEGqHmrOuGrPnb7tVqWcNBqVNteJHdl4y9lIAm8N+HPCadzx1Vsc5mzZnsHlT8f77NUvXlJty/X6muLiYNUvXqMqHDXLSXB3pKyDnbDpOnVXJ7bRHxqWIJLvJt2DjNu1W4P+97MTXOl+9fJXtm7drftb9wvbN21VL2XWilbbUih+ZqWQrUoDNgFfPsW2nQbMz2LCBzLhR6rd9zdLVnPlJtQJ933PmpzOsWbpaVT5uVDENNcRrAsX527ZT5f27UWTrhUoBbA77JeCLsmVFRbDuU+0x60cPd6o2Lty9c5fU8a9Vi63j1YXc7FxSx7+mOomcGC8zerj2t3/dpwbViW3gC49svfDXub8NeEluy3aD5nx2tWvBzElqK5CdmU3q+Ncouht64ul7haK7RaSOf024ojlzUrEoL3BA8m9IbNmuelllFJmqECqAzWH/EdhZtqygED7Zqt0K/Km/i5FD1Vp86sdTzH9dlZL4vmP+6/M59eMpVfnIoU7+1F97roZPthoQhDna6ZGpikDuvUpjNnyu1xytGmDWtGKSBBsYdu/cxabVKr/kvmHT6k3CKCRJj7qZNU37aCknT2LD50LnT/j2QwAF8AwXvKIl3XJIzF+mPXOFXg/LFxTRtLHamVmzZDX79+zX/Myazv49+1mzRO30NW0ss3xBEYJ9q+Uyf5mRWw7VC/qt79CvLMHkDvaS2o6v9KqcAMHQoJ7M2sVFqqVMWZaZO33OfaUE+/fsZ+70OaqZ0ehoWLu4SHN+RlByCQgmfmRCzR0MYHPYjwCqCEuzFpo0hSotoVULN0vmFqlOHd0pvMPrE1Pvi+5g0+pNvD4xlTs+GSElCZbMLaJVC+1H0Z1ORSYCNnhk6JdgXuVXAa8F+HN2ifUhOIQAKT1dvDJW3b/JsszqxR8xd/rce3J0UHS3iLnT57J68UfCNZFXxhaT0jOEjNHA+q0GztlVpj8fRXYBKVcBbA57NqBK8rtsrVH0pUExebTTbzay3Tt3MemFiffUPEFudi6TXpjoN+zcoAEuJo8OwaSivIzL1gr9slSP7AISbGe+Fp8dQ4V3YOLMKNGQIygWzyli6rhi4SHUUz+e4uXBo++JGcMzP53h5cGjhUM9SYKp44pZPCc0i1dQqMhAkF/4GIrMykUKdonWarZ0RNlJ6mX7Bw1whfwDAHbt0zNttkmYBSOqVhRjp45j8IjBNS7hZHFxMds3b2fN0tXCWIPR0UqfH6rZB5g+xyRy/JzAYzaH/aTgIyqCVgAAq9nyGqCavVn4RhFD/xD6Dzl7QceY6SYuZYi7lEZNGjF26lj6PtOv2kQg9Ycsy+z5227WLF0jPKMIylBv7eLQHL4Stn2pZ+bbQscv1eawLwj2OVoVQAK+Ap4qW14rCr74+C5tW4b+g/JvSEx6zcThAHHwWz3UmgkzxlfbHIQnDh1n5aJVqvX8siQ96mb5gtCGeiWcPqdjyJ+jEBiWb4ABNoc96IdrUgAAq9kSD/wTnyQTifEyX6y/S2ONhxbL4nLBW0uMbNoWeITRpUcXJrw6sdqkozt/5jwr310h3MZVlpFDncyaVhzSJE8JGdckhoyOEkULzwB+H4zjVxbNCgBgNVuSUXaWev2UFs1lPl97NyztBvjr13oWLjcGDIkuSRLJ/ZJ5on9vknomVfq+wNuO2xzed5gfvv6etN1pAbe7JcbLzJxUHNLcflnyb0g8OyZKdNDTBfS2Oeyaj2KFpAAAVrMlFXjHt/yR9m42r7yreRXLl8I7sH6LgdUbjao09L4YjUY6du3I432T6dGnR4WlqsnJyuHAdwfYvyeNk0dOlru7qU60sp4/ergzIu0xYkKUv/zBr9sc9pBW1kJWAACr2bIGGONb3qu7i7XvhTaf7UtevsQH6wxs3WEIavZRkiTadmhLct9kuvToQmLjB4ipH6PZeZRlmZvXb5KZcY1jB46RtieN0z+eDmpjq8EAwwc7mTLaqXkzhwiXC8b8xcTeg8IGXWtz2NU554MkXAXQA9uAQb7XenV3sWJ+UdiaX8LFSxKLVhqFmcnLw2Aw0DC+IXHxccQmxBGXoMQQLAm/mpuVQ252LjlZueRm5ZCTnUNedp4qLEsw9O/jYsaEYs17+PxReAcmpvoV/g5gqM1hD7lvCUsBAKxmSy0U77On77VH2rtZtzQ8j9eXf/yk472VRo6e1KnyFFcVOp1yaOMvE4o1b90ORP4NiZenmvyZ/X3AUzaHXT0NpIGwFQDAarbU81ToYd9rLZrLbFgW3uhARE6exJ40Pbv26Th0XJ21tKIxmZSDmik93fRNdmk+sVMeGdckXpwidPgA/gX0tDnsN8L9nogoAIDVbElE2T+gUoLEeJmP3y8Ka54gEAUFSjrVXXv17D2kzmEYKWLqyvTq5iall4teSdpP6QbL6XM6/vyKyd8o6F/AkzaHPTMS3xUxBYBSS/C/CLqDWlEwd0Z4M4bB4HQqiSzTjui5ck0iM1siK1siK1ed2sYfJpMShy8hXiYxXuZ3D8gkd3XR9VFtkTlCYduXemYvMokmeUCxsn+MxJtfQkQVAEp9gi0IHENQ1g7mzSyiKg715N9QlCEzRyIr+9d4+4lxMgnxJX/liPoswVJQCG8uFM7tl7ADGB5un+9LxBUASkcHqxAMEQEebC6zYuFdWlqqLppodeKcXWLizCjO+4/OthYYH463748KUYASPJNF8/CZMQRly/iUMcWMHuascLNaXXE6lc0cy9YaRUu6oMzwvRnqJE8wVKgCQOm08VZ81g5KaGmReWtmkSquwL3O0ZM6Zi00BdpUkwEMC2V6VwsVrgBQuoC0EZ9VxLIMGuAidUpxxIdT1Y2cPGVndTmnrr8BRmld2AmFSlEAKF1KnonSJQiNfl2zzIvPunhpmLNKHLGKJP+GxCdbDWz4XC/aul2CE3gTWKhlSTccKk0BSvDsLFoF+I3FHl1bmUt/+QUnCRqzYlQ3snIk1n1qYMt24YmdshxDcfSC2skTKSpdAQCsZosOZYQwH/AbzdFkgqEDnYwa6qKltWb5COdsOjZu07Ntp/CgZlnyUTbdrrU57JX+I6tEAUrw+AbvAi8CAafv2rVyM2iAi4EprmprFbJyJHbu0rPjK70oOIMvMsqZi1cro6/3R5UqQAlWs6UrygmWcgP163XQrbObgSlOundx0yixaut/NVPi4DEdO3cZOHRcJ4rJI+JbYE55hzYqg2qhACVYzZbOwBvAQMqxCCU0ayrTrZOLpE5ukjq5iY3A+nsgcvMlDp/QcfiEjkMn9KIgjP6QUU5cvx3orF5lU60UoASr2dIBRRGGEPzZBSQJmjSSsTZz06K5jKWZTItmbqzNZOJj5aATYZXkTbKlS1xI12FPl7hwUcKWruPyVWHunUC4UQJuvO3viHZVUi0VoASr2dIUGAGMBNqG8yxJgtq1lRTq5jpKIuWSXLqOAiWpouO2klqtJHNamJxGicmzWRSZo7pQrRWgLJ7uYRTwPFD12anF5KDEWtxYncx8IGqMApRgNVuMQHfgCaA3ynyC8IREJVCEMn7/HiX86sGyQRhrAjVOAXyxmi3RKArRG3gMaI2fdYcIkIGSaeMoitAPlkTdrqnUeAUQrZX8/gAAAC9JREFUYTVb6gKtgDYoCtECqA/EAHV9/oKSPfWWz9/rKDkVf0bJrHbWk1/pnuL/A6t7Ru6t8SvPAAAAAElFTkSuQmCC";
            doc.rect(138, 20, 53, 30);
            doc.addImage(punctuality_percentage, 'PNG', 140, 26, 17, 17);
            doc.setFontSize(14);
            doc.text(pdfDocVariables.punctuality_title, 140, 25);
            doc.text("Punctuality\nAbove 50%", 160, 31);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.teacher_punctuality, 164, 46);


            doc.line(20, 53, 191, 53);// horizontal line
            doc.line(20, 55, 191, 55);// horizontal line


            var adequately_marked_exercises = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADngAAA54BEWVixwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAArvSURBVHja7Z17UFT3FcdFRBCJYpRH6iOACZgo5oEao+CgQR5NDanTGG07VSsZrZMWOmnBpGlKJpNXH+JoFZ1uJpM0NU1boZUEa6JuYoqmNRYivuWhBEPNSiFGJSB4es8iHXbvj+V39/7ub+/uPWfm+9fufew5n733nt/v/M4dAgBD9EqxaEXrFbUqApJTZxU9pShChI+N0hBBAGyggA+oD80MgYjgBytqo0D7JwQiAAijAHNpvxkhIAAsDoFhANhsNjh48KClVFhYyAvByIAHAB1iNSstLdVyJRhJAFgXANQHZoCAADAYgKCgIFNDQAAYDEBcXBzk5OSYFgLpAJw5cwby8/O51Nzc7PcAxMfHw86dO00LgXQA7HY7932yuro6IACoqKhwQpCdne3p977vCwgIAEkAmBUCAkAiAGaEgACQDIAGCMIDEoDa2lrIzc3lUkNDQ0AC0AdBVlaWJwjsMiCgNNBHAPRBkJmZ6VMICAAfAmAGCAgAHwPQB8HChQt9AgEBYAIAOCHYZwQE0gHo7OyElpYWoWpvb/d7AHwFganTQF4tX748IADwBQQEgMEAhIaGQlJSkiYlJiYONosoDAICwHf1AHpVRACYDIBt27bJBKBeURABYCIA6uvrB7t0i1aY3wHQ0dEBjY2NQuVwOEyTCubl5cmEwP8AsIJh0cvGjRuhpKREiAoKCggAKxv6kgAgAAgAAoAAIAAIAJ12vQug6wLAlRMAXxwA+PKwkpqcBei+RAAEHACdnwE4dgA0Pg1wejXAsUcAjmQPrNpFACe+B1D3Y4DmTQCXDinAXCMA/AeA6wBXTwP85zUl4Gs8B5tXR78JcO4FgDa7cpW4TACYEwAl8G17AU6uEBP0Aa8Q3wD4dL1yC7lIAJgGALyHn15rbOBVV4WHAFpeBei5QgD4DICOeoCGp+QG3l3HlwBc/KvhzwkEgLt9/kclADm+DX5/4RUIMwsCwGAAejoBml40T+BdrgaPAlyuJQAMA6DLAXDmh+YMfv+HxNZKAkA4ADhoc3yZuYPfX+c3K88F3QSAEPvqXG8u7i/B7w8BAaDTcHj25ErTBTfx1pEQM3a4i/bZZqm/K+h2YE0A8BLasE5XoHpqsuDt36bAzx6bDJVbUoQBMDpimOr37iqdwX4mEPBgaE0Azm/RHagn8xJczunF/ES5APRlBzpTROsB8N89uoN0+u15EBTkek7BQ4Pg3O50uQD0jRPoGCyyFgA9V5V/zVLdQSr6fgLTaT9dES8fABSOGBIAHHbhDd0B6vp3lvPBjHVeY0aFwJV/LeTaT/OedMidH63SsGB11e/9d0WqvvfmL+9yHTb2cu7AOgBcaxOS8u1Yf4/HcurSp6dy7efE39J0lW3/4ge3ue4TJ5AIAE8PfpuFPKRlzx3nMTB3JETA9U98AADOInoxlWwNADrP96ZNOoOPD3lDhw6+MOPvW2fIBwCF9QQEAMNabEL+/eh0nuDkpEb5BgCEXGNlkTUAOLVKd/Bx4GdibJjqXFhXBEwRT+5M87i/C+8vgPzv3KrS8JChqv09vCBG9b0BMwMsLyMA+o/3Nwn597+zOYXpqJcKEpkQrH10krw0sL+wxpAA6F/g8ZYQAPBf6H4eUWOGQ+fhTOZnI0cEQ1vVA/IBwExHw8BQ4ANQl687+C375jPz8yeWxzk/t78yi+nEXz8xRT4AKCw5JwAw928VUt6F4/ys8+h/n5+eeJO6n//XRkC38uwgHQBcd0AAKHbpY93Bx5x+8sRw1Tmk3TvG5XuvPDuN6UgcONJyvIzZY+G+5EgXHXxjtrbzxsUnBABO/OzWDcBe20ymg15/frrL9zoOZcK4SPUQ8byUm+XXFeAKJAIAx/6363bm0uxbVMePvCkErh5Sj/ljbQCzM/mf5kiuH1xEADgN74U6HHlx/wMQOlydlz++jJ3ind87H0KGqR8WV+SOl38V4FyQGtgAnC3W5cSSwilM53zyl7kDbrMsR33FQIhw4EcqALgq2fIA6Cz1vnNyhOrYs6aN9rjNR3+YzXTos2tvkwsALmuzPACnHvPagVWvswP5u+Jpg247e3qkarvYcaHOQSNpAGB/AssDUF/otQPxvu1+3IjwYPjyo8ELPrBYg3Xev39hujwAcL2D5QFoetkr531xIAPCw4JVx81bPIFr+2vVWTA+Wj1xNGPqaHkAcBaLBjYAXk4DY1UP67j/3H4/9z6e/xF79PAfr90nBwBsV2N5ABzlXjnv3jtGqY6JQ71a9uHYvwDCQtUp5COZscYHH9vV0DiAYu37NTvv8FtzmA7Z9OSdmve1SrlluO8HJ5Wa3k03FgDsWUQA4MLPk5qdt2bJRNXxRoR6N7V7ZMdcpnMLV8YbCwA2riIAbiwBO/YtbsdhSfcoxmwcFoJiCZc3unuK+nZy82j+8nGvhN3LCIC+TOBX3I579blkae3Yt/58qnEAYAs7AqDvOeBDbsfNuTtSGgA4ymjM/X8NlYSploPVPjSo446Vp8p8GYNTu7fOEA8A9jEkANys8ZlBHVfw3TjpAHw9LUo8ANjMkgBwLwx5z6PTvvo4E8ZGhkgHAMvHT1WkiQs+NrPExpYEACMb8LA2YPvL7LH7ik0pzvRPhGrLUpnl4wPVFngl7GhKK4O0PwwumDVWdYyECeFca/y0CFf1siaY2g9kiOknqPHfb73l4YwS8bp31M0eUDiWL/r+jL1+WL/nNz+ZIm3+39oAXD6icty6VQnM4drP9s43JE1Lvl1dPh4/foRz6ZnX+8W2ttQgQntGgNO2WKjhvn+8VBs1SIMFJazfVL7hHi/3mdPb25gA4DRcQ3/8207nlZWwmz1g9y+jAMBqYlbGkT7Ty/Jx7G2sw6zZJQwniWoXOfNw931PiAnTvJpHb4exPtX8ea62fWFvY51m2UaRTbVvMtOyZ9YYX7z56XvpzLWGKx/WUD6OBa/Y4JoA8M6Ki4uZa/1FtHrj0ZKsWGb5+OcfcJSPY2/jLjGvtLUkAD09PTBp0iTmtK+smr2Bqo6fe/z2wZd/cxZ8EgADWGVlpZCFnHqFRaLu53BLVKizFR17qHdlb4NrgWZJABYvXqzaZ0zUGOiqflAqAFgmzpomxsEpda6/zpD3D1oSgLKyMsjIyHB5FXtRUVFvdnAjRZQhXCiC4xA4EvngvCh4d9vMAdrDbxH6jgB6COwbGa6rcwY+NjbW+Vr2/48TcEwfixI2f8Dew+yHvaW9vY0NNHpjiGLd3d3sYWMB7WW8fqEktrXFYhaDjQDgmUUU0GaO+71A2NEU29pKMgKAt54Ai0rw1sBRXqZZCBiuYsKOppKNANA8iHC196qA1cYaSs5VEzh4e8EWdtjH0IdGAOi9MmDmgCuQcBka/otxQSquSsal6Thci00qsFMJtqvBnkXYuAq7l5nECACLGwFAABAABAABQAAQAAQAAUAAEAAEAAFAABAABAABQAAQAASAnwFQVVUFMTExXDp69Khq+8LCQq5tU1NTmcdPTk7mPr5soW8CHgC73c69Nr+6ulq1/erVq7m2TUpKYh4fHS27xwCv0DcEAAFAABAABAABEKgAOBwOKC8v51J7e7tq+5qaGq5t9+xhV+ru2rWL+/iyhb6hNJCMACAjAMgIADICgIwAICMACAACgAAgAAgA/wLAZrM5T5ykX+hLvwOAJEUEAAHgewCCFbVRMKQLfR7scwBuQLCBAiJdJUJiJwiAaEXrFbVSYAxX6w1fR4uI3f8A6GJNyL+CJlsAAAAASUVORK5CYII=";
            doc.addImage(adequately_marked_exercises, 'PNG', 23, 66, 17, 17);
            doc.rect(20, 58, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.adequate_marked_title, 22, 63);
            doc.setFontSize(15);
            doc.text("Marked\nExercises", 44, 69);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.adequately_marked_exercises, 47, 84);


            var with_record_books = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAB8VAAAfFQEIJ2vyAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAihQTFRF//////8A89Vc/6oA/8wz/9Ur48Yc5swa68QU89Vc7cgS89Vc89Vc8cYc8skb9L8g9cIf7sQa78cY8MEX89Vc89Vc8sMb8sQa8sYa7sIc78Mb8cUd8cYc7sQa78Ud8MIb89Vc8MQa89Vc8cUb8sMb78Qc8MUc8cMa8cQc78Ub78Ua8MQa89Vc89Vc89Vc8MQb8MUb8cMa78Qb78Qb8MMc8MQc8cQa8cUa89Vc8MQb89Vc8MQa8MUb8cQb78Qc8MUb8MQa8MQc8tNX78Ub89Vc78Qb78Qa89Vc89Vc8MQb8MUb8MQb8cQb78Ub8MQc8MQb8MUb8MQa89Vc89Vc89Vc8MQb8MQb89Vc8MQb8MQb8MQb89Vc8tJO89BL78Qc8MQb8MQb8MQc89Vc89Vc8MQb8cQb78Qb8MQb8MQb89Vc8MQb8MQc8MQb89Vc8MQb8MQb8MQb8MQb8MQb8MQb1tC719G82NK92NK+2NO+2NO/2dO+2dS/2tO/2tTA3NbD3dfD3dfE3djD3tjF3tnG39rH4NrI4dvI4dvJ4t3K493L497M5N7M5ODN5eDO5uHP5+LQ5+LR6OTS6ubV6ufW6+bW7OfX7OjX7ejX7enY7enZ7unZ7una7urZ7+va8MQb8Ovc8Ozc8ezc8e3d8e3e8e7e8u3e8u7f89Vc89py892B8+/f8/Dg9N6G9OKd9OSk9O/h9PDg9PDh9evH9e7S9e7W9fDh9fHh9fHiSvpmDQAAAHR0Uk5TAAECAwUGCQoNDQ4ODxITGBkeICEkJSYnKC4vNTY8PkNERUVLTFJTWVpgYWNjZGVnaWpwcXd4fn+DhYWHjI6UlZucoKKio6SkpamqrLKzubrAwcPExcfIyM7P0NDS1NXX3d7h4uTl5uvs7fDx9fb4+fv8/f6tMyeMAAACcklEQVR42u3aZ1MTURSA4QPYe++99967Ym/YY8PeBWsiKiJ2EILE3rCAgbVLbPl75iZZZSaJy+4qrzPe99OZcz/kmcnu7MzOiuh0Op2OzmOjDc3/AsCw0ygasI4GGJ1owDQasCcdBhj9aMASGpDbEgYYo2nAehpgdKYBM2jA3nQYYAygAUtpQG4rGGCMoQGHtzlr4Z8COM2jARqgAf89YPvWxLIaEpD9PLFlNGAsDegCA/YJDMikAcNpQHsYsEtgwDwaMJQGtIUBOwQGzKEBg2HAqdYwYLPAgFk0YCAMONEMBmwUGDCdBvSFAceawIA1AgOm0IDeMCCnEQxYLTBgEg3oCQOOZMCAlQIDJtCA7jDgYFpywIevNvvsELA8xUuqj2GbfXEIGEcDusKA/QIDFtGAEZaAYGW8anPzzhxeqXXIHaCDBaDqivdnV9+qzcvLvmu1aqi8FN1euOsGsFssAH5vnQJqUxIZbquhOL4t+O4CMN8KUFwXUKY2RZHhlhqux7dnQy4Aw6wAz879+v38KrV5muctqFHDk7zo1lfu5i9oZwUIh+7fiffwW2zz6bE53FProJuLcKcIexvOpQFDYMDpNjBgi8CA2TRgkDWg1p9/JrHzN2MPhsDFkmoXgJMtrAEBb/JK1eEjc3AI2CTWgBspAIXqsNwcHAJm1gPwIAUg+jgK+uKDQ0D/egDCL8pKE/NXxA5rAhUuLsLjTYW9C9YKDJhKA/rAgJzGMCBLYMDk3wNev7fZG7uAXuQXFBHA0QwYsEpgwEQa0AMGHEqDASsEBoynAd1gwAH4U67sxTRgJA3omAzgabgWiE6n0+n+xX4Aj4wEZA3pk5QAAAAASUVORK5CYII=";
            doc.addImage(with_record_books, 'PNG', 82, 63, 19, 19);

            doc.rect(78, 58, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 99, 63);
            doc.setFontSize(15);
            doc.text("Record\nBooks", 105, 69);
            doc.setFontSize(20);

            doc.text(pdfDocVariables.record_books, 106.5, 84);


            var received_grant = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzA3NEQyQzRFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3NEQyQzVFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1RjcyNjY3NURFQjExMUU3QTgyOEYwMUNCNDExQ0RFRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1RjcyNjY3NkRFQjExMUU3QTgyOEYwMUNCNDExQ0RFRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtwUdIEAABMUSURBVHja7F0JWBRH2n4Hhku5EQZB5BA5FAVBEQ80mhiN0Y3ZRLMmJmvMxhwmJms2ye7mWjf+mmP1T0w2Me6/ia6rcd1cqybqRiWKB2pEQA5BBOU+5L7P+etrUKenRxhgZpjp6fd56mm6pumuru/t+r6qr+ormVKphDlCJpMtYYffsTTJDF+/nqWdLK2TmSMBmPC92eEqS3KYNz6zMNMXf0gSPodHzJUAjpLsOdhbSHVg3pCawW5MWBmECU8Fi/odP4/aJ8iTWgAzh0QAiQASJAKY1xiAOzuMkkRvJkYgE7gdO0xnaQ5Ld7EUQdmS6EVKACZwatUmqAichG8jiVrEBGBC9+8WNgl9NktukmhFTAAmcJduQd/4ygddp5cmVaLo7HVAT74VW1cbBMz1ho2jlfkRgAmcmvCpKl951EAMWJshcrQ0tuusfId/exZ5x0v1Xg/nPkjH3L/GQBHhKm4CMIGTkTZeReCxLA3p7/0c3W0xZroCYTMUGBOrwPFdOfhuU5pOylp1pc4gwie0N3cg7csccROAyX44O3zJ0syBfOHBMe6csMeyNCLEGTI92f15x0sMWj/58WVQdiohs5CJtgXY1VfhW1jK4D/etUvgMzwRONENcivDDHFUpNcYtHI6WjpQnVMHl0BH0RIgSJuLPAMcuK+bhB46TYEhOjaOtEFnhxJlF6sE+fc/Nhnevrpppr/9xxkUXqvk5ZVcqBQ1Aeo1ZTq42XTrcU9O6G7eQwa9oMl/v4zG8ma+veFshz/99SHY2OqGkDZ2Vtj4x728vAufZcL/Li/YulgP+P7GOBR8WD0jZtFIbE5ehGc+mYLYX/kbhfDpyycCqGP+kiidCZ9w3yPRsLTki6m5qhUn1yVztoAYCfCjekZucpXejLi+gir94vZsHFh5Cp3tneoGLBY/MVWnz/PwcsLM+WMF+dd+KsH+5SdQc7VedCogjmwdlixvDrLk1qGisHHQvvw69uyiM+WoyKxBKdO/1PXThCdfmYOxkT46f/5bHy1BUkIuKsv5wi5Pq8Z3S49hxFQPuIU4YdgYZ3ix3g8ZxSZLAKVSWcO+pHPU8qvmp7Hu1oylAQb/2uNePc99bb01txOnj8ILa+/VSzkU3s54b9tjeHLBp1Cfxd3R2smVjxJhqMIW014Lx4hpHiarAjTaAWk6GGy5fO46tjyXgCPbsrXr4x8rxdWjxb0KP3yyHz74coVAV+sS0+8OxdpPHoKVtWWP1zWUNuPnjzNM2gbQaAeknygd0DB7/O5cvPPgUSR8ew11lS2aRqA0DLr0TrrHX5yNnXEvYpin/icaL/nNNOyOfwk+/sN6vK4yq5YRocmkCXCayMzTw0xoeWlV/b7h0R3Z6GhX9qR7hFkdmq+nr3DuAxHY/uNqvPr+/ZBbWRqsYsjG+ObcK3jxzwvgG+h++9fpUJouAZiea2OHY7pUA2RE6gIPPTkNJ/LX48PdT2DyHaMHpX4cnOzw9B/m4lDGm9h17LdwdOm/cWzMU8IEaiAtvv8E0NXQcESMP5xchxhNJUVODYC9o60oCSAwBLNYV6ytpaNfNxuMoWJTgNESgKmBVHYoVs0j4WfRpIt+oLmhXZK2ibUAhCOC3kA/1MDVlCpcz2+QpG2CBBDYAan98L+f/OqqJGkTJYDADshLrUJ9VavWN0g8WIjDX1yWJG2KBGB2QBG1+urddRoU0gYX44qxZdVpnXjNJAIYU3ewFzWQcaoMGx44io3LjqO1WdhrcBlmLyohevm48Lu8dpawcbYWDQEOazseQCN9u9cm4d3FcchMKNd4zYx5Y7D0qemiIgANEasi8F4fWA3Rzs9nCtPCf6IeIEs3O/Jk0ZderYfCz16th1CCg1szb3ujwFBPbNr5OLZ9cFRUBFj48ES4eThg++fHoAyTc+sHtIXRE4DZAfUymSwBXVPDeWpA4RfIu7a8h64ejd2//enSAY2aGStoIsq0OSFwjnLE9wWporMBtFYDLp52whe0kGH9/z3Cjd0PZMxcMgKNzBDMYD2BTjWPV8gUD1jK+W7dTtYDiJ45WpK0iROAZgjVqmY01rYhN4U/XdrOwQoBE4TrQk8dviRJ2pQJwOwAGsiPU8/XNCxMC0PUceJHiQCm3gJotgM0jAfQ+j91JMRlcapAgmkTQGAHZP9cIVjl6x/hxqkCVdRWNSL1fJ4kbVMmAFMD1MHPV81rb+sUDPiQERg61cOs7ID62mZsen0fVk74GPFvJaGuoEGULcBt1IAGO2Cm0A44KWI7YPfWE9j67n9RUVSHy/vzcWZjmmgJoGGamHZ2wIWEXDTWt4iSAEf2pvDOC06WcauIxUgAmiDCs+YKLtWgpoy/QFPh7wC3EUPBVxcdOHs8W5QEmLVgHO/cm8ZDbCzFRwBmB5SxQ4qwFSjVqhUQqx3wq5XTsWLNnXAd7oBR93gjes1Y0aoAzWpAQ3dQ03iAWAlAS9JfeXcR/pb0HGaui4ST71BRE0ArvwDFElAPo5KdUYLSwmqp76cCUwwTF88SWXM3gz9WlzahKKsWXkG3lmfZu1jDb5wLcpP5w8WnjmQOSqFLCqq5sQhKaYn5yEgugJu7A8KifDA2aiQ7jkTIeG9Y28glAvRiBzTKZLJTZPuo5tNkUVUCdKkBhYAAJ5ka8OthSZWuUVvdhPVrvsJ3O84KfrteUovMi4X4elsCd+4+3JFzWd9xb5jBymeqwaKFi0e19AucZi2AofbJOnYgDQvC/0ej8DWhvLgWTy/6DH944p+oq2mSCNAXO+DSadb3VYvYMXrSMFjb8btDFWV13Fenb/z5+T146hdbUFbU9yhiFBhqYcR6pCcVSAS4Dc6zxGvbaeXPlfMVfP1mZYHgGOGwMDmH9An64ndtiR+wzfDyo9vR0tzW67Xk6PrphzR8+OxeZH2XxwWUFLMRSHZAJ7MDaGLfg+q9gaDJ7gI7gKaHq4LGzvUF6mWQztcEit4RtMgX7mOduZAuTRUtuJ5ejcLT5cg9XCS4/sqlEmz+0/d4+Z1FPT5z785z+P2KHV0nX3eFjpn22nhRtwCau4Ma3cOeBi3UG09/yRl+6ghaNBK//PcsbnMqCt9i52YDV2a0Uv6sd6Nw9+bJGOIhnK/4xf8e5eID9YSvPj/FJ86BArRpGQvZlAkgMARzkirRVMdvMkewL83JwzATQeMPpeP4wXRBfvjjozH9jXBYDb19g0ukWLgtFtb2VoLmfcNL3/T43KJ8fuCM9qYOtFS3ipsATA3ksAPv06A5grQoRJvegD6QeCpHkOcc4ICIlVoFP+VUxOSXhcO4qYl5WtkC5mQE3rYV0OQeDotVGKQwqefzBXlRz4bA0vpWNbfUtiFFLUgVhaC72XNZ4MOpBlVQ7+ZSSqFEgP7aAWNmGIYAaYnCWUce4fxlW4eePc3FHVTFwWcT+P8z3kXDvfP1UmZT3zKG3MOdqkQuyalDZVEjXL1urQFw9rDjbAFyHesLxXlVgkCOQxV2sHPtGrFW3bXzekYNMr+5xrv2xu8rzi/kAj4C/N9Tf86TCKDBDqhk3cEk9mekaj7FA1i4egzfDoj11CsB2tp6trpJsISdsw5i4vOhCP6lL3dORPj5oww8EjdvQPc3SxXAhP8LUvHq+Wf3CZvLsJn6VQPefm6CcHEUq69JLSbhrHeiOIHvXXacS/Q35amCxgbU4TfaQ2oBNAj/3ywJ1kEXZFRzcQUdXG/tFkcjgjQySBNJ9QGKEjoyYBhyMvlGaFlyFXxn3eqFeE12x4P/uRMVGV1Cdgt1FmwEVZYijIfoH6SQWgBthN+lGrp8A6ognwD5BvSJ0IgRgrzzn1zi4vmqggRORKCkLnya1EmRPtUREu4tEUAb4d/ApZOGHw949DnhTje0vUvSVu18DxTn98z7whm9FCc4IFhqAbQWPiFdIwH0awdQEElN0UOTv7iME28no62HUHU0k3ff8ni01gsHfJ76/d16K7NcDMKPCvbBhcsFvOVfxdm1qC5r4rqANw2pcS7cTKG+BJnqK557cz7OHd8sWIpGXrpCppb64gwiTJkdjEmxgeZNgJ6EHzPGD8ujhuLj1uFIzS0SqIGY+31v3cdCxm0wdW5/vt7KSsL646YHsO7FrzQ28bTfj7bwD/LApp3L9Vq3cg2VTfv2zWBpIro2b9TnxsvkNqPI4LTy9wzr17f2R/jWcguM93UVECBdjQBd3UFPvRKAsGzVTJQV13KrdfoLmh729x9W6T2glVylomkK7fMsbcAAdunsB2gEZC2RgRXhdUaCTX0VPve1OAvNGU2OoTADDQuvWbcQru72+Gjt92io69uKJApo8faWpfDyddV7OS26K5oqmCZYfGhg4auClPVGVpb3ZV3QWviEEQ4dkFvyB2LKr9WjQm2hJK0YopVDhsDyF2bhQOobmL8kUqvradOJ97f/Gv84vLrHvQB0ql5pDxpW2evY368ZkdonL99MbYV/Ax+frEJiVgHEApoiHhTmxW0SMW6SL7eFnKYtY2YHvoUitc0ll+y7E/Ze/G9Z1R9xswVgwo9gx1eN7N3n9FX4hHE+LhATWlvauXUE//rbSby+chcemPwet55A1zbAw+rGoExuBafIe+Accz8sbXs3Qq68t1iQF/DKnl7/r7O5AdUJ36Im8QCU7T1PeOhN+ARfJ4gaWalFWDzlL3jzoyVYoqP9CeXd1j4PHvNXQXHfSwO6sX3wFK2ucwy/C6X/2YjSvR8MSPgEHwclgn0UyMwvFS0JaJXz26v3IDzaF8HjBj48TDUapZ7pGrvUoC/V0/O0FT7BkvXzH412Rey4AIFBKCa0tXbg1eU7uKMuWgDBfmdWrl4GfaHbPa8vwr8BL0c5Hp9IaYzpf+0dSpQ0KHHwUiNOpfFnBtMUsR/2nMd9y6IH3g00VvRV+GKD3FKGEY4WeCLaHlPG+gl+v6iDWUJGXbvmLHyeUc7S3GChMa5pDqKoCCDhFlxshBNZrmWXSwQwF1S1CEU1KsRTIoA5gBzLhzLrBfnjo3110guQYAK9gNNpVwW/h0f7SQQwNAprOhCX24rjKblMQB2DWpaXHt0G91ec+hWDQCJAP7ErsRIZV0uMoiw0EKTuBJJsAD2ioqHNaISvK0gE6APchloh1M/TJMraXNUqEUAfeDjSFbMnBBq9r0F9BbJkA+gI3k6WWBZhx9Lg+xqa2ztRVG+BrxPLkXGNr5quHi1GRWYN3IKdpBZArLCVWyDAGXhhhhv8hwtXPRWduS6pAHMA+UwWRArnBlTn1kkEMBc4a/AV1Bc3SQQwF1Rr8BU4ePc+wVsyAkWAVmYM7k8sFuS7BDhIBBAzbvUCKpBbrGbwyYDhWiyHlwjQRxiTL6An+N/lJYg2JhFABzAmX8BthWpryYWn0waSEdgHmIIvwNrBCvM+iYHjyKESAXQNY/cFDAt1wvytU+ERrv2iUkkF9BHkC4hzsTcqG4B0/eTfhWF4lFvf1YUk0r5hsHwBlUz95NTbYNvRDDQ28z19FIyyP8KXVIAJwZWpn4mKTqy5J1jwG4WVU/Zzd3SdEMB2RGiP5xJ0B3L+RAbxw9HR3gAVmbWDRwD3Ob+hKBPdAxCyrnMJeoOPm3CRSGVW/+YF6sQGcJm+BENGT0LthUNwnDAXNgp/SUp6RH6FcIq400j7wSMAgYTuPu9pSTp6Rk41BFFQKPqZa7Dj4BJAguF6AYKv388eVkPkEgEMAWP0BUxcFdLv/5UI0EcYmy9g9EIfjLzDc3AJ0FZVjLJ9m1Gb9F84RtwNj4WrYeUyXHTCNzZfgHuYC2JeHtg+wzrpBpLwK479E201ZdyRzsUIY/EFWMgtEPlMCBZ8Ma3HregM1gLQl69+7v3YhgHfl+L+3xheMBYMli+AtpSjzSWGhThxG1C4BDrq5L46IQA1+/Tlq57rRN+mNGFxmC2sLY2HBfrwBbTSKmDWtd+XVoPzmcI4xvM+nQKvaP1sdqETFUA6323mMlg5eXBHOtcFjiRmY8upKtQ1G+/MG12ACD7SSYZnpjghfJQwYFZ5apXenq2TFoAMPmryddHsqyMpuxAb65rwTKw7FA7WoiaCBdN388cNQ/KVIjUCVOvvmaZQMXmllXjvUD7yapQQO5zkwkWd2izwGAgBBE/sbGkw6Etr87yqukZs2H8JGRXi9mDXtAtbOWc9Rjen2kwRVHbCtwZ9aW2f19Lajr/sS8H+rHY0tIrPLuhk3Z4fLl7X0N931tszaY0zBbPnxQtuzkuHhY0dbIePgkxurdcvv/LkHm7coLNZ4OE60d06CSa4ZeRfx5lrjbB3ckWbkl6iHTYmHFOQegGFrJXfmViDC5eFm0SHrxit1Sqf3nDhNruX0cYMSiNMd7BEfZ/TRlo+cSTaMILhGyMrWDyVq7tstJPI15Kw9EsA2kin1EgKdZnU3g0CqBCBQooXSELTaapTrWCFEbQEh1nyUxe+ShlptcN6MlMk4ekkbZF1V+xNdG/WdA9ubRunz9EX2iaENtK7yNIOVpYD2vwTKyPNiowl+0glic/9qD+Qxb2TpXX/L8AAK23SCjk4axoAAAAASUVORK5CYII=";
            doc.addImage(received_grant, 'PNG', 141, 63, 19, 19);

            doc.rect(138, 58, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_or_amount_of_title, 162, 63);
            doc.setFontSize(15);
            doc.text("Received\nGrants", 164, 69);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.grant_received, 164, 84);


            doc.line(20, 91, 191, 91);// horizontal line
            doc.line(20, 93, 191, 93);// horizontal line

            var school_support = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAejAAAHowBNXh8qQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA59SURBVHja7Z17WFPnHcftzbW7dN3WzRdlU6ttVUgK+ri2thWsgrWInW3dUwSn1W5t0a66ro9b3Trt3XWtruvNboBgFQFJoCAQLpKEJIT7xQACllsAEUQoUIRwOfu99LDRkEA4Sc45Oef94/uEy8nJOfl+3vO+v997m0VR1Cw+KDcl8juglaDnQJ+BikBdoFqQGhQDOgraBrplhue+DbQZ9CZIAWoHDYB6QfmgT0DPgJaDZvPlO2FDfDD+dtBpkAlE2ahu0BHQrdOc+ybQi6DOGZx7EBQO+iEBwPnmLwJdnoE55moCPWrhvNfRJb7GjnMbQW4EAOcC8KkdBk1UCuhJ0CrQ26BGB533HQKAcwHodZBRzpKRAOBcAFp5DkAlAcC5AGTzHAA5AcC5ACwD9fPU/K9A8wkAzodgB2iUZ+aP4EYlCQPZg+B5ngGwkySC2IfgCE/MjyeZQO5SwU0cm4/DUncCAHcQPMMxAC+JyXw+AnAj3fnDhfll+PMJANxDEMwRAKvEZj5fAbgeZGDZ/Gwxms9LAGgIXiF1v7gBCGIZgCcJAPwC4D6WAVhJAOCRIg7vvysj9mOKLX34xr4HXwjecCsBgGPt23L/LaFb/eW7g/2HQBTLugYKIwBwqD1b/XdxYLyZ/NYSADjS7uD1e7kGYE+I/68IABxp7w7f23aH+JdxCMC5LVu23EAA4FihwY+sjP7ktfL4sHcoNnTs8B+PPrfN35M0AvkVCipZDAMPkTCQL6V/x6MIqoGXjxwMbfr4rb0UG3r3wG/LQkPW78FVEAGAQz2/dd0dUA8PctgGuLpz56YfEAA4iwL8DnEfBax/mgDA1eM/ZP0jXAMQum3dUgIAVxcza9Z1u0P8/gBPgop9OwNNL+16jGJLL4SsLwYAtpM2AH+igDKWO4SWkCiAXwA0sAyAHwGAR1XAS7s2jbJZBex7euPA9s2rjx08OOv6qa7R19f3xkBfr2c2+nrnBq7x/mCzv/RnBAABNQI3+68s2rRGerel69u0drl0o493EZhP/V/LjxIABBQGPhVwPzb2WqDv8uQAH+9DAb7LQwJ8vd6Av6UF+nqbvm0+yMc7jwAgkETQ80HrqMCHV1CTTJ5CAWuW/44A4OBU8N9eDPqIrTTwuI4fPUD9esN9MzG/C54M20kj0DkRQCgXcwP27XqiyDbzvZI2+XnPJVGA8wA4xgUAurPHDwWs8QoAk1+Huv0svF6iTW8GfQENvoObfLz8SBjofADyOJodpDO/Fn9/6fdIHoD9mUFfcwTAsFjWB+QzAHdzPEOYjAkU+WohHxIAuFsyNoIH6wX1gLYTANg1/zczXM+XDSWRpWKdb/ytoFM8XicQQxlEAHCO+feC6ni+UOS44nAVRQBwXJj3J9CQi5g/Lryq+WMEAPvMdwNlupjx5orEG1AQAGZufgC9UwclAOG9BPwJALYZP5ve6oUSoPBWM98nAEyd1SsWqPnj+hL0EAFgsvlPg/oEbv7ExaXfA90segDo2D5aJMabq8rV1htyxuJODo/t9WknqMLMaKrkXCxVrpZTBu0XVKUueex3HkKAw9vX8Y5logGA3qXrz46M7fWpUVRJdhxVpT9L1RamWxUGg6dPgxKQp+ABgJu8ge7EccgXl684OVbKq/PTpjR+XOVqGZ+rBLz6+AbBAkCHeGcc8WXlpX0+9mi3xfSJwsDwvF2AB5k8JzgA4Ka+C0pzxJeE63JbS3wNHIfrf1zy8xWfu1ID8V1cVQoCADxsCpRjd6kHAyt0SVMajsEoU8mogoxoaBeccPUoIczlAYCb+IkjkjvFUOprCqyXetz4K4ZGIG4MCixUfNNlAaBb+wp7v4RSZbz1Ep+XAi3700LPF+xxVQD+Yu/Nl+fIrZp/PieByhVeief1tnQzMX8N3aJleNNR0MpPtFLqU6ki4Zd6c+E0+Z0uAQBc6BzQJcY3C6XaWmMPt+jz0k5QIk0d63EehdcA0CN4spibH2nVfBz34/+L1Pxxvcp3APbbc4M4hLNY32sSKX2K6M0f7ztYyUsA4MJ+BOpmenNFWTGW07fQECTGf0vFXCWJpgPgHaY3VZB+iqotUFhu6RPDLSmIVwDABSGmkzRxxg7H85MafLnJY9EAMduiLnLRhTwVAP9iejOWwj2c0hVxa99W7bboR+GKm8bEFgBwIQtAg0xuojAjenIHDlQFhenRxGBrafG0D7rrsn5f1qF6UjaUK/3HsFby+bBOkjmikxjg9QpolNYV+m+ZoJNDWsl7Izrpy8NazxAq12uBIwH4jOnNWBrAwdORO5xJnxLRVZOxP6VLvSHVpPW+CGZSjtCQTlKFoRjKlayjKjxmMwIA10OgLqYdPJYSPcT0b1Sd+YqhT7O6cFgn7XGU6VOoD54kicM6z40zBWA90yFc5n36NXjARvpJ0Rtfmf5qtkm7Ip8F061IqhnSSB+yFQBGj/9SVbyFkE/c8X6p4silfs0DedwZP0kplObbq6BaSvteZpLurc5PNWv4pQlhAAfzDKji721QJ3fwyPxxdZr0khXWAPBh1PLPnNzyL1WeEXXp79esKuah+bQ8s6wB8E8mN3tek2AGgELUpR+rPXVpKh268Q2A0c7U+TJrABiYNP5qzFK+Bm2S6Bt+Rvm8utYEt9LuzEVak8azm2vjh7QePT2Zi3WtiXOLm+WoYRIAdP0/4IjQrzjrNAEAAIAvmqJlwl/8VcVCVb9qSaVJ4/EVG4YPwGd1pS9UtSa6FcI1DE64HosAzGdyoxVmY/nxsG2S758EwGQloCstCXPPt33hrrmSuiAbnhSansw7c/vO3VXYn72kHMyrHshZ2jCYs6xtSOvZi4V/xn/D/8PH4GN7su7Ud2cs1lxJWZDdluyeA+csh/N3TPnZclRvCQA/Jjd6wazTh/T22QgAt7L4BGC0Qrd5/S/21r8rA3CEycQOUv8LB4Czjuj5wwNBCACuCYB25nP64iYBIJJx/dOqSe5eyVcAjHJUZQmAbHtDwAv6FGL+eOP41MJ0+LJ7eQhAX81JlGgJAAWTJNDEHkD8RCDm030B4fPqzkeg1osxSGmUIQN88aMcmj4Kpb7iyxiUbYhELWVhlquAJKbdwMVZsa4wT591AOCLpsYFMLRdjEaqxjNIQwNx1YmGd+HPaIxHGvyZGMSJ12INABkxznkAWFL5cdRVEYUM1SeRBpfOuliUXR+LlA1xSI1BaTqD9MZ4VNgkQ2VYYz/D3/D/8DH4WPwe/F54rGsq4Vzl4ejqdJ9rDYDTxDh2AeBQFgGIIsaJG4DXiHHiBiCQGOdIANzrXQ0AN2Kc41QecUcVXwEwhKNiawNCmol5Vvo9UsOoqoy/Uq3KbVSn+jGqJ2cd9bXmAXhdO/Z7a/Y26kLGATjuP2PH18k8DIYI1Mg7848jI0QUmdYASCBmm43sTTtCtSu3UCatt00DMUw6b6pD9QTVnOxlhHj8WlM8yqs9hdQQi3dwZTp8dmfNKZQD4aMerqnfYl8ADcABYjo9uzn12FipHtLdw3jsXb9qSW5r4tw+OjkzYpShcojblbXRSF0VhYrprNyoA80eBbNbqk6gEjBcXReDlDh/AJ89PG1nkD2DQoSmkrT34fH+oKOmazW3J7tXTJG1u9YsQ7XwpChojEe6hjiUUx+HVOMJnounkRKbiYV//l/CCI7Bx0Kp1sF78+E8tXTptiVTWG8NgJsYzQsQ1CyeQ9SgdoWjx+j1d6YtrOJ1d/AECA6LdybPe9SgboWzBmr2X07+ebErALA4l/vtWzmo8z912GPfenXg2daaOK+H1wDQEGSJDYBL2VtZGZ8/oF5W4AoAPCW2R/+QTsrazJzLKe6lXAKAG47TAYD3AegQCwAdqsdZnaUzqPEo4cp8CA2LysOR3pY1gt4Sg/n5Kf+mTDovtqdqDbUmuH3FkukjEDaWVUUhZVkYarLaF2Bl9682wa/akfEKJ3P1OtMWJV2MQYWNcagS4v9Go2xshNCIPUbDeYx1p1FRzSmkqohESkMEygezO2zqDJpi7z9hN/6UwZwA0HduaYGlTB6ouywcNcFjurI8AhWdP450lVFIXRGFVIbjKAekw8aej0AlcAweAVSLj4f3DdjVGzjF/gD5QgbgqnojJwAMqj0qeNUdPM1egILNC/RpfDkBwKSVNLkEADQE4UIF4Jr2Xq7m7Pe4EgB4h7BSIQLQq/Hh6Ang2eIyAExYPfSK0ADoUgdwAsCA2qOas/EB4SiD6Z5BD9u3bQz/1KZ8ihMAerOWst4ILA9DPZWRY2MEquzZNm6vkACoydjPCQCtiYtKWDL+UkUkUtXFoInLxDTYu3Hks0zWE+LryB87Rv0w1Uh9zFxF5QmkNEQiNcT6uTiuh3geVwt4GtfXMzB4BNSL8wFwHn1lFFLWfjMaqKQpHrUy6gyyEYKVoEYBQDB6TXN/Ias5gByPCzZk9vB4wraGOFRbH4NK62JRcX0sMjTGolqc8YP/dRptHwFk24gghruIKlw/Hbz/IzbX9Gs/+4s2XncHM9hNFC8wnQwyuSgExmGtNIadDOCyCt6PB7BzO3kv0OOgENAOV1G7KkgCBrU5GYCutiT3dsEC4Ooa0nmuApMGndUF3JEyv5onYwLrCQBWNKyTBoFZJkeb36VYWM+bUcGyOe8TAKZ8Ekh86L16HGF+N5R8PgwJx0vU5LXI5+xivHu4qJQjuWM4V3LWLvO1knQq7567jIk/XWyUuQW3yNEHYAKenjXAkul4karMFhnabUxyn2f39vGifBpoJavBTOUMwsTRsS1adNK11s5ZEesxG2L4XwIQe+D1MDySP21OQNFgVkqz3E3b8s0aQnhuYY+FxaVGaGMv47qcPjYPdKYlwe3tFtmcnU2Jbqsb4m93c+j28aKXeqnbiE7yLJgbD684cdRCNxhb4fcieJWPaKWhlO6eeQ793IOzrq+XL7jNGOv+4/qIBTc7+r7+CwrY4DGTSNpzAAAAAElFTkSuQmCC";


            doc.addImage(school_support, 'PNG', 23, 104, 17, 17);
            doc.rect(20, 96, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_that_received_or_any_type_of_support, 25, 101);
            doc.setFontSize(15);
            doc.text("   support\n(kind/cash)", 41, 107);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.support_received, 45, 122);

            var adequate_furniture = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAN2AAADdgF91YLMAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAWJQTFRF////AP///wAA////gICAgICPVVVjXlFrTb/y/00ai4CXioCVnWJYe1pjnGNa5r2Ua2t5g4OSc22AhoaUhISSXVNrW1JpAMR7gF5imWZZ/1EaSrnthYWQ/04aS7rt/04YXFRqSrjsXVVr/1AZdG2Acm6AmmVYXFRqXVNrW1RqdnGChYSRdnKCS7ns/04ZXVNrXFRqf1ximWNZ572US7ns/1AZmGRYS7nt/08ZhoSRhoSRmmRZeHSEeXSEhoOQeHWFAMN6XVRrfl1immRZ/08ZXFVqS7nthoSR/04ZS7nshYSR/08ZTLnsh4SR/08ZfHeHe3iGS7ns/08ZflximWRZ5r6ThoSQXFRqflxihoSRmWRZ5r6UhoSRf3yKXFRqg4GOAMN6S7nsXFRqb8Pid3KDeXWEflxigX+NgoCNhYOQhoSRj2FclmNamWRZzt3G1eDE5r6U/08Z/245/8CM/8eS/+u3xyVjjwAAAGB0Uk5TAAEBAQIQEhMUFBYYGh8fHyMjJSY2Nzg8PDw8RUVFVVVbXV1dYmRlgIGJlpeYo6Olpqenp7Cws7W1t7y/wMHEyMnJycnJytDQ0NbW1tjY2N/g4+Pm5ubp7vDw8PDz9P7+Pm8aaAAAAo1JREFUeNrtmvlzEjEUx+PWo9UCHtUe3hdqPSktHvXAgvctS609FKqi0qKs2v/fbFdmd0yybPLemFLf57cuefl+ZrJ50wQYIwiCIAiCkDJWLo+hTnhy6rUn8PCwcnzZ88qI8QPXPCnPHdno4WJx2P8UUWDK0xEo1mpFXIGjinzFEtQ4uAKX+Gw/fq0JKIbjCzzhs/1cAwk42YLb0sAtZB1MgcHpljbTg3hL4BjkcwMH7SXMtozIGmzDc++WfZo+X+90nhbMBAoGjejucijQXOo8dc0EXINWfCUq8LLztGXIn/Ljk6/qAvcPStf/bFTgFopA/9W6lKdbZAInogIXUAQm6zoCB6ICpzEEjijyFUuwIyqwF0PgIg/78D1xE2JvQoH3DEPgERf4lrwNs3uhwJIVgcuhwAtmYwnOhAI3mYWXkB0LBcaZhW3I9ocCp5iFRsS2hwK72b9pxZl8tW2Jaj7D8xfaFlnIsHzbKnlWtStQtS9gfQmsv4TWt2FAg6M+WgEbkQQhjwQ2uADCwQQmAD6aQQXAh1OoAPR4DhaAXlDABYBXNAgCPv7/akmupJKMIwES6CWBkZkGgJkRsAAonxuABRpAel+gAsuvgAVyMIEcWCA1B8mfS8H7QCpnvAqVXAqnESVtQclrSIAESIAESIAESKAXBUwggc0lQLuABEiABDa5QHpldXUlrRq959OXzx936eXH1/ydl573b7jnFQb7Fv1fE8zu1MmPrxHyJoI79gn58OvBr4rO6wjE1wh5btyXTFsXg8kea+R3qRHyYr9m2xbM1XyrIdClRsgjAesCpeDvknR037NgshsaAl1qhLzR9SelUfnwQw/8uW4P6WzD+Jr4PIIgCIIg/jd+A+MYLb+J8y4eAAAAAElFTkSuQmCC";
            doc.addImage(adequate_furniture, 'PNG', 82, 104, 19, 19);
            doc.rect(78, 96, 53, 30);
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 102, 101);
            doc.setFontSize(15);
            //Schools with Adequate Furniture
            doc.text("Adequate\nFurniture", 103, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.adequate_furniture, 106.5, 122);


            var good_structure = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAN2AAADdgF91YLMAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAotQTFRF/////wAA/4AA/1Ur/0Ag/0kS5r6U/1Uc/04d/00a/1IZ/1Eb/1EZ/1EX/04c/1Eb/04aXlVq/00X/04a/08Z5AAe/04a/08Z/04Z/04Z/04Y/1Aa/08Z/08Z/08Y/08Z/1AZ/08Z/08Z/08Y/08ZXVRr/1AZ/08Z/1AZ/08Z/08Z/1AZ/08Y/08a/04Z/08Y/08Z5r6U/08Z/08Z/08Z/08Z4wAe/08Z/08ZS7nsTrPjULDfUa7bUqzYVKjTVKnVVabQVqXPV6PMWKLKWaDHW53CXFRqZoijZ4WfaYKaaYOca3+WbH2TbXuQbnqOcHWHcnKDeWRue2FqfF9mfGBnfV1kfV5lflxiflxjfl1jf1xif11jf3R7gF1hgF1jgF5jgV5jgjxUg2FkhWJlhWNlhmRmh2Rmh2RniGVmiGZniWZniWdnimdnimhoi2hojGlpjWppjmtqjmxqj2xqkGxqkW5rkm9sknJvk3BslHFslnNtlnVvl2RZl3NumHRvmHVumWRZmXZvnHhwnXpxnnpyn3tyoH1zo390o4V7qIR2qYR3r4p5so17s498tpF9t5uIuJN9uZR+vZeAvqOMv5qBwZuCxZ+ExhIux6CFyaOGyqSHzKWIzaeIzqeIz6iJz6mJ0KmK0quK06uK1K2L1K6M2MCe2bKO2cil3LSP3LWP3raQ37eR37iR4AEg4LiR4bqS4wAe4wEe4wIf4wIg4wMg4ys547uS47yT48yl5DA85LyT5LyU5Xpq5Xtq5Xtr5Xxq5X9t5Ypz5Zh85b2T5b6U5r6U6RId6WdT6mRQ6mVQ6mpW62BM62FN62JP62VR62ZR62hT7GFN7GJN8t2v9jUb9zka+ua0/08Z/+u3Qc3uHwAAADl0Uk5TAAECBggOEBIaHh8mKSwuOTs8QkVRVFhnaG9/gIeXnq6wtbi8xMnKzM3W2Nrb3N3l6fDx8vP5+/v+kdRjUAAABSlJREFUeNrtm+tfVEUYx9e0tMwSC83ykuVaaWpWWmQlVm4pmpfM9bIl4iVAC/ASUtlai0a5QYLiDVNSkCxl8RaUeaPxWlqhsH+Oz5yze87ZMzNnZ/bADn44vzfMPjszv+85M/M8sy9wuRw5urOV4nanSAVwezxuqQAekAPQRmhMP8kAbaNkA4yWDTBAMsCT3eQCvHCf5FMwQPIxTOICdFIA6UsgfxNKP4byE5H8VCy7GCW5HDs3IgegawMs2bFjiUz/1COh0JFUif4TQqAJqXL95REo/sePSyNQ/U+flkWg+p9B6IwcAs1fEoHBXwpBjH+7EvR5oo1DNxuw/1mk6SwmaLjJM/bx3lb+vZ5PyF+I4LmeFgBDuf0bYvwR+rOBm2AI2/9hfv9LyKR/+AmYN8a7x3L7X0aErnETPNuDATDYjr8IwSC6f9+XbfkLELz0AM2/+zM2/QUIRtxFAXjMtr8AwUDS//7x9v35CV681+zfbTin/xVkqb85CYaZAR5pH39+gv6iOZjiXx3052dl5fuD1QkQmDLyUHH/ygKvpoJKcYIhQjmY8K/d4I3Rhlphgn4COVj1v6577FnuNWn5Hv3b/7gIDBl5sLB/JvZcXFJR09JSU1GyGH/KFCYYxJuDCf9a5fkLG8MRNRYq76BWkCCakePlYMIf4fVfVNYK1od8vkPwp7VsEd4HSJAgkpEfFfWvxM9bpjy7z+PxKY0yHKsUJRjIkYNV/xvGk47PX2Gr4ot/niuNVrwKBcZe/3MQ4IwcJwdT/Kvx/ousvwYQbsQ7sVqUYFi8HEzxR0FwKgmbAcIlEA4iUYL+rtGi/sgPThUkQAWE/UiU4CnXGItvb9H8UT441cD+90QUAaiBcD6iEdyysHja9ZAFwUUYfuyqud5lgVNLWPOPArRAOMvc999jMMVFtsPIB9VUcA+9tDaFQifOoTgAPisAdO5EKNREnXycsRgxAC40nTxPRmOXQElErCUAnT/ZdCFhALqMm9Agyia0VOIAxmNoEHkMOwrAmIh0kYmowwCMqVgTmYoTBvj1+0D2x5t+OKVHDge/XvHJ5q2/U4qRpthiVF9anJNTXFpvNSsLYNdK9Yaz+kfN7yM1su4nshxHnz+2HFflqSPyqtizsgB2anesD/epke1aJPMg34WkamF0xMIq1qwsgF9y9VveauV91S3TI+v/4LmS1efpI/Lq6bMyAbZAl/emvv7G9PehsRVHvoPG3GmT3pyBR2/juZSWwqd5GenpGfOgUUqflQnwJfR4Jw30LjS+wZH10HgbR2ZC41uea3kxfMzAIzKgUUyflQXwF+T5+a/hrm9B1zUQ+Q1e74JXcWQKRD7l+WGSA4F0PCIdGjnUWZkAR6HDHNwz7RVoLYXIz/B3thKZCK1sFP+nGcJEyog03KLOagmgKwqgK5snw8SuDXVWB6BzA/BvQgsAO5tQOgDHEjSXB3LNmTA3UN6crD1Q5/dS5a+zDdBcYOz6Fc5iq4yRgNLpcy9DfuUdFBlDRdRZ2bVg9zq952f7lUq61mBwAEfKvUyV4+/3GgiK9tJntXUlC8A0syanmTR5VvQNdfidEO8/wh8I8E5MCoB+zGIV2XJdBIClOx+APDDkoYoHwDMHfyIi00o8AK45+FMxmViVyEZVX0Dzg0h7IzvxUpep8wKYCidRXHkA4s7RqQGcPeAAOKega+8B7mJkAWCrGHGXYwsAW+WY+0JiBZCUG5ED4AC0I0CK2yNR7hSXVH/8/5rSAaQvgSNHsnUbfogttl2Xh5oAAAAASUVORK5CYII=";
            doc.addImage(good_structure, 'PNG', 141, 104, 19, 19);
            doc.rect(138, 96, 53, 30);
            // +8, +5, +11, +26
            doc.setFontSize(13);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 162, 101);
            doc.setFontSize(13);
            doc.text("Good School\nStructures", 162, 107);
            doc.setFontSize(20);
            doc.text(pdfDocVariables.good_structure, 166.5, 122);

            // doc.addPage();

            doc.line(20, 129, 191, 129);// horizontal line
            doc.line(20, 131, 191, 131);// horizontal line


            var sanitation_facilities = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAqwSURBVHic7Z3tj1TVHce/v/swswMLyMMiCzOubLBCtNaUALuupjbRxLQ2YglNSRGtL/gDTNs0Teo7jZqYRl/4qjWxDdp20tSGNtpADa0FXDak2kSsLURghwWWfZqdnYd7597z64uVhV3m3lmW+zB3z/kkGzOem/v7ce9nzj3n3DPnEDMjLE6ePGnW6vWnSVAfCNsAbAGghRYwIQghStVabZlt2ShNleG67qxiAJ+BMMBMRyulsbf2799fDysXCkuA/v7++1yBtwDcH0qARYLrCoyNjWOqXPY65GMN2tN79uz6dxjxQxHg2LET3wbxHwGYgZ98kVIsljA2Pu5VXGfCk3u/v/svQccNXIATJ06sdlz+FMDtgZ5YAi5dHka1WvMqvuzWjXv27XtyNMiYgT+P6654HermL4iO1augEXkV364bzutBxwxUgHw+rxNoZ5DnlAndMJBKp/wO2ZnP5/UgYwYqwB133HEPgCVBnlM20n4CEJZYQtwbZDwjyJOZZtu9cOufEGCCyJj1X5BBBBOAASKTpmNL3yWcy/LlK2DbzsxnBteZucyCpwTzlGHoXwXwSVDxAhWgY+3qtQC+FuQ5ZaO9vQZDn/W9MAHc9uUfCFgTZDz1DZQcJYDkKAEkRwkgOUoAyVECSI4SQHICHQeYD2fPDeLIP47hysj832m0taXx9fvvQ1/vthAzk5PIBfjze4cxchM3HwDK5QoO/e3v6N7Yhc51a0PKTE4ifQQwM1zHaX6gB0KIALNRABELQET41mOPoHPdWui6Pu+/pUuX4KEHe7Bh/boo05WCyB8Bd23aiLs2bYw6rMID1QuQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5CgBJEcJIDlKAMlRAkiOEkBylACSowSQHCWA5AS9RMyFmzlYmxxH27/+CePSeZhD56CVJgJOJx7YMMGlCXC1Arc0AVGdvSMYr1wDt3sL3Ds3o/7NJ8Crrq18NmcLuQbQTV3jZgS6adRgodAF4Ox8jm37+BiW/vV30KxqYPFbFWdsBO7IELjBKmecaYe17znUv/E4AGBsbBzlcsXzXBrxxp6enrNB5Rb4rmGDhcIwgA6/Y9rfexuZgSOBxm11hFVF/dz/GkoAAPVHd6P2wx/j8uVh2LbXPpF05YHe7YEulBhGG+AXfoWpM59Kd/MBQEtnYKzp9Cw3D+XBJz/0ufkAg32v7YLyCvqEAF4B8FGjAqrbWHbw1yGETAb6yjWgzFLP8iW/fBFkW40LCR8NFc6/EnROgQuQy2ZdAE8B+HxumTF0Ftqk5+6Yix8i6O0rPIuN4igyhdONij4nFk/t3r27WQvxpgmlG5jLZk9jes/gVzG9GTIAwLh4PoxwiYIy/rvqZQpnrv8oQHjVtqr39/b2NjTjVgltpdBcNlsD8KPBQuF1AL0AtlkTY7uwrmu1YGgM1gRIZ2aNmUkwiJmJGQSEt6N53FC7hfHqjeslE8ApXavXxkcugZBn8ABc83hf39ZQvzWh7R7eiAO/zb8LxhORBWxBmBlnzw96l4P+9POfPhfZ7qtqJFBylACSowSQHCWA5CgBJEcJEDkUdwKziHTHEI35VwLakauf7+zK/UTXNe8B8kVCcbI0s9+REAKmeW3HFNd1L37xxbmZIV7S+MyNZwiPSMcB5jI4OHgIRI/ElkBEFApD8LrODBzu693xaMQpzRD3I2Ao5vihw8yeN/9LYr0GcQsQ6OyWVsR1/be602K+BrEKQESLvgYQ7C+AQLzXQNUAISNUDeDLoheg+SRPV14B0un0KQAeU2AWB47vVrlkWZZ1KrJkGhCrAB0dHVMAPogzh7CxLduzjMAfPPzww1MRpnMDcT8CwETvxp1DmFi2twAAYv+3xy+A4xzEIp0CZFm23xgAO45xMMp8GhG7AF1dXRcBnIg7jzCwLe/mDQEnHnpo68UI02lI7AIAALdAVRgGtRav/oEWEcDU9bexyHoDQgifBiBZQhhvR5qQBy0hQGdn53kwvxF3HkFSrVRn3gDOhSDeCHu273xpCQEAQNO0FwAU484jKEpTZa+ioq5rL0SZix8tI8CGDRtGGXgp7jyCwLYs1OuNf+NHwEvbt28fjTglT1pGAADQgNewCIaHfb79FwDxWpS5NKOlBMhms1UwPx93HreCcF1UKo3XPCDg+d7e3pZaEKGlBACAXC73JoB34s5joUxMTHoVvdPbu+PNKHOZDy0nAAC4jvMsgIG487hZajUL5Uqj1T14wLaqz0ae0DyIdU6gH4ODg+tBNABgfdy5zAdmxqVLw43e/g2xcLb19fW15OSXlqwBACCXyw2BeSeAWty5zIfJYqnRza+BtZ2tevOBFhYAAHK53ACYnwHg91I9dmy7jslSae7/dpj4mQce2NbSj7KWfQRcz/mhoUdIiN8DWBl3LnNxHBfDw8NzJ3+Oa8Tf6+npORxXXvMlEQIAQKFQ2ATgIAOb487lKq4rMDw8DMeZNe3rPwTxnbBW9Aialn4EXE82mz1t23YPgPfjzgWYftlz5cqVWTefQe+nU0ZPUm4+kCABAKC7u7uYy2YfJ+BlxNguEEJgZGQU9fpMCg6IXh4qnHt869atiXqfkZhHwFwuXLjwFZf5RQJ2RRnXqlkYHRu/NtuX8Aeh0c8e3L79v1HmERSJFeAqhUJhh2XXD6VS5rKwYxUnipgsTc/htGy7ZKbNRx/s6ekPO26YJF4AADh2vH+iLdO2YsWydqTS6cDPb9t1jI1PwLYsWJaN4uQkKpVq8Qd7dt8WeLCISbwAH354slM3nJmBFk3TsCSTQSbThnRbGkQL+z2+YEalUsFUaQrFyRIqlQoqldqsH3qwq6/fu/e7sc/ruxUiXR8gDHTd3XL9ZyEEpsplTJXL0IjQ1jYtgq7r03+GDl27se3LzHDqdTiOi5ploVyuoDxVwfDoqOcCzzwdWwkQJ0S8xasOE8yoVKuoVG98AzstgwEww3GcxtO3CJ43HwA04i1I+A9bEi8AwJsXsuyK67pNf7dnmqZvOYFaZlBqoSRqHKARDGxpftTCME3/7weL8GJHReIFACjUm5BK+dQCpASIlf7+/uUIeb5Ak8fA+gMHDiwPM37YJFoAIUToz+Cm7QBKJbodkGwBSA+9Ck41EYCnewKJJdECgDmCGqBZRynZPYFEC0Ah9gCuYpqm/2hiwhuCiRYAHP7FJyLohu5dHkEOYZJYAU6dOpUCoTuKWKbh/RhgoDufz6eiyCMMEivAaKm0CRGNZPqOBQCGbWNTFHmEQWIF0N3oql7T8P+Cs5bcnkBiBQCiu+jNegIaaUqA6NEi6341GwwSHP6AVFgkV4AIB2B0XYPWYA7BtVTCfR8RJkkVgAi4O8qAqZRPO4BwN1ptK5B5kkgBjh49mWPAexfmEGjSDlj6m3w+F1UuQZJIATTNjbzKbdYO0OvJ7AkkUoAoewBXSTWbHJLQnkAiBRAx/D7QNJsN9iWzJ5BIAaJ4CTQXw9Ch+U4xT2ZPIJECxHWx/doBYc5NDJPECXD8+PFVAHfEEdtMmZ6rlRDQkc/nV0WZTxAkTgCi+KraTCbzmV+5bSevJ/B/kaOnefVwGsIAAAAASUVORK5CYII=";

            doc.addImage(sanitation_facilities, 'PNG', 23, 142, 19, 19);
            doc.rect(20, 134, 53, 30);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 41, 140);
            doc.text("Sanitation\nFacilities", 45, 146);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.sanitation_facilities, 49, 161);

            var recreational_facilities = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABpASURBVHja7V0HmBVF1gUm55xzBoYZHGBIMmQBEcSE6Vd3WVkVRFFWV8HsuIAKJmRBRRQRBAUVluAikgVEUDGsoqigAoZ1jauw7lp/naLfo7u6ul/3e/1e1xuZ7zsffDPzerrrnq66devce1sRQlq1NNCvYoqeFCMoRlNMpJhO8QTFaoqdFPso/k3xH4pvKD6j2EPxGsVmisUUkykupuirXLNNixurFmLwKsXQ8yk+oSBBwk8UmyimUgynyDhOAHcM3pbiUoqFFAeCaHAreI9iJkU/iojjBAie0btTzKP43GWDm+FLiocpBlFEHidA4EaPpriQ4lW7xsjNziC9ezSQ0RecRu665Uoy78FbydLH7iJ/f3oG2bJyDnlj/QKyd8dzDLs3LCRbV80la5fOJMufnE66d6lzggxfKUtF8XEC2Dd8PsXtFF9YGeyszDRy1vD+ZMaUa8nLqx4lX733Ijn82ct+46Wlf9X9jZzCMlJYVkPSs/NIbHwiad26tVUi/JdiCUXv4wTwbXh47osofjEb1Pi4WDJiSB9yb/ME8tq6JwMythEG9G7U/M3omFhSU9+V1HTsxlBV14UUlrclGdn5JC4hySoh3qA47TgB9IZvUrZmhoOHAe7TsxN55J4bAn7DrWDT8od195BXXOElAI+qDl1IdkEJiY6Ns0KELRQ9fvMEoF8pFLMpfjVby2+//jK2Vgfb6DyG9O+hmwWqVbOAEYoq2pGk1AwrswKWhqrfJAEwFVrZwk0Yc37IDe8B/AkdIYvKfRLAg8raTiQrr4hERceYPSMCUc1weH8TBKBfeQrzLTlRD02b6BoBgOGDmzT3A2NamQV4FJRWsxnE5FnfoujSYglAv1or0bpvRAMQGRlF8ksq2b/q729c9pCrBNixZp5uKs+lOwK7BACaBgwl1e3r6TNGmu0YEH6OaVEEUEK1643Yn5KeRSo7dGaOFP+zQ++84CoBgDNO6cfNAtG2Z4GGHn3J4FNHMvQdPJwUFJWazQZvU1S3CALQr4uUGLruQTElwmHyDFJxZa3m59lZ6a4bH8BWs02bNtq4QEGpLQL0HXyqlwAedO89gKSmZRiR4FuKoWFLAJycUUwz2tJh/1xd36gZJDhY6t9DJE8GAgBnjxioXbKionX3b4T6rk0646tR16krvV6UiAT/o5gUdgSgX4kUy0XGRxSttKZOOFCIsql/95KLTpeGAG9ufIpERGhnAez7rRDgRLr2mxEAaBpwMklMSjaaDZ6hSAgLAtCvLKOgTmZuoelAJSanan5/+u1XSUMA4Pwzh+gc1+o637PAScPO8EkAYODQ00lOXoERCXZhbKUmAP0qUY5HNTffJiKChU19DRS/TVq58D6pCPDOlsXU6BHaWSC/2PSZ6hp7WTK+GlXtOhgFkfZijKUkAP0qVVQ2nMccS8radvRpfKynrbiHdiP65wu/O3eY5h4j6CxQZTILNPYaaJsAQOfuTSSK+hkCEuxzkgROvvn79Ot9AouGWVkn4ReoP5uYECed8YE925aQKG4fn5VXHND6b+4XpIhI8BFFoRQEUNZ83bQfn5jMTsysbpPyS6o0n+9c31ZKAgDQGGhngUjDZ+0zaLjfBAAGDD2NpKVnikgA/WKOqwRQvH2dw5dAWWvFOVIDDqL6GuedMVhaAmBpiomOsuTg9hsywpbBBw07k/TqP4Sc0NiT+gJ1JL+oxGgWALZRRLlCAGWfv1z05ts1PpCcqg2I3HbdpdISABgz6kztLBARySKZ/HMNPOUMwze7e9MAUtfQlZRVtSXZuQUkITHJjtDEg/vcIsA00ZpvZ9pXIyYuXnOtxXMmS02Afa8tJ7ExWictI6dAEP8/mXTp0Zu0q2sgxaUVJD0zm8RY0wzYwVkhJQD9ukDk7Vt1+ERozYVaodmTmQDAlX88R2cM6ACS0zLZy9CmTYSjhkY4urS4gGSmp/I/+85fXYG/Bzs/8vt8K1s9I5S3O4ELsESQH/ZtlJ4An76xgiQnJTiuLo6LjSH1tdVk5IhB5KZrLiVPPjSV7Fy3mHy3fzs58vlrZNuaBdQH0W0Rd1PEBZUAypGu7lTPSpDHDIXlNZrrVVcUS298D6bceLnfhs7KSCO9ujWQiy84g9x925/I8oUzyJ4dfyM/H9zJDG2GB++6QXTNR4NNgNH8H83IyQ/I+ACiaeprnjqkd9gQgJeNiabt8tJCcvLAJnLVmAvJ7HtuJuuXzyUH313v08i+8H8jTxH9zd8HhQCKkkcj5oiOjfdLHcMjJSNb8xB/HndhWBhfJBw957STmIZxwexmsnPtE+TbjzYGbGgjfPPxVlLbtlKUvlYXDAJoZVx0u1JU2S5g4wOQVauv/eh9N4YFAXjpeHlJAflx3ybd7x35fFfQSPD2y8+RpMQEUZAozjECKAJOzR9JpW+tE8YHIrjQKjJ3ZDf+umdn6d5+I+IeObgjaAQA5s+eIloK/uwIARTp9gFeDFHZvpMjxsfWkb/5UOj+AwVyFNT3XFVeRP69f7P49w9sDyoBgIF9db7I17CdEwSYrUuQKKpwzPjQA6qvXVqcJ73xkV/IjwlyD80+c+TQzqAS4M0tz5KoKJ3YtDkgAigZO5qkjaSUdCbgtLvPx1YP3j4cPqz5EQbKWKR8yU6Anl3rNffcrrqU/PTJZvPPhWAWmDD2d/x4Il6THQgBdvLx7pLqDobn+TjSxakeDkYQ20d4l4/w+QJi4Q9MvkZa40Okwt/zwofusPTZYM8C/9y7meTm6E4O7/eLAEqiJqd+KWFvM9S7EHBCwwcZF1Py2D/ECEsSdO2kVS7Xt68kP3+6xdrnD2wL+izw+Mw7+PE8YiYgMSPAIt4wmAGcDntizce0z5+CyUiC55+Yprv/Zx6dausawd4RACd2O4G/z8dsEUDJz//FKSMjto/wLiJ8CPJgu4Stntrbh7FlJwFEKur761Rf49d1jhx6NagEWPHUTFHGUTs7BLjdH0NDxoVBgpgD5/k40sWpntWDHZlJsGTunbrnxYzg7/WC6Q8cPrSLtK0q02UiWyKAUpbFtDIHMnaQtAHdPqTbcIycEnDKSAKs8R1rtZI1+AKBXXdrUEkw485JoiSTPCsEuFAjdcpIJaPOG04m3zCWJWqGIldPFhJ8//EGJgKddff1upfAKcl6sJYDnBOkpegSTcZZIYCmINP4S84jb21aRD7bvTKkgx9KEnz57hqyecUjZM69N5Jrxl5Ahg3qxSJ7fCaQByd26+jo3z9y8JVQxQU2mhJAKcWmGfAXl8xkBPh6z9qQT71OkwAyrhcWP0Duu2MC0/T169WF5OVk2vZ17Hr+lreIDi8JH+xcyZNYtwzwBJinWecaapnxAcM4t2QkwH0igwcl4f4yaSy58OyhbL1OSUp0bOsaVL0CIoaOEWEXnc16my4DPAE0RRjvmDiGGR/roJseuBEJ7mm+mtX1u+HqUeTMYf1JbU25Tq7tRFBK9H2Ukgvqc4MILGbgx1Ey3QUcpssKrvPw9EmmywBfflWjS9v+wmOMAPtf/5vr2zARCRwVXEZEsMxlHE6hrk9BWTXTOULwUlHbSZe3GNLUdbo8wKCMEHAaMUNQIx/991X2fWZwSprDB7ZqPvsJtR03blgGckUEuFT9gAOaunqn/y/+8XcpAjFOkABH2UhcScvMJTmFpaxARYUFNTOfuQTAnwgH4UqXju34e79cRICF6l+aNH6UlwD/+uAlaR4G076VaRv1+nByCa0+6vvhEMvfnAVv7kKsNnehR2N9WBDgpj+N5sdog4gAGtHHc09M8xJAJok21nxdKlpyqm7adkqtxFf64v/2igX3Sk+AbavnioJCOV4CKFp/7y+kpyV7jf/25kVSPQwcPt74wTC2EWLjtPq7xob20hMAkUzBdneEmgAauffg/t29BHB7B8AD3r42NbsopARAwWh+Fnhu3t1SEwBCVf4gi+I6NQHmq3+INcNDgI92Pi/Vw2Crp75XTPuhJACbBeITHTkVDJXx+TJ3CuaqCaBps7LokcleAux/fbk0D4MgD7/PDyQlzf9MJt3bRJ6eMyWcjA9sVWzPmiFpfujZ/wOfvLFCqho9vLcfLIfPbi6DLWWQ+8ZnqmEPAXryp38e47NDoDdXSvNQCO9qM5PiXDG+pyK4v9pACYzvAaq7sNZqGq9WTYCDb62ShgCI7fMKZbcIAKAYhvp+2leX+VYHu2R8FLPi4xiK6lu7Axh56kANAWSo1esBDnZ8FWQIJYor29vOD3DL+FBsp2bm8D+D7VlTRe83rx13kYYAn0sSBhYpcs06eIQKCVz9Hmgf3Tg59WV83Cu2zNzPUeWFddT0fvPOm6+Qdgbgj3SNchRCOgtU1eoGfe79N0lnfAAvDPc7K1op7VS930TnLRl9AIg5+AcMNLbv2CzAlbitKC0UZgm7aXyD7SvK0rNeuscyXCl7NbuA3XLsAnDyxp/qyWB8ADORG51O7BjfYLZ6S5f+9dTDf9EQQJY4AGRcfC1CWQjACl2npGmXp6K8oB6i2TU+q8ZaXSdqY6ct8bps/jQNATD1yliXD+f5MhFAMLjkwanXSmN8AFFT7jN4+VkLde83PSLQY2cBy6QgAASc2k6epVIRAEBcQlM8Kz+bSctlMD5Q0b6B/9z2VkrbMkMCvP/Ks1IQgD/OVLeakWYWqKnXGcZJGXsgxheV46N4uRVf+Gn5/OkaAiD+7rbxodvnH7qitpN0BBCVvMXp5dfvv+S68Q2c1Y0gwGdGJ4Ee/Lh/k6sEQNIGL+CU0fiisveeQ6uighxyUp9u5IrR5zDfAJ3Kre6wnDC+wTbwpVZKRSnvNx+fcYuOAN98sM5VAiBjR6NYjk+UlgCCadYU6WkprOII0u8QhFtGZ2CIcNQni0i2DdT4R4WtupJya0CA19TfnDXteh0B3FYFI12L7zMoo/ERmOKPiQMpF4uE1P5NjY4YH0CrO+5aq0CAzepvntChmsybcauGAJ+6HAxCrp6bMrBAjO90ooq/xhf1ZFDK/bdaLPpDPRrryIJZzYwAbvfuQaKm2zIwf4zfoW0Fk9RDVYXKYtgRjB11FnurC/Ky/SIATiD9vc+0rFxROzrWq9bwDzZ1byCL5kx2Te2CfTSfpeuGDMyu8XEe8OGr5i8OKqSgUgoqpqByCnIOcZrIdyZzaveDMvbc9e4BAS62wryhA09kjZTdaNIkiwzMSeObASFkVFbBjiwjPcVW70U7h1bIEAIB+tpJlMR2BL10Q6VnFxVnkCEGECzj8xh38dm6LbCoNY0VCNrXnywUhYJlUdExpiXQ0UsX7VSDWZOHL8uibjqNXL2WbnwA1+MdSX+UUCjuKRjLMk/zJ01379zCMlJWU0+y8otNiYC1Ge1UnYwWIvVLkMwoBDRuSNdqqcY3OgjDLGC3WiscZ+6ev2cNQBRp8CbNPjsjizlaAOLbmXTbFSnuYuktA4eOmoFkEa1edD/p1rmDX54x0rWQsdMSje8RwyAuoJkFsu016kjPyuPve6U6MWSq5s2Ki/cSQE0E7L+joo2JgI6aaKpoZ9uIkGivbubRM9TkQVkWeMmmRIhPDLh9jWzG92D8JedyyzCdBWqtzwJ8NhPFBDUBhvPOHg4OeBIA6JOLiJLZjIA1C9OWmZYAFcf4I14eEIHy1bhQmQPFGUyjaNRQTp4Wum18T4OqhHitE4dSvdbuv1FUV6GjmgAZ/MOhFrCIAJ61B8WhswtKWAt1wzcyJpq1VvtUpSrCvndQ3+6mBkSuna8ijJCIIT/f7DrQ7QcSOJHF+EYhcTjjVnZERfpDoK+w/vP1Ad7jO4KICIDDDk2VcMoulIGPMCFCUmI8S+o45aQTTQ2G9Cq7FbiQn49kFrPrQj4GPVw4Gx+AQBdjqVFGZflWRmHXwD3D06ICETN5x0pEgKMFGBqF0wy6aBv1ATAD6u0jrSqQaCNStDFzmBIhOdWylFw243swcfzvtbEZzAI+urfwGUwoByQiQD+dH1Al9gPM/iAGDnEEK5XFEeNHJo2T6VTI0sVMYlrTOCWNafjCzfgAEnX4/AgzfSSepXVrXcHLShEBIii+5HsCGs8C5uFYRKsEU4+3uxbi38HKoMFMghkFuXpmRICGD7ubcDG+Uc0fGBh6P4tHwPvN6gQ+zEfcjAhgtU9wEieROu/0QSFLmsDMghkGByxmRICMC0es8G9kN75HIpeemmypi5sgIXSuGQEG8Q8PFYm/s4DoBKp54mWuFJZAuhYMadc/kc34HmAc+SWbd9BFyasUfcwIEKlsEY6tl8lpAc0C6KKtvh46arqZPYuMHSRthLPxAQhNUctBq5TK9nX8u9tKtfCp/PpSQrdQ/s4CfAt1tFN1e/Bw3AphJnT7RsaHmENW43sw9aZxhrMAooQC52+0FQIUKy1GNHFnIwIYOR+iRAQELr79cL00AwixCVQ6fOEpRDJlKo5phG/2riM5WRlCvSS25IJGknFWW8Zo+gTjRNCIAJ7wsJUIFLx/WadTPlQqU3EsM6BjC9/TGTaJitad/U+10zOotz40XGZIAN758LaF57YgJw/oKe1AQrevvldo+MKBAN99tEGnL0TtJEHTqGK7bePe0MwCdEuIPbOxQ6g/meJLklx16XnSDiSSNoKV0hWKIto+HNol/vQN1HUMN/MFRCFiPgQ5++6J0g4iMnbU9wr1brgQAL5McWGuGQH6+Ns6dguvRDHbEfBLAX9kvP65WdIOInYF6nuFdDtcCADg5TIw/q5Aegf3EIVPzWYBzzkBQsH8Z2UqOScSpmhyD+i6Gk4EQIyjrDifH3M0/u4ZaPv4JbroYGmV+a6AGr+EK0eCoIXMA4hETf451Z1NZQdiFqnJOtXP7IDax6tKyf9He1Qcb0oAAIKSYLZaCwaQqKm+Z4hXwsH4OPwSqKsOUaQGTACFBM382wHFsBkBUjO0O4A/nH+q9AOJLF1Nwaz7bgwLAky7bbxo7T/Xkm0tEiBaqSilcQghwDQiQHySdgdw1y1XSj+QSNFW3zPStWS/59fXLWDSO874q63Y1TIBFBJ04UPEiA0YiUf5SBTy3mUfTOTnh6w/oENnGg11OhUUcjzKHCeAQgJdIinedJGEnA+tvrd1ifQEAEn5sq8y3+91V1wkmvqvs2VTmwSIUapL6iRJagIUltfo1MEf71omPQH4RFQkvMjUMEuNJ2c1i6Teu3GkHzQCKCSopviWJ0FO4bGzAl6GVFNZ4i05J1NTBZE3zWfgIEtXtvtEboSg8AR0HBW27Wn3AwoJhiqtx4ROYRqXhjSkfw9vtZG9O551vaa+GfiEVKRoy3R/IGRaik62dthXwMdRAigkmKRLDYNTWNWBqYj4uLq65Mye7UtDpgu0C74mD/LzkaLtdpUUAEfUgpg/on3n+G1Hfz+okOAZUaImn4d+963jdYWn3t36DPlOInEICCmqxsWnu7nly/xzz1pWv0lwb5MCsmGABEjAYYMvbd3Sx+7UEcDTlFKGvsQ2euwwhxYnh5+EsKE2jG+QR/loIPYLmAAKCdB46AOzYhI7184XEsBbkHrXctf8AjvG12x/42LJhDHnkwNvrgr6tG/w5q+liHKdAAoJCik+Eg1UQX6WqfE9eI9uwUK9JJhV4ISkGiIXZDnBwTXMMkqIYxHDg2+vDorDZ3DO/w5FiiO2c+IiCglKRCRIpR6rqPys0ZKANnWh2CraKb/qyXIyI0JyUgLra+xUjyVs9VJTkoyMX+SY3Zy6kGom2KOr6RMVSW6+ZrQlEngcRKx7MjVb8NTZgSqKl7prCJ+cyJ41kKNkBHkMCkyuderNDwoBFBLkKHXodQ8wfHATeXXNPMtE+HDn8+SHjzdKYXwNEejSgOIM8G8MawCnJrPsHTuVwiHtQiUQQYSPOXxOrPlBJ4BCgiiK+4UZwWVFLKffKgmwLKBtzY8OhGSdqrrtzXuo7cTy81ubEAFCmCk3Xm6aZ4Alb8Oyh0i7qlLRNX4NdKsXcgKoiDBSqUalzdOPj2PHw1ZJ4CHCfrr18ndGcNr42gSYTuw8RJCJcyxUnpXBzu3ViTE/UcMfeGsVufbyC9gyKfjc4UCCPK4TQHV28KZoUCDA4BtUWAFqD9nZMQTT+HwmFLJ0Dabwo80uczLJvc1Xk310e/fikgdJZ+OSeF/5G96VigAKCeIoHheWmKOeNYQY6o7lVvH+9qWssaVZQ4tQGZ/vGYBETTMi5GZnsJnQ4Oe7/TnYkZYAKiKgV+3PhmvlTeNsk0DtMMLzVgeU3DC+ngiZLF3LYoAJYo7r7B7phg0BFBKcQLHXaBAa6mvI7GkTWRlaf8nwwSvPsmVihKCuYLCNj2xpVBlB6jwIcFQf0ZYdlPkw/mo7Sp6wJYBqSQDT/2U0IEgkRSkUO9tGDxBBG9S3m9D4kKs7VW38qLEbWaCoQmVwD/C30rPySWRUjJnhD1kVcLYYAqiIkEoxhe9dyEfY4COseXpGYMaPiGT1cvlMJjhu8OLxxmJ/j2APAKOy/+N79GcwMH4Pv88bmgeqquBI3GxXoGzvZluRbrdYAqiIkEfxVz7/gC9M3a9XZ9J8/WVkw/OzAza+04A4NjOngETHxFlZ63eFwsMPGwKoiFBBsVB5O0z7FqCCx9g/jCSL50xm/kKojV9SXUfyispZBjS6mPl42z0p2siy6i3VmMt0MyoidFR62/9qxXvOykzTVflw2vhYz9GuFhU4UEy7tXXP/itFTV0k5VjLeFMqIuRTXKlkKf9q98we0zEKJaFUOjKZ8Mai0QR6DMAzx5Evikoj1xGpbNn5JWwaR1QvmW7fsI7HJSab9kwwwesUoyhipR5jmW+OI0MBxXil3+2vfhgkFPiYYg5Fr7AZ13C5UY4MRRRXU2xzmQyorLqI4o8U5WE5luF404KZ4RSKa5Qj0618Q2wH8b3im4B89Z6S62E9fuH+ACbEyFU6oo2heIDiRYp/UHyqOGY/UPyinLh9QfE+xQ7l95YoZJqu+CDDUGC5JRicx/8DEyLaClD4WBsAAAAASUVORK5CYII=";
            doc.addImage(recreational_facilities, 'PNG', 80, 143, 19, 19);
            doc.rect(78, 134, 53, 30);
            // +8, +5, +11, +26
            doc.setFontSize(15);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 100, 141);
            doc.text("Recreational\n  Facilities", 100, 146);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.recreational_facilities, 108, 161);

            var security_facilities = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADnwAAA58BwyKAIwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA5CSURBVHja7V17cFTVGQ+GBGQsIhaKGMsrCGE3u5u9N2+QyEskVMZBsJQBq9ZOyVQUYUAoWGpbWrBOp44IRUV8VJHhIfgAhBleQsXyfskjhFBehopgQEQIbL+TfIHN3e++sjd3z549f/xmGHY395zv+91zvvO9TlIoFEoSDV6/0gTgBQwF/B4wGzAfsBywCbAXcBxwFlABKAXsAGwAfAJ4H/AK4BnAzwAZ7G+KKCsRlJ0KKAI8D1gKOAioAoQcxlVAORJkIqAAkCIJ4L7CbwIEAeMBKwHfNYCyrYI9ezVgCiBPEqBhFe8DvAg4FUOFm+EIYBrbfiQBnFF6a8DTgO0OKegaoBJwDLAH8AVgN9oAJ9Em+MGhZ+0CPAu4QxLAvuIz0Wi7Ug/BnwF8BpiD5LkP0AHQgm0fFp/fDI3IQWgIvowG5Ff1GA8j1KuAuyUBzAWvAj7AN9WqgI8C3gCMBNzlwhg7Ax7DZx62aUQuAmRLAkQKtQDfMKuCZMbfE4BOHIw9DfAU4N82yMAMx0DCEwCE0AbwrkWhHcDj150c2yzt8XSyzSKRZwFuTzgC4FGuBHDOREiXAfMA+XF4XFXQoVRlwWYZBUi2uerMQZ8Hc3Slxg0BYLBZgM0mQvkeMBPQTgBHVUecy0WTOTNPZJbFvzlX89sR3BMA3/qpJm/EBcDfeD861XP+rQDTTY6Zl5g9YeFvaZ1fy7kmAE5+lckbwGyBNiL62zWySAcsM5HFh0a2AdoP4d/fwC0BYHCFGHjRmyzbx/qIrnhCLv0A+wzkwmRWpPNbrX9kE5cEgIGNNXDmXMItoUmiKT9MPo0BkwxkdJXaEohtZDNXBGAWLTpK9Ni9P5785S4QIQ9QZiAvFgNpFPZ9rUG5hRsCwGCaojdPbzLMxfsjqfgIud2KstGT24La1RKN5fDPtnNBABhIc8BaA5/4b6WyTWX4mMGRcT3gNgxo1Qk8xZwAGLnT84CdAORIBdtyjX+tI8t9BEH2xpQA7MiCrlpqwIeYi1Qq1rZMu2B+gSU3ecwIgClZ63QGxuL4raVCo4qVbLVAgNJYEkDP2mekaC4VGbV8b8Hop2EmUkwIAA+eoDMgljjZVCrQkgxTAGNwq6xEC/8i+kkuE14/Cv91nQDw0Ad1EjfYsn+LVK5lOQ5zIP3shKsEwKxcKhuX5dWlScXakuU7DhBgm2sEgIe11fHts2VLlUq1Lc9BDhBglCsEwJDuJp1s24d4F3bXnJzbPT51pMev/tPrV5d5fep/qsnsU45m+pXPPT7lA/j3TK8/+HB6bm5zlwjAZPpLrErai9nEbBvdAvgcsBEdQGswlWwl2lgsYvgvTHpt5BYBntRh4CRele7xeFJBqaO8AXWdzYoh5rlckRkIDk9KSrpJ1BXIbvJjJSGoN7mcWFJSI3jbf24zc5dGQNmZGVAHJDoBluokanIXzvX5ctMya5ZPpyt/Vno8+S0TjgAw8cE6AinibULdfEqujfKxc9X7rq/av15p8TeHfD61S8IQAMOUJwlBzOVuv/epgzGhVDfLJtOvsiqffmAbRPgqFAXmGlCKa4xEwyqgbzx+pXuiEGAWIYDTgJYcvvmXdBRW4QkoJUVFRY2t/r309PQmQIQxsDroReXOeILBdKEJgGFJyts3nKdJ+P3+O/WX/eB7XboU1jsBpXpV8CuLdUjwZSAQaCEyAdZQhhBv1r6OwXfN6wtOcuoZsDVMo0gAx8SlQhIACziohggdeJoAO6dTivEE1PGOO210SODzZfUWkQBvEZOdztPga/bpyKSJTJ8yr6FWG53tYCv7TBgCoL//MlGnx1VhJhhoowllHGvfvqjBQtHVNgFpGAaHikSAP8eDxw+W5C0Rb78/+GiDHzfZ6SBCPuoyIQiA3THOEATwcaV8b/ZdhMu2dMiQIclubD2En+AirA7NRCDAbwjlf8phGPVJ4u13zUZBZ5HmRKAO4jTk3EKvhFz7xUZYtaMlQD/+JqUuIiz/Qhe3n+KI5/uVFzlU/riwMY4zI8A9VLcrHllNnP2r3Fj+NQ4irR0wnzPlZxB5G4VGBPgLQYDHuVzWfNXNoeqko8VAwHUDSAF1HWcEyCf0+YIRAXYS1ak/5jHWT1TIbo0BCbXl3Qc5I0BbggC7SQKwMz7x5Y08vv1ohdcdqy+4loNt6BSHNsBOQq9pFAEej5dUL+bo4ZQAX3FIgBmEXn9FEWAh72d/SYB6EaAXodeFdQiAnSrOOVVtIgnAFQFY7eZ5IhOqcTgBqOPfLEmA+CcA6pdqStU9nADU8W+gJIAwBCgh9PuncAKsIo5/zSQBhCFAR6qAN5wAZU6VG0sCcFuCVkH5LGoNQG3FzCpJAOEIsInI70hmH3QilodXuCzzCgbTPT51mKfmgijtmMu9AXWqq4gskr0ApJgCY3woIyurXRxUIXdkH/QlPhjDVeAnU+lqsx8/L1jBqpQ4IcAfqCivXvyfmxMAvGl9TYo9eMdZrzfo44AAI4mxlbAPXiA+6MqD8jHkeiyOlX+9uJRVKceYAIVUB9IkvMdGewRM5WLp9yvPxb3yryerBJ/goOOYdlxLk4gr2Y5xZLis0A56fEFu6JM+BaHlfQu5xEcwttH5uRQJXuNAntoWs7uSiBSwMo4I8L/wsfUMqqHjxT1CJwfyjXIYoxpQtdvAFg7keVDbtJt3AtQp9nxAzeZe+bXoEVQjOqRzIM8DVghwWBJAWAJYWgFKJQGEJcAhKwQ4JAkgLAFKrRDgoCSAsAQ4bIUAByQBJAESggA7+3cPLYFz+9yi/NCR4oQgQBlFgN2a/zyfCARY3LsgFAj72/cqamgh/J/gBNC61Xew//yY8Fr9RGQCrO9XGAoGIt212VlqqKy4u5AEwORQbd7HEvbBSwQBCkQmwGxY8vV89vPuzReVABl6waCniA9GiEyAaT1ydQkwo0eeqAR4QC8cPJD4YKrIBFgNW4AvQBNgZd8CUQkwjpjvfXpLw9uiG4HPd49cBdj/iWoEwvPnEHpOr73p85pTlxHHCwFYxC4v64aS+sAp4LjAx0Ci5yMzCFP0jgcVieAH6K/eUNIvcrKF9gPg5Z0Rqf+1H1L3/nWUBBCDAHilr3Y8q8MJQB0FSyQBhCHAg4R+Z4QToJjKF5MEEIYArxH67RVOgGZEq3VWUpwiMgFGgNJr//ajeWISADu/nSB0m6ptEPEpbzeCNDQB1vQrDPlZs2fAe73yRSVAltHqHv5Fov2p8lfRw8H7B3QPHXLI/88pASYb3TNo5iveIfMB4p4AVEldB702ceVEY8E2kgDxSQDW4o+4fPqAUZ/A2TxFBiUBopYf1fntH0YEGED8oJskQNwSgOoR2N+sW/hLYcvGszyVMt0fJwQ4MbBunAGx12XZ9SV7KPiVZCsXRrBCwphfiYaXO9eZxILe+dwrfy6ZcKIuinVdJeAZSzeGcNQQeiYVs8/JUkK5AZVLZEe++bWt5Ce7qHwvMYZvWUwgrgjQza8yJ8YVAcrDKwOBQHsXCfAGlf5l+c4grvoC0f2A4qs3gO9Gb14XlH8H0Umdxf7bxSUB2CUQmWCMGlwLy/Wb76bykQDUhV/zLd8ZxCtYoyi2j7KbOrFP/35OsdfrDy4E0k7wKMpPXVZ+O6IJBENO3BNAwhIBqBqPDYaXb0jBCaP8h3W2oZ6SAOIr/zbiHkOG102v35ECFIIAVMbPaUBLSQDxld+TSOtnGG7l91KI8a38JkTjJ1s3vUpBxjcB3iSUf5E1AJcEEF/5k3Ws/ol2/k602ab3A94FfMhuoACsZAUHWIa0nt076K3pqb8FO5LuqnGUVBeiPAK4SSqzXrIfqrPv77SbyR3NIJ52wFU6SCrUttzzdLqnnw7P9XODAHscIMA7Uqm2Xb0VhBwv1Td1L5rBnHGAAMOkYm3V9+1xWo7RDOi0BQVfxbtpLqF1egETEw7gFpIilWtJ1s3QtqJk/Fw0fzuaQZ0yUf5HPF89F2du3o0NtYVGM7DjFlaAzYBWUpFRJXfs0pHtBuYIiiUBjlrc51l/2s5Sobbl24lo7Hi9uIMVfTjxHCe7Tp4nulHXgl38kCcVa1m22QZb7HYn+zg62XueKbkVLvshHRflI1LBpnIdTeT01YI512518nnRDPRL7fVoYRbrMoMtYb7TkxBE8S0Aiw3kxmR6s9PPddIRVBn2WTK7ft5gMuW115dLXF/yjxjI6y12xW9DPNvJurPviO9MNJhUFd5mmZrAir8Z8EeDJZ/JaAqLuzTUGKIZ/DbNYH/Q+V6xidOI2RLFCaj8QSZv/Uk3OrREMwFt3V6VwXfb6LSgCQfLaL07QY53H5vIgsmqtRvjiWYia822ACJ8PA5dw3oTZ0vhDBGdRzCnNMDfTQpcqjDO71qYPJoJaS+dnmvxdwpgn8kbcBGNyM4CKL4Lk40J8UOYM5Hj9viiDVBMR4cQ6yySZuO3KaxUGXDOQjBpMS/3F9iUTwHey3zVZI7fsKZNsUqOibWQWgNetSCkEF5t8zs7+W4xmE8GWvWlFubDMnpej/V2x4vgghjcsJpH8AWuIO04GHs3tG222Rj/Kl5c47y9Qfegx+uaDWEyp9LbgF8Durqk8BLAAp3sHKPtjG0JKk8y53Up7YpbQ31Kwk9jUir7/QTAYIAfXa3JFp7dFGMaCtbbMat8HuAzwNf1GM9lbNjQlUdZ825ItUEF7HGwZv97VGQ5Zijvw/sSzjrcjWQbbg1teZZxPFnVXjSwDnLcEOIwjjEjXuQar2frLPSRr9JpiOAWvsV6iAm87e0JUxmEPgWWKz8e8xCP2zQireIKZuIsAYxlCrdiU0gCxC7K5sGAy1j0Ki5GH/smzLMrQ4OxAs/t2zHhgr3R7wNexjsVWffU9IYKx8Ya/wdkRPdTmKgdlwAAAABJRU5ErkJggg==";
            doc.addImage(security_facilities, 'PNG', 140, 143, 19, 19);
            doc.rect(138, 134, 53, 30);
            // +8, +5, +11, +26
            doc.setFontSize(14);
            doc.text(pdfDocVariables.schools_with_or_number_of_title, 160, 141);
            doc.text("Security\nFacilities", 164, 146);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.security_facilities, 166, 161);


            doc.line(20, 167, 191, 167);// horizontal line
            doc.line(20, 169, 191, 169);// horizontal line

            var hold_meetings = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzA3NEQyQzhFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzA3NEQyQzlFM0U3MTFFN0E4MjhGMDFDQjQxMUNERUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozMDc0RDJDNkUzRTcxMUU3QTgyOEYwMUNCNDExQ0RFRCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozMDc0RDJDN0UzRTcxMUU3QTgyOEYwMUNCNDExQ0RFRCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PsGrDmUAADbzSURBVHja7H0JeFzHcWZ19zvmnsGNwUkcPEVSPExSJ62LsiTLkiytHa0de21ZdpJNNnHs3WxiJ+vYOTf7eXej7LdyEkeWb3sT25J8yLJsnbZMiRQt3pQIXgCIGxhgznd2b/WbAQkQwOAakJDFp+8J4GDmvTddf1f9VV1dRepfGISLcXDKvJ/rDvz09qYjL/2hZqavBiAqZ8qxnuYN33xj483/NFbZkFCsHBAh5nZRQgDkdRmelALYFoBpACTHGIwMqJBOqtDbrYJja5BJKzA8QPF9ivzkBaconBw45xCKuFBR5YAQNsQbbQiELKiqtSEcdcAfBNC0/P1dN38KDm/Wg1wMADiqDoy7sOXn3/5U4+u7/wyF7hOEFv4qgLkOGP7w4Y4NN/3XE1fs/CEOPIJghkGVgh4XuhT42CiF0WEUdJfPO7NpFHZKAdtWwHVIHiXjYKFQEHTRMZH3RyDAuQckRICquRAIOngiGOIm1DUZUFFtQSTmgj+Qf78EA37PywCYcLiKBvHOQ1tbD7/w2zVnX3/QUbTp5YoDp1iGOL7plgf2X33fo8w2J890Rc3/zKQAZ7IGpzv80NMZgME+P1gGCtulHigksCjNC62UhxDEm+mcE0/YeVA4UFZhQLwpC83tOahBYERiwnsGx54IorcmAFxFh8aOPddve+4rP8Z5FJCaoOjDCKkNbHP3ro9s62m+8iATTl7wZg6g86QGHUeD0HUqBKkxHw4w8wTOlNILez6g4C4Bx5EaiaN5sFAzpKFtTRpaVhloSgqawQEQYlkCQFk6aBEg+MVbjrz4AP4Lhe8DqfaJmFlFCk9bC73t0POf7Gvd/CEYTlB443AQXj8YgaG+AA4mCl3JD7Y8L/n0QeBJALLCMJqGDh1HfHD8cAVEy3PQviYFqzemoLbBOWeylhkQlgwA8msy7oBqm+Vc8i6czYqdBoPFQGEzAQG1LNPAHR2pF9//RpCcOVGNKl/3BlhRuUfSlvMhTQ/V8hLOJAOw9xcBOLC3AolkCq7cPgrta000G8sKCMqSakgUKAo/4AoK5dAPV5efhGOZajhlxcEkIRwvOk7RgAsJGBM0hUAulRnhfQdiVEHCoPvenBSboobSmRQ0g66TZXhGEQhpd93mYb5mo6lK4rgMgLCEACB4cQc0NxuwcbJH9AyEfBS2aMPQZiWgKxuCYR4DQ/jlvIcQTUNcH4bD2SboInXfA1Ulc/cHlzPL8sii9z3c7tOhiGOW+42k3R+tPslWXWF7HEcC4dcNACbVYNvYrzbfRZ7d+pxbA+3BBNhowuVMD6EWXK8n0aSPgSvyKkBF7ZlzGfRlBuCdK8QdXznTvlsYtqQF5E0PAsSxYzucByPm//zj3/2L9fHyDTf9/ufuTr/60qh64x0JqG92PPfRvfguJIt8+I+WBgBMh02J167Z2fvj9xnIhGuC/JwkvYiL9KpAmgDinRx/l69rwoJGv1H7bKbxG64p3Dc7AFzHkYQV6tvborWNDXpiYGhk/4nOPSmiDPmFG07vfiHoDg1w2tBsQCB00eMIS6YBFCR5Pf66Ewb1Q2vUBkrItBGYia8xFHVjyIGDInzScGiGEVd70056zoXDBa+qrwtXNTQyc3SEnjl40Dicyf0cv7MI6Jq/trlJ37Bta93ZU6dDg1//whBs3znANm23pJoUUhuQNzEApAfQE2zsMPyxXl96KG6DPrfPIW86QGp+ZptgIR/US/U8NoeMuEgxW8lc/JGo3tLeEhWOY548dDidTKYsRVGooue/kg2QOtVxKtnb0zfcsmpleVmdWtmz9/lQ4mxnB7v+VkcLBC5KiHlJA0Em88F7Bn90+337Pv81yzDKRWE9YKbDh6TxGX3lt/+ld80f0TFDl2GDEjkjtC2ivMPHSOhi0Eo0dmJdU/yGnGme6urt72SUKejxEJgmDC1QyLbj2k21VSuqIqHNLx87+V1/LEZXVobXIAde8gjX0gFAqnxNh+wbHXDb8X/8gwftn/9vU6hFfAaUEzLBz+R23nbytHJUV7m/RPEIjo/CPtgSeKXGxyKm7UDWsi+C7c+rcMbY3N7vctT86DAozFP/nC+d6FWKGormTfLSmAC56CLZ2+5ng/4Xf1b9Qizygxsbaj7WmhtYZ5Hpb6kLB/ay5mdOj/iOaszylc4ZBYrj6v603/y4LtzylpXt9b9968aPw0WYXcsyPIFyOTKSfu7HXcM/0ChhpQeAvKTUdk8/EYNf/bKG6j6SS0PyB1b7Q7+vDD9MXDnwZMrst1XN/WGm5WGexjew0tIfCYLOlPt0OpvLRrbEN1xz7bUfh7fw0ZuAF75/eOzzEUaBlpZSoorPZQAe/0Yl7H+5Fnx+qQm4Tzih3QMVP9irNf5Eg6lujo6vPau0fePwYPglnbqBJWG7FAI6I2Em3CC8xQ/XMnXdsUBzzBICYFz4T3yjBl4/WAmazsdJj7c6mnTcPcmyp9g0d5R/70jr+3nOFb8GYZ83l0koofAJPP71Wjh7ugxnPr+QiQniGpVW72rHcr3Az3muSMAxHKjPdW9BbBogLgvlzQWAc2r/6zUo/BhoUxZvhGXamY3XbbzvXetX3jM6mgLunA9yuMjKR8dScNv2q2/ccN2m+yzTygBchsGbAwCS8GXT0wtf4GE7BvpcI3R72wer79r2xTsf+HxN/bXvgeTwMBipnHemR8eg6eYPwe2/+dl45V1bvsi2t79ffsb7rBCXgbDEx8K9AC83D6fxT5+ohK5T5aj2PXYnXO6A4+aYpuhKc/VGurH5XvPK5v/Qj1rCVnxw1Qf/FvRYDbzx1KPeZda/99OwftdHQa6HDeSyoNy+5a/1uorV/MCZ7zpnhw+7pm2CwvyEUeWyuJYLAEgh1/LpJ6Jw/HAl6D5HWE4OX+FqRbheWV13M7mi8R5eV77NVZH2mTakHQeSjg0VaDJW7nw/wLGHPPbXtPUO75JJZKVp2wvQMHd7+wN0S8uH9LMje8Shru85r/c844ykzgoJOZX58N70suguJQA0HWD3c0HY91I1UC1JGQsqbbXXs/VNd4v22l085CsXMhfOwROFL9f5Uq4DacGhzDIhu/tPYW3N2bzgd/8ZhG55GNCQQIq7+TVB/AxHIfPGih2kuWqHsnPtiHqi/yf8UOfj9unBV3nOSgqFSq2gLSe+kE6nobunF1G6PPApM9yrqiqhoryshADQfABvHFLh5z+poYEg1W7e+Glord4pqiPrHS833/EEODkQQ8DEh0mgFmhEDSDj3+NS4yLvFSRQA1j4HjruIUjzb7v4Phdcn1oOm1bcTzc23a/3jx1yekZfg18cfdjqHz1NdDW0XACg4cSoqqyY5OVcykNSqEDAX0INIBl/YpDAz74fF7brKm1VW8V1q/+jjHsTmfYDM69l2/gwQ7YB1B+EbORq8I/8yKMQvOoGIKoGQ9kUWPge33SDxwtaQf5aHV0P9eXrldbq6+GxVz5hH+/5OehqdDmMuuu6kM3mlhUANJmD6CuFFyDVmkxWePqJKkiNBQlVTHZFw7vkokVe+EU+WgDAiEyfxqMipoI/HABfOAgidcp7bRhNhCPmEAeSLmTOAjvib2bvu+6rvp1XfIy4Iicc1wK4xGEkAucSXJbLOduIKPPQb2j3nw3B6TfKhKpZasBXLdpqdwk5++d4iUEnbxqGT74MjQUuSXOnPWCN/23OY433dSgJ0Ns3/YWvqXKb/aN9f24n0v0X2yRYRqpgGcPA0AT6fD5YJgoABE5OZZbVyLkBQKqRMx0MXnqm2iOAFrp5bTXv4LFgLZnH0uog2nkp7NpqH1C0JJSp4I4hAEwThlxn/hNOZs4YNjjrGu5Sa2Mb6Y/2fdo62v0zoSohQgkrxSCmh7vgzMv/Cr5INZ6VkOw/Aa6RhmTvG2DnkpAZPANlbVvh6g8/DDbyn0RiFJUlKZVCye9SW2BiiCSBBEHp9/sXAQAJZymcF5+uwp+63CNHkanRdY13OvMg4PIywzLNycjBQMc+aC5XvUyNaNABw0zCsNQkCx035Ad2NLCC3X/dV/273/g/1rOHHnJMO0c0ZVE5BbnUILzy6O9C8sxBoHJLm8xxwO+g+EMQqm0HX7QGylu2QkXbNu/9uk+HeLwGSpXGKOS+SZzBfp9vybSEMqfZ/8rzQeg5E8XZ7wqX20pVZIVoqtw5H/UvycYwqnkHf4nX+IBbKH6mgJ0ZAJbpgxERXFRYUpoEz3W8fs3va02VO+gPX/203TV8WOioDQozCZ/XplwUzcHOoobSKAM3MwIHn/hbSHcfBS143o3i+B38FY1wze88ikMzeWExm81CZ2d3yTSAVOE+vw/aW1suEQBkqDcxROCVF6vQA8inNDluTllVdxMP6UFizF39yyX+UYHuXu9REH2n0T9VPXIf1JEcDp2A4fAVwBZrPKWUpUlorNjBPnTjd9jPDv61/erJ/+dSommaykRQbcyGlfYX0qPokVgwiOcAmiUJzGH8OSR/4mtfWLUVqvY9AT17HgfNH7lAKC7oofIpwpdHJByG9Ves/TUKBEm//pUXYpDL+LzlXfQrmKb6yNr6d3GXz0vRSf9+xDLBDbpQVabmN86iZ0GFiSZgAMb8a4CWisRb6FGoLEpv3/zffZtb3m8R8Cu6pqp+tfI4o8H7Ow4QW/DCUkNeYctJK++fQwH32yasrGhGjvLrH31Wivr8PWcUOPyrivGdLcLhJmusXM/ryrcQZ37ERA6wjASe7D4JlTLgIxMD5N5K5oNeS4EUenGzak5S+N8c1oiImw82mUgOx93QfN0B9I3xGvoMHFG+L+vawMZ3MksCVojsydnP8W91W975awMAWnT2v/pSGRI/ZXz7NeHcYusabuMaMpN5LtTJeZa1s6Csvh36zDXgWllwzQz0WFcArLrH+9ts5AmFajOXG6AjgWRzYwySG8jYgQSE9BokKmbDWdo2QAnEQEVV78r4BLJ+x855AatQ/Woob7ry11wDyNl/pkOFE8di47Nfxm9Z0BeDVfE7UBPMW1nL98tQ76iMTzd/GOyeT+XjOisegDSSLlsWiCjCAYTKQD8z8kv7h7/6NNvYfB9c0XA3rwi1eDiUgagSrRxLvdFtZEGrWws3fOJ7kBo4CYmugxCsbIJITTv4y+pwSAK/5gCQgpCzXxZhyKd2gbBdg7XH3y4qI23EXoDPLuWEgzuCM71+0z1w9NhXvfus3XQ37B7p8lSvWgQA0p81k8aI3T10BHpHOthLx/5ZWRl/O9u44j7eXHk99yFS7fxMn5ug0buV6cJy354QnsslASg3IxgSTPi7H908eVavvPotxAHGbX/nici52Z+3Fa70/d2ZDLVUybMMPvfCwTYKyQbfpv9UUOu2x8L5HGIKesY4azOqowkIO4ZtOvtOPk73n/mBUl++VtnQdI9YW38nLws1nluJLAgyH3Ik3jNKlW4gSQyi1qlElV6v6dCqB2ClPH0BaNH9ENd0eKscyrSzf98vo2AjMyvMfhSsw2LBOLRV74Jp4v4CBzfWN3osXR5qcxlVi91wCAXOkfBVt+dnlfx92J2bO2mNZs6e1wjI4nQ1JGFjdQ+/bncOfo69eOxhZXX8RtQK/443VlwDmsLQIxAka/aTZK7X7E+8vrKiJvOHV2/+cKvup3EEQJmizhpNS6czy05w0oPRdc0LPZcOANLtGRmkcOZEFFT13JQUjlT/tdfxSKDiwqVeObOUtNmXfP7IZ/i7t3/NW4AoYo9HCiFf7hSKQOFMHJ5DGFju7iBpY1iQqWEWojKcskx3ckbW2dPxb+y104+xpsotNBZs5EOpDpHM9rlZcyw7lh6N3Lprw523Vz4w1wGSK3xDwyPLDgByES4WjZQYAAr+s+NI0Cu1NqEyB7pslK5ruNuZTrCaAuLg8X+Fs8NHNSFy6HOrM225kX72CJ+86idtfwIBQIsFgWQI1ubAk7lu/F0pwhNQK9CQt7n21MA+HKU9Xl1ARtC8o8bQlLCrkHntC1BwTOrr48sOAOPRTcM0p4sheyFkVVXmAQApACMHcOxgxAPC+dlvqdWxNtFYec0U9Y9zkWXMDNribyMBy9KcPUJ0NTJTko6culLY9gQgSesvQUGLxsTxQR0HPUX0G+eSDiZXQmXqGCx+Pci2bThzpmsZqPz5aAcOsVgU4rXV8wCAtIUnjqpeNa58QaaCH80NtrruFjeg+eDC0C/OfvLame85A2Md4NeizJYMjxQJOhAYQ3fPFBy0ghxlplBSagAo5gGgBshZgyDPEq3yzfXQNA1aW5ov7WzHMaV0adLMJmuAjqMhrxTbeDUuZBrUpwbI2vq74cLIn5z9Odtw93R8STDqU1ye0zPmqRyBlaRI1CmNAEgLFyoLXyjtul6EkBYfAdROVsa1bPtir7XbtgWnTpdQA8wzXCGXgiPhCNTV1SwhAKQwZAVOWYRxkvrnJmup2cJrYuunuHhoX8jh009YPSNHibStlpPKpY0s8QTrzmACCGQQABmc9dWFeZzBLyhfK8YBpJdBk8YpYvOsUOhF3dunoGZc0dxUMumLBQCGMVpijTIFAEyWX1W9CpwTCjASzm2ttfpqS9ZuM6xJV6Co7pFxP4qs3FsmlEZKz1lD1izExSiQvjbV5/1b/i5fo7OoQCdjpLjcRK9c3Ixbx3Ggq/vs4lm7KzN0KzzbfMk9CG+4yQQAyLSh0x2BQvlVPlHQViTQNoWBqAzo4e6nrM6h/Ui2zqVgIUs/NZdQqwPnvQAvCjcesClyqGPZ1y9FMTXJpmuqq0rkty+PANOxk5194KLYFA0Ub+BlnTpZeHli/pjchaEwRYv4Y5KokQnTWC6qIPP/Ghdiok9OrIxhsiJmTiIrjByuVjm/SbwWVWwINVAGecFMZQHkq+jFJ+ESJH1K8hUK/XrtKLf6ewlwBHVNLaoCqf7HRgkM9vm8urfnEcCFwgJ2yNc8uV4j+li247hj2UFk55McTebVPioSVMHrVKLAK9A1dwpx+AqmQiVT8PfiPhATwrq8UbBEHECW3+0768V98iXWR4c1sAx1YtVtFL9Q/Zpf8WvBiTmJsnKjYjoDquXIqNxEl4zTmtjqYkKS2zzqcPaHKPXoEPcqhDKoU3XvbzPKX6Y447XJcq8V/CbCAAz1M4/LeR6AbLQg6+1PfIcQjuPXqh2fWjNRA0j33TRsyzJsk0yg7kRhKqmJrocixY3kjG9GYauEnmvRoeElmqRJKBbpkHsPaqIb5D0uy64kdg2gr8svexpQz/73dvvggvxxubtbD/sjRJb2njA7JX3TLTtBXG8FhxS0hUsDvjJREWoXszRJWDFNz4AWzTdbaAtERbidBvSYvNdlCS4WAEzA8KCOXh8CQPbYyaRUuCDShETPtsO+FWKy/L0AkEgZXeDw83ugOHdoWbBeBH1xMoMGkK/Kmd+k6JOWfiVc5GvjWmEmDSBCvjgrC9XLe12WYAkCAaahyD5KFFHAIJtG+0+njL8eCcTEND65lbNs9GvP/8nlNq2OrhYyXiBmBoC0/fWqhgRwgp+N769HExCkRQAgXUVNoaQqskre67IEFw0AgZqfQWJIpV4ASK79T2m7goKOBVZP9/lAxjw5US1IckZqYxvELB5AObL9SrkbaMJn5e/SMyj3PAFRLCAGJF62/jIRLCERHBnUUAMkUSLuFP+aKpRpIX9ETKPSs8lcYoJkBFUVHUnaumJNkmTwJ44zPUyZlxl0XrsLiOBrcdkppFigVMYiqiNXELzXQiKql49pjnQKNUBftzolwCKQyiks4IR9LRf2bJCJGcywUmI8dIdvJUFfuSgLthVz5uXsbkTZ6QVbP7Fhn3yt0TMNxT0BUR5qZ0G9TAh+WQss9pCkPzGIAHAcbarJ9WIAPipjABOFQjx7DzRtnEH66AWBhCtsWh5qRAJYNduidQt6AGTSxci531aos3kCCICgr5qUhRoRTZd5QCkOx0ESKJssXhCHRybvuH6thvu0msmsnniLGkbWtEnBA5ALRrQGCaDKoBgBlNu+mlHI0gMIIvqeSgx4p/xd/l3GB4puDZNEUK5BINkklz2B0hBBy2SynSq70AXEWc/RA4hyn8LMc6uA+WIDAdsdpZadMAhhhTksaG1sfTGJ5D0A5vn73t4MFOYjg93AVBXuqqjx3tCK4JCZuk6RlUEPSHgvWAARxMtKuiH3S7/l4wiEMrk3y+XptFCA0inrN4gNYSm0QhszTm0sL2+Wu25dwUHGClNjQz0DWStBiGfMBdFUP1QVJ4AiH1mEtGNDMBCBZ3Dm702NQaAsAq8aadgZiEFWuJ7gxTmGMD0RBCSbSAR9Xir/PPZhM4WoqopfOpcK9fb0eCB8KzJJRgkYyYQW1Jnfr9GAMu1oa0rAOtr1rJqE5//uUw98s13JbvIWb/BPn3i24+HH07lswKcFhAwAhQOSALaKIptFpWrPWBY8+Ppr8MmGVng6MYQznYOD6uCLowPQYRnw0EgPyN5CRXcHyf1+ZaFWFvZX2KksgnD22oGFfgHKnfW+r5WpgUY2ejz8pX/sIG9VR0Km3iGx+9OPro79gefszTTdGGMi0T+S+Lu//9Nv/ZayZ5OJykIoitg3fPU+ghNQjiuSMYdWhFeIgFZOimgAeQMFATRgmfCHJ46Ahij0owKRBaGeyY7BU+mEtyagzJITILUI3qsCSWcTjGYGx4norP4u4mDI5IcsVww4QmZUv7VJJPWGmkjyJRSYcYOonF9cbSVd261cFiw51qhyf6P82J98A1b/1VAvGVCQAKL/v0YoTGZ3Fifxbn52qxP5hgBvO5g614RHyR/wXkg615CO3t0IU98cpC/bEYgX+sxPi8tBpCmHUsTr4oGACLSx1GZXLvsXVn7fnu240ylnzj8k1/6eMmqDjADOZVT5ND3xFlIK2PtEbdmG+daTUQgELot7OpMwQwIPWnQ3QqzqsGtU8gmDnQINNvC+m2vCZgOXlRHjZZtni8t4Wy/5BRxhfL/+PB9YUvlgNLRVVZSQx+ovH4s5xMwAEMQNKXYsKozwRCGphMMpEds/klL6VI36WNo466UFaMrMeX2iBLu3C/eQewT8I5leTfaYvBwRLokJ4NPxAGQIaoIGzh4INLywKtu3PegYPtn+3U9sSAlt2LCpSW1TzX3jxY+qbbU72JbW9/O2mpu5ruogS8eJaZX3ZGLI+ZxESKTdd7lJjp39KX/15NfPnhp4hRMh81EuF41epAYg9b/x8RVAqT6dJkDV7zoBzdhRk7jtGqX7ziTRMymu9R8zyvYe7Aq8pALX830B3BwKQ3jbtDe1vMe9svkDglH/xGmfG00Cl5VCCx1EpfCr/H5IRoIwKdpYKANDzj0D/q9zcB996sAn7d7EcTQBctuXf9nUY30Tx4NQPrYCoYgDMklwmvGkQjA1aQReyYV/+ktlw5OeNbeFUIWro/B9HmhkTFhTAuPbtK3e0b9QW6pvJpWhtkkL/xcccu9xjWnBKISATUwQcfJkUXIGB//uyjpCp/reIGcGD5CgXnlZ6qWa+0IWNuQKlFe7MjUIZigpShhOOBcFjuf5GTqNTofCNm3GFNmTb2aXPG/PHZfnjDFzP60pv2qcyslJ7do2mOl8vSAv2IdQQzfRdL0t4JeP0gEABz0Y4hR/seegLCYu3s3BWYdp3ny+OpP84aD6OZvIvjY1mnv+TeNannGRuSyxJTEBDoV4owWlbLogplEOYvIr3qKOANe1bDGDjphsijjPXZZXiQ+517OiGjlAMGQX0sFKYl5luJbJKBLOXl5oIesVZKJ5S0/luj7eSeUi7XfdlAliVgSQWcq7LtACTioYR7ynuvRdC/PRSjI+T0g+DxvoktzJH0QAVMdtWQAaHFudmhe4IG/dBoVlJHv3Z7Og5izQbAsymRxYCAjDp0MuIDeCEMNnOGctMZ2DOCUoUZLYPQdiuUKYCEVNJUL2ZyfjaUkOUFNWE0JvRrZv9l1MMHBZhUGAIX/D5wpTWQWRgg95sIHj5DiCpuXQMrmEko/dlAYBngaQXkAg6MDYiAqLq71AOFCUJ7fb9xxI12TGIDCSBAWZPJX+vtQEDAGg6zBWEYXOQGTMzIiThFLBXV7Up/M6kS3qmxKOgs/GiLm2TnNur1D4dj+VLQvEeO1RMAUZTjjk1V5HeXrI1XYDoQoKYsl7ErkIOh/Y1XWqjc/lbg8zWI0sWq6T6aimTAcnStIh+/sd+lyfoz6DszXLPPAu4rk8P1zhUFZhKagGAAFgQWIo4G0YWKDw8UEzQWLVXekz/7L2yNGtcIEJkKaBoosXtDMQSaagzuWrhqLlf3xoLC0ysTBh430HplMACy2YX5hdRLhwhZb7z20+/lsagQCH8ez18zcLEmiuVMSWNmF99KxlP3bA0P/CINogaoPA0oCASI86VUdzN24K2P8jyKBeQD5Gcs6JKlS4izGxukl33jvqOIcO5tzP9nP95yqByGLiP6j1bQhHkATKTiBVdSYsPM9SNgTPRsFsvTqQ+1K9BrtcFW2KgjiWuf6FOn2icEpQ2PJvuhapzoxeu+27P6bhwRFwixQ0chYIACl8yh2x1Zf9H+v84g8pkIAtiJe7ymHyKV+zvdcJNOlwzzVB46tBYdaiCs4uhTlwBE+vVLMf2xGyv+RnpF4+lwybXPhs8t+OdxKIKWT9jqD1SAPN3YqvLdwz4i5erMKEULRgT+oajYXafxwwM0jM+I6g8aWoQtfNdQuvfJvDFAgPJeBtj/8UAmNpNBHM8//JFMIqxELUPhUu2eLPfh4Feu9UrjHzc8n34mCvuypoPhoAKy65QymnPgIt2arkPnhlwP0sflvdFXMFjeyCTsLbQs4/xWlup+QsC7M7LkBtvSG7v1DvHxXVJqoEZ+YAzsyMFQdZ3eo3Hw4z2uosAEKOpkJ4OAFrX3i5ED4g08Qs5n9htPmZdjX3YLMO77YX8lz4mTKVrEOT9jdIQkqWhIoaJVdFzW3r/e6fSW0zX9XGve6ZRN8adL4QIWb7wsCJE6q6zvBcbA8AkRj3VAJ3yfwGmeTiirWrSoUtziKspARBzYlOiPUPgutFJC/cpjA/DSCXsgPEibfo/AFnEdrblimIqripitpXS8GVKALntmnOb2uUBPkCx0xqDB8lkRbN/g+oHK15yp6AL+Cg92dK2VMvJOhHnlPXlIF5Al2u8lcp/KbFmkjJDZjteObA24xKpvHa56eZ7BB125DpVy9mGTqfzg5QqfDrSpFGJs2SRngkqvD17qI1iXwusVMBHp5XplPe/uegvEo2PygEGCQBXNGeRS+ALwCN6ZJQZJIHwkzBpQUMdslaj8tOhVCSZnD5VGb5bCUYLjn2GbyWOy+SKjV+U2vW6wV1LgVfvlgdt9AltBAMZO4PQeiIS3cv1kuSonJUFdIVMS9mMPXv8x4cJcPpGVNAYjFiIwUmPubSQ7IsYgmERpHtp1IueZ2WAAAJl+xB05Qh8wEApS40rMiOa9WCBkAAhGMCzUAazcCcL4aDoiVc5ZDJIbeY6SEzijOxCKTKpwLAix/k1e/cnwuEkhOsf8whBxb1XPjhnAtDo5wdwYEqyWokfht30KEvLHbSSF0y7CqvnOtnMzf1TyFSZkBtgzVu7ukEZAC0rUl7nZ3nPtBqltPOUYe8uphOadTlMNxYB7ZPn6K4CynlmfkmgMio5IDNnlsMAOTgjDjkFQRTHyrukpSoxXHShx3lZfRMbLLwa8gY8ciIw/bK682dbaPQG1vSEAzDeNznPABk69aWVQZEy415eAPEpkpuCJTddDGIll+oKlSoHkimMDHKRU7Mk2nK9aZBpr3iLiDxdCL5HCTqbk5YyfYRIMCVMaadynA4u9BJIz82KuihDFV7aT61f24fY4oL6zalJtZxohPirQChCED72iTYcwOnw6nZUm2uvzF68j2mQxaqEmVzebj/8DehIindQGXKl0UAmPMxAVKHsZBCdzX23K86ORALXEyzUTPtrOh6d0U1qUGaVJI9hThhjBuahu5rpIN19gKVisUprFPPXnlNQ+JdpmBzCwY5KNOa+gzEGx2Y0KBj8shIEKzZkAJNn1NQyAZqrFET125hPc34WAuKmMpgSIiloM3og4r0MLgXbhLJN8+dFweQzewrVLPxVnHiN/wkDe4CnssDJrrY15HTm1exkWttwowSEADgCuVbtMEbGuig5iwwECCr87Qqg9HtSu9dnNG5+e6yw8SaDWNe72chZgCAJAY1DS6iJOUhZladIlgCfIM+jUOYpIAvYKbJL1OtpuS2MzCpMj3jn2dlMLlkmhNK2lS0RLU6tqBlDgnMKE15SyVJNNtElGhNXsiVR2ZW6Tlg88N14ctRUEUOKrQcjJBgL8zFwknPLhg2oH1dFrX7FJ4zebbJSNym7aMwh+ACE0I9a4XecFXNqWBpcOcLaFmoEAehWknBmB6G0UA5XpNPjg0IrwGCO5+dwPilaMpSRnpp9HgNggsWEMORk7MKnyur+kd6Tf8pBfjiaxQSrwK7OO3EDpdpFrK3+Zsnjoo5SHMQ0lw45UQPynDs7OrfIrD2ylGIlgnP45sRAJ5eR1XettZELSC7JRZ9Oka4MpTVzg7S0OlKJZ03vvOMSurEhDLUrt2ROIwFoggId8q75HIwmd8kIZYhrE47fKxcM0FD8zTvCYwDVaWmYRCCnWM5ZZjSEnkBrlBO5UKHmUYgQrMLWguIURSNqrmn8TqK4NoshIiAL2jBldtTMM32PDod8/W6h2zclpht8x6aa5rJkdQhUf1CnZbMq7R5eGue/Yc0xEQWDtesRgKoFrYFkEmPs5DVQOa46l6r9sc+P3j3mJ8vQEBFYlqrZWCvqPuRZYBBStQTXiWu3pGN7BvUo521aEBdPl8AyLJ6CTirlh3pyfhOKmQWzSQJ/er1CSir5NOF+lnkw380nc1A/Rd3oPOkDsmEr1iiCHUEO0XK9gejJKqk3Q0WCRbSGuYSRqbQyHqhd2ULPL7xbiRImszdAccwz7sA+D96tOfrkDaHgJI5d3NmRKhDpt6TLIv2VBpj16V5VKVzDClyYBAhY3C8vOqLTw40/RPPOlT2rCiJGyhrbZtgdYRrX6qjyU25nL8G5twOhoDmopdeJl7+trHujxLDZITRIm6gtP2+gA233dsPmk9Mp6HpTP6v1zlk6zUjXgeiYgPNhDrSD8M/TLf8L81nZeezcKc4JgRCDjxy7UcgGaxAd8+ZxmiKBe0i9pQICu7p7tovmwH1qDqf+pKoZ30+M/t0ouHh1DBPMwol7VOEals/dMq395hS9XgIzcBczZN8X5Bl4CXS8M/HuvT9KuHFg0C2SdDvH4HyKj7TQt/Md/a4wBoDmtvH8PeiTb10hfuHRtXu7mDkO7przInZChzTIEk5B2vXfSelRngxAYkF5ixThpjKusphUvmonyS9e84FOTrPQKce/M5QUuvTGPdBqQ9UkX7Xiu3PVf2b43cHyBzdQYpCTIeUIx2p2PMBYceK5rjIYF6kLAebrxrzIoBFop0zj7oMylx7yzCOZPG4gJyoGUc/aJb/M2jpVH49qUipF8JQDxqQDdPHnk82P0THZ/h4nh6ZEj1buM3lbuDQWMUTI2H2HEXdK4qFzmXaGo6b48v1vmrH/4HkHH2pcoPR2VJGhtnAG/7YQwpPeWanuFlS8L8kvK7EHkqPiKwEd/HZjxP4bdcNQVmVKLbMP/tF6pocuGLzkKdOiqk1KrTeQa3jTCz2lxXQDQ4yTkdWkeH5BAYvt43nc9uImYKaUN/AflHzV6NDBitsAZ1k9yf6zYvZWy49AnOMi/0k/pmy0MAotbJTnsv1Wg2j4G0HqkkXnIpW/OXgoNKN9nVJy9MrjhM6OFb+VVFuPBe0BsH2qu6Q888l8s8lE1Nidh9YlfDYsZHo9xXuhmaJ+lGobUzB+i0ZlFvxZ5iLO4RaYAw6T4QhNRZErcBnDL7bTmjPcPVXt9T177hl7LV70+DzCCEt1P4aZ+Jh1bS/79/8JyeOqd3+KreW5FN+yAzactGr5yrlvq4+7fgb7fFP/TvtV/8blZU2fm0oPJc8g8SAg9GmbxzoKntcda0QLPHmcwlOO+HSl8Itf/y7dS//m5rubnCQ55LCE40/l4beVTIaPPR/k1s+y1OOrrBZfGtCHXj77YOgB2A2AMz+FaXvGI4K2HHDAKoSXky3S83ujFrixUzzL6oCGWhgfVCvDkFcH4U6PYG/D0IL64EBvez5n3ZWPKkIN6RwkZXndAamsIXMZGJ+awHTgdPn2uHnOqu+e8pf+dM2dhZdqUGo0xL4XKPeMzbjcyECur81tPJzIm3L6rUXZWOIyrh2uls9+aLT/MUVWj/UKXhqw/kxw5/1Sh/EfSPie9aaz/X10X6FieJ+v2UQT2M3t9mzCX9uGsC7KF5o/VYTzp4egkOvVnsuxUxjzV2WtEhCb7sSGtqvhVjDeiBKPvvEMdKQPPlL2HfoRDKbMawQhYDiuCk805amhqbjNKgdDHmKxabkEEHMjGG7wXguvnU7hFuvBsUXztt9x4ZU9wFIduy3s12WgZxEBbg4tSck43FNE9KuP1Gx6Q5oxOcKVjTJYo6e9jXHBqC/40VI7UW/1DIZ+Irs3ZCqv6YhBdffOlqM+M0fAIV6PrDzthE42xmAsUTI21kyJeiEVCYcZL/57+9fee2O1ejeTjWh9RvfAbGbxjbt/9Yz1zz51DP7ddMxBJ4QgvO74iaMjkwGobLT3CITDw2XG/e86/Zdv3PvNTdUyvXwC47atW+HFTe7Da+v3v+hv3/k249Sx9IJXXoQWLZtr96wrvmPf+vd92+YoTto0/b7yGd3dP3e7z30rU9me7s5UxQ6reqnzIYb7+hH3zo/aecWNp/rqg3KIIADt+vufpSsPZ1XYJqWtenK9as/fvX6P5lO+ONHPBJt/dz9t/1NMBJlpmW7tuPy84G2yV4A4eAQmRG0iEicbDgZKi/X/+Y9u/4KhV8xM1dg6qdv2vLn669Y3W7KnrFLfeCkynHI/e5dN//O9rqaq4u99fa2xnfdf/vb787abnZaRSIFfs1Nfei2O3MV/tw1wPnAAsCKlTbeqBeef7IRdP94xfc8ALjIrYqXe00mhs70w8DRThg5NQginQOBfo8/HoGq9jqIr2uCikggEgz5aSKT4nRi2VZyvox8YZDc/GrgwhUA51wEQ0Ee9VG1pzcBLx44A4dO90PPUNq7U1VZANY3V8HVVzRC24paqCiLItH2olLakoNAoXZDRbDcybnQcXQUuk6nYLDf8KqiMh+FeNwHza0RWNEeg83x6GaXki9PuYZpUFi1fgC2XZ+Zj/DnD4BxPrDt+iwMDw7Cwb3VwufHkRImus9WMODz+08Nrnv2v30dTu/tACdtTJq4Hq9F7RWpi0HlNeuCdYKtTVDyKuqSYcOntGo5WcL43K7ogvxlAzvBF2YCvJx5kymM+Tm58ZN/+734T149Dr2jmfze6/GWB4WsofKQDrs2tkKGZN+u+/UjhIikbPiJ79SghNvDuJBVE11vN1ZNsKz50I8H4r863QOdvWmvfC6ZtBaCD6BSWNkYhlzcbigLhSotOzOKlIYxwnzEtlWorhtDzTy8EJeZ1L8wuIAoBuLGzAE89rVq2ttdFddD7Vfa6jvWmcqtZWmrzbFdYLrqlXSbdgDw7wJPEtLNbpXv7tpa39a5prqhszECiTIfOIMJBJqTT6BLm6fVf9vzTsuwLbzeHEyWkDPXoFS+W2sRXL/RdbQ7TAO2pHOWqqsKKDM0Y5a5CQbe168x4fezw4pqPU2Z+RMurGOoRWQpA58shLMwbY9SR6FLNz+oBKprtZqrGpWGO2qhcic3SExu0lOVmSPztsO93Cnhd/p63f6fdTpnfzyQ6zvgaOoouecD3RBv5POd/QsHgHwoBQVspskHvv2vj27qGPmgBIVsCzOfzDDp33vtSpx8ubjhygD8YmcjvNwWhKRGwPSrEBjOnvL9v1fe6aJRhpkBgBpCyBU7F7lHHTp916HQ77QtdpXjiJA3w8m8zbOnvZhCHFXlrzLF+D6h1nNIR84UditLMCjFw91eRz0LbZjpU/RItVb9tial/rY4rbk5KAJ18iqu1yhn7rOWyl6fMmqIH05ro796dlfzTWPxplG6AOEvCgCe16Fq0DY4sPZDX/vuj4NDySZbW1zgTHHyJmAsqsNwVIXX1pXDK63hE+kf7rtBN2yVEjJxv760EzYKNkcIK0NzvZW7+u2Ore6ybVI5nty82O0c4xVvpFpGGpNSNeclxqwfIvpfQrj34m3wSxMfXFAFxxZ2RmGKXq6UrW1Q63Y1KvXviIroWiKjffgfX0SRUyTGsmCnve/G+vcebqt6jFoLz1ldFAA890rToLW/b/2DX3vsJ6HhZHyxIPAsDKpihkBQ8OyKBzIHqfno85D+Qo67WXZu1knPQCknIvgx21JvcRzSml+3FLBUFQTHawrI6yuKGFI19zlCM48IYR3C1/0FniMJhb0y0Hp/C2u+twzKtqhCYV6/hRLklUrhK8gi9twQf//hVVXf1MzFXXPRAPBAoGvQ0tu/5cGvf+9HkeFkjaWVLoQuQSBLy++Oiie+oqX+k8KFtwSKNt51nNiXjQy7TtqSi1020gODrLOgw6DuH72bu3af5Ae2cNIbQms+sk3Z+jnHSyV2S9bkrCB8eGVn3UeOrKl8ZLHCn18coMjhMy04Fa/Z9y+/+e53pCojZzSrdOX4HUYgTQVsTtO71gj97RaIHAo8C8J3m5lTrpP9Li9FzVCv7gWV/RehituBj+G/DaQGTlgN1a9laz5huTZyIqd0wnc9tW/tuaHug6USfskAMA6Ck7U1+7/4gXfvGo2XHdDN0sVRvDUB1ATrbWUXp8SRZWk519/BxXIoFi1XW9UbKVXKcLZna9WqHX7wx3gJC5lLXsQUPrbnxvi7D6+s/GqphF9SAJwDQVXN8S984N5dnasaXvTL1K4SyUgq0ipQmlRCZPORIHfoKrJMqoVzTuqEIHEZKotq4cZSPpZiI3cI0t4XdjXehoTvR5pV2p5XJQ92+ywLeqLlA//4vntv23fNxn/xSe+NL35EZODZb/J6xsHP0cW3XOFbDnWDpSlA50V1BPFWvFRHaynVtTUUfrpW/+Xzd6y4/mRj2W7VKn3DsyVZ7dBR6BlFy37p7tsffPKWq/6zrNKlOot7+HyRyXzajEIJVRkjy6VbgIx36YxRL57IlUUzYC8+gsLuavY9+tRtrbf2RIMndGtput0t2XKXggxYsRz4/s07P/+V9911x2gk0KUvwiTIj2lS8DLEB0Sj+fDssji8gBEhAbmy7OFgEdyEObKOiGMe2FbxiWd3tX04pyjpxU6eSwKAPJLRhUNesO+KNU/+w4ffu+Pw5lXf9DkWEjq+IADohGq6LF4OyAEAQssFAPLZkKeVIQiEnymqWOBFpH3PVSovP3drw859b2v4XxTtHlviNskXJevBhzN/pLK894vvvft937nv1o8YYV+/z7DmneyJDxtSZPNSR/gdhweXS8sIue/SsnhUdjBRKQvM1/VjtgANuHN8U9nfPPnOthu6V1S+IsFALoKXo1ysQVJtx6sB9LO3bXnk2Iqmp2977hef2fza6x+RSze2OrfHYA4PoEsUkI3/OIdl0zsiv3sJqCJ78QENz929k6bSgZE67ZlD2+o+dbIu9jKTjNK6eN1tL2rPHc8koDboj5V1ffneOx/84gfuuaW7sepl6Tkos9g56QWg8CnDc9pKYpcaBFTafqIQPjsAZERPznArKDr3Xl/90afuWHnLiXjsZSn4UnhMy1IDTNIGjuNlux5YufJnx5sbn9928OgHb/j5K/+lui+xxmYquMqMvSxxoAsFdCenjSwDd5BI9k6pQ7WZwOmFcnGGW0EYOnpl7OGja6sfGgv7h1R09zT70vS0Vi7ZgKHwdJz5DlGc57ZteeTAmrZvXbXvwANX7Tn0B5VDY+0Ocn1HUSbPdC/8SpFxU7moBmIZdY1jlJKZ2qrkBe+CHYDEiTWhR46sr/n74bJQl4KMX7MubTNz5VIPHBXcMwtpPZh9cuf1/+elLZse3XTs+Huu3fvab8W7h3YQHCTJEYRXp40QP9pZtLXU8YobLx8EoIsqg0GFWof5TVsyfC3rHZhhOHlsZeyRN1ZVfmU0GugaNwHL4VCWzQziLjDThazuSz//ti1f2rth7aMrT3fu2rHvwIfbTvW8I5jKlVGmyK0m0t92l1v3MOp5qQQUTgKqiwxece2RWu0Xp1ZWfrmrKfqdZCiQkjF9GdpdToeyzMbR83uZaYJNmNi/atVPDq5s/0ltYiS+8sixm6860fmgSJ8hYFoV3PZ7bWnkoF9KMMhC1rLVHTetWkVxqVXDXjsU11/obq14YqgqethCMyaDO+oymfFTzFMp8gGW+nAYQz7AwI8uE+k7q9Ceng30eM/nnL7BrWYqG5cOhOxD4G2mKJjipQCFKCRdCjnDJVBlCDioJ9SKsoNkdf0/2LU1j9H6Jke2xZFqXga8iFje7W1J9fPLHwATI25eQioCgnEb1GQqRgb61ou+wRtF39B2d3B4lciazY7l6OMl4sV4syrIh2wnBo/IxOtOuEm+WUOhlKqMxMlGWHLvo8o49avdrLzsBNRUvkrjVc9ATc2vnLLyPkdR8wBx7GUv9EkAuH7PCLxZDync5Kises7R9uKsTCU1PZ1ppsOj65yRxConlVkrhoZjqu2uHuNCy4JTg3INcFnYRHbHLuxtkRBhlEieKb1Tywe0P0oVA2nmGbuyos8XCnRCZfkRVhY9ZETCp3k4nHJVzYNIpMwHChHLyyWZx/H/BRgA4E/tO3/9oP8AAAAASUVORK5CYII=";
            // +8, +5, +11, +26 114
            doc.addImage(hold_meetings, 'PNG', 28, 181, 19, 19);
            doc.rect(20, 172, 80, 30);
            doc.setFontSize(15);
            doc.text(pdfDocVariables.meetings_title, 28, 178);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.meetings, 60, 194);

            var active_community = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADdgAAA3YBfdWCzAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAuQSURBVHja7d17VBTXHQdwPaY9SWuaNG1P2ticpua0p6f1FDQPNGJiYhpFAUHi6YlaWU2MJjFGhQV8svjgobKLiqhAxV1UzLLIQ9QAK+D7iShh8RWBVqNtT1KtSIKwM7f3zoIizAV27+zuzO7vj+9RmL3jnPl95s6d8e5MP4RQP69LxaaBXJl2M1+mvUVC/k5+5437wvuKj1B/XPQSHNQlJWQZAPD0mFNeESm+ELIMAHhSNk8fhDLDo1B6uBZlqkKE35UmT6MCwMuEz5DPCm1wW7IOAKDAZKiGo4zwOzioU3bhIqt6AKASPvNomzvCugCA0gCE13UppC0FyzZQAeBlom3IugCAgpL5/jOUQiKUG11KBYCXUduRdbZfQaCDumGectXgoed+1QvUQu6OqKMCwMuo7cg6y3Sx+HOt7Z9vJT8DAKUByFlwmwoAL6O2K4qLEG1TljwLACgKwHxEBzAf0drx++OPibYzJx8CAF4BILFZHIC2AQB4A4ADibR2jQAAAAAAAAAAAAAAAAAAAABIngrN6Md2TPX7CQDwMgBbP3zpR9tn+KfhNOPw21X+p7bNGDUMAHgJAFzwTFx49GhGfZPxgd+zAMDDAbQf/dbuAHBm+n8CADrHaByAzLrBwp8KBLAq0GeQZqLvWk3Q0CJNsM8KzeQ/PaOfPurPosUXMlJL2uE2U/DnczTBvnr89/HeCaBU+xkuQlN7MZrIz0oCsDLQ93e4gLdxUKd8vWXqq2NoALJm+Kfgz6R2aYPjs9S7AJh1U0ULWbz6E6UA0Ez0MXUvpC9KCns5hwYgM/y1HWJtcNpWBw951msA4J1fLlqUkuQjcgSgnz5iUJZqVMj2cP+X8K4XZiCTo12smAmhw76kAdjyt9cOUgAgTZDPO94DwKxtEC9K8tdyA6BX+Ufj4rV1KuRhfEn3C1y0O2KFjA8ddo8GIG3qiPN0AL4h3tQDNIr3AOv+IycA22a8/halmJ/3AADRAKROGf4PANAjgLW35QQAX8+nUop5zxEAG6f4NQGAngB8sbZJTgCyZowsoBUTF63JXgAb3vNDAKBHAGtaFATgewAgNYADSRza9j4Sze4FGEiSaMgyWjt+3ypauwYWAHH40g0ASN8DID5X7aJE3WACMHEoAgDSDwJdCEDdCAAAAAB4GOOA+3WGIehSznMAwMsAWC36AKvFcBMH2ZK9H13e9XMA4A0AanYOxkVvflj8B8kHAF4AgKs1LBApPgmPrhmfAgAeA6Bff5Qb9TFvjDrEG9UnUK56Mdr64Q/aag06CgDUWpvtg3arX+RMkQa8w2r43MhcZFo4FAA4D8C1xMlPNSS8O/qfa0JelBQAl6ve3G0nGNX5bRZ9Cg2A9WhKIC56U5d2rSgv4hUAID2AhoSwOQ2JYd/hIJLGhLC9DbqQp9kB5Cx8nrYjrNUZu6kA9sflU9odAADSAsj/+M0YXHS+o/gPkhC2nR1AbkQAFcDptEoaAK449iKl3b+F9V43PmGtzQ7j6gzzWy36VwGA4wCqlgYWdyu+LXeF0zcTAFNUCBXAiY01VAB7l7VQ2t0hVw/49HGlS5s0JgDC7eAk2/37oljE71mMeFOUVwC4EBtcRQGAyLjAeQCOrb/RAwDazrtjrcsuEW1Xpw9lAtB9kgjiC5Z6fg+wLPAKDYDYOEAyANwRXbNDACyGFtF2tfoMSQF0ZN9q3BtEeyyAU0sm3HQPgMNaZDcAU9Q96sDRYihwCoCOMQI5LXgggGMxAXeVBKDFLQAEBGvwv+95AA5Hj+OUAyAv2soIoN5hACTFK6UAUN/bdn4++429xtlvILFsmjIcpU7x65Yt04YjWhvbxFA/0RyNCUDKAbAnBjECOMMEgGTPElYAZ3rbzorIsUU4yBU5HxvkTQB0hcwAyFw/FgBGdVEfABS4CoBlxUTvAdD+0gfEHAYAnEm9FQC4CQAq1c6RBADDFQEyRc4CAO4CcFA7SBIAhbGOA8hZ+DwAcBcAMg4w684yAyC3jB0DUNuXbQQATgSAzMnzmAHsj3fs6DeqP/JcABUVj7VZdozBhVChmqw/yBaAUfND5vsBjgAwqRvIRBjPBHBZP4izZFd3ntbVZjHEyxKArRf4q6sB4MHftL5un+IAWC36QtFCHtWtlCUA4VVwyfmuAsDlqnfZc+pUGADjALzzvxMtyrn0L2QKQHiVCy7mBRcAuIRPOwPlCqCOGUB11tPUQlZnVMsWgHBZuO43fKn2uhMBXEN5MYPt3S5nATikHodOLhqPzi0PQpa4EHQ1fhK9+FIAsFZtvSprAEJPsOaXfOm6eskBGCNuoLz5v3Jkm5wB4OTiCeir3goueQ9wZvMt2QMgyYuZK1zblyVLA2DnXPLWsAWObo+UAMhR/6Um2L7CSwbgVFqTIgCYoubY1r8Y8QcSHAdAnheQ9YHtIRHpqjnuBnA0epz9R72kAE6mcooC0JH8JYgvXmWbF9gbAGOE7YgnD4bo/JQQNwOoxLm0MsTx4kvTA2yy3TcXS4UWcafTRcPvXyXeZm8ctQ13OiNPMgAPH+Zg6xUKMcgiDUaxwjYppHC5bfmuz6iPiGEBgIuXxwqgalkQW/FxLiUFP8kG4Gw6kuQ/X/qWAukB9JKd85wDQD1uK1vXH4DqE8JYAXzfty+G9ASgKhMAOHQKGKdhAeDwoO/RXAMA7uoBIsfOYgFweXUoM4D6pLCjAMBNAMwRAX90ePCHL/vq2Y9+Ei0AcBMA4TQQMbbBEQDHF42XoviIfFUcALgTQOTYjY6N/gOlAPBf8uIpAOBGAOaF43+PC2q1F0D18iCndf8AwIUA2geDejcAuH0jPvRnAEAGAMwL3h6METS7FsCkqJ62CQC4EABJuXrsdJcBSJhUebaXaWv2ATgHAKRIReQ7WS4AUH953Xu9PqPRvvkAtu/tI656G+LOpCPuWBriy9cDADtTOHPkk8WfjnEmgFtfrQ4d0pdtsR9ANxC4ZzieBgDsSJZq9OPkG75Fc99yBoCqq0mTft3XbWEH8GC62N8RX5ECAOwAQGKcPRqZF77DDKA+cRK5Y5h5XTv5CXu2RTIAD04PlRsAgB0ASAwzbb1BeYRjAI7HjEdp04bXOLIt0gIgPcH5bYg36wCAHQA6op85Cpk+ehOVzP8LKu8FwPnYYIxmDHmBVMfTQU5KA8BiHMgCwDZzaIsUAEzeBuARDDg7Z72OCnHPUIZPEST75r2Nj/QRaM27L4s9HkYiAMIXQwzNbAjwqeAQ26mAK9OleTMA6kumKM8UkhrAVeZe4GwGEwBk1i0DAO4DUMkKwHohiw1AabIKALgJQJvFsIoZAA7LYBCVrP0tAHATgFaLwU8KANzhVEcB1DHtZADABgCnPy7gLWYAJzY7NgA06xIBgHsB9OMshuVuuhxsIc/+AQBuBoAuGH7M2gs4AoAzJ6cy72QAIAEAWy8wx8UAvkWlSc8BAJkAaB8LFLgIAI/KdBMk2ckAQDIAwq1h3BPUOBsAV6pNkGoHAwApAZBczHpB5LUu0gHYuwKhLM3jAECuAEhqdv4UF7VMcgDkW7rkqVsAQOYAhBgHCHcJa/UcM4CSpEee0QsAFAHAFr58/TkyJ5BMB7MbAHldS1Fct7d4AQAlASjTnhSKeTBFmBhKZguT2UC9AiCFpz14EQAoEMAj0dlAVG5A3JFUnE2Ir+gyYzh/KQDwXAB9CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIC+AeABAAAAAAAAAHgrAA4AAAAA4LUAggGAtwOwAgAAoFQAS3gA4N0A7gMAVgC+bUoG0CxzADwAcC6A/8kbwKctAMCZAPYs/lbmAO4CAOcC+JfMAXyjAACtigXA7Vl0Q+YAbgIAZ/YAeYsaZQ1gx9xGAOBcAFdlDSB77hUFALivYAAxF2UOoBYAOBdAjcwBVAMAZwIwRZ+TOYBTCgDQIiWA/wPnX3R8b+9OXAAAAABJRU5ErkJggg==";
            doc.addImage(active_community, 'PNG', 114, 179, 19, 19);
            doc.rect(110, 172, 80, 30);
            // +8, +5, +11, +26
            doc.setFontSize(15);
            doc.text(pdfDocVariables.community_title, 113, 177);
            doc.setFontSize(26);
            doc.text(pdfDocVariables.community_participation, 150, 194);
            // doc.save('a4.pdf');


            doc.save(pdfDocVariables.pdf_export_name)
        }

        /*This is for the circuit level*/
        this.circuitDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            // doc.text("Circuit", 35, 32);
            // doc.text(pdfDocVariables.entity_name, 50, 32);
            doc.text("Circuit", 35, 36);
            doc.text(pdfDocVariables.entity_name, 50, 36);
            doc.setFontType("normal");
            doc.text("District", 35, 40);
            doc.text(pdfDocVariables.entity.districtName, 50, 40);
            doc.text("Region", 35, 44);
            doc.text(pdfDocVariables.entity.regionName, 50, 44);

            // doc.text("Circuit Supervisor", 125, 32);
            // doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 32);
            doc.text("Circuit Supervisor", 125, 36);
            doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 36);
            doc.text("Phone Number", 125, 40);
            doc.text(pdfDocVariables.entity.circuitSupervisor.phone_number, 150, 40);
            doc.text("Email Address", 125, 44);
            doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        };


        /*This is for the district level*/
        this.districtDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            // doc.text("District", 35, 32);
            // doc.text(pdfDocVariables.entity_name, 50, 32);
            doc.text("District", 35, 36);
            doc.text(pdfDocVariables.entity_name, 50, 36);
            doc.setFontType("normal");
            doc.text("Region", 35, 40);
            doc.text(pdfDocVariables.entity.regionName, 50, 40);
            doc.text("Country", 35, 44);
            doc.text("Ghana", 50, 44);

            // doc.text("Circuit Supervisor", 125, 32);
            // doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 32);
            doc.text("Phone Number", 125, 36);
            doc.text(pdfDocVariables.entity.phone_number, 150, 36);
            doc.text("Email Address", 125, 40);
            doc.text(pdfDocVariables.entity.email, 150, 40);
            // doc.text("Email Address", 125, 44);
            // doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        };

        /*This is for the region level*/
        this.regionDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            // doc.text("District", 35, 32);
            // doc.text(pdfDocVariables.entity_name, 50, 32);
            // doc.text("District", 35, 36);
            // doc.text(pdfDocVariables.entity_name, 50, 36);
            doc.text("Region", 35, 40);
            doc.setFontType("normal");
            doc.text(pdfDocVariables.entity.name, 50, 40);
            doc.text("Country", 35, 44);
            doc.text("Ghana", 50, 44);

            // doc.text("Circuit Supervisor", 125, 32);
            // doc.text(pdfDocVariables.entity.circuitSupervisor.fullName, 150, 32);
            // doc.text("Phone Number", 125, 36);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 36);
            doc.text("Phone Number", 125, 40);
            doc.text(pdfDocVariables.entity.phone_number, 150, 40);
            doc.text("Email Address", 125, 44);
            doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        };


        /*This is for the region level*/
        this.countryDownloadPDF = function (pdfDocVariables) {
            var doc = new jsPDF();

            doc.setFontSize(8);
            doc.setFontType("bold");
            doc.text("Country", 35, 32);
            doc.text("Ghana", 50, 32);
            // doc.setFontType("normal");
            // doc.text("District", 35, 36);
            // doc.text(pdfDocVariables.entity_name, 50, 36);
            // doc.text("Region", 35, 40);
            // doc.text(pdfDocVariables.entity.name, 50, 40);
            // doc.text("Country", 35, 44);
            // doc.text("Ghana", 50, 44);

            // doc.text("", 125, 32);
            doc.text("West Africa", 150, 32);
            // doc.text("Phone Number", 125, 36);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 36);
            // doc.text("Phone Number", 125, 40);
            // doc.text(pdfDocVariables.entity.phone_number, 150, 40);
            // doc.text("Email Address", 125, 44);
            // doc.text(pdfDocVariables.entity.email, 150, 44);
            doc.rect(15, 5, 180, 42);

            commonModelPDF.call(this, doc, pdfDocVariables);

        }
    }]);

/**
 * Created by Kaygee on 09/08/2014.
 */

//This file is used to store all values that will remain permanent
// but may be used in lots of places

schoolMonitor.value('app_name_short', 'mSRC');

schoolMonitor.value('app_name_long', 'Mobile School Report ard');

schoolMonitor.value('company', 'TechMerge');

schoolMonitor.value('foursquare_client_id', 'RTY1P13DUWC5PSX0BS0UBQV0TMBGG1DZZILTEDKYN4HP5REX');

schoolMonitor.value('foursquare_client_secret', '2NSVFVBCKUSQXMLERLMHKMZOFFONOGEXI1K2VGRTVYSHMN4H');

var d = new Date();

schoolMonitor.value('year', d.getYear());

schoolMonitor.value('school_years', ["2014/2015", "2015/2016", "2016/2017", "2017/2018", "2019/20202"]);

schoolMonitor.value('format_school_term', {
    first_term : {number : '1st', label : '1st Term', value : 'first_term'},
    second_term: {number : '2nd', label : '2nd Term', value : 'second_term'},
    third_term : {number : '3rd', label : '3rd Term', value : 'third_term'}
});
schoolMonitor.value('formatSchoolGrantType', {
    school_grant : 'School Grant',
    capitation : 'Capitation'
});

schoolMonitor.value('formatAvailability', {
    available : 'Available',
    'available functional' : 'Available, functional',
    'available_functional' : 'Available, functional',
    'available not functional' : 'Available, not functional',
    'available_not_functional' : 'Available, not functional',
    'adequate' : 'Adequate',
    'inadequate' : 'Inadequate',
    "not adequate" : 'Inadequate',
    "not_adequate" : 'Inadequate',
    'not available' : 'Not available'
});

schoolMonitor.value('formatGoodPoorFair' , {
    good : 'Good',
    fair : 'Fair',
    poor : 'Poor'
});

schoolMonitor.value( 'formatAdequecy', {
    adequate : 'Adequate',
    "not adequate" : 'Inadequate'
});

schoolMonitor.value('coat_of_arms','data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAeAAD/4QPvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0idXVpZDo2NUU2MzkwNjg2Q0YxMURCQTZFMkQ4ODdDRUFDQjQwNyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2QjYxNDFCOTQzMUIxMUU0OTQwREUxNTFBOUM2MUJBRCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo2QjYxNDFCODQzMUIxMUU0OTQwREUxNTFBOUM2MUJBRCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBJbGx1c3RyYXRvciBDUzUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyQ0FCMEY1MEE0NkRFMDExOTVBM0QzNTg0MjNGQUI2QSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyQ0FCMEY1MEE0NkRFMDExOTVBM0QzNTg0MjNGQUI2QSIvPiA8ZGM6dGl0bGU+IDxyZGY6QWx0PiA8cmRmOmxpIHhtbDpsYW5nPSJ4LWRlZmF1bHQiPmNvYXQgb2YgYXJtczwvcmRmOmxpPiA8L3JkZjpBbHQ+IDwvZGM6dGl0bGU+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+0ASFBob3Rvc2hvcCAzLjAAOEJJTQQEAAAAAAAPHAFaAAMbJUccAgAAAgACADhCSU0EJQAAAAAAEPzhH4nIt8l4LzRiNAdYd+v/7gAOQWRvYmUAZMAAAAAB/9sAhAAQCwsLDAsQDAwQFw8NDxcbFBAQFBsfFxcXFxcfHhcaGhoaFx4eIyUnJSMeLy8zMy8vQEBAQEBAQEBAQEBAQEBAAREPDxETERUSEhUUERQRFBoUFhYUGiYaGhwaGiYwIx4eHh4jMCsuJycnLis1NTAwNTVAQD9AQEBAQEBAQEBAQED/wAARCADwAR8DASIAAhEBAxEB/8QAuQAAAgMBAQEAAAAAAAAAAAAAAAUDBAYCAQcBAAIDAQEAAAAAAAAAAAAAAAADAgQFAQYQAAIBAwIDBAYGBQcKBgMAAAECAwARBCESMRMFQVFhInGBoTIUBpGxQlJiI8FyojMV8NGCklM1B+HxssJDY3OTJDTSw1RVFhfThEURAAIBAgQDBQUGBAYDAQAAAAECABEDITESBEFRYXGBIkITkaGx0TLBUnIzFAXw4WIj8YKSQ3MVomMkRP/aAAwDAQACEQMRAD8A+gUUUUQhRRRRCFFFFEIUUUUQhRRXlwRccKIT2uJZo4U3ysFW4UX7SxsAPEmqHTM6XMycwsfyUZRAPwC4J/pEXqqJP4j1qJib42Jd417C1iA5+kEekUs3VotDXW2kfbJaTj0FY0zM2HDj3yHUglV7TbifADtNVelS52W0mbktsibyY+OBYKAfM7dpYnT1Utnd+o5uxT5WZfoOsQ9SjmEUw6hnLhxDFxr7lATy8RceVFv9o+wamkruKl7hNLVvwj+ppIpkoxZseyT53VcTBsrlpJm0SCIbnYngLfz1PjPkSRB8iIQO2vKDbyo7NxAAv32+k0u6P06x+Ons0z35Z4gA8WF+/v4nieNN6fbZmXUw01yHTrIMADQYwoooqc5CiiiiEiyHnjiL48YmddeWW2Fh27WIIv3X+kVXweq4mb5ULRzA2eCUbXVhxFv5qu0o6z04N/10HlmT94fvKO027vp7RqKhcZlXUo1UzHTpOqATQ4SXqsmdimPMxTvjQ7cjHIuGU8GXtDA/y0q1h5sOZEJI9DYEr2i/A+g1S6dnDLjbFytWYFPNxOnmRrfaA7e0a1QiZ+nZpUnyqza+HGRf6S/mDxpDbihS4DW1c8Lf0mTCZqfqXHtmhimjmXfGwYAlTbsZTYg+INd0jaT+HdakddMfM2vKvZutt3j1KT6vRVrqmdLhz4jKfyWZhOO9DtW/9EtenC6viqaaG0t9kjpOHUVjKivLgC54d9e0yRhRRRRCFFFFEIUUUUQhRRRRCFFFFEIUUVSxcxsvNyVj/wC3xbRbvvynzP8A1RYfTRWEu0Vw80cbIjsA8h2xr2sQLmw9AruiEKKKKISHLkaLGlkX3lU7f1joPbSzps5XoRQEl8eHapPG2y6H9HqqDqOblx5OZCi85CoaOEmxJQIW2Hv4adtVMbNijwJpQ14djIx/A/mic+jd6te6qVzdUugA+HxW26Nwjlt+GpzwbulnCcwxTqh2mQSopHfzdi/RuqGGdYI3mNxzxMgIB8oJUX04WSOoshnjxpJEJDY2RzHA7U38xv2Wv6q7YbcFo20CvsbxTmDX0bTWeLzAW6eRmA/zcfeI7SCW6ge6WcAnHjbIIHN23VTw5s3D+oq/RXODj/HZIdrtGbkE8eWT5nPjKf2arOJ5JpIpDtSVyVW/uwqqq7nxa230VoOmwcuDmEWeazW4bV+wvqHtvVjbL6rInktDW3Vj/FO6QuHSCeLYDslsC2g4V7RRWrK0Kin/AHRUGxfyAjiN2l/VxqWlfXuojp2Bk5lxuxYmdL/2r3ji9t6IS/ASYgp1ZPIx8V0v66lpZ0TPXPwsfMB/7uFZWt/aKBHKPUbUzohCvK9oohM5n43wWQXS6xgAkjjywdGH4oj+zXWcxyI1nUDnW8wHDnQ+Yf11b6Ka9Sg5sHMUXeHzAd4t5l9Y9tqz6CaOWOKE7o4nUlfvQuGVGHil7eisrcr6TOnkujWvRh/FO+WbZ1AHiuB7J3LkDIiWWxIxxCt2HvAEm477xya+ups5+ZBCreYxLGpJ7fzdnt2VAAThCNNQzmND+HmHX0bRUePufGEjccvI3oP93v3L+wl6rm8xFyvnKqf8tMfcZPSPDThU+2Nuozk9CEZPmni2Me3bt859Y09dMcKVpcSJ31cqA/6w0b2is3k5sU2DHIWtCEEak/cU7pXHqX2Dvq107Oy5MjEjkXkpZnkhB1HM3bN579eHZatC3uq3aE+Hw2x1bjEtb8NRni3dNDRRRV2JhRRXCSxu7orAvGQHXtUkXF/SKITuiqWXmNiZmMJP+3ySYSfuS+9H6m1H0eNXaKwhRRRRCFVepZfwOBPlAbmiQlFP2m+yPprnq882P06eeA2ljAZb8Lhhx8KpdZmTJ6VHKt+XIylh3aNcH0Gl3LmlWIzVdVJJVqR1NJfychoMFpifOEFj+JtB7TVPofLxeltLJ5RcySE6m5VSa8zJTJ0dbm59x/Fow1/2lqlNKf4IY1PvOA3o5av9dqQ98C6DmosFx2kyYTw9ddJc6PzMzKm6pP7zARwJ2Rxmz7R6RtJNOKp9LULiC2g3P7GKj2CrlPskm2pObKGPfjIN9RA4YQpd1HqEuFMmimFluSwOhvYksDoNR2UxrNZebmymOORUaRToGG0ScVliDbrX04EdlL3V307eB0s30ns4TttdRyqOM8zJxNltsQx5Gk0KtazsBtkRWB10HtvVQKiZC8tS2LmgjYB9/wDeLbssTu/rV6Vx5okiu3w7OFhYaTY044L36ezgfLU+HCxCTs92JkLACy+YgHaOzVL/AE1kO9Sztm2Y6/4y0BkBwkeMz87kyAvJtMWTpp5P3chP41/lpVmWEtCuOmkZsrkm5EY4jxJ4VJHGE3W1LsXPpNdAgi4Nx3jWklsaj+DJgSPGiM/UXhce9sPhyVuT9LXB9IrQ0u6Vj2aXLYay2jT9SO/1sW9lXZkmYAxPsYdmlm9ZBtW3srWiyCc3ox+yU7rVY9MJLRS/Mzfg8Z8hviJeVYyRRJG8iqftbbC49FZ//wCxOg/2mX/y46tRc2FYL/EbqW3Bx8JD5suQzuP93H5I/Ux1q4f8Q+gEW5mXr/u46w/zR1iPrPVnyoAy4yIkWOraEIg7R6SaITXf4c9R39Omw3Pmw5RIo/3U3lb1Kda3dfGPlXrUXRuqjIyAxxZEeKdU1YqwuLC4+0BW2/8AsToH9pl/8uOiE2NFY/8A+xOg/wBpl/8AKjrQYmYcvGjyF+IiMuscMqIshH3itjYemiEv1nsqI4/UI4kGq7yO7ksL+xrAeinsKSqCZX3k8BYWXwuAL1T6rjgmLLHvRXR/1JLfUwX1XqtvLWuyx4oCw9mMZaajDrhFkUTLE8D/ALvVUIOpRu/xF7VWyWcS8mJdjhRFii3lu488gPDyLV8kAXJsO86VzJGH2g6FGDj0rWGGxqZcIwi4pG+QQ42YuCAChHYuqDx3EbvUtW8OdYstTIpkyDeaZVtZCRtjRyTpofZeuM2Jwrzo1mXllVIuPIT73fq31VAFx4IpI9zfDo5GQ51lyZjxXv19vAaU5HoVcZrSgzx/xkCMweMf9O6jLmzuLKIVW4Kgm5va4Y8Rx+yKY1msXNzYubHGqLI3EBd3LFrRRlt1r68AO2tJWvtbvqJidTL9R7eEq3F0nKg4T2k/WRJiZEPVMf30HLmTskjF32n0DcRTiqnU1DYjX7GQ/tgH2Uy9UW2IzVSw7RjOL9QrxwlTrXLy+k81NRdZIyON+z66u4c5yMKOW/nK2Y/iGhP0ikuPLbo/KY6CSy/qgF9f6pq3gSmLoz62YAIn60iJb9pqQl8G7XIGwHPaDJlPD/npLvS834/p8GWRtaVAXXub7Q+mrdJ+iypj9MlkbSNJHKjvGm0D01LiZmS3Q2zZCGn5csg+7cFyo9Ap9u5qVSc2XVSQZaE9DSWMpo8qPKw1N5kQEofxC6N6CRb1GkmNIHw5cGU3UgAbjazDVCb8BIuh8av9Zgnimh6ni3EsIMcoH2oyb6+g/X2caU5mTCxGUEZFa4lkRd6oSfMHT3tp4spGnYaq7p6OCPqAoVPnRswIy2MOnPkRJI85YscxzkkFojvtodxCCT0Muh7mHjUJeS0eEfddZYj4SxL5D601qEQFIhZ1EBJMbkGWIB/eVWXUI3aG4dhq06RRSQO7Hedtogdzs6ghSp7fKSD4VQa5kAa0UoOzh7KxwHxrHfRZhLhDvDE/1/zP9a1MKzXTsyeB5IoEsGkVLyAjYGa6+XS/vkaHsrSVq7W4HtKBmgCn7JWuLRj1xnEwdoZBGbSFSEPc1tKy87OZX3lshZrPJAwCuLaFodoXzLbzLxvrx4tetZCoAUkbfFcPGgc232sxEfG31XpJ+XIbxEK0jFmjLEpI/wB+GTisn8iO2qu+uVYJj4fYfkRG2VoK85NGrmfcF3tE67pvd3qykWYffUN/LhVs8uGNm91Fu7evU1HiKeWJWcs0iqZARbzgbWPhwqNuXmyLtUSY8bMGJIKMQLaWPFT+ms04nHJc4/h2zp4ZMllZ2eKIW/J0BYg67iNdfTUadNdJkGJK0W4hIogLoLrtYkE66DcT4VejSSVisKGQjQkaKPSx0/TTDDwWhfnTEGS1lVb2UHjqeJNWdtt7txlJGm1xrkR05xdx1UHGrS1HGsUaxoLKgCqPAaV3RRW3Kk4kjWS17hh7rDiPRWR+ZfkrG6kWycULi5x13gWhmPc4HuMe/trY14QCCCLg8QaIT4RnYGX0/JbFzImhmTirfWD2jxFV6+g/4j5GOuBiY2xWmkld42Iu8cMflsG42Ym9fPqIQqxgdPzOo5K4uFE00z8FXsHeTwA8TVevof8AhzkY79NycfaqyxTK8zAWaSKQbQGPGytRCXvlr5KxembcifblZ4/2hF4YT/uwffbxrWJGqXtqx95jqTXQAAAAsBoAK9ohCuZEWRGjcXVwVYeB0rqiiEysnTXadxlzGUKSskRFkPl2qQL6aHdfxrtIZMZmdGeWI3/J0NiSLbS2umvE06zMFpmE0JAlttYNwYDhqOBFL5EkhYLMhQnQE6qT4MNP01ibrb3bbMQuq1wpkB1lu26sBjRpGOXPGre8jWYeo3HtFVJVcT7iuxpXbbN73LVUA8o++wXT+QrscvCkclRHjyMoUggIrHTW54se4d1SZSkpzQ5XlBmSw3ecjarW7bX4VWGBwybKMOI7JBC7CRApbHSLzxwqAznueXcG1Y+6vG/jw1EAkEMYlN5Aqhz+K2tZGyJrKQzIQ4i3WSN/vzy9r+H0Dtp30XIVwS8jb5bBEYOL7L3YCThf6hWlsblGKY+L/SPmTEXlqK8o3qh1mYRYRJ7WB/qfmf6tqv1m+pZc87JDMlwsrJeME7wrEny620S3rq3urgS0wObgqO+KtqSw6YynvktJh9iJFEO8yyg7voW5qd85XxxDAbbXlO+2g2koZPQi/S1cokUss7oxDi94r7HVyoUsT2eUAA1VMDPGQWXkCxkcXiisnuoXY3KL3Lx7TWStzMVpVQh7OPtpLJX41l/IkWPCjwojZbG5Gt3I8x04iJfbbupttSHAjwP9vNC4WPtvtu59ALW9dIsPJhUnKKtIi2EUjrsVyD5Qie9tHFVUeY8TTLAXJK5XWMi7TGNlgBt5UXzG2ttSB29nHtN/avVyT9RFAo8iLzibgw6c+ZM76gmdhTtmYrl8eX99A5uitw3LfgG7eGvppe3wszM0UbY+WB7iNt+lX2kD0ErT7POasG7CWOSQHzRyA2ZO0LYjWs1MzSSWkAhUaGBoi8an8JVyyez0Vze1XGtQ3lZajuMLWPdxBniibHjmyZI+TJbaSdhuSQoa8R1tx929TBYMSA5T3klKjcbsTIwGm1XubmqzmQxsBPGNg3ptnvZl1XyyqTx/FVo78rDDxmRZQA8UhUIzG3YNRqDas1uFcATjTlwEsD7IRHLac5GxQm1dq7ixJQ8xWGg7dCLVp43WSNZEN1cBlPgdazeFOZ4Aze8uje9a4/E6rc99NsGWRMKXlpzGhZtiXtuuBJb9q1Xf2+4Q72yKYav9OETfXAN3e2L+r5EczkbVhdW5YeSQpu2N+Aad6ndVeKBTMWcbyjb4prL5gVt5mHEjXWvZc0S5BkcMwc8pljUbGbh5huLXFra28a6b/pcYIrC6gJEX0F/shrVV3FwvcYg11GgNKYfyjba0UdITvI8iwRoGV9ZWJ05d9rBe81dw8L4gBj5MYcNuhk9FuC+Pb2aVD07EGS7Epy4r7pQG3BidQotp5uLW/TT0AAAAWA4CrWz2gYC5cHhH0rz6mLu3SPCveZ4iJGoRFCqugUCwArqivK1JWntFFFEIVHMzLE202c+VP1m0HtqSq+QXJ2x23orSDdou621N3hx+iiE+UfOmWc/5ikggBdMULiwotySU4gAfiJp18ufIRBTJ6ym6TRo8EG3oM7DgPw/5q0Xy/wDK+J00nJB5+dIS0ua41u3vCFTwB+921oURY12qLDt7ST3knjRCfO/mP5CIL5PRls2rSYJNzbtMDfaHhx+qlHyPmHB+YUxp7omWrY0qnykM3u8e3cLV9beNJBtYXHEdhB7wazvXvlfF6g65YPw+fCQ8Wao4suq89VtfX7QohNBCxeJS3vDRrfeXRvaKkqCDersrgXcLIbe7uI2sB9F/XU9EIUUUUQhXLIrqUcBlbQqRcGva9ohE+ZhfD+dfPjHjfUx+nvXx7PRwpQO6SNjugVEF4Sp05YsoDdxrSEAix1BpH1HEGO6kJzIb7ol3bQpGpU3004rf9FZe82gUG5bGHmXl1EsWrpPhbuMqSwDnAoChdt0stl8iqpHlJ4MdKn6TkRwye6szs2wukhfbvNh766n7x3eio0PxOMUZxuYFJDHqAbeYAmuYcwRTq0YZREeUqyKNit4DcGuQbaeqqu3uFLikmmk0JpXCNuLVT1mld1jRpHNlQFmPcBrWYlOWs4yNimOzblLFSDId7E6HgdLU5zZZHwY+YnLeZlDpe9vtkfs0pzZzBAWHvHQHzWufxIrWNWv3C4S9u2BXDV/qwirC4Mx7PZIysGXCMlQUkAbabsCjEWO4JY3qFhNkRQ5CJz5ANtwEGqkqX/NOl7X929TrvxcQvIZGlYb5ZAodlNu0acALVVjMnLW+RGd43vuntdm1byxLf1bqpLxpiAcK8uIjjLaHGgZWmjbIyyPcdt3HuVNxI9NhVyAZ+UHzsltuPjozQYy2CO4U2LagEDxNr9ulLIi0cn5dplPCBYikbH8TMwZ/5aVocduonBkfJjiE21jDAgNgAvlVrk8T3VpbKrY1oF8qrQd5le7h38SZepD1/CbY+VLlf9OCDyJLCzWttjZdrEnuJ9dPq5ZVdSrAMp0IOoNXXUMpU8YoGhrMTGQu1YwoUWN2la4/orM9TY+VFizNFYypJdkkjRybk/u1SzaLx7rVf6z0xyzTyJB8Kn7qNEAcm3AhmCsxpVy4St9myMcXRgGQ2+yYtkSkfrGse9a0MVap/jOWkaoqJamQ4uSsyoCkpFmIdjGTptWNdLuTx08asS5MrLJh4yvzt4kZtwWMIyqvnOrX0NhtPDWqeM3NhPTpnCzMl4kCsWCjXeZNzXPjcV7OkS5ASVFmkYKqX3SbABqGUXYAnXd9NLRyhNPq0Fa815yRAI6Vr3zh4Zo8iOLeglJ5hQmNOYAdArLCrXvx/wAtMZA9w5YCMKebGRuvbXynSqSwTx5y8pVCqgEiROVVRckFty637quc+KWOQIwLKGDJcbht0NxS2xK5dT2zo4x5hQcjGRCAHI3Pb7zamusnJixYHnlNlQXt2k9gHiaMjKix4hK5vuIWNRqzs3BVHef8vCshgdeyOp5eQ00ZaQWbGhQjbGgurC7EC/e36NK2710WbZ0ipRRRemQ7pTWjOATTWT842m6vNOSImLD+zx+H9KVrey1UDJmGR5p44sWCNd7TyHe4Otxu3Dh31HP1OaBuYzwmEg7ixKqpBA8jjVuOpt6KrdK6Zl9elfqnWptnRIWL42Ofy0mVTpI4P2PTx9FULS377HU2H3iT4fwgUFY9iiDAd3PtnnR2+bs3qa9XxZHbpLSonJmcgSwXCNIkbk20831XreVSXq3RwoVMzHCroAJUsLaW417/ABfpX/rYP+an89ayqQAMTQZyqWFcxJcvMxsKA5GVIIol0LHvPYANSazcPzVBl9TbHjjOyZwmPI5stguhdOJu17C/dV7r0/TOo9KnxoszHMxAeIc1NXQ7gOPba1Y3A6NNk2kky8fFiNiGmlUSDxVVN9PG1RdXKkLVSQaGmR5xT3GDqFAZcz8prur52cnT8o407LNAFZ/KAdhIJKkDuvr6ayE3WerSOvMzJip4Wdlsf6JFbZP4MV3z9Rjn5acuRzLGoKta+/ZbjbtrwD5PUEA9PsfGI1Ts2NyFIvXKmvMn5TV2u929lWD2VuEkMrGlRzGMwsnWeqySc18yYyBtpIkZbW4aKQK13ROr5mX0z4jJnbdE/KJVVBIBUBm3A3Oupq1JH8nSABzgWUWFmiH1Gux/8cXFSCHKiixypjXZKtmW5J1Ym+pOvGm37V17Wm0+h6jEGleeUzlCi9cev9tixCnygnD2RPkfMmR0vqzwuBNiQnzgCz+dQx29nG2nh2Vouk9bwurI7Y25Xj9+NwAwB4HQkWPprOdU6N0vLyJMmHq0QeSxZXKsCQNvvRkW4d1efKgg6XlZQy8iIbESNHVrpJ9olT22sKlZXcLpW4NWFC3Wk0Ln/XtttVu5pvqq1WubVoc/sm0rCdaPzdh9Tbq88jr0mOVk5MLk8uAEosjxpa9x5uPptWt/jfSv/Up7f5q5brXSSCpnVgdCNrEf6NWCjEEUbHlM/wBRB5l9sQiXL3pPjpFlQSrvE6Eo500824nXvq/D1aaAgSsV/wB3kcP6My39pNI+q9My+iOnV+hSk9FnKyZONYukSsbmRE47bd2o9HCxD1SaduYjxCHTaVJZGJJHnfio00IHprIupfsMNJw+8CRq/EDUVltSjjEd3ymtxcmLKgWeI3Vhw7Qe0GvMyDn4zxj3rXS/3l1X21jP4zlp8wQphDaSVgniY3SQkktuK390cG7PpFbLDzYM3H58J0BZXU8UdDZlbxBrQtXBcQBwAzJVl6HCV6jU2k10Npr1iSMOTvBHKKjlxgbbA660vSGaTJki3oZbmQIDG/LBNiGZoWa9+H+Sr/xEUUSF2ALBdqXG47jYWX11UeGd8481VKlGEaSuWVhcEldq6W7jWGuBbLoeyXTkJaiyJESPEyBIZi+9GJDRlVVgdnA3sdRtHhVaJDlZTSsg5cRN2s6mQgEbGjbS6Eca4gSI5JSKNYZVDK5G6PepHuqrWYi+u76K8yW5MI6dC4aYJeRGVgzKdd4k3La3fcmmO5civ1BAtf6ec4AAOla98MnKiyplhsY0SzPJIkgIIP7spZdG+i1RSENuWQKVOt1lYE/0WmSvOXCFvs3xn/aOwLMQLeZpN8TH+kKa9G6Y25Z4kgGM/wC+idAXBte1lYqrCmWbWtgq1H8ZyLNQVMl6BhsY0yYsr/pwTeCOx81vdkZtzAjuB9dPq5VVRQqgKo4AaAV1Wwi6VC505yqTU1hRRRUpyVszAxs0IMhd3LJZNe0i3DhWUzsXLglWSSLlSTMREjyc10QfdsSFH6pHeWAraVVzsKLKhlHLjbIaNo45JFB27h38bXpN6wtwVoNXOTRyvZMfEsJcQsQxkJIkY3VCR5mTvNh73D1XvPGxEXKSNhtu7SO+yMxsTtklYectt+zXubhy4uX8Og50rhObORtjRWuxB14eS5HcAugrnH5krNBCylHkaQyyKWcBQtm1O0tciwtp7KybqMpo2BGOPxllSDlJWdMd0niSRYtgukaW5r7tqXvw49p1uK7xMnHQyK14WUBnSQgBdzNot7E+PZ3VwVbmfBKWY3DyDfeU635ksn2OGgGvoFRSRFFsAuVGN7LY2Z5VOryMTuO1fu93opdARQ8fhJVMj61mZWLl4GQH5uPiGyxDTabcG8WQ2BPdS/qmKMeZOr9PIkwZ23qexJG96OQdzH+bup5kHEHT5iYjJCQx2sD+YwFw1zrx+0aSQmXBZjig5OFOp52JIDeRFFpH22vtB0D6HwNOt3XJ14swGhg2Try6EcIXbVu7bVARbu28UPA41HfDpXScDrPXMZZo9uIkLTGIaCRojGm17acW7K3HWFUdIykUbV5LKANLC1tKzXyRBA/UM3IhYtFCgSEt7wWY7rHxHLANabrP91ZX/DNa9gDSg4deR5yg5ajEjSwBB7RnMaXyGn3N73aBpzdtxcAfbtqeF+wVKrhhdWuO8GvHUOpU8D2jiPEV5eV5HdyrSWBKIu3cijV11N2H2h3cKvj/AOUgf/nY5n/aJyH4fhMo03IJwF9RkP8AcA4/i+M7ue+p+n4cU6zSypdWYBGuQfKLMVIPC9V41E1/NtgW3NkXX3uCJbizVblzuXGMfGQRyKNrcCsI7F7i1uzs7ap/uV97rLtNtVrpYM7KaaadfjLOwsrbVtzfoqaaKG81enwnAQxdM65CW38vkgMeJB8wv4i9IrmncQt0brOpJIiJJ1JJOpJpHULiupCu2t1ADNzPExG8ZWFtkXSpDFV5CsLmnp/uLpP68/1yUip6f7i6T+vP9clT235q9okLH5d//jkFzRc13FBNMSIUaQrqQovam2BhyRYOTLJhfEZCawwvZS5A90M3CtS5dVAScacOMVastcYKMK+YjCJqO2nHVcF2+HOPilCyXkRBfaxt5SV7qVSRSwsFlQo1r2YWNjXbdwOARx4cYXbTW2KnGnmphNV0gBuk4qsLgxKCDwta1qwHzB03D6L12aHEjHKyYBMkbe7CZC8bbR/RuK+gdG/urF/4YrLfNcgj+ZcYpFz8j4ZTDERcM4eTaW8FvurIvkBXJFaVNMptIGIRU+pqAHlXjKWDEnRMf+I5YvnSqRh4x4oDxkk7v5DiavfL0WRBiSySSFDnk3RvtqBqwHYza691Vhixwzvk9TY5WaQHkX3lj3aIxX7ag6aaL3U1XIiycOOaZGCttLixvG1r7rjWw7xWK91wS4J1OaF1ypwVRymiiWrVr0U8VcXY8acvnOMvJx35areYsCyJGRZihHlawJHh7a5DpkSPPLHI0QQ/lyJflODta1uPDsJ4VDHEXWxVMaM7Ga5uyysfLJGwO4Fh97t9dS7WEnwbFlJYvGC9pRrrJFJ9rjqDr6RS6ACgzHwnKmRyM3J5TRsdwDJIj74wikFniLecMF+zUEywhzApAKEEyqbB7C6M/cbH3v0cJ8gSRFYJmXYsiyCVFKuQdwZvKdob1a+yvcPDlyctcd/yZF3CHIUbo3VRvsLH8QI8CRwplpCxouJOOHxkWIGc5wcXLnmZ44hLLCRzEWTlu6N965AYeknvDGtXh4GNhh/h02cwhn17QLeiucDBixYIgY4xkLGsckkahd20a68bXq3WtZsLbFaDVzlZ3LdkKKKKdIQoooohCiiiiEodWwTmY6qGKKjF5AvvOoRhtHpNvVWSHO3IkHNh8qxwm5UsHP7xh2+9vN+JIFbys7m4uLPnS5EZcMSLyBzq67R5RwAXbb03qlvlQJ6hNDWnbHWSSdMV5MUeHIsGMWPOKidEtzHtuI3SNrdr3J4gXrqF+ZFziCu8BbKQPKDZYYADopI1ft+q0nT8B7sE5gJYNuZmBJ0e4JseGtL8nHQ8wQoWx5CNieXaxSylySC7Kg+yNPGs1WVsMa8WMeQRjLORkR5pjxom3yh1Ywg/lsI9rszPb3Be3iarIkmTthxxbIyCFksu1Z/MzHay6iNe031GnGvYoWyArTyWaQso2m0gQbgSQltq8FVR363JphkRdc6RjnPx4ky3C2MIGwwxDgqKLj9b/JVjbWNbaR9KYt/KQuPQV4nKX+jYuDgZ+ViwW+IZInyHBsZHG7c+waL73Z+irvWf7qyv+G1fMcX5tz4vmF+ppBvlmUw/DknUk3tw+9W95/WMvomVP1GGPE3QuRCLs9tt/Mb2WthQAQBwpKj4q3YYiJAuToBxNc23sq3aI6Or7SG0OnLBtub+R0oss55YYIpBd5W9xUU6tr73oFT5GX8Vj8lkZjzLiRwF8gPGwPEjS1qbu9zdZxt9souaxpuuBq9MNh2TP2ti2q+vfJt6TqtqTp16ce2Qc1ipMZC7SWkCcFDe9LD2a3s5HD7OhvXqhVUBRZRwAoI4FTtZTdGHFTXIIW5A2qPfQcEJ+0v4D+ydKlt7K7NtBFUukUu0xDfdbofL7JG/dbdLqGD261t8NP3l6jjLUf8Ac3WP1YfrpFT2P+5usfqw/XSKkbr81u2Lv/l2PwGFPT/cXSf15/rkpFWkxI45emdEjl3GNpZw2wXbjJUduaXFPIzu2FVvAcUpJ+hLM5y1gYRzGK0bsNwVr+Ule3WnjSZOPjQowXIy22obfloz2uzH3tq6E9tJh0iGHIkeLJgdHsAuTctFb7oV09utS/wzCtczTO3HnLECt+8Nyybeun3qu5YA0NJd24Fu0EYiorxjF3ysrBf4ZlxczVAzjmLG6mx7twpJ18EZyAm55a3I77mrJwo1Xyz4ATiXaG7+k3m41xF0rpr7pWmyGLDWTZsiuO1VCWFdsEo+og0ywE5uQLlvQpWta4mNejf3Vi/8Nao5D4ef1fJ6fdVyY4IwJQoMkbAtItieI14fz1Unh69F0TGyOjZSkpCrNDMoN1tfyNpaw7DXz/B651l+svl47Kc3LO030W/hrSXAJYHEGsspgq9gmsdJMXfBk65GPcR+XcsPmVgWL6mJuw30GlWYMiPBaTHlYJKzlhCT+WokLMGV7X2EjXuNMF6L1STFXIzMnmZ6rZkUflOh4xMrHzdttePhSiWE44ZoZDuiIFz+82HaLgPe44qyfRqKyNzY0NpP0vivylq29RXiM5JM/LiM6qWKAqAxBIW43QzAnVNdG7Pr5x448uU4+SWCxbhBG5BkS4U+SRTe62up42qPHx0HL5yFYI2JkXyhVL3VXFgHVWH2eHjpTB+n9PSzFOWAVC7WZQCDZNAbDjpVZmVcMa8GEYATjFrCYO0c3NmBDRTG5YhU/wBoo8du/TgQR21q+j4LYeMVZzIrkPHuADICijabW7b+ql2Hi4sGbHPIXLBtJC58rNceYcLNex07q0NaWxVCnqA1NadkReJB0z2iiirsTCiiiiEKKKKIQooqvmZQxorizStpGp7T3nwHbXGYKCzGgGJM6ASaCV+pZoiUwq4RiLyPe2xPT3ns+mk3xoMkcOLEZTtBYDyiNT7ha/1cal5kEpR2tM8jttcgElh7zDw0+quJMiMRuyAqWSRyygBgYztPHtrD3F83rlSDpGCg/Ey2iaFzx4yu3xcSril444C/LE32uWFLcOAY24+yq7LhPZ1d40vHEHdrqwUc3aQ32VXiKZMkUGK/M/NRAzuWAO86uSRw1pTO0JLmDG2tGuQsixgXUbAoZrkfaPsqFs1rTDqMB3zrYS7BkPFIuQUaNSwKyzKArSXYAvt4KykbT2Hxp1Lntn9LkOCpORKDEIz9hyNdx7vHhWYXJy1AMatJzN0Twy22NZRYAE3D6GynRqudLyosSZTjyl1ZEVGk0AVbjkSi3lN77b6/pt7a+bJIb6GPDGkXcTVlmJXP+HMUeGkkOS38VRhLzjohca2A4jXtp3ndSjn6XkYbebOfHdJIlBsHKsjG5toGppj5sM52C6S2uY20Pq7D6qS5eJfJyBuKS2kjDAfZmbmBh/Lvq3uNz6doXLdGDGmrMCKS3qYqcKcInyc2XISHGkw5IyhRmbbuQNbyqG91b37TUow88i4xJiDwIUEf6Ve9Qz4MPHOBifvQuy41EY72J4t/nNI43kiXZFJJGguQqyOo11Ogap7DdNbtsEtC2hcstSSTXtmf+4eh6o9R7lxwtDo00WO/guof+jn/AKo/8VeHA6ibMuHMHW+07AeOhBG7UHtFKPiMn+3m/wCbJ/4qsYE98hVyZZ2RtBaWSwPefNVm7vm9NtSK60xWmY9sr2F27XUCm6jFsGJGBjNMXKx+h9WORC0AcR8tW7lPAdul6QVb6lzRlSw7pRGTpG8jsCOw2LEG5qwzYiYbR8uOVhuDOvldW+ybm96pXNyKWyFY6wMK1NO0yw+39dnAb0xttQJYYE9AOEWVp8HHz5Oi9KnwE3yQPMx1VdGaRft2FIcZl5TIpVZyRZnW91twBINqljax2ZLMqgeUxSiM3422q1tfRUl3HpsSVrp4cT3cZDa2gFrVj6q5jBV7W4Humo5vW2IeXpcTyDg5eO/+kajaX5pL7hjhR9wNFb2t+mstbdKETKksxAXzSHifA1xK2RDK0Znk3KbErK/1hqeu7UmmkVpWjLw75K4wVSx9QqG0lkuKce6a98rre3fJ0yPcOMjPF9OslUU6z1oSkABzqNpMWwkdgYOAT6DWZkeSVCkskkiHirSOw08C1O+ndQgy4BgZf7wrsBPCQeB7G/zioXt49tQy2kda+LmB0ktu1m++j1LttvLUgaj3Ros4zOjQdILnHy8iBYpCQPKpUBmHZrwHjUcvyL0f4OKHGUw5UBDxZPFt4N/MO0V1iYdsiAbi8oEce49qxNzNx+ineRmw452m7y2uI11bXtPcPTS9vufUttcuaVCmmrIGX3t6WCrjhlIZs4YOCsuWDzR5Ag1LuNNPTa9+wcaz2RkSTSNkBGkAYl5IVBVZLqPJu95VUHce0+PD3qmVHlzE5EpjVEdXdNRtNgYI9PMxNt1tfqFI5OWR+YrRCLZEkMQ8q3GtwDcvYjyi+2qm5v8AqkBfoU8eMbbTTnmZKi4SkyM7yLukjLxtZVDjmgKF+yw4C9r1OvxcivjK8csAYJzj73KZb8OBYX4+yqWMYlCHIx9zOmNHEslrsLctmWxPBjTdEiyMROWOUjqrptAGw6MDbhoaqXDQ8+pxHdGLjIxm2keHKiMdlJS/mEii+8i3q8fCnPTM1ZVELOHYC8Ul7709PeP8vfSdMiMxK0gLFERwzAFiXJQHTtqTfBEZHW0Txuu5wACGPut6NfrqW3vmzcqAdJwYDLunHTWuePCaSiq+HlDJjufLKukijv7x4HsqxW4rBgGU1BxBlQgg0MKKKK7OQooqOaaOCNpZDtReJ+oAdpNGWJhCaaOCMyyGyj6STwA8TWV6tOMyVllALAHmE6rGg12aftd507dLubl5GTMFXyyEEonEQodN7dhc8B/Ne6gxgnlaNGpBl10kkudqX+6De5/Wasvcbn1G0ofAuP4uvZyllLekVOZ90hV8iNg0EjIXUAbvOwj7LFtVv4dvqqt8VlSZfw+8chUKkILCyqW23uSRusT39tXH91pCblrhTwv95vDQ+q/4ajx8fY8jW1CkH0skpb2rSQQASQMpLHCaGfFV8PnMrOsrPHNtNtqluWthfspIImb8rHsz5EQGRMSL8xnuvMPioIHqrUY0HxXSjEHKFy5Di1wRIzA61ntrmUQoDA0jQRqjKPMyybZHPA+Unvpl21QWig/MRB01SKtXUD5SfZJG6TnPjrkOgEckZJaO2/ax5g5nobVSmo8O3uDlFGaPzxM23JhcmQq50JHvE37u3iK1MaCONYxwQBR6hao2w8ZijGNQYzuQgbbG+7s8dasXNgrU0sV5jOQW8RmKzP4eE6StHjxM6qLq6ttZLnVGLMv8uNS5UGTDG5WIxzSKyxuSrXcKSoNmY1oar5uMciIKpAkQ7kJ4X4WPpBrj7BfTNGdnAqKnwk9RAXjqFQAPfPmcO/lJzL8y3nvx3fav43runD43SsrLyXyciXDdZChRYi6lk8jtu2N9pf017/C+g/8Auc3/ACD/APjq0lu4yhihWoy5TFu7RvUaj2yNRoS0TV1HHJIwWNS7dgUXNM/g/lo//wBeTt/2X3eP+z7Kmxoug48weDq78wjQcncSD3Dl0NauaTpWp4Vyhb2h1rrdQtcSrY90pRZzcyKLNvaE25nCRfTpfSmDDBEchvzl23O1SVA7ye+3CrWU2N/D5+oY0seUYGVXVoShuxUa7teBpFk9SysldjMEj4bEFh66oPtrhYVT0vvUbDuE0juksIQ9z1iRVPBj3mtDGuLlySTx48OOYImXdzdouRbRtBt1oy8vDmWTGncRTpoJLbhfvBF9PClDdQy2RIxIURF2AJcXHefGruN0bFk6fBn5fUBjfElwqmPdqrMOO7XRa6mxLNU+GmWnE15msh+uLqyWh6mNSbnhGk8BSV5Y8hITkQzJJEujPGAjKT2HQGqNPY8bpUOPJir1Zdst9x5DFj6DfsqD+F9H/wDdWPoxnNW7Vi6NQK8fCQMSOtJV3NrWE0MK6fEGuVAb+msU1zKWEbFb77eS3Hd9m3jenH8L6R2dUc//AKz0RYvT8bMxngyXzHMgUR8howGbyI25vxEVN7dxVLBGagrSmcTa2rG4oLoo1Cp1R1iQZM0UZaJpJo0USOCos5UFtSy1FmYUjSKk8TRqwu8jHczW4IpVm1/kKfYWMceIhyDI53ORwvYAAX7gKsVVTYL6Yqzq5FSAfCD2TbN46sACPfMpPywqtL5IUbbjQITGzSDS590gg9nZxNRp0jPWFp0QGNI7hpCN1geYeV6W1YvqbdvZqFw8ZGdhGpMhu5Pmvru+1ft1qV0DoyHgwKn16V23sFWupi3IZQa8TkKTEGJlHKyLK+PEwx5gR+9Vt55Z71FhTrHxVTDMyhlSEpHEWNyQrct7gHtpYyssrQvfIaJ5keMILKWfajganzHxrQ5EHwvSuTuLlSt3a1yWkDHh6aRZtV9UuPy0cdNUmzU008xHsmKbKyoc34feOQ6AAONws43FdCDbdr4dlWWkyHYmeR3MYI8vkYx9oJXU7fE8de+ucnHEnKI4sNoPiFjZfaa7S+1ZL2ZbBiOI+61v6Prt+KlEggEAVpjJY4y70mcYcqrEFBIBjYaLIh12G/f9nuOnZrq4Jo54xLGbqew8QRxB8RWLCBTyzZY3JMeukcmm5L/cbQj1NTfCy58aUo/mksC44CZBpuH414H+a1nbfc+m2lz4G/8AHr2c5F7eoVGY980NFRwzRzxiSM3U/wAiCO+pK1Aa4iVoUs6wJlEcyIZES42ggWdyqoxuR3kUzrl0SRGjkUMjCzKRcEHsNQu2xcRkOGoTqtpIPKZkoy/9OrXnl8+RKNCF4ad1+C1UeMopAXciXU7dFLaBgPT5Yx4Xp5k9LmiE74QQBgXVTfdvC2twN+FL1Rd2PClzHHdySD5mXQeu53GsW5ZuWSdQwxx5049JbVlYYGUTEN6Ixv5ipPeFbax/pHmGvVX3r8WVGPpaGdj9dTiMGVymscTMpN+G1GY/tSVEBdrd6RD6YJhUa4HsrO0mk6Ob4Kelvad36apZQ5eXJ2KsqSD0XRz7Qa8xeqRYmFFGqmbJmJ5MKkC4UAMzMdFUHifovS/K63jjKkXLYGQgK6xKWSMC+jMdWOuth6qvX2BsWVHiuKLdwKMThF2rbtcbSpIOrKaskAEk2A4k1xHkQSsVikR2GpCsCfZWad5ZVEkdsvFceXa5Lr4rvJVvYarp1LFWaGSLqDLsYGRHUltgPnA8qm+nDX0VNd/qYAWzprRjXKR/TmhNa0xwE2NFeKysoZTdWFwRwINRY+Vj5PM5Egk5LmKS32XXipq9ETH9bIw8vNcglUYyADtDgP2+LGsdkZUzvunkd7C7i91YHhYDTSvo3zLi2kizVHlYcmX06mM+0j6K+c5cHw0zwOSwU7dB36g/RT7rM1q2QcB4SOsz9AW/cBH1HUOwyIKdTb7pGmotXhcrcqxVgb3FgQw7Qa83MEsAdNABwNq9c62XU2+0OzSq0Zxmu6XPJP8AKnUnluXEsS7j9qxjpbTTpKqvylnqvC0LH9Ziu6ldF2urE1MrbulLVPufbCnp/uLpP68/1yUip6f7i6T+vP8AXJUtt+avaJGx+Xf/AOOZd5byKZgCZC4llLkNGy6kDy6WA8bU7xGkfFiaX94VG6qOdHEep4rFASNW0Hm3Hb67U0rStqQzY5YSF5gVSgpUV/lCrXS4ud1LGTsD728BGC4/aAqrTz5axbtLmsNNYYvUbyH6QB6jRfbTbbr4R3zu1QveXkp1Huj+iqZ6jGvVf4Yws7QCeNuxvMyMvqsD/mq07pGjSOQqICzMeAA1JNZs2ZzJkQRMFlkRGOoDMAT9NdgggEG4PAisc/UsR5pppc9iJGJjRUIfYdUB8rG+ug09F6tJJNGDJJ/0uKgu25yJG8W2EKvtNUW3+lyDbOmtAa5/Puj/ANOaA1oTjiJdxxzMuO/Bpnc+ovIPbar3Vv8AsmHeyf6QpBi9bxjlRrikcwXVBKpVHvbRWGqnTS49VMsrqUWXgzJYxZEJQyxNqVBPlYEcVPYfpsahYYeheU+G44uXNJzxEldtutxNSkAac4kIukduIVmHpWBGHtFeiMB3RfKN1ge5WYKp/onlmu7WdBbQBxb0YyC1d8oBo2k0jkKLuvx3Rr2/rRiqNcuysZIVjLKFK7UYhbkeVW1VVPoO6M+BFWwjN/0zsRNF58eU6kqNPXbg3eK6Mal8iF78qUBrgHys2jevTcKv4vTJphA+aEIUB2UX3bttrEaW499St2bl4jSMMMeVePWcZlXMyXo4nYPM6GNHsNpIILoSrMLE91qZ1zHGkaLHGoRFFlUCwAFdVtWrYtoqDHSJUZtRJ5woooqc5CuJIo5V2yorrxswBF/XXdFEJA+Fiu4dolJUADsFhwBA0NvGvEwcNHMiwqGPHS/0A6CrFFc0rWtBXPKdqecxnzXz8XqyTQNy+dAq7x7y7Ga4U9l9L1n/ANOpJ43Nav53jVYMXKY7QjPGxPCzgP8A6lYKfrcKErChkI+0dFqs9sm42kZ0m5sr+3tbZXuMqMaqeLGkcY2ZlYjbsZ7a3MZ9xj+IVBtBG1vN337TxpI3W8snRUHqJ/TXcfXJgfzI1YfhuD+muegwqQBU585Jd/stZOKl8GOnAz6Tk9SlwOg9JzoRzI05POUH3kMTIw9Nz9NZ/wCXuvRdOyM/JlJMciO6xE2aSVX8i+mzG/01U6N8xRtgTdNyo2m6dKxsNBJCW8907DZtbUs2IzFwbg6Xtb8N7VZrlPMbm5pukq2oIze35GbvN60Z16x0ucKJIcd5caQd3LWQA3+0jMLf5Ncx8Jh9VjWeUFZh5ZdjbTuA7f0VQyJmypnmkBVmYE68SAPZRj5MmHM0y+ZXsHThew09Yp1m6qtpcVRs+PfK15zcbwmjLXSRxHKSZfR1xoGkWYsoIG1lBOp7Tep8boECqHlkdndfNbygE93q0qXMXJycS8bLIrkELGOI79zGrCJk7FvMQ1hcFVYA+oCj9RtBcJoStABhxxrgYss/pL41DF21c6UFPtjCJVXoPVUUWVeSAPAbaRU6xzJ/A+sCQhiDDqBt7R2XNJaTedXcsuTYiR3H0WfwQp6f7i6T+vP9clIqdyX/AID0ixteSf8A8yiy4RtZyXHCG3FUvD/1xNm3OWT2jRF7W2AMbH7NqpyxdfM0gXJYDcbBR5db2A+iwpmcdSHEiguwJMl2CEk2A969E8EKw73VUZXjlkdbKSY23C7EXIv667d3mr8vUmOMft2tLhcXVgAMKy3iRzStjYkjqMqciPcOG4As7D0KCac5XWumY2H1Hpci8r4NBBEm67TCRPKV4G9/e7uN6yuH1ZcbqydQcGYRq3w6DQBnj0Dd2reY0tlyJMjJkzMgGaWeXzjhcbvMPAcaZdvF9PJR7+JkrWm0GpTU7nuHD2R9P1SXA6h0jJyi82TBjLzVv5zvEpRXLdux13X1pzhdWlyugZC5b7sjIgzJ1PYqI3L2+Hmey+isNk5c+RPPnznmTSttU954aeF9PQK8HUp8FXjLsY5FWOYra7gEybB3LfjSic5Y271vqCaKWGJ7qk9kYbQFKrp6Ow1NkZmVlndkvfW4jHuKRpoP56QSdcmJ/LjVR43J/RXC9byh7yo3qI/TVb0GNCQKjKelbf7IupNTowU6cBHn+etF8pc/J6nLPO3M5EJTefePMYEB++202rGY/WoJCFmUxE/a4rW++SYl+FyckWPMkVAR2hF3f69dt2yLg1DKsjvr9i7tme2yuwIUHzCsfPg4kjh2hUsvDS30gca9TCxUcusSgsCCOIseNlOgvU9FWdK1rQVzymHU85xHFHEu2JFReNlAAv6q7oors5CiiiiEKKKKIQoooohClnXuv4XQsP4nKO520hgX35G8PAdppnXyj5uxuudT67lTHEmaCFzDAApKhE4EW+973rrhYLixA7YAE5RT1v5h6l1zIMuXJaMH8qBdI4x4DtPiaWU0h+WeuzGy4br4vZB+0RTCP5KykAbPy4MRT2Ftx/QPbSm3Flc7i9gNT7BJBHPAzN11HFJK4SNSzHsFbWD5V6Dj4jZ0ksmfFGpYmIjaQvGwT+em3TsTp6iNYOnrDBKnNSRir34WuLsdQeNIf9wtgHSrNTDHw/HGTFhjxA98xuLjmEBI/MCPPbXzd+lXUwsx/cx5SD3I1vqrQxdRm5s8LzwQSw5Ix0iWP94DtKn3iRcNU2Srydcjx2mlWCTGeTYkjIN6Oq38p7mpTb16/QB4S2ZOWPSJ/wCvVjVrjMa8AB85nR0nqh4Ysn0D+evT0XqhFzivpr2X09dM58/Ngxc/FMzMcbJhgXLNt6xT7SbkablBtermYgwM3p5xAVE83ImjuSHQqzbmDE6ra96id3eHC3jXTgeA1c+s6P2+zzuYdR2colxcLq+GxtiSNCxu8dhcH7ya8fDtq6kiSLuQ3F7HsII4gg8CKsx50uL1nIjyZScGbcId5uI5IUWR1F+xlYkeiq2LjZHUsnqGS7mDIR0WFABt2mMSBJF03Hzam9+6ueuTVrgVRpVtS144ZRd79vUj+0za6kUbjSW4f7m6x6IT7aR0/kgnwujdS+LCqMlImglQlo3A/EQNp14N6r0gq/SiqDymdu1KraBFCFIhTyT+4ekf8Sf/AMykdPI4crO6FgJhgAY3OkmnkB5SgmSwXtdvBfWRRSqsOkNopYXVGZSkXySqIx8SlmZrRwj8x2IPl2heJqpN0/q2VIZWw5EU22pp2cC2vGtJ0/pMOEec5M+Yws878R3qg4KvoqPpzSP1LqcckjvHFJGsaMxKqHQM1h6TWd+soWNsAhBWrVxxpgJp2/25Qo9Rjqb7mQ9szo6L1RV0xHA7ANv1Xrz+EdTUf9pIPQB2+unOAW/+Ozzb35jGcq+9tw2uypZr9lhVrqyZIx4Vxg0xhYSTYwcrJNEo2sAwN7gkHjrU/wBbdD6aW/qK1oRl3yX/AF9mla3Mq5j5TLPg5a6vjyjab6o1gfoqllYokTax2G5YbtPMa22GoycFZsfLm5DO0itf8wIARyiX3W2mvOjDKysLHy8qczieK7RyKlgxPlI2qOyu/rmAJZV8LaSKkGvs6Tg/bwCClxgcxUAz5k6PG21xtPca5r6NFj9Kz8OfK6jiY8cMMkicxQV8sTFS9xYi5HZSyb5Y6LkctohlYQnIWFnXcjFvdFtWX+lanrvkqQyutDQ+Ye6O9FqDEH3TGU16F8x9S6HkczFfdCx/Nx2N43Ho7D4ireV8oyxTNDBm48sqWvE7cp9eGjae2qcvy112I2OHI3ilnH7JNPXcWWydceeHxkCjDgZ9a6H1zB65hjKxGsRYSxN78bdx/QaZV8q+TcfrnTOu47nFmXHyDyZwVIUqw4m/3T5q+q0wMCKgg9kiQRnCiiiuwhRRRRCFFFFEIUUUUQhS7qcAUrkrpwSXxBNlb1E29fhTGo54hNC8RNt6kX7vGl3rYuW2Q+YYdvCSRtLA8pkOqSSx9RwVBleKcSo8ET7NzqodPtL49tQ9TTk9Kxc3aVkw5YpvzTvZFZtrqzcSAH9lO8noWTkzQSuyq2MxeMpIQNxG0kgxHsPC9Wm6Nzl25LoyXvtCBtRwPnuP2azE2t/+3/bpp+qpHM/YZYNxPFjnlM50rZB8VgZSFMjJeSYsiloZVk03RFRwt2HWuOjRSwQ4inGlXMRBDM0jXAjB7ELluCiwC1ro+l4iAAqXA0sx8v8AUFl9lWkjjjXbGoRRwCgAeynrsWOrWwGuhIWpyw6SBvAUoMucykXSuqifNlgChMxw67o23IVUJf8AMaMX0vVp+hZU2RHkkyxzRRmJW5iLdWtu90PqSK0dFNGxtZkscKcBwpykfWbkIih+XhHjvjbIuVKWMwZmkLlveZywBYmuovl3lSLKsgeSNSsZk3vsU8Qu6Q2vTuip/o7GPhJrn4m+c56r8/cJlepdGhxcSXL6gwkx4ZfjHJfUOgtZRsW+7htvrS3pXWuhmbIdM48zLk5rJOojs1guhsBwHfXX+JGJ1TJGL8NvlxEVmlhTWzggbyBx0Pq9dfOiCpIIsRxBpb7Ky9VVmXgQDWnHIzousKEgGfbulTJJE8Fw6DzIRYhkfiPGx9lqr5nyr0nJu0SHFc9sJCr/AMsgr9ArC/4eJ1V+so2OzDBiVvir35dmGi924tY19TqxbQqiox16RSvThFXArk1UEHgcYmw/lXpWMQ0qHKcds1iv9QAL9INc/MXWOn9NjjhypliDecpxYqh8oCrrq31U7r5N/iFBlx/Mcss4bkzIhxmPu7FUKwHoa+n89ddAyFK6dQoSM4IFSmlQKcI2m+eekC6pHO4ItuUKtr912px8uYjZeD/EMclhlnc0s7gyty/yxuES7RotfLYoZpnEcKNI54KgLH6BX0T/AA96R1Pp02RJl3ijnjBGOTc3DDa7DsNr1VGz2ykIa1fylsTTGN9W4ceXSOl+V1Vjtm2xM5kOON3J3k7r7d/frbh4V3k9CmmkWXcBMgKpLHI8bBW1Ye6w1t208opp2dg46TXnqPzkfVfn7pn06PPjYnwmPEUSxAZHVyC2rMTLtuTeuMXAyOn4ceMI5HEChELLe4Hu35W/6q0dFLb9vtEEVcVOo41xkhfYcBymFfpmT/AW6cSj5SvzTGGsJfzecy+YKRfhrTSLJTII/JkW3mbmoU2EfrdvorRvHHIu2RA69zAEe2qz9MxW9wNEfwGw/qm6+yk3dg7A6XB8RbHw4tnzk1vgZjhT2THY7nJn6llLLAYJZ1hdZRc8mIKhK69tzbSnyqqKEUWVQAo7gNBVmboaO4e8crKQVMsYLAjgdw/mo/h+bfhEPxb2PrtsH11Xu7TcEilvpgRwFBJrdQcZN0yAHdktxN0j8FBsx9ZH0UwqOGIQwpEpuI1CgnibC1SVrWrYt21QeUe/jKzNqYk8YUUUUyRhRRRRCFFFFEIUUUUQhRRRRCFFFFEIUUUUQhRRRRCFFFFEJUz8YzRiRP3sVyv4gfeX12pHk4vT54zLlQxSRhdzO6KbKBe960zDcpHeLVm8zAyZIFxpFeNbrzLRmVZFW11uvBWtWdv7JLpcQNxDstcuGUfZYUKkjoDKfQst7ZGIq/CiFw8UMTFV5Mw3xmw0vxv41JN1nJxupDEynYY0oXl5CswCu5YKkmv2tpsaPgEh6iudAyQRiIxTRBNocX3A3BFiPRXr48eXPkLNy5cSeJI2QMS/lLG9rWHvd9VfUfUfFd0lB4asCDxp8Y3SKZLWueEnxZ8qdZTNK25JXjXa8gFkNrnz0qbKy8jAlz58fHyVxnlV4ZAzPthYq5R5C2tlva1MemYkmHi/Ds75DB3YSbWZiGN13WB1tVWHpHVTiZOGqiOPKlldnVJN6pMbsq8xUF9bXoUXWZvrca1z1fTxnDpAGQwPLOX8U43w6TYyrHDIgkXaoUbWG7s8KcdOxzHEZXFpJrEg/ZUe6vt18ao4/SZuSmMEEECII/MQzBANugW44d59VORoLVb2O2ZGa46kHJNWdOMXeuAgKD20ntFFFaERCiiiiEKKKKIQoooohCiiiiEKKKKIQoooohP/2Q==');

schoolMonitor.value('msrc_datauri_logo', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAC1CAYAAADlY8JKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkQwMzgzODE0MzJBMTFFNEI2QUJCMDZDQjVERUZFMzYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkQwMzgzODI0MzJBMTFFNEI2QUJCMDZDQjVERUZFMzYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyRDAzODM3RjQzMkExMUU0QjZBQkIwNkNCNURFRkUzNiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyRDAzODM4MDQzMkExMUU0QjZBQkIwNkNCNURFRkUzNiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqbAphYAAD5nSURBVHja7F0HnNzE1X/Stqu+c+8VF3DFheACxqaT0EJJgHwhJEBCJ0Ag4Uuh5EshJJQkkARSIIGQELohdOJgCAFjDNhgY1ywjXs5n6/fFn3zl2bv1rKklbSzu9q7eb/f8/n2VqPRaOY///fmzRtF0zSSkl3uf+RZ+vr1t1KPqorMj8cxvYjpiUyHMw134SZAR9nB9DWm9zJ9Png1TBJFK6n8tN+RUtmbKOX89eY40ZEjif5+uuzfpSJh2QS+5Sqm1zPt202eV2HanymG98lMH2J6NdNdgQMtOQl3WVFlE/iSXzO9rRuBlVkiTM9l+iLTAcEBqxQpZTWkRGKyh0rAksLlW0wvlc2gy1SmDwSFqWvxVgqPOJyUquqs5qAUCVjdQeCzulk2wz5yFNMLi16LZDupPQZReMIppCXkS5GAJQVyOdNy2Qz7ydVFbZdUUv8Rnfl1Umv6MPCSL6SrinS6u5dKpicV6F6vM92U44QCz3MZ06MLACajmc5i+kpRwEpLUvSwKyg8bj5p7bKjSsCSAhnLdFgB7rOC6WECy7uD6ZUFqPfMggNWMm4wqzmXUmTyiRKsJGBJyZDBBboPVh6PYbpeEMOaUqB6Dy/cq1CIEq1EsSqKzf82hUfNNMBKRjNIwJLSIdEC3acP0xeYthkjM+f3Wyg/ZYFiCRQGTo2k1g6lsuNuIrXvSMmsJGBJCYDIYCIr2sjAKjToYMasrmWgNUiClQQsKVKCClZNFBoyg8qOv4mUWAVpcdkm3U1kWIOUEgKr6QysbmTGuQQrCVhSpAQdrI67Sd/cTDIwVJqEUnzLEqZv+gR/hXJ3rBcUO5j2Y/r5ooBVTIKVBCwpucrjTH/UjZ53QqEAS4KVFGkSipfutppXWTiwmsbA6kYJVlIkYEkJqigGWA2explVlQQrKRKwpAQYrAZM0kMXJFhJkYAlJbiSaCW1x0CKHXkdKWUSrKRIwJISVMFG5rIeFDvhx6T2GiLjrKRIwJISUEEedkWl2LxrKdRvuNxuI0UClpSggpXGTL92ihx6AYUPmElam2wSKRKwpAQVr+LNFJ5wMkWnni6ZlRQJWFKCKgpRvEXPvBCdfYmRh13ms5IiAUtKICUVJ6W8lmJHXENKOCJPuZEiAUtKYA1BBlAJPQ+72meIPOVGigQsKQGGq/ZmCh/0OQqPPUL6raRIwJISYEm0UajPaIrOukj6raRIwJISZGqVYj1Opcjsy0gpK5d+KykSsKQEGK/iLRSeeBqFh0+RpqAUCVhSAiw4Sr7XCIpOP1duu5HiW2QCv4CbUFqizTClrERRSVEUNu2w16iEmJqSl+pR5G3s8iQp4ShRCCeVaUV6lCTFZnyVlPJyya6kSMDqckDFzCelrJoiY44ipcdAopQJaBg2pfZsJGrbS6n6TaQ17dKjxhU1woApbFzPACp0wFxS+42m5OpFlNyxipRIReFBC9HsI+ZQePRcya6kSMAqfYBiAJIEEwKTSpESY0A1/kSKTD6N1D4jjazvVhijGZdSawsDrK2U3LyU4h8sIK1+M4VHHk6RKWdQaNAk/S2nRh9Lbc9+l1I7PjYOcigUaIEdRqsocsj5xh3lqqAUCVglLDD5mEkXGjqDQsMO1Qe32ns0hfqN1Me6K/MpWs5AbiRF2DWhA44lrWEzqf3H6kCnMxqmSnUfip3wI2oFaO38mDGtwoAWmF5k4qkU6j9CbmyWIgGrhGmVkV2z74EUnXk+A6tDOs7PQbYVT36eVOd1SHynVDCwSprwiIGWykCrjIGWzrR2rWFvvyzv7Epni5PO0OsmRYoErJLEqpTOrCIHnUjRORczkKkU54hOkW18k5ZmWsfeQC1PXEHUUs9BKz9MS2dXE05ljHGw1+erZlrDtIpphFdQ4U/WwnQv0wamQjkbbrJkC1FzvNsvn0d4+9cyNc9qbbz99/J3kVN7Q6YNJKqMSMAKpqSSjG3EGau6kKLTz/LOpnIVDMbaQVR27E3U+vz3WfdrIgpFxN+ng12dbrvIyQUD42CmM5lOZXoA0wFMezDFCkEoA7A0PmAame5hupHpKqbvMF3M9EOmvlszyep52bNEK3cSxULdqlcOZnoo18lMRzDtzSeO6P49SG9/NtvRJlP7v+9lEtH4W33lXKKJfSVgBRCsErq/quyo71B4zBGGf6kITmjcNzR0AsXmXk1tL95sMD5FFXwPsKtTjM3N1hByJNMvMT2G6dAsxSkZP8u5oouP4eWkJ+yVTJ9j+jDT//qpNwOqa2Jhmsp+5hqHj7q28gHcztlISwZD3M0HPX7u5D8bC9gNMCGczPQspnM4m3LVRFwBaKOYHs70fP43tP8/mf6NA5grwPJykrAErEKCFdIAH34NhccxsGotslXK7h8ecxil9n6d2v9zFylRgccNZvqu9h/2JzG9lnd0kYJ+fxDXq5i+wvROpk95LOdzrKD5BT6Ou4mD1nqm7zF9kelCbvaKFrCmrzO9mLNZkXIgV7T/80xv5e9BmMhI94IxqxBFGViFRswJzGoZmE/k4DMoPHq+vgDgba5zZlfh0Ufq7Crj5JshfOZ9Kg9gZcfgnuQz/iQP1zUX4VVgthjOdC7Ty3kbvcuBvULgfU5h+ibTn+cBrMyTx/FMX2Z6Pzc5JWCVDrNiYDX3OgqPmsvAqjE4ddMM1fNS9Ryux4LlyXcFk+M1pl8swlOewPR1pheUWM+BufUzpouYHpJjWXBS3s70Cc5ACynnMn0DzFUCVtBFz04QpuicK4IHVmlJsipW1TLQutKYGLXcnGpavJWzq6FpdjWXs5zhRXxKmEH3Mr2+BHvRNKYvMf1sDs/+CNNvFvEZhnLWeKkErICzKyzrh4bP5iZXQHGVmYbh4VMpPPEUfRuN/4I0UqLlFBl/SppdjSTDAd4jII/6Yz7jl5qg/f7hw5TGCt9fyXCuF1uANb/OFbQkYOVTsDm5sq+eqSDwZJAxrcjUL5FS3d8wY32xtbgepa/0HgHmBj/G3Uz7B+xR76Tsq5JBFPiyHiQj5MOtwOl9YsCe41c5sEUJWPlnWSUS4q2bhjUMtM4xMkT4soATpPafQEoYpqXeKY8P4JPWijBNimha/cTld8GqrgjiNM70HjLCUiRgScmBZTFiFR57HGNJI42j4732REWhUL8D079eFuBHPZOMWK5SlP8hI7gzmwl5W4CfAauG10rAkpIjG8RexBhFJp7GTESvZqzGbMoyUnoMgv9qGPvgiAA/KVbgJpboW0Ls5NeyfOdCym/YgghBsGk/CVhScmdZY44mtZdHloXVxXCMAV4PYNccwQwGLnzRqxbTS/g1neTQvvB1XSL4fok8tH8v8hHqIAFLyv4sq7zMO8tKJUkt78UAqycA61CBNfotGUv7E8iIqVopqNwJJfyWnBjiPP53EYIZ6wdk7PUcT0Y82N8EPscJfuillHyKGsqIbeKRmkFnWXH4so6m+PLHSKvf5HJztGakaoZqwoITERJxccbv2LoCFEVcUq5h+YUwmbAxeB0ZDubRJDa84zNkvV/vBIH3wIreDzN+30CGDw2b1McJKH8GGdkgXG9Uk4CVu6SX1CJ8EGDmhud5GGMdFVprfZVS1bMHJVMRNvA1NpgR6LSV6Sdk7BtDp9vkUD7K/TGf4bDkiM2zdXzwfsB0KRlZC5ze8Y/I2J6Sznawh9dhFa/Dcj6bmljW56nt1dtIcZvNAXnjFf1+IsIGUNc7LT5fwnQbeVvet5J+Begbv6NO5zfaBBuNb6b9U7b4ETvH+zSB9X/U4jP0wdcEARa2a43wwpolYOUuoMu38pkN2QM603GwgR5f9oieTVQdNL5zX92+JAvg8xzv2G/b0PJXmX7L5v7Y8b+Q6W/I2HBq5X/A9o7rHIBhOaf6ML92d7KsYxjLetwVy0JIAxzuSlipZtf2FNCu23i9zLKXg22ugNWT+4Fa8tg3MsfXRt5P4Ib5qSCz0CyVJC7GrJEzKitZI+ge6UneNWBJH1bucgYHkwlkzh2kqKS1NWyML33oRQYLcQuwSg+cs/msdY3NPRaQsYnUSpBP6hQOer8na2fs00wfsrle4ewLLAwpWQ43syxKeQpxqOaaq+wg63QraEERG5QrBTEdr/IAiUkjM5CMXGGZ0pv3JxFSx5m4lewW2B6eTHMJWOJlBdM/Mf2G7mfQaKLSc9ixDLveyOK+Qo6hn/PrrOQuyn5WMpaK77N5r7+h7A60MRwcp6RZVmjkXFKq+nkJgC3nz5KrtDvUV0Q0bkxQPb3KVm7O5yrpBIfZPvMrzWSfjE9k2htPjFACljj5OxkbfWEiIk7mHkq2L1Z7Dd8b1aPH9RxHbuQWsk7H8S73OWWTL5DhGDXLYpcDBYztdr1vgGVV1ZDaZ4yXEIeooH7lVEaToPKLkVc0ydmLCIZYYfHuRI3pZspMDrT/ZCJKBkrAKqyABWALxFncV2S8TE3TDw+NzjiPlIpKcKPlLstDp7NKwwLE+MBlGZdZDMZWD9fPI+7U1c9p7TWKPU7SS3uIWAq1Ss8r+r0VS0Skb4xatE+hNpmLzG/oyYSVgJW7wHf0q/3hpZmiB59N4XHz0oeHbvdQ5jybz92WAbCxSufyqYcOObPjt3AxXD36puneXbTPpASUYTUxCEwbS4ks7EuU1Hr5sgSs3OUtK7AKjTqCorMuyMzb7qWTDrOZxdyyghgvwywtHuvA7+p5fImYgcGwJuWx33bFvi/yNJEWyq8P0RfISsAS3fGxvF87jGJHXGPEi2r7DEAvdN/Kv9Irx44QLkDnb80yO3uRsx0GTIr/9KMpgXUsligWE0MpbuiOeemXMg5LtGME5w2Onk9KdbX5oAkvcUNxm1lsmMcycvEX7PE+hFQMISzZN5EY/9M5ZMSHPWf6/GI+AaRyGOxJj2a6SBFhY8ctQLcUx3OYT84Jt1+WIvwVVFgNJS+Rwbss6Dic8V72iFnFynhZQt64DxBlQwB8p7UeMFDH4GAXq72IeCCwvIc408oErfUl3kNErE420/6rpVoJtoWnOkuTUDhRV0kps7T+DvZQilUkMQDPbfbO9CGXZhNxhIc6fNTxv0QLZXVLIUi2cSdpSf1MoA0CWxROWeQDv47Erk4VE6xqBJTTSMU54SdA/hcpueMVAKuqv3neAIJN9VDMuxafHeZhwK63MHeQX32Qy+txrR7zBT9cqn6Tnpwvq5WF0Af9lCB9f6JIAdNCfNoLJHavXDGkhnLfVgTZZmFGdfnxLAFLKLnVSImUk1LRx7ywNsUDWEDesfjsKA/XY0+i2Qc2w4MLAIBpBDemNOMAjWyApYYo1bybtNY6ANZreWrho8k4sgt78vqUaC+ZLKju6yw+S3X1ISYBSyhgsf4SqyKlvNbMsI71UApMOXOQKTILzPJQxkKLz470cP3LadJE8QQz9bbrZytmoZZ6OIfWtBPW4SISE81tJXBYY+8mIve/EvAeYd7aApP+B4LKfq87DjEJWEIBK6mfkqP7sLR92thLjiIwCPNeLaQbduvEhl/jVdNn2MIx1+X1KW566TXXWsCa9hh5vbKZhMl23Xxk/8XG5afz3NojyNg3CWf8wQHtEci8+lUy/G8PcOY6X1DZb0jAkpIjwUqQqqdYoUzAQmZIL/6rZyw+O8WjOWheRUOyN7cHmS7jqpMmrWEr4wkuTMI02m3rIId3UWFWrY7jIH8DBS8OCYdd/JEM/9uXSIzvCrLSxm0gAUuKF8TSSOm5Hy6cRu6XscGsXjJ9hlWyoz3U4gmLzz7v4fonKe3/UuFw/5RvfHYBWGqYUjs+Iq0du6bpTfbJYwVqeTDIG5n+m+nsbtDTMBkU87BLkeFQlcW6cbcXhZlNau9Rmae9Y3XrDA9FYMBtNn02n9yHMyBUdYHpM7COk1xenzSDjLZnIyFNqit+pUYotedT0urWk9J3JNaw/pcM/111gV4Bco7/i4zcXsjSmuiC3Qzm+j1FrsMmPjHmyqAV3t9dLxZIwBLIrihSQWr1gMzmx2zv5bCDf1h8drqH67E6t9r0GfxfI11eD0f2+50mrsGwFNUlQWRmI1YUExv/S9EBI5FSB6ER1xR4gCHC/iYyNpBfRO5S8pSK4Nj5S4rMriBLPbJ2aRIGTrCHsLK3OQbrfzyUgMj0Z02fwdF+jMcObZZzPFz/UMesibmvvZ1SDVtcONwzMCsUpcTHL5HW0pruXfcy/VkR3giYKRYfTi3xnoWU0FjAwJFY8IPVd+dhJgFLIGCptcPY/B5LD3mkRvHiLAdY7bAYdG4PS9hN+6/M9SP3Z78hcvrxzJ6RatxOWiOrkuKBiIeYWbhrLSVWvURK5/bpb3PWU2jpz03c75RQT0py18AP+GQ1jpv0/5SDTAKWQIswRWrNEGPbnQFY6GR9c2RHp3m4foEF4AEw3WZ4QOzVxk6mBP/VBtLiza5XCDuvjRhHhHWyLMiNZOwJ3FXgV4PK/4QMR7VSImNyFHclYMKJy9ElASsPw0Ilpdc+e5O/7OFqRC0vzNEcfMDiMy912A8w9RXClI/UR8wsTO1ep5uGyr5JapB54XDaP06sEALfzz0lAFqoHzapI+vsg2ScEfAL8phKWAKWFCd+xQZmGak9R6S35BxE6dNn3AlMMfNG1iM8mIMfWYAAtgO5XeLHwQgv7DNksIdw12ojC4OfUcdASz8irKXN3MswAI/mJmKh2cMFZOSrLyUBS7+aKcJEPisBS0ruwliIUlZjONwNwMJBEG4T4MGAtFodPNlDDXBCsnnlCM52t95y+L72yX+lJVKk7YXD3WcX6fBlvWDlAotzExHbhZYU+G1dSYbzutRkKJ/YjpOAJSVHgpXeklMJ+MHw9BKKgNBw8wGqVeQ+WBQ22yOmzxB75SX+6yFzr9Ca95DWuM2bw30/KzmsHySrtbbZ9TSEYWDL0E8KzLZwnFohNk/v4OxXVKJAhGzgfMph3XWoScASgVdgWDWDmRmEvSw0nYztOG4Fq1jmAMfPkPtke2AoyyzMSbfJ/pB76z9mLwrASmvd659hwTZmGj6QWTHhsFOIIUxhBJjO52ZPIQRbZC4qwH1gfiI7w3gyDvXAYSW5ZlTozwFXApYUv4ilGSENhjv3RHLv2EXnXWDxuRdfxZO0Pxyc5eF6LJe37suMWMX2bmLEsZ18+agBVkgVPetiis74Ivs95CYm+nUOtN8jMecOZhOcHVmR53skuKm+i4MxjoO7S0C52KN4qAQsKT5tH4WU6oGcm3ha2UMU9vumz0IezEEAnjnfeY1HP8dTlphTt97fxgucx8iALjrrIopOP4M0p/Ob9xekY8G2GixYLMzzW0P0/2H57hlZzW//8sXuONTk1hwB7EoJl5HaYyCIBcy4SR6uxqA0+27GchPCrTlnPhx1DrnPCrDZ0gzTjKBRr/FX+qWJFv3w2Mj0M72CVaYs5aB9LRkBlPnKwoC0Py8UuMesJWOBozbHciYVsdejf15CuR+aC8KEzKk/IXkIRSF5KmvGiH4QyhSPZsYii8/gy3G7wriE9k8Sd7yH+yOnUoOZEyDTMdLKeA5pSMZJrRlKkSlfyDyP0a9gMeGnZGxmvpvykxp5FnUEcRRMsLVmtwDA6leEuqcF/tFLBZWFkJqfuQUsaRLmzLCSpFT0JLVS3/R8oEf/xjKLz72kkllqYU7O9XD9q1ZGjNbaQFrzLk97CPWmSMUpMvFUUsrLRCbrBQOcR8aeRNECNtu3wD2mjfwcoba/IC1LtEi9XmQWjAZpEhZSlBAb3LspVb+e1P5jHmIDFYwg23BV+EtfYfG375Phx8k2c6rcvMiUoXwQupW37ZgSnOaeHO568sLBFB57HNjVidw0zeWEYNz8d2ScwINO/XUO8LeTmGOyiLMcJDAr5PmEeK8iTrsp49rWnYabBKycAYuNq/Ymir//MJUd891PWW/8NMcSP8iRMbj19yDn+horGNQat5LW1ugtpEFLkRKr1lPssCGJPYwXCGjdZ2nfI8MQFoDsBX8iMVtsFA5YiwvdawSUESGxR9OXhvdFIo4I2C+n5LrXKLltDSnRotbEy0GrcLjvtGZLSTIf++MWtPh1LYKex4qhIXDycYFtNqQI70kEK4oV0SSUgFXyLCuZoPbXf0laS4N33ipuO24vD9/dam+yBT6pwe8FltW3CPUX4QMKdcfxKwFLGMsqo+SW96n1uRvY/OkStBgupLMZ6D8L+zYaLasEk7Bhi+5ADzBwvU3ijhHrVaI9zsrH2VKCzwG26XpblgQsgf1HiVZSctNSA7Ra9xrmYdhadYBKtVL8g2eo9cnLqH3xX9g1e4zPw86qf8fa7bw9xw7PD57YUJzFcvcCU/YTQWVVd6FO2Fawzi5OAFYyp3vRrEMGWikwrSevIaVHP/3kZGtgUPRsnsmdRgqX5NYP9MwG+hYfzaE/IEiqrJaih15Aak1fIzizU97JZaACYJPrllJ8+ZN6upxcmqEAAwY+uKkCygp1oe7XWKD7lBerzhKw8mQepvYwlrJ7nf3QxZBTQwwYKjpGuA5g9VuyDnctmdDP/4vOuYzCo2bpGMa9Udjmg7glN/vMRpDhuG1L94TUzg3U+tIPjZCGUE4LUIU4Ml1UKILWhXreHoFlVZB9YKpIZ7+nHPUSsPIlGPBeBz0i5tXsr0QJg53tpLbnb6Dk2GMoMuNcxsz645SaBAMu5C9/2YW5j+V8hEEs07tlMk5tr97GzFLWfyI5T6ANBQCTVtnJLAd/UhBrVAs0Ie0RVSkpgRXNAEM1QvEV/6TWxy6n9jf+RFrTTlJitJB11/NdDGggo54zHj6x+HuPUnLTu/7BirHFVPMu/Wh71qtEmSaFOMlZ6UIdo07gZKE6tI3I97JDAlZ3EWSJiFaS1tZA7W//mQHXJQy47gNw3adEaR7T5xksaQ5d7yL2t36pxnpKfPBEjn4rdgOsLOqrizbxXd6lEIGRXYmp1ZG41dMqh/avElhnT4HW0iR0a+HZR30Xf2sEfGEArta9DLjup8RHz1FoyPQ31YGTjld7jxmnVg04kcJlhyuREExAHD9WyfAlwnQARen25KY1X0o17mCA5WribHQmK3o7bRX0ZE7HmPcWhPkdJklYLXm6BfDFqcwjBbW93dYfkaEg68MeaFN4z95GiUZuRmlzC+vclt15Pu8oxV9tUsMMuBilaqmn+MpniVY+qzEQalHKezYpsap/K1X9P2TmZB9KpfoqlX1qmFYxfDk4uXn5rYoadkPN4bsYntVcNQaNJsDcsjspBm09MdfmwuuMJ2l7PR+SCfZ0Sa3kQQsnf4vI8wVQQhppK6f4CHG+DVq3h42e9DvAAnlV1Jg8LAFr5qnfkGjkZupqa6eqCkuT6SiuwREwLrWiowfAt6UfiLrj4w4mpAF7ePiEEoqMp1CMclowQ8RpexOl9m6hcO2AT7WkbprkOhMjVfTdFp8j3fCBuTZTGbMv3ttKm+fd3/lZPRs8kdIOdHhPUDnoEDgbcY3F30SEk2DCgOtg3YULYMFwcyVB9PuTiWbbbJgKb92xS6KRGwxgJmFILUGXH2hE+iCJjFVLxZoZ5ThfpoxMD4YPa60AwMLJQciRv8TEuu4kAf5XtEF7itZub8o0/UueYYk8hQjHi71icgN8SRRgMWGMn+p2ZcTntzDAanfI8RGOhKUbS4ogfp/C0WCbDAJnbJ+ZkWOROEwWIRqPkhHZjqR1yAQxVFCVGxg4rQl3raUnxOLBhzhAQFnIsf9fMrJmYPURmUZPJ3GLda/oIJRRWiTLhCHRKg+STBphKqFQfkaCCOdQniCLtJYOH/aLJOZkGuSo/1qeKox8ZFu6WPeDz2kheTuIxEkmcBU+TMhH1g0Z1iDU+lKopbWNqqvKqaqynOobGqmhsZniiYSdw96zoKwwN03TwJhrndvb49TU3MK0dR/Fs3grK6QfUc8z0yCR4faAv7IXqWtFuqflzyVQR5zWtMzrRZJhCRQ45o+cPY1+edM3qa6+gdZu2Exbt++mX//5Mdq8dQdFo/7DijRNozYGLAcMH0zDh/SneDxJq9ZupO276iiWQ7nt8QRNnTiWRg0bRKlUah8gq29ookVvveee0SF4tH4jCoW/rI5d+AD3gwRRkOLlb120KwKI3yJj0SKIgpnw+34ulAxLoMAxv33nHlr+0Tpa9tFaen3xMho0oC899MsbGOuqpGTKHyPSOIAcfdgMGjFkIL2x5AN6f+UamjhuJE05aPQ+QOMHCCsryql3z5r9tLaHp/hAhUJh0hq2MZa1Jh3k8fMAs6y/k3HqdlcUgPG3A8webyCfq5kSsARKNBKmFas/obMvv5Eu//7tdMcfH6YzL/4exWJRmjVtArW1tfsqN8VMv769a3VweXHRYh2gWlpa6V9vLKUBfXsx87NC/5tfwCpj9athgNojQ2uqK3Ug84arCmnxFkqu/w9S3RP3D10SwFeFen2ni3fHhUxvCWC97s9aL0UClgjZ62bww+yrZgBSw9jJgH69af6sadSzppo2bdtJoZD/AB8wLJh/KS1F8USSEgzE2uNx3fTMJdwi7cNqaG7Rg2M7tKlFB0UPonvblXCMEqtfoVRDfbp3Pcpn+6AInNJwSH/aDfrsd5n+NUD1QV0utAWpdC44h/Tc0oflXhBBjBHsuOEOzurvXfEVOvNz8ynJgKW8PEZ3/vEf9P6K1VReFvN9c4AK/EwoQ+UABYAEw1q5ZoN/M5b7qrYwQE1lsDQA2d5GTyfGG4dnqGFK1X9KybWvUmTySel8XTh3DoXdTsU9OAER+OeQ1fFmXVMw8s/jPqOvFrkucA9ct5+Zqhphglpco+Qni6l12TOUmnkG0fBJErByFKDCu2REWdsKAOUvjz1PC156Xf8dDGjj5u1UUe4frBAegS1Ury1+n8LwE/F3DlCB4x3Ofr+rkBFmxr634mN6e9lKCzBT2fNE3TjcAUv/7gA7NaKzrPD4E43AVaO6d5ERRvBLys8yeTZ5mukVTNd1+Z66bxYrRPIiLAQrcj8k5/2Z+ZokriHDZ7g/ULW2UHzFS5T46HlKbl9JiZYWouTJkmEJmq3+mA2wVFUh7B7AqqDxu5oTWGVKQ2OLZedUcwiZ0M3YSETXHATHva/qRNiIEd7QXE9Kj1oDzgxBoOBspt8iw7fVuwDvDcwPR6E/uO8swN+o5sWD4gs6PHpp3JNj69mN9A2RSkQxEjt2WldgtwjCvZmM4Nt8C+5+H9Mb9zG/VSOdkdbSyoDqRYovf4JSu9aySS6sJ75EvZ2aR/qwvMkDnGU5SjgU0n1Z0HBY3MY0gOF+qhQ9hBRwdMO+w5F1qzibORf/nrS6TcjRlbk1HL7AH5Bx9Dxm/DV5mlxe4+bQIR1gpVBHXnwNmV1VyylbLQBgqYIAS9kHCHDg9tYPqPWJi6n9HUZo2huNtu+8G6LgTyVjw/7fyYVf1oegzL+QcZDuBR1ghfqhLgkGVO8voJbHL6W2f99G2p6NeqYRCrub1JU+B39OwpA3wT4qBEXWyKbQ5Sqmd1iyt3gzG0Q1FB57tH6EvdpzsPF5Z0pnCGInsAUEHRFZBkaTvwRxu7jZAxaBoMR3OiAjbX4k2IDevoINmEcpueEtCo+YTeEpZ1Co32jDz2scvoVN1b0o95AA3PkT9u/m9FZO3UVo3OMgMrYd5XKPpD55hqgd5af27qLEhwsYY3lKP8wEzFmtHUqRCSdReNzxpFRW6zBuYl3IvHEsV+zZHEb+so5s4e2Ndn+G6foOkAoZLYH6JdcupMTK54xzDPSMvPtmWkZjtLa20YI/3ELzZ0+TgCVQMMCw4jGoG7cB+td3ubnl8K0kA65WUip6UnjIdAqNmENqv/GkVPdLhz4YYGGYZyGm6dTNY8g45BR74qo5F0rHsMKJvIf7R9br5qhCSEWxPc07FDUNmilK1a2n1LZlxmG3W5Yz4GL1YeaHXq9oOYVHzaXw5NMp1He0fRZznxxIa09Rcs2/KPnpEgofdCKFBo13vEcGqO0PfyHDJZhZvgFUT1NixT8p1biNPVc5T7Ot6RvRtWS7DlyhYZ9hbT+bPSPD47IK4/773ge+rZF8wsCBvJhd+vIJxdz2WGndSMb+zhV62ytU1zE5KPydtjRQcttyPcwluWExq+tWfRXZLnW4BKz8yghun59OYjMwloIs4ubci+7hLaUDBfq8Ul7LBs4YUvuOY6xrJCk9BpFSxQAsypoxHO4AG1eeHj7wwZ6IDU6kaNaad1Fq91pK7VrNGNUqfdVSa2PmkcpGPNLoZI561EsHrgoKj55HoeGzONIIAKu2vQaQACRTST1BYmjUYTpAGpRvf3BXKnqTOvAg49n50ZD6VxkzwmlMSNKoIz3AaufHRvkNW3UAtj0PIJVg7dOmsxqlegCpvUdRZBJjlkMnGqu4WgYgquR+s2pm2zM2rbXU6aCJeqW2Ltd9U1rjduPZAVRqKGtxErDyL5iNZnNGEKWuuTdN4RwIR2thy8fi3LhZygAXdGSAB5ytzHRUYlVsQPXnztcKUmqGODSnop/uo59OpBgnDmEwY9AQA6AOcAyF9dz3lM3Xhzol+N7JzpXNnFoMZhmOcOvwz4A+6ffQbO6R0s2k0LBDKQJTddAk/RKYr/H3HqHU5ncRRdyBJshppsCsUt2unWn69Tp4MfCMHHIeRSafwcBaZcDFStu6jBKr/6UDnMJYme1xc3rmwxbW9hv1/+ug1FzH2r9e/1xjzA77SikU9lA3CVhSSsa41AzAgGJA6V1Xcz6fsaMHq3wMqcb/1bTdpJRuWwBsGRsKjZyjA1xy/ZvG55GYuOfSmWULhYfPJHXwJHYPZrJt+5CBWTsHU49tr3J6pvhfT0gD1j//dCvNnXmw5Xe6U1gD3vY4pmiJBeScrB/2/Le4qVdMxhQKwMjT8l4PPclgyLgNzBZHQyswK9v5yEuqsra4hyLlbwMwkqsXGm0T7gCqo8mI0s+1T25jwHKTEq2MJze+TYn1/+VhBVHGtiJY3ftqgfo96GIzGftNPyUjNObjppbWOgRgdzeGBdDBig8OFcVLmMEdigp3Lq53uBaOXgSKRkiKlMIKMnrabafB/sefCLgHTPsDyPrEIORM/23xCKa2pXfPmrej0chT7NenyLR5visxLIQZICPiTA5Q08j69JC9lLmobs8q8L3ecvxIKbA47ZCPC7oH+rbdhr1EMR9eUZSB23fVnZRKaSeREeT6e6a/IH4YRikDFsAEJ6fM4gAFU2+Iy2uz5WMJkukhpXuJlqVfirqHw2pGcQWB19zgRv5+5M3Cgb9fZrq0lACrP9MpZKzIQSfzz0S+rEC9OCkSsPLUJ53GQBAnauw9RUDqkUEGrCGcNR3GzbyJgky0lEuGJQFLimRYwRGwrTuCAlhopBFk+J3mcDMP/qgeRa6TwG7Il+2DFqbVVaLGFCXnZXX7NtKMyE3NZpyn7yvm3oVgWCnBDAt+r3oXJCBTQj7G91HFAixUFqsUMziDwkoeVvQqcigTaUMQ0PgZcj6ZVisYw9KDEVuNjhyt0gMjzfunAjHQhe5HETFP+Gj6VNwIXGxr4DFLZbm/Qn1rS1zfvqNU9GFlVmSEglNnuyXajNOCcO/MQNFgMyyRoIhxd0qWa1XT/bGf8mWPz1SwQwnxBrE/7BDOoAAqY/jnvroSGbv8cfYdduUj+hoJnZAk7iUSc5S2ktOlyTbWvOUUPnA+hYbP1regUKyHnisqeIAVqAr5q5MOWHsotXs1JVYvpOQnrxtlqT7DpbBdp+dwih54PKmDp5JSOUCPDt8nQl3h7ItvCUpu/1DP66RHpId9B3mWog8L4fs7PF7jx72TyhdgIQZqHAemw6gzBspvsB0a5GMTQK3in5slmsPLEsOwWAdWaoZS7MhvU2jAWCNoO4DWYJcyCbEHMcYmiF4DKTz6cIqvXEjti27Tt5l4NtUYYwqNOpxi865hzKqqM8OBZgEb+h48xsBigynSezCFxx1N8bf+TPGlD/pl08X2YakF6kVRH2NMEwVYPTjFm8UBahpnOX4HPaJfP2L6JgeotzmjchMjIuqlKr77ApuFo4ddSaFBY9msT1IKCLwa32oXGT+PUhvfpPiqFwxm5MWMj1ZQdOZFRGUMrNo83jukUGTauZRct8jYaxfyzKhTJWgS+jm2yQ8p8A1YyBdkjoHK5fhwBLJ9mAFQ73CflOazIbI1brZy/W9GQ8lqmNTynk659KUUAsO0HOgjjk7zO3RhNiIpnXj6KtIkFAmKWqGexS1gId5pMgcoxEAhHmpAjo2GzJ1IrQtnw1Iy8uuIkOLGkShGFoH2t+6l2Pxr9cRp+5kUUvLQ7obqWxJZO8eXvaj7sfS0K57KYd2nvZna37ibvT9mElbV7pv0TnM5DP2DZbIEGVahAMuWYQ3mrGkOByhRMVCZ8m0OWKJFddG4+fVhhWN60rKWx6+k8EHHMdNwOuv4/blpouS5HxTaDAtGHbGaR611lNy5Sk+RktzwZmcGAT/vb8MbxvsbewyFBk/T80gp4YrsqWfSjngtSUXo2yI6VkhweUJNz0zAOpzpyRygEFma7xTAapHKLUykOzJa7t1E7f/5nbEszkxEYoCleBhAmqZRoEFLC1DdsFKH5HYILdCdWDmmYsH7a9zGmPIfGVA9SEpZD/39uc2VpeflCvlaEU4W2SQslA/LN8MCm7qTjMyZBZ+f82AYiPJh5S5IlYJOywa21rTT+CmNt/yZ4pgMIuXiylTDbLIxsgPrYIg4L7dvUE9cJzxkpBAMq1AuFV++MrTqY2SEHxTFoMiPJyNnhqWIH0zyRLUSRkP/8Vxi2UpQfVhJn2PVc73VIoGVXxrpphFCObwsL8AnRUoRkLNbM6wULrqpCwGWm4Zwy7AkYEkphnSXVUJf9UYD3EiG/+o/JC5BWDEZlqhVQilSgiZqAcZeoVYJffuwII9xxepgOmMn9v3lst8vm7TkCbAUFy9LMiwpQZVi+7AKRT6ExGF9wPUPZASVmjMqYH9ghaCHRJwX9gfuDqhJKEVKMaRQ6WVEMqyiAVamYN/eR1wfpM7DG9I5q8DEcslZ9Vtujr7PzVFEvOOo8W0BMAklw5IiGVZ+y8s5cNRNpdZyfYR/hv2DUzMADBHxvTyUOYDrsfz3XRy00gD2HhnHkYs0Cd00rvRhSZEMq8QYlhvZyPWpDAAy513v56E8bP+ZxxWyh4xN0f+lzk3R63MELDcpkiXDkiIZVn7Ly8npLkq2cn2e/96H6STqdOTDbzXYQ3m1GeB3NdNGpivIyOoABraEjLQz6ZfcKhmWlC7MsAqxSujnHgULHM13+PVOpv/imgYg+L3SaWlgTo7wUB5OYj6E62UcoOBjQ0K/V5kup2Knl5EipbQZlp9+7yccqj2IgGUWmHj/4YrDEZE0CIn/sAKJlcjpTEd5sKPLuAkKvZA3nJrjC5OAJSWoDKsQPiw/oIix+2cP1yoeLa2iAZZZkIP9ba53cQBC7vd0auV0LJjbbe9uvpckGTgqpXsDluh7jCTrU9ZFy7tB25ELE+99rr/nAIRc8OlYMAAZYsHK81wPya6kBNEkVEroHqIFadNvCHoKAZh4K7j+hTOfkdx0TAezwqSs9viyJMOSElR2VYrHfOVbkEL9K0zfLLWcJwCbNVwf5p8No85YMDjzsb2oZw6dQjIsKcUErEKwn2QJ9X1kJUZ2YqRUp3AymSz1l7yB65P890FkxH+lAQz/75vx/ZBkWFICKtn6nahDLcM+/1YAUSieSOxIaRoOWf0TmdKoh3tUVXoqLsqGe0gNdOLezVyf478DrBALhliuuS5nEaB4gwv6LOD1sJuxO7Qnsn4tyPmSg1o3N6d8B6nNUFen5X7sBNme4zMBFJ22v9ULuIfb94KfSIJQx3Q9++XDeDzx5g+uPG/JhHEjt1sOhM3bdnq607o97Ina2FN3Dvlyzmpq+cBCBXbwhw6iIJarKcsAwyGPvQtRmRSrRW2MIWp/x6/hdLxWmzpHKf+LEH4BC8Df6MAWKgrRxHkuv8nBxCoj+4N97frfHofywC5iOU4OCgdFu/dSweut5bu/IDt+PEXtLXHqOL0T5xgM6t/HvvJez2776tNE/1zNWi6iH1pxARmHVwyizjQ0Gm90BHQuYHofZzz5Egz1m8g6dgu+rp8G2QZIsOE0kEHosQc4fu0EpifaMEME5f4joI8H/+J3bP6Go93u7QJm3FVkhN5YyQNkxBxKsZqFmVUxbSDRWQd5QFuvgPXlJ0h95mP6RVWUvunyEtDPa8jI+JAPQZjDSpu/YSP15KC/uCQDrRZnk/AWptfZ/A12/tcC+miYzF61+Ru2bx3fBcYd9rdOtfnblUx/KaHJWhqY3XDWRKK/nOr+Gs8ONsYIfhhS6JselhL685kGea+ezRPlj5O1Q7KxFF5cWCWqjjp+xenA+5YCVxfphS63+durHEAzTUI7aS5ikyPLyM02f/vIIyt36mPtJMXRNi33iECevv5JPQ1bsoWuiflbR7g+T4AlpbAC4/U8m79FTYAVVOnj8AxLgu5G6M7iCXpe30BH7GiiWA/rpMmrmT7D9H/I2mGdzpWVLcMoSkcgqMpn4VxYUiYRVPn9YQPv8lFOLWdxMN728p9+BIO6B68PnOn1BZjIavh9wUYb+H39ihOja/L5biK8fdGmdT76cA/+M726m43ZtPpkTIWQGj4G3D6LnUR4u4QElGVVdsQHS+7F+/1u8rkY4gmw1tTRQQ4BATATnuMVutzmRfS3ASy8oLOYnsaBrZaXg86zhpeLSPdNHp+vlYMnHKOnkLE4kE5EeD/Tu8l5NQTR9OeSEVE/gA/6BAc8ZIZAHvxHXHQEdJovMj0zA7jTgLWFjHxfDzn4e/wI/ENfIGNXwIAMwMKCyCreprinl9XcfrwsO6nm5lY9B/VswDeQjKDA43nZ6Sy39/D3bSdou9N5e07ijCkNWHX8+Z7lrog9FoNmqEPZZfzvTSQ+fbed4Bm+zPQIMhYqyjKeBfngEGP4NxcAEeZtcjovszfve4mMdgGpeNDm/ZxBxgKPZjG5/JqMhZJrmZ5PxuHLvyEj28r3bOqzjumPyIhSv4iMbXYqH8d/ZfpzrxO/J8Da3UJ9VHs/0jr+/x0OncwqnTI2OP+W+0bMAuAawl/kVbxz3+ehysj8gFWasabPe/P7zuaNmbCYQe5geomDSTGOd4xv8Zfxls13R3BwnGvzd4D4wbwMdMpLcxwoaLM/Mv28wyw3ioMEOt83qTODbDYBwM1x+Ds6/MlMb+Cd0Ummc6AeZvq8L3Vm7rBa2MG7vNehPXvzgfFZMpzeX6F9V+p+ysHBTtAvVvIBdWEBwAq+tOvI+rCX9LOczPs/+sjrNuVM4EA/26HPYjXzc0yv4BPxYtN30KZ2Czj/4O1+QQawp/vv+TbXoHyEEX3Hog/+hIwsK1/ywrY8RXQ3ttsCnJrxAGoWKpkpYC4v2YCV1ewO/8g3PFR5mAVYZco5XM3yWwewMgtWiF7gjW/V4Z51GFxmActE9tZc4pPucQArsyDFx9+ZHuXy+5XkfIpSmNfdzUlLYy3AKlMANseaPsM+0pc9tCcG+9N8cslkUGVZxkRFlu+Ikh8z/b7L9prIWfGhNm35kgNYmeVAzrRGujSVWzjAXWAiKUTOCysHkX1YS7q/H+ulwTwBVmXEkb4dwX/C3HqDOvNeZeoeE3j9juwPsbDzldzGdLiHasezmH2fNf1+tMMs02ZTVg1/FjOgf4d3Diuxo/dzOJP0I+O5SWAl7Tb0W+UDx01fcBugGnX5vWy+NHPYw08547Zi+HZl9eSgQCZmkE3yDViTHAZz3AYIqviEZAa4W21MdafIeTDZ/3VZV9zvbB/PWMXv78SgjsobYPWvoq0p+6F/E/cT/YUj/RwLXZ7x/YNtWAlxJoXBt8zibxXcFHMj8EN8xjQzmMX8or9iZxGTEdP1A5u/H5oB2ul6nmXz3Zf48y20+ftFHAS9yiE2n2/jTPBUG8A9kNxF9t/N/XZ28j4HlX+5KGsbb8/vOnxnoMmksYvbuoz3JTt/3OEZAPQwN5ntZCN/hofzDFjnkHUgcDuv71dtrkObzcv4HX7ZY2y+ewG3XuxcDPNcuoX87q3dxpnhlQ7fGZo3wBrfl5YoiqPv5AnONAa4KM4poBN2+icWNnZaZrmsMvxq2OX9Cjlvd0hLiPtOrAT1WUXOkcuHZ/x/jA0bSNv2OExjqYP5O95HB7G7H9LzpA/zaLJhRG7M0N9xc9npvV3vAMSZspy358sO38lMG3SAAxvHc8FZ/56D3y59mhP8df/ncM+P+TPke/eAXR+GFQJ/6CIHZnKUqZ+VO7RL+qxRO39fbR6fcTVvzzcswYehz542b8cEegKsucNo4cha2hp3dpF9nYxYlnOzFDfI4W+JLDa1W5MwbVZqZL8aETF17AEOJhVlMS8zZ4sRWczUzJ92/iWvUpbFlAvZdO4yD32hUoApmGkSO/WmiMmEcVum1fNX2wChlR8u3xJxeL+JjDokHHxDmZOblWgZ79SuXSrIWy45r9jhFD6i08tEylsGCk8vp1c51U/oRzevr6e7I2pWMLqfU3iEOOzy2PG1LI2SXsbOtiSayBgUqSz3Sndku3q5cTJWmmYvO0l6LMut2PkrJnLT+C1uDvWwuK6BCisJF22gmfwolOXd2PH/EBVmo7WXiaXSYTKD1jn02b4uJ4lklnaJkL+N843kbodFMtukpHjcZO15NtE0PfYCNPQqF18/m/uqvmjhj3JT0T02fomd1Blf4qraLu8XJn8HSebKNkTJWgfww2rltZwBB0k0Ad/bw9/baIfvxAL23IqD+YqFpSqyz39V5qJdUhz00A9HObAkL/30Ae4WWEfuAnwTLli0J/FLf6/mPhHE2mRzDoO+Ytn/SO5L8fIyEaPyU5uX0VbkAVSocrzIIs6WojYD4Q9kxMz8jDoTHpaKwJF/aEbbtvM+AJMIYQ6IXxvv0rwMMlBXuyADmdc+b2qXNt42iI9CEOhlWYDcbbvAD/zlYjdaLvY6DomAA/pXHIycZAD//lzyduhiCxV+c28pCzKvIuD1OofvYAUXiyMLyQjwfLVEnq2O9g3OxQpiOo5uWjd7z5mukl0ml8uQjHaZIvCeLwftwf3wWLCso/iMkG0fWfoEZyn5FaxwuUlpMo+zlh9RaeWwRwgG/HAfc4Y/rRu+Yyt2Bj8lzgbEyustgsEqbXYXXbwzLOuufQefsf+QpQMdxc0WKfkTmMtXcvb7Ywf/RXrCQvBgOTfzgy5n8j5mtbKFsIbWPAzUQgtil07hz4LVPSs/rXlxBQkHEG5i5cj/kPeJiTnWq7XkAOu5NXTaog10vU0OGzhzETj5DNlvnfAaW4TVrMmmWUXhjfcOBTdfdxAEW26e40AEAHPyNV7FWcvyAD8P6n+XDVh9wt0SP+4CgNXGTV/0d/imhtqwnXQiAKyY/9oGrD7m7fJLAYAViJz9ngBrexMN3N1CM2qt11sQV4JASATlvWBTRD+P9Zti42NBNPIYyp/j3a8oASgr8zpkTYCfCht5sRPhiw7XHRtwwMLmXrtYLCzMIA13VReYaBTOeJv5WDraBojSe2QnO0xG/8cZW6WAesVLDrBCCiVD9sMo7UxfTfYrVV5jPjSHWSiI7CpZ5LLQuW+3aDfM2Ngm9DQ3qazezaiAD2SnyW5ZkFiAwAmp2cXnTu2yXGC7lB7D0pwZjZJh67ZR9vgON4GKdosCLeQ/gV62WSRJ/ldPm0ygmos0+bimlw31T4Mf4miwgjjJ4juxAg1Cv+K0QNQi8P4in8FuhTtB7pLp2T1zysV3NOr0O3WZg4E9DcyaMtqh2j96GqBCDp0/03G32UW97ECvPk+I38BnL6v6h0w/La3mjP875bSKuGj/nT7q3+yCrdkl1hORjTKfsU5ujlcPF3pMZJHjTeYYkjU+zscB/FDDbPqZ6qL/Z2sXRXC7lB5gDa+hdbEwgt3Jag80EoNhs+X5Dg29JeP/TkGkaee83U7uFXlqjzpu81sddd/ThVn7Ycb/1zt8L71tp9YB2Nf6qL/dAZmZvh+7e24V4MuoLlI/7unx/gnOUqzYiUg/2Fm0b8aOpRywADLwQ03Ocn+7zeyr3HKMIr+X4gLWmF60emQtrVuzm0ZF9+cZSGVxJjmvRr1t8jtssJllLuAv9RSbcv6ap/YAE8HOcqscVogWxmqM3ZFO2F/1iqlTfULWm6BP4ybfyTZlIevBpz7qv5qDrhlw0cZ/4gPV7hS4d3NkcZD5ZAQIY/P7bwS/G6fc91gdxOLMoR5MtYTNxIpkfw/yyecWwa6HzHzxSNNjlSYJK+P38v5h964edtkuP+MkYmpXASxPgaPlYWob1ZPucsjWUJPFJ/OIqeP/yua7eGnXkPWWgkfJXfoSv/Ibh7bC9g+74NefmxgkBsU9Nt/Ftolv8Z9Wps+NOTDER23+dh6fCKzeOczzf7u8xwYHM6SGM+yvuTDjvAomADu/4Gd4e7pdDdvhYLIDxBAlfnWeTVy8p8UOzOx8G98TrsuMOv+I7Bdo5vBxVN4tAUvvgZqefP4+j5e18U78ienzOx0GmJUg9uoiD8+hevwO8U50g8fnQ+6kH1l8jhW7lzyWhTzmr9n4aJz8N2lBIOhbHu4HE+9iyn5oRFpwKMgfXLIwJce+l/kdMM4FDt9NmRi8UzuB6dzhgg1pgseQahoT53gw74gz14ss3sezWczfd1y2i5d+Rnl4x2JNQt25kqBkY7ueDfG/fFYbnaUTvcAB4C2bwYLZ5Ho+aAbalIPBhBWu75H1LvGkzYMnTf/XsnwnLTdzJoHnm5CFbSAL561kHWYBXxSyfCJLKfKDDbCZBFAxAOUPbQZlipx35ZvZA3YUfJvf0y5vOspbxOv2b4/d4FLuLzubP1PM1P7JjHtoDuY3ZWFi5ndzGb/XSRb96JucHU13eN5MuYUD60W8jcptnsGNG0Hz8F2zCY+kj9/l46CfQ/9/kH/Pqv9/g5viJ1gQBbyrvg5moeaynzl97usdN8f1E889he94Pqr+9jcZXDPDJ2b4sJBjaC43kxDHU80fGqtly7lf4T2XRffi1B4O9z688+zlMxC7qx4sakfhD7BB+mbO6iL8O6oNqKx1KBvpcRAGMJg/b4oP1uWcje318HzTuX+sP68TOtTGSIjeqYnps6Ddy+hL9kGTMG22OpjW0zjoDuT3hKm6ns+671FuUsbrVWlq2yZ+D4DASIdB+CkvY5QDy9lg8flMMk7WweIFVlNfJCM9c28bM5t4P2i2mbT78b6b6Zlt5+wl2wAZQe5zbTVbWBlpQZ8/hPutvPR/swkI7cknLmRy+CBL/1lHneEX/ck+j9smG3+Zm/dn+Z12BlWT+1PjeVMs37EYwJIiRYqUYsn/CzAAImPVKLAM/GEAAAAASUVORK5CYII=');

