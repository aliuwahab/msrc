/* mSRC - v0.0.1 - 2019-07-23
 * http://msrcghana.org
 * Copyright (c) 2019 TechMerge Ltd;
 * Licensed MIT
 */
/**
 * Created by Kaygee on 14/05/2015.
 */

var mobileMSRC = angular.module('mobileMsrc',[
    'ngMaterial',
    'ui.router',
    'templates.app'
]);
/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobAttendanceController',
    ['$scope','mobDataService','$stateParams','$location', '$timeout',
        function($scope, mobDataService, $stateParams, $location, $timeout){

            $scope.week_number = $location.search().week_number;
            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('normal_attendanceLoaded', function (evt, data) {
                if (data.code == '200') {
                    $scope.normalAttendanceData = data.data;
                    if (data.data.length > 0 && !$scope.school_in_session) {
                        $scope.school_in_session = data.data[0].school_in_session
                    }
                    if ($scope.loadLevel < 100) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.$on('special_attendanceLoaded', function (evt, data) {
                if (data.code == '200') {
                    $scope.specialAttendanceData = data.data;
                    $scope.selected_school = data.school;
                    if (data.data.length > 0 && !$scope.school_in_session) {
                        $scope.school_in_session = data.data[0].school_in_session
                    }
                    if ($scope.loadLevel < 100) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.getTotal = function (fig1, fig2) {
                return parseInt(fig1) +  parseInt(fig2)

            };

            $scope.loadLevel = 25;
            $timeout(function () {
                if (!$scope.normalAttendanceData  && !$scope.specialAttendanceData) {
                    $scope.loadLevel += 10;
                }
            },500, 8);

            $scope.completed = false;
            $scope.$watch('loadLevel', function (newValue, oldValue) {
                if (parseInt(newValue) == 100) $scope.completed = true
            });



            $scope.showSpecialNeed = false;



        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobCommunityInvolvementController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;

            $scope.$on('community_involvementLoaded', function (evt, commInvolveData) {

                $scope.school = commInvolveData.school;

                $scope.updateLoadLevel(15);

                $scope.commInvolveData = [];
                if (commInvolveData.code === 200 && commInvolveData.data.length > 0) {

                    var communityObject = commInvolveData.data[0];

                    var communityHolder = [];

                    communityHolder.push({
                        name: "PTA Involvement",
                        status: $scope.formatGoodPoorFair[communityObject.pta_involvement]
                    });
                    communityHolder.push({
                        name: "Parents Notified of Student Progress",
                        status: $scope.formatGoodPoorFair[communityObject.parents_notified_of_student_progress]
                    });
                    communityHolder.push({
                        name: "Community Sensitization on School Attendance",
                        status: $scope.formatGoodPoorFair[communityObject.community_sensitization_for_school_attendance]
                    });
                    communityHolder.push({
                        name: "School Management Committees",
                        status: $scope.formatGoodPoorFair[communityObject.school_management_committees]
                    });

                    $scope.commInvolveData = $scope.commInvolveData.concat(communityHolder);

                    $scope.submittedDate = commInvolveData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobEnrollmentController',
    ['$scope','mobDataService', '$stateParams', '$location', '$timeout',
        function($scope, mobDataService, $stateParams, $location, $timeout){

            $scope.week_number = $location.search().week_number;
            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('normal_enrollmentLoaded', function (evt, data) {
                if (data.code == '200') {
                    $scope.normalEnrollmentData = data.data;
                    if (data.data.length > 0 && !$scope.school_in_session) {
                        $scope.school_in_session = data.data[0].school_in_session
                    }
                    if ($scope.loadLevel < 90) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.$on('special_enrollmentLoaded', function (evt, data) {
                if (data.code == '200') {
                    $scope.specialEnrollmentData = data.data;
                    $scope.selected_school = data.school;
                    if (data.data.length > 0 && !$scope.school_in_session) {
                        $scope.school_in_session = data.data[0].school_in_session
                    }
                    if ($scope.loadLevel < 90) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.getTotal = function (fig1, fig2) {
                return parseInt(fig1) +  parseInt(fig2)

            };

            $scope.loadLevel = 25;
            $timeout(function () {
                if (!$scope.normalEnrollmentData  && !$scope.specialEnrollmentData) {
                    $scope.loadLevel += 10;
                }
            },500, 8);

            $scope.completed = false;
            $scope.$watch('loadLevel', function (newValue, oldValue) {
                if (parseInt(newValue) == 100) $scope.completed = true
            });



            $scope.showSpecialNeed = false;
            
            


        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

mobileMSRC.controller('mobErrorController', ['$scope','$mdDialog', function($scope, $mdDialog){

}]);


/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobFurnitureController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('furnitureLoaded', function (evt, furnitureData) {

                $scope.school = furnitureData.school;

                $scope.updateLoadLevel(15);

                $scope.furnitureData = [];
                if (furnitureData.code === 200 && furnitureData.data.length > 0) {

                    var furnitureObject = furnitureData.data[0];

                    var furnitureHolder =[];

                    furnitureHolder.push({
                        name: "Pupils Furniture",
                        status : $scope.formatAdequecy[furnitureObject.pupils_furniture],
                        comment : furnitureObject.comment
                    });
                    furnitureHolder.push({
                        name: "Teacher Tables",
                        status : $scope.formatAdequecy[furnitureObject.teacher_tables],
                        comment : furnitureObject.comment
                    });
                    furnitureHolder.push({
                        name: "Teacher Chairs",
                        status : $scope.formatAdequecy[furnitureObject.teacher_chairs],
                        comment : furnitureObject.comment
                    });
                    furnitureHolder.push({
                        name: "Classrooms Cupboard",
                        status : $scope.formatAdequecy[furnitureObject.classrooms_cupboard],
                        comment : furnitureObject.comment
                    });

                    $scope.furnitureData = $scope.furnitureData.concat(furnitureHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = furnitureData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobGeneralIssuesController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;
            $scope.$on('general_issuesLoaded', function (evt, generalIssuesData) {
                console.log('general_issues : ', generalIssuesData);


                $scope.school = generalIssuesData.school;

                $scope.updateLoadLevel(15);

                $scope.generalIssuesData = [];
                if (generalIssuesData.code === 200 && generalIssuesData.data.length > 0) {

                    var allSchoolGeneralSituationData = generalIssuesData.data;

                    $scope.generalIssueItems = [];/*This is used to track what to show on the html*/

                    $scope.generalIssueItemsObject = {
                        'Inclusive' : [],
                        'Effective Teaching and Learning' : [],
                        'Healthy' : [],
                        'Safe and Protective' : [],
                        'Friendliness to Boys and Girls' : [],
                        'Community Involvement' : []
                    };

                    $scope.generalIssueItemsObject['Effective Teaching and Learning'].push(allSchoolGeneralSituationData[0]);
                    $scope.generalIssueItemsObject['Healthy'].push(allSchoolGeneralSituationData[1]);
                    $scope.generalIssueItemsObject['Effective Teaching and Learning'].push(allSchoolGeneralSituationData[2]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[3]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[4]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[5]);
                    $scope.generalIssueItemsObject['Friendliness to Boys and Girls'].push(allSchoolGeneralSituationData[6]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[7]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[8]);
                    $scope.generalIssueItemsObject['Inclusive'].push(allSchoolGeneralSituationData[9]);
                    $scope.generalIssueItemsObject['Inclusive'].push(allSchoolGeneralSituationData[10]);
                    $scope.generalIssueItemsObject['Community Involvement'].push(allSchoolGeneralSituationData[11]);
                    $scope.generalIssueItemsObject['Healthy'].push(allSchoolGeneralSituationData[12]);
                    $scope.generalIssueItemsObject['Safe and Protective'].push(allSchoolGeneralSituationData[13]);


                    angular.forEach(generalIssuesData.data, function(issueObject, index) {

                        /*This is used to track what to show on the html*/
                        $scope.generalIssueItems.push(issueObject);


                    });

                    $scope.submittedDate = generalIssuesData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobGrantController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('grantLoaded', function (evt, grantData) {
                $scope.school = grantData.school;

                $scope.updateLoadLevel(15);

                $scope.grantData = [];
                if (grantData.code === 200 && grantData.data.length) {
                    $scope.grantData = grantData.data;

                    $scope.submittedDate = grantData.data[0].updated_at;
                    $scope.updateLoadLevel(100);

                }
                if (!grantData.data.length) {
                    $scope.completed = true;
                }
            });

            $scope.formatGrant = {
                school_grant : 'School Grant',
                capitation : 'Capitation'
            };

            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            $scope.doSecondaryAction = function(event) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Secondary Action')
                        .content('Secondary actions can be used for one click actions')
                        .ariaLabel('Secondary click demo')
                        .ok('Neat!')
                        .targetEvent(event)
                );
            };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

mobileMSRC.controller('mobHomeController', ['$scope','$mdDialog', function($scope, $mdDialog){

    $scope.doSecondaryAction = function(event) {
        $mdDialog.show(
            $mdDialog.alert()
                .title('Secondary Action')
                .content('Secondary actions can be used for one click actions')
                .ariaLabel('Secondary click demo')
                .ok('Neat!')
                .targetEvent(event)
        );
    };

}]);


/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobMeetingsController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;

            $scope.$on('meetingsLoaded', function (evt, meetingData) {

                $scope.school = meetingData.school;

                $scope.updateLoadLevel(15);

                $scope.meetingData = [];
                if (meetingData.code === 200 && meetingData.data.length > 0) {

                    var meetingObject = meetingData.data[0];

                    var meetingsHolder = [];

//                number_of_circuit_supervisor_visit
//                number_of_inset

                    meetingsHolder.push({
                        name: "Staff Meeting",
                        frequency: meetingObject.number_of_staff_meeting,
                        males_present: meetingObject.num_males_present_at_staff_meeting,
                        females_present: meetingObject.num_females_present_at_staff_meeting,
                        comment : meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "SPAM",
                        frequency: meetingObject.number_of_spam_meeting,
                        males_present: meetingObject.num_males_present_at_spam_meeting,
                        females_present: meetingObject.num_females_present_at_spam_meeting,
                        comment : meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "PTA",
                        frequency: meetingObject.number_of_pta_meeting,
                        males_present: meetingObject.num_males_present_at_pta_meeting,
                        females_present: meetingObject.num_females_present_at_pta_meeting,
                        comment : meetingObject.comment
                    });

                    meetingsHolder.push({
                        name: "SMC",
                        frequency: meetingObject.number_of_smc_meeting,
                        males_present: meetingObject.num_males_present_at_smc_meeting,
                        females_present: meetingObject.num_females_present_at_smc_meeting,
                        comment : meetingObject.comment
                    });
                    $scope.meetingData = $scope.meetingData.concat(meetingsHolder);

                    $scope.submittedDate = meetingData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobPupilPerformanceController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('pupil_performanceLoaded', function (evt, pupilPerformanceData) {
                /*
                *

                average_english_score
                :
                "59"
                average_ghanaian_language_score
                :
                "65"
                average_maths_score
                :
                "71"
                circuit_id
                :
                13
                comment
                :
                ""
                country_id
                :
                1
                created_at
                :
                "2015-06-03 12:58:24"
                data_collector_id
                :
                81
                data_collector_type
                :
                "head_teacher"
                district_id
                :
                597
                id
                :
                1833
                lat
                :
                "0"
                level
                :
                "KG1"
                long
                :
                "0"
                num_pupil_who_score_above_average_in_english
                :
                40
                num_pupil_who_score_above_average_in_ghanaian_language
                :
                45
                num_pupil_who_score_above_average_in_maths
                :
                50
                questions_category_reference_code
                :
                "PEPA"
                region_id
                :
                9
                school_id
                :
                49
                school_reference_code
                :
                "8L5b60CvggIKM7nv"
                term
                :
                "second_term"
                updated_at
                :
                "2015-06-03 12:58:24"
                year
                :
                "2014/2015"
                * */

                $scope.school = pupilPerformanceData.school;

                $scope.updateLoadLevel(15);

                $scope.pupilPerformanceData = [];
                if (pupilPerformanceData.code === 200 && pupilPerformanceData.data.length) {
                    $scope.pupilPerformanceData = pupilPerformanceData.data;
                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };
                    $scope.submittedDate = pupilPerformanceData.data[0].updated_at;
                    $scope.updateLoadLevel(100);

                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };


        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobRecordBooksController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('record_booksLoaded', function (evt, recordBooksData) {
                $scope.updateLoadLevel(15);

                if (recordBooksData.school) {
                    $scope.school = recordBooksData.school;
                }

                if (recordBooksData.code === 200 && recordBooksData.data.length) {

                    var item = recordBooksData.data[0];
                    var booksHolder = [];

                    $scope.recordBooksData = [];

                    booksHolder.push({
                        name: "Admission Register",
                        status: item.admission_register
                    });
                    booksHolder.push({
                        name: "Class Registers",
                        status: item.class_registers
                    });
                    booksHolder.push({
                        name: "Teacher Attendance Register",
                        status: item.teacher_attendance_register
                    });
                    booksHolder.push({
                        name: "Visitors\' Book",
                        status: item.visitors
                    });
                    booksHolder.push({
                        name: "Log Book",
                        status: item.log
                    });
                    $scope.updateLoadLevel(35);

                    booksHolder.push({
                        name: "SBA",
                        status: item.sba
                    });
                    booksHolder.push({
                        name: "Movement Book",
                        status: item.movement
                    });
                    booksHolder.push({
                        name: "SPIP",
                        status: item.spip
                    });
                    booksHolder.push({
                        name: "Inventory Books",
                        status: item.inventory_books
                    });
                    booksHolder.push({
                        name: "Cumulative Record Books",
                        status: item.cummulative_records_books
                    });

                    booksHolder.push({
                        name: "Continuous Assessment / SMB",
                        status: item.continous_assessment
                    });


                    $scope.recordBooksData = $scope.recordBooksData.concat(booksHolder);


                    $scope.submittedDate = item.updated_at;


                    $scope.updateLoadLevel(100);
                }else{
                    $scope.completed = true;
                }
            });


            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };


        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSanitationController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.completed = false;
            $scope.loadLevel = 15;
            $scope.$on('sanitationLoaded', function (evt, sanitationData) {
                
                $scope.school = sanitationData.school;

                $scope.updateLoadLevel(15);

                $scope.sanitationData = [];
                if (sanitationData.code === 200 && sanitationData.data.length > 0) {

                        var sanitationObject = sanitationData.data[0];


                    //An empty object that gets initialized every time the loop runs.
                    //It is used to hold the items in object being looped over for the purpose of display
                    var sanitationHolder = [];
                    $scope.sanitationData = [];

                    //reformatting the way the objects are coming from the server tp be used in the table for display
                    sanitationHolder.push({
                        name: "Toilet",
                        status : $scope.formatAvailability[sanitationObject.toilet],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Urinal",
                        status : $scope.formatAvailability[sanitationObject.urinal],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Water",
                        status : $scope.formatAvailability[sanitationObject.water],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Dust Bins",
                        status : $scope.formatAvailability[sanitationObject.dust_bins],
                        comment : sanitationObject.comment
                    });
                    sanitationHolder.push({
                        name: "Veronica Buckets",
                        status : $scope.formatAvailability[ sanitationObject.veronica_buckets],
                        comment : sanitationObject.comment
                    });

                    $scope.sanitationData = $scope.sanitationData.concat(sanitationHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = sanitationData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSchoolStructureController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);
            $scope.schoool =

            $scope.$on('school_structureLoaded', function (evt, structureData) {
                $scope.school = structureData.school;

                $scope.updateLoadLevel(15);

                $scope.structureData = [];
                if (structureData.code === 200 && structureData.data.length > 0) {

                    var structureObject = structureData.data[0];

                    var structureHolder =[];

                    structureHolder.push({
                        name: "Walls",
                        status : $scope.formatGoodPoorFair[structureObject.walls],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Doors",
                        status : $scope.formatGoodPoorFair[structureObject.doors],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Windows",
                        status : $scope.formatGoodPoorFair[structureObject.windows],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Floors",
                        status : $scope.formatGoodPoorFair[structureObject.floors],
                        comment : structureObject.comment
                    });
                    structureHolder.push({
                        name: "Blackboard",
                        status : $scope.formatGoodPoorFair[structureObject.blackboard],
                        comment : structureObject.comment
                    });
                    try{
                        structureHolder.push({
                            name: "Illumination",
                            status : $scope.formatGoodPoorFair[structureObject.illumination],
                            comment : structureObject.comment
                        });
                    }catch(e){
                        console.log(e);
                        structureHolder.push({
                            name: "Illumination",
                            status : ' ',
                            comment : structureObject.comment
                        });
                    }

                    $scope.structureData = $scope.structureData.concat(structureHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = structureData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);
/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSecurityController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('securityLoaded', function (evt, securityData) {

                $scope.securityData = [];
                if (securityData.code === 200 && securityData.data.length > 0) {

                    var securityObject = securityData.data[0];
                    var securityHolder =[];

                    securityHolder.push({
                        name: "Walled / Fenced",
                        status : $scope.formatAvailability[securityObject.walled],
                        comment : securityObject.comment
                    });

                    securityHolder.push({
                        name: "Gated",
                        status : $scope.formatAvailability[securityObject.gated],
                        comment : securityObject.comment
                    });
                    securityHolder.push({
                        name: "Lights",
                        status : $scope.formatAvailability[securityObject.lights],
                        comment : securityObject.comment
                    });
                    securityHolder.push({
                        name: "Security Man",
                        status : $scope.formatAvailability[securityObject.security_man],
                        comment : securityObject.comment
                    });


                    $scope.securityData = $scope.securityData.concat(securityHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = securityData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSportsRecreationalController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);

            $scope.$on('sports_recreationalLoaded', function (evt, sportsRecreationData) {

                $scope.sportsRecreationData = [];
                if (sportsRecreationData.code === 200 && sportsRecreationData.data.length > 0) {

                    $scope.school = sportsRecreationData.school;


                    var recreationObject = sportsRecreationData.data[0];
                    var recreationHolder =[];
                    recreationHolder.push({
                        name: "Football",
                        status : $scope.formatAvailability[recreationObject.football],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Volleyball",
                        status : $scope.formatAvailability[recreationObject.volleyball],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Netball",
                        status : $scope.formatAvailability[recreationObject.netball],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Playing Field",
                        status : $scope.formatAvailability[recreationObject.playing_field],
                        comment : recreationObject.comment
                    });
                    recreationHolder.push({
                        name: "Sports Wear",
                        status : $scope.formatAvailability[recreationObject.sports_wear],
                        comment : recreationObject.comment
                    });
                    try{
                        recreationHolder.push({
                            name: "Seesaw",
                            status : $scope.formatAvailability[recreationObject.seesaw],
                            comment : recreationObject.comment
                        })
                    }catch(e){

                    }

                    try{
                        recreationHolder.push({
                            name: "Merry-Go-Round",
                            status : $scope.formatAvailability[recreationObject.merry_go_round],
                            comment : recreationObject.comment
                        });
                    }catch(e){

                    }


                    recreationHolder.push({
                        name: "First Aid Box",
                        term :   recreationObject.term,
                        status : $scope.formatAvailability[recreationObject.first_aid_box],
                        year : recreationObject.year,
                        comment : recreationObject.comment,
                        data_collector_type : recreationObject.data_collector_type
                    });



                    $scope.sportsRecreationData = $scope.sportsRecreationData.concat(recreationHolder);


                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = sportsRecreationData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobSupportController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);
            $scope.schoool =

                $scope.$on('supportLoaded', function (evt, supportData) {
                    $scope.school = supportData.school;

                    $scope.updateLoadLevel(15);

                    $scope.supportData = [];
                    if (supportData.code === 200 && supportData.data.length) {
                        var item = supportData.data[0];

                        var supportTypeHolder =[];
//                var format_term = $scope.academicTermDisplay(item.term);
                        supportTypeHolder.push({
                            name: "Donor Support",
                            in_cash : item.donor_support_in_cash,
                            in_kind : item.donor_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "Community Support",
                            in_cash : item.community_support_in_cash,
                            in_kind : item.community_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "District Support",
                            in_cash : item.district_support_in_cash,
                            in_kind : item.district_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "PTA Support",
                            in_cash : item.pta_support_in_cash,
                            in_kind : item.pta_support_in_kind,
                            comment : item.comment
                        });
                        supportTypeHolder.push({
                            name: "Other",
                            in_cash : item.other_support_in_cash,
                            in_kind : item.other_support_in_kind,
                            comment : item.comment
                        });
                        $scope.supportData = $scope.supportData.concat(supportTypeHolder);

                        $scope.submittedDate = supportData.data[0].updated_at;
                        $scope.updateLoadLevel(100);

                    }else{
                        $scope.completed = true;
                    }
                });


            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;
            };

            $scope.doSecondaryAction = function(event) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Secondary Action')
                        .content('Secondary actions can be used for one click actions')
                        .ariaLabel('Secondary click demo')
                        .ok('Neat!')
                        .targetEvent(event)
                );
            };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobTeacherAttendanceController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.week_number = $location.search().week_number;
            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);
            $scope.schoool =

            $scope.$on('teacher_attendanceLoaded', function (evt, teacherData) {
                $scope.teacherAttendanceData = [];
                $scope.school_in_session = 'Unavailable';
                if (teacherData.code == '200') {
                    for(var i=0; i < teacherData.data.length; i++){
                        if ( teacherData.data[i].puntuality_and_lesson_plans.length > 0) {//if it has a punc & lessons array
                            //get the days school was in session that week
                            $scope.school_in_session = teacherData.data[i].puntuality_and_lesson_plans[0].days_in_session;
                            $scope.school_in_session = $scope.school_in_session != 1 ? $scope.school_in_session + ' days' : $scope.school_in_session + ' day' ;
                            //overwrite the property with the array content
                            teacherData.data[i].puntuality_and_lesson_plans =  teacherData.data[i].puntuality_and_lesson_plans[0];
                        }
                        $scope.teacherAttendanceData.push(teacherData.data[i])
                    }
                    if ($scope.loadLevel < 90) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.$on('class_managementLoaded', function (evt, classMangmnt) {
                $scope.classManagementData = [];
                if (classMangmnt.code == '200') {
                    for(var i=0; i < classMangmnt.data.length; i++){
                        if ( classMangmnt.data[i].classroom_management.length > 0) {//if it has a classroom management array
                            //overwrite the property with the array content
                            classMangmnt.data[i].classroom_management =  classMangmnt.data[i].classroom_management[0];
                        }
                        $scope.classManagementData.push(classMangmnt.data[i])
                    }
                    if ($scope.loadLevel < 100) {
                        $scope.loadLevel = 100;
                    }
                }
            });

            $scope.loadLevel = 15;
            $timeout(function () {
                if (!$scope.teacherAttendanceData  && !$scope.classManagementData) {
                    $scope.loadLevel += 10;
                }
            },500, 8);

            $scope.completed = false;
            $scope.$watch('loadLevel', function (newValue, oldValue) {
                if (parseInt(newValue) == 100) $scope.completed = true
            });

            $scope.showClassManagement = false;

            $scope.doSecondaryAction = function(event) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Secondary Action')
                        .content('Secondary actions can be used for one click actions')
                        .ariaLabel('Secondary click demo')
                        .ok('Neat!')
                        .targetEvent(event)
                );
            };

            $scope.displayMoreInfo = function (teacherObj, event) {
                var teacherName = teacherObj.first_name + ' ' + teacherObj.last_name;



                $mdDialog.show({
                    parent: angular.element(document.body),
                    targetEvent: event,
                    templateUrl: 'partials/teacher_dialog_list.tpl.html',
                    locals: {
                        teacherObj: teacherObj
                    },
                    controller: DialogController
                });

                function DialogController(scope, $mdDialog, teacherObj) {
                    scope.teacherObj = teacherObj;
                    scope.closeDialog = function () {
                        $mdDialog.hide();
                    }
                }
                //$mdDialog.alert()
                //    .title('Information of ' + teacherObj.first_name + ' ' + teacherObj.last_name)
                //    .content('Secondary actions can be used for one click actions')
                //    .ariaLabel('Secondary click demo')
                //    .ok('Neat!')
                //    .targetEvent(event)
            }
        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

/**
 * Created by Kaygee on 14/05/2015.
 */


mobileMSRC.controller('mobTextBooksController',
    ['$scope','mobDataService', '$stateParams', '$location', '$mdDialog','$timeout',
        function($scope, mobDataService, $stateParams, $location, $mdDialog, $timeout){

            $scope.term = $scope.academicTermDisplay($location.search().term);
            $scope.year = $location.search().year;
            $scope.data_collector_type = $scope.formatDataCollectorType($location.search().data_collector_type);


            $scope.completed = false;
            $scope.loadLevel = 15;


            $scope.$on('textbookLoaded', function (evt, textbookData) {

                $scope.school = textbookData.school;

                $scope.updateLoadLevel(15);
                /*
                *
                circuit_id:13
                country_id:1
                created_at:"2015-06-03 12:47:10"
                data_collector_id:81
                data_collector_type:"head_teacher"
                district_id:597
                id:1876
                lat:"0"
                level:"KG1"
                long:"0"
                number_of_english_textbooks:"0"
                number_of_ghanaian_language_textbooks:"0"
                number_of_maths_textbooks:"0"
                questions_category_reference_code:"CTB"
                region_id:9
                school_id:49
                school_reference_code:"8L5b60CvggIKM7nv"
                term:"second_term"
                updated_at:"2015-06-03 12:47:10"
                week_number:0
                year:"2014/2015"
                * */
                $scope.textbookData = [];
                if (textbookData.code === 200 && textbookData.data.length > 0) {

                    $scope.textbookData = textbookData.data;

                    // //Assign the comments to an object
                    // $scope.textBookComment = {
                    //     ht_comment : item.comment,
                    //     cs_comments : item.cs_comments
                    // };

                    $scope.submittedDate = textbookData.data[0].updated_at;

                    $scope.updateLoadLevel(100);


                }else{
                    $scope.completed = true;
                }
            });



            $scope.updateLoadLevel = function (value) {
                if (parseInt(value) >= 99) $scope.completed = true;
                else $scope.loadLevel = +15;

            };

            // $scope.doSecondaryAction = function(event) {
            //     $mdDialog.show(
            //         $mdDialog.alert()
            //             .title('Secondary Action')
            //             .content('Secondary actions can be used for one click actions')
            //             .ariaLabel('Secondary click demo')
            //             .ok('Neat!')
            //             .targetEvent(event)
            //     );
            // };

        }]);

/**
 * Created by Kaygee on 14/05/2015.
 */

mobileMSRC.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    //The redirect from the server after login has no trailing slash, hence the redirect send the route to the error page.
    // urlRouter uses the 'when' function to redirect to the main dashboard
    $urlRouterProvider.when('', '/');

    //This happens when a url does not exist.
    $urlRouterProvider.otherwise(function ($inj, $loc) {
        console.log('inj',$inj);
        console.log('loc',$loc);
    });

    $stateProvider
        .state('home', {
            url : '/',
            controller: 'mobHomeController',
            templateUrl: 'partials/home.tpl.html',
            page_name : 'Home'
        })
        .state('home.enrollment_state', {
            //url : 'enrollment
            // http://localhost:81/mobileview#/enrollment?school_id=49&week_number=10&term=first_term&year=2015%2F2016&data_collector_type=head_teacher
            url : 'enrollment',
            controller: 'mobEnrollmentController',
            templateUrl: 'partials/enrollment.tpl.html',
            page_name : 'Enrollment',
            resolve: {
                normalEnrollmentData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchData(
                        'normal_enrollment',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}],
                specialEnrollmentData : ['mobDataService', '$location', function(mobDataService, $location){
                    mobDataService.fetchData(
                        'special_enrollment',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.attendance_state', {
            // http://localhost:81/mobileview#/attendance?school_id=49&week_number=10&term=first_term&year=2015%2F2016&data_collector_type=head_teacher
            url : 'attendance',
            controller: 'mobAttendanceController',
            templateUrl: 'partials/attendance.tpl.html',
            page_name : 'Attendance',
            resolve: {
                normalAttendanceData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchData(
                        'normal_attendance',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}],
                specialAttendanceData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchData(
                        'special_attendance',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.teacher_attendance_state', {
            //url : 'teacher_attendance/:school_id/:week_number/:term/:year/:data_collector_type',
            url : 'teacher_attendance',
            controller: 'mobTeacherAttendanceController',
            templateUrl: 'partials/teacher_attendance.tpl.html',
            page_name : 'Teacher Attendance',
            resolve: {
                teacherAttendanceData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTeacherData(
                        'teacher_attendance',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}],

                classManagementData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTeacherData(
                        'class_management',
                        $location.search().school_id,
                        $location.search().week_number,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.textbooks_state', {
            //url : 'textbooks/:school_id/:term/:year/:data_collector_type',
            url : 'textbooks',
            controller: 'mobTextBooksController',
            templateUrl: 'partials/text_books.tpl.html',
            page_name : 'Text Books',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'textbook',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.pupil_performance', {
            url : 'pupil_performance',
            controller: 'mobPupilPerformanceController',
            templateUrl: 'partials/pupil_performance.tpl.html',
            page_name : 'Pupil Performance',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'pupil_performance',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.record_books', {
            url : 'record_books',
            controller: 'mobRecordBooksController',
            templateUrl: 'partials/record_books.tpl.html',
            page_name : 'Record Books',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'record_books',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.support', {
            url : 'support',
            controller: 'mobSupportController',
            templateUrl: 'partials/support.tpl.html',
            page_name : 'Support',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'support',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.grant', {
            url : 'grant',
            controller: 'mobGrantController',
            templateUrl: 'partials/grant.tpl.html',
            page_name : 'Grant',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'grant',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.sanitation', {
            url : 'sanitation',
            controller: 'mobSanitationController',
            templateUrl: 'partials/sanitation.tpl.html',
            page_name : 'Sanitation',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'sanitation',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.security', {
            url : 'security',
            controller: 'mobSecurityController',
            templateUrl: 'partials/security.tpl.html',
            page_name : 'Security',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'security',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.sports_recreational', {
            url : 'sports_recreational',
            controller: 'mobSportsRecreationalController',
            templateUrl: 'partials/sports_recreational.tpl.html',
            page_name : 'Sports & Recreational',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'sports_recreational',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.school_structure', {
            url : 'school_structure',
            controller: 'mobSchoolStructureController',
            templateUrl: 'partials/school_structure.tpl.html',
            page_name : 'School Structure',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'school_structure',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
       .state('home.furniture', {
            url : 'furniture',
            controller: 'mobFurnitureController',
            templateUrl: 'partials/furniture.tpl.html',
            page_name : 'Furniture',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'furniture',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.community_involvement', {
            url : 'community_involvement',
            controller: 'mobCommunityInvolvementController',
            templateUrl: 'partials/community_involvement.tpl.html',
            page_name : 'Community Involvement',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'community_involvement',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.meetings', {
            url : 'meetings',
            controller: 'mobMeetingsController',
            templateUrl: 'partials/meetings.tpl.html',
            page_name : 'Meetings',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'meetings',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('home.general_issues', {
            url : 'general_issues',
            controller: 'mobGeneralIssuesController',
            templateUrl: 'partials/general_issues.tpl.html',
            page_name : 'General Issues',
            resolve: {
                textBooksData : ['mobDataService', '$location', function(mobDataService, $location){
                    return  mobDataService.fetchTermData(
                        'general_issues',
                        $location.search().school_id,
                        $location.search().term,
                        $location.search().year,
                        $location.search().data_collector_type
                    )}]
            }
        })
        .state('error_state', {
            url : 'error',
            controller: 'mobErrorController',
            templateUrl: 'partials/error.tpl.html',
            page_name : 'Error'
        })
}]);

/*
* textbooks/:school_id/:term/:year/:data_collector_type
* pupil_performance/:school_id/:term/:year/:data_collector_type
* record_books/:school_id/:term/:year/:data_collector_type
* support/:school_id/:term/:year/:data_collector_type
* grant/:school_id/:term/:year/:data_collector_type
* sanitation/:school_id/:term/:year/:data_collector_type
* security/:school_id/:term/:year/:data_collector_type
* sports_recreational/:school_id/:term/:year/:data_collector_type
* school_structure/:school_id/:term/:year/:data_collector_type
* furniture/:school_id/:term/:year/:data_collector_type
* community_involvement/:school_id/:term/:year/:data_collector_type
* meetings/:school_id/:term/:year/:data_collector_type
* general_issues/:school_id/:term/:year/:data_collector_type
* school_visit/:school_id/:term/:year/:data_collector_type
* school_review/:school_id/:term/:year/:data_collector_type
*
*
* */
/**
 * Created by Kaygee on 14/05/2015.
 */

mobileMSRC.run( ['$rootScope', '$location', '$state', '$stateParams', '$http', '$templateCache',
    function ($rootScope, $location, $state, $stateParams, $http, $templateCache) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        //Helper function to know the chosen term in a dropdown and associate the server format to readable format
        $rootScope.academicTermDisplay = function(term){
            return term === 'first_term' ? '1st Term' :
                term === 'second_term' ? '2nd Term' :
                    term === 'third_term' ? '3rd Term' : 'NA'
        };

        $rootScope.formatDataCollectorType = function(collector) {
            return collector === 'head_teacher' ? 'Head Teacher' :
                collector === 'circuit_supervisor' ? 'Circuit Supervisor' : 'NA';
        };

        //This is a associative object to format the lowercase and underscores to readable words
        $rootScope.formatAvailability = {
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };

        $rootScope.formatGoodPoorFair = {
            good : 'Good',
            fair : 'Fair',
            poor : 'Poor',
            available : 'Available',
            'available functional' : 'Available, functional',
            'available_functional' : 'Available, functional',
            'available not functional' : 'Available, not functional',
            'available_not_functional' : 'Available, not functional',
            'adequate' : 'Adequate',
            'inadequate' : 'Inadequate',
            "not adequate" : 'Inadequate',
            "not_adequate" : 'Inadequate',
            'not available' : 'Not available',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable'
        };

        //This is a associative object to format the lowercase to sentence case words
        $rootScope.formatAdequecy = {
            'adequate' : 'Adequate',
            'not adequate' : 'Inadequate',
            'N/A' : 'Not applicable',
            'NA' : 'Not applicable',
            'not available' : 'Not Available',
            '' : 'Not Available'
        };

    }]);

/**
 * Created by Kaygee on 15/05/2015.
 */


mobileMSRC.service('mobDataService', ['$rootScope', '$location', '$state', '$stateParams', '$http', '$templateCache',
    function ($rootScope, $location, $state, $stateParams, $http, $templateCache) {

        var urlBase = '/public/data/';
        var urls = {
            normal_enrollment : urlBase+'normal/enrolment',
            special_enrollment : urlBase+'special/enrolment',
            normal_attendance : urlBase+'normal/attendance',
            special_attendance : urlBase+'special/attendance',
            teacher_attendance : urlBase+'teachers/attendance',
            class_management : urlBase+'teachers/class/management',
            textbook : urlBase+'school/textbooks',
            pupil_performance : urlBase+'school/pupils/performance',
            record_books : urlBase+'school/record/books',
            support : urlBase+'school/support',
            grant : urlBase+'school/grants',
            sanitation : urlBase+'school/sanitation',
            security : urlBase+'school/security',
            sports_recreational : urlBase+'school/recreational',
            school_structure : urlBase+'school/school_structure',
            furniture : urlBase+'school/furniture',
            community_involvement : urlBase+'school/community/relations',
            meetings : urlBase+'school/meetings/held',
            general_issues : urlBase+'school/general/situation'
        };


        this.fetchData = function (data_type, school_id, week_number, term, year, data_collector_type) {
            $http.get( urls[data_type ], {params : {
                school_id : school_id,
                week_number : week_number,
                term : term,
                year : year,
                data_collector_type : data_collector_type
            }, cache : false})
                .success(function (sucessData) {
                    if (sucessData.code == '200' && sucessData.status.toLowerCase() == 'ok') {
                        var toSend = {
                            code : 200,
                            school : sucessData.school,
                            data : sucessData.data
                        };
                        $rootScope.$broadcast(data_type+'Loaded', toSend);
                        return toSend
                    }else{
                        var errorOccurred = {
                            code : 404,
                            message : 'An error occurred. Please check internet connection'
                        };
                        $rootScope.$broadcast(data_type+'loaded', sucessData);
                        return errorOccurred;
                    }
                })
        };

        this.fetchTeacherData = function (data_type, school_id, week_number, term, year, data_collector_type) {
            $http.get(urls[data_type], {
                params: {
                    school_id: school_id,
                    week_number: week_number,
                    term: term,
                    year: year,
                    data_collector_type: data_collector_type
                }, cache: false
            })
                .success(function (successData) {
                    if (successData.length > 0) {
                        var toSend = {
                            code : 200,
                            data : successData
                        };
                        $rootScope.$broadcast(data_type+'Loaded', toSend);
                        return toSend;
                    }else{
                        var errorOccurred = {
                            code : 404,
                            message : 'An error occurred. Please check internet connection'
                        };
                        $rootScope.$broadcast(data_type+'loaded', sucessData);
                        return errorOccurred;
                    }
                });


        };

        this.fetchTermData = function (data_type, school_id, term, year, data_collector_type) {
            $http.get( urls[data_type ], {params : {
                school_id : school_id,
                term : term,
                year : year,
                data_collector_type : data_collector_type
            }, cache : false})
                .success(function (sucessData) {
                    if (sucessData.code == '200' && sucessData.status.toLowerCase() == 'ok') {
                        var toSend = {
                            code : 200,
                            school : sucessData.school,
                            data : sucessData.data
                        };
                        $rootScope.$broadcast(data_type+'Loaded', toSend);
                        return toSend
                    }else{
                        var errorOccurred = {
                            code : 404,
                            message : 'An error occurred. Please check internet connection'
                        };
                        $rootScope.$broadcast(data_type+'loaded', sucessData);
                        return errorOccurred;
                    }
                })
        };

    }]);