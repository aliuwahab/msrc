/**
 * Created by sophismay on 7/3/14.
 */


var publicApp = angular.module('publicApp', ['ngResource', 'ngRoute', 'ui.router', 'ui.bootstrap']);

publicApp.config(['$interpolateProvider', '$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($interpolateProvider, $stateProvider, $urlRouterProvider, $locationProvider){
//        $locationProvider.html5Mode(true);
        //$locationProvider.hashPrefix("!");
        // strip trailing slashes from calculated URLs
//        $resourceProvider.defaults.stripTrailingSlashes = true;

        // strip trailing slashes
        $urlRouterProvider.rule(function($injector, $location){
            var path = $location.url();
            // check to see if the path has a trailing slash
            if('/' === path[path.length - 1]){
                return path.replace(/\/$/, '');
            }
            if(path.indexOf('/?') > 0){
                return path.replace('/?', '?');
            }
            return false;
        });

        // to prevent confusion between self and blade
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');

        // routes and states
        $urlRouterProvider.when('', '/');
        $urlRouterProvider.otherwise('/not-found');
        $stateProvider
            .state('home',{
                url: '/',
                resolve: {
                    psFeaturedSchool: 'psFeaturedSchool',
                    schools: function($resource){
                        return $resource('/admin/ghana/schools/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    featured_school: function(psFeaturedSchool, schools){
                        psFeaturedSchool.initialize(schools);
                        //return psFeaturedSchool.getTemporalSchool();
                        return psFeaturedSchool.getFeaturedSchool();
                    },
                    regions: function($resource){
                        return $resource('/admin/ghana/regions/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    districts: function($resource){
                        return $resource('/admin/ghana/districts/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    circuits: function($resource){
                        return $resource('/admin/ghana/circuits/:id', {id: "@id"},
                            {query: {isArray:true,cache:true}}).query().$promise;
                    },
                    reviews_questions: function($resource){
                        return $resource('/school/review/questions', {},
                            {query: {isArray:true, cache:true}}).query().$promise;
                    }
                },
                templateUrl: 'frontend/partials/home.html',
                controller: 'psHomeController'

            })
            .state('schools', {
                url: '/schools',
                templateUrl: 'frontend/partials/schools.html',
                resolve: {
                    schools: function($resource){
                        return $resource('/admin/ghana/schools/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    regions: function($resource){
                        return $resource('/admin/ghana/regions/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    districts: function($resource){
                        return $resource('/admin/ghana/districts/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    circuits: function($resource){
                        return $resource('/admin/ghana/circuits/:id', {id: "@id"},
                            {query: {isArray:true,cache:true}}).query().$promise;
                    }
                },
                controller: 'psSchoolsController'
            })
            .state('school', {
                abstract: true,
                template: '<ui-view />',
                resolve: {
                    schools: function($resource) {
                        return $resource('/admin/ghana/schools/:id', {id: "@id"},
                            {query: {isArray: true, cache: true}}).query().$promise;
                    },
                    regions: function($resource){
                        return $resource('/admin/ghana/regions/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    districts: function($resource){
                        return $resource('/admin/ghana/districts/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    circuits: function($resource){
                        return $resource('/admin/ghana/circuits/:id', {id: "@id"},
                            {query: {isArray:true,cache:true}}).query().$promise;
                    }

                }
            })
            .state('school.compare', {
                url: '/schools/:id',
                templateUrl: 'frontend/partials/first-school.html',
                controller: 'psFirstSchoolController',
                resolve: {
                    schools: function(schools){
                        return schools;
                    },
                    regions: function(regions){
                        return regions;
                    },
                    districts: function(districts){
                        return districts;
                    },
                    circuits: function(circuits){
                        return circuits;
                    },
                    headteacher: function(schools, $stateParams, $filter, $resource, $modal){
                        var school = $filter('filter')(schools, {id: $stateParams.id})[0];
                        var SchoolNotFoundModalInstanceCtrl = function($scope, $modalInstance){
                            $scope.ok = function(){
                                $modalInstance.dismiss('closed');
                                history.back();
                            };
                        };
                        // make sure school is found
                        if(!school){
                            var SchoolNotFoundModalInstance = $modal.open({
                                templateUrl: '/frontend/partials/school-not-found-modal.html',
                                controller: SchoolNotFoundModalInstanceCtrl,
                                size: 'sm'
                            });
                        }
                        var head_teacher_id = school.headteacher_id;
                        if(head_teacher_id){
                            return $resource('/admin/data_collector/:htid/profile',
                                {htid: head_teacher_id, type: 'head_teacher'},
                                {query: {cache:true}}).query().$promise;
                        }else{
                            return null;
                        }

                    },
                    reviews_questions: function($resource){
                        return $resource('/school/review/questions', {},
                            {query: {isArray:true, cache:true}}).query().$promise;
                    }
                }
            })
            .state('school.compare.second', {
                url: '/second/:sid',
                templateUrl: 'frontend/partials/second-school.html',
                controller: 'psSecondSchoolController',
                resolve: {
                    schools: function(schools){
                        return schools;
                    },
                    headteacher: function(schools, $stateParams,$modal, $filter, $resource){
                        var school = $filter('filter')(schools, {id: $stateParams.sid})[0];
                        var SchoolNotFoundModalInstanceCtrl = function($scope, $modalInstance){
                            $scope.ok = function(){
                                $modalInstance.dismiss('closed');
                                history.back();
                            };
                        };
                        // make sure school is found
                        if(!school){
                            var SchoolNotFoundModalInstance = $modal.open({
                                templateUrl: '/frontend/partials/school-not-found-modal.html',
                                controller: SchoolNotFoundModalInstanceCtrl,
                                size: 'sm'
                            });
                        }
                        var head_teacher_id = school.headteacher_id;
                        return $resource('/admin/data_collector/:htid/profile',
                            {htid: head_teacher_id, type: 'head_teacher'},
                            {query: {cache:true}}).query().$promise;
                    },
                    regions: function(regions){
                        return regions;
                    },
                    districts: function(districts){
                        return districts;
                    },
                    circuits: function(circuits){
                        return circuits;
                    },
                    reviews_questions: function($resource){
                        return $resource('/school/review/questions', {},
                            {query: {isArray:true, cache:true}}).query().$promise;
                    }
                }
            })
            .state('region', {
                url: '/regions/:id',
                templateUrl: 'frontend/partials/region.html',
                resolve: {
                    regions: function($resource){
                        return $resource('/admin/ghana/regions/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    schools: function($resource){
                        return $resource('/admin/ghana/schools/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    districts: function($resource){
                        return $resource('/admin/ghana/districts/:id', {id: "@id"},
                            {query:{isArray:true,cache:true}}).query().$promise;
                    },
                    circuits: function($resource){
                        return $resource('/admin/ghana/circuits/:id', {id: "@id"},
                            {query: {isArray:true,cache:true}}).query().$promise;
                    }
                },
                controller: 'psRegionController'
            })
            .state('about-us', {
                url: '/about-us',
                templateUrl: 'frontend/partials/about-us.html',
                controller: 'psAboutController'
            })
            .state('contact-us', {
                url: '/contact',
                templateUrl: 'frontend/partials/contact-us.html',
                controller: 'psContactController'
            })
            .state('not-found', {
                url: '/not-found',
                templateUrl: 'frontend/partials/404.html'
            });


}]);

publicApp.run( function ($rootScope, $location, $state, $stateParams) {

    $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
        $rootScope.loading = true;
    });

    $rootScope.$on('$viewContentLoaded',function(event){
        $rootScope.loading = false;
    });
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
        $rootScope.loading = false;
    });
    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams){
        $rootScope.loading = false;
    });

    //This makes it possible to access UI-State and StateParams within the scope from the HTML partial view
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
});