/**
 * Created by sophismay on 7/30/2014.
 */

/**
 *  Service for featured school
 */

publicApp.factory('psFeaturedSchool', ['$filter', function($filter){
    var schools = [];
    return {
        initialize: function(schls){
            //var schools = [];
            schools = schls;
        },
        getFeaturedSchool: function(){
            var randomIndex = Math.floor(Math.random()*schools.length);
            return schools[randomIndex];
        },
        getTemporalSchool: function(){
            // get pulima school since it is the one with data
            return $filter('filter')(schools, {id: 1})[0];
        }
    }
}]);
