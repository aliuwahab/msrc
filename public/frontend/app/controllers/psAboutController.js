/**
 * Created by sophismay on 7/10/14.
 */

publicApp.controller('psAboutController', function($scope){
    $("ul.navbar-right").find("li").removeClass('active');
    $("ul.navbar-right > li.nav-about").addClass('active');

    var scrollUp = function(){
        $('html,body').animate({
            scrollTop: $(".background").offset().top
        }, 1000);
    };

    /**
     * true if content one is active, else false
     */
    $scope.isPageOne = function(){
        var s = $("#background-content-one").css("display");
        return s == "block";
    };
    /**
     * show background content one
     */
    $scope.showContentOne = function(){
        $("#background-content-two").css({"display":"none"});
        scrollUp();
        $("#background-content-one").css({"display":"block"});
    };
    /**
     * show background content two
     */
    $scope.showContentTwo = function(){
        $("#background-content-one").css({"display":"none"});
        scrollUp();
        $("#background-content-two").css({"display":"block"});
    };
});