/**
 * Created by sophismay on 7/12/2014.
 */

/**
 *  Region Controller
 */

publicApp.controller('psRegionController', function($scope, $stateParams, $filter,
                                                    regions, schools, districts, circuits){

    $("ul.navbar-right").find("li").removeClass('active');
    $("ul.navbar-right > li.nav-regions").addClass('active');

    var regionId = $stateParams.id;
    $scope.regions = regions;
    $scope.region = $filter('filter')($scope.regions, {id: regionId})[0];

    // to store new school ratings
    $scope.newSchoolsRatings = {};

    // initialize new ratings for schools
    var setNewSchoolsRatings = function(){
        angular.forEach(schools, function(val, key){
            // set review to null if not present or at least one review not present
            if(!val || !val.current_state_of_community_involvement || !val.current_state_of_teaching_and_learning
                || !val.current_state_of_friendliness_to_boys_girls || !val.current_state_of_healthy_level
                || !val.current_state_of_safety_and_protection || !val.current_state_of_school_inclusiveness){
                this[val.id] = null;
            }else{
                this[val.id] = Number(val.current_state_of_community_involvement)
                    + Number(val.current_state_of_teaching_and_learning)
                    + Number(val.current_state_of_friendliness_to_boys_girls)
                    + Number(val.current_state_of_healthy_level)
                    + Number(val.current_state_of_safety_and_protection)
                    + Number(val.current_state_of_school_inclusiveness);
                this[val.id] = (this[val.id]*100/60).toFixed();
            }
        }, $scope.newSchoolsRatings);
    };

    // set ratings for all schools
    setNewSchoolsRatings();

    console.log("new schools ratings");console.log($scope.newSchoolsRatings);

    $scope.isRegion = function(){
        return $scope.region ? true: false;
    };


    var getDataByRegion = function(data, id){
        var list = [];
        angular.forEach(data, function(value, key){
            if(value.region_id == id){
                this.push(value);
            }
        }, list);
        return list;
    };

    /**
     * Object to store all reviews for all schools
     * key: school id, value: object - {key: review, value: review value}
     * @type {{}}
     */
    $scope.schoolReviews = {};

    /**
     * Object to store ratings for all schools
     * key: school id, value: rating
     * @type {{}}
     */
    $scope.schoolRatings = {};

    // populate schools under region
    //$scope.schools = $filter('filter')(schools, {region_id: 10});
    var regionSchools = $scope.schools = getDataByRegion(schools, regionId);

    // populate districts under region
    //$scope.districts = $filter('filter')(districts, {region_id: regionId});
    $scope.districts = getDataByRegion(districts, regionId);

    // populate circuits under region
    //$scope.circuits = $filter('filter')(circuits, {region_id: regionId});
    $scope.circuits = getDataByRegion(circuits, regionId);

    // object containing region names
    $scope.regionName = {};

    // function to get circuit by selected district
    $scope.getCircuitBySelectedDistrict = function(){
        return function(circuit){
            if(!$scope.selectedDistrict) return false;
            else return circuit.district_id == $scope.selectedDistrict.id;
        }
    };

    $scope.searchByDistrict = function(){
        return function(school){
            if(!$scope.selectedDistrict) return true;
            else return $scope.selectedDistrict.id == school.district_id;
        }
    };
    $scope.searchByCircuit = function(){
        return function(school){
            if(!$scope.selectedCircuit) return true;
            else return $scope.selectedCircuit.id == school.circuit_id;
        }
    };

    /**
     * function to check if school rating is available
     * @param school_id - Integer
     * @return Boolean
     */
    $scope.isSchoolRatingAvailable = function(school_id){
        return $scope.schoolRatings[school_id] ? true : false;
    };

    // listen to change on search refine elements and reset
    // for the purpose of pagination
    $scope.$watch('searchquery', function(newQuery){
        initializePaginationSettings();
    });
    $scope.$watch('selectedDistrict', function(newSelectedDistrict){
        // reset circuits when selected district changes
        $scope.selectedCircuit = null;
        initializePaginationSettings();
    });
    $scope.$watch('selectedCircuit', function(newSelectedCircuit){
        initializePaginationSettings();
    });
    $scope.$watch('currentPage', function(newPage){
        // scroll to top of search results
        $('html,body').animate({
            scrollTop: $(".search-results").offset().top
        }, 500);
    });

    /**
     *  Pagination
     */
    $scope.filteredSchools = [];
    // reset filtered schools
    var setFilteredSchools = function(){
        var fs = [];
        $scope.filteredSchools = []; // reset filtered schools
        $scope.filteredSchools = $filter('filter')(regionSchools, $scope.searchquery);
        // filter by selected district
        fs = $filter('filter')($scope.filteredSchools, function(val){
            return $scope.selectedDistrict ? $scope.selectedDistrict.id == val.district_id : true;
        });
        $scope.filteredSchools = fs;
        // filter by selected circuit
        fs = $filter('filter')($scope.filteredSchools, function(val){
            return $scope.selectedCircuit ? $scope.selectedCircuit.id == val.circuit_id : true;
        });
        $scope.filteredSchools = fs;
        console.log("after filtering schools for pagination");console.log($scope.filteredSchools);

    };
    // change page when pagination link clicked
    $scope.changePage = function(){
        var nextPage = ($scope.itemsPerPage*$scope.currentPage) - $scope.itemsPerPage;
        setFilteredSchools();
        $scope.totalItems = $scope.filteredSchools.length;
        $scope.schools = $scope.filteredSchools.slice(nextPage, nextPage + $scope.itemsPerPage);
    };

    // initialize filteredSchools
    var initializePaginationSettings = function(){
        setFilteredSchools();
        $scope.currentPage = 1;
        $scope.maxSize = 10;
        $scope.nextPage = 0;
        $scope.totalItems = $scope.filteredSchools.length;
        $scope.itemsPerPage = 10;
        $scope.changePage();
    };
    initializePaginationSettings();
});
