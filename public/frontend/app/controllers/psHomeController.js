/**
 * Created by sophismay on 7/4/14.
 */

publicApp.controller('psHomeController', function($scope, $filter, $location,$rootScope, $state, $stateParams, schools,
                                                  regions, districts, circuits, reviews_questions,
                                                  featured_school){
    $("ul.navbar-right").find("li").removeClass('active');
    $("ul.navbar-right > li.nav-home").addClass('active');

    /** load a random image as banner background **/
    var images = [
        'school_image1.jpg',
        'school_image2.jpg',
        'school_image3.jpg',
        'school_image4.jpg',
        'school_image5.jpg'
    ];
    // ui bootstrap carousel
    $scope.carouselInterval = 5000;
    $scope.slides = [
        {image: 'frontend/images/school_image1.jpg', header: 'Monitor Performance of Basic Schools in Ghana',
            sub: 'With Up-to-date Educational Reviews', button: 'View Schools', link: 'schools'},
        {image: 'frontend/images/school_image2.jpg', header: 'View Data By Regions, Districts and Circuits',
            sub: 'With real-time performance data', button: 'Search Now', link: 'schools'},
        {image: 'frontend/images/school_image3.jpg', header: ' mSRC, Enhancing the Quality of Education through ICT',
            sub: 'Learn More About the Project', button: 'View Background', link: 'about-us'}
    ];

    // initialize schools
    $scope.schools = schools;
    $scope.regions = regions;
    $scope.featuredSchool = featured_school;

    //console.log('regions');
    //console.log(regions);

    //console.log('schools');
    //console.log(schools);

    $scope.searchquery = "";

    // set region names by id
    $scope.regionsObj = {};
    angular.forEach(regions, function(obj, ind){
        this[obj.id] = obj.name;
    }, $scope.regionsObj);

    // object to store featured school's rating
    $scope.featuredSchoolRating = null;

    //function to set featured school's rating
    var setFeaturedSchoolRating = function(){
        if(!$scope.featuredSchool){
            $scope.featuredSchoolRating = null;
        }
        if(!$scope.featuredSchool.current_state_of_community_involvement || !$scope.featuredSchool.current_state_of_teaching_and_learning
            || !$scope.featuredSchool.current_state_of_friendliness_to_boys_girls || !$scope.featuredSchool.current_state_of_healthy_level
            || !$scope.featuredSchool.current_state_of_safety_and_protection || !$scope.featuredSchool.current_state_of_school_inclusiveness){
            $scope.featuredSchoolRating = null;
        }
        else{
            $scope.featuredSchoolRating = Number($scope.featuredSchool.current_state_of_community_involvement)
                + Number($scope.featuredSchool.current_state_of_teaching_and_learning)
                + Number($scope.featuredSchool.current_state_of_friendliness_to_boys_girls)
                + Number($scope.featuredSchool.current_state_of_healthy_level)
                + Number($scope.featuredSchool.current_state_of_safety_and_protection)
                + Number($scope.featuredSchool.current_state_of_school_inclusiveness);
            $scope.featuredSchoolRating = ($scope.featuredSchoolRating*100/60).toFixed();//Math.floor($scope.featuredSchoolRating*100/60);
        }
    };
    setFeaturedSchoolRating(); // set featured school rating

    $scope.isQueryEmpty = function(){
        return $scope.searchquery.length === 0;
    };

    $scope.getSchoolRegion = function(region_id){
        return $scope.regionsObj.hasOwnProperty(region_id) ? $scope.regionsObj[region_id] : "Not Available";
    };
    $scope.getSchoolDistrict = function(district_id){
        var district = $filter('filter')(districts, {id: district_id})[0];
        return district ? district.name : "Not Available";
    };
    $scope.getSchoolCircuit = function(circuit_id){
        var circuit = $filter('filter')(circuits, {id: circuit_id})[0];
        return circuit ? circuit.name : "Not Available";
    };

    $scope.schoolNames = $scope.schools.map(function(school){
        return school.name;
    });
    $scope.selected = undefined;


    /**
     * Object to store all reviews for all schools
     * key: school id, value: object - {key: review, value: review value}
     * @type {{}}
     */
    $scope.schoolReviews = {};

    /**
     * Object to store ratings for all schools
     * key: school id, value: rating
     * @type {{}}
     */
    $scope.schoolRatings = {};

    /*
        Object to store questions of school Reviews
     */
    $scope.schoolQuestions = {};

    // function to assign questions to each review category
    var setQuestionsByCategory = function(questionsList){
        angular.forEach(questionsList, function(qObj, ind){
            var c = qObj.question_category_reference;
            var q = qObj.question;
            if(!this[c]) this[c] = [q];
            else this[c].push(q);
        }, $scope.schoolQuestions);
    };
    setQuestionsByCategory(reviews_questions);

    /**
     * function to calculate and return number of stars for school
     * @param rating - the rating of the school - Number
     */
    var calculateNumberOfStars = function(rating){
        var nStars = 0;
        if(rating >= 0 && rating <= 6) nStars = 0.5;
        else if(rating >=7 && rating <= 12) nStars = 1;
        else if(rating >=13 && rating <= 18) nStars = 1.5;
        else if(rating >=19 && rating <= 24) nStars = 2;
        else if(rating >=25 && rating <= 30) nStars = 2.5;
        else if(rating >=31 && rating <= 36) nStars = 3;
        else if(rating >=37 && rating <= 42) nStars = 3.5;
        else if(rating >=43 && rating <= 48) nStars = 4;
        else if(rating >=49 && rating <= 54) nStars = 4.5;
        else nStars = 5;
        console.log("NUMBER OF STARS: " + nStars);
        return nStars;


    };
    // new function to set rating stars for school
    var setStars = function(rating){
        console.log("RATINGGGGG: " + rating);
        var starsNumber = calculateNumberOfStars(rating*60/100);
        var remainderLacking = 5 - starsNumber; // number of hollow stars
        var ul = $('ul.stars-rating'); console.log("NUmBER OF STARRRS" +starsNumber);
        // reset stars
        ul.find('li').remove();
        for(var i=0;i<Math.floor(starsNumber);i++){
            ul.append("<li class='colored'><i class='fa fa-star fa-2x'></i></li>");
        }
        // if decimal
        if(starsNumber % 1 > 0){
            ul.append("<li class='colored'><i class='fa fa-star-half-empty fa-2x'></i></li>");
        }
        for(var x=0;x<Math.floor(remainderLacking);x++){
            ul.append("<li class='colored'><i class='fa fa-star-o fa-2x'></i></li>");
        }
    };

    setStars($scope.featuredSchoolRating);
    /**
     * function to return type of progress bar
     * @param val - current value of a review
     */
    $scope.getProgressbarType = function(val){
        if(val >= 8) return "success";
        else if(val >= 5) return null;
        else if(val >= 3) return "warning";
        else return "danger";
    };

    // set random featured school making sure it isn't same as the previous
    var setRandomSchool = function(){
        var randomIndex = Math.floor(Math.random()*schools.length);
        var schl = schools[randomIndex];
        if($scope.featuredSchool.id == schl.id) {
            setRandomSchool();
        }
        else $scope.featuredSchool = schl;
    };

    $scope.changeFeaturedSchool = function(){
        setRandomSchool();
        setFeaturedSchoolRating();
        setStars($scope.featuredSchoolRating);
    };
});