/**
 * Created by sophismay on 7/24/2014.
 */

publicApp.controller('psFirstSchoolController', ['$scope', '$rootScope', '$location', '$state', '$stateParams', '$filter', 'regions', 'districts', 'circuits',
    'schools', 'headteacher', 'reviews_questions','$modal','$log',
    function($scope, $rootScope, $location, $state,$stateParams, $filter, regions, districts, circuits, schools, headteacher,
        reviews_questions,$modal, $log){

        // set the schools navbar active
        $("ul.navbar-right").find("li").removeClass('active');
        $("ul.navbar-right > li.nav-schools").addClass('active');

        // initialize resources
        $scope.schools = schools; console.log($scope.schools);
        $scope.regions = regions;
        $scope.circuits = circuits;console.log("FIRST LOOK AT CIRCUiTS");console.log($scope.circuits);
        $scope.districts = districts;

        // function to change first school when school selected from overlay
        $scope.changeFirstSchool = function(school_id){
            // set second school id if present in url
            var second_school_id = null;
            var secondSchoolExists = $location.path().match(/second/g);
            if(secondSchoolExists) second_school_id = $location.path().split("/").pop();

            // if second school not present, change first school using state
            if(!second_school_id){
                $state.go('school.compare', {id: school_id});
                $scope.hideOverlay("#school-change-overlay");
            }else{
                console.log("ROUTE SECOND SCHOOL ID");console.log(second_school_id);
                $state.go('school.compare.second', {id: school_id, sid : second_school_id});
                $scope.hideOverlay("#school-change-overlay");
            }
            // automatically scroll to school container
            $("html, body").animate({
                scrollTop:  $("#school-info").offset().top
            }, 1000);
        };

        // function to hide 'Add School to compare' button if second school present (has been searched for)
        $scope.onlyFirstSchool = function(){
            var matchResult = $location.path().match(/second/g);
            return matchResult ? true : false;
        };


        // initialize first school
        var schoolId = $stateParams.id;
        $scope.school = $filter('filter')(schools, {id: schoolId})[0];

        // list of districts in search modal in a drop down
        $scope.ddDistricts = [];

        // initialize dropdown districts with all districts
        $scope.ddDistricts = angular.copy($scope.districts);

        // initialize checked regions for sorting
        $scope.searchRegion = {
            1: true,
            2: true,
            3: true,
            4: true,
            5: true,
            6: true,
            7: true,
            8: true,
            9: true,
            10: true
        };
        // object to store names of regions
        $scope.regionName = {};
        // object to store names of districts
        $scope.districtName = {};
        // object to store name of circuit
        $scope.circuitName = {};

        // set head teacher name if present, else set to not available
        if(headteacher) $scope.headTeacherName = headteacher.last_name + ", " + headteacher.first_name;
        else  $scope.headTeacherName = "Name not available";

        // set first school, this is the first school being compared
        $scope.firstSchool = null;
        $scope.firstSchool = $filter('filter')(schools, {id: $stateParams.id})[0];
        console.log("first school");console.log($scope.firstSchool);

        // variable to store featured school's rating
        $scope.firstSchoolRating = null;

        //function to set featured school's rating
        var setFirstSchoolRating = function(){
            if(!$scope.firstSchool){
                $scope.firstSchoolRating = null;
            }
            if(!$scope.firstSchool.current_state_of_community_involvement || !$scope.firstSchool.current_state_of_teaching_and_learning
                || !$scope.firstSchool.current_state_of_friendliness_to_boys_girls || !$scope.firstSchool.current_state_of_healthy_level
                || !$scope.firstSchool.current_state_of_safety_and_protection || !$scope.firstSchool.current_state_of_school_inclusiveness){
                $scope.firstSchoolRating = null;
            }
            else{
                $scope.firstSchoolRating = Number($scope.firstSchool.current_state_of_community_involvement)
                    + Number($scope.firstSchool.current_state_of_teaching_and_learning)
                    + Number($scope.firstSchool.current_state_of_friendliness_to_boys_girls)
                    + Number($scope.firstSchool.current_state_of_healthy_level)
                    + Number($scope.firstSchool.current_state_of_safety_and_protection)
                    + Number($scope.firstSchool.current_state_of_school_inclusiveness);
                $scope.firstSchoolRating = ($scope.firstSchoolRating*100/60).toFixed();
            }
        };
        setFirstSchoolRating(); // set first school rating




        /**
         * Object to store ratings for all schools
         * key: school id, value: rating
         * @type {{}}
         */
        $scope.schoolRatings = {};


        /**
         * Object to store all reviews for all schools
         * key: school id, value: object - {key: review, value: review value}
         * @type {{}}
         */
        $scope.schoolReviews = {};

        /**
         * Object to store ratings for all schools
         * key: school id, value: rating
         * @type {{}}
         */
        $scope.schoolRatings = {};

        // for schools sorting purposes
        $scope.sortOptions = [
            {value: "name", text: "Sort by Name"},
            {value: "status", text: "Sort by Status"}
        ];
        // initialize sort order
        $scope.sortOrder = $scope.sortOptions[0].value;

        // function to filter schools by checked regions
        $scope.searchByRegion = function(){
            return function(school){
                if($scope.searchRegion[school.region_id] === true){
                    return true;
                }
            }
        };

        // function to filter schools by selected district (from dropdown)
        $scope.searchByDistrict = function(){
            return function(school){
                if(!$scope.selectedDistrict) return true;
                // if not null, filter school by district
                if($scope.selectedDistrict){
                    return $scope.selectedDistrict.id === school.district_id;
                }
            }
        };
        // function to filter dropdown districts based on checked regions
        $scope.getDistrictBySelectedRegion = function(){
            return function(district){
                return $scope.searchRegion[district.region_id] === true;
            }
        };

        /*
         Object to store questions of school Reviews
         */
        $scope.schoolQuestions = {};

        // function to assign questions to each review category
        var setQuestionsByCategory = function(questionsList){
            angular.forEach(questionsList, function(qObj, ind){
                var c = qObj.question_category_reference;
                var q = qObj.question;
                if(!this[c]) this[c] = [q];
                else this[c].push(q);
            }, $scope.schoolQuestions);
        };
        setQuestionsByCategory(reviews_questions);



        $scope.removeCompareSchool = function(school){
            angular.forEach($scope.comparisonSchools, function(value, key){
                if(value.id == school.id){
                    addSchoolToSearchResults(school);
                    //starsInitialized[school.id] = false;
                    return $scope.comparisonSchools.splice(key, 1);
                }
            });
        };

        /**
         * function to add school to search results
         * @param school
         */
        var addSchoolToSearchResults = function(school){
            $scope.schools.push(school);
        };

        $scope.comparisonSchools = []; // list to contain all schools being compared
        //$scope.comparisonSchools.push($scope.school);
        //$scope.addSchoolToCompare($scope.school); // add to compare list
        var starsInitialized = {}; // object to store if stars have been initialized for each school

        // get region name
        $scope.getRegionName = function(region_id){
            var region = $filter('filter')($scope.regions, {id: region_id})[0];
            return region ? region.name : "Not Available";
        };

        // get district name
        $scope.getDistrictName = function(district_id){
            var district = $filter('filter')($scope.districts, {id: district_id})[0];
            return district ? district.name : "Not Available";
        };
        // get circuit name
        $scope.getCircuitName = function(circuit_id){
            var circuit = $filter('filter')($scope.circuits, {id: circuit_id})[0];
            return circuit ? circuit.name : "Not Available";
        };

        /**
         * function to calculate and return number of stars for school
         * @param rating - the rating of the school - Number
         */
        var calculateNumberOfStars = function(rating){
            var nStars = 0;
            if(rating >= 0 && rating <= 6) nStars = 0.5;
            else if(rating >=7 && rating <= 12) nStars = 1;
            else if(rating >=13 && rating <= 18) nStars = 1.5;
            else if(rating >=19 && rating <= 24) nStars = 2;
            else if(rating >=25 && rating <= 30) nStars = 2.5;
            else if(rating >=31 && rating <= 36) nStars = 3;
            else if(rating >=37 && rating <= 42) nStars = 3.5;
            else if(rating >=43 && rating <= 48) nStars = 4;
            else if(rating >=49 && rating <= 54) nStars = 4.5;
            else nStars = 5;
            return nStars;
            //console.log("NUMBER OF STARS: " + nStars);

        };

        /**
         * function to return type of progress bar
         * @param val - current value of a review
         */
        $scope.getProgressbarType = function(val){
            if(val >= 8) return "success";
            else if(val >= 5) return null;
            else if(val >= 3) return "warning";
            else return "danger";
        };

        // new function to set rating stars for school
        $scope.setFirstStars = function(rating){
            var starsNumber = calculateNumberOfStars(rating*60/100);
            var remainderLacking = 5 - starsNumber; // number of hollow stars
            var ul = $('ul#first-school-star-rating'); console.log("NUmBER OF STARRRS" +starsNumber);

            for(var i=0;i<Math.floor(starsNumber);i++){
                ul.append("<li class='colored'><i class='fa fa-star fa-2x'></i></li>");
            }
            // if decimal
            if(starsNumber % 1 > 0){
                ul.append("<li class='colored'><i class='fa fa-star-half-empty fa-2x'></i></li>");
            }
            for(var x=0;x<Math.floor(remainderLacking);x++){
                ul.append("<li class='colored'><i class='fa fa-star-o fa-2x'></i></li>");
            }
        };
        $scope.setFirstStars($scope.firstSchoolRating);

        /**
         * Pagination
         */
        $scope.totalSchools = $scope.schools.length;
        $scope.currentPage = 1;
        $scope.paginationMaxSize = 2;






        $scope.items = ['item1', 'item2', 'item3'];

        $scope.openFirstOverlay = function (size) {

            var modalInstance = $modal.open({
                templateUrl: '/frontend/partials/first-modal.html',
                controller: ModalInstanceCtrl,
                size: size,
                resolve: {
                    schools: function(){
                        return schools;
                    },
                    regions: function(){
                        return regions;
                    },
                    districts: function(){
                        return districts;
                    },
                    circuits: function(){
                        return circuits;
                    }
                }
            });

        };

        $scope.openSecondOverlay = function(size){
            var secondModalInstance = $modal.open({
                templateUrl: '/frontend/partials/second-modal.html',
                controller: SecondModalInstanceCtrl,
                size: size,
                resolve: {
                    schools: function(){
                        return schools;
                    },
                    regions: function(){
                        return regions;
                    },
                    districts: function(){
                        return districts;
                    },
                    circuits: function(){
                        return circuits;
                    }
                }
            });
        };

        var ModalInstanceCtrl = function ($scope, $modalInstance, $location, schools, regions, districts, circuits) {

            $scope.schools = schools;
            $scope.regions = regions;
            $scope.districts = districts;
            $scope.circuits = circuits;console.log("circuits AGain");console.log($scope.circuits);
            // for schools sorting purposes

            $scope.ddDistricts = [];  // use {key: value}

            // initialize dropdown districts with all districts
            $scope.ddDistricts = angular.copy($scope.districts);

            // list of circuits in dropdown
            $scope.ddCircuits = [];
            $scope.ddCircuits = angular.copy($scope.circuits);

            // initialize checked regions for sorting
            $scope.searchRegion = {
                1: true,
                2: true,
                3: true,
                4: true,
                5: true,
                6: true,
                7: true,
                8: true,
                9: true,
                10: true
            };
            // object to store names of regions
            $scope.regionName = {};
            // object to store names of districts
            $scope.districtName = {};
            // object to store name of circuit
            $scope.circuitName = {};
            // wrapped in an object to avoid nested scope issues
            // initialize
            $scope.sel = {selectedDistrict: null, selectedCircuit: null, searchquery: ""};

            $scope.searchDistrict = {};
            // function to filter schools by checked regions
            $scope.searchByRegion = function(){
                return function(school){
                    if($scope.searchRegion[school.region_id] === true){
                        return true;
                    }
                }
            };

            // function to toggle regions and reset selected district and selected circuit
            $scope.toggleRegions = function(region_id){
                $scope.searchRegion[region_id] = $scope.searchRegion[region_id] != true;
                $scope.sel.selectedDistrict = null;
                $scope.sel.selectedCircuit = null;
                initializePaginationSettings();
            };

            // function to filter dropdown districts based on checked regions
            $scope.getDistrictBySelectedRegionModalOne = function(){
                return function(district){
                    return $scope.searchRegion[district.region_id] == true;
                }
            };
            // function to filter dropdown circuits based on selected district
            $scope.getCircuitBySelectedDistrict = function(){
                return function(circuit){
                    if(!$scope.sel.selectedDistrict) return false; // no district selected, no circuit
                    else return $scope.sel.selectedDistrict.id == circuit.district_id;
                }
            };

            // function to filter schools by selected district (from dropdown)
            $scope.searchByDistrict = function(){
                return function(school){
                    // if not null, filter school by district
                    if(!$scope.sel.selectedDistrict) return true;
                    else return $scope.sel.selectedDistrict.id == school.district_id;
                }
            };
            //function to filter school by selected circuit (from dropdown)
            $scope.searchByCircuit = function(){
                return function(school){
                    if(!$scope.sel.selectedCircuit) return true;
                    else return $scope.sel.selectedCircuit.id == school.circuit_id;
                }
            };

            // Pagination
            // listen to change on search refine elements and reset
            // for the purpose of pagination
            $scope.$watch('sel.searchquery', function(newQuery){
                initializePaginationSettings();
            });
            $scope.$watch('sel.selectedDistrict', function(newSelectedDistrict){
                initializePaginationSettings();
            });
            $scope.$watch('sel.selectedCircuit', function(newSelectedCircuit){
                initializePaginationSettings();
            });

            /**
             *  Pagination
             */
            $scope.filteredSchools = [];
            // reset filtered schools
            var setFilteredSchools = function(){
                var fs = [];
                $scope.filteredSchools = []; // reset filtered schools
                $scope.filteredSchools = $filter('filter')(schools, $scope.sel.searchquery);
                // search by region
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    if($scope.searchRegion[val.region_id] == true){
                        return true;
                    }
                });
                console.log(fs);
                $scope.filteredSchools = fs;
                console.log($scope.filteredSchools);
                // filter by selected district
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    return $scope.sel.selectedDistrict ? $scope.sel.selectedDistrict.id == val.district_id : true;
                });
                $scope.filteredSchools = fs;
                // filter by selected circuit
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    return $scope.sel.selectedCircuit ? $scope.sel.selectedCircuit.id == val.circuit_id : true;
                });
                $scope.filteredSchools = fs;
                console.log("after filtering schools for pagination");console.log($scope.filteredSchools);

            };
            // change page when pagination link clicked
            $scope.changePage = function(){
                var nextPage = ($scope.pag.itemsPerPage*$scope.pag.currentPage) - $scope.pag.itemsPerPage;
                setFilteredSchools();
                $scope.pag.totalItems = $scope.filteredSchools.length;
                $scope.schools = $scope.filteredSchools.slice(nextPage, nextPage + $scope.pag.itemsPerPage);
            };

            // initialize filteredSchools
            var initializePaginationSettings = function(){
                setFilteredSchools();
                var ttlItems = $scope.filteredSchools.length;
                $scope.pag = {currentPage: 1, maxSize: 6, nextPage: 0, totalItems: ttlItems, itemsPerPage: 15};
                /*$scope.currentPage = 1;
                $scope.maxSize = 6;
                $scope.nextPage = 0;
                $scope.totalItems = $scope.filteredSchools.length;
                $scope.itemsPerPage = 3;*/
                $scope.changePage();
            };
            initializePaginationSettings();

            $scope.closeAndRoute = function(first_school_id){
                $modalInstance.dismiss('selected');
                var second_school_id = null;
                var secondSchoolExists = $location.path().match(/second/g);
                if(secondSchoolExists) second_school_id = $location.path().split("/").pop();

                // if second school not present
                if(!second_school_id){
                    $state.go('school.compare', {id: first_school_id});
                }else{
                    console.log("ROUTE SECOND SCHOOL ID");console.log(second_school_id);
                    $state.go('school.compare.second', {id: first_school_id, sid : second_school_id});
                }
            };

           /* $scope.ok = function () {
                $modalInstance.close($scope.selected.item);
            };*/

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };

        var SecondModalInstanceCtrl = function ($scope, $modalInstance, $location, schools, regions, districts, circuits) {

            $scope.schools = schools;
            $scope.regions = regions;
            $scope.districts = districts;
            $scope.circuits = circuits;
            // for schools sorting purposes
            /*$scope.sortOptions = [
                {value: "name", text: "Sort by Name"},
                {value: "status", text: "Sort by Status"}
            ];*/
            //$scope.sortOrder = $scope.sortOptions[0].value;

            $scope.ddDistricts = [];  // use {key: value}

            // initialize dropdown districts with all districts
            $scope.ddDistricts = angular.copy($scope.districts);

            // list of circuits in dropdown
            $scope.ddCircuits = [];
            $scope.ddCircuits = angular.copy($scope.circuits);
            // wrapped in an object to avoid nested scope issues
            $scope.sel = {selectedDistrict: null, selectedCircuit: null, searchquery: ""};

            // initialize checked regions for sorting
            $scope.searchRegionModalTwo = {
                1: true,
                2: true,
                3: true,
                4: true,
                5: true,
                6: true,
                7: true,
                8: true,
                9: true,
                10: true
            };
            // object to store names of regions
            $scope.regionName = {};
            // object to store names of districts
            $scope.districtName = {};
            // object to store name of circuit
            $scope.circuitName = {};

            $scope.searchDistrict = {};
            // function to filter schools by checked regions
            $scope.searchByRegionModalTwo = function(){
                return function(school){
                    if($scope.searchRegionModalTwo[school.region_id] === true){
                        return true;
                    }
                }
            };

            // function to toggle regions when clicked
            $scope.toggleRegions = function(region_id){
                $scope.searchRegionModalTwo[region_id] = $scope.searchRegionModalTwo[region_id] != true;
                $scope.sel.selectedDistrict = null;
                $scope.sel.selectedCircuit = null;
                initializePaginationSettings();
                console.log("Region " + region_id + "set to : " + $scope.searchRegionModalTwo[region_id]);
            };

            // function to filter dropdown districts based on checked regions
            $scope.getDistrictBySelectedRegionModalTwo = function(){
                return function(district){
                    return $scope.searchRegionModalTwo[district.region_id] == true;
                }
            };
            // function to filter dropdown circuits based on selected district
            $scope.getCircuitBySelectedDistrictModalTwo = function(){
                return function(circuit){
                    if(!$scope.sel.selectedDistrict) return false; // no district selected, no circuit
                    else return $scope.sel.selectedDistrict.id == circuit.district_id;
                }
            };

            // function to filter schools by selected district (from dropdown)
            $scope.searchByDistrictModalTwo = function(){
                return function(school){
                    if(!$scope.sel.selectedDistrict) return true;
                    else return $scope.sel.selectedDistrict.id == school.district_id;
                }
            };
            //function to filter school by selected circuit (from dropdown)
            $scope.searchByCircuitModalTwo = function(){
                return function(school){
                    if(!$scope.sel.selectedCircuit) return true;
                    else return $scope.sel.selectedCircuit.id == school.circuit_id;
                }
            };
            // Pagination
            // listen to change on search refine elements and reset
            // for the purpose of pagination
            $scope.$watch('sel.searchquery', function(newQuery){
                initializePaginationSettings();
            });
            $scope.$watch('sel.selectedDistrict', function(newSelectedDistrict){
                initializePaginationSettings();
            });
            $scope.$watch('sel.selectedCircuit', function(newSelectedCircuit){
                initializePaginationSettings();
            });

            /**
             *  Pagination
             */
            $scope.filteredSchools = [];
            // reset filtered schools
            var setFilteredSchools = function(){
                var fs = [];
                $scope.filteredSchools = []; // reset filtered schools
                $scope.filteredSchools = $filter('filter')(schools, $scope.sel.searchquery);
                // search by region
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    if($scope.searchRegionModalTwo[val.region_id] == true){
                        return true;
                    }
                });
                console.log(fs);
                $scope.filteredSchools = fs;
                console.log($scope.filteredSchools);
                // filter by selected district
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    return $scope.sel.selectedDistrict ? $scope.sel.selectedDistrict.id == val.district_id : true;
                });
                $scope.filteredSchools = fs;
                // filter by selected circuit
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    return $scope.sel.selectedCircuit ? $scope.sel.selectedCircuit.id == val.circuit_id : true;
                });
                $scope.filteredSchools = fs;
                console.log("after filtering schools for pagination");console.log($scope.filteredSchools);

            };
            // change page when pagination link clicked
            $scope.changePage = function(){
                var nextPage = ($scope.pag.itemsPerPage*$scope.pag.currentPage) - $scope.pag.itemsPerPage;
                setFilteredSchools();
                $scope.pag.totalItems = $scope.filteredSchools.length;
                $scope.schools = $scope.filteredSchools.slice(nextPage, nextPage + $scope.pag.itemsPerPage);
            };

            // initialize filteredSchools
            var initializePaginationSettings = function(){
                setFilteredSchools();
                var ttlItems = $scope.filteredSchools.length;
                $scope.pag = {currentPage: 1, maxSize: 6, nextPage: 0, totalItems: ttlItems, itemsPerPage: 15};
                $scope.changePage();
            };
            initializePaginationSettings();

            $scope.closeAndRoute = function(second_school_id){
                $modalInstance.dismiss('selected');
                $state.go('school.compare.second', {id: $stateParams.id, sid : second_school_id});
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };


    }]);