/**
 * Created by sophismay on 7/4/14.
 */

publicApp.controller('psSchoolsController', function($scope,$stateParams, $filter, $location, schools, regions, districts, circuits){
    $("ul.navbar-right").find("li").removeClass('active');
    $("ul.navbar-right > li.nav-schools").addClass('active');

    // get and initialize query if it exists
    var query = $location.search().query;
    if(query){
        $scope.searchquery = query;
    }

    $scope.schools = schools;
    $scope.regions = regions;
    $scope.districts = districts;
    $scope.circuits = circuits;
    $scope.regionNames = {};
    $scope.regionsObject = {}; // to store names of regions by id
    $scope.districtNames = {};
    $scope.circuitNames = {};
    $scope.schoolStatus = "";

    // initialize regionsObject
    angular.forEach($scope.regions, function(value, index){
        if(!this[value.id]) this[value.id] = value.name;
    }, $scope.regionsObject);

    // initialize checked regions for sorting
    $scope.searchRegion = {
        1: true,
        2: true,
        3: true,
        4: true,
        5: true,
        6: true,
        7: true,
        8: true,
        9: true,
        10: true
    };
    $scope.searchDistrict = {};
    //$scope.selectedDistrict = "none";

    // list of districts in dropdown: refined by selected regions
    $scope.ddDistricts = [];  // use {key: value}
    // initialize dropdown with all districts
    $scope.ddDistricts = angular.copy($scope.districts);

    // list of circuits in dropdown
    $scope.ddCircuits = [];
    $scope.ddCircuits = angular.copy($scope.circuits);

    // to store new school ratings
    $scope.newSchoolsRatings = {};

    // initialize new ratings for schools
    var setNewSchoolsRatings = function(){
        angular.forEach($scope.schools, function(val, key){
            // set review to null if not present or at least one review not present
            if(!val || !val.current_state_of_community_involvement || !val.current_state_of_teaching_and_learning
                || !val.current_state_of_friendliness_to_boys_girls || !val.current_state_of_healthy_level
                || !val.current_state_of_safety_and_protection || !val.current_state_of_school_inclusiveness){
                this[val.id] = null;
            }else{
                this[val.id] = Number(val.current_state_of_community_involvement)
                    + Number(val.current_state_of_teaching_and_learning)
                    + Number(val.current_state_of_friendliness_to_boys_girls)
                    + Number(val.current_state_of_healthy_level)
                    + Number(val.current_state_of_safety_and_protection)
                    + Number(val.current_state_of_school_inclusiveness);
                this[val.id] = (this[val.id]*100/60).toFixed();//Math.floor(this[val.id]*100/60);
            }
        }, $scope.newSchoolsRatings);
    };

    // set ratings for all schools
    setNewSchoolsRatings();

    // toggle regions check boxes and reset selected district and circuit
    $scope.toggleRegions = function(region_id){
        $scope.searchRegion[region_id] = $scope.searchRegion[region_id] != true;
        $scope.selectedDistrict = null;
        $scope.selectedCircuit = null;
        // reset for pagination
        initializePaginationSettings();
    };
    // function to filter dropdown districts based on checked regions
    $scope.getDistrictBySelectedRegion = function(){
        return function(district){
            return $scope.searchRegion[district.region_id]? true : false;
        }
    };
    // function to filter dropdown circuits based on selected district
    $scope.getCircuitBySelectedDistrict = function(){
        return function(circuit){
            if(!$scope.selectedDistrict) return false; // no district selected, no circuit
            else return $scope.selectedDistrict.id == circuit.district_id;
        }
    };

    // function to filter schools by selected district (from dropdown)
    $scope.searchByDistrict = function(){
        //console.log("DDD: " + d);
        return function(school){
            if(!$scope.selectedDistrict) return true;
            else return $scope.selectedDistrict.id == school.district_id;
            //if($scope.selectedDistrict) return $scope.selectedDistrict.id == school.district_id;


        }
    };
    //function to filter school by selected circuit (from dropdown)
    $scope.searchByCircuit = function(){
        return function(school){
            if(!$scope.selectedCircuit) return true;
            else return $scope.selectedCircuit.id == school.circuit_id;
        }
    };

    $scope.searchByRegion = function(){
        return function(school){
            if($scope.searchRegion[school.region_id] === true){
                return true;
            }
        }
    };

    // listen to change on search refine elements and reset
    // for the purpose of pagination
    $scope.$watch('searchquery', function(newQuery){
        initializePaginationSettings();
    });
    $scope.$watch('selectedDistrict', function(newSelectedDistrict){
        initializePaginationSettings();
    });
    $scope.$watch('selectedCircuit', function(newSelectedCircuit){
        initializePaginationSettings();
    });
    $scope.$watch('currentPage', function(newPage){
        // scroll to top of search results
        $('html,body').animate({
            scrollTop: $(".search-results").offset().top
        }, 500);
    });

    /**
     *  Pagination
     */
    $scope.filteredSchools = [];
    // reset filtered schools
    var setFilteredSchools = function(){
        var fs = [];
        $scope.filteredSchools = []; // reset filtered schools
        $scope.filteredSchools = $filter('filter')(schools, $scope.searchquery);
        // search by region
        fs = $filter('filter')($scope.filteredSchools, function(val){
            if($scope.searchRegion[val.region_id] == true){
                return true;
            }
        });
        console.log(fs);
        $scope.filteredSchools = fs;
        console.log($scope.filteredSchools);
        // filter by selected district
        fs = $filter('filter')($scope.filteredSchools, function(val){
            return $scope.selectedDistrict ? $scope.selectedDistrict.id == val.district_id : true;
        });
        $scope.filteredSchools = fs;
        // filter by selected circuit
        fs = $filter('filter')($scope.filteredSchools, function(val){
            return $scope.selectedCircuit ? $scope.selectedCircuit.id == val.circuit_id : true;
        });
        $scope.filteredSchools = fs;
        console.log("after filtering schools for pagination");console.log($scope.filteredSchools);

    };
    // change page when pagination link clicked
    $scope.changePage = function(){
        var nextPage = ($scope.itemsPerPage*$scope.currentPage) - $scope.itemsPerPage;
        setFilteredSchools();
        $scope.totalItems = $scope.filteredSchools.length;
        $scope.schools = $scope.filteredSchools.slice(nextPage, nextPage + $scope.itemsPerPage);
    };

    // initialize filteredSchools
    var initializePaginationSettings = function(){
        setFilteredSchools();
        $scope.currentPage = 1;
        $scope.maxSize = 10;
        $scope.nextPage = 0;
        $scope.totalItems = $scope.filteredSchools.length;
        $scope.itemsPerPage = 10;
        $scope.changePage();
    };
    initializePaginationSettings();
});