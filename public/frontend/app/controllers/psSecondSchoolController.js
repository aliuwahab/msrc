/**
 * Created by sophismay on 7/24/2014.
 */

publicApp.controller('psSecondSchoolController', ['$scope', '$stateParams', '$modal', '$filter', '$location', '$state','regions', 'districts', 'circuits', 'schools', 'headteacher',
    'reviews_questions',
    function($scope, $stateParams, $modal, $filter, $location, $state, regions, districts, circuits, schools, headteacher, reviews_questions){

        $("ul.navbar-right").find("li").removeClass('active');
        $("ul.navbar-right > li.nav-schools").addClass('active');

        $scope.schools = schools;
        $scope.regions = regions;
        $scope.circuits = circuits;
        $scope.districts = districts;

        // set first school
        //$scope.secondSchool = null;
        $scope.secondSchool = $filter('filter')(schools, {id: $stateParams.sid})[0];

        // object to store featured school's rating
        $scope.secondSchoolRating = null;

        //function to set featured school's rating
        var setSecondSchoolRating = function(){
            if(!$scope.secondSchool){
                $scope.secondSchoolRating = null;
            }
            if(!$scope.secondSchool.current_state_of_community_involvement || !$scope.secondSchool.current_state_of_teaching_and_learning
                || !$scope.secondSchool.current_state_of_friendliness_to_boys_girls || !$scope.secondSchool.current_state_of_healthy_level
                || !$scope.secondSchool.current_state_of_safety_and_protection || !$scope.secondSchool.current_state_of_school_inclusiveness){
                $scope.secondSchoolRating = null;
            }
            else{
                $scope.secondSchoolRating = Number($scope.secondSchool.current_state_of_community_involvement)
                    + Number($scope.secondSchool.current_state_of_teaching_and_learning)
                    + Number($scope.secondSchool.current_state_of_friendliness_to_boys_girls)
                    + Number($scope.secondSchool.current_state_of_healthy_level)
                    + Number($scope.secondSchool.current_state_of_safety_and_protection)
                    + Number($scope.secondSchool.current_state_of_school_inclusiveness);
                $scope.secondSchoolRating = ($scope.secondSchoolRating*100/60).toFixed();
            }
        };
        setSecondSchoolRating(); // set first school rating
        var ssRating = $scope.secondSchoolRating;
        console.log("second school rating");console.log($scope.secondSchool);

        var showSecondOverlay = function(elem){
         var el = $(elem);
         el.css({
         "display": "block"
         });
         //animate content
         $(elem + ' .dark-overlay-content').css({"display": "block"}).addClass('animated').addClass('fadeIn');
         // make body non-scrollable
         $('body').css({'overflow': 'hidden'});
         };

        // show second school overlay
        $scope.showSecondSchoolChangeOverlay = function(){
            $scope.showOverlay('#second-school-change-overlay');
            //showSecondOverlay("#second-school-change-overlay");
        };


        if(headteacher) $scope.secondHeadTeacherName = headteacher.last_name + ", " + headteacher.first_name;
        else $scope.secondHeadTeacherName = "name not available";

        $scope.secondSchool = $filter('filter')(schools, {id: $stateParams.sid})[0];

        /*
         Object to store questions of school Reviews
         */
        $scope.schoolQuestions = {};

        // function to assign questions to each review category
        var setQuestionsByCategory = function(questionsList){
            angular.forEach(questionsList, function(qObj, ind){
                var c = qObj.question_category_reference;
                var q = qObj.question;
                if(!this[c]) this[c] = [q];
                else this[c].push(q);
            }, $scope.schoolQuestions);
        };
        setQuestionsByCategory(reviews_questions);

        // get region name
        $scope.getSecondRegionName = function(region_id){
            var region = $filter('filter')($scope.regions, {id: region_id})[0];
            return region ? region.name : "Not Available";
        };

        // get district name
        $scope.getSecondDistrictName = function(district_id){
            var district = $filter('filter')($scope.districts, {id: district_id})[0];
            return district ? district.name : "Not Available";
        };
        $scope.getSecondCircuitName = function(circuit_id){
            var circuit = $filter('filter')($scope.circuits, {id: circuit_id})[0];
            return circuit ? circuit.name : "Not Available";
        };


        // function to remove second school
        $scope.removeSecondSchool = function(secondSchool){
            // scroll to search container
            $("html, body").animate({
                scrollTop:  $("#compare-search-container").offset().top
            }, 1000);

            window.location = '#/schools/' + $stateParams.id;
        };

        /**
         * function to return type of progress bar
         * @param val - current value of a review
         */
        $scope.getProgressbarType = function(val){
            if(val >= 8) return "success";
            else if(val >= 5) return null;
            else if(val >= 3) return "warning";
            else return "danger";
        };

        /**
         * function to calculate and return number of stars for school
         * @param rating - the rating of the school - Number
         */
        var calculateNumberOfStars = function(rating){
            var nStars = 0;
            if(rating >= 0 && rating <= 6) nStars = 0.5;
            else if(rating >=7 && rating <= 12) nStars = 1;
            else if(rating >=13 && rating <= 18) nStars = 1.5;
            else if(rating >=19 && rating <= 24) nStars = 2;
            else if(rating >=25 && rating <= 30) nStars = 2.5;
            else if(rating >=31 && rating <= 36) nStars = 3;
            else if(rating >=37 && rating <= 42) nStars = 3.5;
            else if(rating >=43 && rating <= 48) nStars = 4;
            else if(rating >=49 && rating <= 54) nStars = 4.5;
            else nStars = 5;
            return nStars;
        };

        // new function to set rating stars for school
        $scope.setSecondStars = function(secondRating){
            var starsNumber = calculateNumberOfStars(secondRating*60/100);
            var remainderLacking = 5 - starsNumber; // number of hollow stars
            var ul = $('ul#second-school-star-rating'); console.log("NUmBER OF STARRRS" +starsNumber);

            for(var i=0;i<Math.floor(starsNumber);i++){
                ul.append("<li class='colored'><i class='fa fa-star fa-2x'></i></li>");
            }
            // if decimal
            if(starsNumber % 1 > 0){
                ul.append("<li class='colored'><i class='fa fa-star-half-empty fa-2x'></i></li>");
            }
            for(var x=0;x<Math.floor(remainderLacking);x++){
                ul.append("<li class='colored'><i class='fa fa-star-o fa-2x'></i></li>");
            }
        };
        $scope.setSecondStars($scope.secondSchoolRating);

        $scope.hideMenu = function(){
            alert("HIDE ME");
        };


        /*
        *   Modal
        * */
        $scope.openSecondSchoolModal = function(size){
            var secondSchoolModalInstance = $modal.open({
                templateUrl: '/frontend/partials/second-school-modal.html',
                controller: SecondSchoolModalInstanceCtrl,
                size: size,
                resolve: {
                    schools: function(){
                        return schools;
                    },
                    regions: function(){
                        return regions;
                    },
                    districts: function(){
                        return districts;
                    },
                    circuits: function(){
                        return circuits;
                    }
                }
            });
        };

        var SecondSchoolModalInstanceCtrl = function ($scope, $modalInstance, $location, $state, schools, regions, districts, circuits) {

            $scope.schools = schools;
            $scope.regions = regions;
            $scope.districts = districts;
            $scope.circuits = circuits;console.log("circuits AGain");console.log($scope.circuits);
            // for schools sorting purposes

            $scope.ddDistricts = [];  // use {key: value}

            // initialize dropdown districts with all districts
            $scope.ddDistricts = angular.copy($scope.districts);

            // list of circuits in dropdown
            $scope.ddCircuits = [];
            $scope.ddCircuits = angular.copy($scope.circuits);

            // initialize checked regions for sorting
            $scope.searchRegion = {
                1: true,
                2: true,
                3: true,
                4: true,
                5: true,
                6: true,
                7: true,
                8: true,
                9: true,
                10: true
            };
            // object to store names of regions
            $scope.regionName = {};
            // object to store names of districts
            $scope.districtName = {};
            // object to store name of circuit
            $scope.circuitName = {};
            // wrapped in an object to avoid nested scope issues
            $scope.sel = {selectedDistrict: null, selectedCircuit: null, searchquery: ""};

            $scope.searchDistrict = {};
            // function to filter schools by checked regions
            $scope.searchByRegion = function(){
                return function(school){
                    if($scope.searchRegion[school.region_id] === true){
                        return true;
                    }
                }
            };

            // function to toggle regions and reset selected district and circuit
            $scope.toggleRegions = function(region_id){
                $scope.searchRegion[region_id] = $scope.searchRegion[region_id] != true;
                $scope.sel.selectedDistrict = null;
                $scope.sel.selectedCircuit = null;
                initializePaginationSettings();
            };

            // function to filter dropdown districts based on checked regions
            $scope.getDistrictBySelectedRegion = function(){
                return function(district){
                    return $scope.searchRegion[district.region_id] == true;
                }
            };
            // function to filter dropdown circuits based on selected district
            $scope.getCircuitBySelectedDistrict = function(){
                return function(circuit){
                    if(!$scope.sel.selectedDistrict) return false; // no district selected, no circuit
                    else return $scope.sel.selectedDistrict.id == circuit.district_id;
                }
            };

            // function to filter schools by selected district (from dropdown)
            $scope.searchByDistrict = function(){
                return function(school){
                    // if not null, filter school by district
                    if(!$scope.sel.selectedDistrict) return true;
                    else return $scope.sel.selectedDistrict.id == school.district_id;
                }
            };
            //function to filter school by selected circuit (from dropdown)
            $scope.searchByCircuit = function(){
                return function(school){
                    if(!$scope.sel.selectedCircuit) return true;
                    else return $scope.sel.selectedCircuit.id == school.circuit_id;
                }
            };

            // Pagination
            // listen to change on search refine elements and reset
            // for the purpose of pagination
            $scope.$watch('sel.searchquery', function(newQuery){
                initializePaginationSettings();
            });
            $scope.$watch('sel.selectedDistrict', function(newSelectedDistrict){
                initializePaginationSettings();
            });
            $scope.$watch('sel.selectedCircuit', function(newSelectedCircuit){
                initializePaginationSettings();
            });

            /**
             *  Pagination
             */
            $scope.filteredSchools = [];
            // reset filtered schools
            var setFilteredSchools = function(){
                var fs = [];
                $scope.filteredSchools = []; // reset filtered schools
                $scope.filteredSchools = $filter('filter')(schools, $scope.sel.searchquery);
                // search by region
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    if($scope.searchRegion[val.region_id] == true){
                        return true;
                    }
                });
                console.log(fs);
                $scope.filteredSchools = fs;
                console.log($scope.filteredSchools);
                // filter by selected district
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    return $scope.sel.selectedDistrict ? $scope.sel.selectedDistrict.id == val.district_id : true;
                });
                $scope.filteredSchools = fs;
                // filter by selected circuit
                fs = $filter('filter')($scope.filteredSchools, function(val){
                    return $scope.sel.selectedCircuit ? $scope.sel.selectedCircuit.id == val.circuit_id : true;
                });
                $scope.filteredSchools = fs;
                console.log("after filtering schools for pagination");console.log($scope.filteredSchools);

            };
            // change page when pagination link clicked
            $scope.changePage = function(){
                var nextPage = ($scope.pag.itemsPerPage*$scope.pag.currentPage) - $scope.pag.itemsPerPage;
                setFilteredSchools();
                $scope.pag.totalItems = $scope.filteredSchools.length;
                $scope.schools = $scope.filteredSchools.slice(nextPage, nextPage + $scope.pag.itemsPerPage);
            };

            // initialize filteredSchools
            var initializePaginationSettings = function(){
                setFilteredSchools();
                var ttlItems = $scope.filteredSchools.length;
                $scope.pag = {currentPage: 1, maxSize: 6, nextPage: 0, totalItems: ttlItems, itemsPerPage: 15};
                $scope.changePage();
            };
            initializePaginationSettings();

            $scope.closeAndRouteSecondSchool = function(second_school_id){
                $modalInstance.dismiss('selected');
                $state.go('school.compare.second', {id: $stateParams.id, sid : second_school_id});
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };


}]);